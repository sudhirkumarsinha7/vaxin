/* eslint-disable no-alert */
/* eslint-disable prettier/prettier */
import { insertNewProductList, insertNewOrder, insertNewPar } from './allSchemas';

export const addInventorylocal = async todoItems =>
  new Promise((resolve, reject) => {
    todoItems = todoItems.map(each => {
      if (each.mfgDate) {
      } else {
        delete each.mfgDate;
      }
      if (each.expDate) {
      } else {
        delete each.expDate;
      }
      return each;
    });
    console.log('addInventorylocal ' + JSON.stringify(todoItems));
    const newTodoList = {
      products: todoItems,
    };
    console.log('Data is inseration started' + JSON.stringify(newTodoList));
    insertNewProductList(newTodoList)
      .then(newData => {
        console.log('inseration data' + JSON.stringify(newData));
        resolve({ status: 200, data: newData });
      })
      .catch(error => {
        console.log(`Insert new todoList error ${error}`);
        alert(`Insert new todoList error ${error}`);
        // reject({ status: 500,data:error})
        resolve({ status: 500, data: newTodoList });
      });
  });

export async function addOrderlocal(todoItems) {
  // console.log(' addOrderlocal todoItems '+JSON.stringify(todoItems))
  // console.log('order Data is inseration started')
  await insertNewOrder(todoItems)
    .then()
    .catch(error => {
      alert(`Insert new todoList error ${error}`);
    });
  return { status: 200 };
}
export async function insertNewParlocal(todoItems) {
  // console.log(' addOrderlocal todoItems '+JSON.stringify(todoItems))
  // console.log('order Data is inseration started')
  await insertNewPar(todoItems)
    .then()
    .catch(error => {
      alert(`Insert new todoList error ${error}`);
    });
  return { status: 200 };
}
