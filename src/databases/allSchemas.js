/* eslint-disable prettier/prettier */
import Realm from 'realm';
export const PRODUCTS_LIST = 'products';
export const PRODUCTS_LIST_ORDER = 'Products_order';

export const INVENTORY_LIST_SCHEMA = 'Inventory';
export const ORDER_SCHEMA = 'OrderList';

export const SHIPMENT_SCHEMA = 'ShipmentList';
export const PRODUCTS_LIST_SHIPMENT = 'Products_shipment';
export const FROM_LOC_SCHEMA = 'Customer_location';
export const ADD_BENEFICIARY = 'Add_beneficiary';

export const productSchema = {
  name: PRODUCTS_LIST,
  // primaryKey: 'id',
  properties: {
    productId: { type: 'string', optional: true },
    quantity: { type: 'int', optional: true },
    mfgDate: { type: 'date', optional: true },
    expDate: { type: 'date', optional: true },
    batchNo: { type: 'string', optional: true },
    serialNo: { type: 'string', optional: true },
    units: { type: 'string', optional: true },
    inventoryId: { type: 'string', optional: true },
    manufacturerId: { type: 'string', optional: true },
    quantityPerUnit: { type: 'int', optional: true },
  },
};
export const InventorySchema = {
  name: INVENTORY_LIST_SCHEMA,
  properties: {
    products: {
      type: 'list',
      objectType: PRODUCTS_LIST,
    },
  },
};

export const productOrderSchema = {
  name: PRODUCTS_LIST_ORDER,
  // primaryKey: 'id',
  properties: {
    productId: { type: 'string', optional: true },
    quantity: { type: 'int', optional: true },
    units: { type: 'string', optional: true },
    productName: { type: 'string', optional: true },
    fromPage: { type: 'string', optional: true },
  },
};

export const orderSchema = {
  name: ORDER_SCHEMA,
  properties: {
    destination: { type: 'string', optional: true },
    source: { type: 'string', optional: true },
    fromName: { type: 'string', optional: true },
    toName: { type: 'string', optional: true },
    // locations: { type: 'object', optional: true }, // Free object property
    products: {
      type: 'list',
      objectType: PRODUCTS_LIST_ORDER,
    },
  },
};
//par
export const PRODUCTS_LIST_PAR = 'products_par';
export const PAR_SCHEMA = 'par';
export const PAR_SHIPMENT_SCHEMA = 'par_shipment';
export const PAR_DOCUMENT_SCHEMA = 'par_document';
export const PAR_SH_INDICATOR_SCHEMA = 'par_indicator';
export const PAR_CONDITION_SCHEMA = 'par_condition';
export const PAR_SIGNATURE_SCHEMA = 'par_Signature';
export const SIGNATURE_SCHEMA = 'signature';

export const parproductSchema = {
  name: PRODUCTS_LIST_PAR,
  // primaryKey: 'id',
  properties: {
    productId: { type: 'string', optional: true },
    quantity: { type: 'int', optional: true },
    noOfUnits: { type: 'int', optional: true },
    mfgDate: { type: 'date', optional: true },
    expDate: { type: 'date', optional: true },
    batchNo: { type: 'string', optional: true },
    manufacturerId: { type: 'string', optional: true },
    quantityPerUnit: { type: 'int', optional: true },
  },
};
export const parshipentSchema = {
  name: PAR_SHIPMENT_SCHEMA,
  properties: {
    poNo: { type: 'string', optional: true },
    consignee: { type: 'string', optional: true },
    originCountry: { type: 'string', optional: true },
    shortShipment: { type: 'bool', default: false },
    shortShipmentNotification: { type: 'bool', default: false },
    comments: { type: 'string', optional: true },
    products: {
      type: 'list',
      objectType: PRODUCTS_LIST_PAR,
    },
  },
};
export const pardocumentSchema = {
  name: PAR_DOCUMENT_SCHEMA,
  properties: {
    invoice: { type: 'bool', default: false },
    packingList: { type: 'bool', default: false },
    productArrivalReport: { type: 'bool', default: false },
    releaseCertificate: { type: 'bool', default: false },
    comment: { type: 'string', optional: true },
  },
};
export const parShInicatorSchema = {
  name: PAR_SH_INDICATOR_SCHEMA,
  properties: {
    noOfInspected: { type: 'int', optional: true },
    invoiceAmount: { type: 'int', optional: true },
  },
};
export const parConditionSchema = {
  name: PAR_CONDITION_SCHEMA,
  properties: {
    type: { type: 'string', optional: true },
    labelsAttached: { type: 'bool', default: false },
    comment: { type: 'string', optional: true },
  },
};
export const signSchema = {
  name: SIGNATURE_SCHEMA,
  properties: {
    name: { type: 'string', optional: true },
    sign: { type: 'string', optional: true },
    date: { type: 'date', optional: true },
  },
};
export const parSignatureSchema = {
  name: PAR_SIGNATURE_SCHEMA,
  properties: {
    inspectionSupervisor: {
      type: 'object',
      objectType: SIGNATURE_SCHEMA,
    },
    epiManager: {
      type: 'object',
      objectType: SIGNATURE_SCHEMA,
    },

    receivedDate: { type: 'date', optional: true },
    contact: { type: 'string', optional: true },
  },
};
export const parSchema = {
  name: PAR_SCHEMA,
  properties: {
    inventoryId: { type: 'string', optional: true },
    country: { type: 'string', optional: true },
    reportNo: { type: 'string', optional: true },
    reportDate: { type: 'date', optional: true },
    inspectionDateTime: { type: 'date', optional: true },
    enteredDateTime: { type: 'date', optional: true },
    shipmentDetails: {
      type: 'object',
      objectType: PAR_SHIPMENT_SCHEMA,
    },
    documents: {
      type: 'object',
      objectType: PAR_DOCUMENT_SCHEMA,
    },
    shippingIndicators: {
      type: 'object',
      objectType: PAR_SH_INDICATOR_SCHEMA,
    },
    conditions: {
      type: 'object',
      objectType: PAR_CONDITION_SCHEMA,
    },
    signatures: {
      type: 'object',
      objectType: PAR_SIGNATURE_SCHEMA,
    },
  },
};

const databaseOptions = new Realm({
  path: 'VL.realm',
  schema: [
    InventorySchema,
    productSchema,
    orderSchema,
    productOrderSchema,
    parSchema,
    parproductSchema,
    parshipentSchema,
    pardocumentSchema,
    parConditionSchema,
    parSignatureSchema,
    parShInicatorSchema,
    signSchema,
  ],
  schemaVersion: 2, //optional
});
// export default new Realm(databaseOptions);
export default databaseOptions;

export const insertNewProductList = newTodoList =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        realm.write(() => {
          realm.create(INVENTORY_LIST_SCHEMA, newTodoList);
          resolve(newTodoList);
        });
      })
      .catch(error => reject(error));
  });

export const queryAllInventryData = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allTodoLists = realm.objects(INVENTORY_LIST_SCHEMA);
        resolve(allTodoLists);
      })
      .catch(error => {
        resolve([]);
        reject(error);
      });
  });
export const queryDeleteAllInventryData = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allTodoLists = realm.objects(INVENTORY_LIST_SCHEMA);
        realm.write(() => {
          realm.delete(allTodoLists);
        });
        resolve();
      })
      .catch(error => {
        console.log(
          'queryDeleteAllInventryData error ' + JSON.stringify(error),
        );
        reject(error);
      });
  });
export const queryInventryDataByBatch = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allTodoLists = realm.objects(PRODUCTS_LIST);
        // let newList =allTodoLists.filter(each=>{
        //   if(each.batchNumber===batchNumber && each.expDate){
        //     return each
        //   }
        // })
        resolve(allTodoLists);
      })
      .catch(error => {
        reject(error);
      });
  });

//Order
export const insertNewOrder = newTodoList =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        realm.write(() => {
          realm.create(ORDER_SCHEMA, newTodoList);
          resolve(newTodoList);
        });
      })
      .catch(error => reject(error));
  });
export const queryAllorder = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allTodoLists = realm.objects(ORDER_SCHEMA);
        resolve(allTodoLists);
      })
      .catch(error => {
        reject(error);
      });
  });
export const queryDeleteAllOrder = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allTodoLists = realm.objects(ORDER_SCHEMA);
        realm.write(() => {
          realm.delete(allTodoLists);
        });
        resolve();
      })
      .catch(error => {
        console.log('queryDeleteAllOrder error ' + JSON.stringify(error));
        reject(error);
      });
  });

//Par
export const insertNewPar = newTodoList =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        realm.write(() => {
          realm.create(PAR_SCHEMA, newTodoList);
          resolve(newTodoList);
        });
      })
      .catch(error => reject(error));
  });
export const queryAllpar = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allTodoLists = realm.objects(PAR_SCHEMA);
        resolve(allTodoLists);
      })
      .catch(error => {
        reject(error);
      });
  });
export const queryDeleteAllPar = () =>
  new Promise((resolve, reject) => {
    Realm.open(databaseOptions)
      .then(realm => {
        let allTodoLists = realm.objects(PAR_SCHEMA);
        realm.write(() => {
          realm.delete(allTodoLists);
        });
        resolve();
      })
      .catch(error => {
        console.log('queryDeleteAllPar error ' + JSON.stringify(error));
        reject(error);
      });
  });
