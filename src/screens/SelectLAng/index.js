/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
// SelectLang.js
import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useGlobal } from '../../Helpers/GlobalContext';
import { fonts } from '@rneui/base';
import { HeaderWithBack } from '../../components/Header';
import { DropdownWithICon } from '../../components/Dropdown';
import { langList } from '../../config/global';
export const LangButton = props => {
    const { t } = useTranslation();

    const {
        title = '',
        currentLanguage,
        changeLanguage,
        id = '',
        icon = '',
    } = props;
    return (
        <TouchableOpacity
            onPress={() => changeLanguage()}
            style={{
                flexDirection: 'row',
                backgroundColor:
                    currentLanguage === id
                        ? '#ede6f7'
                        : '#F5F8F6',
                borderRadius: 12,
                padding: 9,
                alignItems: 'center',
                margin: 12,
            }}>
            <View
                style={{
                    width: 45,
                    height: 45,
                    borderRadius: 22,
                    borderWidth: 1,
                    justifyContent: 'center',
                    padding: 7,
                    borderColor: '#ede6f7',
                    backgroundColor: '#ede6f7',
                }}>
                <Text
                    style={{
                        textAlign: 'center',
                        fontSize: 16,
                        fontFamily: fonts.MEDIUM,
                        color: '#2E4138',
                    }}>
                    {icon}
                </Text>
            </View>
            <Text
                style={{
                    marginLeft: 22,
                    fontSize: 16,
                    fontFamily: fonts.REGULAR,
                }}>
                {(title)}
            </Text>
        </TouchableOpacity>
    );
};

const SelectLang = props => {
    const { t } = useTranslation();
    const insets = useSafeAreaInsets();
    const myContext = useGlobal();
    const changeAfterSelectLang = (item) => {
        const { value } = item;
        myContext?.storeLanguage(value);


    };
    return (
        <View style={{ backgroundColor: '#fff', flex: 1 }}>
            <HeaderWithBack navigation={props.navigation} name={t('Choose your language')} />
            <View style={{ marginTop: 50, marginHorizontal: 15 }}>
                <DropdownWithICon
                    labelText={t('Language')}
                    val={myContext.currentLanguage}
                    dropdownData={langList || []}

                    onChangeValue={changeAfterSelectLang}
                />
            </View>
            {/* <LangButton
                title="English"
                currentLanguage={myContext.currentLanguage}
                changeLanguage={() => myContext?.storeLanguage('en')}
                id={'en'}
                icon="A"
            />
            <LangButton
                title="বাংলা"
                currentLanguage={myContext.currentLanguage}
                changeLanguage={() => myContext?.storeLanguage('bn')}
                id={'bn'}
                icon="অ"
            />

            <LangButton
                title="తెలుగు"
                currentLanguage={myContext.currentLanguage}
                changeLanguage={() => myContext?.storeLanguage('te')}
                id={'te'}
                icon="అ"
            /> */}
        </View>
    );
};

export default SelectLang;
