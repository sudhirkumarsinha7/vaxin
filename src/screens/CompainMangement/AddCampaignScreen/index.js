/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    Keyboard,
    View,
    Text,
    StyleSheet,
    Platform,
    TouchableOpacity,
    Image,
    ActivityIndicator,
    PermissionsAndroid,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import {
    Colors,
    DeviceHeight,
} from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
    InputField,
    CustomButton,
    CustomButtonWithBorder,
} from '../../../components/Common/helper';

import { useIsFocused } from '@react-navigation/native';
import fonts from '../../../../FontFamily';
import { ToastShow } from '../../../components/Toast';
import { filterUniqueObjectsByProperty } from '../../inventory/AddInventory/productUtil';
import DropDown from '../../../components/Dropdown';
import { useRoute } from '@react-navigation/native';
import { DatePickerComponent } from '../../../components/DatePicker';
import { create_Compaign } from '../../../redux/action/campaign';
import { ImageUploadPopUp } from '../../../components/popUp';
import {
    selectPhotoFromGallary,
    takePhotoFromCamera,
} from '../../../components/ImageHelper';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
    getLocationList,
    getOrgLevel
} from '../../../redux/action/auth';
const AddNewLocation = () => {
    const add = {
        locationId: '',
    };
    return add;
};
export function FilterVaccineList(data) {
    return data
        .filter(
            item =>
                item?.product?.type === 'Vaccine' || item?.product?.type === 'VACCINE',
        )
        .map(item => ({ _id: item?.product?._id, name: item?.product?.name }));
}
const AddCampaignScreen = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [errorMessage, setErrorMessage] = useState('');

    const [localLoader, setLocalLoader] = useState(false);
    const [visible, setVisible] = useState(false);

    const [isMandatory, setIsMandatory] = useState(false);
    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    const route = useRoute();
    const { params } = route;
    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        await props.getOrgLevel();
    };

    var validationSchema = Yup.object().shape(
        {
            name: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            description: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            startDate: Yup.string().required(t('Required')),
            endDate: Yup.string().required(t('Required')),
            logoId: Yup.string().required(t('Required')),
            productId: Yup.string().required(t('Required')),
            administrationType: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            locationIds: Yup.array()
                .of(
                    Yup.object().shape({
                        locationId: Yup.string().required(t('Required')),
                    })
                )
                .test('is-unique', t('Location must be unique'), function (value) {
                    if (!value || value.length === 0) {
                        // Return true if array is empty or null (no validation needed)
                        return true;
                    }

                    const ids = value.map(item => item.locationId); // Extract locationIds
                    return new Set(ids).size === ids.length; // Check if all locationIds are unique
                }),

        },
        [],
    ); // <-- HERE!!!!!!!!
    const addCampaignForm = useFormik({
        initialValues: {
            name: '',
            description: '',
            logoId: '',
            startDate: new Date(),
            endDate: null,
            productId: '',
            administrationType: '',
            orgLevel: '',
            locationIds: [
                { 'locationId': '' },
            ],
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _create = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addCampaignForm.handleSubmit();
    };

    const handleSubmit = async values => {
        setLocalLoader(true);
        const locationIds = values.locationIds.filter(item => item.locationId).map(item => item.locationId);
        values.locationIds = locationIds;
        console.log('create_Compaign values' + JSON.stringify(values));

        const result = await props.create_Compaign(values);

        setLocalLoader(false);

        console.log('create_Compaign res result' + JSON.stringify(result));
        if (result?.status === 200) {
            ToastShow(t('Compaign created suceesfully'), 'success', 'long', 'top');
            props.navigation.goBack();
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };

    const onChangeStartDate = val => {
        addCampaignForm.handleChange({ target: { name: 'startDate', value: val } });
        addCampaignForm.handleChange({ target: { name: 'endDate', value: null } });
    };
    const onChangeEndDate = val => {
        addCampaignForm.handleChange({ target: { name: 'endDate', value: val } });
    };
    const changeAfterSelectProduct = item => {
        const { value } = item;
        addCampaignForm.handleChange({ target: { name: 'productId', value: value } });
    };
    const requestCameraPermission = async () => {
        if (Platform.OS === 'ios') {
            handleCameraPhoto();
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: t('App Camera Permission'),
                        message: t('App needs access to your camera'),
                        buttonNeutral: t('Ask Me Later'),
                        buttonNegative: t('Cancel'),
                        buttonPositive: t('OK'),
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    // console.log('Camera permission given');
                    handleCameraPhoto();
                } else {
                    // console.log('Camera permission denied');
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };
    const handleCameraPhoto = async () => {
        takePhotoFromCamera(setLogoSign, hideModal, onChangelogo);
    };
    const onChangelogo = val => {
        console.log('onChangelogo val', val)
        addCampaignForm.handleChange({ target: { name: 'logoId', value: val } });
    };
    const handleChoosePhoto = async () => {
        selectPhotoFromGallary(setLogoSign, hideModal, onChangelogo);
    };
    const setLogoSign = val => {
        addCampaignForm.handleChange({ target: { name: 'logoSign', value: val } });
    };
    const changeAfterSelectOrg = async item => {
        const { value } = item;
        // console.log('changeAfterSelectOrg value ' + value);
        await props.getLocationList(value);

        addCampaignForm.handleChange({ target: { name: 'orgLevel', value: value } });
        const newData = AddNewLocation();

        addCampaignForm.handleChange({ target: { name: 'locationIds', value: [newData] } });
    };

    const changeAfterSelectLocation = async (item, i) => {
        const { value } = item;
        addCampaignForm.handleChange({
            target: { name: `locationIds[${i}].locationId`, value: value },
        });
    };
    const AddAnotherLocation = () => {
        const data = [...addCampaignForm.values.locationIds];
        const newData = AddNewLocation();
        const finalData = [...data, newData];

        addCampaignForm.handleChange({ target: { name: 'locationIds', value: finalData } });
    };
    const cancel = index => {
        const productClone = JSON.parse(
            JSON.stringify(addCampaignForm?.values?.locationIds),
        );
        // console.log(Object.keys(productClone));
        productClone.splice(index, 1);

        addCampaignForm.handleChange({ target: { name: 'locationIds', value: productClone } });
    };
    const DeleteIcon = index => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        width: 40,
                        height: 40,
                        borderRadius: 400,
                        backgroundColor: '#fff',
                        justifyContent: 'center',
                        alignItems: 'center',
                        elevation: 10,
                        marginTop: 5,
                    }}
                    opacity={5}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'flex-end',
                        }}
                        onPress={() => cancel(index)}
                        disabled={
                            addCampaignForm?.values?.locationIds.length === 1 ? true : false
                        }>
                        <MaterialIcons
                            name="delete"
                            size={24}
                            color={
                                addCampaignForm?.values?.locationIds.length === 1 ? 'gray' : 'red'
                            }
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
    const LocationListComponent = () => {
        return (
            <View
                style={{
                    backgroundColor: '#E0F0EA',
                    paddingHorizontal: 15,
                    marginTop: 10,
                }}>
                {addCampaignForm?.values?.locationIds.map((each, index) => {
                    return (
                        <View>
                            <View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                    }}>
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            fontFamily: fonts.REGULAR,
                                            marginBottom: 5,
                                            textAlign: 'center',
                                        }}>
                                        {index + 1 + '. ' + t('Location')}
                                    </Text>
                                    {DeleteIcon(index)}
                                </View>
                                <DropDown
                                    dropdownData={locationList}
                                    onChangeValue={val => changeAfterSelectLocation(val, index)}

                                    label="name"
                                    mapKey="_id"
                                    val={addCampaignForm?.values?.locationIds[index]?.locationId}

                                    placeholderText={t('Location')}
                                    labelText={t('Location')}
                                    errorMsg={
                                        addCampaignForm.errors &&
                                        addCampaignForm.errors.locationIds &&
                                        addCampaignForm.errors.locationIds[index] &&
                                        addCampaignForm.errors.locationIds[index].locationId
                                    }
                                    mandatory={isMandatory}
                                    search={true}

                                />

                            </View>
                        </View>
                    );
                })}
                {addCampaignForm?.errors?.locationIds && typeof addCampaignForm?.errors?.locationIds === 'string' ? <Text
                    style={{
                        fontSize: 15,
                        fontFamily: fonts.REGULAR,
                        marginBottom: 5,
                        color: 'red',
                    }}>
                    {addCampaignForm?.errors?.locationIds}
                </Text> : null}
                <View style={{ flexDirection: 'row' }}>
                    <CustomButtonWithBorder
                        buttonName={t('Add more')}
                        onPressButton={AddAnotherLocation}
                        bdColor={'#BFBFBF'}
                        textColor={'#20232B'}
                        bgColor={'#fff'}
                    />
                </View>
            </View>
        );
    };
    const { inv_product_list = [], locationList = [], orgLevels = [] } = props;
    const uniqueProduct = filterUniqueObjectsByProperty(inv_product_list, 'name');
    const VaccineList1 = uniqueProduct.filter(each => {
        if (
            each.type === 'Vaccine' ||
            each.type === 'VACCINE' ||
            each.type === 'vaccine'
        ) {
            return each;
        }
    });
    const VaccineList = VaccineList1.sort((a, b) => a.name.localeCompare(b.name));
    // console.log('addCampaignForm?.errors  ' + JSON.stringify(addCampaignForm?.errors));
    // console.log('addCampaignForm?.values  ' + JSON.stringify(addCampaignForm?.values));

    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Add New Campaign')}
            />

            <ImageUploadPopUp
                isVisible={visible}
                onClose={hideModal}
                handleChoosePhoto={handleChoosePhoto}
                requestCameraPermission={() => requestCameraPermission('logoId')}
            />
            <VirtualizedList>
                <View style={{ marginHorizontal: 10 }}>
                    {props.loder || localLoader ? (
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    ) : null}
                </View>
                <View style={{ margin: 15 }}>
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Campaign Name')}
                        inputValue={addCampaignForm?.values?.name}
                        setInputValue={addCampaignForm.handleChange('name')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addCampaignForm?.errors?.name}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Campaign Description')}
                        inputValue={addCampaignForm?.values?.description}
                        setInputValue={addCampaignForm.handleChange('description')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addCampaignForm?.errors?.description}
                        mandatory={isMandatory}
                    />
                    <Text style={{ fontWeight: 14, marginBottom: 10 }}>{t('Campaign Logo')}</Text>

                    <View
                        style={{
                            borderWidth: 0.5,
                            height: DeviceHeight / 8,
                            borderRadius: 10,
                            justifyContent: 'center',
                        }}>
                        {addCampaignForm?.values?.logoId &&
                            addCampaignForm?.values?.logoSign ? (
                            <TouchableOpacity onPress={() => showModal()}>
                                <Image
                                    source={{ uri: addCampaignForm?.values?.logoSign }}
                                    style={{
                                        height: DeviceHeight / 8,
                                        justifyContent: 'center',
                                        borderRadius: 10,
                                    }}
                                />
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity onPress={() => showModal()}>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontWeight: 14,
                                        color: Colors.green4e,
                                    }}>
                                    {t('Upload Logo')}
                                </Text>
                            </TouchableOpacity>
                        )}
                    </View>
                    {isMandatory && addCampaignForm?.errors?.logoId && (
                        <Text
                            style={{
                                color: 'red',
                                fontSize: 12,
                                marginTop: 7,
                                fontFamily: fonts.MEDIUM,
                            }}>
                            {addCampaignForm?.errors?.logoId}
                        </Text>
                    )}
                    <DatePickerComponent
                        label={t('Start Date')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeStartDate(val)}
                        val={addCampaignForm?.values?.startDate}
                        errorMsg={addCampaignForm?.errors?.startDate}
                        mandatory={isMandatory}
                    // minDate={new Date()}
                    />
                    {addCampaignForm?.values?.startDate ? (
                        <DatePickerComponent
                            label={t('End Date')}
                            placeholder="DD/MM/YYYY"
                            onChange={val => onChangeEndDate(val)}
                            val={addCampaignForm?.values?.endDate}
                            errorMsg={addCampaignForm?.errors?.endDate}
                            mandatory={isMandatory}
                            minDate={
                                addCampaignForm?.values?.startDate
                                    ? new Date(addCampaignForm?.values?.startDate)
                                    : new Date()
                            }
                            isMandatoryField={false}
                        />
                    ) : null}

                    <DropDown
                        dropdownData={VaccineList}
                        onChangeValue={changeAfterSelectProduct}
                        label="name"
                        mapKey="_id"
                        val={addCampaignForm?.values?.productId}
                        placeholderText={t('Vaccine')}
                        labelText={t('Vaccine Administered')}
                        changeAfterSelect={() => console.log()}
                        errorMsg={addCampaignForm?.errors?.productId}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Administration Type')}
                        inputValue={addCampaignForm?.values?.administrationType}
                        setInputValue={addCampaignForm.handleChange('administrationType')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addCampaignForm?.errors?.administrationType}
                        mandatory={isMandatory}
                    />
                    <DropDown
                        labelText={t('Organization Level')}
                        val={addCampaignForm?.values?.orgLevel}
                        errorMsg={addCampaignForm?.errors?.orgLevel}
                        mandatory={isMandatory}
                        dropdownData={orgLevels}
                        label={'name'}
                        mapKey={'value'}
                        onChangeValue={changeAfterSelectOrg}
                    />
                    {LocationListComponent()}

                </View>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    {props.loder || localLoader ? (
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    ) : (
                        <CustomButton
                            buttonName={t('Create')}
                            onPressButton={_create}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    )}
                </View>
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        inv_product_list: state.inventory.inv_product_list,
        inv_list: state.inventory.inv_list,
        loder: state.loder,
        userInfo: state.auth.userInfo,
        locationList: state.auth.locationList,
        orgLevels: state.auth.orgLevels,



    };
}
export default connect(mapStateToProps, {
    create_Compaign,
    getLocationList,
    getOrgLevel
})(AddCampaignScreen);
