/* eslint-disable prettier/prettier */
import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Compaign from './index';
import AddCampaignScreen from './AddCampaignScreen';
import UpdateCampaignScreen from './UpdateCampaign';

const headerOptions = {
    headerShown: false,
};
const Stack = createNativeStackNavigator();

const CampaignStack = () => {
    return (
        <Stack.Navigator initialRouteName="Compaign" screenOptions={headerOptions}>
            <Stack.Screen
                name="Compaign"
                component={Compaign}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="AddCampaign"
                component={AddCampaignScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="UpdateCampaign"
                component={UpdateCampaignScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};
export default CampaignStack;
