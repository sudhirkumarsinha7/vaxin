/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../components/Header';
import { Colors } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { useIsFocused } from '@react-navigation/native';
import { getProdList } from '../../redux/action/inventory';
import { CampaignHeader, CampaignList, OnGoingCampaignList } from './helpler';
import Empty_Card from '../../components/Empty_Card';
import {
    getCampaignList,
    getActiveCampaignList,
} from '../../redux/action/campaign';
import {
    getLocationList,
    getOrgLevel
} from '../../redux/action/auth';
import fonts from '../../../FontFamily';
import { VirtualizedList } from '../inventory/AddInventory/productHelper';

const CampaignScreen = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [localLoader, setLocalLoader] = useState(false);

    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        setLocalLoader(true);
        await props.getCampaignList();
        await props.getActiveCampaignList();
        await props.getProdList();
        await props.getLocationList();

        setLocalLoader(false);
    };
    const _createShipment = item => {
        props.navigation.navigate('shipment', {
            screen: 'addshipment',
            params: {
                campaignDetail: item,
                acceptOrderDetail: {},
            },
        });
    };
    const _editCompaign = item => {
        props.navigation.navigate('UpdateCampaign', {
            campaignDetail: item,
        });
    };
    const loadMoreCampaign = async () => {
        await props.getCampaignList(campaign_page + 1);
    };
    const {
        userPermissions = {},
        campaign_list = [],
        campaign_active_list = [],
        userInfo,
        campaign_active_page = 1,
        campaign_page = 1,
    } = props;
    // console.log('campaign_list  ' + JSON.stringify(campaign_list))
    // const compaign_ongoing = campaign_list.filter(each => {
    //     if (each.isActive) {
    //         return each
    //     }
    // })
    let {
        firstName = '',
        lastName = '',
        orgLevel = '',
    } = userInfo;
    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Campaign Management')}
            />
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <View
                    style={{
                        backgroundColor: Colors.whiteFF,
                        borderTopLeftRadius: 10,
                        borderTopEndRadius: 10,
                        paddingBottom: 20,
                    }}>
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 12,
                        }}>
                        <View
                            style={{
                                borderRadius: 2,
                                width: scale(72),
                                height: scale(3),
                                backgroundColor: '#9B9B9B',
                            }}
                        />
                    </View>
                </View>
                <View style={{ marginHorizontal: 10 }}>
                    {props.loder || localLoader ? (
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    ) : null}
                </View>
                <View style={{ backgroundColor: '#fff', margin: 15 }}>
                    <Text style={{ fontFamily: fonts.BOLD, fontSize: 16 }}>
                        {t('Ongoing Campaign: ')}
                    </Text>
                    <FlatList
                        horizontal
                        data={campaign_active_list}
                        renderItem={({ item }) => (
                            <OnGoingCampaignList
                                item={item}
                                navigation={props.navigation}
                                createShipment={item => _createShipment(item)}
                                editCompaign={item => _editCompaign(item)}
                            />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Text>--</Text>}
                    />
                    <Text style={{ fontFamily: fonts.BOLD, fontSize: 16 }}>
                        {t('Campaign History :')}
                    </Text>
                </View>

                <VirtualizedList>
                    {/* <CampaignHeader /> */}
                    <FlatList
                        contentContainerStylse={{ paddingBottom: 100 }}
                        data={campaign_list}
                        renderItem={({ item }) => (
                            <CampaignList item={item}
                                navigation={props.navigation}
                                editCompaign={item => _editCompaign(item)}

                            />
                        )}
                        onEndReached={loadMoreCampaign}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                </VirtualizedList>
            </View>
            {userPermissions.CREATE_CAMPAIGN && (
                <View
                    style={{
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        marginRight: scale(15),
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 10,
                        }}>
                        <TouchableOpacity
                            onPress={() =>
                                props.navigation.navigate('AddCampaign', { details: {} })
                            }
                            style={{
                                backgroundColor: Colors.whiteFF,
                                borderRadius: scale(42),
                            }}>
                            <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
function mapStateToProps(state) {
    return {
        userPermissions: state.auth.userPermissions,
        campaign_list: state.campaign.campaign_list,
        campaign_active_list: state.campaign.campaign_active_list,
        userInfo: state.auth.userInfo,
        loder: state.loder,
        orgLevels: state.auth.orgLevels,
        campaign_page: state.campaign.campaign_page,
        campaign_active_page: state.campaign.campaign_active_page,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    getCampaignList,
    getActiveCampaignList,
    getLocationList,
    getOrgLevel
})(CampaignScreen);
