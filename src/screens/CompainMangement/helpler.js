/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { } from 'react';
import {
    View,
    Text,
    TouchableOpacity
} from 'react-native';

import { Colors } from '../../components/Common/Style';
import fonts from '../../../FontFamily';
import {
    formatDateDDMMYYYY,
} from '../../Util/utils';
import { CustomButton } from '../../components/Common/helper';
import { useTranslation } from 'react-i18next';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

export function removeNullKeys(obj) {
    const cleanedObj = {};

    for (const key in obj) {
        if (obj[key] !== null && obj[key] !== undefined) {
            cleanedObj[key] = obj[key];
        }
    }

    return cleanedObj;
}

export const CampaignHeader = () => {
    const { t } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.3, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Campaign Name')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13, marginTop: 2 }}>
                    {t('Status')}
                </Text>
            </View>
            <View style={{ flex: 0.5, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Vaccine Name')}
                </Text>
            </View>
            <View style={{ flex: 0.2, alignItems: 'flex-end', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Start Date')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13, marginTop: 2 }}>
                    {t('End Date')}
                </Text>
            </View>
        </View>
    );
};

export const CampaignListOld = props => {
    const { item = {} } = props;
    const { productDetails = {} } = item;
    const { t } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                borderBottomWidth: 1,
                borderBottomColor: '#E9ECF2',
                justifyContent: 'center',
                padding: 10,
                backgroundColor: '#fff',
            }}>
            <View style={{ flex: 0.3, justifyContent: 'center' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 14 }}>
                    {item.name + ' - ' + item.id}
                </Text>
                <Text
                    style={{
                        fontFamily: fonts.BOLD,
                        fontSize: 14,
                        marginTop: 5,
                        color: item.isActive ? Colors.green00 : Colors.red00,
                    }}>
                    {item.isActive ? t('Open') : t('Close')}
                </Text>
            </View>
            <View style={{ flex: 0.5, justifyContent: 'center' }}>
                <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 14 }}>
                    {productDetails.name}
                </Text>
            </View>
            <View style={{ flex: 0.2, alignItems: 'flex-end', marginRight: 15 }}>
                <Text
                    style={{ fontFamily: fonts.REGULAR, fontSize: 12, marginBottom: 5 }}>
                    {formatDateDDMMYYYY(item.startDate)}
                </Text>
                <Text style={{ fontFamily: fonts.REGULAR, fontSize: 12 }}>
                    {formatDateDDMMYYYY(item.endDate)}
                </Text>
            </View>
        </View>
    );
};
export const CampaignList = props => {
    const { item = {}, editCompaign } = props;
    const { productDetails = {} } = item;
    const { t } = useTranslation();

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
                padding: 10,
            }}>
            <View
                style={{

                    flexDirection: 'row',
                }}>
                <View style={{ flex: 0.5, flexDirection: 'row', }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: '#208196'
                        }}>
                        {item.id}
                    </Text>
                    {item.isActive && <TouchableOpacity
                        style={{ marginLeft: 10 }}
                        onPress={() => {
                            editCompaign(item)
                        }}>
                        <MaterialCommunityIcons
                            name={'square-edit-outline'}
                            color={'#7F7F7F'}
                            size={20}
                        />
                    </TouchableOpacity>}
                </View>
                <View style={{ flex: 0.5, alignItems: 'flex-end' }}>
                    <Text
                        style={{

                            fontFamily: fonts.MEDIUM,
                            fontSize: 11,
                            color: '#208196'
                        }}>
                        {formatDateDDMMYYYY(item.startDate) + ' To ' + formatDateDDMMYYYY(item.endDate)}

                    </Text>
                </View>
            </View>
            <View style={{
                marginTop: 10
            }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.8 }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                            }}>
                            {t('Campaign: ') + item.name}
                        </Text>
                        <Text style={{
                            fontFamily: fonts.MEDIUM, fontSize: 14, marginTop: 10
                        }}>
                            {t('Vaccine: ') + productDetails.name}
                        </Text>
                    </View>
                    <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                        <Text style={{
                            fontFamily: fonts.BOLD,
                            fontSize: 14,
                            marginTop: 5,
                            color: item.isActive ? Colors.green00 : Colors.red00
                        }}>
                            {item.isActive ? t('Open') : t('Close')}
                        </Text>
                    </View>
                </View>

            </View>

            {/* <View style={{ flex: 0.2, alignItems: 'flex-end', marginRight: 15 }}>
                <Text
                    style={{ fontFamily: fonts.REGULAR, fontSize: 12, marginBottom: 5 }}>
                    {formatDateDDMMYYYY(item.startDate)}
                </Text>
                <Text style={{ fontFamily: fonts.REGULAR, fontSize: 12 }}>
                    {formatDateDDMMYYYY(item.endDate)}
                </Text>
            </View> */}
        </View>
    );
};
export const OnGoingCampaignList = props => {
    const { item = {}, createShipment, editCompaign } = props;
    const { productDetails = {} } = item;
    const { t } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                borderWidth: 1,
                margin: 10,
                borderColor: '#E9ECF2',
                padding: 10,
                borderRadius: 15,
                marginBottom: 20,
            }}>
            <View style={{ flex: 0.9, justifyContent: 'center' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'center' }}>

                    <Text
                        style={{ fontFamily: fonts.MEDIUM, fontSize: 15, color: '#208196' }}>
                        {item.name + ' - ' + item.id}

                    </Text>
                    <TouchableOpacity
                        style={{ marginLeft: 10 }}
                        onPress={() => {
                            editCompaign(item)
                        }}>
                        <MaterialCommunityIcons
                            name={'square-edit-outline'}
                            color={'#7F7F7F'}
                            size={20}
                        />
                    </TouchableOpacity>
                </View>
                <Text
                    style={{
                        fontFamily: fonts.REGULAR,
                        fontSize: 11,
                        marginTop: 5,
                        color: '#7F7F7F',
                    }}>
                    {formatDateDDMMYYYY(item.startDate) +
                        ' to ' +
                        formatDateDDMMYYYY(item.endDate)}
                </Text>
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 12,
                        marginTop: 5,
                        color: '#7F7F7F',
                    }}>
                    {t('Vaccine: ') + productDetails.name}
                </Text>



                <CustomButton
                    bgColor={'#CFE7DE'}
                    textColor={'#208196'}
                    buttonName={t('Create shipment')}
                    navigation={props.navigation}
                    onPressButton={() => createShipment(item)}
                />
            </View>
            <View style={{ flex: 0.1, justifyContent: 'center' }}></View>
        </View>
    );
};
