/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ImageBackground,
    StyleSheet,
    TouchableOpacity,
    Keyboard,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback,
    ActivityIndicator,
    Image
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { useIsFocused } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { langList, localImage } from '../../config/global';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { LogIn } from '../../redux/action/auth';
import { registerNotification } from '../../redux/action/notification';

import {
    CustomInputText,
    CustomButton,
    CustomButtonLine,
    CustomTextLine,
    CustomInputPassword,
} from '../../components/Common/helper';
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;
import { HeaderIcon } from '../../components/Header';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import fonts from '../../../FontFamily';
import PhoneComponent from '../../components/Common/PhoneNumber';
import { isValidNumber } from 'react-native-phone-number-input';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import DropDown, { DropdownWithICon } from '../../components/Dropdown';
import { useGlobal } from '../../Helpers/GlobalContext';
import { Colors } from '../../components/Common/Style';

export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}
export const UsePhoneEmail = props => {
    const { icon, title = '' } = props;
    return (
        <View
            style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
            }}>
            <MaterialCommunityIcons name={icon} color={'#000'} size={20} />
            <Text style={{ color: '#000', marginLeft: 5, fontSize: 14 }}>{title}</Text>
        </View>
    );
};
const LoginScreen = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isMandatory, setIsMandatory] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const insets = useSafeAreaInsets();
    const [isEmail, setIsEmail] = useState(true);
    const myContext = useGlobal();

    const changePhoneEmail = () => {
        signinform.resetForm();

        // signinform.handleChange({ target: { name: `email`, value: '', } })
        // signinform.handleChange({ target: { name: `phoneNumber`, value: '', } })
        setIsMandatory(false);

        setIsEmail(!isEmail);
    };

    useEffect(() => {
        async function fetchDeta() {
            signinform.resetForm();
            setIsMandatory(false);
            setErrorMessage('');
            setIsEmail(true);
            signinform.handleChange({ target: { name: 'lang', value: myContext.currentLanguage } });

        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    var validationSchema = Yup.object()
        .shape(
            {
                email: Yup.string().matches(
                    reg_email,
                    t('Please enter a valid email.'),
                ),
                password: Yup.string()
                    .min(1, t('Too Short!'))
                    .max(256, t('Too Lonng!'))
                    .required(t('Required')),

                phoneNumber: Yup.string().test(
                    'is-valid-phone-number',
                    'Invalid phone number',
                    function (value) {
                        if (!value) {
                            return true; // Allow empty field if not required
                        }
                        return isValidNumber(value, signinform?.values?.countryCode);
                    },
                ),
            },
            ['email', 'mob'],
        )
        .test(
            'at-least-one-contact-field',
            'Either email or phone is required',
            function (values) {
                const { email, mob } = values;

                if (!email && !mob) {
                    return this.createError({
                        path: 'emailPhone',
                        message: t('Either email or phone is required'),
                    });
                }

                return true;
            },
        ); // <-- HERE!!!!!!!!
    const signinform = useFormik({
        initialValues: {
            email: '',
            phoneNumber: '',
            countryCode: 'BD',
            mob: '',
            lang: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _signin = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        setErrorMessage('');
        signinform.handleSubmit();
    };

    const handleSubmit = async values => {
        setLocalLoader(true);

        // console.log('handleSubmit values ' + JSON.stringify(values))
        const newData = values;
        const values1 = removeEmptyValues(newData);
        const result = await props.LogIn(values1);

        // console.log('res result' + JSON.stringify(result));
        if (result?.status === 200) {
            //do something
            RemotePushService();
            // props.navigation.navigate('logindashboard')
        } else if (result.status === 500) {
            setLocalLoader(false);
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else if (result?.status === 401) {
            setLocalLoader(false);
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else {
            // const err = result?.data?.data[0];
            // setErrorMessage(err?.msg);
            const err = result.data;
            setErrorMessage(err?.message);
            // ToastShow(
            //     t((err?.message)),
            //     'error',
            //     'long',
            //     'top',
            // )
        }
    };
    const RemotePushService = async (userIDPushNotification = '') => {
        setLocalLoader(true);

        PushNotification.configure({
            onRegister: async function (data) {
                var reqParamsRaw = {
                    notificationToken: data.token,
                    device_type: Platform.OS,
                };
                // console.log('reqParamsRaw' + JSON.stringify(reqParamsRaw));
                await props.registerNotification(reqParamsRaw);
                signinform.resetForm();

                // console.log(reqParamsRaw);
                setLocalLoader(false);
                props.navigation.navigate('main');
                // props.navigation.navigate('logindashboard')
            },
            onNotification: function (notification) {
                PushNotification.localNotification(notification);
                if (Platform.OS === 'ios') {
                    notification.finish(PushNotificationIOS.FetchResult.NoData);
                }
            },
            onAction: function (notification) {
                //  console.log("ACTION:", notification.action);
                console.log('NOTIFICATION:', notification);
            },
            onRegistrationError: function (err) {
                console.log('Not Registered:' + err.message);
            },
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },
            // Android only: GCM or FCM Sender ID
            senderID: '246292311598',
            popInitialNotification: true,
            requestPermissions: true,
        });
    };
    const handleResetPin = () => {
        props.navigation.navigate('resetpin');
    };
    const changeAfterSelectLang = (item) => {
        const { value } = item;
        myContext?.storeLanguage(value);
        console.log('changeAfterSelectLang value ', value);
        console.log('myContext.currentLanguage  ', myContext.currentLanguage)

        signinform.handleChange({ target: { name: 'lang', value: value } });
    };
    console.log('myContext.currentLanguage  ', myContext.currentLanguage);

    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={styles.container}>
                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    <View style={styles.container}>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                paddingTop: insets.top,
                            }}>
                            <HeaderIcon />

                            <View style={styles.formContainer}>
                                <View style={{ marginTop: 30, marginBottom: 40 }}>

                                    <DropdownWithICon
                                        labelText={t('Language')}
                                        val={signinform?.values?.lang}
                                        errorMsg={signinform?.errors?.lang}
                                        dropdownData={langList || []}

                                        onChangeValue={changeAfterSelectLang}
                                    />
                                    <View style={{ marginBottom: 20 }} />
                                    {isEmail ? (
                                        <CustomInputText
                                            label={t('Email')}
                                            placeholderText={t('Email')}
                                            val={signinform?.values?.email}
                                            onChange={text => {
                                                signinform.handleChange({
                                                    target: { name: 'email', value: text },
                                                });
                                                setErrorMessage('');
                                            }}
                                            errorMsg={signinform?.errors?.email}
                                            mandatory={isMandatory}
                                            islabel={true}
                                        />
                                    ) : (
                                        <PhoneComponent
                                            // label={'Phone number'}
                                            val={signinform?.values?.phoneNumber}
                                            onChange={text => {
                                                signinform.handleChange({
                                                    target: { name: 'mob', value: text },
                                                });
                                            }}
                                            onChangeFormattedText={text => {
                                                signinform.handleChange({
                                                    target: { name: 'phoneNumber', value: text },
                                                });
                                                setErrorMessage('');
                                            }}
                                            setCountryCode={text => {
                                                signinform.handleChange({
                                                    target: { name: 'countryCode', value: text },
                                                });
                                            }}
                                            countryCode={signinform?.values?.countryCode}
                                            errorMsg={signinform?.errors?.phoneNumber}
                                            mandatory={isMandatory}
                                            KeyboardType={'numeric'}
                                        />
                                    )}
                                    {isMandatory && signinform?.errors?.emailPhone ? (
                                        <Text
                                            style={{
                                                color: 'red',
                                                fontSize: 12,
                                                marginTop: 7,
                                                fontFamily: fonts.MEDIUM,
                                            }}>
                                            {signinform?.errors?.emailPhone}
                                        </Text>
                                    ) : null}
                                    <TouchableOpacity
                                        onPress={() => changePhoneEmail()}
                                        style={{ alignItems: 'flex-end', marginTop: 10 }}>
                                        {isEmail ? (
                                            <UsePhoneEmail icon={'phone'} title={t('Use Phone')} />
                                        ) : (
                                            <UsePhoneEmail icon={'email'} title={t('Use Email')} />
                                        )}
                                    </TouchableOpacity>
                                    <CustomInputPassword
                                        label={t('Password')}
                                        placeholderText={t('Enter Password')}
                                        val={signinform?.values?.password}
                                        onChange={text => {
                                            signinform.handleChange({
                                                target: { name: 'password', value: text },
                                            });
                                            setErrorMessage('');
                                        }}
                                        errorMsg={signinform?.errors?.password}
                                        mandatory={isMandatory}
                                        islabel={true}
                                    />
                                    {errorMessage && (
                                        <Text
                                            style={{
                                                color: 'red',
                                                fontSize: 12,
                                                marginTop: 7,
                                                fontFamily: fonts.MEDIUM,
                                            }}>
                                            {errorMessage}
                                        </Text>
                                    )}
                                    <View
                                        style={{
                                            flexDirection: 'row',
                                            paddingTop: 15,
                                            marginRight: 15,
                                        }}>
                                        <CustomTextLine
                                            name={t('Forgot Password?')}
                                        // textColor={'white'}
                                        />
                                        <CustomButtonLine
                                            buttonName={t('Reset Password')}
                                            onPressButton={handleResetPin}
                                            bgColor={'#4DAED8'}
                                        />
                                    </View>
                                </View>

                                {props.loder || localLoader ? (
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            flex: 1,
                                        }}>
                                        <ActivityIndicator size="large" color="#000" />
                                    </View>
                                ) : (
                                    <View>
                                        <CustomButton
                                            bgColor={'#208196'}
                                            buttonName={t('Continue')}
                                            navigation={props.navigation}
                                            onPressButton={_signin}
                                        />
                                        <Text
                                            style={{
                                                color: '#000',
                                                fontSize: 12,
                                                marginTop: 7,
                                                fontFamily: fonts.MEDIUM,
                                                textAlign: 'center',
                                            }}>
                                            {t('OR')}
                                        </Text>
                                        <CustomButton
                                            bgColor={Colors.YellowishBrown}
                                            buttonName={t('Login with HRIS')}
                                            navigation={props.navigation}
                                            onPressButton={() =>
                                                props.navigation.navigate('loginWithM')
                                            }
                                        />
                                    </View>
                                )}

                                <View
                                    style={{
                                        flexDirection: 'row',
                                        margin: scale(15),
                                        alignItems: 'center',
                                    }}>
                                    <CustomTextLine
                                        name={t('Don’t have an account?')}
                                    // textColor={'white'}
                                    />
                                    <CustomButtonLine
                                        buttonName={t('Sign Up')}
                                        onPressButton={() =>
                                            props.navigation.navigate('register')
                                        }


                                        bgColor={'#4DAED8'}
                                    />
                                </View>
                            </View>

                            <View style={{ height: scale(20) }} />
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
            {/* </TouchableWithoutFeedback> */}
        </ImageBackground>
    );
};
const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        margin: 12,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontFamily: fonts.BOLD,
        // color: 'white',
        fontSize: scale(16),
    },
    scrollContainer: {
        flexGrow: 1,
        justifyContent: 'flex-end',

        paddingBottom: 20,
    },
    formContainer: {
        width: '100%',
        backgroundColor: 'white',
        padding: 15,
        borderRadius: 15,

    },
    icon: {
        width: 24,
        height: 24,
        marginRight: 8,
    },
});

function mapStateToProps(state) {
    return {
        otpdata: state.auth.otpdata,
        userdata: state.auth.userdata,
        loder: state.loder,
    };
}

export default connect(mapStateToProps, {
    LogIn,
    registerNotification,
})(LoginScreen);
