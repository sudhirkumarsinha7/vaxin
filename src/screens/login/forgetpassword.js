/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View, Text, ImageBackground, StyleSheet, TouchableOpacity, Keyboard, ScrollView,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback,
    ActivityIndicator,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../config/global';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { sendOtp } from '../../redux/action/auth';
import { CustomInputText, CustomButton, CustomButtonLine, SubHeaderComponent, CustomTextLine } from '../../components/Common/helper';
import fonts from '../../../FontFamily';
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;
import { HeaderIcon } from '../../components/Header';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import PhoneComponent from '../../components/Common/PhoneNumber';
import { UsePhoneEmail } from '.';
import { Footer } from '../../components/Footer';

export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}

const FogetPass = (props) => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [isMandatory, setIsMandatory] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const insets = useSafeAreaInsets();
    const [isEmail, setIsEmail] = useState(true);
    useEffect(() => {
        async function fetchDeta() {
            setIsMandatory(false);
            setErrorMessage('');
        }
        fetchDeta();
    }, [isFocused]);
    const changePhoneEmail = () => {
        FogetPassForm.resetForm();

        // FogetPassForm.handleChange({ target: { name: `email`, value: '', } })
        // FogetPassForm.handleChange({ target: { name: `phoneNumber`, value: '', } })
        // FogetPassForm.handleChange({ target: { name: `mob`, value: '', } })
        setIsMandatory(false);

        setIsEmail(!isEmail);
    };

    var validationSchema = Yup.object().shape(
        {
            email: Yup.string().matches(reg_email, t('Please enter a valid email.')),

        },
        ['email', 'mob'],
    ).test('at-least-one-contact-field', 'Either email or phone is required', function (values) {
        const { email, mob } = values;

        if (!email && !mob) {
            return this.createError({ path: 'emailPhone', message: t('Either email or phone is required') });
        }

        return true;
    }); // <-- HERE!!!!!!!!
    const FogetPassForm = useFormik({
        initialValues: {
            email: '',
            phoneNumber: '',
            countryCode: 'BD',
            mob: '',
        },
        validationSchema,
        onSubmit: (values, actions) => {
            handleSubmit({ ...values });
        },
    });
    const _FogetPass = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        setErrorMessage('');

        FogetPassForm.handleSubmit();
    };

    const handleSubmit = async values => {
        // console.log('handleSubmit values ' + JSON.stringify(values))
        // props.navigation.navigate('setotp')
        const newData = {
            email: values.email,
            forgotPassword: true,
            resend: false,
            phoneNumber: values.phoneNumber,

        };
        const data = removeEmptyValues(newData);
        const result = await props.sendOtp(data);
        setLocalLoader(false);

        console.log('res result' + JSON.stringify(result));
        if (result?.status === 200) {

            props.navigation.navigate('setotp', values);
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            // ToastShow(
            //     t((err?.message)),
            //     'error',
            //     'long',
            //     'top',
            // )
        }
    };
    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={styles.container}
            >


                <ScrollView contentContainerStyle={styles.scrollContainer}>


                    <View
                        style={styles.container}>

                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                paddingTop: insets.top,
                            }}>


                            <HeaderIcon />





                            <View style={styles.formContainer}>
                                <View style={{ marginTop: 100, marginBottom: 40 }}>


                                    <SubHeaderComponent
                                        name={t('Reset Password')}
                                    // textColor={'white'}
                                    />
                                    <CustomTextLine
                                        name={t('We’ll send a verification code. Please enter your registered Email ID')}
                                    // textColor={'white'}
                                    />

                                    {isEmail ? <CustomInputText
                                        label={t('Email')}
                                        placeholderText={t('Email')}
                                        val={FogetPassForm?.values?.email}
                                        onChange={(text) => { FogetPassForm.handleChange({ target: { name: 'email', value: text } }); setErrorMessage(''); }}
                                        errorMsg={FogetPassForm?.errors?.email}
                                        mandatory={isMandatory}
                                        islabel={false}

                                    /> :
                                        <PhoneComponent
                                            // label={'Phone number'}
                                            val={FogetPassForm?.values?.phoneNumber}
                                            onChange={(text) => { FogetPassForm.handleChange({ target: { name: 'mob', value: text } }); }}
                                            onChangeFormattedText={(text) => { FogetPassForm.handleChange({ target: { name: 'phoneNumber', value: text } }); setErrorMessage(''); }}
                                            setCountryCode={(text) => { FogetPassForm.handleChange({ target: { name: 'countryCode', value: text } }); }}
                                            countryCode={FogetPassForm?.values?.countryCode}
                                            errorMsg={FogetPassForm?.errors?.phoneNumber}
                                            mandatory={isMandatory}
                                            KeyboardType={'numeric'}
                                        />}
                                    {isMandatory && FogetPassForm?.errors?.emailPhone ?
                                        <Text
                                            style={{
                                                color: 'red',
                                                fontSize: 12,
                                                marginTop: 7,
                                                fontFamily: fonts.MEDIUM,
                                            }}>{FogetPassForm?.errors?.emailPhone}</Text>
                                        : null}

                                    <TouchableOpacity onPress={() => changePhoneEmail()} style={{ alignItems: 'flex-end', marginTop: 10 }}>

                                        {isEmail ?
                                            <UsePhoneEmail icon={'phone'} title={t('Use Phone')} /> :
                                            <UsePhoneEmail icon={'email'} title={t('Use Email')} />}



                                    </TouchableOpacity>
                                </View>

                                {errorMessage ? <SubHeaderComponent
                                    name={errorMessage}
                                    textColor={'red'}
                                /> : null}
                                {props.loder || localLoader ? (
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            flex: 1,
                                        }}>

                                        <ActivityIndicator size="large" color="#0000ff" />

                                    </View>
                                ) : null}
                                <CustomButton
                                    bgColor={'#208196'}
                                    buttonName={t('Continue')}
                                    navigation={props.navigation}
                                    onPressButton={_FogetPass}
                                />
                                <View style={{ flexDirection: 'row', margin: scale(15), alignItems: 'center' }}>
                                    <CustomTextLine name={t('Don’t have an account?')}
                                    // textColor={'white'} 
                                    />
                                    <CustomButtonLine buttonName={t('Sign Up')} onPressButton={() => props.navigation.navigate('register')} bgColor={'#4DAED8'} />

                                </View>

                            </View>
                            {Platform.OS === 'ios' && <Footer navigation={props.navigation} />}

                            <View style={{ height: scale(20) }} />
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>

            {/* </TouchableWithoutFeedback> */}
        </ImageBackground>


    );
};
const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        margin: 12,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontFamily: fonts.BOLD,
        // color: 'white',
        fontSize: scale(16),
    },
    scrollContainer: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        paddingBottom: 20,
    },
    formContainer: {
        width: '100%',
        backgroundColor: 'white',
        padding: 15,
        borderRadius: 15,
    },

});

function mapStateToProps(state) {
    return {
        otpdata: state.auth.otpdata,
        userdata: state.auth.userdata,
        loder: state.loder,
    };
}

export default connect(
    mapStateToProps,
    { sendOtp },
)(FogetPass);
