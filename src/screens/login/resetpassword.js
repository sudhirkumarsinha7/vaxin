/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ImageBackground,
    StyleSheet,
    Keyboard,
    ActivityIndicator,
    Platform
} from 'react-native';
import { localImage } from '../../config/global';
import { scale } from '../../components/Scale';
import { useTranslation } from 'react-i18next';
import { HeaderIcon } from '../../components/Header';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import {
    CustomInputText,
    CustomButton,
    SubHeaderComponent,
    CustomInputPassword,
} from '../../components/Common/helper';
import fonts from '../../../FontFamily';
import { resetPassword } from '../../redux/action/auth';
import { connect } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import { Footer } from '../../components/Footer';

const passwordValidationRegex =
    /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/;

const passwordValidationMessage =
    'Password must be at least 8 characters long and contain at least one letter, one number, and one special character (@$!%*#?&)';

const ResetPasswordScreen = props => {
    const { t } = useTranslation();
    const insets = useSafeAreaInsets();
    const [isMandatory, setIsMandatory] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [localLoader, setLocalLoader] = useState(false);
    const isFocused = useIsFocused();

    useEffect(() => {
        async function fetchDeta() {
            setIsMandatory(false);
            setErrorMessage('');
        }
        fetchDeta();
    }, [isFocused]);
    var validationSchema = Yup.object().shape(
        {
            password: Yup.string()
                .matches(passwordValidationRegex, t(passwordValidationMessage))
                .required(t('Password is required')),
            confirmPassword: Yup.string()
                .oneOf([Yup.ref('password'), null], t('Passwords must match'))
                .required(t('Confirm Password is required')),
        },
        ['password', 'confirmPassword'],
    ); // <-- HERE!!!!!!!!
    const setPassform = useFormik({
        initialValues: {
            password: '',
            confirmPassword: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _setPass = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        setErrorMessage('');

        setPassform.handleSubmit();
    };

    const handleSubmit = async values => {
        console.log('handleSubmit values ' + JSON.stringify(values));
        const result = await props.resetPassword(values);
        setLocalLoader(false);

        // console.log('res result' + JSON.stringify(result));
        if (result?.status === 200) {
            setPassform.resetForm();

            props.navigation.navigate('Login');
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else {
            // const err = result?.data?.data[0];
            // setErrorMessage(err?.msg);
            const err = result.data;
            setErrorMessage(err?.message);
            // ToastShow(
            //     t((err?.message)),
            //     'error',
            //     'long',
            //     'top',
            // )
        }
    };

    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            <View style={styles.container}>
                <View
                    style={{
                        flex: 1,
                        backgroundColor: 'transparent',
                        paddingTop: insets.top,
                    }}>
                    <HeaderIcon />
                    <View style={{ marginTop: 50, margin: 15, flex: 1 }}>
                        <Text style={styles.title}>{t('Reset Password')}</Text>
                        <Text style={styles.subtitle}>{t('Please enter a new Password')}</Text>

                        <CustomInputPassword
                            label={t('Password')}
                            placeholderText={t('Enter Password')}
                            val={setPassform?.values?.password}
                            onChange={text => {
                                setPassform.handleChange({
                                    target: { name: 'password', value: text },
                                });
                            }}
                            errorMsg={setPassform?.errors?.password}
                            mandatory={isMandatory}
                            islabel={false}
                        />
                        <Text style={styles.subtitle}>{t('Confirm Password')}</Text>

                        <CustomInputText
                            label={t('Confirm Password')}
                            placeholderText={t('Enter Password')}
                            val={setPassform?.values?.confirmPassword}
                            onChange={text => {
                                setPassform.handleChange({
                                    target: { name: 'confirmPassword', value: text },
                                });
                            }}
                            errorMsg={setPassform?.errors?.confirmPassword}
                            mandatory={isMandatory}
                            islabel={false}
                        />
                        {errorMessage ? (
                            <SubHeaderComponent name={errorMessage} textColor={'red'} />
                        ) : null}
                        <View style={{ height: 50 }} />


                        {props.loder || localLoader ? (
                            <View
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    flex: 1,
                                }}>

                                <ActivityIndicator size="large" color="#000" />

                            </View>
                        ) : <CustomButton
                            bgColor={'#208196'}
                            buttonName={t('Continue')}
                            navigation={props.navigation}
                            onPressButton={_setPass}
                        />}
                        {Platform.OS === 'ios' && <Footer navigation={props.navigation} />}

                    </View>
                </View>
            </View>
        </ImageBackground>
    );
};

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        margin: 12,
    },
    title: {
        fontFamily: fonts.BOLD,
        // color: 'white',
        fontSize: 24,
    },
    subtitle: {
        fontFamily: fonts.REGULAR,
        // color: '#fff',
        fontSize: 20,
        marginTop: 15,
    },
});
function mapStateToProps(state) {
    return {
        orgLevels: state.auth.orgLevels,
        loder: state.loder,
    };
}

export default connect(mapStateToProps, { resetPassword })(ResetPasswordScreen);
