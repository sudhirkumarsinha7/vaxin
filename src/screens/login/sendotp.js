/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import {
    View,
    ImageBackground,
    StyleSheet,
    Platform,
    ActivityIndicator,
} from 'react-native';
import { localImage } from '../../config/global';
import {
    SubHeaderComponent,
} from '../../components/Common/helper';
import { useTranslation } from 'react-i18next';
import OTPComponent from '../../components/OTPComponet';
import { HeaderIcon } from '../../components/Header';
import { Footer } from '../../components/Footer';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useRoute } from '@react-navigation/native';
import { connect } from 'react-redux';
import { sendOtp, verifyOtp } from '../../redux/action/auth';
import { ToastShow } from '../../components/Toast';
export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}

const OTPVerify = props => {
    const { t } = useTranslation();
    const [otp, setOtp] = useState('');
    const insets = useSafeAreaInsets();
    const [errorMessage, setErrorMessage] = useState('');
    const [localLoader, setLocalLoader] = useState(false);

    const route = useRoute();

    const handleNumberPress = number => {
        // Concatenate the pressed number to the existing OTP
        if (otp.length < 4) {
            setOtp(otp + number.toString());
        }
    };

    const handleBackspacePress = () => {
        // Remove the last character from the OTP
        setOtp(otp.slice(0, -1));
    };

    const handleVerifyPress = async () => {
        // Implement OTP verification logic here
        // For example, you can send the OTP to a server for validation
        console.log('Verifying OTP:', otp);
        if (otp.length === 4) {
            const newData = {
                email: params.email,
                phoneNumber: params.phoneNumber,
                otp: otp,
            };
            const data = removeEmptyValues(newData);
            const result = await props.verifyOtp(data);
            setLocalLoader(false);

            console.log('res result' + JSON.stringify(result));
            if (result?.status === 200) {
                props.navigation.navigate('resetpass');
            } else if (result.status === 500) {
                const err = result?.data?.message;
                setErrorMessage(err);
                // ToastShow(
                //     t(err),
                //     'error',
                //     'long',
                //     'top',
                // )
            } else if (result?.status === 401) {
                const err = result?.data?.message;
                setErrorMessage(err);
                // ToastShow(
                //     t(err),
                //     'error',
                //     'long',
                //     'top',
                // )
            } else {
                const err = result.data;
                setErrorMessage(err?.message);
                // ToastShow(
                //     t((err?.message)),
                //     'error',
                //     'long',
                //     'top',
                // )
            }
        } else {
            ToastShow(t('Please enter 4 digit otp'), 'error', 'long', 'top');
        }
    };

    const handlePressbotomLine = async () => {
        const newData = {
            email: params.email,
            forgotPassword: false,
            resend: false,
            phoneNumber: params.phoneNumber,
        };

        const data = removeEmptyValues(newData);
        const result = await props.sendOtp(data);
        setLocalLoader(false);

        console.log('res result' + JSON.stringify(result));
        if (result?.status === 200) {
            setOtp('');
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            // ToastShow(
            //     t((err?.message)),
            //     'error',
            //     'long',
            //     'top',
            // )
        }
    };
    const { params } = route;

    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            <View style={styles.container}>
                <View
                    style={{
                        flex: 1,
                        backgroundColor: 'transparent',
                        paddingTop: insets.top,
                    }}>
                    <HeaderIcon />

                    {errorMessage ? (
                        <SubHeaderComponent name={errorMessage} textColor={'red'} />
                    ) : null}
                    {props.loder || localLoader ? (
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}>
                            <ActivityIndicator size="large" color="#fff" />
                        </View>
                    ) : null}
                    <OTPComponent
                        handleBackspacePress={handleBackspacePress}
                        handleNumberPress={handleNumberPress}
                        otp={otp}
                        noOfCode={4}
                        headerLine={t('Enter the code you received.')}
                        bottomline1={t("Didn't receive the OTP ?")}
                        bottomline2={t('Resend')}
                        buttonName={t('Continue')}
                        handleVerifyPress={handleVerifyPress}
                        handlePressbotomLine={handlePressbotomLine}
                    />

                    {Platform.OS === 'ios' && <Footer navigation={props.navigation} />}
                </View>
            </View>
        </ImageBackground>
    );
};

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },


    footer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'transparent', // Customize the background color
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 56, // Customize the height as needed
    },
    footerButton: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 10,
    },
});

function mapStateToProps(state) {
    return {
        orgLevels: state.auth.orgLevels,
        loder: state.loder,
    };
}

export default connect(mapStateToProps, { verifyOtp, sendOtp })(OTPVerify);
