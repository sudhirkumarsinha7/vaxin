/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react';
import {
    View,
    ImageBackground,
    StyleSheet,
    Image,
    Keyboard,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../config/global';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';

import { getUserInfo, getUserLocation } from '../../redux/action/auth';
import { CustomButton, SubHeaderComponent } from '../../components/Common/helper';
import { HeaderIcon } from '../../components/Header';
import fonts from '../../../FontFamily';
import { Footer } from '../../components/Footer';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const Login = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();

    const insets = useSafeAreaInsets();

    useEffect(() => {
        async function fetchData() {
            await props.getUserInfo();
            await props.getUserLocation();
            setTimeout(() => {
                _goto_dashboard();
            }, 1000);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const _goto_dashboard = () => {
        props.navigation.navigate('main');
        // props.navigation.navigate('main', {
        //     screen: 'home',
        // })
    };

    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={styles.container}>
                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    <View style={styles.container}>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                justifyContent: 'center',
                                paddingTop: insets.top,
                            }}>
                            <HeaderIcon />

                            <View style={styles.formContainer}>
                                <View style={{ marginTop: '20%', alignSelf: 'center' }}>
                                    <Image source={localImage.loginDashboard} />
                                </View>
                                <SubHeaderComponent
                                    name={'Welcome On Board'}
                                    textColor={'white'}
                                />
                                <View style={{ height: scale(30) }} />
                                <CustomButton
                                    bgColor={'#208196'}
                                    buttonName={t('Go To Dashboard')}
                                    navigation={props.navigation}
                                    onPressButton={_goto_dashboard}
                                />
                            </View>

                            <View style={{ height: scale(20) }} />
                        </View>
                        {Platform.OS === 'ios' && <Footer navigation={props.navigation} />}
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
            {/* </TouchableWithoutFeedback> */}
        </ImageBackground>
    );
};
const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        margin: 12,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontFamily: fonts.BOLD,
        color: 'white',
        fontSize: scale(16),
    },
    scrollContainer: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 20,
    },
    formContainer: {
        width: '100%',
        borderRadius: 15,

    },
});

function mapStateToProps(state) {
    return {
        otpdata: state.auth.otpdata,
        userdata: state.auth.userdata,
        loder: state.loder,
    };
}

export default connect(mapStateToProps, { getUserInfo, getUserLocation })(Login);
