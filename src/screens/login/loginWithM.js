/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ImageBackground,
    StyleSheet,
    TouchableOpacity,
    Keyboard,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback,
    ActivityIndicator,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { useIsFocused } from '@react-navigation/native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { langList, localImage } from '../../config/global';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { LogIn_hris } from '../../redux/action/auth';
import { registerNotification } from '../../redux/action/notification';

import {
    CustomInputText,
    CustomButton,
    CustomButtonLine,
    CustomTextLine,
    CustomInputPassword,
} from '../../components/Common/helper';
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;
import { HeaderIcon } from '../../components/Header';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import fonts from '../../../FontFamily';
import PhoneComponent from '../../components/Common/PhoneNumber';
import { isValidNumber } from 'react-native-phone-number-input';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import Dropdown, { DropdownWithICon } from '../../components/Dropdown';
import { useGlobal } from '../../Helpers/GlobalContext';
import { Footer } from '../../components/Footer';


export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}
export const UsePhoneEmail = props => {
    const { icon, title = '' } = props;
    return (
        <View
            style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
            }}>
            <MaterialCommunityIcons name={icon} color={'#fff'} size={20} />
            <Text style={{ color: '#fff', marginLeft: 5, fontSize: 14 }}>{title}</Text>
        </View>
    );
};
const LoginScreen = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isMandatory, setIsMandatory] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const insets = useSafeAreaInsets();
    const myContext = useGlobal();



    useEffect(() => {
        async function fetchDeta() {
            signinform.resetForm();
            setIsMandatory(false);
            setErrorMessage('');
            signinform.handleChange({ target: { name: 'lang', value: myContext.currentLanguage } });

        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    var validationSchema = Yup.object()
        .shape(
            {
                email: Yup.string().matches(
                    reg_email,
                    t('Please enter a valid email.'),
                ),
                password: Yup.string()
                    .min(1, t('Too Short!'))
                    .max(256, t('Too Lonng!'))
                    .required(t('Required')),
            },
            ['email'],
        )
        .test(
            'at-least-one-contact-field',
            'Either email or phone is required',
            function (values) {
                const { email, mob } = values;

                if (!email && !mob) {
                    return this.createError({
                        path: 'emailPhone',
                        message: t('Either email or phone is required'),
                    });
                }

                return true;
            },
        ); // <-- HERE!!!!!!!!
    const signinform = useFormik({
        initialValues: {
            email: '',
            lang: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _signin = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        setErrorMessage('');
        signinform.handleSubmit();
    };

    const handleSubmit = async values => {
        setLocalLoader(true);

        // console.log('handleSubmit values ' + JSON.stringify(values))
        const newData = values;
        const values1 = removeEmptyValues(newData);
        const result = await props.LogIn_hris(values1);

        // console.log('res result' + JSON.stringify(result));
        if (result?.status === 200) {
            //do something
            if (result?.data?.data?.token) {
                RemotePushService();

            } else {
                props.navigation.navigate('registerdashboard');
            }
            // signinform.resetForm();
            // props.navigation.navigate('logindashboard')
        } else if (result.status === 500) {
            setLocalLoader(false);
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else if (result?.status === 401) {
            setLocalLoader(false);
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else {
            // const err = result?.data?.data[0];
            // setErrorMessage(err?.msg);
            const err = result.data;
            setErrorMessage(err?.message);
            // ToastShow(
            //     t((err?.message)),
            //     'error',
            //     'long',
            //     'top',
            // )
        }
    };
    const RemotePushService = async (userIDPushNotification = '') => {
        setLocalLoader(true);

        PushNotification.configure({
            onRegister: async function (data) {
                var reqParamsRaw = {
                    notificationToken: data.token,
                    device_type: Platform.OS,
                };
                console.log('reqParamsRaw' + JSON.stringify(reqParamsRaw));
                await props.registerNotification(reqParamsRaw);
                // console.log(reqParamsRaw);
                signinform.resetForm();
                setLocalLoader(false);
                props.navigation.navigate('main');
                // props.navigation.navigate('logindashboard')
            },
            onNotification: function (notification) {
                PushNotification.localNotification(notification);
                if (Platform.OS === 'ios') {
                    notification.finish(PushNotificationIOS.FetchResult.NoData);
                }
            },
            onAction: function (notification) {
                //  console.log("ACTION:", notification.action);
                console.log('NOTIFICATION:', notification);
            },
            onRegistrationError: function (err) {
                console.log('Not Registered:' + err.message);
            },
            permissions: {
                alert: true,
                badge: true,
                sound: true,
            },
            // Android only: GCM or FCM Sender ID
            senderID: '246292311598',
            popInitialNotification: true,
            requestPermissions: true,
        });
    };

    const changeAfterSelectLang = (item) => {
        const { value } = item;
        myContext?.storeLanguage(value);
        // console.log('changeAfterSelectLang value ', value);
        // console.log('myContext.currentLanguage  ', myContext.currentLanguage);

        signinform.handleChange({ target: { name: 'lang', value: value } });
    };
    console.log('myContext.currentLanguage  ', myContext.currentLanguage);

    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={styles.container}>
                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    <View style={styles.container}>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                paddingTop: insets.top + 50,
                            }}>
                            {/* <HeaderIcon /> */}

                            <View style={styles.formContainer}>
                                <View style={{ marginTop: 10, marginBottom: 40 }}>
                                    <Text
                                        style={{
                                            fontSize: 12,
                                            padding: 15,
                                            fontFamily: fonts.MEDIUM,
                                            textAlign: 'center'
                                        }}>
                                        {t('Login with HRIS')}
                                    </Text>
                                    <DropdownWithICon
                                        labelText={t('Language')}
                                        val={signinform?.values?.lang}
                                        errorMsg={signinform?.errors?.lang}
                                        dropdownData={langList || []}

                                        onChangeValue={changeAfterSelectLang}
                                        search={false}
                                    />
                                    <View style={{ marginBottom: 20 }} />
                                    <CustomInputText
                                        label={t('Email')}
                                        placeholderText={t('Email')}
                                        val={signinform?.values?.email}
                                        onChange={text => {
                                            signinform.handleChange({
                                                target: { name: 'email', value: text },
                                            });
                                            setErrorMessage('');
                                        }}
                                        errorMsg={signinform?.errors?.email}
                                        mandatory={isMandatory}
                                        islabel={false}
                                    />
                                    <CustomInputPassword
                                        label={t('Password')}
                                        placeholderText={t('Enter Password')}
                                        val={signinform?.values?.password}
                                        onChange={text => {
                                            signinform.handleChange({
                                                target: { name: 'password', value: text },
                                            });
                                            setErrorMessage('');
                                        }}
                                        errorMsg={signinform?.errors?.password}
                                        mandatory={isMandatory}
                                        islabel={false}
                                    />
                                    {errorMessage && (
                                        <Text
                                            style={{
                                                color: 'red',
                                                fontSize: 12,
                                                marginTop: 7,
                                                fontFamily: fonts.MEDIUM,
                                            }}>
                                            {errorMessage}
                                        </Text>
                                    )}

                                </View>

                                {props.loder || localLoader ? (
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            flex: 1,
                                        }}>
                                        <ActivityIndicator size="large" color="#fff" />
                                    </View>
                                ) : (
                                    <CustomButton
                                        bgColor={'#208196'}
                                        buttonName={t('Continue')}
                                        navigation={props.navigation}
                                        onPressButton={_signin}
                                    />
                                )}
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        margin: scale(15),
                                        alignItems: 'center',
                                    }}>
                                    <CustomTextLine
                                        name={t('Already have an account?')}
                                    // textColor={'white'}
                                    />
                                    <CustomButtonLine
                                        buttonName={t('Log In')}
                                        onPressButton={() => props.navigation.navigate('Login')}
                                        bgColor={'#4DAED8'}
                                    />
                                </View>
                            </View>
                            {Platform.OS === 'ios' && <Footer navigation={props.navigation} />}

                            <View style={{ height: scale(20) }} />
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
            {/* </TouchableWithoutFeedback> */}
        </ImageBackground>
    );
};
const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        margin: 12,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontFamily: fonts.BOLD,
        // color: 'white',
        fontSize: scale(16),
    },
    scrollContainer: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        paddingBottom: 20,
    },
    formContainer: {
        width: '100%',
        backgroundColor: 'white',
        padding: 15,
        borderRadius: 15,

    },
});

function mapStateToProps(state) {
    return {
        otpdata: state.auth.otpdata,
        userdata: state.auth.userdata,
        loder: state.loder,
    };
}

export default connect(mapStateToProps, {
    LogIn_hris,
    registerNotification,
})(LoginScreen);
