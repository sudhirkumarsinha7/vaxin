/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    ImageBackground,
    StyleSheet,
    Keyboard,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    Text,
    TouchableWithoutFeedback,
    ActivityIndicator,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../config/global';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import {
    CustomInputText,
    CustomButton,
    CustomButtonLine,
    SubHeaderComponent,
    CustomTextLine,
} from '../../components/Common/helper';
import { HeaderIcon } from '../../components/Header';
import fonts from '../../../FontFamily';
import { getOrgLevel, register } from '../../redux/action/auth';
import DropDown, { DropDownComponent } from '../../components/Dropdown';
import PhoneComponent from '../../components/Common/PhoneNumber';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

import {
    isValidNumber,
} from 'react-native-phone-number-input';
import { Colors } from '../../components/Common/Style';
export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}
function removeSpaces(str) {
    return str.trim();
}
const reg_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})*(\s*)+$/;

const reg_string = /^[a-zA-Z][a-zA-Z\s]*$/;

const RegisterScreen = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [isMandatory, setIsMandatory] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const insets = useSafeAreaInsets();

    useEffect(() => {
        async function fetchDeta() {
            await props.getOrgLevel();
            signupform.resetForm();
            setIsMandatory(false);
            setErrorMessage('');
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const validationSchema = Yup.object()
        .shape(
            {
                firstName: Yup.string()
                    .matches(reg_string, t('First Name must contain only letters'))
                    .min(3, t('First Name must be at least 3 characters'))
                    .max(10, t('First Name must be at most 10 characters'))
                    .required(t('Required')),
                lastName: Yup.string()
                    .required(t('Required')).trim().min(2, t('Must be at least 2 characters long'))
                    .test('no-spaces', t('It must not be empty spaces'), (value) => value && value.trim() !== ''),
                orgLevel: Yup.string().required(t('Required')),
                email: Yup.string().matches(
                    reg_email,
                    t('Please enter a valid email.'),
                ),
                phoneNumber: Yup.string().test(
                    'is-valid-phone-number',
                    'Invalid phone number',
                    function (value) {
                        if (!value) {
                            return true; // Allow empty field if not required
                        }
                        return isValidNumber(value, signupform?.values?.countryCode);
                    },
                ),
            },
            ['firstName', 'lastName', 'email', 'orgLevel', 'phoneNumber'],
        )
        .test(
            'at-least-one-contact-field',
            'Either email or phone is required',
            function (values) {
                const { email, mob } = values;

                if (!email && !mob) {
                    return this.createError({
                        path: 'email',
                        message: t('Either email or phone is required'),
                    });
                }

                return true;
            },
        );
    const signupform = useFormik({
        initialValues: {
            email: '',
            firstName: '',
            lastName: '',
            orgLevel: '',
            phoneNumber: '',
            countryCode: 'BD',
            mob: '',
        },
        validationSchema,
        onSubmit: (values, actions) => {
            handleSubmit({ ...values });
        },
    });
    const _register = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        signupform.handleSubmit();
    };

    const handleSubmit = async values => {
        console.log('handleSubmit values', JSON.stringify(values));
        const newData = values;
        const values1 = removeEmptyValues(newData);
        // values1.email = removeSpaces(values1.email);
        values1.firstName = removeSpaces(values1.firstName);
        values1.lastName = removeSpaces(values1.lastName);
        console.log('removeEmptyValues values1 ' + JSON.stringify(values1));
        const result = await props.register(values1);
        setLocalLoader(false);

        console.log('res result' + JSON.stringify(result));
        if (result?.status === 200) {
            // signupform.resetForm()

            props.navigation.navigate('veryotp', values);
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            // ToastShow(
            //     t((err?.message)),
            //     'error',
            //     'long',
            //     'top',
            // )
        }
    };
    const changeAfterSelectOrg = item => {
        const { value } = item;

        signupform.handleChange({ target: { name: 'orgLevel', value: value } });
    };
    // console.log('handleSubmit values ', JSON.stringify(signupform?.values))

    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={styles.container}>
                <ScrollView contentContainerStyle={styles.scrollContainer}>
                    <View style={styles.container}>
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                justifyContent: 'center',
                                paddingTop: insets.top,
                            }}>
                            <View style={{ justifyContent: 'center', marginTop: scale(30) }}>
                                <HeaderIcon />
                            </View>

                            <View style={styles.formContainer}>
                                <SubHeaderComponent
                                    name={t('Create your Account')}
                                // textColor={'white'}
                                />

                                <CustomInputText
                                    label={t('First name')}
                                    val={signupform?.values?.firstName}
                                    onChange={text => {
                                        signupform.handleChange({
                                            target: { name: 'firstName', value: text },
                                        });
                                    }}
                                    errorMsg={signupform?.errors?.firstName}
                                    mandatory={isMandatory}
                                />
                                <CustomInputText
                                    label={t('Last name')}
                                    val={signupform?.values?.lastName}
                                    onChange={text => {
                                        signupform.handleChange({
                                            target: { name: 'lastName', value: text },
                                        });
                                    }}
                                    errorMsg={signupform?.errors?.lastName}
                                    mandatory={isMandatory}
                                />
                                <CustomInputText
                                    label={t('Email')}
                                    val={signupform?.values?.email}
                                    onChange={text => {
                                        signupform.handleChange({
                                            target: { name: 'email', value: text },
                                        });
                                    }}
                                    errorMsg={signupform?.errors?.email}
                                    mandatory={isMandatory}
                                />

                                <PhoneComponent
                                    label={t('Phone number')}
                                    val={signupform?.values?.phoneNumber}
                                    onChange={text => {
                                        signupform.handleChange({
                                            target: { name: 'mob', value: text },
                                        });
                                    }}
                                    onChangeFormattedText={text => {
                                        signupform.handleChange({
                                            target: { name: 'phoneNumber', value: text },
                                        });
                                    }}
                                    setCountryCode={text => {
                                        signupform.handleChange({
                                            target: { name: 'countryCode', value: text },
                                        });
                                    }}
                                    countryCode={signupform?.values?.countryCode}
                                    errorMsg={signupform?.errors?.phoneNumber}
                                    mandatory={isMandatory}
                                    KeyboardType={'numeric'}
                                />
                                <DropDown
                                    labelText={t('Organization Level')}
                                    val={signupform?.values?.orgLevel}
                                    errorMsg={signupform?.errors?.orgLevel}
                                    mandatory={isMandatory}
                                    dropdownData={props.orgLevels || []}
                                    label={'name'}
                                    mapKey={'value'}
                                    onChangeValue={changeAfterSelectOrg}
                                />
                                {/* <Text style={{ color: 'red' }}>{JSON.stringify(signupform)}</Text> */}
                                {errorMessage ? (
                                    <SubHeaderComponent name={errorMessage} textColor={'red'} />
                                ) : null}
                                <View style={{ marginTop: 12 }} />
                                {props.loder || localLoader ? (
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            flex: 1,
                                        }}>
                                        <ActivityIndicator size="large" color="#fff" />
                                    </View>
                                ) : (
                                    <View>
                                        <CustomButton
                                            bgColor={'#208196'}
                                            buttonName={t('Continue')}
                                            navigation={props.navigation}
                                            onPressButton={() => _register()}
                                        />
                                        <Text
                                            style={{
                                                color: '#fff',
                                                fontSize: 12,
                                                marginTop: 7,
                                                fontFamily: fonts.MEDIUM,
                                                textAlign: 'center',
                                            }}>
                                            {t('OR')}
                                        </Text>
                                        <CustomButton
                                            bgColor={Colors.YellowishBrown}
                                            buttonName={t('Login with HRIS')}
                                            navigation={props.navigation}
                                            onPressButton={() =>
                                                props.navigation.navigate('loginWithM')
                                            }
                                        />
                                    </View>
                                )}

                                <View
                                    style={{
                                        flexDirection: 'row',
                                        margin: scale(15),
                                        alignItems: 'center',
                                    }}>
                                    <CustomTextLine
                                        name={t('Already have an account?')}
                                    // textColor={'white'}
                                    />
                                    <CustomButtonLine
                                        buttonName={t('Log In')}
                                        onPressButton={() => props.navigation.navigate('Login')}
                                        bgColor={'#4DAED8'}
                                    />
                                </View>
                            </View>

                            <View style={{ height: scale(20) }} />
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
            {/* </TouchableWithoutFeedback> */}
        </ImageBackground>
    );
};
const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        // margin: 12
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontFamily: fonts.BOLD,
        // color: 'white',
        fontSize: scale(16),
    },
    scrollContainer: {
        // flexGrow: 1,
        // justifyContent: 'flex-end',
        paddingHorizontal: 15,
    },
    formContainer: {
        width: '100%',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 15,

    },
});

function mapStateToProps(state) {
    return {
        orgLevels: state.auth.orgLevels,
        loder: state.loder,
    };
}

export default connect(mapStateToProps, { register, getOrgLevel })(
    RegisterScreen,
);
