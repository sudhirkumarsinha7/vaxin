/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    ImageBackground,
    StyleSheet,
    ActivityIndicator,
} from 'react-native';
import { localImage } from '../../config/global';
import { scale } from '../../components/Scale';
import { useTranslation } from 'react-i18next';
import OTPComponent from '../../components/OTPComponet';
import { HeaderIcon } from '../../components/Header';
import fonts from '../../../FontFamily';
import { verifyOtp, sendOtp } from '../../redux/action/auth';
import { useRoute } from '@react-navigation/native';
import { connect } from 'react-redux';
import { ToastShow } from '../../components/Toast';
import {
    SubHeaderComponent,
} from '../../components/Common/helper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}
const verifyOtpScreen = props => {
    const { t, i18n } = useTranslation();
    const route = useRoute();

    const [otp, setOtp] = useState('');
    const [errorMessage, setErrorMessage] = useState('');
    const insets = useSafeAreaInsets();
    const [isMandatory, setIsMandatory] = useState(false);

    const [localLoader, setLocalLoader] = useState(false);

    useEffect(() => {
        async function fetchData() {
            handlePressbotomLine();
            setIsMandatory(false);
            setErrorMessage('');
        }
        fetchData();
    }, []);

    const handleNumberPress = number => {
        // Concatenate the pressed number to the existing OTP
        if (otp.length < 4) {
            setOtp(otp + number.toString());
        }
    };

    const handleBackspacePress = () => {
        // Remove the last character from the OTP
        setOtp(otp.slice(0, -1));
    };

    const handleVerifyPress = async () => {
        // Implement OTP verification logic here
        // For example, you can send the OTP to a server for validation
        console.log('Verifying OTP:', otp);
        if (otp.length === 4) {
            const newData = {
                email: params.email,
                phoneNumber: params.phoneNumber,
                otp: otp,
            };
            const data = removeEmptyValues(newData);

            const result = await props.verifyOtp(data);
            setLocalLoader(false);

            console.log('res result' + JSON.stringify(result));
            if (result?.status === 200) {
                props.navigation.navigate('setpass');
            } else if (result.status === 500) {
                const err = result?.data?.message;
                setErrorMessage(err);
                // ToastShow(
                //     t(err),
                //     'error',
                //     'long',
                //     'top',
                // )
            } else if (result?.status === 401) {
                const err = result?.data?.message;
                setErrorMessage(err);
                // ToastShow(
                //     t(err),
                //     'error',
                //     'long',
                //     'top',
                // )
            } else {
                const err = result.data;
                setErrorMessage(err?.message);
                // ToastShow(
                //     t((err?.message)),
                //     'error',
                //     'long',
                //     'top',
                // )
            }
        } else {
            ToastShow(t('Please enter 4 digit otp'), 'error', 'long', 'top');
        }
    };
    const handlePressbotomLine = async () => {
        const newData = {
            email: params.email,
            forgotPassword: false,
            resend: false,
            phoneNumber: params.phoneNumber,
        };
        const data = removeEmptyValues(newData);
        // console.log('res data' + JSON.stringify(data));

        const result = await props.sendOtp(data);
        setLocalLoader(false);

        console.log('res result' + JSON.stringify(result));
        if (result?.status === 200) {
            setOtp('');
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            // ToastShow(
            //     t(err),
            //     'error',
            //     'long',
            //     'top',
            // )
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            // ToastShow(
            //     t((err?.message)),
            //     'error',
            //     'long',
            //     'top',
            // )
        }
    };
    const { params } = route;
    console.log('params', params);
    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            <View style={styles.container}>
                <View style={{ justifyContent: 'center', paddingTop: insets.top }}>
                    <HeaderIcon />
                </View>

                {props.loder || localLoader ? (
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 1,
                        }}>
                        <ActivityIndicator size="large" color="#fff" />
                    </View>
                ) : null}
                {errorMessage ? (
                    <SubHeaderComponent name={errorMessage} textColor={'red'} />
                ) : null}
                <OTPComponent
                    handleBackspacePress={handleBackspacePress}
                    handleNumberPress={handleNumberPress}
                    otp={otp}
                    noOfCode={4}
                    headerLine={'Enter the code you received.'}
                    bottomline1={"Didn't receive the OTP ?"}
                    bottomline2={'Resend'}
                    buttonName={'Continue'}
                    handleVerifyPress={handleVerifyPress}
                    handlePressbotomLine={handlePressbotomLine}
                />
            </View>
        </ImageBackground>
    );
};

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontFamily: fonts.BOLD,
        color: 'white',
        fontSize: scale(16),
    },
    otpContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    otpDigitContainer: {
        borderWidth: 1,
        borderColor: 'white',
        width: scale(40),
        height: scale(40),
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        borderRadius: 12,
    },
    otpDigit: {
        fontSize: 18,
        color: 'white',
    },
    keypadContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        margin: 20,
        borderRadius: scale(20),
    },
    keypadButton: {
        width: 80,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'white',
        margin: 5,
        borderRadius: scale(20),
    },
    keypadButtonText: {
        fontSize: 24,
        color: 'white',
    },
    verifyButton: {
        backgroundColor: 'blue',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
    },
    verifyButtonText: {
        color: 'white',
        fontSize: 18,
    },
});
function mapStateToProps(state) {
    return {
        orgLevels: state.auth.orgLevels,
        loder: state.loder,
    };
}

export default connect(mapStateToProps, { verifyOtp, sendOtp })(verifyOtpScreen);
