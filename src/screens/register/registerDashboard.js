/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View, Text, ImageBackground, StyleSheet, TouchableOpacity, Image, Keyboard, ScrollView,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { localImage } from '../../config/global';
import { scale } from '../../components/Scale';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { sendOtp } from '../../redux/action/auth';
import { CustomButton, SubHeaderComponent } from '../../components/Common/helper';
import fonts from '../../../FontFamily';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { HeaderIcon } from '../../components/Header';

const RegisterDashboard = (props) => {
    const { t } = useTranslation();

    const [currentFlaour, setCurrentFlaour] = useState('VAXIN');
    const insets = useSafeAreaInsets();

    const _goto_dashboard = () => {
        props.navigation.navigate('Login');
    };

    useEffect(() => {
        async function fetchData() {
            const flavor = await AsyncStorage.getItem('productFlavour');
            setCurrentFlaour(flavor);

        }
        fetchData();
    }, []);
    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="stretch"
            source={localImage.backgraound}>
            {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={styles.container}
            >


                <ScrollView contentContainerStyle={styles.scrollContainer}>


                    <View
                        style={styles.container}>

                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                justifyContent: 'center',
                                paddingTop: insets.top,
                            }}>


                            <HeaderIcon />





                            <View style={styles.formContainer}>
                                <View style={{ marginTop: '20%', alignSelf: 'center', marginBottom: '10%' }} >
                                    <Image source={localImage.loginDashboard} />

                                </View>
                                <SubHeaderComponent
                                    name={t('Welcome On Board')}
                                // textColor={'white'}
                                />
                                <View style={{ height: scale(30) }} />


                                <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 16, textAlign: 'center' }}>{t('We have sent the your request to admin. Please wait for the Approval, Thank you')}</Text>

                                <View style={{ height: scale(30) }} />
                                <CustomButton
                                    bgColor={'#208196'}
                                    buttonName={t('Go To Login')}
                                    navigation={props.navigation}
                                    onPressButton={_goto_dashboard}
                                />
                                <CustomButton
                                    bgColor={'#208196'}
                                    buttonName={t('Register Another Account')}
                                    navigation={props.navigation}
                                    onPressButton={() => props.navigation.navigate('register')}
                                />


                            </View>

                            <View style={{ height: scale(20) }} />
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>

            {/* </TouchableWithoutFeedback> */}
        </ImageBackground>


    );
};
const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        margin: 12,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontFamily: fonts.BOLD,
        // color: 'white',
        fontSize: scale(16),
    },
    scrollContainer: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        paddingBottom: 20,
    },
    formContainer: {
        width: '100%',
        // backgroundColor: 'white',
        padding: 15,
        borderRadius: 15,

    },

});

function mapStateToProps(state) {
    return {
        otpdata: state.auth.otpdata,
        userdata: state.auth.userdata,
        loder: state.loder,
    };
}

export default connect(
    mapStateToProps,
    { sendOtp },
)(RegisterDashboard);
