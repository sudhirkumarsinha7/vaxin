/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import {
  Platform,
  View,
  StyleSheet,
  ActivityIndicator,
  StatusBar,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import { useIsFocused } from '@react-navigation/native';
import { jwtDecode } from 'jwt-decode';
import { getUserInfo, getUserLocation } from '../../redux/action/auth';
import setAuthToken from '../../config/setAuthToken';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import PushNotification from 'react-native-push-notification';
import { useGlobal } from '../../Helpers/GlobalContext';

import { request, PERMISSIONS, RESULTS } from 'react-native-permissions';

class NotificationsPermissions {
  static async requestPermissionsNotifications() {
    console.log('Platform.Version', Platform.Version);

    if (Platform.OS === 'android' && Platform.Version >= 33) {
      try {
        const result = await request(PERMISSIONS.ANDROID.POST_NOTIFICATIONS);
        // console.log('requestPermissionsNotifications result', result)
        // Handling the result of the permit request
        if (result === RESULTS.GRANTED) {
          console.log('Permissions granted');
        } else {
          console.log('Permissions not granted');
        }
      } catch (error) {
        // Error handling during permission request
        console.error(error);
      }
    }
  }
}

const AuthLoadingScreen = props => {
  const isFocused = useIsFocused();
  const myContext = useGlobal();

  useEffect(() => {
    NotificationsPermissions.requestPermissionsNotifications();
    myContext?.storeLanguage(myContext.currentLanguage)
    _loadData();
    // const backAction = () => {
    //   return true;
    // };
    // const backHandler = BackHandler.addEventListener(
    //   "hardwareBackPress",
    //   backAction
    // );
    // return () => backHandler.remove();
    // RemotePushService()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFocused]);

  const RemotePushService = async (userIDPushNotification = '') => {
    PushNotification.configure({
      onRegister: function (data) {
        var reqParamsRaw = {
          token_id: data.token,
          device_type: Platform.OS,
          username: userIDPushNotification,
        };
        console.log('reqParamsRaw' + JSON.stringify(reqParamsRaw));

        // console.log(reqParamsRaw);
      },
      onNotification: function (notification) {
        PushNotification.localNotification(notification);
        if (Platform.OS === 'ios') {
          notification.finish(PushNotificationIOS.FetchResult.NoData);
        }
      },
      onAction: function (notification) {
        //  console.log("ACTION:", notification.action);
        console.log('NOTIFICATION:', notification);
      },
      onRegistrationError: function (err) {
        console.log('Not Registered:' + err.message);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      // Android only: GCM or FCM Sender ID
      senderID: '246292311598',
      popInitialNotification: true,
      requestPermissions: true,
    });
  };
  const _loadData = async () => {
    const token = await AsyncStorage.getItem('token');

    console.log('auth_tokenn', token);
    if (token) {
      const decoded = jwtDecode(token);
      console.log('decode' + JSON.stringify(decoded));
      const currenTime = Date.now() / 1000;
      if (decoded.exp < currenTime) {
        await AsyncStorage.clear();
        props.navigation.navigate('Auth');
      } else {
        decoded.token = token;
        setAuthToken(token);
        await props.getUserInfo();
        await props.getUserLocation();
        props.navigation.navigate('main');
      }
    } else {
      props.navigation.navigate('Auth');
    }
  };

  return (
    <View style={styles.container}>
      <ActivityIndicator />
      {/* <Spinner size={50} type={'ThreeBounce'} color={'#5DC4DD'} style={{ alignItems: 'center', flex: 1, justifyContent: 'center',}} /> */}
      <StatusBar barStyle="default" />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
});

function mapStateToProps(state) {
  return {
    loder: state.loder,
  };
}

export default connect(mapStateToProps, { getUserInfo, getUserLocation })(
  AuthLoadingScreen,
);
