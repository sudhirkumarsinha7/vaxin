/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import { useTranslation } from 'react-i18next';
import { formatDateDDMMYYYYTime } from '../../../Util/utils';

import { useIsFocused } from '@react-navigation/native';
import fonts from '../../../../FontFamily';
import {
    getNotificationList,
    getOrderNotificationList,
    getShipmentNotificationList,
    getinventoryNotificationList,
    getUserNotificationList,
} from '../../../redux/action/notification';
import Empty_Card from '../../../components/Empty_Card';

const InventoryNotification = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();

    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        await props.getNotificationList();
        await props.getOrderNotificationList();
        await props.getShipmentNotificationList();
        await props.getinventoryNotificationList();
        await props.getUserNotificationList();
    };

    const eachItem = (item, index) => {
        return (
            <View style={{ padding: 10, borderBottomWidth: 0.5 }}>
                <Text
                    style={{ color: '#963B07', fontFamily: fonts.MEDIUM, fontSize: 14 }}>
                    {item.title}:
                    <Text style={{ color: '#7F7F7F', fontFamily: fonts.REGULAR }}>
                        {item.body}
                    </Text>
                    {/* Order Status:<Text style={{ color: '#7F7F7F', fontFamily: fonts.REGULAR }}>Your vaccine order (Order ID 326472) has been received; await confirmation.</Text> */}
                </Text>
                <Text style={{ marginTop: 7 }}>
                    {formatDateDDMMYYYYTime(item.updatedAt)}
                </Text>
            </View>
        );
    };
    const loadMoreNotification = async () => {
        await props.getinventoryNotificationList(inventory_notifications_list_page + 1);
    };
    const { inventory_notifications_list = [], inventory_notifications_list_page = 1 } = props;
    // console.log('notifications_list ' + JSON.stringify(inventory_notifications_list))
    return (
        <View style={styles.container}>
            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={inventory_notifications_list}
                onEndReached={loadMoreNotification}
                onEndReachedThreshold={20}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => eachItem(item, index)}
                ListEmptyComponent={<Empty_Card Text="No items available" />}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        notifications_list: state.notifications.notifications_list,
        order_notifications_list: state.notifications.order_notifications_list,
        shipment_notifications_list:
            state.notifications.shipment_notifications_list,
        inventory_notifications_list:
            state.notifications.inventory_notifications_list,
        lastmile_notifications_list:
            state.notifications.lastmile_notifications_list,
        notifications_list_page: state.notifications.notifications_list_page,
        order_notifications_list_page: state.notifications.order_notifications_list_page,
        shipment_notifications_list_page: state.notifications.shipment_notifications_list_page,
        inventory_notifications_list_page: state.notifications.inventory_notifications_list_page,
    };
}
export default connect(mapStateToProps, {
    getNotificationList,
    getOrderNotificationList,
    getShipmentNotificationList,
    getinventoryNotificationList,
    getUserNotificationList,
})(InventoryNotification);
