/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import {
    Image,
    View,
    StyleSheet,
    TouchableOpacity,
    Text
} from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { scale } from '../../components/Scale';
import { fonts, Colors } from '../../components/Common/Style';
import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';

import InventoryNotification from './Inventory';
import ShipmentNotification from './shipment';
import AllNotification from './AllNotification';

import OrderShipment from './Order';
import { localImage } from '../../config/global';
import Notifications from '../../assets/img/Notifications.svg'
import InventorySvg from '../../assets/img/inventory.svg'
import OrderSvg from '../../assets/img/order.svg'
import ShipmentSvg from '../../assets/img/shipment.svg'

export const CustomTabBar = ({ state, descriptors, navigation }) => {
    const { t, i18n } = useTranslation();

    return (
        <View style={styles.tabBar}>
            {state.routes.map((route, index) => {
                const { options } = descriptors[route.key];
                const label = options.tabBarLabel !== undefined
                    ? options.tabBarLabel
                    : options.title !== undefined
                        ? options.title
                        : route.name;

                const iconName = options.tabBarIcon;

                const isFocused = state.index === index;

                const onPress = () => {
                    const event = navigation.emit({
                        type: 'tabPress',
                        target: route.key,
                        canPreventDefault: true,
                    });

                    if (!isFocused && !event.defaultPrevented) {
                        navigation.navigate(route.name);
                    }
                };

                return (
                    <TouchableOpacity
                        key={index}
                        onPress={onPress}
                        style={[styles.tab, isFocused ? styles.focusedTab : null]}
                    >
                        {iconName ? iconName() : null}
                        <Text style={{ color: isFocused ? '#208196' : '#222', marginLeft: 10 }}>
                            {t(label)}
                        </Text>
                    </TouchableOpacity>
                );
            })}
        </View>
    );
};

const styles = StyleSheet.create({
    tabBar: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        justifyContent: 'space-around',
    },
    tab: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10,

    },
    focusedTab: {
        borderBottomWidth: 2,
        borderColor: '#208196',
        // width: '25%'

    },
});
;

const Tab = createMaterialTopTabNavigator();
const NotificationTab = props => {
    const isFocused = useIsFocused();
    const { t, i18n } = useTranslation();

    useEffect(() => {
        async function fetchData() { }
        fetchData();
    }, [isFocused]);
    return (
        <View style={{ flex: 1, backgroundColor: Colors.whiteFF }}>
            <HeaderWithBack navigation={props.navigation} name={t('Notifications')} />

            <Tab.Navigator
                initialRoute={'notification'}
                // screenOptions={{
                //     tabBarLabelStyle: {
                //         fontSize: scale(7),
                //         textTransform: 'capitalize',
                //         fontFamily: fonts.BOLD,
                //     },
                //     tabBarInactiveTintColor: '#7F7F7F',
                //     tabBarActiveTintColor: '#208196',

                //     tabBarIndicatorStyle: { backgroundColor: '#208196' },

                //     tabBarPressColor: '#208196',
                //     // tabBarShowLabel: false
                // }}
                tabBar={(props) => <CustomTabBar {...props} />}
            >
                <Tab.Screen
                    name="notification"
                    component={AllNotification}
                    options={{
                        // tabBarLabel: t('All'),
                        // tabBarOptions: {
                        //     showIcon: true,
                        //     showLabel: false,
                        // },
                        // tabBarIcon: ({ color, size }) => (
                        //     <Notifications height={20} width={20} />
                        // ),
                        tabBarLabel: 'All',
                        tabBarIcon: () => <Notifications height={20} width={20} />,
                    }}
                />
                <Tab.Screen
                    name="order_notification"
                    component={OrderShipment}
                    options={{
                        // tabBarLabel: t('Order'),
                        // tabBarOptions: {
                        //     showIcon: true,
                        //     showLabel: false,
                        // },
                        // tabBarIcon: ({ color, size }) => (
                        //     <OrderSvg height={20} width={20} />

                        // ),
                        tabBarLabel: 'Order',
                        tabBarIcon: () => <OrderSvg height={20} width={20} />,
                    }}
                />

                <Tab.Screen
                    name="shipment_notification"
                    component={ShipmentNotification}
                    options={{
                        // tabBarLabel: t('Shipment'),
                        // tabBarIcon: ({ color, size }) => (
                        //     <ShipmentSvg height={20} width={20} />
                        // ),
                        tabBarLabel: 'Shipment',
                        tabBarIcon: () => <ShipmentSvg height={20} width={20} />,
                    }}
                />
                <Tab.Screen
                    name="invetory_notification"
                    component={InventoryNotification}
                    options={{
                        // tabBarLabel: t('Inventory'),

                        // tabBarIcon: ({ color, size }) => (

                        //     <InventorySvg height={20} width={20} />
                        // ),
                        tabBarLabel: 'Inventory',
                        tabBarIcon: () => <InventorySvg height={20} width={20} />,
                    }}
                />


            </Tab.Navigator>
        </View>
    );
};
export default NotificationTab;
