/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';
import setAuthToken from '../../config/setAuthToken';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { Colors } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { log_out, getUserInfo, getUserLocation } from '../../redux/action/auth';
import fonts from '../../../FontFamily';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CardNavigationBtn } from '../../components/Common/Button';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { localImage } from '../../config/global';
import { ConfirmationPopUp } from '../../components/popUp';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Image } from '@rneui/base';
import UserProfile from '../../assets/img/UserProfile.svg'
import Notifications from '../../assets/img/Notifications.svg'
import VaccineReport from '../../assets/img/VaccineReport.svg'
import ColdChain from '../../assets/img/ColdChain.svg'
import LastMile from '../../assets/img/LastMile.svg'
import ProductReport from '../../assets/img/ProductReport.svg'
import Campaign from '../../assets/img/campaign.svg'
import Track_map from '../../assets/img/track_map.svg'

const DrawerHeader = props => {
    const { t } = useTranslation();
    const insets = useSafeAreaInsets();
    const [isDeletePrdPopup, setIsDeletePrdPopup] = useState(false);

    const { userInfo = {}, userLocation = {} } = props;
    let {
        firstName = '',
        lastName = '',
        orgLevel = '',
    } = userInfo;
    return (
        // <View
        //     style={{
        //         backgroundColor: Colors.headerBackground,
        //         // paddingVertical: 10
        //         paddingTop: 16,
        //     }}>
        <ImageBackground
            style={{
                paddingVertical: 15,
                // backgroundColor: Colors.headerBackground,
            }}
            resizeMode="stretch"
            source={localImage.backgraoundHeader}>
            <View
                style={{
                    paddingTop: insets.top + 30,
                    paddingVertical: 10,
                    // flexDirection: 'row',
                    // alignItems: 'center',
                }}>
                <ConfirmationPopUp
                    title={t('Logout')}
                    message={t('Are you sure you want to logout ?')}
                    isVisible={isDeletePrdPopup}
                    onClose={() => setIsDeletePrdPopup(false)}
                    confirm={() => {
                        setIsDeletePrdPopup(false);
                        props.onPressButton();
                    }}
                />
                <View style={{ paddingHorizontal: scale(15) }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.9 }}>
                            <UserProfile height={30} width={30} />
                        </View>

                        <TouchableOpacity
                            onPress={() => setIsDeletePrdPopup(true)}
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                            }}>
                            <Text
                                style={{
                                    fontSize: 16,
                                    color: '#000',
                                    fontFamily: fonts.MEDIUM,
                                    marginRight: 10,
                                }}>
                                {t('Logout')}
                            </Text>
                            <FontAwesome name="power-off" color={'red'} size={16} />
                        </TouchableOpacity>
                    </View>
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: fonts.BOLD,
                            marginTop: 14,
                            color: '#000',
                        }}
                        numberOfLines={1}
                        ellipsizeMode="tail">
                        {firstName + ' ' + lastName}
                    </Text>
                    <Text
                        style={{
                            fontSize: 16,
                            color: '#000',
                            fontFamily: fonts.REGULAR,
                            marginTop: 5,
                        }}
                        numberOfLines={1}
                        ellipsizeMode="tail">
                        {orgLevel}
                    </Text>
                    <Text
                        style={{
                            fontSize: 16,
                            color: '#000',
                            fontFamily: fonts.REGULAR,
                            marginTop: 7,
                        }}
                        numberOfLines={1}
                        ellipsizeMode="tail">
                        {t('Role : ') + userInfo?.roleId?.name}
                    </Text>
                    <Text
                        style={{
                            fontSize: 16,
                            color: '#000',
                            fontFamily: fonts.REGULAR,
                            marginTop: 7,
                        }}
                        numberOfLines={1}
                        ellipsizeMode="tail">
                        {t('Location: ') + userLocation?.name}
                    </Text>
                </View>
            </View>
            {/* <CardNavigationBtn
                image={<FontAwesome name="power-off" color={'#000'} size={16} />}
                bgColor={'#CFE7DE'}
                title="Logout"
                type="icon"
                onPressButton={() => setIsDeletePrdPopup(true)} /> */}
        </ImageBackground>
    );
};

const MenuScreen = props => {
    const { t } = useTranslation();


    useEffect(() => {
        async function fetchDeta() {
            const token = await AsyncStorage.getItem('token');
            if (token) {
                await props.getUserInfo();
                await props.getUserLocation();
            }
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const _logout = async () => {
        await props.log_out();
        await AsyncStorage.clear();
        setAuthToken('');
        // props.navigation.navigate('Auth', { screen: 'Login' });;
        props.navigation.reset({
            index: 0,
            routes: [{ name: 'Auth' }], // Navigate to your login screen
        });
        // props.navigation.navigate('Login');;
    };
    const { userInfo = {}, userPermissions = {}, userLocation = {} } = props;

    return (
        <View style={{ flex: 1, width: '100%' }}>
            <DrawerHeader userInfo={userInfo} onPressButton={_logout} userLocation={userLocation} />

            {/* <DrawerContentScrollView {...props}> */}

            <View style={{ backgroundColor: '#fff' }}>
                <CardNavigationBtn
                    type="icon"
                    image={<UserProfile height={25} width={25} />}
                    title={t('Profile')}
                    bgColor={'#CFE7DE'}
                    onPressButton={() => props.navigation.navigate('profile')}
                />
                <CardNavigationBtn
                    type="icon"
                    image={<Notifications height={25} width={25} />}
                    title={t('Notifications')}
                    onPressButton={() => props.navigation.navigate('notification')}
                />
                {userPermissions.VIEW_COLDCHAIN && (
                    <CardNavigationBtn
                        type="icon"
                        image={<ColdChain height={25} width={25} />}

                        title={t('Cold Chain')}
                        onPressButton={() => props.navigation.navigate('coldchain')}
                    />
                )}
                {userPermissions.VIEW_VAR && (
                    <CardNavigationBtn
                        type="icon"
                        image={<VaccineReport height={25} width={25} />}

                        title={t('VAR')}
                        onPressButton={() => props.navigation.navigate('var')}
                    />
                )}
                {userPermissions.LAST_MILE && (
                    <CardNavigationBtn
                        type="icon"
                        image={<LastMile height={25} width={25} />}
                        title={t('Last Mile')}
                        onPressButton={() => props.navigation.navigate('Beneficiaries')}
                    />
                )}
                {userPermissions.VIEW_PAR && (
                    <CardNavigationBtn
                        type="icon"
                        image={<ProductReport height={25} width={25} />}
                        title={t('PAR')}
                        onPressButton={() => props.navigation.navigate('par')}
                    />
                )}

                {userPermissions.VIEW_CAMPAIGN && (
                    <CardNavigationBtn
                        type="icon"
                        image={<Campaign height={25} width={25} />}
                        title={t('Campaign')}
                        onPressButton={() => props.navigation.navigate('Campaign')}
                    />
                )}

                <CardNavigationBtn
                    type="icon"
                    image={<Track_map height={25} width={25} />}
                    title={t('Geo Tracking')}
                    onPressButton={() => props.navigation.navigate('GeoTracking')} />

                <CardNavigationBtn type="icon"
                    image={<Ionicons name="language" size={25} color={Colors.blue96} />}
                    title={t('Select App Language')}
                    onPressButton={() => props.navigation.navigate('SelectLang')} />

            </View>

            {/* </DrawerContentScrollView> */}
        </View>
    );
};

function mapStateToProps(state) {
    return {
        userInfo: state.auth.userInfo,
        userPermissions: state.auth.userPermissions,
        userLocation: state.auth.userLocation,

    };
}
export default connect(mapStateToProps, {
    log_out,
    getUserInfo,
    getUserLocation
})(MenuScreen);
