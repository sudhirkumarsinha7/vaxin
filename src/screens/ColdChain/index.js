/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';
import Empty_Card from '../../components/Empty_Card';

import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import fonts from '../../../FontFamily';
import { formatDateDDMMYYYY } from '../../Util/utils';
import { TextLineComponent } from '../VAR/addVarHelper';
import { CustomButton } from '../../components/Common/helper';
import { getColdchainList, getEquipmentList } from '../../redux/action/coldchain';

export const ColdChainHeader = props => {
    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#F4F4F4',
                justifyContent: 'center',
                padding: 10,
                marginTop: 10,
            }}>
            <View style={{ flex: 0.37, justifyContent: 'center', padding: 2 }}>
                <Text
                    style={{ fontFamily: fonts.MEDIUM, fontSize: 13, color: '#20232B' }}>
                    {'Total Capacity'}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13, marginTop: 10 }}>
                    {'2317'}
                </Text>
            </View>
            <View style={{ flex: 0.37, justifyContent: 'center', padding: 2 }}>
                <Text
                    style={{ fontFamily: fonts.MEDIUM, fontSize: 13, color: '#20232B' }}>
                    {'Present Capacity'}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13, marginTop: 10 }}>
                    {'2217'}
                </Text>
            </View>
            <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                <Text
                    style={{ fontFamily: fonts.MEDIUM, fontSize: 13, color: '#20232B' }}>
                    {'Gap'}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13, marginTop: 10 }}>
                    {'10'}
                </Text>
            </View>
        </View>
    );
};
const COLDChainScreen = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [userToken, setToken] = useState(true);
    const insets = useSafeAreaInsets();
    const [isAdd, setISAdd] = React.useState(true);

    useEffect(() => {
        async function fetchDeta() {
            getData();
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const getData = async () => {
        await props.getColdchainList();
        await props.getEquipmentList();
    };

    const viewMore = async item => {
        props.navigation.navigate('view_coldchain', { details: item });
    };
    const navigateHistoricalData = async item => {
        props.navigation.navigate('HistoricalData', { details: item });
    };
    const eachColdCahin = item => {
        const { location = {}, manufacturerDetails = {} } = item;
        return (
            <View style={{ backgroundColor: '#FCF4F3', padding: 15, marginTop: 15 }}>
                <TextLineComponent
                    leftText={t('Registration Date')}
                    rightText={formatDateDDMMYYYY(item.registrationDate)}
                />
                <TextLineComponent leftText={t('Location')} rightText={location?.name} />
                <TextLineComponent
                    leftText={t('Serial Number')}
                    rightText={item.serialNumber}
                />
                <TextLineComponent
                    leftText={t('Equipment Type (Model)')}
                    rightText={item.type + '(' + item.model + ')'}
                />
                <TextLineComponent
                    leftText={t('Manufacturer')}
                    rightText={manufacturerDetails?.name}
                />
                <TextLineComponent
                    leftText={t('PQS Status')}
                    rightText={item?.pqsStatus}
                />
                <TextLineComponent
                    leftText={t('Year of Installation')}
                    rightText={item?.yearOfInstallation}
                />
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <View />
                    <CustomButton
                        bgColor={'#208196'}
                        textColor={'#fff'}
                        buttonName={t('Historical Data')}
                        navigation={props.navigation}
                        onPressButton={() => navigateHistoricalData(item)}
                    />
                    <CustomButton
                        bgColor={'#CFE7DE'}
                        textColor={'#208196'}
                        buttonName={t('View More')}
                        navigation={props.navigation}
                        onPressButton={() => viewMore(item)}
                    />
                </View>
            </View>
        );
    };
    const { coldcahin_list = [], assets_list = [], userPermissions = {} } = props;
    // console.log('assets_list ' + JSON.stringify(assets_list))

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Cold Chain Monitoring')}
            />
            {/* <ColdChainHeader /> */}
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 16,
                    marginTop: 10,
                    textAlign: 'center',
                }}>
                {t('Asset Information')}
            </Text>

            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={assets_list}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => eachColdCahin(item, index)}
                ListEmptyComponent={<Empty_Card Text="No items available" />}
            />

            <View
                style={{
                    alignItems: 'flex-end',
                    justifyContent: 'flex-end',
                    marginRight: scale(15),
                }}>
                <View
                    style={{
                        position: 'absolute',
                        bottom: 10,
                    }}>
                    {/* {isAdd ? <TouchableOpacity onPress={() => setISAdd(false)} style={{ backgroundColor: Colors.whiteFF, borderRadius: scale(42), }}>

                        <AntDesign name='pluscircle' size={40} color={'#16B0D2'} />
                    </TouchableOpacity> : <TouchableOpacity onPress={() => setISAdd(true)} style={{ backgroundColor: '#208196', borderRadius: 12, padding: 12 }}>
                        <TouchableOpacity onPress={() => { setISAdd(true); props.navigation.navigate('create_coldchain') }}
                            style={{ flexDirection: 'row', padding: scale(8), justifyContent: 'center', backgroundColor: '#0B6579', borderRadius: scale(16), }}>
                            <Text style={{ color: '#fff', fontFamily: fonts.MEDIUM, marginRight: 10, fontSize: 14 }}>{'Add'}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { setISAdd(true); props.navigation.navigate('Maintance') }}
                            style={{ flexDirection: 'row', marginTop: scale(7), padding: scale(8), justifyContent: 'center', backgroundColor: '#0B6579', borderRadius: scale(16), }}>
                            <Text style={{ color: '#fff', fontFamily: fonts.MEDIUM, marginRight: 10, fontSize: 14 }}>{'Maintance'}</Text>

                        </TouchableOpacity>

                    </TouchableOpacity>} */}
                    <TouchableOpacity
                        onPress={() => {
                            setISAdd(true);
                            props.navigation.navigate('Maintance');
                        }}
                        style={{
                            flexDirection: 'row',
                            marginTop: scale(7),
                            padding: scale(8),
                            justifyContent: 'center',
                            backgroundColor: '#0B6579',
                            borderRadius: scale(16),
                        }}>
                        <Text
                            style={{
                                color: '#fff',
                                fontFamily: fonts.MEDIUM,
                                marginRight: 10,
                                fontSize: 14,
                            }}>
                            {t('Maintance')}
                        </Text>

                        {/* <Entypo name='swap' size={18} color={'#fff'} /> */}
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        airport_list: state.Var.airport_list,
        userLocation: state.auth.userLocation,
        coldcahin_list: state.coldchain.coldcahin_list,
        assets_list: state.coldchain.assets_list,
        userPermissions: state.auth.userPermissions,
    };
}
export default connect(mapStateToProps, {
    getColdchainList,
    getEquipmentList,
})(COLDChainScreen);
