/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */


import React, { useState, useEffect } from 'react';
import {

    View,
    ScrollView,
    Text,
    Platform,
    StyleSheet,
    Keyboard,
    TouchableOpacity,
    Image,
    PermissionsAndroid,
    KeyboardAvoidingView,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import { Colors, DeviceWidth, DeviceHeight } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../../config/global';
import fonts from '../../../../FontFamily';
import DropDown from '../../../components/Dropdown';
import { InputField, CustomButton } from '../../../components/Common/helper';
import { selectPhotoFromGallary, takePhotoFromCamera } from '../../../components/ImageHelper';
import { ImageUploadPopUp, PopupMessage } from '../../../components/popUp';
import { DatePickerComponent } from '../../../components/DatePicker';

import { getUserInfo, getUserLocationList } from '../../../redux/action/auth';
import { getCountryList } from '../../../redux/action/var';
import { create_Coldchain, getEquipmentList } from '../../../redux/action/coldchain';

import { ToastShow } from '../../../components/Toast';

const CreateColdCahin = (props) => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isMandatory, setIsMandatory] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [isPopupVisible, setPopupVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('');

    // const actionSheetRef = createRef();
    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        // await props.getCountryList();
        await props.getUserInfo();
        await props.getUserLocationList();
        await props.getEquipmentList();

    };
    const [visible, setVisible] = useState(false);
    const [healthUnitStaffSignVisible, sethealthUnitStaffVisible] = useState(false);
    const [repairedStaffSignVisible, setRepairedVisible] = useState(false);

    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);

    const showhelthModal = () => sethealthUnitStaffVisible(true);
    const hidehelthModal = () => sethealthUnitStaffVisible(false);

    const showRepaireModal = () => setRepairedVisible(true);
    const hideRepaireModal = () => setRepairedVisible(false);

    const handleChoosePhotoRepaire = async () => {
        selectPhotoFromGallary(setRepaireStaffSign, hideRepaireModal, onChangeRepaireSign);
    };
    const handleChoosePhoto = async () => {
        selectPhotoFromGallary(setdistStaffSign, hideModal, onChangedistStaffSign);
    };

    const handleHealthChoosePhoto = async () => {
        selectPhotoFromGallary(sethealthUnitStaffSign, hidehelthModal, onChangehealthUnitStaffSign);
    };
    const onChangeRepaireSign = (val) => {
        coldChainForm.handleChange({ target: { name: 'repairedStaffSignBase64', value: val } });
    };
    const onChangedistStaffSign = (val) => {
        coldChainForm.handleChange({ target: { name: 'distStaffSignBase64', value: val } });
    };
    const onChangehealthUnitStaffSign = (val) => {
        coldChainForm.handleChange({ target: { name: 'healthUnitStaffSignBase64', value: val } });
    };
    const setRepaireStaffSign = (val) => {
        coldChainForm.handleChange({ target: { name: 'repairedStaffSign', value: val } });
    };
    const setdistStaffSign = (val) => {
        coldChainForm.handleChange({ target: { name: 'distStaffSign', value: val } });
    };
    const sethealthUnitStaffSign = (val) => {
        coldChainForm.handleChange({ target: { name: 'healthUnitStaffSign', value: val } });
    };

    const requestCameraPermission = async (val) => {
        if (Platform.OS === 'ios') {
            if (val === 'distStaffSign') {
                handleCameraPhoto();

            } else if (val === 'repairedStaffSign') {
                handleCameraPhotoRepaire();
            } else {
                handleHealthUnitPhoto();
            }
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'App Camera Permission',
                        message: 'App needs access to your camera ',
                        buttonNeutral: 'Ask Me Later',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    // console.log('Camera permission given');
                    if (val === 'distStaffSign') {
                        handleCameraPhoto();

                    } else if (val === 'repairedStaffSign') {
                        handleCameraPhotoRepaire();
                    } else {
                        handleHealthUnitPhoto();
                    }
                } else {
                    // console.log('Camera permission denied');
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };
    const handleCameraPhotoRepaire = async () => {
        takePhotoFromCamera(setRepaireStaffSign, hideRepaireModal, onChangeRepaireSign);

    };
    const handleCameraPhoto = async () => {
        takePhotoFromCamera(setdistStaffSign, hideModal, onChangedistStaffSign);
    };
    const handleHealthUnitPhoto = async () => {
        takePhotoFromCamera(sethealthUnitStaffSign, hidehelthModal, onChangehealthUnitStaffSign);
    };

    var validationSchema = Yup.object().shape(
        {

            assetId: Yup.string().required(t('Required')),
            locationId: Yup.string().required(t('Required')),
            fault: Yup.string().required(t('Required')),
            summary: Yup.string().required(t('Required')),
            comment: Yup.string().required(t('Required')),
            repairedByName: Yup.string().required(t('Required')),
            repairedDesignation: Yup.string().required(t('Required')),
            // repairedStaffSign: Yup.string().required(t('Required')),

            staffName: Yup.string().required(t('Required')),
            // distStaffSign: Yup.string().required(t('Required')),
            healthUnitStaffName: Yup.string().required(t('Required')),
            healthUnitdesignation: Yup.string().required(t('Required')),
            // healthUnitStaffSign: Yup.string().required(t('Required')),
            status: Yup.string().required(t('Required')),

        },
        [],
    ); // <-- HERE!!!!!!!!
    const coldChainForm = useFormik({
        initialValues: {
            date: new Date(),
            assetId: '',
            locationId: '',
            model: '',
            healthCenterName: '',
            serialNumber: '',
            fault: '',
            spareparts: '',
            otherComment: '',
            summary: '',
            comment: '',
            repairedByName: '',
            repairedDesignation: '',
            distStaffdesignation: '',
            distStaffSign: '',
            repairedStaffSign: '',
            distStaffSignDate: '',
            staffName: '',
            staffDesignation: '',
            healthUnitStaffName: '',
            healthUnitdesignation: '',
            healthUnitStaffSign: '',
            healthUnitStaffSignDate: '',
            status: '',



        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _addColdChain = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        coldChainForm.handleSubmit();
    };

    const handleSubmit = async values => {
        // console.log('handleSubmit values', values)
        let body = {
            locationId: values.locationId,
            assetId: values.assetId,
            date: values.date,
            fault: values.fault,
            spareparts: [values.spareparts],
            comments: [values.comment],
            summary: values.summary,
            status: values.status,
            repairedBy: {
                name: values.repairedByName,
                designation: values.repairedDesignation,
                signature: values.repairedStaffSignBase64 || 'key',
            },
            staff: {
                name: values.staffName,
                designation: values.staffDesignation,
                signature: values.distStaffSignBase64 || 'key',
            },
            healthUnitStaff: {
                name: values.healthUnitStaffName,
                designation: values.healthUnitdesignation,
                signature: values.healthUnitStaffSignBase64 || 'key',
            },
        };
        // console.log('create_Coldchain values', body)
        const result = await props.create_Coldchain(body);
        setLocalLoader(false);
        // console.log('create_Var result?.status' + JSON.stringify(result?.status));

        // console.log('create_Var res result' + JSON.stringify(result));
        if (result?.status === 200) {
            // const orderID = result.data && result.data.data && result.data.data.id || ''
            const msg = 'Cold chain Updated ';
            setSuccessMsg(msg);

            setPopupVisible(true);

        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(
                t(err),
                'error',
                'long',
                'top',
            );
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(
                t(err),
                'error',
                'long',
                'top',
            );
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(
                t((err?.message)),
                'error',
                'long',
                'top',
            );
        }


    };




    const changeAfterSelectEquipent = async (item) => {
        const { value, eachItem } = item;

        coldChainForm.handleChange({ target: { name: 'assetId', value: value } });
        coldChainForm.handleChange({ target: { name: 'model', value: eachItem.model } });
        coldChainForm.handleChange({ target: { name: 'serialNumber', value: eachItem.serialNumber } });
    };
    const changeAfterSelectLocation = async (item) => {
        const { value } = item;
        coldChainForm.handleChange({ target: { name: 'locationId', value: value } });

    };

    const changeAfterSelectReportedFault = async (item) => {
        const { value } = item;
        coldChainForm.handleChange({ target: { name: 'fault', value: value } });

    };
    const changeAfterSelectSpareParts = async (item) => {
        const { value } = item;
        coldChainForm.handleChange({ target: { name: 'spareparts', value: value } });

    };


    const changeAfterSelectStatus = async (item) => {
        const { value } = item;
        coldChainForm.handleChange({ target: { name: 'status', value: value } });

    };
    const closPopUp = () => {
        setPopupVisible(false);
        props.navigation.navigate('Maintance');

    };
    const onChangeReportDate = val => {
        coldChainForm.handleChange({ target: { name: 'date', value: val } });
    };
    const { userLocationList = {}, assets_list = [] } = props;
    const { locations = [] } = userLocationList;
    // console.log('assets_list ' + JSON.stringify(assets_list))
    return (
        <View style={styles.container}>
            <HeaderWithBack navigation={props.navigation}
                name={t('Add Maintenance Job Card')}
            />
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={{ paddingHorizontal: 15, flex: 1 }}
            >
                <ScrollView>
                    <PopupMessage
                        message={successMsg}
                        title={t('Maintenance Job Card created Successfully')}
                        isVisible={isPopupVisible}
                        onClose={() => closPopUp()}
                        buttonName={t('Okay')}
                        image={localImage.congrats}
                    />
                    <DatePickerComponent
                        label={t('Date')}
                        placeholder="DD/MM/YYYY"
                        onChange={(val) => onChangeReportDate(val)}
                        val={coldChainForm?.values?.date}
                        errorMsg={coldChainForm?.errors?.date}
                        mandatory={isMandatory}
                    />
                    <DropDown
                        dropdownData={locations}
                        onChangeValue={changeAfterSelectLocation}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.locationId}
                        placeholderText={t('Location')}
                        labelText={t('Location')}
                        errorMsg={coldChainForm?.errors?.locationId}
                        mandatory={isMandatory}
                        search={true}

                    />

                    <DropDown
                        dropdownData={assets_list}
                        onChangeValue={changeAfterSelectEquipent}
                        label="type"
                        mapKey="_id"
                        val={coldChainForm?.values?.assetId}
                        placeholderText={t('Equipment assetId')}
                        labelText={t('Equipment assetId')}
                        errorMsg={coldChainForm?.errors?.assetId}
                        mandatory={isMandatory}
                        search={true}


                    />




                    <InputField
                        placeholder={t('Model')}
                        label={t('Model')}
                        inputValue={coldChainForm?.values?.model}
                        setInputValue={coldChainForm.handleChange('model')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.model}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Name of Health Facility')}
                        label={t('Name of Health Facility')}
                        inputValue={coldChainForm?.values?.healthCenterName}
                        setInputValue={coldChainForm.handleChange('healthCenterName')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.healthCenterName}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Serial Number')}
                        label={t('Serial Number')}
                        inputValue={coldChainForm?.values?.serialNumber}
                        setInputValue={coldChainForm.handleChange('serialNumber')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.serialNumber}
                        mandatory={isMandatory}
                    />

                    <DropDown
                        dropdownData={['AWAITING REPAIR', 'LACK OF SPARE PARTS', 'LACK OF POWER']}
                        onChangeValue={changeAfterSelectReportedFault}
                        // label='name'
                        // mapKey='_id'
                        val={coldChainForm?.values?.fault}
                        placeholderText={t('Reported fault')}
                        labelText={t('Reported fault')}
                        errorMsg={coldChainForm?.errors?.fault}
                        mandatory={isMandatory}
                        search={true}

                    />
                    <DropDown
                        dropdownData={['WORKING', 'INTACT', 'REPAIRABLE', 'NOT_REPAIRABLE']}
                        onChangeValue={changeAfterSelectSpareParts}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.spareparts}
                        placeholderText={t('Spare parts used')}
                        labelText={t('Spare parts used')}
                        errorMsg={coldChainForm?.errors?.spareparts}
                        mandatory={isMandatory}
                        search={true}

                    />

                    {/* <InputField
                        placeholder="Other comments"
                        label="Other comments"
                        inputValue={coldChainForm?.values?.otherComment}
                        setInputValue={coldChainForm.handleChange('otherComment')}
                        inputStyle={{ marginBottom: 10, height: 100 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.otherComment}
                        mandatory={isMandatory}
                    /> */}
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Summary of Maintenance Services Completed')}
                        inputValue={coldChainForm?.values?.summary}
                        setInputValue={coldChainForm.handleChange('summary')}
                        inputStyle={{ marginBottom: 10, height: 100 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.summary}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Comments')}
                        inputValue={coldChainForm?.values?.comment}
                        setInputValue={coldChainForm.handleChange('comment')}
                        inputStyle={{ marginBottom: 1, height: 100 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.comment}
                        mandatory={isMandatory}
                    />

                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Repaired/Installed by')}
                        inputValue={coldChainForm?.values?.repairedByName}
                        setInputValue={coldChainForm.handleChange('repairedByName')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.repairedByName}
                        mandatory={isMandatory}
                    />

                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Repaired/Installed Designation')}
                        inputValue={coldChainForm?.values?.repairedDesignation}
                        setInputValue={coldChainForm.handleChange('repairedDesignation')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.repairedDesignation}
                        mandatory={isMandatory}
                    />
                    {/* <View style={{ borderWidth: 0.5, height: DeviceHeight / 10, borderRadius: 10, justifyContent: 'center' }}>

                        {coldChainForm?.values?.repairedStaffSign ?
                            <TouchableOpacity onPress={() => showRepaireModal()}>
                                <Image source={{ uri: coldChainForm?.values?.repairedStaffSign }} style={{ height: DeviceHeight / 10, width: DeviceWidth / 1.07, justifyContent: 'center', borderRadius: 10 }} />
                            </TouchableOpacity>
                            : <TouchableOpacity onPress={() => showRepaireModal()}>
                                <Text style={{ textAlign: 'center', fontWeight: 14, color: Colors.green4e }}>Upload Sign</Text>

                            </TouchableOpacity>}

                    </View>
                    {isMandatory && coldChainForm?.errors?.repairedStaffSign
                        && <Text
                            style={{
                                color: 'red',
                                fontSize: 12,
                                marginTop: 7,
                                fontFamily: fonts.MEDIUM,
                            }}>{coldChainForm?.errors?.repairedStaffSign}</Text>
                    }
                    <View style={{ marginTop: 10 }} /> */}
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Staff Name')}
                        inputValue={coldChainForm?.values?.staffName}
                        setInputValue={coldChainForm.handleChange('staffName')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.staffName}
                        mandatory={isMandatory}
                    />

                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Staff Designation')}
                        inputValue={coldChainForm?.values?.staffDesignation}
                        setInputValue={coldChainForm.handleChange('staffDesignation')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.staffDesignation}
                        mandatory={isMandatory}
                    />
                    {/* <View style={{ borderWidth: 0.5, height: DeviceHeight / 10, borderRadius: 10, justifyContent: 'center' }}>

                        {coldChainForm?.values?.distStaffSign ?
                            <TouchableOpacity onPress={() => showModal()}>
                                <Image source={{ uri: coldChainForm?.values?.distStaffSign }} style={{ height: DeviceHeight / 10, width: DeviceWidth / 1.07, justifyContent: 'center', borderRadius: 10 }} />
                            </TouchableOpacity>
                            : <TouchableOpacity onPress={() => showModal()}>
                                <Text style={{ textAlign: 'center', fontWeight: 14, color: Colors.green4e }}>Upload Sign</Text>

                            </TouchableOpacity>}

                    </View>
                    {isMandatory && coldChainForm?.errors?.distStaffSign
                        && <Text
                            style={{
                                color: 'red',
                                fontSize: 12,
                                marginTop: 7,
                                fontFamily: fonts.MEDIUM,
                            }}>{coldChainForm?.errors?.distStaffSign}</Text>
                    }
                    <View style={{ marginTop: 10 }} /> */}

                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Health Unit Staff Name')}
                        inputValue={coldChainForm?.values?.healthUnitStaffName}
                        setInputValue={coldChainForm.handleChange('healthUnitStaffName')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.healthUnitStaffName}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Staff Designation')}
                        inputValue={coldChainForm?.values?.healthUnitdesignation}
                        setInputValue={coldChainForm.handleChange('healthUnitdesignation')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.healthUnitdesignation}
                        mandatory={isMandatory}
                    />
                    {/* <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectHealthStaff}
                        label='name'
                        mapKey='_id'
                        val={coldChainForm?.values?.healthUnitStaffSignDate}
                        placeholderText="Staff Designation"
                        labelText='Staff Designation'
                        errorMsg={coldChainForm?.errors?.healthUnitStaffSignDate}
                        mandatory={isMandatory}
                        search={true}

                    /> */}


                    {/* <View style={{ borderWidth: 0.5, height: DeviceHeight / 10, borderRadius: 10, justifyContent: 'center' }}>

                        {coldChainForm?.values?.healthUnitStaffSign ?
                            <TouchableOpacity onPress={() => showhelthModal()}>
                                <Image source={{ uri: coldChainForm?.values?.healthUnitStaffSign }} style={{ height: DeviceHeight / 10, width: DeviceWidth / 1.07, justifyContent: 'center', borderRadius: 10 }} />
                            </TouchableOpacity>
                            : <TouchableOpacity onPress={() => showhelthModal()}>
                                <Text style={{ textAlign: 'center', fontWeight: 14, color: Colors.green4e }}>Upload Sign</Text>

                            </TouchableOpacity>}

                    </View>
                    {isMandatory && coldChainForm?.errors?.healthUnitStaffSign
                        && <Text
                            style={{
                                color: 'red',
                                fontSize: 12,
                                marginTop: 7,
                                fontFamily: fonts.MEDIUM,
                            }}>{coldChainForm?.errors?.healthUnitStaffSign}</Text>
                    } */}

                    <DropDown
                        dropdownData={['PENDING', 'COMPLETED']}
                        onChangeValue={changeAfterSelectStatus}
                        // label='name'
                        // mapKey='_id'
                        val={coldChainForm?.values?.status}
                        placeholderText={t('Status')}
                        labelText={t('Status')}
                        errorMsg={coldChainForm?.errors?.status}
                        mandatory={isMandatory}
                        search={true}


                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View />
                        <View style={{ justifyContent: 'flex-end' }}>
                            <CustomButton
                                bgColor={'#208196'}
                                buttonName={t('Save')}
                                navigation={props.navigation}
                                onPressButton={_addColdChain}
                            />
                        </View>
                    </View>

                    <ImageUploadPopUp
                        isVisible={repairedStaffSignVisible}
                        onClose={hideRepaireModal}
                        handleChoosePhoto={handleChoosePhotoRepaire}
                        requestCameraPermission={() => requestCameraPermission('repairedStaffSign')}
                    />

                    <ImageUploadPopUp
                        isVisible={visible}
                        onClose={hideModal}
                        handleChoosePhoto={handleChoosePhoto}
                        requestCameraPermission={() => requestCameraPermission('distStaffSign')}
                    />
                    <ImageUploadPopUp

                        isVisible={healthUnitStaffSignVisible}
                        onClose={hidehelthModal}
                        handleChoosePhoto={handleHealthChoosePhoto}
                        requestCameraPermission={() => requestCameraPermission('healthUnitStaffSign')}
                    />
                </ScrollView>
            </KeyboardAvoidingView>
        </View >
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },

});

function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        inv_product_list: state.inventory.inv_product_list,
        userLocationList: state.auth.userLocationList,
        assets_list: state.coldchain.assets_list,
    };
}
export default connect(mapStateToProps, {
    getCountryList,
    getUserLocationList,
    getUserInfo,
    create_Coldchain,
    getEquipmentList,
},
)(CreateColdCahin);
