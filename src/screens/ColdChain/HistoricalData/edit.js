/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    ScrollView,
    Text,
    StyleSheet,
    Keyboard,
    TouchableOpacity,
    Image,
    PermissionsAndroid,
    Platform,
    KeyboardAvoidingView,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import {
    Colors,
    DeviceWidth,
    DeviceHeight,
} from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useIsFocused } from '@react-navigation/native';
import fonts from '../../../../FontFamily';
import DropDown from '../../../components/Dropdown';
import {
    InputField,
    CustomButton,
} from '../../../components/Common/helper';
import {
    selectPhotoFromGallary,
    takePhotoFromCamera,
} from '../../../components/ImageHelper';
import { ImageUploadPopUp } from '../../../components/popUp';
import {
    DatePickerComponent,
} from '../../../components/DatePicker';

import { getUserInfo, getUserLocationList } from '../../../redux/action/auth';
import { getCountryList } from '../../../redux/action/var';


const CreateColdCahin = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isMandatory, setIsMandatory] = useState(false);

    // const actionSheetRef = createRef();
    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        await props.getCountryList();
        await props.getUserInfo();
        await props.getUserLocationList();
    };
    const [visible, setVisible] = useState(false);
    const [healthUnitStaffSignVisible, setIpiSignVisible] = useState(false);

    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    const hideEpiModal = () => setIpiSignVisible(false);

    const handleChoosePhoto = async () => {
        selectPhotoFromGallary(setdistStaffSign, hideModal, onChangedistStaffSign);
    };

    const handleEPIChoosePhoto = async () => {
        selectPhotoFromGallary(
            sethealthUnitStaffSign,
            hideEpiModal,
            onChangehealthUnitStaffSign,
        );
    };
    const onChangedistStaffSign = val => {
        coldChainForm.handleChange({
            target: { name: 'distStaffSignBase64', value: val },
        });
    };
    const onChangehealthUnitStaffSign = val => {
        coldChainForm.handleChange({
            target: { name: 'healthUnitStaffSignBase64', value: val },
        });
    };
    const setdistStaffSign = val => {
        coldChainForm.handleChange({ target: { name: 'distStaffSign', value: val } });
    };
    const sethealthUnitStaffSign = val => {
        coldChainForm.handleChange({
            target: { name: 'healthUnitStaffSign', value: val },
        });
    };

    const requestCameraPermission = async val => {
        if (Platform.OS == 'ios') {
            if (val === 'distStaffSign') {
                handleCameraPhoto();
            } else {
                handleEPICameraPhoto();
            }
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'App Camera Permission',
                        message: 'App needs access to your camera ',
                        buttonNeutral: 'Ask Me Later',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    // console.log('Camera permission given');
                    if (val === 'distStaffSign') {
                        handleCameraPhoto();
                    } else {
                        handleEPICameraPhoto();
                    }
                } else {
                    // console.log('Camera permission denied');
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };

    const handleCameraPhoto = async () => {
        takePhotoFromCamera(setdistStaffSign, hideModal, onChangedistStaffSign);
    };
    const handleEPICameraPhoto = async () => {
        takePhotoFromCamera(
            sethealthUnitStaffSign,
            hideEpiModal,
            onChangehealthUnitStaffSign,
        );
    };

    var validationSchema = Yup.object().shape(
        {
            countryId: Yup.string().required(t('Required')),
            equipmentType: Yup.string().required(t('Required')),
        },
        [],
    ); // <-- HERE!!!!!!!!
    const coldChainForm = useFormik({
        initialValues: {
            country: '',
            countryId: '',
            equipmentType: '',
            district: '',
            model: '',
            healthcenter: '',
            serialNumber: '',
            repotedFault: '',
            spareparts: '',
            otherComment: '',
            maintenanceSummary: '',
            comment: '',
            repairby: '',
            disStaffName: '',
            distStaffdesignation: '',
            distStaffSign: '',
            distStaffSignDate: '',

            healthUnitStaffName: '',
            healthUnitdesignation: '',
            healthUnitStaffSign: '',
            healthUnitStaffSignDate: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _addColdChain = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        coldChainForm.handleSubmit();
    };

    const handleSubmit = async values => {
        console.log('handleSubmit values', values);
    };

    const changeAfterSelectCountry = async item => {
        const { value, label } = item;
        coldChainForm.handleChange({ target: { name: 'country', value: label } });
        coldChainForm.handleChange({ target: { name: 'countryId', value: value } });
        // await props.getAirports(value)
    };
    const changeAfterSelectEquipent = async item => {
        const { value } = item;
        coldChainForm.handleChange({ target: { name: 'equipmentType', value: value } });
    };
    const changeAfterSelectDist = async item => {
        const { value } = item;
        coldChainForm.handleChange({ target: { name: 'district', value: value } });
    };

    const changeAfterSelectReportedFault = async item => {
        const { value } = item;
        coldChainForm.handleChange({ target: { name: 'repotedFault', value: value } });
    };
    const changeAfterSelectSpareParts = async item => {
        const { value } = item;
        coldChainForm.handleChange({ target: { name: 'spareparts', value: value } });
    };

    const changeAfterSelectDistStaff = async item => {
        const { value } = item;
        coldChainForm.handleChange({
            target: { name: 'distStaffdesignation', value: value },
        });
    };
    const changeAfterSelectHealthStaff = async item => {
        const { value } = item;
        coldChainForm.handleChange({
            target: { name: 'healthUnitdesignation', value: value },
        });
    };
    const onChangeReportDate = val => {
        coldChainForm.handleChange({ target: { name: 'date', value: val } });
    };
    const { country_list = [] } = props;
    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Historical Data Form')}
            />
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
                style={{ paddingHorizontal: 15, flex: 1 }}>
                <ScrollView>
                    <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectCountry}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.countryId}
                        placeholderText="Country "
                        labelText="Country "
                        errorMsg={coldChainForm?.errors?.countryId}
                        mandatory={isMandatory}
                        search={true}
                    />
                    <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectEquipent}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.equipmentType}
                        placeholderText="Equipment Type"
                        labelText="Equipment Type"
                        errorMsg={coldChainForm?.errors?.equipmentType}
                        mandatory={isMandatory}
                        search={true}
                    />

                    <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectDist}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.district}
                        placeholderText="Subcountry/district"
                        labelText="Subcountry/district"
                        errorMsg={coldChainForm?.errors?.district}
                        mandatory={isMandatory}
                        search={true}
                    />

                    <InputField
                        placeholder="Model"
                        label="Model"
                        inputValue={coldChainForm?.values?.model}
                        setInputValue={coldChainForm.handleChange('model')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.model}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder="Name of Health Facility"
                        label="Name of Health Facility"
                        inputValue={coldChainForm?.values?.healthcenter}
                        setInputValue={coldChainForm.handleChange('healthcenter')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.healthcenter}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder="Serial Number"
                        label="Serial Number"
                        inputValue={coldChainForm?.values?.serialNumber}
                        setInputValue={coldChainForm.handleChange('serialNumber')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.serialNumber}
                        mandatory={isMandatory}
                    />

                    <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectReportedFault}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.repotedFault}
                        placeholderText="Reported fault"
                        labelText="Reported fault"
                        errorMsg={coldChainForm?.errors?.repotedFault}
                        mandatory={isMandatory}
                        search={true}
                    />
                    <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectSpareParts}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.spareparts}
                        placeholderText="Spare parts used"
                        labelText="Spare parts used"
                        errorMsg={coldChainForm?.errors?.spareparts}
                        mandatory={isMandatory}
                        search={true}
                    />

                    <InputField
                        placeholder="Other comments"
                        label="Other comments"
                        inputValue={coldChainForm?.values?.otherComment}
                        setInputValue={coldChainForm.handleChange('otherComment')}
                        inputStyle={{ marginBottom: 10, height: 100 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.otherComment}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder="Enter here"
                        label="Summary of Maintenance Services Completed"
                        inputValue={coldChainForm?.values?.maintenanceSummary}
                        setInputValue={coldChainForm.handleChange('maintenanceSummary')}
                        inputStyle={{ marginBottom: 10, height: 100 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.maintenanceSummary}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder="Enter here"
                        label="Comments"
                        inputValue={coldChainForm?.values?.comment}
                        setInputValue={coldChainForm.handleChange('comment')}
                        inputStyle={{ marginBottom: 1, height: 100 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.comment}
                        mandatory={isMandatory}
                    />

                    <InputField
                        placeholder="Enter here"
                        label="Repaired/Installed by"
                        inputValue={coldChainForm?.values?.repairby}
                        setInputValue={coldChainForm.handleChange('repairby')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.repairby}
                        mandatory={isMandatory}
                    />

                    <InputField
                        placeholder="Enter here"
                        label="Distt. Staff Name"
                        inputValue={coldChainForm?.values?.disStaffName}
                        setInputValue={coldChainForm.handleChange('disStaffName')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.disStaffName}
                        mandatory={isMandatory}
                    />
                    <DatePickerComponent
                        label={'Date'}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeReportDate(val)}
                        val={coldChainForm?.values?.reportDate}
                        errorMsg={coldChainForm?.errors?.reportDate}
                        mandatory={isMandatory}
                    />
                    <View
                        style={{
                            borderWidth: 0.5,
                            height: DeviceHeight / 10,
                            borderRadius: 10,
                            justifyContent: 'center',
                        }}>
                        {coldChainForm?.values?.distStaffSign ? (
                            <TouchableOpacity onPress={() => showModal()}>
                                <Image
                                    source={{ uri: coldChainForm?.values?.distStaffSign }}
                                    style={{
                                        height: DeviceHeight / 10,
                                        width: DeviceWidth / 1.07,
                                        justifyContent: 'center',
                                        borderRadius: 10,
                                    }}
                                />
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity onPress={() => showModal()}>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontWeight: 14,
                                        color: Colors.green4e,
                                    }}>
                                    Upload Sign
                                </Text>
                            </TouchableOpacity>
                        )}
                    </View>
                    {isMandatory && coldChainForm?.errors?.distStaffSign && (
                        <Text
                            style={{
                                color: 'red',
                                fontSize: 12,
                                marginTop: 7,
                                fontFamily: fonts.MEDIUM,
                            }}>
                            {coldChainForm?.errors?.distStaffSign}
                        </Text>
                    )}
                    <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectDistStaff}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.distStaffdesignation}
                        placeholderText="Staff Designation"
                        labelText="Staff Designation"
                        errorMsg={coldChainForm?.errors?.distStaffdesignation}
                        mandatory={isMandatory}
                        search={true}
                    />

                    <InputField
                        placeholder="Enter here"
                        label="Health Unit Staff Name"
                        inputValue={coldChainForm?.values?.healthUnitStaffName}
                        setInputValue={coldChainForm.handleChange('healthUnitStaffName')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={coldChainForm?.errors?.healthUnitStaffName}
                        mandatory={isMandatory}
                    />
                    <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectHealthStaff}
                        label="name"
                        mapKey="_id"
                        val={coldChainForm?.values?.healthUnitStaffSignDate}
                        placeholderText="Staff Designation"
                        labelText="Staff Designation"
                        errorMsg={coldChainForm?.errors?.healthUnitStaffSignDate}
                        mandatory={isMandatory}
                        search={true}
                    />
                    <DatePickerComponent
                        label={'Date'}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeReportDate(val)}
                        val={coldChainForm?.values?.reportDate}
                        errorMsg={coldChainForm?.errors?.reportDate}
                        mandatory={isMandatory}
                    />

                    <View
                        style={{
                            borderWidth: 0.5,
                            height: DeviceHeight / 10,
                            borderRadius: 10,
                            justifyContent: 'center',
                        }}>
                        {coldChainForm?.values?.distStaffSign ? (
                            <TouchableOpacity onPress={() => showModal()}>
                                <Image
                                    source={{ uri: coldChainForm?.values?.distStaffSign }}
                                    style={{
                                        height: DeviceHeight / 10,
                                        width: DeviceWidth / 1.07,
                                        justifyContent: 'center',
                                        borderRadius: 10,
                                    }}
                                />
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity onPress={() => showModal()}>
                                <Text
                                    style={{
                                        textAlign: 'center',
                                        fontWeight: 14,
                                        color: Colors.green4e,
                                    }}>
                                    Upload Sign
                                </Text>
                            </TouchableOpacity>
                        )}
                    </View>
                    {isMandatory && coldChainForm?.errors?.distStaffSign && (
                        <Text
                            style={{
                                color: 'red',
                                fontSize: 12,
                                marginTop: 7,
                                fontFamily: fonts.MEDIUM,
                            }}>
                            {coldChainForm?.errors?.distStaffSign}
                        </Text>
                    )}

                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View />
                        <View style={{ justifyContent: 'flex-end' }}>
                            <CustomButton
                                bgColor={'#208196'}
                                buttonName={'Save'}
                                navigation={props.navigation}
                                onPressButton={_addColdChain}
                            />
                        </View>
                    </View>

                    <ImageUploadPopUp
                        isVisible={visible}
                        onClose={hideModal}
                        handleChoosePhoto={handleChoosePhoto}
                        requestCameraPermission={() =>
                            requestCameraPermission('distStaffSign')
                        }
                    />
                    <ImageUploadPopUp
                        isVisible={healthUnitStaffSignVisible}
                        onClose={hideEpiModal}
                        handleChoosePhoto={handleEPIChoosePhoto}
                        requestCameraPermission={() =>
                            requestCameraPermission('healthUnitStaffSign')
                        }
                    />
                </ScrollView>
            </KeyboardAvoidingView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        inv_product_list: state.inventory.inv_product_list,
        userLocation: state.auth.userLocationList,
    };
}
export default connect(mapStateToProps, {
    getCountryList,
    getUserLocationList,
    getUserInfo,
})(CreateColdCahin);
