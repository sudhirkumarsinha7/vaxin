/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import {
    View,
    ScrollView,
    Text,
    StyleSheet,
    FlatList
} from 'react-native';
import { connect } from 'react-redux';
import { TextLineComponent } from '../../VAR/addVarHelper';

import { HeaderWithBack } from '../../../components/Header';
import { useTranslation } from 'react-i18next';
import { useIsFocused } from '@react-navigation/native';
import fonts from '../../../../FontFamily';
import { useRoute } from '@react-navigation/native';
import { formatDateDDMMYYYY } from '../../../Util/utils';
import { getProdList } from '../../../redux/action/inventory';
import { TempListComponent, TemperatureGraph, TextLineComponentVertical } from '../helpler';
import { getTempList } from '../../../redux/action/coldchain';
import Empty_Card from '../../../components/Empty_Card';

const HistoricalData = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [localLoader, setLocalLoader] = useState(false);
    const [temp, setTemp] = useState({});

    const route = useRoute();
    const { params } = route;
    const details = params.details || {};

    // const actionSheetRef = createRef();
    useEffect(() => {
        async function fetchData() {
            // GetData();
            const list = await props.getTempList(details?._id);
            setTemp(list)
        }
        fetchData();
    }, [isFocused]);
    const GetData = async () => { };

    const { locationDetails = {}, assetDetails = {} } = details;
    const { allTemperatures = [], hourlyAggregate = [] } = temp
    // console.log('allTemperatures ' + JSON.stringify(temp))
    // console.log('allTemperatures length ' + JSON.stringify(allTemperatures.length))

    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Historical Data Form')}
            />
            <ScrollView>
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 16,
                        marginTop: 10,
                        textAlign: 'center',
                    }}>
                    {t('Report Information')}
                </Text>

                <View style={{ padding: 15 }}>
                    <TextLineComponent
                        leftText={t('Reporting Period Date')}
                        rightText={formatDateDDMMYYYY(details.date)}
                    />
                    <TextLineComponent leftText={t('Reporting Time')} rightText="12:25 PM" />
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 16,
                            marginVertical: 15,
                            textAlign: 'center',
                        }}>
                        {t('Asset Information')}
                    </Text>

                    <TextLineComponent leftText={t('Facility ID')} rightText="843212345674" />
                    <TextLineComponent leftText={t('Refrigerator ID')} rightText="FD123456" />
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 16,
                            marginTop: 10,
                            textAlign: 'center',
                        }}>
                        {t('Temperature History')}
                    </Text>
                    <TextLineComponent leftText={t('Time Period')} rightText="Temperature" />
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 20 }}
                        data={allTemperatures}
                        renderItem={({ item }) => (
                            <TempListComponent item={item} navigation={props.navigation} />
                        )}
                        ListEmptyComponent={<Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                marginVertical: 15,
                                textAlign: 'center',
                            }}>
                            {t('No data available')}
                        </Text>}

                    />

                    {allTemperatures.length ? <TemperatureGraph data={allTemperatures} /> : null}
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 16,
                            marginVertical: 15,
                            textAlign: 'center',
                        }}>
                        {t('Functional Status')}
                    </Text>
                    <TextLineComponentVertical
                        leftText={t('Heat alarms with over 10 - hour duration above 8 degree Celsius are recorded in the temperature log from the first day of the month to the last day of the month')}
                        rightText="0"
                    />
                    <TextLineComponentVertical
                        leftText={t('Freeze alarms with over 1 - hour duration below - 0.5 degree Celsius are recorded in the temperature log from the first day of the month to the last day of the month')}
                        rightText="0"
                    />
                    <TextLineComponentVertical
                        leftText={t('Heat alarms with 48 - hour or longer duration are recorded in the temperature log from the first day of the month to the last day of the month')}
                        rightText="0"
                    />

                    <TextLineComponentVertical
                        leftText={t('Equipment working status')}
                        rightText="Active"
                    />
                    <TextLineComponentVertical
                        leftText={t('Refrigerator Failure Reason')}
                        rightText="Power cut"
                    />
                    <TextLineComponentVertical
                        leftText={t('Action Taken')}
                        rightText="Action Taken"
                    />
                    <TextLineComponentVertical
                        leftText={t('Parts Replaced')}
                        rightText="Parts Replaced"
                    />

                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        inv_product_list: state.inventory.inv_product_list,
        userLocation: state.auth.userLocation,
        assets_list: state.coldchain.assets_list,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    getTempList
})(HistoricalData);
