/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { useIsFocused } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


import ViewColdCahin from './ViewColdChain';
import AddMaintance from './AddMaintance';
import EditMaintanceJob from './AddMaintance/edit';

import Maintance from './Maintance';
import HistoricalData from './HistoricalData';
// import HistoricalDataEdit from './HistoricalData/edit'

const headerOptions = {
    headerShown: false,
};
import ColdChain from '.';
const Stack = createNativeStackNavigator();
const ColdCahionTab = () => {
    const isFocused = useIsFocused();
    useEffect(() => {
        async function fetchData() { }
        fetchData();
    }, [isFocused]);
    return (
        <Stack.Navigator screenOptions={headerOptions} initialRoute={'coldchain'}>
            <Stack.Screen
                name="coldchain"
                component={ColdChain}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="view_coldchain"
                component={ViewColdCahin}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="add_maimtance"
                component={AddMaintance}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="HistoricalData"
                component={HistoricalData}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Maintance"
                component={Maintance}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="EditMaintanceJob"
                component={EditMaintanceJob}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};
export default ColdCahionTab;
