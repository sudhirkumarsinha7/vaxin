/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';
import Empty_Card from '../../../components/Empty_Card';

import { HeaderWithBack } from '../../../components/Header';
import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useIsFocused } from '@react-navigation/native';
import fonts from '../../../../FontFamily';
import { MaintaceListComponent } from '../helpler';
import {
    getColdchainList,
    getEquipmentList,
} from '../../../redux/action/coldchain';

const MaintaceScreen = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();

    useEffect(() => {
        async function fetchDeta() {
            getData();
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const getData = async () => {
        await props.getColdchainList();
        await props.getEquipmentList();
    };

    const { coldcahin_list = [] } = props;
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack navigation={props.navigation} name={t('Maintance')} />
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 16,
                    marginTop: 10,
                    textAlign: 'center',
                }}>
                {t('Maintenance Job Card')}
            </Text>

            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={coldcahin_list}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                    <MaintaceListComponent item={item} navigation={props.navigation} />
                )}
                ListEmptyComponent={<Empty_Card Text="No items available" />}
            />
            <View
                style={{
                    alignItems: 'flex-end',
                    justifyContent: 'flex-end',
                    marginRight: scale(15),
                }}>
                <View
                    style={{
                        position: 'absolute',
                        bottom: 10,
                    }}>
                    <TouchableOpacity
                        onPress={() => props.navigation.navigate('add_maimtance')}
                        style={{ backgroundColor: Colors.whiteFF, borderRadius: scale(42) }}>
                        <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        airport_list: state.Var.airport_list,
        userLocation: state.auth.userLocation,
        coldcahin_list: state.coldchain.coldcahin_list,
        assets_list: state.coldchain.assets_list,
    };
}
export default connect(mapStateToProps, {
    getColdchainList,
    getEquipmentList,
})(MaintaceScreen);
