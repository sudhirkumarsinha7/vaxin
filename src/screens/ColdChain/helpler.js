/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Dimensions,
    FlatList
} from 'react-native';
import { scale } from '../../components/Scale';
import {
    CustomButtonWithBorder,
} from '../../components/Common/helper';
import { Colors } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import fonts from '../../../FontFamily';
import { formatDateDDMMYYYY } from '../../Util/utils';
import { LineChart } from 'react-native-chart-kit';
import { TextLineComponent } from '../PAR/addParHelper';

export const TemperatureGraph = ({ data }) => {
    const { t, i18n } = useTranslation();

    // Extract labels (timestamps) and values for avgTemperature, ambientTemperature, and humidity
    const labels = data.map(temp => {
        const date = new Date(temp.timestamp);
        return `${date.getHours()}:${date.getMinutes()}`;
    });

    // const avgTemperatures = data.map(temp => temp.temperature);
    const ambientTemperatures = data.map(temp => temp.ambientTemperature);
    const humidities = data.map(temp => temp.humidity);
    const avgTemperatures = data.map(temp => {
        const sumTemperatures = temp.temperatures.reduce((acc, cur) => acc + cur, 0);
        return sumTemperatures / temp.temperatures.length;
    });
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{('Temperature, Ambient Temperature, and Humidity Graph')}</Text>
            <LineChart
                data={{
                    labels: labels,
                    datasets: [
                        {
                            data: avgTemperatures,
                            color: (opacity = 1) => `rgba(255, 0, 0, ${opacity})`, // red
                            strokeWidth: 2,
                        },
                        {
                            data: ambientTemperatures,
                            color: (opacity = 1) => `rgba(0, 0, 255, ${opacity})`, // blue
                            strokeWidth: 2,
                        },
                        {
                            data: humidities,
                            color: (opacity = 1) => `rgba(0, 255, 0, ${opacity})`, // green
                            strokeWidth: 2,
                        },
                    ],
                    legend: ['Avg Temperature', 'Ambient Temperature', 'Humidity'],
                }}
                width={Dimensions.get('window').width - 30} // from react-native
                height={220}
                yAxisSuffix="°C"
                chartConfig={{
                    backgroundColor: '#ffffff',
                    backgroundGradientFrom: '#ffffff',
                    backgroundGradientTo: '#ffffff',
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                    style: {
                        borderRadius: 16,
                    },
                    propsForDots: {
                        r: '6',
                        strokeWidth: '2',
                        stroke: '#ffa726',
                    },
                }}
                style={{
                    marginVertical: 8,
                    borderRadius: 16,
                }}
            />
        </View>
    );
};

export const TextLineComponentVertical = props => {
    const { leftText = '', rightText = '' } = props;
    return (
        <View style={{ marginTop: 10 }}>
            <Text style={{ fontSize: 14, fontFamily: fonts.REGULAR }}>{leftText}</Text>

            <Text
                style={{
                    fontSize: 14,
                    fontFamily: fonts.REGULAR,
                    color: '#7F7F7F',
                    marginTop: 10,
                }}>
                {rightText}
            </Text>
        </View>
    );
};
export function getBackgroundcolor(status) {
    if (status === 'CANCELLED' || status === 'CANCELED') {
        return Colors.redE6;
    } else if (status === 'RECEIVED') {
        return Colors.greenDA;
    } else {
        return '#D6D7EC';
    }
}
export function getTextcolor(status) {
    if (status === 'CANCELLED' || status === 'CANCELED') {
        return Colors.red00;
    } else if (status === 'RECEIVED') {
        return Colors.green00;
    } else {
        return '#545AEB';
    }
}
export function getTextStatus(status) {
    if (status === 'CANCELLED' || status === 'CANCELED') {
        return 'Canceled';
    } else if (status === 'CREATED') {
        return 'Shipped';
    } else if (status === 'RECEIVED') {
        return 'Delivered';
    } else {
        return status;
    }
}
export const MaintanceStatus = ({ status }) => {
    const { t, i18n } = useTranslation();

    return (
        <View
            style={{
                padding: 7,
                paddingHorizontal: scale(15),
                borderRadius: scale(20),
                backgroundColor: getBackgroundcolor(status),
            }}>
            <Text
                style={{
                    fontFamily: fonts.BOLD,
                    fontSize: 14,
                    color: getTextcolor(status),
                    textAlign: 'center',
                }}>
                {t(getTextStatus(status))}
            </Text>
        </View>
    );
};

export const MaintaceListComponent = props => {
    const { t, i18n } = useTranslation();

    const { item = {} } = props;
    const { locationDetails = {}, assetDetails = {} } = item;
    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: scale(7),
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                    alignSelf: 'center',
                }}>
                <View style={{ flex: 0.7, flexDirection: 'row', alignSelf: 'center' }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Location: ') + locationDetails.name}
                    </Text>
                </View>
                <View style={{ flex: 0.3, alignSelf: 'center' }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 12,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.date)}
                    </Text>
                </View>
            </View>
            <View style={{ paddingHorizontal: scale(7) }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.58 }}>
                        <Text
                            style={{
                                fontSize: 15,
                                color: Colors.black0,
                                fontFamily: fonts.BOLD,
                                marginTop: 5,
                            }}>
                            {assetDetails?.type}
                        </Text>

                        <View
                            style={{
                                borderBottomWidth: 0.5,
                                marginTop: 5,
                                borderColor: Colors.grayTextColor,
                            }}>
                            <Text
                                style={{
                                    fontFamily: fonts.REGULAR,
                                    fontSize: 14,
                                    color: Colors.grayTextColor,
                                }}>
                                {t('Model Number: ') + assetDetails?.model}
                            </Text>
                        </View>
                        <View
                            style={{
                                borderBottomWidth: 0.5,
                                marginTop: 5,
                                marginBottom: 10,
                                borderColor: Colors.grayTextColor,
                            }}>
                            <Text
                                style={{
                                    fontFamily: fonts.REGULAR,
                                    fontSize: 14,
                                    color: Colors.grayTextColor,
                                }}>
                                {t('Serial Number: ') + assetDetails?.serialNumber}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            flex: 0.38,
                            justifyContent: 'center',
                            alignItems: 'flex-end',
                        }}>
                        <MaintanceStatus status={'Completed'} />
                    </View>
                </View>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <View />
                <CustomButtonWithBorder
                    buttonName={t('View/Edit')}
                    onPressButton={() =>
                        props.navigation.navigate('EditMaintanceJob', { details: item })
                    }
                    bdColor={Colors.grayA8}
                    textColor={Colors.black0}
                />
            </View>
        </View>
    );
};
export const TempListComponent = props => {
    const { t, i18n } = useTranslation();

    const { item = {} } = props;
    const { temperatures = [] } = item
    const sum = temperatures.reduce((acc, cur) => acc + cur, 0);
    const avgTemperatures = sum / temperatures.length;

    return (
        <View
            style={{
                backgroundColor: Colors.blue96,
                padding: 10,
                borderRadius: 15,
                marginTop: 10
            }}>
            <TextLineComponent
                leftText="12:00 PM - 01:00 PM"
                rightText={temperatures && temperatures.length && temperatures[0] + '° C'}
            />
            <TextLineComponent
                leftText="01:00 PM - 02:00 PM"
                rightText={temperatures && temperatures.length && temperatures[1] + '° C'}

            />
            <TextLineComponent
                leftText="02:00 PM - 03:00 PM"
                rightText={temperatures && temperatures.length && temperatures[2] + '° C'}

            />
            <TextLineComponent
                leftText="03:00 PM - 04:00 PM"
                rightText={temperatures && temperatures.length && temperatures[3] + '° C'}

            />

            <TextLineComponent
                leftText={t('Average Temperature')}
                rightText={avgTemperatures + '° C'}

            />

            <TextLineComponent
                leftText={t('Ambient Temperature')}
                rightText={item.ambientTemperature + '° C'}
            />

            <TextLineComponent
                leftText={t('Humidity')}
                rightText={item.ambientTemperature + '%'}
            />
        </View>
    );
};
export const TemperatureGraphOld = () => {
    const data = [
        { time: '12:00 AM', temperature: 25 },
        { time: '3:00 AM', temperature: 24 },
        { time: '6:00 AM', temperature: 23 },
        { time: '9:00 AM', temperature: 22 },
        { time: '12:00 PM', temperature: 24 },
        { time: '3:00 PM', temperature: 26 },
        { time: '6:00 PM', temperature: 27 },
        { time: '9:00 PM', temperature: 26 },
    ];

    const renderDataPoints = () => {
        const points = [];
        const maxX = data.length * 50;
        const maxY = Math.max(...data.map(point => point.temperature));

        data.forEach((point, index) => {
            const x = index * 50;
            const y = (maxY - point.temperature) * 5;
            points.push(
                <View key={index} style={[styles.dataPoint, { left: x, bottom: y }]} />,
            );
        });

        return points;
    };

    return (
        <View style={styles.container}>
            <View style={styles.chart}>
                {renderDataPoints()}
                <View style={styles.xAxis} />
                <View style={styles.yAxis} />
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    title: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    chart: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        width: '80%',
        height: 200,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 20,
    },
    dataPoint: {
        position: 'absolute',
        width: 10,
        height: 10,
        backgroundColor: 'blue',
        borderRadius: 5,
    },
    xAxis: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        height: 1,
        backgroundColor: 'black',
    },
    yAxis: {
        position: 'absolute',
        right: 0,
        width: 1,
        height: '100%',
        backgroundColor: 'black',
    },
});
