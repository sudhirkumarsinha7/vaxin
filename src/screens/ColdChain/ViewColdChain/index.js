/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Keyboard
} from 'react-native';
import { connect } from 'react-redux';
import { TextLineComponent } from '../../VAR/addVarHelper';
import { add_temperature, getEquipmentList } from '../../../redux/action/coldchain';

import { HeaderWithBack } from '../../../components/Header';
import { useTranslation } from 'react-i18next';
import { useIsFocused } from '@react-navigation/native';
import fonts from '../../../../FontFamily';

import { getProdList } from '../../../redux/action/inventory';
import { useRoute } from '@react-navigation/native';
import { formatDateDDMMYYYY, formatDateDDMMYYYYTime } from '../../../Util/utils';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { CustomButton, InputField } from '../../../components/Common/helper';
import { ToastShow } from '../../../components/Toast';
const ViewColdCahin = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isMandatory, setIsMandatory] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);

    const route = useRoute();
    const { params } = route;
    const details = params.details || {};
    // const actionSheetRef = createRef();
    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
    }, [isFocused]);
    const GetData = async () => { };

    var validationSchema = Yup.object().shape(
        {

            t1: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            t2: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),

            t3: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            t4: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            ambientTemperature: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            humidity: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
        },
        [],
    ); // <-- HERE!!!!!!!!
    const temperatureForm = useFormik({
        initialValues: {

            t1: '',
            t2: '',
            t3: '',
            t4: '',
            ambientTemperature: '',
            humidity: '',



        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _addtemp = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        temperatureForm.handleSubmit();
    };

    const handleSubmit = async values => {
        // console.log('handleSubmit values', values)
        let body = {
            assetId: details._id,
            timestamp: new Date(),
            temperatures: [values.t1, values.t2, values.t3, values.t4],
            ambientTemperature: values.ambientTemperature,
            humidity: values.humidity
        };
        console.log('add_temperature values', body)
        const result = await props.add_temperature(body);
        setLocalLoader(false);
        console.log('add_temperature result?.status' + JSON.stringify(result));

        if (result?.status === 200) {
            await props.getEquipmentList();
            ToastShow(t('Temperature added suceesfully'), 'success', 'long', 'top');
            props.navigation.goBack();
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(
                t(err),
                'error',
                'long',
                'top',
            );
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(
                t(err),
                'error',
                'long',
                'top',
            );
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(
                t((err?.message)),
                'error',
                'long',
                'top',
            );
        }


    };


    const { location = {}, manufacturerDetails = {} } = details;

    return (
        <View style={styles.container}>
            <HeaderWithBack navigation={props.navigation} name={t('Asset Details')} />
            <VirtualizedList>
                <View style={{ padding: 15 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 16,
                            marginTop: 10,
                            textAlign: 'center',
                        }}>
                        {t('Report Information')}
                    </Text>
                    <TextLineComponent
                        leftText="Reporting Period time"
                        rightText={formatDateDDMMYYYYTime(new Date())} />
                </View>

                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 16,
                        marginTop: 10,
                        textAlign: 'center',
                    }}>
                    {t('Asset Information')}
                </Text>


                <View style={{ padding: 15 }}>
                    <TextLineComponent
                        leftText={t('Facility ID')}
                        rightText={formatDateDDMMYYYY(details.id)}
                    />
                    <TextLineComponent
                        leftText={t('Refrigerator ID')}
                        rightText={details.serialNumber}
                    />
                    <TextLineComponent leftText="Location" rightText={location?.name} />

                    <TextLineComponent
                        leftText={t('Manufacturer')}
                        rightText={manufacturerDetails?.name}
                    />
                </View>
                <View style={{ padding: 15 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 16,
                            marginTop: 10,
                            textAlign: 'center',
                        }}>
                        {t('Functional Status')}
                    </Text>
                    <InputField
                        placeholder={t('Temperature') + ' 1'}
                        label={t('Temperature') + ' 1'}
                        inputValue={temperatureForm?.values?.t1}
                        setInputValue={temperatureForm.handleChange('t1')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={temperatureForm?.errors?.t1}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Temperature') + ' 2'}
                        label={t('Temperature') + ' 2'}
                        inputValue={temperatureForm?.values?.t2}
                        setInputValue={temperatureForm.handleChange('t2')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={temperatureForm?.errors?.t2}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Temperature') + ' 3'}
                        label={t('Temperature') + ' 3'}
                        inputValue={temperatureForm?.values?.t3}
                        setInputValue={temperatureForm.handleChange('t3')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={temperatureForm?.errors?.t3}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Temperature') + ' 4'}
                        label={t('Temperature') + ' 4'}
                        inputValue={temperatureForm?.values?.t4}
                        setInputValue={temperatureForm.handleChange('t4')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={temperatureForm?.errors?.t4}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Ambient Temperature')}
                        label={t('Ambient Temperature')}
                        inputValue={temperatureForm?.values?.ambientTemperature}
                        setInputValue={temperatureForm.handleChange('ambientTemperature')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={temperatureForm?.errors?.ambientTemperature}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Humidity')}
                        label={t('Humidity')}
                        inputValue={temperatureForm?.values?.humidity}
                        setInputValue={temperatureForm.handleChange('humidity')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={temperatureForm?.errors?.humidity}
                        mandatory={isMandatory}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View />
                        <View style={{ justifyContent: 'flex-end' }}>
                            <CustomButton
                                bgColor={'#208196'}
                                buttonName={t('Save')}
                                navigation={props.navigation}
                                onPressButton={_addtemp}
                            />
                        </View>
                    </View>
                </View>
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        inv_product_list: state.inventory.inv_product_list,
        userLocation: state.auth.userLocation,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    add_temperature,
    getEquipmentList
})(ViewColdCahin);
