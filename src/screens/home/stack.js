/* eslint-disable prettier/prettier */
import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Home from './index';

import VAR from '../VAR';
import CreateVAR from '../VAR/createVar';
import ReviewVAR from '../VAR/reviewVar';
import ViewVAR from '../VAR/viewReport';

import PAR from '../PAR';
import CreatePar from '../PAR/createPar';
import ReviewPAR from '../PAR/reviewPar';
import ViewPAR from '../PAR/viewReport';

import ProfileStack from '../profile';
import NotificationTab from '../Notificans/stack';
import ColdCahionTab from '../ColdChain/stack';
import BeneficiariesScreen from '../Beneficiaries/stack';
import CampaignStack from '../CompainMangement/stack';
import SelectLang from '../SelectLAng';
import GeoTracking from '../GeoTracking';

const headerOptions = {
    headerShown: false,
};
const Stack = createNativeStackNavigator();

const HomeStack = () => {
    return (
        <Stack.Navigator initialRouteName="home" screenOptions={headerOptions}>
            <Stack.Screen
                name="home"
                component={Home}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="var"
                component={VAR}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="createVar"
                component={CreateVAR}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="profile"
                component={ProfileStack}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="view_var"
                component={ViewVAR}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="review_var"
                component={ReviewVAR}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="par"
                component={PAR}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="create_par"
                component={CreatePar}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="review_par"
                component={ReviewPAR}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="view_par"
                component={ViewPAR}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="notification"
                component={NotificationTab}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="coldchain"
                component={ColdCahionTab}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Beneficiaries"
                component={BeneficiariesScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Campaign"
                component={CampaignStack}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="SelectLang"
                component={SelectLang}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="GeoTracking"
                component={GeoTracking}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};
export default HomeStack;
