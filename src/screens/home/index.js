/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect, cloneElement } from 'react';
import {
    View,
    ScrollView,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import Header from '../../components/Header';
import { scale } from '../../components/Scale';
import { useTranslation } from 'react-i18next';
import fonts from '../../../FontFamily';
import Search from '../../components/Search';
import { useIsFocused } from '@react-navigation/native';
import { CardNavigationBtnHome } from '../../components/Common/Button';
import { DeviceWidth, localImage } from '../../config/global';
import { Colors, DeviceHeight, width } from '../../components/Common/Style';
import { getInventoryAnalytcs, getProdList } from '../../redux/action/inventory';
import {
    getInboudOrderAnalytcs,
    getOutboudOrderAnalytcs,
} from '../../redux/action/order';
import {
    getInboudShipmentAnalytcs,
    getOutboudShipmentAnalytcs,
} from '../../redux/action/shipment';
import { getUserInfo, getUserLocation } from '../../redux/action/auth';
import InventorySvg from '../../assets/img/inventory.svg'
import OrderSvg from '../../assets/img/order.svg'
import ShipmentSvg from '../../assets/img/shipment.svg'
const HomeScreen = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [localLoader, setLocalLoader] = useState(false);
    useEffect(() => {
        async function fetchDeta() {
            setLocalLoader(true);
            await props.getUserInfo();
            await props.getInboudOrderAnalytcs();
            await props.getOutboudOrderAnalytcs();
            await props.getInboudShipmentAnalytcs();
            await props.getOutboudShipmentAnalytcs();
            await props.getInventoryAnalytcs();
            await props.getProdList();
            await props.getUserInfo();
            setLocalLoader(false);
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const {
        order_inbound_analytcs = {},
        inv_product_list = [],
        order_outbound_analytcs = {},
        shipment_inbound_analytcs = {},
        shipment_outbound_analytcs = {},
        inv_analytics = {},
    } = props;

    return (
        <View style={styles.container}>
            <Header navigation={props.navigation} name={t('inventory')} />

            <ScrollView
                style={{ padding: 20, backgroundColor: '#fff' }}
                scrollEnabled={true}
                showsVerticalScrollIndicator={false}>
                <View
                    style={{
                        borderTopLeftRadius: 10,
                        borderTopEndRadius: 10,
                        paddingBottom: 10,
                    }}>
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 12,
                        }}>
                        <View
                            style={{
                                borderRadius: 2,
                                width: scale(72),
                                height: scale(3),
                                backgroundColor: '#9B9B9B',
                            }}
                        />
                    </View>
                </View>
                <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                    <Image
                        source={localImage.Welcome}
                        style={{ height: 140, width: 140 }}
                    />
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            color: '#208196',
                            fontSize: 20,
                            margin: 15,
                        }}>
                        {t('Welcome Aboard!')}
                    </Text>
                </View>
                {props.loder || localLoader ? (
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 1,
                        }}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                ) : null}
                <CardNavigationBtnHome
                    type="icon"
                    image={<ShipmentSvg height={40} width={40} />}
                    title={t('Shipments')}
                    subtitle1={t('Inbound')}
                    subtitle2={t('Outbound')}
                    val1={shipment_inbound_analytcs.totalInboundShipments || 0}
                    val2={shipment_outbound_analytcs.totalOutboundShipments || 0}
                    textColor={'#709620'}
                    onPressButton={() =>
                        props.navigation.navigate('shipment', {
                            screen: 'inboundshipment',
                        })
                    }
                />

                <CardNavigationBtnHome
                    type="icon"
                    image={<OrderSvg height={40} width={40} />}
                    title={t('Orders')}
                    subtitle1={t('Inbound')}
                    subtitle2={t('Outbound')}
                    val1={order_inbound_analytcs.totalOrders || 0}
                    val2={order_outbound_analytcs.totalOrders || 0}
                    textColor={'#852096'}
                    onPressButton={() =>
                        props.navigation.navigate('order', {
                            screen: 'outbound',
                        })
                    }
                />

                <CardNavigationBtnHome
                    type="icon"
                    image={<InventorySvg height={40} width={40} />}
                    title={t('Total no. of Products ')}
                    subtitle1={t('Products')}
                    subtitle2={t('Current Stock')}
                    val1={inv_product_list.length}
                    // val2={1200000}

                    val2={inv_analytics.inStock || 0}
                    // descpription={inv_product_list.length}
                    textColor={'#205696'}
                    onPressButton={() => props.navigation.navigate('inventrory')}
                />

                <View style={{ height: 100 }} />
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});
function mapStateToProps(state) {
    return {
        order_inbound_analytcs: state.order.order_inbound_analytcs,
        order_outbound_analytcs: state.order.order_outbound_analytcs,
        shipment_inbound_analytcs: state.shipment.shipment_inbound_analytcs,
        shipment_outbound_analytcs: state.shipment.shipment_outbound_analytcs,
        inv_analytics: state.inventory.inv_analytics,
        inv_product_list: state.inventory.inv_product_list,
        loder: state.loder,
    };
}
export default connect(mapStateToProps, {
    getInboudOrderAnalytcs,
    getOutboudOrderAnalytcs,
    getInboudShipmentAnalytcs,
    getOutboudShipmentAnalytcs,
    getInventoryAnalytcs,
    getProdList,
    getUserInfo,
})(HomeScreen);
