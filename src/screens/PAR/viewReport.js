/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';
import { useRoute } from '@react-navigation/native';
import { useIsFocused } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ParViewComponent } from './addParHelper';
import {
    create_Var,
} from '../../redux/action/var';
import { VirtualizedList } from '../inventory/AddInventory/productHelper';

const ViewVarComponent = props => {
    const { t } = useTranslation();

    const route = useRoute();
    const { params } = route;
    const { form1 } = params;

    useEffect(() => {
        async function fetchDeta() { }
        fetchDeta();
    }, []);

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Product Arrival Report')}
            />

            <VirtualizedList>
                <ParViewComponent form1={form1} />
            </VirtualizedList>
        </View>
    );
};

function mapStateToProps(state) {
    return {};
}
export default connect(mapStateToProps, {
    create_Var,
})(ViewVarComponent);
