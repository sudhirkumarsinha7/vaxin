/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    Keyboard,
    View,
    Text,
    TouchableOpacity,
    Platform,
    PermissionsAndroid,
    FlatList,
    ActivityIndicator,
} from 'react-native';
import { scale } from '../../components/Scale';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import DropDown, { ManufacturerDropdown } from '../../components/Dropdown';
import {
    InputField,
    CustomButtonWithBorder,
    CustomButton,
} from '../../components/Common/helper';
import {
    DatePickerComponent,
    DateTimePickerComponent,
} from '../../components/DatePicker';
import fonts from '../../../FontFamily';
import { ToastShow } from '../../components/Toast';
import RadioButtonGroup from '../../components/RadioButton';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
    selectPhotoFromGallary,
    takePhotoFromCamera,
} from '../../components/ImageHelper';
import { filterUniqueObjectsByProperty } from '../inventory/AddInventory/productUtil';
import { Colors } from '../../components/Common/Style';
import { ImageUploadPopUp } from '../../components/popUp';
import { formatDateDDMMYYYY, formatDateDDMMYYYYTime } from '../../Util/utils';
const options = [
    { id: true, label: 'Yes' },
    { id: false, label: 'No' },
];

const AddNewProduct = () => {
    const add = {
        noOfBoxes: '',
        mfgDate: '',
        expDate: '',
        batchNo: '',
        noOfUnits: '',
    };
    return add;
};
export const AddParPart = props => {
    const { t } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);
    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        if (form.invoiceAmount) {
            addParForm.setValues(form);
        }
        if (inv_list.length) {
            addParForm.handleChange({
                target: { name: 'inventoryId', value: inv_list[0]._id },
            });
            addParForm.handleChange({
                target: { name: 'inventory', value: inv_list[0].name },
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [form]);

    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        async function fetchDeta() {
            const data = { value: 'BD', label: 'Bangladesh' };
            await changeAfterSelectCountry(data);
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    var validationSchema = Yup.object().shape(
        {
            productId: Yup.string().required(t('Required')),
            manufacturerId: Yup.string().required(t('Required')),
            invoiceDate: Yup.string().required(t('Required')),
            recieveDateTime: Yup.string().required(t('Required')),
            inventoryId: Yup.string().required(t('Required')),

            poNo: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            countryId: Yup.string().required(t('Required')),
            invoiceAmount: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            quantityPerUnit: Yup.string().required(t('Required')),
            products: Yup.array()
                .of(
                    Yup.object().shape({
                        mfgDate: Yup.string().required(t('Required')),
                        batchNo: Yup.string()
                            .required(t('Required'))
                            .trim()
                            .min(2, t('Must be at least 2 characters long'))
                            .test(
                                'no-spaces',
                                t('It must not be empty spaces'),
                                value => value && value.trim() !== '',
                            ),
                        noOfUnits: Yup.string().required(t('Required')),
                    }),
                )
                .required(t('Required')),
        },
        ['country', 'placeOfInspection'],
    ); // <-- HERE!!!!!!!!
    const addParForm = useFormik({
        initialValues: {
            country: '',
            countryId: '',
            manufacturerId: '',
            invoiceDate: new Date(),
            productId: '',
            consignee: 'CMSD- Central Medical Stores Depot',
            recieveDateTime: new Date(),
            poNo: '',
            invoiceAmount: '',
            quantityPerUnit: '',
            inventoryId: '',
            products: [
                {
                    noOfBoxes: '',
                    mfgDate: null,
                    expDate: null,
                    batchNo: '',
                    noOfUnits: '',
                },
            ],
            shortShipmentNotification: true,
            shortShipment: true,
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addParForm.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };

    const changeAfterSelectCountry = async item => {
        const { value, label } = item;
        addParForm.handleChange({ target: { name: 'country', value: label } });
        addParForm.handleChange({ target: { name: 'countryId', value: value } });
        await props.getAirports(value);
    };

    const onChangeInVoiceDate = val => {
        addParForm.handleChange({ target: { name: 'invoiceDate', value: val } });
    };
    const onChangeInspectionDate = val => {
        addParForm.handleChange({ target: { name: 'recieveDateTime', value: val } });
        addParForm.handleChange({ target: { name: 'enteredDateTime', value: val } });
    };
    const changeAfterSelectPerUnit = item => {
        const { value } = item;
        addParForm.handleChange({ target: { name: 'quantityPerUnit', value: value } });
    };

    const changeAfterSelectProduct = item => {
        const { value, eachItem } = item;
        console.log('each ' + JSON.stringify(eachItem));
        const { manufacturers = [] } = eachItem;
        addParForm.handleChange({ target: { name: 'productId', value: value } });
        addParForm.handleChange({
            target: { name: 'productName', value: eachItem.name },
        });
        addParForm.handleChange({ target: { name: 'units', value: eachItem.units } });
        if (manufacturers.length) {
            setManufatureList(manufacturers);
            setPrdName(eachItem.name);
        } else {
            ToastShow(t('Please select another Product'), 'error', 'long', 'top');
        }
    };
    const changeAfterManufature = item => {
        const { value, eachItem, label } = item;
        addParForm.handleChange({ target: { name: 'manufacturerId', value: value } });
        addParForm.handleChange({ target: { name: 'manufacturerName', value: label } });
        if (eachItem.quantityPerUnit.length) {
            props.setPerUnitList(eachItem.quantityPerUnit);
            addParForm.handleChange({
                target: { name: 'quantityPerUnit', value: eachItem.quantityPerUnit[0] },
            });
        }
    };
    const onChangeMfgDate = (val, i) => {
        // addParForm.handleChange({ target: { name: `mfgDate`, value: val, } })
        addParForm.handleChange({
            target: { name: `products[${i}].mfgDate`, value: val },
        });
        addParForm.handleChange({
            target: { name: `products[${i}].expDate`, value: null },
        });
    };
    const onChangeExpDate = (val, i) => {
        addParForm.handleChange({
            target: { name: `products[${i}].expDate`, value: val },
        });
    };

    const AddAnotherProduct = () => {
        const data = [...addParForm.values.products];
        const newData = AddNewProduct();
        const finalData = [...data, newData];

        addParForm.handleChange({ target: { name: 'products', value: finalData } });
    };
    const cancel = index => {
        const productClone = JSON.parse(
            JSON.stringify(addParForm?.values?.products),
        );
        // console.log(Object.keys(productClone));
        productClone.splice(index, 1);

        addParForm.handleChange({ target: { name: 'products', value: productClone } });
    };
    const DeleteIcon = index => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        width: 40,
                        height: 40,
                        borderRadius: 400,
                        backgroundColor: '#fff',
                        justifyContent: 'center',
                        alignItems: 'center',
                        elevation: 10,
                        marginTop: 5,
                    }}
                    opacity={5}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'flex-end',
                        }}
                        onPress={() => cancel(index)}
                        disabled={addParForm?.values?.products.length === 1 ? true : false}>
                        <MaterialIcons
                            name="delete"
                            size={24}
                            color={addParForm?.values?.products.length === 1 ? 'gray' : 'red'}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
    const ProductListComponent = () => {
        return (
            <View
                style={{
                    backgroundColor: '#E0F0EA',
                    paddingHorizontal: 15,
                    marginTop: 10,
                    borderRadius: 15,
                }}>
                {addParForm?.values?.products.map((each, index) => {
                    return (
                        <View>
                            <View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                    }}>
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            fontFamily: fonts.REGULAR,
                                            marginBottom: 10,
                                            textAlign: 'center',
                                        }}>
                                        {index + 1 + '. Product'}
                                    </Text>
                                    {DeleteIcon(index)}
                                </View>

                                <View
                                    style={{
                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                    }}>
                                    <View style={{ flex: 0.48 }}>
                                        {/* <InputField
                                        placeholder="Enter here"
                                        label="No. of Boxes"
                                        inputValue={addParForm?.values?.products[index]?.noOfBoxes + ''}
                                        // setInputValue={addParForm.handleChange('noOfBoxes')}
                                        setInputValue={addParForm.handleChange(`products[${index}].noOfBoxes`)}
                                        KeyboardType={'numeric'}
                                        inputStyle={{ marginBottom: 10 }}
                                        labelStyle={{ marginBottom: 5 }}
                                        errorMsg={addParForm.errors && addParForm.errors.products && addParForm.errors.products[index] && addParForm.errors.products[index].noOfBoxes}

                                        mandatory={isMandatory}
                                    /> */}
                                        <InputField
                                            placeholder={t('Enter here')}
                                            label={t('Lot Number')}
                                            inputValue={addParForm?.values?.products[index]?.batchNo}
                                            setInputValue={addParForm.handleChange(
                                                `products[${index}].batchNo`,
                                            )}
                                            errorMsg={
                                                addParForm.errors &&
                                                addParForm.errors.products &&
                                                addParForm.errors.products[index] &&
                                                addParForm.errors.products[index].batchNo
                                            }
                                            inputStyle={{ marginBottom: 10 }}
                                            labelStyle={{ marginBottom: 5 }}
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                    <View style={{ flex: 0.48 }}>
                                        <InputField
                                            placeholder={t('Enter here')}
                                            label={t('No. of Units')}
                                            inputValue={
                                                addParForm?.values?.products[index]?.noOfUnits + ''
                                            }
                                            // setInputValue={addParForm.handleChange('noOfBoxes')}
                                            setInputValue={addParForm.handleChange(
                                                `products[${index}].noOfUnits`,
                                            )}
                                            KeyboardType={'numeric'}
                                            inputStyle={{ marginBottom: 10 }}
                                            labelStyle={{ marginBottom: 5 }}
                                            errorMsg={
                                                addParForm.errors &&
                                                addParForm.errors.products &&
                                                addParForm.errors.products[index] &&
                                                addParForm.errors.products[index].noOfUnits
                                            }
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                </View>
                                <View
                                    style={{
                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                    }}>
                                    <View style={{ flex: 0.48 }}>
                                        <DatePickerComponent
                                            label={t('Mfg Date')}
                                            placeholder="DD/MM/YYYY"
                                            onChange={val => onChangeMfgDate(val, index)}
                                            val={addParForm?.values?.products[index]?.mfgDate}
                                            errorMsg={
                                                addParForm.errors &&
                                                addParForm.errors.products &&
                                                addParForm.errors.products[index] &&
                                                addParForm.errors.products[index].mfgDate
                                            }
                                            isMfgDate={true}
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                    <View style={{ flex: 0.48 }}>
                                        <DatePickerComponent
                                            label={t('Expiry Date')}
                                            placeholder="DD/MM/YYYY"
                                            onChange={val => onChangeExpDate(val, index)}
                                            val={addParForm?.values?.products[index]?.expDate}
                                            errorMsg={
                                                addParForm.errors &&
                                                addParForm.errors.products &&
                                                addParForm.errors.products[index] &&
                                                addParForm.errors.products[index].expDate
                                            }
                                            minDate={
                                                addParForm?.values?.products[index]?.mfgDate
                                                    ? new Date(
                                                        addParForm?.values?.products[index]?.mfgDate,
                                                    ) >= new Date()
                                                        ? addParForm?.values?.products[index]?.mfgDate
                                                        : new Date()
                                                    : new Date()
                                            }
                                            isMfgDate={false}
                                            isMandatoryField={false}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                    );
                })}
                <View style={{ flexDirection: 'row' }}>
                    <CustomButtonWithBorder
                        buttonName={t('Add more')}
                        onPressButton={AddAnotherProduct}
                        bdColor={'#BFBFBF'}
                        textColor={'#20232B'}
                        bgColor={'#fff'}
                    />
                </View>
            </View>
        );
    };
    const changeAfterSelectInventory = item => {
        const { value, label } = item;
        addParForm.handleChange({ target: { name: 'inventoryId', value: value } });
        addParForm.handleChange({ target: { name: 'inventory', value: label } });
    };
    const {
        currentStep,
        handleNext,
        handlePrev,
        country_list = [],
        inv_product_list = [],
        setManufatureList,
        form,
        setPrdName,
        manufaturerList,
        inv_list = [],
        perUnitList = [],
    } = props;
    const uniqueProduct = filterUniqueObjectsByProperty(inv_product_list, 'name');
    const VaccineList1 = uniqueProduct.filter(each => {
        if (
            each.type === 'Vaccine' ||
            each.type === 'VACCINE' ||
            each.type === 'vaccine'
        ) {
        } else {
            return each;
        }
    });
    const VaccineList = VaccineList1.sort((a, b) => a.name.localeCompare(b.name));
    // console.log('VaccineList ' + JSON.stringify(VaccineList))

    return (
        <View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 10,
                }}>
                <View>
                    <View style={{ marginTop: 10 }} />
                    <DropDown
                        dropdownData={VaccineList}
                        onChangeValue={changeAfterSelectProduct}
                        label="name"
                        mapKey="_id"
                        val={addParForm?.values?.productId}
                        placeholderText={t('Product Name')}
                        labelText={t('Product Name')}
                        changeAfterSelect={() => console.log()}
                        errorMsg={addParForm?.errors?.order_id}
                        mandatory={isMandatory}
                    />
                    <ManufacturerDropdown
                        dropdownData={manufaturerList}
                        onChangeValue={changeAfterManufature}
                        label="name"
                        mapKey="id"
                        val={addParForm?.values?.manufacturerId}
                        placeholderText={t('Manufacturer')}
                        labelText={t('Manufacturer')}
                        errorMsg={addParForm?.errors?.manufacturerId}
                        mandatory={isMandatory}
                        search={false}
                    />

                    {/* <InputField
                        placeholder="Report No. "
                        label="Report No. "
                        inputValue={addParForm?.values?.reportNo}
                        setInputValue={addParForm.handleChange('reportNo')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addParForm?.errors?.reportNo}
                        mandatory={isMandatory}
                    /> */}
                    <DatePickerComponent
                        label={t('Date of Invoice')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeInVoiceDate(val)}
                        val={addParForm?.values?.invoiceDate}
                        errorMsg={addParForm?.errors?.invoiceDate}
                        mandatory={isMandatory}
                    />
                    <DateTimePickerComponent
                        label={t('Date of Shipment Received')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeInspectionDate(val)}
                        val={addParForm?.values?.recieveDateTime}
                        errorMsg={addParForm?.errors?.recieveDateTime}
                        mandatory={isMandatory}
                    />
                    <Text
                        style={{ textAlign: 'center', fontSize: 16, fontFamily: fonts.BOLD }}>
                        {t('Part 1 of 3')}
                    </Text>

                    <Text
                        style={{
                            textAlign: 'center',
                            fontSize: 14,
                            fontFamily: fonts.BOLD,
                            marginTop: 10,
                        }}>
                        {t('Lot Details')}
                    </Text>
                    <View style={{ marginTop: 10 }} />

                    <View>
                        <InputField
                            placeholder={t('Enter here')}
                            label={t('Purchase Order Number')}
                            inputValue={addParForm?.values?.poNo}
                            setInputValue={addParForm.handleChange('poNo')}
                            inputStyle={{ marginBottom: 10 }}
                            labelStyle={{ marginBottom: 5 }}
                            errorMsg={addParForm?.errors?.poNo}
                            mandatory={isMandatory}
                        />
                        {/* <InputField
                            placeholder="Enter here"
                            label="Product Description( and Doses/Vial)"
                            inputValue={addParForm?.values?.doses}
                            setInputValue={addParForm.handleChange('doses')}
                            inputStyle={{ marginBottom: 10 }}
                            labelStyle={{ marginBottom: 5 }}
                            errorMsg={addParForm?.errors?.doses}
                            mandatory={isMandatory}
                        /> */}
                        <DropDown
                            dropdownData={country_list}
                            onChangeValue={changeAfterSelectCountry}
                            label="name"
                            mapKey="_id"
                            val={addParForm?.values?.countryId}
                            placeholderText={('Country')}
                            labelText={t('Country')}
                            errorMsg={addParForm?.errors?.countryId}
                            mandatory={isMandatory}
                            search={true}
                        />

                        <InputField
                            placeholder={t('Enter here')}
                            label={t('Consignee')}
                            inputValue={addParForm?.values?.consignee}
                            setInputValue={addParForm.handleChange('consignee')}
                            inputStyle={{ marginBottom: 10 }}
                            labelStyle={{ marginBottom: 5 }}
                            errorMsg={addParForm?.errors?.consignee}
                            mandatory={isMandatory}
                            disable={true}
                        />

                        <DropDown
                            dropdownData={inv_list}
                            onChangeValue={changeAfterSelectInventory}
                            label="name"
                            mapKey="_id"
                            val={addParForm?.values?.inventoryId}
                            placeholder={t('Select Location')}
                            errorMsg={addParForm?.errors?.inventoryId}
                            mandatory={isMandatory}
                            labelText={t('Inventory Location')}
                            search={false}
                        />

                        <DropDown
                            dropdownData={perUnitList}
                            onChangeValue={changeAfterSelectPerUnit}
                            val={addParForm?.values?.quantityPerUnit}
                            placeholder={t('Select')}
                            errorMsg={addParForm?.errors?.quantityPerUnit}
                            mandatory={isMandatory}
                            labelText={t('Quantity Per unit')}
                            search={false}
                        />
                        <InputField
                            placeholder={t('Enter here')}
                            label={t('Invoice Amount')}
                            inputValue={addParForm?.values?.invoiceAmount}
                            setInputValue={addParForm.handleChange('invoiceAmount')}
                            inputStyle={{ marginBottom: 10 }}
                            labelStyle={{ marginBottom: 5 }}
                            errorMsg={addParForm?.errors?.invoiceAmount}
                            mandatory={isMandatory}
                            KeyboardType={'numeric'}
                        />
                    </View>

                    {ProductListComponent()}

                    <RadioButtonGroup
                        label={t('was quantity received as per shipping notification?')}
                        options={options}
                        val={addParForm?.values?.shortShipmentNotification}
                        onChangeValue={val =>
                            addParForm.handleChange({
                                target: { name: 'shortShipmentNotification', value: val },
                            })
                        }
                    />
                    <RadioButtonGroup
                        label={
                            t('if not, were details of short-shipment provided prior to Product arrival?')
                        }
                        options={options}
                        val={addParForm?.values?.shortShipment}
                        onChangeValue={val =>
                            addParForm.handleChange({
                                target: { name: 'shortShipment', value: val },
                            })
                        }
                    />

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        {currentStep > 0 && (
                            <CustomButtonWithBorder
                                buttonName={t('Prev')}
                                onPressButton={handlePrev}
                                bdColor={'#BFBFBF'}
                                textColor={'#20232B'}
                            />
                        )}
                        <CustomButton
                            buttonName={t('Save && continue')}
                            onPressButton={_add}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>
                </View>
            </View>
        </View>
    );
};

export const AddParPart1 = props => {
    const { t } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);

    useEffect(() => {
        // Use useEffect to update initial values when parent data changes

        if (form.comment) {
            addParForm.setValues(form);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [form]);
    var validationSchema = Yup.object().shape(
        {
            reportNo: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            reportDate: Yup.string().required(t('Required')),
            comment: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
        },
        ['comment'],
    ); // <-- HERE!!!!!!!!
    const reportNum =
        'PAR-BANA-' +
        new Date().getFullYear() +
        '-' +
        (new Date().getMonth() + 1) +
        '-' +
        Math.floor(Math.random() * 10);
    const addParForm = useFormik({
        initialValues: {
            invoice: true,
            packingList: true,
            vaccineArrivalReport: true,
            releaseCertificate: true,
            comment: '',
            reportNo: reportNum,
            reportDate: new Date(),
            labelsAttached: true,
            type: 'GOOD',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addParForm.handleSubmit();
    };

    const handleSubmit = async values => {
        console.log(' values', values);
        await props.setForm(values);

        handleNext();
    };
    const changeAfterTypeSelect = item => {
        const { value } = item;
        addParForm.handleChange({ target: { name: 'type', value: value } });
    };
    const onChangeReportDate = val => {
        addParForm.handleChange({ target: { name: 'reportDate', value: val } });
    };

    const { currentStep, handleNext, handlePrev, form } = props;
    return (
        <View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 15,
                }}>
                <Text
                    style={{ textAlign: 'center', fontSize: 16, fontFamily: fonts.BOLD }}>
                    {t('Part 2 of 3')}
                </Text>

                <Text
                    style={{
                        textAlign: 'center',
                        fontSize: 14,
                        fontFamily: fonts.BOLD,
                        marginTop: 10,
                    }}>
                    {t('Documents Accompanying the Shipment')}
                </Text>
                <View style={{ marginTop: 10 }} />

                <InputField
                    placeholder={t('Report No.')}
                    label={t('Report No.')}
                    inputValue={addParForm?.values?.reportNo}
                    setInputValue={addParForm.handleChange('reportNo')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addParForm?.errors?.reportNo}
                    mandatory={isMandatory}
                />
                <DatePickerComponent
                    label={t('Date of Report')}
                    placeholder="DD/MM/YYYY"
                    onChange={val => onChangeReportDate(val)}
                    val={addParForm?.values?.reportDate}
                    errorMsg={addParForm?.errors?.reportDate}
                    mandatory={isMandatory}
                />
                <RadioButtonGroup
                    label={t('Invoice')}
                    options={options}
                    val={addParForm?.values?.invoice}
                    onChangeValue={val =>
                        addParForm.handleChange({ target: { name: 'invoice', value: val } })
                    }
                />
                <RadioButtonGroup
                    label={t('Packing List')}
                    options={options}
                    val={addParForm?.values?.packingList}
                    onChangeValue={val =>
                        addParForm.handleChange({ target: { name: 'packingList', value: val } })
                    }
                />
                <RadioButtonGroup
                    label={t('Product Arrival Report')}
                    options={options}
                    val={addParForm?.values?.vaccineArrivalReport}
                    onChangeValue={val =>
                        addParForm.handleChange({
                            target: { name: 'vaccineArrivalReport', value: val },
                        })
                    }
                />
                <RadioButtonGroup
                    label={t('Release Certificate')}
                    options={options}
                    val={addParForm?.values?.releaseCertificate}
                    onChangeValue={val =>
                        addParForm.handleChange({
                            target: { name: 'releaseCertificate', value: val },
                        })
                    }
                />

                <DropDown
                    dropdownData={['GOOD', 'DAMAGED', 'DEFECTIVE']}
                    onChangeValue={changeAfterTypeSelect}
                    // label='name'
                    // mapKey='_id'
                    val={addParForm?.values?.type}
                    placeholderText={t('Select')}
                    labelText={t('What was the condition of the boxes on arrival?')}
                    errorMsg={addParForm?.errors?.type}
                    mandatory={isMandatory}
                    search={false}
                />
                <RadioButtonGroup
                    label={t('Were necessary labels attached to the shipping boxes?')}
                    options={options}
                    val={addParForm?.values?.labelsAttached}
                    onChangeValue={val =>
                        addParForm.handleChange({
                            target: { name: 'labelsAttached', value: val },
                        })
                    }
                />

                <View style={{ marginTop: 10 }} />
                <InputField
                    placeholder={t('Enter here')}
                    label={t('Other comments including description of alarms in electronic devices')}
                    inputValue={addParForm?.values?.comment}
                    setInputValue={addParForm.handleChange('comment')}
                    inputStyle={{ marginBottom: 10, height: 100 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addParForm?.errors?.comment}
                    mandatory={isMandatory}
                />
                <View style={{ marginTop: 10 }} />
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    {currentStep > 0 && (
                        <CustomButtonWithBorder
                            buttonName={t('Prev')}
                            onPressButton={handlePrev}
                            bdColor={'#BFBFBF'}
                            textColor={'#20232B'}
                        />
                    )}
                    <CustomButton
                        buttonName={t('Save && continue')}
                        onPressButton={_add}
                        bgColor={'#208196'}
                        textColor={'#fff'}
                    />
                </View>
            </View>
        </View>
    );
};

export const AddParPart2 = props => {
    const { t, i18n } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);
    // const [inspectionSign, setInspectionSign] = useState('');
    // const [epiSign, setEpiSign] = useState('');
    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        if (form.inspectionSupervisorName) {
            addParForm.setValues(form);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [form]);
    const [visible, setVisible] = useState(false);
    const [epiSignVisible, setIpiSignVisible] = useState(false);

    const hideModal = () => setVisible(false);
    const hideEpiModal = () => setIpiSignVisible(false);

    const handleChoosePhoto = async () => {
        selectPhotoFromGallary(
            setInspectionSign,
            hideModal,
            onChangeInspectionSign,
        );
    };

    const handleEPIChoosePhoto = async () => {
        selectPhotoFromGallary(setEpiSign, hideEpiModal, onChangeEpiSign);
    };
    const onChangeInspectionSign = val => {
        addParForm.handleChange({
            target: { name: 'inspectionSupervisorSign', value: val },
        });
    };
    const onChangeEpiSign = val => {
        addParForm.handleChange({ target: { name: 'epiManagerSign', value: val } });
    };
    const setInspectionSign = val => {
        addParForm.handleChange({ target: { name: 'inspectionSign', value: val } });
    };
    const setEpiSign = val => {
        addParForm.handleChange({ target: { name: 'epiSign', value: val } });
    };

    const requestCameraPermission = async val => {
        if (Platform.OS == 'ios') {
            if (val === 'inspectionSign') {
                handleCameraPhoto();
            } else {
                handleEPICameraPhoto();
            }
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'App Camera Permission',
                        message: 'App needs access to your camera ',
                        buttonNeutral: 'Ask Me Later',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    // console.log('Camera permission given');
                    if (val === 'inspectionSign') {
                        handleCameraPhoto();
                    } else {
                        handleEPICameraPhoto();
                    }
                } else {
                    // console.log('Camera permission denied');
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };

    const handleCameraPhoto = async () => {
        takePhotoFromCamera(setInspectionSign, hideModal, onChangeInspectionSign);
    };
    const handleEPICameraPhoto = async () => {
        takePhotoFromCamera(setEpiSign, hideEpiModal, onChangeEpiSign);
    };
    var validationSchema = Yup.object().shape(
        {
            inspectionSupervisorName: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            // inspectionSupervisorSign: Yup.string().required(t('Required')),
            epiManagerName: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            contact: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            // epiManagerDate: Yup.string().required(t('Required')),
            // inspectionSupervisorDate: Yup.string().required(t('Required')),
            // receivedDate: Yup.string().required(t('Required')),
            // inspectionSign: Yup.string().required(t('Required')),
            // epiSign: Yup.string().required(t('Required')),
        },
        ['contact'],
    ); // <-- HERE!!!!!!!!
    const addParForm = useFormik({
        initialValues: {
            inspectionSupervisorName: '',
            inspectionSupervisorSign: '',
            inspectionSupervisorDate: new Date(),
            epiManagerName: '',
            epiManagerSign: '',
            epiManagerDate: new Date(),
            receivedDate: new Date(),
            contact: '',
            inspectionSign: '',
            epiSign: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });

    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addParForm.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };

    const onChangeInspectionDate = val => {
        addParForm.handleChange({
            target: { name: 'inspectionSupervisorDate', value: val },
        });
        props.setForm(addParForm?.values);
    };
    const onChangeEpiDate = val => {
        addParForm.handleChange({ target: { name: 'epiManagerDate', value: val } });
        props.setForm(addParForm?.values);
    };
    const onChangeReceiveDate = val => {
        addParForm.handleChange({ target: { name: 'receivedDate', value: val } });
        props.setForm(addParForm?.values);
    };
    const onChangeContact = async val => {
        addParForm.handleChange({ target: { name: 'contact', value: val } });
        await props.setForm(addParForm?.values);
    };
    const { currentStep, handleNext, handlePrev, form } = props;
    return (
        <View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 15,
                }}>
                <Text
                    style={{ textAlign: 'center', fontSize: 16, fontFamily: fonts.BOLD }}>
                    {t('Part 3 of 3')}
                </Text>

                <Text
                    style={{
                        textAlign: 'center',
                        fontSize: 14,
                        fontFamily: fonts.BOLD,
                        marginTop: 10,
                    }}>
                    {t('Name and Signature')}
                </Text>
                <View style={{ marginTop: 10 }} />
                <InputField
                    placeholder={t('Enter here')}
                    label={t('Authorized Inspection Supervisor')}
                    inputValue={addParForm?.values?.inspectionSupervisorName}
                    setInputValue={addParForm.handleChange('inspectionSupervisorName')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addParForm?.errors?.inspectionSupervisorName}
                    mandatory={isMandatory}
                />
                {/* <View style={{ borderWidth: 0.5, height: DeviceHeight / 10, borderRadius: 10, justifyContent: 'center' }}>

                    {addParForm?.values?.inspectionSign ?
                        <TouchableOpacity onPress={() => showModal()}>
                            <Image source={{ uri: addParForm?.values?.inspectionSign }} style={{ height: DeviceHeight / 10, width: DeviceWidth / 1.07, justifyContent: 'center', borderRadius: 10, }} />
                        </TouchableOpacity>
                        : <TouchableOpacity onPress={() => showModal()}>
                            <Text style={{ textAlign: 'center', fontWeight: 14, color: Colors.green4e }}>Upload Sign</Text>

                        </TouchableOpacity>}

                </View>
                {isMandatory && addParForm?.errors?.inspectionSign
                    && <Text
                        style={{
                            color: 'red',
                            fontSize: 12,
                            marginTop: 7,
                            fontFamily: fonts.MEDIUM
                        }}>{addParForm?.errors?.inspectionSign}</Text>
                } */}
                <DatePickerComponent
                    label={t('Authorized Inspection Date')}
                    placeholder="DD/MM/YYYY"
                    onChange={val => onChangeInspectionDate(val)}
                    val={addParForm?.values?.inspectionSupervisorDate}
                    errorMsg={addParForm?.errors?.inspectionSupervisorDate}
                    mandatory={isMandatory}
                />
                <InputField
                    placeholder={t('Enter here')}
                    label={t('Central store or EPI Manager')}
                    inputValue={addParForm?.values?.epiManagerName}
                    setInputValue={addParForm.handleChange('epiManagerName')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addParForm?.errors?.epiManagerName}
                    mandatory={isMandatory}
                />
                {/* <View style={{ borderWidth: 0.5, height: DeviceHeight / 10, borderRadius: 10, justifyContent: 'center' }}>

                    {addParForm?.values?.epiSign ? <TouchableOpacity onPress={() => showEpiModal()}>
                        <Image source={{ uri: addParForm?.values?.epiSign }} style={{ height: DeviceHeight / 10, width: DeviceWidth / 1.07, justifyContent: 'center', borderRadius: 10, }} />
                    </TouchableOpacity>
                        : <TouchableOpacity onPress={() => showEpiModal()}>
                            <Text style={{ textAlign: 'center', fontWeight: 14, color: Colors.green4e }}>Upload Sign</Text>

                        </TouchableOpacity>}

                </View>
                {isMandatory && addParForm?.errors?.epiSign
                    && <Text
                        style={{
                            color: 'red',
                            fontSize: 12,
                            marginTop: 7,
                            fontFamily: fonts.MEDIUM
                        }}>{addParForm?.errors?.epiSign}</Text>
                } */}
                <DatePickerComponent
                    label={t('Date')}
                    placeholder="DD/MM/YYYY"
                    onChange={val => onChangeEpiDate(val)}
                    val={addParForm?.values?.epiManagerDate}
                    errorMsg={addParForm?.errors?.epiManagerDate}
                    mandatory={isMandatory}
                />
                <Text
                    style={{
                        color: '#CA3737',
                        fontFamily: fonts.REGULAR,
                        marginBottom: 10,
                    }}>
                    {t('For procurement agency office use only')}
                </Text>
                <DatePickerComponent
                    label={t('Date received by the office')}
                    placeholder="DD/MM/YYYY"
                    onChange={val => onChangeReceiveDate(val)}
                    val={addParForm?.values?.receivedDate}
                    errorMsg={addParForm?.errors?.receivedDate}
                    mandatory={isMandatory}
                />

                <InputField
                    placeholder={t('Enter here')}
                    label={t('Contact person')}
                    inputValue={addParForm?.values?.contact}
                    setInputValue={onChangeContact}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addParForm?.errors?.contact}
                    mandatory={isMandatory}
                />
                {/* <Text>{JSON.stringify(addParForm?.values)}</Text> */}

                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    {currentStep > 0 && (
                        <CustomButtonWithBorder
                            buttonName={t('Prev')}
                            onPressButton={handlePrev}
                            bdColor={'#BFBFBF'}
                            textColor={'#20232B'}
                        />
                    )}
                    <CustomButton
                        buttonName={t('Save && continue')}
                        onPressButton={_add}
                        bgColor={'#208196'}
                        textColor={'#fff'}
                    />
                </View>
            </View>
            <ImageUploadPopUp
                isVisible={visible}
                onClose={hideModal}
                handleChoosePhoto={handleChoosePhoto}
                requestCameraPermission={() =>
                    requestCameraPermission('inspectionSign')
                }
            />
            <ImageUploadPopUp
                isVisible={epiSignVisible}
                onClose={hideEpiModal}
                handleChoosePhoto={handleEPIChoosePhoto}
                requestCameraPermission={() => requestCameraPermission('epiSign')}
            />
        </View>
    );
};

export const TextLineComponent = props => {
    const { leftText = '', rightText = '' } = props;
    return (
        <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 0.5 }}>
                <Text style={{ fontSize: 14, fontFamily: fonts.REGULAR }}>
                    {leftText}
                </Text>
            </View>
            <View style={{ flex: 0.5 }}>
                <Text
                    style={{ fontSize: 14, fontFamily: fonts.REGULAR, color: '#7F7F7F' }}>
                    {rightText}
                </Text>
            </View>
        </View>
    );
};



export const ParReviewComponent = props => {
    const { t, i18n } = useTranslation();

    const {
        form1 = {},
        form2 = {},
        form3 = {},
        handleNext,
        handlePrev,
        localLoader = false,
    } = props;

    return (
        <View style={{ margin: 15, marginTop: 5 }}>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    marginBottom: 10,
                    textAlign: 'centers',
                }}>
                {t('General Information')}
            </Text>

            <ReViewParPart1 form={form1} />
            <ReViewParPart2 form={form2} />

            <ReViewParPart3 form={form3} />

            {localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : (
                handleNext && (
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <CustomButtonWithBorder
                            buttonName={t('Edit')}
                            onPressButton={handlePrev}
                            bdColor={'#BFBFBF'}
                            textColor={'#20232B'}
                        />
                        <CustomButton
                            buttonName={t('Submit')}
                            onPressButton={handleNext}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>
                )
            )}
        </View>
    );
};
export const ReViewParPart1 = props => {
    const { t, i18n } = useTranslation();

    const { form = {} } = props;
    const { products = [] } = form;
    const eachProd = item => {
        return (
            <View
                style={{
                    marginTop: 10,
                    backgroundColor: Colors.blue96,
                    paddingHorizontal: 10,
                    paddingBottom: 7,
                    borderRadius: 10,
                }}>
                <TextLineComponent leftText={t('Lot Number')} rightText={item.batchNo} />
                {/* <TextLineComponent
                leftText='No. of Boxes'
                rightText={item.noOfBoxes}
            /> */}
                <TextLineComponent leftText={t('No. of units')} rightText={item.noOfUnits} />
                <TextLineComponent
                    leftText={t('Mfg Date')}
                    rightText={formatDateDDMMYYYY(item.mfgDate)}
                />
                <TextLineComponent
                    leftText={t('Exp Date')}
                    rightText={formatDateDDMMYYYY(item.expDate)}
                />
            </View>
        );
    };
    return (
        <View>
            <TextLineComponent
                leftText={t('Product Name')}
                rightText={form.productName}
            />
            <TextLineComponent
                leftText={t('Manufacturer')}
                rightText={form.manufacturerName}
            />
            <TextLineComponent
                leftText={t('Invoice Amount')}
                rightText={form.invoiceAmount}
            />
            <TextLineComponent
                leftText={t('Date of Invoice')}
                rightText={formatDateDDMMYYYY(form.invoiceDate)}
            />
            <TextLineComponent
                leftText={t('Date of Shipment Received')}
                rightText={formatDateDDMMYYYYTime(form.recieveDateTime)}
            />
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 15,
                    fontFamily: fonts.BOLD,
                    margin: 10,
                }}>
                {t('Part 1 :Lot Details')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Purchase Order Number')}
                    rightText={form.poNo}
                />

                <TextLineComponent leftText={t('Country')} rightText={form.country} />
                <TextLineComponent
                    leftText={t('was quantity received as per shipping notification?')}
                    rightText={form.shortShipmentNotification ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={
                        t('if not, were details of short-shipment provided prior to Product arrival?')
                    }
                    rightText={form.shortShipment ? t('Yes') : t('No')}
                />

                <Text
                    style={{
                        fontSize: 16,
                        fontFamily: fonts.MEDIUM,
                        marginTop: 10,
                        color: Colors.blue1EA,
                    }}>
                    {t('Products')}
                </Text>

                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={products}
                    renderItem={({ item }) => eachProd(item)}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        </View>
    );
};
export const ReViewParPart2 = props => {
    const { t, i18n } = useTranslation();

    const { form = {} } = props;

    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.BOLD,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 2: Documents')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent leftText={t('Report No. ')} rightText={form.reportNo} />
                <TextLineComponent
                    leftText={t('Report Date')}
                    rightText={formatDateDDMMYYYY(form.reportDate)}
                />
                <TextLineComponent
                    leftText={t('Invoice')}
                    rightText={form.invoice ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Packing List')}
                    rightText={form.packingList ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Product Arrival Report')}
                    rightText={form.vaccineArrivalReport ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Release certificate')}
                    rightText={form.releaseCertificate ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('What was the condition of the boxes on arrival?')}
                    rightText={form.type}
                />
                <TextLineComponent
                    leftText={t('Were necessary labels attached to the shipping boxes?')}
                    rightText={form.labelsAttached ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Other comments including description of alarms in electronic devices')}
                    rightText={form.comment}
                />
            </View>
        </View>
    );
};
export const ReViewParPart3 = props => {
    const { t, i18n } = useTranslation();

    const { form = {} } = props;
    const { inspectionSupervisor = {}, epiManager = {} } = form;
    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.BOLD,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 3: Name and Signature')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Authorized Inspection Supervisor')}
                    rightText={form.inspectionSupervisorName || inspectionSupervisor.name}
                />
                <TextLineComponent
                    leftText={t('Authorized Inspection Date')}
                    rightText={
                        form.inspectionSupervisorDate
                            ? formatDateDDMMYYYY(form.inspectionSupervisorDate)
                            : formatDateDDMMYYYY(inspectionSupervisor.date)
                    }
                />
                <TextLineComponent
                    leftText={t('Central store or EPI Manager')}
                    rightText={form.epiManagerName || epiManager.name}
                />
                <TextLineComponent
                    leftText={t('Date')}
                    rightText={
                        form.epiManagerDate
                            ? formatDateDDMMYYYY(form.epiManagerDate)
                            : formatDateDDMMYYYY(epiManager.date)
                    }
                />
                <TextLineComponent leftText={t('Contact')} rightText={form.contact} />
            </View>
        </View>
    );
};
export const ParViewComponent = props => {
    const { t, i18n } = useTranslation();

    const { form1 = {} } = props;
    let {
        documents = {},
        conditions = {},
        signatures = {},
        shipmentDetails = {},
    } = form1;

    return (
        <View style={{ margin: 15, marginTop: 5 }}>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    marginBottom: 10,
                    textAlign: 'centers',
                }}>
                {t('General Information')}
            </Text>

            <ViewParPart1 form={form1} shipmentDetails={shipmentDetails} />
            <ViewParPart2
                documents={documents}
                conditions={conditions}
                reportNo={form1.reportNo}
                reportDate={form1.reportDate}
            />

            {/* {handleNext ? <ViewPart4
            form={form4}
            productName={productName}
        /> :
            <ViewPart4InView
                form={form4}
            />} */}

            <ViewParPart3 form={signatures} />
        </View>
    );
};
export const ViewParPart1 = props => {
    const { t, i18n } = useTranslation();

    const { form = {}, shipmentDetails = {} } = props;
    const { products = [] } = shipmentDetails;
    const { shippingIndicators = {} } = form;

    let mfgDetails = products.length ? products[0] : {};

    const { manufacturer = {}, productDetails = {} } = mfgDetails;
    const eachProd = item => {
        return (
            <View
                style={{
                    marginTop: 10,
                    backgroundColor: Colors.blue96,
                    paddingHorizontal: 10,
                    paddingBottom: 7,
                    borderRadius: 10,
                }}>
                <TextLineComponent leftText={t('Lot Number')} rightText={item.batchNo} />
                {/* <TextLineComponent
                leftText='No. of Boxes'
                rightText={item.noOfBoxes}
            /> */}
                <TextLineComponent leftText={t('No. of units')} rightText={item.noOfUnits} />
                <TextLineComponent
                    leftText={t('Mfg Date')}
                    rightText={formatDateDDMMYYYY(item.mfgDate)}
                />
                <TextLineComponent
                    leftText={t('Exp Date')}
                    rightText={formatDateDDMMYYYY(item.expDate)}
                />
            </View>
        );
    };
    return (
        <View>
            <TextLineComponent
                leftText={t('Product Name')}
                rightText={productDetails.name}
            />
            <TextLineComponent
                leftText={t('Manufacturer')}
                rightText={manufacturer.name}
            />
            <TextLineComponent
                leftText={t('Invoice Amount')}
                rightText={shippingIndicators.invoiceAmount}
            />
            <TextLineComponent
                leftText={t('Date of Invoice')}
                rightText={formatDateDDMMYYYY(form.invoiceDate)}
            />
            <TextLineComponent
                leftText={t('Shipment Received')}
                rightText={formatDateDDMMYYYYTime(form.recieveDateTime)}
            />
            <Text
                style={{
                    textAlign: 'center',
                    fontSize: 14,
                    fontFamily: fonts.BOLD,
                    margin: 10,
                }}>
                {t('Part 1 :Lot Details')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Purchase Order Number')}
                    rightText={shipmentDetails.poNo}
                />

                <TextLineComponent
                    leftText={t('Country')}
                    rightText={shipmentDetails.originCountry}
                />
                <TextLineComponent
                    leftText={t('was quantity received as per shipping notification?')}
                    rightText={shipmentDetails.shortShipmentNotification ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={
                        t('if not, were details of short-shipment provided prior to Product arrival?')
                    }
                    rightText={shipmentDetails.shortShipment ? t('Yes') : t('No')}
                />

                <Text
                    style={{
                        fontSize: 16,
                        fontFamily: fonts.MEDIUM,
                        marginTop: 10,
                        color: Colors.blue1EA,
                    }}>
                    {t('Products')}
                </Text>

                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={products}
                    renderItem={({ item }) => eachProd(item)}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        </View>
    );
};
export const ViewParPart2 = props => {
    const { form = {}, reportNo = '', reportDate = '', conditions = {} } = props;
    const { t, i18n } = useTranslation();

    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.BOLD,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 2: Documents')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent leftText={t('Report No.')} rightText={reportNo} />
                <TextLineComponent
                    leftText={t('Report Date')}
                    rightText={formatDateDDMMYYYY(reportDate)}
                />
                <TextLineComponent
                    leftText={t('Invoice')}
                    rightText={form.invoice ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Packing List')}
                    rightText={form.packingList ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Product Arrival Report')}
                    rightText={form.vaccineArrivalReport ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Release certificate')}
                    rightText={form.releaseCertificate ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('What was the condition of the boxes on arrival?')}
                    rightText={conditions.type}
                />
                <TextLineComponent
                    leftText={t('Were necessary labels attached to the shipping boxes?')}
                    rightText={conditions.labelsAttached ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Other comments including description of alarms in electronic devices')}
                    rightText={conditions.comment}
                />
            </View>
        </View>
    );
};
export const ViewParPart3 = props => {
    const { t, i18n } = useTranslation();

    const { form = {} } = props;
    const { inspectionSupervisor = {}, epiManager = {} } = form;
    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.BOLD,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 3: Name and Signature')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Authorized Inspection Supervisor')}
                    rightText={form.inspectionSupervisorName || inspectionSupervisor.name}
                />
                <TextLineComponent
                    leftText={t('Date')}
                    rightText={
                        form.inspectionSupervisorDate
                            ? formatDateDDMMYYYY(form.inspectionSupervisorDate)
                            : formatDateDDMMYYYY(inspectionSupervisor.date)
                    }
                />
                <TextLineComponent
                    leftText={t('Central store or EPI Manager')}
                    rightText={form.epiManagerName || epiManager.name}
                />
                <TextLineComponent
                    leftText={t('Date')}
                    rightText={
                        form.epiManagerDate
                            ? formatDateDDMMYYYY(form.epiManagerDate)
                            : formatDateDDMMYYYY(epiManager.date)
                    }
                />
                <TextLineComponent leftText={t('Contact')} rightText={form.contact} />
            </View>
        </View>
    );
};
