/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import { View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';
import { useRoute } from '@react-navigation/native';
import { ToastShow } from '../../components/Toast';
import { useIsFocused } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ParReviewComponent } from './addParHelper';
import { create_Par } from '../../redux/action/par';
import { PopupMessage } from '../../components/popUp';
import { localImage } from '../../config/global';
import { VirtualizedList } from '../inventory/AddInventory/productHelper';
import { NetworkUtils } from '../../Util/utils';
import { insertNewParlocal } from '../../databases/realmConnection';

const ReviewVarComponent = props => {
    const { t } = useTranslation();
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [isPopupVisible, setPopupVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('');
    const route = useRoute();
    const { params } = route;
    const { form1 = {}, form2 = {}, form3 = {} } = params;
    const { productName = '' } = form1;
    useEffect(() => {
        async function fetchDeta() { }
        fetchDeta();
    }, []);

    const handleNext = async () => {
        const { products = [] } = form1;
        setLocalLoader(true);

        const newProductList = products.map(each => {
            (each.productId = form1.productId),
                (each.manufacturerId = form1.manufacturerId), // manufacturerId
                (each.noOfUnits = Number(each.noOfUnits)),
                // each.noOfBoxes = Number(each.noOfBoxes),
                (each.quantityPerUnit = Number(form1.quantityPerUnit));

            return each;
        });

        let parBody = {
            inventoryId: form1.inventoryId,
            country: form1.country,
            reportNo: form2.reportNo,
            reportDate: form2.reportDate,
            inspectionDateTime: form1.invoiceDate,

            enteredDateTime: form1.recieveDateTime,
            shipmentDetails: {
                poNo: form1.poNo,
                doses: form1.doses,
                consignee: form1.consignee,
                originCountry: form1.country,
                shortShipment: form1.shortShipment,
                shortShipmentNotification: form1.shortShipmentNotification,
                comments: 'PAR Shipment comments',
                products: newProductList,
            },
            documents: {
                invoice: form2.invoice,
                packingList: form2.packingList,
                vaccineArrivalReport: form2.vaccineArrivalReport,
                releaseCertificate: form2.releaseCertificate,
                comment: form2.comment,
            },

            conditions: {
                type: form2.type,
                labelsAttached: form2.labelsAttached,
                comment: form2.comment,
            },
            signatures: {
                inspectionSupervisor: {
                    name: form3.inspectionSupervisorName,
                    sign: form3.inspectionSupervisorSign || 'key',
                    date: form3.inspectionSupervisorDate,
                },
                epiManager: {
                    name: form3.epiManagerName,
                    sign: form3.epiManagerSign || 'key',
                    date: form3.epiManagerDate,
                },
                receivedDate: form3.receivedDate,
                contact: form3.contact,
            },
            shippingIndicators: {
                noOfInspected: 45,
                invoiceAmount: Number(form1.invoiceAmount),
            },
        };

        console.log(' create_Par res varbody' + JSON.stringify(parBody));
        const isConnected = await NetworkUtils.isNetworkAvailable();
        let result = {};
        if (isConnected) {
            result = await props.create_Par(parBody);
        } else {
            result = await insertNewParlocal(parBody);
        }
        setLocalLoader(false);

        console.log(' create_Par res result' + JSON.stringify(result));
        if (result?.status === 200) {
            // const orderID = result.data && result.data.data && result.data.data.id || ''
            const msg = 'par Created ';
            setSuccessMsg(msg);

            setPopupVisible(true);
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };
    const handlePrev = () => {
        // props.navigation.navigate('var')
        props.navigation.goBack();
    };
    const closPopUp = () => {
        setPopupVisible(false);
        props.navigation.navigate('par');
    };
    console.log('val ' + JSON.stringify(form3));
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Product Arrival Report Review')}
            />
            <PopupMessage
                // message={successMsg}
                title={t('Par has been created Successfully')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Okay')}
                image={localImage.congrats}
            />
            <VirtualizedList>
                <ParReviewComponent
                    handlePrev={handlePrev}
                    handleNext={handleNext}
                    form1={form1}
                    form2={form2}
                    form3={form3}
                    localLoader={localLoader}
                />
            </VirtualizedList>
        </View>
    );
};

function mapStateToProps(state) {
    return {};
}
export default connect(mapStateToProps, {
    create_Par,
})(ReviewVarComponent);
