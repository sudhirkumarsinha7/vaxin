/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useRef } from 'react';
import {
    View,
    ScrollView,
    StyleSheet,
} from 'react-native';
import { DeviceWidth } from '../../config/global';
import { AddParPart, AddParPart1, AddParPart2 } from './addParHelper';
import { connect } from 'react-redux';
import {
    getAirportList,
} from '../../redux/action/var';

const ProgressStepper = props => {
    const { steps, initialStep = 0, navigation } = props;
    const [currentStep, setCurrentStep] = useState(initialStep);
    const [manufaturerList, setManufatureList] = useState([]);
    const [productName, setPrdName] = useState([]);
    const [perUnitList, setPerUnitList] = useState([]);

    const reportNum =
        'VAR-BANA-' +
        new Date().getFullYear() +
        '-' +
        (new Date().getMonth() + 1) +
        '-' +
        Math.floor(Math.random() * 10);
    const [form1, setForm1] = useState({});
    const [form2, setForm2] = useState({});
    const [form3, setForm3] = useState({});

    const scrollViewRef = useRef(null);

    const handleNext = () => {
        if (currentStep < steps - 1) {
            setCurrentStep(prevStep => prevStep + 1);
        } else {
            navigation.navigate('review_par', { form1, form2, form3 });
        }
    };

    const handlePrev = () => {
        if (currentStep > 0) {
            setCurrentStep(prevStep => prevStep - 1);
        }
    };

    const { inv_list = [] } = props;
    return (
        <View style={styles.container}>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingHorizontal: 16,
                    alignItems: 'center',
                    marginBottom: 20,
                }}>
                {Array.from({ length: steps }, (_, index) => (
                    <View
                        key={index}
                        style={[
                            styles.step,
                            index <= currentStep && styles.completedStep,
                            index === currentStep && styles.nextStep,
                        ]}
                        activeOpacity={0.8}
                    />
                ))}
                <View
                    style={[styles.line, { width: DeviceWidth / 1.1, marginLeft: 20 }]}
                />
            </View>

            <ScrollView
                ref={scrollViewRef}
                // horizontal
                // pagingEnabled
                showsHorizontalScrollIndicator={false}
                scrollEventThrottle={16}
            // onScroll={(event) => {
            //     const offset = event.nativeEvent.contentOffset.x;
            //     const step = Math.floor(offset / 300);
            //     setCurrentStep(step);
            // }}
            >
                {currentStep === 0 && (
                    <AddParPart
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        handleNext={handleNext}
                        country_list={props.country_list}
                        // getAirports={getAirports}
                        form={form1}
                        setForm={setForm1}
                        manufaturerList={manufaturerList}
                        setManufatureList={setManufatureList}
                        setPrdName={setPrdName}
                        perUnitList={perUnitList}
                        setPerUnitList={setPerUnitList}
                        inv_list={inv_list}
                        inv_product_list={props.inv_product_list}
                    />
                )}

                {currentStep === 1 && (
                    <AddParPart1
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        manufaturerList={manufaturerList}
                        perUnitList={perUnitList}
                        setPerUnitList={setPerUnitList}
                        country_list={props.country_list}
                        form={form2}
                        inv_list={inv_list}
                        setForm={setForm2}
                        productName={productName}
                        userLocation={props.userLocation}
                        handleNext={handleNext}
                    />
                )}

                {currentStep === 2 && (
                    <AddParPart2
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        form={form3}
                        setForm={setForm3}
                        handleNext={handleNext}
                    />
                )}
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: '#fff',
    },
    stepContainer: {
        width: 300,
        justifyContent: 'center',
        alignItems: 'center',
    },
    stepText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        marginTop: 20,
    },
    button: {
        backgroundColor: 'blue',
        padding: 10,
        borderRadius: 5,
        flex: 1,
        marginHorizontal: 8,
    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
    },
    step: {
        width: 10,
        height: 10,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#208196',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    completedStep: {
        borderColor: '#208196',
        backgroundColor: '#208196',
    },
    nextStep: {

        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 2,
        alignItems: 'center',
        borderColor: '#208196',
        backgroundColor: '#208196',
    },
    line: {
        position: 'absolute',
        height: 2,
        backgroundColor: '#208196',
        zIndex: -1,
    },
});

function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        airport_list: state.Var.airport_list,
        inv_product_list: state.inventory.inv_product_list,
        userLocation: state.auth.userLocationList,
        inv_list: state.inventory.inv_list,
    };
}
export default connect(mapStateToProps, {
    getAirportList,
})(ProgressStepper);
