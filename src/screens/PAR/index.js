/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';
import Empty_Card from '../../components/Empty_Card';

import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { useIsFocused } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import {
    getProdList,
    getInventoryLocationList,
} from '../../redux/action/inventory';
import { getUserLocationList } from '../../redux/action/auth';
import { TextLineComponent } from './addParHelper';
import { getCountryList } from '../../redux/action/var';
import { formatDateDDMMYYYY } from '../../Util/utils';
import { getParList } from '../../redux/action/par';
import { NetworkUtils } from '../../Util/utils';
import { Colors } from '../../components/Common/Style';

import {
    queryAllpar,
    queryDeleteAllPar,
} from '../../databases/allSchemas';
import { create_Par } from '../../redux/action/par';
export const removeNullValues = obj => {
    for (const key in obj) {
        if (obj[key] === null) {
            delete obj[key];
        } else if (typeof obj[key] === 'object') {
            removeNullValues(obj[key]);
        }
    }
    return obj;
};
const PARScreen = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [localLoader, setLocalLoader] = useState(false);

    useEffect(() => {
        async function fetchDeta() {
            getData();
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const getData = async () => {
        const isConnected = await NetworkUtils.isNetworkAvailable();
        if (isConnected) {
            await syncParData();

            setLocalLoader(true);
            await props.getCountryList();
            await props.getProdList();
            await props.getUserLocationList();
            await props.getInventoryLocationList();
            await props.getParList();
            setLocalLoader(false);
        } else {
            // await syncParData();
        }
    };
    const syncParData = async () => {
        let data = await queryAllpar();
        // console.log('queryAllpar list ' + JSON.stringify(data));

        if (data.length) {
            for (var i = 0; i < data.length; i++) {
                // Remove null values from the data array
                const filteredData = JSON.parse(
                    JSON.stringify(data[i], (key, value) => {
                        if (value === null) { return undefined; } // Remove null values during stringification
                        return value;
                    }),
                );
                // console.log('filteredData  ' + JSON.stringify(filteredData));

                await props.create_Par(filteredData);
            }
            await queryDeleteAllPar();
        } else {
        }
    };
    const eachPar = item => {
        let {
            shipmentDetails = {},
        } = item;
        let { products = [] } = shipmentDetails;
        let mfgDetails = products.length ? products[0] : {};

        const { manufacturer = {} } = mfgDetails;
        return (
            <View
                style={{
                    padding: 10,
                    marginHorizontal: 15,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    marginTop: 10,
                    borderRadius: 10,
                }}>
                <TouchableOpacity
                    onPress={() => props.navigation.navigate('view_par', { form1: item })}>
                    <TextLineComponent leftText={t('PAR ID No ')} rightText={item.reportNo} />
                    <TextLineComponent
                        leftText={t('Date of Report')}
                        rightText={formatDateDDMMYYYY(item.reportDate)}
                    />
                    <TextLineComponent
                        leftText={t('Received')}
                        rightText={formatDateDDMMYYYY(item.enteredDateTime)}
                    />
                    <TextLineComponent leftText={t('From')} rightText={manufacturer.name} />
                    <TextLineComponent
                        leftText={t('To')}
                        rightText={shipmentDetails.consignee}
                    />
                </TouchableOpacity>
            </View>
        );
    };
    const loadMorePar = async () => {
        await props.getParList(par_list_page + 1);
    };
    const { Par_list = [], userPermissions = {}, par_list_page = 1 } = props;
    // console.log('Par_list ' + JSON.stringify(Par_list))
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Product Arrival Report')}
            />
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : null}
            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={Par_list}
                onEndReached={loadMorePar}
                onEndReachedThreshold={10}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => eachPar(item, index)}
                ListEmptyComponent={<Empty_Card Text="No items available" />}
            />

            {userPermissions.ADD_PAR && (
                <View
                    style={{
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        marginRight: scale(15),
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 10,
                        }}>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('create_par')}
                            style={{
                                backgroundColor: Colors.whiteFF,
                                borderRadius: scale(42),
                            }}>
                            <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    );
};

function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        airport_list: state.Var.airport_list,
        userLocation: state.auth.userLocation,
        Par_list: state.Par.par_list,
        loder: state.loder,
        userPermissions: state.auth.userPermissions,
        par_list_page: state.Par.par_list_page
    };
}
export default connect(mapStateToProps, {
    getCountryList,
    getProdList,
    getUserLocationList,
    getInventoryLocationList,
    getParList,
    create_Par,
})(PARScreen);
