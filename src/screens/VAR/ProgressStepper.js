/* eslint-disable prettier/prettier */
import React, { useState, useRef } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    StyleSheet,
    Animated,
} from 'react-native';
import { DeviceWidth } from '../../config/global';
import {
    AddVarPart1,
    AddVarPart2,
    AddVarPart3,
    AddVarPart4,
    AddVarPart5,
    AddVarPart6,
    AddVarPart7,
    AddVarPart8,
    VarReviewComponent,
} from './addVarHelper';
import { connect } from 'react-redux';
import {
    getCountryList,
    getAirportList,
    getVarList,
    getVarInfo,
    create_Var,
} from '../../redux/action/var';
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';

const ProgressStepper = props => {
    const { steps, initialStep = 0, navigation } = props;
    const [currentStep, setCurrentStep] = useState(initialStep);
    const [manufaturerList, setManufatureList] = useState([]);
    const [productName, setPrdName] = useState([]);
    const [perUnitList, setPerUnitList] = useState([]);

    const reportNum =
        'VAR-BANA-' +
        new Date().getFullYear() +
        '-' +
        (new Date().getMonth() + 1) +
        '-' +
        Math.floor(Math.random() * 10);
    const [form1, setForm1] = useState({});
    const [form2, setForm2] = useState({});
    const [form3, setForm3] = useState({});
    const [form4, setForm4] = useState({});
    const [form5, setForm5] = useState({});
    const [form6, setForm6] = useState({});
    const [form7, setForm7] = useState({});
    const [form8, setForm8] = useState({});

    const scrollViewRef = useRef(null);

    const handleNext = () => {
        if (currentStep < steps - 1) {
            setCurrentStep(prevStep => prevStep + 1);
        } else {
            navigation.navigate('review_var', {
                form1,
                form2,
                form3,
                form4,
                form5,
                form6,
                form7,
                form8,
            });
        }
    };

    const handlePrev = () => {
        if (currentStep > 0) {
            setCurrentStep(prevStep => prevStep - 1);
        }
    };
    const getAirports = async id => {
        await props.getAirportList(id);
    };
    const { inv_list = [] } = props;
    return (
        <View style={styles.container}>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingHorizontal: 16,
                    alignItems: 'center',
                    marginBottom: 20,
                }}>
                {Array.from({ length: steps }, (_, index) => (
                    <View
                        key={index}
                        style={[
                            styles.step,
                            index <= currentStep && styles.completedStep,
                            index === currentStep && styles.nextStep,
                        ]}
                        activeOpacity={0.8}
                    />
                ))}
                <View
                    style={[styles.line, { width: DeviceWidth / 1.1, marginLeft: 20 }]}
                />
            </View>

            <ScrollView
                ref={scrollViewRef}
                // horizontal
                // pagingEnabled
                showsHorizontalScrollIndicator={false}
                scrollEventThrottle={16}
            // onScroll={(event) => {
            //     const offset = event.nativeEvent.contentOffset.x;
            //     const step = Math.floor(offset / 300);
            //     setCurrentStep(step);
            // }}
            >
                {/* <AddVarPart6
                    currentStep={currentStep}
                    handlePrev={handlePrev}
                    manufaturerList={manufaturerList}

                    form={form4}
                    setForm={setForm4}
                    userLocation={props.userLocation}
                    handleNext={handleNext} /> */}
                {/* <Text>{JSON.stringify(manufaturerList)}</Text> */}
                {currentStep === 0 && (
                    <AddVarPart1
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        handleNext={handleNext}
                        country_list={props.country_list}
                        getAirports={getAirports}
                        form={form1}
                        setForm={setForm1}
                        manufaturerList={manufaturerList}
                        setManufatureList={setManufatureList}
                        setPrdName={setPrdName}
                        inv_product_list={props.inv_product_list}
                    />
                )}
                {currentStep === 1 && (
                    <AddVarPart2
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        form={form2}
                        setForm={setForm2}
                        handleNext={handleNext}
                    />
                )}
                {currentStep === 2 && (
                    <AddVarPart3
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        handleNext={handleNext}
                        form={form3}
                        setForm={setForm3}
                        airport_list={props.airport_list}
                    />
                )}
                {currentStep === 3 && (
                    <AddVarPart4
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        manufaturerList={manufaturerList}
                        perUnitList={perUnitList}
                        setPerUnitList={setPerUnitList}
                        country_list={props.country_list}
                        form={form4}
                        inv_list={inv_list}
                        setForm={setForm4}
                        productName={productName || form1.productName}
                        userLocation={props.userLocation}
                        handleNext={handleNext}
                    />
                )}
                {currentStep === 4 && (
                    <AddVarPart5
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        form={form5}
                        setForm={setForm5}
                        handleNext={handleNext}
                    />
                )}
                {currentStep === 5 && (
                    <AddVarPart6
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        form={form6}
                        setForm={setForm6}
                        handleNext={handleNext}
                        form4={form4}
                    />
                )}
                {currentStep === 6 && (
                    <AddVarPart7
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        form={form7}
                        setForm={setForm7}
                        handleNext={handleNext}
                    />
                )}
                {currentStep === 7 && (
                    <AddVarPart8
                        currentStep={currentStep}
                        handlePrev={handlePrev}
                        form={form8}
                        setForm={setForm8}
                        handleNext={handleNext}
                    />
                )}
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 20,
        backgroundColor: '#fff',
    },
    stepContainer: {
        width: 300,
        justifyContent: 'center',
        alignItems: 'center',
    },
    stepText: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 16,
        marginTop: 20,
    },
    button: {
        backgroundColor: 'blue',
        padding: 10,
        borderRadius: 5,
        flex: 1,
        marginHorizontal: 8,
    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
    },
    step: {
        width: 10,
        height: 10,
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#208196',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
    },
    completedStep: {
        borderColor: '#208196',
        backgroundColor: '#208196',
    },
    nextStep: {
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 2,
        alignItems: 'center',
        borderColor: '#208196',
        backgroundColor: '#208196',
    },
    line: {
        position: 'absolute',
        height: 2,
        backgroundColor: '#208196',
        zIndex: -1,
    },
});

function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        airport_list: state.Var.airport_list,
        inv_product_list: state.inventory.inv_product_list,
        userLocation: state.auth.userLocationList,
        inv_list: state.inventory.inv_list,
    };
}
export default connect(mapStateToProps, {
    getAirportList,
})(ProgressStepper);
