/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react';
import {
    View,
} from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';
import ProgressStepper from './ProgressStepper';
import { getProdList } from '../../redux/action/inventory';
import { getUserLocationList } from '../../redux/action/auth';

import {
    getCountryList,
    getVarList,
} from '../../redux/action/var';
const VARScreen = props => {
    const { t } = useTranslation();

    useEffect(() => {
        async function fetchDeta() {
            getData();
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const getData = async () => {
        await props.getCountryList();
        await props.getVarList();
        await props.getProdList();
        await props.getUserLocationList();
    };

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack navigation={props.navigation} name={t('Create Var')} />
            <ProgressStepper steps={8} navigation={props.navigation} />
        </View>
    );
};

function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        airport_list: state.Var.airport_list,
    };
}
export default connect(mapStateToProps, {
    getCountryList,
    getVarList,
    getProdList,
    getUserLocationList,
})(VARScreen);
