/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    Keyboard,
    View,
    Text,
    TouchableOpacity,
    Platform,
    PermissionsAndroid,
    FlatList,
    ActivityIndicator,
} from 'react-native';
import { scale } from '../../components/Scale';
import { useTranslation } from 'react-i18next';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import DropDown, { ManufacturerDropdown } from '../../components/Dropdown';
import {
    InputField,
    CustomButtonWithBorder,
    CustomButton,
} from '../../components/Common/helper';
import {
    DatePickerComponent,
    DateTimePickerComponent,
} from '../../components/DatePicker';
import fonts from '../../../FontFamily';
import { ToastShow } from '../../components/Toast';
import RadioButtonGroup from '../../components/RadioButton';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
    selectPhotoFromGallary,
    takePhotoFromCamera,
} from '../../components/ImageHelper';
import { filterUniqueObjectsByProperty } from '../inventory/AddInventory/productUtil';
import { Colors } from '../../components/Common/Style';
import { ImageUploadPopUp } from '../../components/popUp';
import { formatDateDDMMYYYY, formatDateDDMMYYYYTime } from '../../Util/utils';
import CheckBox from '../../components/checkbox';
const options = [
    { id: true, label: 'Yes' },
    { id: false, label: 'No' },
];
const options1 = [
    { id: 'Dry Ice', label: 'Dry Ice' },
    { id: 'Ice Packs', label: 'Ice Packs' },
    { id: 'Gel Packs', label: 'Gel Packs' },
    { id: 'No Coolants', label: 'No Coolants' },

];
const options2 = [
    { id: 'VVM', label: 'VVM' },
    { id: 'Electronic Device', label: 'Electronic Device' },
    { id: 'Fridge Tag', label: 'Fridge Tag' },
    { id: 'Freeze Tag', label: 'Freeze Tag' },
    { id: 'Temp Logger', label: 'Temp Logger' },

];
const options3 = [
    { id: 45.0, label: '>= 45 C' },
    { id: 30.0, label: '>= 30 C' },
    { id: 10.0, label: '>= 10 C' },
    { id: -5.0, label: '>= -5 C' },
];
const options4 = [
    { id: '1', label: '1' },
    { id: '2', label: '2' },
    { id: '3', label: '3' },
    { id: '4', label: '4' },
];
export const getLabel = (options1 = [], colentye) => {
    const option = options1.find(opt => opt.id === colentye);
    return option ? option.label : colentye;
};

const AddNewAlert = () => {
    const add = {
        batchNo: '',
        boxNo: '',
        temperature: '',
        vvmStatus: '1',
        inspectionDateTime: '',
    };
    return add;
};
const AddNewProduct = () => {
    const add = {
        noOfBoxes: '',
        mfgDate: '',
        expDate: '',
        batchNo: '',
        noOfUnits: '',
    };
    return add;
};
export const AddVarPart1 = props => {
    const { t } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);

    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        if (form.reportNo) {
            addVarForm1.setValues(form);
        }
    }, [form]);

    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        async function fetchDeta() {
            const data = { value: 'BD', label: 'Bangladesh' };
            await changeAfterSelectCountry(data);
        }
        fetchDeta();
    }, []);
    const reportNum =
        'VAR-BANA-' +
        new Date().getFullYear() +
        '-' +
        (new Date().getMonth() + 1) +
        '-' +
        Math.floor(Math.random() * 10);
    var validationSchema = Yup.object().shape(
        {
            country: Yup.string().required(t('Required')),
            placeOfInspection: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            reportNo: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            reportDate: Yup.string().required(t('Required')),
            inspectionDateTime: Yup.string().required(t('Required')),
            storageName: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            productId: Yup.string().required(t('Required')),
        },
        ['country', 'placeOfInspection'],
    ); // <-- HERE!!!!!!!!
    const addVarForm1 = useFormik({
        initialValues: {
            country: '',
            countryId: '',
            reportNo: reportNum,
            reportDate: new Date(),
            placeOfInspection: 'Central Authority',
            inspectionDateTime: new Date(),
            storageName: '',
            vaccine: '',
            productId: '',
            enteredDateTime: new Date(),
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addVarForm1.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };

    const changeAfterSelectCountry = async item => {
        const { value, label } = item;
        addVarForm1.handleChange({ target: { name: 'country', value: label } });
        addVarForm1.handleChange({ target: { name: 'countryId', value: value } });
        await props.getAirports(value);
    };

    const onChangeReportDate = val => {
        addVarForm1.handleChange({ target: { name: 'reportDate', value: val } });
    };
    const onChangeInspectionDate = val => {
        addVarForm1.handleChange({
            target: { name: 'inspectionDateTime', value: val },
        });
        addVarForm1.handleChange({ target: { name: 'enteredDateTime', value: val } });
    };
    const changeAfterSelectProduct = item => {
        const { value, eachItem } = item;
        console.log('each ' + JSON.stringify(eachItem));
        const { manufacturers = [] } = eachItem;
        addVarForm1.handleChange({ target: { name: 'productId', value: value } });
        addVarForm1.handleChange({
            target: { name: 'productName', value: eachItem.name },
        });
        addVarForm1.handleChange({ target: { name: 'units', value: eachItem.units } });
        if (manufacturers.length) {
            setManufatureList(manufacturers);
            setPrdName(eachItem.name);
        } else {
            ToastShow(t('Please select another Vaccine'), 'error', 'long', 'top');
        }
    };
    const {
        currentStep,
        handleNext,
        handlePrev,
        country_list = [],
        inv_product_list = [],
        setManufatureList,
        form,
        setPrdName,
    } = props;
    const uniqueProduct = filterUniqueObjectsByProperty(inv_product_list, 'name');
    const VaccineList1 = uniqueProduct.filter(each => {
        if (
            each.type === 'Vaccine' ||
            each.type === 'VACCINE' ||
            each.type === 'vaccine'
        ) {
            return each;
        }
    });
    const VaccineList = VaccineList1.sort((a, b) => a.name.localeCompare(b.name));

    return (
        <View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 10,
                }}>
                <View>
                    <View style={{ marginTop: 10 }} />

                    <DropDown
                        dropdownData={VaccineList}
                        onChangeValue={changeAfterSelectProduct}
                        label="name"
                        mapKey="_id"
                        val={addVarForm1?.values?.productId}
                        placeholderText={t('Vaccine')}
                        labelText={t('Vaccine')}
                        changeAfterSelect={() => console.log()}
                        errorMsg={addVarForm1?.errors?.order_id}
                        mandatory={isMandatory}
                    />
                    <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectCountry}
                        label="name"
                        mapKey="_id"
                        val={addVarForm1?.values?.countryId}
                        placeholderText={t('Country')}
                        labelText={t('Country')}
                        errorMsg={addVarForm1?.errors?.countryId}
                        mandatory={isMandatory}
                        search={true}
                    />
                    <InputField
                        placeholder={t('Report No. ')}
                        label={t('Report No. ')}
                        inputValue={addVarForm1?.values?.reportNo}
                        setInputValue={addVarForm1.handleChange('reportNo')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.reportNo}
                        mandatory={isMandatory}
                    />
                    <DatePickerComponent
                        label={t('Date of Report')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeReportDate(val)}
                        val={addVarForm1?.values?.reportDate}
                        errorMsg={addVarForm1?.errors?.reportDate}
                        mandatory={isMandatory}
                    />

                    <View style={{ marginTop: 10 }} />
                    <InputField
                        placeholder={t('Place of Inspection')}
                        label={t('Place of Inspection ')}
                        inputValue={addVarForm1?.values?.placeOfInspection}
                        setInputValue={addVarForm1.handleChange('placeOfInspection')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.placeOfInspection}
                        mandatory={isMandatory}
                        disable={true}
                    />
                    <DateTimePickerComponent
                        label={t('Date of Inspection')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeInspectionDate(val)}
                        val={addVarForm1?.values?.inspectionDateTime}
                        errorMsg={addVarForm1?.errors?.inspectionDateTime}
                        mandatory={isMandatory}
                    />

                    {/* <TimePickerComponent
                        label={"Time of Inspection"}
                        placeholder="00:00"
                        onChange={(val) => onChangeInspectionDate(val)}
                        val={addVarForm1?.values?.inspectionDateTime}
                        errorMsg={addVarForm1?.errors?.inspectionDateTime}
                        mandatory={isMandatory}
                    /> */}
                    <InputField
                        placeholder={t('Cold Storage')}
                        label={t('Cold Storage')}
                        inputValue={addVarForm1?.values?.storageName}
                        setInputValue={addVarForm1.handleChange('storageName')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.storageName}
                        mandatory={isMandatory}
                    />

                    {/* <DateTimePickerComponent
                        label={"Date of Cold storage "}
                        placeholder="DD/MM/YYYY"
                        onChange={(val) => onChangeEnterTime(val)}
                        val={addVarForm1?.values?.enteredDateTime}
                        errorMsg={addVarForm1?.errors?.enteredDateTime}
                        mandatory={isMandatory}
                    /> */}
                    {/* <Text>{JSON.stringify(addVarForm1?.values)}</Text> */}
                    {/* <TimePickerComponent
                        label={"Time of Inspection"}
                        placeholder="00:00"
                        onChange={(val) => onChangeEstShipmentDate(val)}
                        val={addVarForm1?.values?.est_date}
                        errorMsg={addVarForm1?.errors?.est_date}
                        mandatory={isMandatory}
                    /> */}
                    {/* <Text style={{ fontFamily: fonts.REGULAR, fontSize: 12, color: '#7F7F7F' }}>{'#When Vaccine entered the cold store'}</Text> */}

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        {currentStep > 0 && (
                            <CustomButtonWithBorder
                                buttonName={t('Prev')}
                                onPressButton={handlePrev}
                                bdColor={'#BFBFBF'}
                                textColor={'#20232B'}
                            />
                        )}
                        <CustomButton
                            buttonName={t('Save && continue')}
                            onPressButton={_add}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>
                </View>
            </View>
        </View>
    );
};
export const AddVarPart2 = props => {
    const { t } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);

    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        if (form.preAdviceDate) {
            addVarForm1.setValues(form);
        }
    }, [form]);
    var validationSchema = Yup.object().shape(
        {
            preAdviceDate: Yup.string().required(t('Required')),
            shippingNotificationDate: Yup.string().required(t('Required')),
        },
        ['preAdviceDate', 'shippingNotificationDate'],
    ); // <-- HERE!!!!!!!!
    const addVarForm1 = useFormik({
        initialValues: {
            preAdviceDate: new Date(),
            shippingNotificationDate: new Date(),
            copyAwb: true,
            copyInvoice: true,
            copyPackingList: true,
            copyReleaseCertificate: true,
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addVarForm1.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };

    const onChangeAdviceDate = val => {
        addVarForm1.handleChange({ target: { name: 'preAdviceDate', value: val } });
    };
    const onChangeShipingNotification = val => {
        addVarForm1.handleChange({
            target: { name: 'shippingNotificationDate', value: val },
        });
    };

    const { currentStep, handleNext, handlePrev, form } = props;
    return (
        <View>
            <View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20 }}>
                <Text
                    style={{
                        textAlign: 'center',
                        fontSize: 14,
                        fontFamily: fonts.MEDIUM,
                        marginBottom: 15,
                    }}>
                    {t('Part 1: Advance Notice')}
                </Text>
                <View style={{ backgroundColor: '#E0F0EA', padding: 15 }}>
                    <Text
                        style={{ fontSize: 14, fontFamily: fonts.REGULAR, marginBottom: 5 }}>
                        {t('Pre - Advice')}
                    </Text>

                    <DatePickerComponent
                        label={t('Date Received by Consignee')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeAdviceDate(val)}
                        val={addVarForm1?.values?.preAdviceDate}
                        errorMsg={addVarForm1?.errors?.preAdviceDate}
                        mandatory={isMandatory}
                    />
                    <Text
                        style={{ fontSize: 14, fontFamily: fonts.REGULAR, marginBottom: 5 }}>
                        {t('Shipping Notification')}
                    </Text>

                    <DatePickerComponent
                        label={t('Date Received by Consignee')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeShipingNotification(val)}
                        val={addVarForm1?.values?.shippingNotificationDate}
                        errorMsg={addVarForm1?.errors?.shippingNotificationDate}
                        mandatory={isMandatory}
                    />
                </View>
                <View style={{ padding: 15 }}>
                    <RadioButtonGroup
                        label={t('Copy of Airway Bill(Awb)')}
                        options={options}
                        val={addVarForm1?.values?.copyAwb}
                        onChangeValue={val =>
                            addVarForm1.handleChange({ target: { name: 'copyAwb', value: val } })
                        }
                    />
                    <RadioButtonGroup
                        label={t('Copy of Packing List')}
                        options={options}
                        val={addVarForm1?.values?.copyPackingList}
                        onChangeValue={val =>
                            addVarForm1.handleChange({
                                target: { name: 'copyPackingList', value: val },
                            })
                        }
                    />
                    <RadioButtonGroup
                        label={t('Copy of Invoice')}
                        options={options}
                        val={addVarForm1?.values?.copyInvoice}
                        onChangeValue={val =>
                            addVarForm1.handleChange({
                                target: { name: 'copyInvoice', value: val },
                            })
                        }
                    />
                    <RadioButtonGroup
                        label={t('Copy of Release Certificate')}
                        options={options}
                        val={addVarForm1?.values?.copyReleaseCertificate}
                        onChangeValue={val =>
                            addVarForm1.handleChange({
                                target: { name: 'copyReleaseCertificate', value: val },
                            })
                        }
                    />

                    <View style={{ marginTop: 10 }} />
                    {/* <Text>{JSON.stringify(addVarForm1?.values)}</Text> */}

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        {currentStep > 0 && (
                            <CustomButtonWithBorder
                                buttonName={t('Prev')}
                                onPressButton={handlePrev}
                                bdColor={'#BFBFBF'}
                                textColor={'#20232B'}
                            />
                        )}
                        <CustomButton
                            buttonName={t('Save && continue')}
                            onPressButton={_add}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>

                    {/* <AddNewProductOrderComponent productListByType={productListByType} addProductInList={addProductInList} selectedProductType={selectedProductType} /> */}
                </View>
            </View>
        </View>
    );
};
export const AddVarPart3 = props => {
    const { t } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);
    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        if (form.name) {
            addVarForm1.setValues(form);
        }
    }, [form]);
    var validationSchema = Yup.object().shape(
        {
            awb: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            flightNo: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            destination: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            scheduledDateTime: Yup.string().required(t('Required')),
            actualDateTime: Yup.string().required(t('Required')),
            name: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            behalf: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
        },
        ['awb', 'flightNo', 'name'],
    ); // <-- HERE!!!!!!!!
    const addVarForm1 = useFormik({
        initialValues: {
            awb: '',
            flightNo: '',
            destination: '',
            scheduledDateTime: new Date(),
            actualDateTime: new Date(),
            // "clearingAgent": {
            //     "name": "AGENT NAME",
            //     "behalf": "ORG NAME"
            // }
            name: '',
            behalf: 'CMSD- Central Medical Stores Depot',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addVarForm1.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };

    const onChangeScheduleDate = val => {
        addVarForm1.handleChange({ target: { name: 'scheduledDateTime', value: val } });
    };
    const onChangeActualTime = val => {
        addVarForm1.handleChange({ target: { name: 'actualDateTime', value: val } });
    };
    const changeAfterAirportSelect = item => {
        const { value } = item;
        addVarForm1.handleChange({ target: { name: 'destination', value: value } });
    };
    const { currentStep, handleNext, handlePrev, form } = props;
    return (
        <View>
            <View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20 }}>
                <Text
                    style={{ textAlign: 'center', fontSize: 14, fontFamily: fonts.MEDIUM }}>
                    {t('Part 2: Flight Arrival Report')}
                </Text>
                <View style={{ padding: 15 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ flex: 0.48 }}>
                            <InputField
                                placeholder={t('Awb Number')}
                                label={t('Awb Number')}
                                inputValue={addVarForm1?.values?.awb}
                                setInputValue={addVarForm1.handleChange('awb')}
                                inputStyle={{ marginBottom: 10 }}
                                labelStyle={{ marginBottom: 5 }}
                                errorMsg={addVarForm1?.errors?.awb}
                                mandatory={isMandatory}
                                KeyboardType={'numeric'}
                            />
                        </View>
                        <View style={{ flex: 0.48 }}>
                            <InputField
                                placeholder={t('Flight Number')}
                                label={t('Flight Number')}
                                inputValue={addVarForm1?.values?.flightNo}
                                setInputValue={addVarForm1.handleChange('flightNo')}
                                inputStyle={{ marginBottom: 10 }}
                                labelStyle={{ marginBottom: 5 }}
                                errorMsg={addVarForm1?.errors?.flightNo}
                                mandatory={isMandatory}
                            />
                        </View>
                    </View>
                    <DropDown
                        dropdownData={props.airport_list}
                        onChangeValue={changeAfterAirportSelect}
                        label="name"
                        mapKey="name"
                        val={addVarForm1?.values?.destination}
                        placeholderText={t('Airport of Destination')}
                        labelText={t('Airport of Destination')}
                        changeAfterSelect={() => console.log()}
                        errorMsg={addVarForm1?.errors?.destination}
                        mandatory={isMandatory}
                    />
                </View>
                <View style={{ backgroundColor: '#E0F0EA', padding: 15 }}>
                    <Text
                        style={{
                            fontSize: 14,
                            fontFamily: fonts.REGULAR,
                            marginBottom: 5,
                            color: '#208196',
                        }}>
                        {t('ETA as per notification')}
                    </Text>

                    <DateTimePickerComponent
                        label={t('Date')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeScheduleDate(val)}
                        val={addVarForm1?.values?.scheduledDateTime}
                        errorMsg={addVarForm1?.errors?.scheduledDateTime}
                        mandatory={isMandatory}
                    />

                    {/* <TimePickerComponent
                        label={"Time"}
                        placeholder="DD/MM/YYYY"
                        onChange={(val) => onChangeEstShipmentDate(val)}
                        val={addVarForm1?.values?.est_date}
                        errorMsg={addVarForm1?.errors?.est_date}
                        mandatory={isMandatory}
                    /> */}
                </View>
                <View style={{ marginTop: 10 }} />

                <View style={{ backgroundColor: '#E0F0EA', padding: 15 }}>
                    <Text
                        style={{
                            fontSize: 14,
                            fontFamily: fonts.REGULAR,
                            marginBottom: 5,
                            color: '#208196',
                        }}>
                        {t('Actual time of arrival')}
                    </Text>

                    <DateTimePickerComponent
                        label={t('Date')}
                        placeholder="DD/MM/YYYY"
                        onChange={val => onChangeActualTime(val)}
                        val={addVarForm1?.values?.actualDateTime}
                        errorMsg={addVarForm1?.errors?.actualDateTime}
                        mandatory={isMandatory}
                    />

                    {/* <TimePickerComponent
                        label={"Time"}
                        placeholder="DD/MM/YYYY"
                        onChange={(val) => onChangeEstShipmentDate(val)}
                        val={addVarForm1?.values?.est_date}
                        errorMsg={addVarForm1?.errors?.est_date}
                        mandatory={isMandatory}
                    /> */}
                </View>
                <View style={{ padding: 15 }}>
                    <View style={{ marginTop: 10 }} />
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Name of clearing agent')}
                        inputValue={addVarForm1?.values?.name}
                        setInputValue={addVarForm1.handleChange('name')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.name}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('On behalf of')}
                        inputValue={addVarForm1?.values?.behalf}
                        setInputValue={addVarForm1.handleChange('behalf')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.behalf}
                        mandatory={isMandatory}
                        disable={true}
                    />
                    {/* <Text>{JSON.stringify(addVarForm1?.values)}</Text> */}

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        {currentStep > 0 && (
                            <CustomButtonWithBorder
                                buttonName={t('Prev')}
                                onPressButton={handlePrev}
                                bdColor={'#BFBFBF'}
                                textColor={'#20232B'}
                            />
                        )}
                        <CustomButton
                            buttonName={t('Save && continue')}
                            onPressButton={_add}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>
                </View>
            </View>
        </View>
    );
};
export const AddVarPart4 = props => {
    const { t } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);


    useEffect(() => {
        // Use useEffect to update initial values when parent data changes

        if (form.products) {
            addVarForm1.setValues(form);
        }
        if (inv_list.length) {
            addVarForm1.handleChange({
                target: { name: 'inventoryId', value: inv_list[0]._id },
            });
            addVarForm1.handleChange({
                target: { name: 'inventory', value: inv_list[0].name },
            });
        }
        if (
            productName === 'BCG' ||
            productName === 'MR' ||
            productName === 'Measles' ||
            productName === 'Rubella'
        ) {
            if (form.diluent && form.diluent.length) {
            } else {
                const newData = AddNewProduct();
                const finalData = [newData];
                addVarForm1.handleChange({ target: { name: 'diluent', value: finalData } });
            }
        } else {
            addVarForm1.handleChange({ target: { name: 'diluent', value: [] } });
        }

    }, [form]);
    var validationSchema = Yup.object().shape(
        {
            poNo: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            manufacturerId: Yup.string().required(t('Required')),
            comment: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            countryId: Yup.string().required(t('Required')),
            quantityPerUnit: Yup.string().required(t('Required')),

            products: Yup.array().of(
                Yup.object().shape({
                    mfgDate: Yup.string().required(t('Required')),
                    expDate: Yup.string().required(t('Required')),
                    noOfBoxes: Yup.number()
                        .min(1, t('Invalid Qauntity'))
                        .required(t('Required')),
                    batchNo: Yup.string()
                        .required(t('Required'))
                        .trim()
                        .min(2, t('Must be at least 2 characters long'))
                        .test(
                            'no-spaces',
                            t('It must not be empty spaces'),
                            value => value && value.trim() !== '',
                        ),
                    noOfUnits: Yup.string()
                        .required(t('Required'))
                        .trim()
                        .test(
                            'no-spaces',
                            t('It must not be empty spaces'),
                            value => value && value.trim() !== '',
                        ),
                }),
            ),
            diluent: Yup.array().of(
                Yup.object().shape({
                    mfgDate: Yup.string().required(t('Required')),
                    expDate: Yup.string().required(t('Required')),
                    noOfBoxes: Yup.number()
                        .min(1, t('Invalid Qauntity'))
                        .required(t('Required')),
                    batchNo: Yup.string()
                        .required(t('Required'))
                        .trim()
                        .min(2, t('Must be at least 2 characters long'))
                        .test(
                            'no-spaces',
                            t('It must not be empty spaces'),
                            value => value && value.trim() !== '',
                        ),
                    noOfUnits: Yup.string()
                        .required(t('Required'))
                        .trim()
                        .test(
                            'no-spaces',
                            t('It must not be empty spaces'),
                            value => value && value.trim() !== '',
                        ),
                }),
            ),
        },
        ['poNo', 'manufacturerId'],
    ); // <-- HERE!!!!!!!!
    const addVarForm1 = useFormik({
        initialValues: {
            poNo: '',
            doses: '',
            manufacturerId: '', // manufacturerId
            status: '',
            shortShipment: true,
            shortShipmentNotification: true,
            comment: '',
            unit: '',
            consignee: 'CMSD- Central Medical Stores Depot',
            products: [
                {
                    noOfBoxes: '',
                    mfgDate: null,
                    expDate: null,
                    batchNo: '',
                    noOfUnits: '',
                },
            ],
            diluent: [
                {
                    noOfBoxes: '',
                    mfgDate: null,
                    expDate: null,
                    batchNo: '',
                    noOfUnits: '',
                },
            ],
            country: '',
            countryId: '',
            quantityPerUnit: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addVarForm1.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };
    const changeAfterManufature = item => {
        const { value, eachItem, label } = item;
        addVarForm1.handleChange({ target: { name: 'manufacturerId', value: value } });
        addVarForm1.handleChange({
            target: { name: 'manufacturerName', value: label },
        });
        addVarForm1.handleChange({
            target: { name: 'countryId', value: eachItem?.details?.country },
        });
        addVarForm1.handleChange({ target: { name: 'country', value: eachItem?.details?.country } });
        if (eachItem.quantityPerUnit.length) {
            props.setPerUnitList(eachItem.quantityPerUnit);
            addVarForm1.handleChange({
                target: { name: 'quantityPerUnit', value: eachItem.quantityPerUnit[0] },
            });
        }
    };

    const onChangeMfgDate = (val, i) => {
        // addVarForm1.handleChange({ target: { name: `mfgDate`, value: val, } })
        addVarForm1.handleChange({
            target: { name: `products[${i}].mfgDate`, value: val },
        });
        addVarForm1.handleChange({
            target: { name: `products[${i}].expDate`, value: null },
        });
    };
    const onChangeExpDate = (val, i) => {
        // addVarForm1.handleChange({ target: { name: `expDate`, value: val, } })
        addVarForm1.handleChange({
            target: { name: `products[${i}].expDate`, value: val },
        });
    };

    const AddAnotherProduct = () => {
        const data = [...addVarForm1.values.products];
        const newData = AddNewProduct();
        const finalData = [...data, newData];

        addVarForm1.handleChange({ target: { name: 'products', value: finalData } });
    };
    const cancel = index => {
        const productClone = JSON.parse(
            JSON.stringify(addVarForm1?.values?.products),
        );
        // console.log(Object.keys(productClone));
        productClone.splice(index, 1);

        addVarForm1.handleChange({ target: { name: 'products', value: productClone } });
    };
    const DeleteIcon = index => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        width: 40,
                        height: 40,
                        borderRadius: 400,
                        backgroundColor: '#fff',
                        justifyContent: 'center',
                        alignItems: 'center',
                        elevation: 10,
                        marginTop: 5,
                    }}
                    opacity={5}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'flex-end',
                        }}
                        onPress={() => cancel(index)}
                        disabled={
                            addVarForm1?.values?.products.length === 1 ? true : false
                        }>
                        <MaterialIcons
                            name="delete"
                            size={24}
                            color={
                                addVarForm1?.values?.products.length === 1 ? 'gray' : 'red'
                            }
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
    const VaccineListComponent = () => {
        return (
            <View
                style={{
                    backgroundColor: '#E0F0EA',
                    paddingHorizontal: 15,
                    marginTop: 10,
                }}>
                {addVarForm1?.values?.products.map((each, index) => {
                    return (
                        <View>
                            <View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                    }}>
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            fontFamily: fonts.REGULAR,
                                            marginBottom: 10,
                                            textAlign: 'center',
                                        }}>
                                        {index + 1 + '. ' + t('Vaccine')}
                                    </Text>
                                    {DeleteIcon(index)}
                                </View>
                                <InputField
                                    placeholder={t('Enter here')}
                                    label={t('Lot Number')}
                                    inputValue={addVarForm1?.values?.products[index]?.batchNo}
                                    setInputValue={addVarForm1.handleChange(
                                        `products[${index}].batchNo`,
                                    )}
                                    errorMsg={
                                        addVarForm1.errors &&
                                        addVarForm1.errors.products &&
                                        addVarForm1.errors.products[index] &&
                                        addVarForm1.errors.products[index].batchNo
                                    }
                                    inputStyle={{ marginBottom: 10 }}
                                    labelStyle={{ marginBottom: 5 }}
                                    mandatory={isMandatory}
                                />
                                <View
                                    style={{
                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                    }}>
                                    <View style={{ flex: 0.48 }}>
                                        <InputField
                                            placeholder={t('Enter here')}
                                            label={t('No. of Vials')}
                                            inputValue={
                                                addVarForm1?.values?.products[index]?.noOfUnits + ''
                                            }
                                            // setInputValue={addVarForm1.handleChange('noOfBoxes')}
                                            setInputValue={addVarForm1.handleChange(
                                                `products[${index}].noOfUnits`,
                                            )}
                                            KeyboardType={'numeric'}
                                            inputStyle={{ marginBottom: 10 }}
                                            labelStyle={{ marginBottom: 5 }}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.products &&
                                                addVarForm1.errors.products[index] &&
                                                addVarForm1.errors.products[index].noOfUnits
                                            }
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                    <View style={{ flex: 0.48 }}>
                                        <InputField
                                            placeholder={t('Enter here')}
                                            label={t('No. of Boxes')}
                                            inputValue={
                                                addVarForm1?.values?.products[index]?.noOfBoxes + ''
                                            }
                                            // setInputValue={addVarForm1.handleChange('noOfBoxes')}
                                            setInputValue={addVarForm1.handleChange(
                                                `products[${index}].noOfBoxes`,
                                            )}
                                            KeyboardType={'numeric'}
                                            inputStyle={{ marginBottom: 10 }}
                                            labelStyle={{ marginBottom: 5 }}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.products &&
                                                addVarForm1.errors.products[index] &&
                                                addVarForm1.errors.products[index].noOfBoxes
                                            }
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                </View>
                                <View
                                    style={{
                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                    }}>
                                    <View style={{ flex: 0.48 }}>
                                        <DatePickerComponent
                                            label={t('Mfg Date')}
                                            placeholder="DD/MM/YYYY"
                                            onChange={val => onChangeMfgDate(val, index)}
                                            val={addVarForm1?.values?.products[index]?.mfgDate}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.products &&
                                                addVarForm1.errors.products[index] &&
                                                addVarForm1.errors.products[index].mfgDate
                                            }
                                            isMfgDate={true}
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                    <View style={{ flex: 0.48 }}>
                                        <DatePickerComponent
                                            label={t('Expiry Date')}
                                            placeholder="DD/MM/YYYY"
                                            onChange={val => onChangeExpDate(val, index)}
                                            val={addVarForm1?.values?.products[index]?.expDate}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.products &&
                                                addVarForm1.errors.products[index] &&
                                                addVarForm1.errors.products[index].expDate
                                            }
                                            minDate={
                                                addVarForm1?.values?.products[index]?.mfgDate
                                                    ? new Date(
                                                        addVarForm1?.values?.products[index]?.mfgDate,
                                                    ) >= new Date()
                                                        ? addVarForm1?.values?.products[index]?.mfgDate
                                                        : new Date()
                                                    : new Date()
                                            }
                                            isMfgDate={false}
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                    );
                })}
                <View style={{ flexDirection: 'row' }}>
                    <CustomButtonWithBorder
                        buttonName={t('Add more')}
                        onPressButton={AddAnotherProduct}
                        bdColor={'#BFBFBF'}
                        textColor={'#20232B'}
                        bgColor={'#fff'}
                    />
                </View>
            </View>
        );
    };
    const onChangeDiluentMfgDate = (val, i) => {
        // addVarForm1.handleChange({ target: { name: `mfgDate`, value: val, } })
        addVarForm1.handleChange({
            target: { name: `diluent[${i}].mfgDate`, value: val },
        });
        addVarForm1.handleChange({
            target: { name: `diluent[${i}].expDate`, value: null },
        });
    };
    const onChangeDiluentExpDate = (val, i) => {
        addVarForm1.handleChange({
            target: { name: `diluent[${i}].expDate`, value: val },
        });
    };

    const AddAnotherDiluent = () => {
        const data = [...addVarForm1.values.diluent];
        const newData = AddNewProduct();
        const finalData = [...data, newData];

        addVarForm1.handleChange({ target: { name: 'diluent', value: finalData } });
    };
    const cancelDiluent = index => {
        const productClone = JSON.parse(
            JSON.stringify(addVarForm1?.values?.diluent),
        );
        // console.log(Object.keys(productClone));
        productClone.splice(index, 1);

        addVarForm1.handleChange({ target: { name: 'diluent', value: productClone } });
    };
    const DeleteDiluent = index => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        width: 40,
                        height: 40,
                        borderRadius: 400,
                        backgroundColor: '#fff',
                        justifyContent: 'center',
                        alignItems: 'center',
                        elevation: 10,
                        marginTop: 5,
                    }}
                    opacity={5}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'flex-end',
                        }}
                        onPress={() => cancelDiluent(index)}
                        disabled={addVarForm1?.values?.diluent.length === 1 ? true : false}>
                        <MaterialIcons
                            name="delete"
                            size={24}
                            color={addVarForm1?.values?.diluent.length === 1 ? 'gray' : 'red'}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
    const DiluentListComponent = () => {
        return (
            <View
                style={{
                    backgroundColor: '#E0F0EA',
                    paddingHorizontal: 15,
                    marginTop: 10,
                }}>
                {addVarForm1?.values?.diluent?.map((each, index) => {
                    return (
                        <View>
                            <View>
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        alignItems: 'center',
                                    }}>
                                    <Text
                                        style={{
                                            fontSize: 15,
                                            fontFamily: fonts.REGULAR,
                                            marginBottom: 10,
                                            textAlign: 'center',
                                        }}>
                                        {index + 1 + t('. Number of Ampules ')}
                                    </Text>
                                    {DeleteDiluent(index)}
                                </View>
                                <InputField
                                    placeholder={t('Enter here')}
                                    label={t('Lot Number')}
                                    inputValue={addVarForm1?.values?.diluent[index]?.batchNo}
                                    setInputValue={addVarForm1.handleChange(
                                        `diluent[${index}].batchNo`,
                                    )}
                                    errorMsg={
                                        addVarForm1.errors &&
                                        addVarForm1.errors.diluent &&
                                        addVarForm1.errors.diluent[index] &&
                                        addVarForm1.errors.diluent[index].batchNo
                                    }
                                    inputStyle={{ marginBottom: 10 }}
                                    labelStyle={{ marginBottom: 5 }}
                                    mandatory={isMandatory}
                                />
                                <View
                                    style={{
                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                    }}>
                                    <View style={{ flex: 0.48 }}>
                                        <InputField
                                            placeholder={t('Enter here')}
                                            label={t('No. of Boxes')}
                                            inputValue={
                                                addVarForm1?.values?.diluent[index]?.noOfBoxes
                                            }
                                            // setInputValue={addVarForm1.handleChange('noOfBoxes')}
                                            setInputValue={addVarForm1.handleChange(
                                                `diluent[${index}].noOfBoxes`,
                                            )}
                                            KeyboardType={'numeric'}
                                            inputStyle={{ marginBottom: 10 }}
                                            labelStyle={{ marginBottom: 5 }}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.diluent &&
                                                addVarForm1.errors.diluent[index] &&
                                                addVarForm1.errors.diluent[index].noOfBoxes
                                            }
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                    <View style={{ flex: 0.48 }}>
                                        <InputField
                                            placeholder={t('Enter here')}
                                            label={t('No. of Units')}
                                            inputValue={
                                                addVarForm1?.values?.diluent[index]?.noOfUnits
                                            }
                                            // setInputValue={addVarForm1.handleChange('noOfBoxes')}
                                            setInputValue={addVarForm1.handleChange(
                                                `diluent[${index}].noOfUnits`,
                                            )}
                                            KeyboardType={'numeric'}
                                            inputStyle={{ marginBottom: 10 }}
                                            labelStyle={{ marginBottom: 5 }}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.diluent &&
                                                addVarForm1.errors.diluent[index] &&
                                                addVarForm1.errors.diluent[index].noOfUnits
                                            }
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                </View>
                                <View
                                    style={{
                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                    }}>
                                    <View style={{ flex: 0.48 }}>
                                        <DatePickerComponent
                                            label={t('Mfg Date')}
                                            placeholder="DD/MM/YYYY"
                                            onChange={val => onChangeDiluentMfgDate(val, index)}
                                            val={addVarForm1?.values?.diluent[index]?.mfgDate}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.diluent &&
                                                addVarForm1.errors.diluent[index] &&
                                                addVarForm1.errors.diluent[index].mfgDate
                                            }
                                            isMfgDate={true}
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                    <View style={{ flex: 0.48 }}>
                                        <DatePickerComponent
                                            label={t('Expiry Date')}
                                            placeholder="DD/MM/YYYY"
                                            onChange={val => onChangeDiluentExpDate(val, index)}
                                            val={addVarForm1?.values?.diluent[index]?.expDate}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.diluent &&
                                                addVarForm1.errors.diluent[index] &&
                                                addVarForm1.errors.diluent[index].expDate
                                            }
                                            minDate={
                                                addVarForm1?.values?.diluent[index]?.mfgDate
                                                    ? new Date(
                                                        addVarForm1?.values?.diluent[index]?.mfgDate,
                                                    ) >= new Date()
                                                        ? addVarForm1?.values?.diluent[index]?.mfgDate
                                                        : new Date()
                                                    : new Date()
                                            }
                                            isMfgDate={false}
                                            mandatory={isMandatory}
                                        />
                                    </View>
                                </View>
                            </View>
                        </View>
                    );
                })}
                <View style={{ flexDirection: 'row' }}>
                    <CustomButtonWithBorder
                        buttonName={t('Add more')}
                        onPressButton={AddAnotherDiluent}
                        bdColor={'#BFBFBF'}
                        textColor={'#20232B'}
                        bgColor={'#fff'}
                    />
                </View>
            </View>
        );
    };
    const changeAfterSelectCountry = async item => {
        const { value, label } = item;
        addVarForm1.handleChange({ target: { name: 'country', value: label } });
        addVarForm1.handleChange({ target: { name: 'countryId', value: value } });
    };
    const changeAfterSelectInventory = item => {
        const { value, label } = item;
        addVarForm1.handleChange({ target: { name: 'inventoryId', value: value } });
        addVarForm1.handleChange({ target: { name: 'inventory', value: label } });
    };
    const changeAfterSelectPerUnit = item => {
        const { value } = item;
        addVarForm1.handleChange({ target: { name: 'quantityPerUnit', value: value } });
    };
    const {
        currentStep,
        handleNext,
        handlePrev,
        userLocation = {},
        manufaturerList = [],
        form,
        country_list = [],
        productName = '',
        inv_list = [],
        perUnitList = [],
    } = props;
    const { locations = [] } = userLocation;
    return (
        <View>
            <View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20 }}>
                <Text
                    style={{ textAlign: 'center', fontSize: 14, fontFamily: fonts.MEDIUM }}>
                    {t('Part 3 Details of vaccine shipment')}
                </Text>
                <View style={{ padding: 15 }}>
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Purchase Order Number')}
                        inputValue={addVarForm1?.values?.poNo}
                        setInputValue={addVarForm1.handleChange('poNo')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.poNo}
                        mandatory={isMandatory}
                    />

                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Consignee')}
                        inputValue={addVarForm1?.values?.consignee}
                        setInputValue={addVarForm1.handleChange('consignee')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.consignee}
                        mandatory={isMandatory}
                        disable={true}
                    />
                    {/* <DropDown
                        dropdownData={locations}
                        onChangeValue={changeAfterConsingnee}
                        label='name'
                        mapKey='_id'
                        val={addVarForm1?.values?.consignee}
                        placeholderText="Enter here"
                        labelText='Consignee'
                        changeAfterSelect={() => console.log()}
                        errorMsg={addVarForm1?.errors?.consignee}
                        mandatory={isMandatory}
                        search={false}
                    /> */}

                    <DropDown
                        dropdownData={inv_list}
                        onChangeValue={changeAfterSelectInventory}
                        label="name"
                        mapKey="_id"
                        val={addVarForm1?.values?.inventoryId}
                        placeholder={t('Select Location')}
                        errorMsg={addVarForm1?.errors?.inventoryId}
                        mandatory={isMandatory}
                        labelText={t('Inventory Location')}
                        search={false}
                    />

                    <ManufacturerDropdown
                        dropdownData={manufaturerList}
                        onChangeValue={changeAfterManufature}
                        label="name"
                        mapKey="id"
                        val={addVarForm1?.values?.manufacturerId}
                        placeholderText={t('Manufacturer')}
                        labelText={t('Manufacturer')}
                        errorMsg={addVarForm1?.errors?.manufacturerId}
                        mandatory={isMandatory}
                        search={false}
                    />
                    <DropDown
                        dropdownData={perUnitList}
                        onChangeValue={changeAfterSelectPerUnit}
                        val={addVarForm1?.values?.quantityPerUnit}
                        placeholder={t('Select')}
                        errorMsg={addVarForm1?.errors?.quantityPerUnit}
                        mandatory={isMandatory}
                        labelText={t('Doses Per Vial')}
                        search={false}
                    />
                    <InputField
                        placeholder={t('Country')}
                        label={t('Country')}
                        inputValue={addVarForm1?.values?.countryId}
                        setInputValue={addVarForm1.handleChange('poNo')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.countryId}
                        mandatory={isMandatory}
                        disable={true}
                    />
                    {/* <DropDown
                        dropdownData={country_list}
                        onChangeValue={changeAfterSelectCountry}
                        label="name"
                        mapKey="_id"
                        val={addVarForm1?.values?.countryId}
                        placeholderText={t('Country')}
                        labelText={t('Country')}
                        errorMsg={addVarForm1?.errors?.countryId}
                        mandatory={isMandatory}
                        disabled={true}
                    /> */}
                </View>

                {VaccineListComponent()}

                <View>
                    {(productName === 'BCG' ||
                        productName === 'MR' ||
                        productName === 'Measles' ||
                        productName === 'Rubella') &&
                        addVarForm1.values &&
                        addVarForm1.values?.diluent
                        ? DiluentListComponent()
                        : null}
                </View>
                <View style={{ padding: 15 }}>
                    <View style={{ marginTop: 10 }} />
                    <RadioButtonGroup
                        label={t('was No.Of Boxes received as per shipping notification?')}
                        options={options}
                        val={addVarForm1?.values?.shortShipment}
                        onChangeValue={val =>
                            addVarForm1.handleChange({
                                target: { name: 'shortShipment', value: val },
                            })
                        }
                    />
                    <RadioButtonGroup
                        label={
                            t('if not, were details of short-shipment provided prior to vaccine arrival?')
                        }
                        options={options}
                        val={addVarForm1?.values?.shortShipmentNotification}
                        onChangeValue={val =>
                            addVarForm1.handleChange({
                                target: { name: 'shortShipmentNotification', value: val },
                            })
                        }
                    />
                    <View style={{ marginTop: 10 }} />

                    <InputField
                        placeholder={t('Comments')}
                        label={t('Comments')}
                        inputValue={addVarForm1?.values?.comment}
                        setInputValue={addVarForm1.handleChange('comment')}
                        inputStyle={{ marginBottom: 10, height: 100 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.comment}
                        mandatory={isMandatory}
                    />
                    {/* <Text>{JSON.stringify(addVarForm1?.values)}</Text> */}

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        {currentStep > 0 && (
                            <CustomButtonWithBorder
                                buttonName={t('Prev')}
                                onPressButton={handlePrev}
                                bdColor={'#BFBFBF'}
                                textColor={'#20232B'}
                            />
                        )}
                        <CustomButton
                            buttonName={t('Save && continue')}
                            onPressButton={_add}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>
                </View>
            </View>
        </View>
    );
};
export const AddVarPart5 = props => {
    const { t, i18n } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);

    useEffect(() => {
        // Use useEffect to update initial values when parent data changes

        if (form.comment) {
            addVarForm1.setValues(form);
        }
    }, [form]);
    var validationSchema = Yup.object().shape(
        {
            comment: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
        },
        ['comment'],
    ); // <-- HERE!!!!!!!!
    const addVarForm1 = useFormik({
        initialValues: {
            invoice: true,
            packingList: true,
            vaccineArrivalReport: true,
            releaseCertificate: true,
            comment: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addVarForm1.handleSubmit();
    };

    const handleSubmit = async values => {
        console.log(' values', values);
        await props.setForm(values);

        handleNext();
    };

    const { currentStep, handleNext, handlePrev, form } = props;
    return (
        <View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 15,
                }}>
                <Text
                    style={{ textAlign: 'center', fontSize: 14, fontFamily: fonts.MEDIUM }}>
                    {t('Part 4: Documents Accompanying the Shipment')}
                </Text>
                <View style={{ marginTop: 10 }} />

                {/* <InputField
                    placeholder="Enter here"
                    label="Report Number"
                    inputValue={addVarForm1?.values?.transitNo}
                    setInputValue={addVarForm1.handleChange('transitNo')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addVarForm1?.errors?.transitNo}
                    mandatory={isMandatory}
                /> */}

                <RadioButtonGroup
                    label={t('Invoice')}
                    options={options}
                    val={addVarForm1?.values?.invoice}
                    onChangeValue={val =>
                        addVarForm1.handleChange({ target: { name: 'invoice', value: val } })
                    }
                />
                <RadioButtonGroup
                    label={t('Packing List')}
                    options={options}
                    val={addVarForm1?.values?.packingList}
                    onChangeValue={val =>
                        addVarForm1.handleChange({
                            target: { name: 'packingList', value: val },
                        })
                    }
                />
                {/* <RadioButtonGroup
                    label={t('Vaccine Arrival Report')}
                    options={options}
                    val={addVarForm1?.values?.vaccineArrivalReport}
                    onChangeValue={val =>
                        addVarForm1.handleChange({
                            target: { name: 'vaccineArrivalReport', value: val },
                        })
                    }
                /> */}
                <RadioButtonGroup
                    label={t('Release Certificate')}
                    options={options}
                    val={addVarForm1?.values?.releaseCertificate}
                    onChangeValue={val =>
                        addVarForm1.handleChange({
                            target: { name: 'releaseCertificate', value: val },
                        })
                    }
                />
                <View style={{ marginTop: 10 }} />

                {/* <InputField
                    placeholder="Enter here"
                    label="Other"
                    inputValue={addVarForm1?.values?.other}
                    setInputValue={addVarForm1.handleChange('other')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addVarForm1?.errors?.other}
                    mandatory={isMandatory}
                /> */}
                <InputField
                    placeholder={t('Enter here')}
                    label={t('Comments')}
                    inputValue={addVarForm1?.values?.comment}
                    setInputValue={addVarForm1.handleChange('comment')}
                    inputStyle={{ marginBottom: 10, height: 100 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addVarForm1?.errors?.comment}
                    mandatory={isMandatory}
                />
                {/* <Text>{JSON.stringify(addVarForm1?.values)}</Text> */}

                <View style={{ marginTop: 10 }} />
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    {currentStep > 0 && (
                        <CustomButtonWithBorder
                            buttonName={t('Prev')}
                            onPressButton={handlePrev}
                            bdColor={'#BFBFBF'}
                            textColor={'#20232B'}
                        />
                    )}
                    <CustomButton
                        buttonName={t('Save && continue')}
                        onPressButton={_add}
                        bgColor={'#208196'}
                        textColor={'#fff'}
                    />
                </View>
            </View>
        </View>
    );
};
export const AddVarPart6 = props => {
    const { t } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);
    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        if (form.invoiceAmount) {
            addVarForm1.setValues(form);
        }
    }, [form]);
    var validationSchema = Yup.object().shape(
        {
            noOfInspected: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            invoiceAmount: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            coolantType: Yup.string().required(t('Required')),
            tempMonitors: Yup.string().required(t('Required')),
            tempMonitors: Yup.array()
                .min(1, t('Required'))
                .required(t('Required')),
            alerts: Yup.array().of(
                Yup.object().shape({
                    inspectionDateTime: Yup.string().required(t('Required')),
                    temperature: Yup.string().required(t('Required')),
                    boxNo: Yup.number()
                        .min(1, t('Invalid Qauntity'))
                        .required(t('Required')),
                    batchNo: Yup.string()
                        .required(t('Required'))
                        .trim()
                        .min(2, t('Must be at least 2 characters long'))
                        .test(
                            'no-spaces',
                            t('It must not be empty spaces'),
                            value => value && value.trim() !== '',
                        ),
                    vvmStatus: Yup.string().required(t('Required')),
                }),
            ),
            // alerts: Yup.array()
            //     .when('addAlert', {
            //         is: true,
            //         then: Yup.array()
            //             .of(
            //                 Yup.object().shape({
            //                     inspectionDateTime: Yup.string().required(t('Required')),
            //                     temperature: Yup.string().required(t('Required')),
            //                     boxNo: Yup.number().min(1, t('Invalid Qauntity')).required(t('Required')),
            //                     batchNo: Yup.string()
            //                         .required(t('Required')).trim().min(2, t('Must be at least 2 characters long'))
            //                         .test('no-spaces',t('It must not be empty spaces'), (value) => value && value.trim() !== ''),
            //                     vvmStatus: Yup.string().required(t('Required')),
            //                 })
            //             )
            //             .required('Reauied'),
            //     }),
        },
        ['noOfInspected', 'invoiceAmount'],
    ); // <-- HERE!!!!!!!!
    const addVarForm1 = useFormik({
        initialValues: {
            noOfInspected: '',
            invoiceAmount: '',
            coolantType: 'Dry Ice',
            tempMonitors: [],
            addAlert: false,
            // alerts: [
            //     {
            //         batchNo: "",
            //         boxNo: "",
            //         temperature: "45 C",
            //         vvmStatus: "1",
            //         inspectionDateTime: ""
            //     }
            // ],
            alerts: [],
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addVarForm1.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };

    const onChangeDateOfInspection = (val, i) => {
        // addVarForm1.handleChange({ target: { name: `inspectionDateTime`, value: val, } })
        addVarForm1.handleChange({
            target: { name: `alerts[${i}].inspectionDateTime`, value: val },
        });
    };
    const AddAnotherAlert = () => {
        const data = [...addVarForm1.values.alerts];
        const newData = AddNewAlert();
        const finalData = [...data, newData];

        addVarForm1.handleChange({ target: { name: 'alerts', value: finalData } });
    };
    const cancel = index => {
        const alertClone = JSON.parse(JSON.stringify(addVarForm1?.values?.alerts));
        // console.log(Object.keys(alertClone));
        alertClone.splice(index, 1);

        addVarForm1.handleChange({ target: { name: 'alerts', value: alertClone } });
    };
    const DeleteIcon = index => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                {/* <Text style={{ width: '80%', fontWeight: 'bold' }}>
            {t('Product') + ' ' + (index + 1)}
          </Text> */}
                <View
                    style={{
                        width: 40,
                        height: 40,
                        borderRadius: 400,
                        backgroundColor: '#fff',
                        justifyContent: 'center',
                        alignItems: 'center',
                        elevation: 10,
                        marginTop: 5,
                    }}
                    opacity={5}>
                    <TouchableOpacity
                        style={{
                            justifyContent: 'flex-end',
                        }}
                        onPress={() => cancel(index)}
                        disabled={addVarForm1?.values?.alerts.length === 1 ? true : false}>
                        <MaterialIcons
                            name="delete"
                            size={24}
                            color={addVarForm1?.values?.alerts.length === 1 ? 'gray' : 'red'}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
    const changeAfterBatch = (item, i) => {
        const { value } = item;
        addVarForm1.handleChange({
            target: { name: `alerts[${i}].batchNo`, value: value },
        });
    };
    const AlertListComponent = () => {
        return (
            <View>
                {addVarForm1?.values?.alerts.map((each, index) => {
                    return (
                        <View
                            style={{
                                backgroundColor: '#E0F0EA',
                                paddingHorizontal: 15,
                                marginTop: 10,
                            }}>
                            <View>
                                <View
                                    style={{
                                        justifyContent: 'space-between',
                                        flexDirection: 'row',
                                        marginTop: 5,
                                    }}>

                                    <View style={{ flex: 0.48, marginTop: -10 }}>
                                        <DropDown
                                            dropdownData={filterProducts}
                                            onChangeValue={val => changeAfterBatch(val, index)}
                                            label='batchNo'
                                            mapKey='batchNo'
                                            val={addVarForm1?.values?.alerts[index]?.batchNo}
                                            placeholderText={t('Select')}
                                            labelText={t('Lot Number')}
                                            mandatory={isMandatory}
                                        />
                                        {/* <InputField
                                            placeholder={t('Enter here')}
                                            label={t('Lot Number')}
                                            inputValue={addVarForm1?.values?.alerts[index]?.batchNo}
                                            setInputValue={addVarForm1.handleChange(
                                                `alerts[${index}].batchNo`,
                                            )}
                                            inputStyle={{ marginBottom: 10 }}
                                            labelStyle={{ marginBottom: 5 }}
                                            // errorMsg={addVarForm1?.errors?.batchNo}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.alerts &&
                                                addVarForm1.errors.alerts[index] &&
                                                addVarForm1.errors.alerts[index].batchNo
                                            }
                                            mandatory={isMandatory}
                                        /> */}
                                    </View>
                                    <View style={{ flex: 0.48 }}>
                                        <InputField
                                            placeholder={t('Enter here')}
                                            label={t('Serial Number of Electronic Device')}
                                            inputValue={addVarForm1?.values?.alerts[index]?.boxNo}
                                            setInputValue={addVarForm1.handleChange(
                                                `alerts[${index}].boxNo`,
                                            )}
                                            inputStyle={{ marginBottom: 10 }}
                                            labelStyle={{ marginBottom: 5 }}
                                            // errorMsg={addVarForm1?.errors?.boxNo}
                                            errorMsg={
                                                addVarForm1.errors &&
                                                addVarForm1.errors.alerts &&
                                                addVarForm1.errors.alerts[index] &&
                                                addVarForm1.errors.alerts[index].boxNo
                                            }
                                            KeyboardType={'numeric'}
                                            mandatory={isMandatory}
                                        />

                                    </View>
                                    {DeleteIcon(index)}
                                </View>
                                {/* <RadioButtonGroup
                                    label={t('Alarm in Electronic Device')}
                                    options={options3}
                                    val={addVarForm1?.values?.alerts[index]?.temperature}
                                    onChangeValue={val =>
                                        addVarForm1.handleChange({
                                            target: {
                                                name: `alerts[${index}].temperature`,
                                                value: val,
                                            },
                                        })
                                    }
                                /> */}
                                <InputField
                                    placeholder={t('Enter here')}
                                    label={t('Alarm in Electronic Device') + '>= '}
                                    inputValue={addVarForm1?.values?.alerts[index]?.temperature}
                                    setInputValue={addVarForm1.handleChange(
                                        `alerts[${index}].temperature`,
                                    )}
                                    inputStyle={{ marginBottom: 10 }}
                                    labelStyle={{ marginBottom: 5 }}
                                    errorMsg={
                                        addVarForm1.errors &&
                                        addVarForm1.errors.alerts &&
                                        addVarForm1.errors.alerts[index] &&
                                        addVarForm1.errors.alerts[index].temperature
                                    }
                                    KeyboardType={'numeric'}
                                    mandatory={isMandatory}
                                />
                                <RadioButtonGroup
                                    label={t('Vaccine Vial Management')}
                                    options={options4}
                                    val={addVarForm1?.values?.alerts[index]?.vvmStatus}
                                    onChangeValue={val =>
                                        addVarForm1.handleChange({
                                            target: {
                                                name: `alerts[${index}].vvmStatus`,
                                                value: val,
                                            },
                                        })
                                    }

                                // onChangeValue={val => addVarForm1.handleChange({ target: { name: `vvmStatus`, value: val, } })}
                                />
                                <TextLineComponent
                                    leftText={t('Vaccine vial monitor')}
                                    rightText={
                                        addVarForm1?.values?.alerts[index]?.vvmStatus === '1' ||
                                            addVarForm1?.values?.alerts[index]?.vvmStatus === '2'
                                            ? t('Usable')
                                            : t('non usable')
                                    }
                                />
                                <View style={{ marginTop: 10 }} />

                                <DateTimePickerComponent
                                    label={t('Date of Inspection')}
                                    placeholder="DD/MM/YYYY"
                                    onChange={val => onChangeDateOfInspection(val, index)}
                                    val={addVarForm1?.values?.alerts[index]?.inspectionDateTime}
                                    // errorMsg={addVarForm1?.errors?.alerts[index]?.inspectionDateTime}
                                    errorMsg={
                                        addVarForm1.errors &&
                                        addVarForm1.errors.alerts &&
                                        addVarForm1.errors.alerts[index] &&
                                        addVarForm1.errors.alerts[index].inspectionDateTime
                                    }
                                    // errorMsg={addVarForm1?.errors?.inspectionDateTime}
                                    mandatory={isMandatory}
                                />
                            </View>
                        </View>
                    );
                })}
            </View>
        );
    };
    const AddAlert = () => {
        const newData = AddNewAlert();
        const finalData = [newData];
        addVarForm1.handleChange({ target: { name: 'alerts', value: finalData } });
        addVarForm1.handleChange({ target: { name: 'addAlert', value: true } });
    };
    const RemoveAlert = () => {
        addVarForm1.handleChange({ target: { name: 'alerts', value: [] } });
        addVarForm1.handleChange({ target: { name: 'addAlert', value: false } });
    };
    const handleCheckboxPress = (label) => {
        if (addVarForm1?.values?.tempMonitors.includes(label)) {

            addVarForm1.handleChange({
                target: { name: 'tempMonitors', value: (addVarForm1?.values?.tempMonitors.filter(item => item !== label)) },
            })
        } else {
            addVarForm1.handleChange({
                target: { name: 'tempMonitors', value: ([...addVarForm1?.values?.tempMonitors, label]) },
            })

        }
    };
    // console.log('addVarForm1?.errors ', addVarForm1?.errors)
    const { currentStep, handleNext, handlePrev, form, form4 = {} } = props;
    const { products = [] } = form4
    const filterProducts = products.filter(each => {
        if (each.batchNo) {
            return each
        }
    })
    const totalNoOfBoxes = products.reduce((sum, product) => sum + product.noOfBoxes, 0);
    return (
        <View>
            <View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20 }}>
                <Text
                    style={{ textAlign: 'center', fontSize: 14, fontFamily: fonts.MEDIUM }}>
                    {'Part 5: Status of Shipping Indicators'}
                </Text>
                <View style={{ padding: 15 }}>
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Total Number of Boxes')}
                        inputValue={totalNoOfBoxes}
                        setInputValue={addVarForm1.handleChange('noOfInspected')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        mandatory={isMandatory}
                        KeyboardType={'numeric'}
                        disable={true}
                    />
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Total Number of Boxes Inspected')}
                        inputValue={addVarForm1?.values?.noOfInspected}
                        setInputValue={addVarForm1.handleChange('noOfInspected')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.noOfInspected}
                        mandatory={isMandatory}
                        KeyboardType={'numeric'}
                    />
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Invoice Amount')}
                        inputValue={addVarForm1?.values?.invoiceAmount}
                        setInputValue={addVarForm1.handleChange('invoiceAmount')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.invoiceAmount}
                        mandatory={isMandatory}
                        KeyboardType={'numeric'}
                    />
                    <RadioButtonGroup
                        label={t('Coolant Type')}
                        options={options1}
                        val={addVarForm1?.values?.coolantType}
                        onChangeValue={val =>
                            addVarForm1.handleChange({
                                target: { name: 'coolantType', value: val },
                            })
                        }
                    />
                    {/* <RadioButtonGroup
                        label={t('Temperature Monitors Present')}
                        options={options2}
                        val={addVarForm1?.values?.tempMonitors}
                        onChangeValue={val =>
                            addVarForm1.handleChange({
                                target: { name: 'tempMonitors', value: val },
                            })
                        }
                    /> */}
                    <CheckBox
                        label={t('Temperature Monitors Present')}
                        options={options2}
                        val={addVarForm1?.values?.tempMonitors}
                        onChangeValue={handleCheckboxPress}
                        errorMsg={addVarForm1?.errors?.tempMonitors}
                        mandatory={isMandatory}
                    />
                    <View style={{ marginTop: 10 }} />

                    <Text
                        style={{ fontSize: 12, fontFamily: fonts.MEDIUM, color: '#C85151' }}>
                        {
                            t('*Provided below details of status only when problems are observed')
                        }
                    </Text>
                    <View style={{ marginTop: 10 }} />
                    <Text
                        style={{ fontSize: 12, fontFamily: fonts.REGULAR, color: '#737373' }}>
                        {
                            t('(In addition fill in alarm reporting form if there are any alarms in electronic devices)')
                        }
                    </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <CustomButton
                            buttonName={addVarForm1?.values?.addAlert ? t('Remove') : t('Add')}
                            onPressButton={() =>
                                addVarForm1?.values?.addAlert ? RemoveAlert() : AddAlert()
                            }
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>
                </View>
                {addVarForm1?.values?.addAlert && AlertListComponent()}
                {addVarForm1?.values?.addAlert && (
                    <View style={{ flexDirection: 'row' }}>
                        <CustomButtonWithBorder
                            buttonName={t('Add more')}
                            onPressButton={AddAnotherAlert}
                            bdColor={'#BFBFBF'}
                            textColor={'#20232B'}
                            bgColor={'#fff'}
                        />
                    </View>
                )}

                <View style={{ marginTop: 10 }} />

                <View style={{ padding: 15 }}>
                    {/* <View style={{ marginTop: 10 }} />
                    <InputField
                        placeholder="Enter here"
                        label="Name of clearing agent"
                        inputValue={addVarForm1?.values?.transitNo}
                        setInputValue={addVarForm1.handleChange('transitNo')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.transitNo}
                        mandatory={isMandatory}
                    />
                    <InputField
                        placeholder="Enter here"
                        label="On behalf of"
                        inputValue={addVarForm1?.values?.transitNo}
                        setInputValue={addVarForm1.handleChange('transitNo')}
                        inputStyle={{ marginBottom: 10, }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={addVarForm1?.errors?.transitNo}
                        mandatory={isMandatory}
                    /> */}
                    {/* <Text>{JSON.stringify(addVarForm1?.values)}</Text> */}

                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        {currentStep > 0 && (
                            <CustomButtonWithBorder
                                buttonName={t('Prev')}
                                onPressButton={handlePrev}
                                bdColor={'#BFBFBF'}
                                textColor={'#20232B'}
                            />
                        )}
                        <CustomButton
                            buttonName={t('Save && continue')}
                            onPressButton={_add}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>

                    {/* <AddNewProductOrderComponent productListByType={productListByType} addProductInList={addProductInList} selectedProductType={selectedProductType} /> */}
                </View>
            </View>
        </View>
    );
};
export const AddVarPart7 = props => {
    const { t, i18n } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);

    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        if (form.comment) {
            addVarForm1.setValues(form);
        }
    }, [form]);
    var validationSchema = Yup.object().shape(
        {
            type: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            comment: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
        },
        ['type', 'comment'],
    ); // <-- HERE!!!!!!!!
    const addVarForm1 = useFormik({
        initialValues: {
            type: '',
            labelsAttached: true,
            comment: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addVarForm1.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };
    const changeAfterTypeSelect = item => {
        const { value } = item;
        addVarForm1.handleChange({ target: { name: 'type', value: value } });
    };

    const { currentStep, handleNext, handlePrev, form, } = props;


    return (
        <View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 15,
                }}>
                <Text
                    style={{
                        textAlign: 'center',
                        fontSize: 14,
                        fontFamily: fonts.MEDIUM,
                        marginBottom: 15,
                    }}>
                    {t('Part 6: General Conditions of Shipment')}
                </Text>
                <DropDown
                    dropdownData={['GOOD', 'DAMAGED', 'DEFECTIVE']}
                    onChangeValue={changeAfterTypeSelect}
                    // label='name'
                    // mapKey='_id'
                    val={addVarForm1?.values?.type}
                    placeholderText={t('Select')}
                    labelText={t('What was the condition of the boxes on arrival?')}
                    errorMsg={addVarForm1?.errors?.type}
                    mandatory={isMandatory}
                    search={false}
                />
                <RadioButtonGroup
                    label={t('Were necessary labels attached to the shipping boxes?')}
                    options={options}
                    val={addVarForm1?.values?.labelsAttached}
                    onChangeValue={val =>
                        addVarForm1.handleChange({
                            target: { name: 'labelsAttached', value: val },
                        })
                    }
                />

                <View style={{ marginTop: 10 }} />
                <InputField
                    placeholder={t('Enter here')}
                    label={t('Other comments including description of alarms in electronic devices')}
                    inputValue={addVarForm1?.values?.comment}
                    setInputValue={addVarForm1.handleChange('comment')}
                    inputStyle={{ marginBottom: 10, height: 100 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addVarForm1?.errors?.comment}
                    mandatory={isMandatory}
                />
                {/* <Text>{JSON.stringify(addVarForm1?.values)}</Text> */}

                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    {currentStep > 0 && (
                        <CustomButtonWithBorder
                            buttonName={t('Prev')}
                            onPressButton={handlePrev}
                            bdColor={'#BFBFBF'}
                            textColor={'#20232B'}
                        />
                    )}
                    <CustomButton
                        buttonName={t('Save && continue')}
                        onPressButton={_add}
                        bgColor={'#208196'}
                        textColor={'#fff'}
                    />
                </View>
            </View>
        </View>
    );
};
export const AddVarPart8 = props => {
    const { t } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);
    // const [inspectionSign, setInspectionSign] = useState('');
    // const [epiSign, setEpiSign] = useState('');
    useEffect(() => {
        // Use useEffect to update initial values when parent data changes
        if (form.inspectionSupervisorName) {
            addVarForm1.setValues(form);
        }
    }, [form]);
    const [visible, setVisible] = useState(false);
    const [epiSignVisible, setIpiSignVisible] = useState(false);

    const hideModal = () => setVisible(false);
    const hideEpiModal = () => setIpiSignVisible(false);

    const handleChoosePhoto = async () => {
        selectPhotoFromGallary(
            setInspectionSign,
            hideModal,
            onChangeInspectionSign,
        );
    };

    const handleEPIChoosePhoto = async () => {
        selectPhotoFromGallary(setEpiSign, hideEpiModal, onChangeEpiSign);
    };
    const onChangeInspectionSign = val => {
        addVarForm1.handleChange({
            target: { name: 'inspectionSupervisorSign', value: val },
        });
    };
    const onChangeEpiSign = val => {
        addVarForm1.handleChange({ target: { name: 'epiManagerSign', value: val } });
    };
    const setInspectionSign = val => {
        addVarForm1.handleChange({ target: { name: 'inspectionSign', value: val } });
    };
    const setEpiSign = val => {
        addVarForm1.handleChange({ target: { name: 'epiSign', value: val } });
    };

    const requestCameraPermission = async val => {
        if (Platform.OS == 'ios') {
            if (val === 'inspectionSign') {
                handleCameraPhoto();
            } else {
                handleEPICameraPhoto();
            }
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    {
                        title: 'App Camera Permission',
                        message: 'App needs access to your camera ',
                        buttonNeutral: 'Ask Me Later',
                        buttonNegative: 'Cancel',
                        buttonPositive: 'OK',
                    },
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    // console.log('Camera permission given');
                    if (val === 'inspectionSign') {
                        handleCameraPhoto();
                    } else {
                        handleEPICameraPhoto();
                    }
                } else {
                    // console.log('Camera permission denied');
                }
            } catch (err) {
                console.warn(err);
            }
        }
    };

    const handleCameraPhoto = async () => {
        takePhotoFromCamera(setInspectionSign, hideModal, onChangeInspectionSign);
    };
    const handleEPICameraPhoto = async () => {
        takePhotoFromCamera(setEpiSign, hideEpiModal, onChangeEpiSign);
    };
    var validationSchema = Yup.object().shape(
        {

            contact: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            epiManagerDate: Yup.string().required(t('Required')),
            inspectionSupervisorDate: Yup.string().required(t('Required')),
            receivedDate: Yup.string().required(t('Required')),
        },
        ['contact'],
    ); // <-- HERE!!!!!!!!
    const addVarForm1 = useFormik({
        initialValues: {
            inspectionSupervisorName: 'This Sign Will Be Computer Generated',
            inspectionSupervisorSign: '',
            inspectionSupervisorDate: new Date(),
            epiManagerName: 'This Sign Will Be Computer Generated',
            epiManagerSign: '',
            epiManagerDate: new Date(),
            receivedDate: new Date(),
            contact: '',
            inspectionSign: '',
            epiSign: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });

    const _add = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        addVarForm1.handleSubmit();
    };

    const handleSubmit = async values => {
        await props.setForm(values);

        handleNext();
    };

    const onChangeInspectionDate = val => {
        addVarForm1.handleChange({
            target: { name: 'inspectionSupervisorDate', value: val },
        });
        props.setForm(addVarForm1?.values);
    };
    const onChangeEpiDate = val => {
        addVarForm1.handleChange({ target: { name: 'epiManagerDate', value: val } });
        props.setForm(addVarForm1?.values);
    };
    const onChangeReceiveDate = val => {
        addVarForm1.handleChange({ target: { name: 'receivedDate', value: val } });
        props.setForm(addVarForm1?.values);
    };
    const onChangeContact = async val => {
        addVarForm1.handleChange({ target: { name: 'contact', value: val } });
        await props.setForm(addVarForm1?.values);
    };
    const { currentStep, handleNext, handlePrev, form } = props;
    return (
        <View>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 15,
                }}>
                <Text
                    style={{
                        textAlign: 'center',
                        fontSize: 14,
                        fontFamily: fonts.MEDIUM,
                        marginBottom: 15,
                    }}>
                    {t('Part 7: Name and Signature')}
                </Text>
                {/* <InputField
                    placeholder={t('Enter here')}
                    label={t('Authorized Inspection Supervisor')}
                    inputValue={addVarForm1?.values?.inspectionSupervisorName}
                    setInputValue={addVarForm1.handleChange('inspectionSupervisorName')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addVarForm1?.errors?.inspectionSupervisorName}
                    mandatory={isMandatory}
                /> */}
                {/* <View style={{ borderWidth: 0.5, height: DeviceHeight / 10, borderRadius: 10, justifyContent: 'center' }}>

                    {addVarForm1?.values?.inspectionSign ?
                        <TouchableOpacity onPress={() => showModal()}>
                            <Image source={{ uri: addVarForm1?.values?.inspectionSign }} style={{ height: DeviceHeight / 10, width: DeviceWidth / 1.07, justifyContent: 'center', borderRadius: 10, }} />
                        </TouchableOpacity>
                        : <TouchableOpacity onPress={() => showModal()}>
                            <Text style={{ textAlign: 'center', fontWeight: 14, color: Colors.green4e }}>Upload Sign</Text>

                        </TouchableOpacity>}

                </View>
                {isMandatory && addVarForm1?.errors?.inspectionSign
                    && <Text
                        style={{
                            color: 'red',
                            fontSize: 12,
                            marginTop: 7,
                            fontFamily: fonts.MEDIUM
                        }}>{addVarForm1?.errors?.inspectionSign}</Text>
                } */}
                <DatePickerComponent
                    label={t('Authorized Inspection Date')}
                    placeholder="DD/MM/YYYY"
                    onChange={val => onChangeInspectionDate(val)}
                    val={addVarForm1?.values?.inspectionSupervisorDate}
                    errorMsg={addVarForm1?.errors?.inspectionSupervisorDate}
                    mandatory={isMandatory}
                />
                {/* <InputField
                    placeholder={t('Enter here')}
                    label={t('Central store or EPI Manager')}
                    inputValue={addVarForm1?.values?.epiManagerName}
                    setInputValue={addVarForm1.handleChange('epiManagerName')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addVarForm1?.errors?.epiManagerName}
                    mandatory={isMandatory}
                /> */}
                {/* <View style={{ borderWidth: 0.5, height: DeviceHeight / 10, borderRadius: 10, justifyContent: 'center' }}>

                    {addVarForm1?.values?.epiSign ? <TouchableOpacity onPress={() => showEpiModal()}>
                        <Image source={{ uri: addVarForm1?.values?.epiSign }} style={{ height: DeviceHeight / 10, width: DeviceWidth / 1.07, justifyContent: 'center', borderRadius: 10, }} />
                    </TouchableOpacity>
                        : <TouchableOpacity onPress={() => showEpiModal()}>
                            <Text style={{ textAlign: 'center', fontWeight: 14, color: Colors.green4e }}>Upload Sign</Text>

                        </TouchableOpacity>}

                </View>
                {isMandatory && addVarForm1?.errors?.epiSign
                    && <Text
                        style={{
                            color: 'red',
                            fontSize: 12,
                            marginTop: 7,
                            fontFamily: fonts.MEDIUM
                        }}>{addVarForm1?.errors?.epiSign}</Text>
                } */}
                <DatePickerComponent
                    label={t('Date')}
                    placeholder="DD/MM/YYYY"
                    onChange={val => onChangeEpiDate(val)}
                    val={addVarForm1?.values?.epiManagerDate}
                    errorMsg={addVarForm1?.errors?.epiManagerDate}
                    mandatory={isMandatory}
                />
                <Text
                    style={{
                        color: '#CA3737',
                        fontFamily: fonts.REGULAR,
                        marginBottom: 10,
                    }}>
                    {t('For procurement agency office use only')}
                </Text>
                <DatePickerComponent
                    label={t('Date received by the office')}
                    placeholder="DD/MM/YYYY"
                    onChange={val => onChangeReceiveDate(val)}
                    val={addVarForm1?.values?.receivedDate}
                    errorMsg={addVarForm1?.errors?.receivedDate}
                    mandatory={isMandatory}
                />

                <InputField
                    placeholder={t('Enter here')}
                    label={t('Contact person')}
                    inputValue={addVarForm1?.values?.contact}
                    setInputValue={onChangeContact}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={addVarForm1?.errors?.contact}
                    mandatory={isMandatory}
                />
                {/* <Text>{JSON.stringify(addVarForm1?.values)}</Text> */}

                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    {currentStep > 0 && (
                        <CustomButtonWithBorder
                            buttonName={t('Prev')}
                            onPressButton={handlePrev}
                            bdColor={'#BFBFBF'}
                            textColor={'#20232B'}
                        />
                    )}
                    <CustomButton
                        buttonName={t('Save && continue')}
                        onPressButton={_add}
                        bgColor={'#208196'}
                        textColor={'#fff'}
                    />
                </View>
            </View>
            <ImageUploadPopUp
                isVisible={visible}
                onClose={hideModal}
                handleChoosePhoto={handleChoosePhoto}
                requestCameraPermission={() =>
                    requestCameraPermission('inspectionSign')
                }
            />
            <ImageUploadPopUp
                isVisible={epiSignVisible}
                onClose={hideEpiModal}
                handleChoosePhoto={handleEPIChoosePhoto}
                requestCameraPermission={() => requestCameraPermission('epiSign')}
            />
        </View>
    );
};

export const TextLineComponent = props => {
    const { leftText = '', rightText = '', rightColor = '#7F7F7F' } = props;
    return (
        <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 0.5 }}>
                <Text style={{ fontSize: 14, fontFamily: fonts.REGULAR }}>
                    {leftText}
                </Text>
            </View>
            <View style={{ flex: 0.5 }}>
                <Text
                    style={{ fontSize: 14, fontFamily: fonts.REGULAR, color: rightColor }}>
                    {rightText}
                </Text>
            </View>
        </View>
    );
};
export const VarReviewComponent = props => {
    const { t } = useTranslation();

    const {
        form1 = {},
        form2 = {},
        form3 = {},
        form4 = {},
        form5 = {},
        form6 = {},
        form7 = {},
        form8 = {},
        handleNext,
        handlePrev,
        isReview = false,
        localLoader = false,
    } = props;
    const { clearingAgent = {} } = form3;
    const { products = [] } = form4;
    const { productName = '' } = form1;
    let mfgDetails = products.length ? products[0] : {};
    const { product = {} } = mfgDetails;
    return (
        <View style={{ margin: 15, marginTop: 5 }}>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    marginBottom: 10,
                    textAlign: 'centers',
                }}>
                {t('General Information')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: -5,
                }}>
                <TextLineComponent
                    leftText={t('Vaccine')}
                    rightText={productName || (product && product.name)}
                />
                <TextLineComponent leftText={t('Country')} rightText={form1.country} />
                <TextLineComponent leftText={t('Report No.')} rightText={form1.reportNo} />

                <TextLineComponent
                    leftText={t('Date of Report')}
                    rightText={formatDateDDMMYYYY(form1.reportDate)}
                />
                <TextLineComponent
                    leftText={t('Place of Inspection')}
                    rightText={form1.placeOfInspection}
                />
                <TextLineComponent
                    leftText={t('Date of Inspection')}
                    rightText={formatDateDDMMYYYYTime(form1.inspectionDateTime)}
                />
                {/* <TextLineComponent
                    leftText={t("Date of Inspection")}
                    rightText={formatDateDDMMYYYY(form1.reportDate)}
                /> */}
                <TextLineComponent
                    leftText={t('Cold Storage')}
                    rightText={form1.storageName}
                />

                {/* <TextLineComponent
                leftText='Time of Inspection#'
                rightText='3:00 PM'
            /> */}
            </View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 1: Advance Notice')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Pre Advice: Date Received by Consignee')}
                    rightText={formatDateDDMMYYYY(form2.preAdviceDate)}
                />
                <TextLineComponent
                    leftText={t('Shipping Notification: Date Received by Consignee')}
                    rightText={formatDateDDMMYYYY(form2.shippingNotificationDate)}
                />
                <TextLineComponent
                    leftText={t('Copy of Airway Bill(Awb)')}
                    rightText={form2.copyAwb ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Copy of Packing List')}
                    rightText={form2.copyPackingList ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Copy of Invoice')}
                    rightText={form2.copyInvoice ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Copy of Release Certificate')}
                    rightText={form2.copyReleaseCertificate ? t('Yes') : t('No')}
                />
            </View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 2: Flight Arrival Report')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent leftText={t('Awb Number')} rightText={form3.awb} />
                <TextLineComponent
                    leftText={t('Flight Number')}
                    rightText={form3.flightNo}
                />
                <TextLineComponent
                    leftText={t('Airport of Destination')}
                    rightText={form3.destination}
                />
                <Text
                    style={{
                        fontSize: 14,
                        color: '#208196',
                        fontFamily: fonts.MEDIUM,
                        paddingVertical: 5,
                    }}>
                    {t('ETA as per notification')}
                </Text>

                <TextLineComponent
                    leftText={t('Date & Time')}
                    rightText={formatDateDDMMYYYYTime(form3.scheduledDateTime)}
                />
                <Text
                    style={{
                        fontSize: 14,
                        color: '#208196',
                        fontFamily: fonts.MEDIUM,
                        paddingVertical: 5,
                    }}>
                    {t('Actual time of arrival')}
                </Text>

                <TextLineComponent
                    leftText={t('Date & Time')}
                    rightText={formatDateDDMMYYYYTime(form3.actualDateTime)}
                />
                <TextLineComponent
                    leftText={t('Name of clearing agent')}
                    rightText={form3.name || clearingAgent.name}
                />
                <TextLineComponent
                    leftText={t('On behalf of')}
                    rightText={form3.behalf || clearingAgent.behalf}
                />
            </View>
            {/* <ViewPart4
            form={form4}
        /> */}
            {isReview ? (
                <ViewPart4 form={form4} productName={productName} />
            ) : (
                <ViewPart4InView form={form4} />
            )}
            <ViewPart5 form={form5} />
            <ViewPart6 form={form6} />
            <ViewPart7 form={form7} />
            <ViewPart8 form={form8} />
            {localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : (
                handleNext && (
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <CustomButtonWithBorder
                            buttonName={t('Edit')}
                            onPressButton={handlePrev}
                            bdColor={'#BFBFBF'}
                            textColor={'#20232B'}
                        />
                        <CustomButton
                            buttonName={t('Submit')}
                            onPressButton={handleNext}
                            bgColor={'#208196'}
                            textColor={'#fff'}
                        />
                    </View>
                )
            )}
        </View>
    );
};

const ViewPart4 = props => {
    const { t } = useTranslation();

    const { form = {}, productName = '' } = props;
    const { products = [], diluent = [] } = form;
    const eachProd = item => {
        return (
            <View
                style={{
                    marginTop: 10,
                    backgroundColor: Colors.blue96,
                    paddingHorizontal: 10,
                    paddingBottom: 7,
                    borderRadius: 10,
                }}>
                <TextLineComponent leftText={t('Lot Number')} rightText={item.batchNo} />
                <TextLineComponent leftText={t('No. of Boxes')} rightText={item.noOfBoxes} />
                <TextLineComponent leftText={t('No. of Vials')} rightText={item.noOfUnits} />
                <TextLineComponent
                    leftText={t('Mfg Date')}
                    rightText={formatDateDDMMYYYY(item.mfgDate)}
                />
                <TextLineComponent
                    leftText={t('Exp Date')}
                    rightText={formatDateDDMMYYYY(item.expDate)}
                />
            </View>
        );
    };
    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 3 Details of vaccine shipment ')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Purchase Order Number')}
                    rightText={form.poNo}
                />
                <TextLineComponent leftText={t('Consignee')} rightText={form.consignee} />

                <TextLineComponent
                    leftText={t('Manufacturer')}
                    rightText={form.manufacturerName}
                />
                <TextLineComponent leftText={t('Country')} rightText={form.country} />
                <TextLineComponent
                    leftText={t('was No.Of Boxes received as per shipping notification?')}
                    rightText={form.shortShipment ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('if not, were details of short-shipment provided prior to vaccine arrival?')}
                    rightText={form.shortShipmentNotification ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Vaccine :')}
                    rightText={productName}
                    rightColor={Colors.blue1EA}
                />
                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={products}
                    renderItem={({ item }) => eachProd(item)}
                    keyExtractor={(item, index) => index.toString()}
                />
                {(productName === 'BCG' ||
                    productName === 'MR' ||
                    productName === 'Measles' ||
                    productName === 'Rubella') &&
                    diluent.length ? (
                    <View>
                        <Text
                            style={{
                                fontSize: 16,
                                fontFamily: fonts.MEDIUM,
                                marginTop: 10,
                                color: Colors.blue1EA,
                            }}>
                            {t('Number of Ampules')}
                        </Text>

                        <FlatList
                            contentContainerStyle={{ paddingBottom: 100 }}
                            data={diluent}
                            renderItem={({ item }) => eachProd(item)}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                ) : null}
            </View>
        </View>
    );
};

const ViewPart4InView = props => {
    const { t } = useTranslation();

    const { form = {} } = props;
    const { products = [], diluentDroppers = [] } = form;
    let mfgDetails = products.length ? products[0] : {};
    const { manufacturer = {}, product = {} } = mfgDetails;

    const eachProd = item => {
        return (
            <View
                style={{
                    marginTop: 10,
                    backgroundColor: Colors.blue96,
                    paddingHorizontal: 10,
                    paddingBottom: 7,
                    borderRadius: 10,
                }}>
                <TextLineComponent leftText={t('Lot Number')} rightText={item.batchNo} />
                <TextLineComponent leftText={t('No. of Boxes')} rightText={item.noOfBoxes} />
                <TextLineComponent
                    leftText={t('Mfg Date')}
                    rightText={formatDateDDMMYYYY(item.mfgDate)}
                />
                <TextLineComponent
                    leftText={t('Exp Date')}
                    rightText={formatDateDDMMYYYY(item.expDate)}
                />
            </View>
        );
    };
    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 3: Details of Vaccine Shipment')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Purchase Order Number')}
                    rightText={form.poNo}
                />
                <TextLineComponent leftText={t('Consignee')} rightText={form.consignee} />

                <TextLineComponent
                    leftText={t('Manufacturer')}
                    rightText={manufacturer && manufacturer?.name}
                />
                <TextLineComponent leftText="Country" rightText={form.originCountry} />
                <TextLineComponent
                    leftText={t('was No.Of Boxes received as per shipping notification?')}
                    rightText={form.shortShipment ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('if not, were details of short-shipment provided prior to vaccine arrival?')}
                    rightText={form.shortShipmentNotification ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Vaccine :')}
                    rightText={product && product.name}
                    rightColor={Colors.blue1EA}
                />
                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={products}
                    renderItem={({ item }) => eachProd(item)}
                    keyExtractor={(item, index) => index.toString()}
                />
                {diluentDroppers.length ? (
                    <View>
                        <Text
                            style={{
                                fontSize: 16,
                                fontFamily: fonts.MEDIUM,
                                marginTop: 10,
                                color: Colors.blue1EA,
                            }}>
                            {t('Number of Ampules')}
                        </Text>

                        <FlatList
                            contentContainerStyle={{ paddingBottom: 100 }}
                            data={diluentDroppers}
                            renderItem={({ item }) => eachProd(item)}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                ) : null}
            </View>
        </View>
    );
};
const ViewPart5 = props => {
    const { form = {} } = props;
    const { t } = useTranslation();

    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 4: Documents')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Invoice')}
                    rightText={form.invoice ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Packing List')}
                    rightText={form.packingList ? t('Yes') : t('No')}
                />
                {/* <TextLineComponent
                    leftText={t('Vaccine Arrival Report')}
                    rightText={form.vaccineArrivalReport ? t('Yes') : t('No')}
                /> */}
                <TextLineComponent
                    leftText={t('Release Certificate data')}
                    rightText={form.releaseCertificate ? t('Yes') : t('No')}
                />
                <TextLineComponent leftText={t('Comments')} rightText={form.comment} />
            </View>
        </View>
    );
};
const ViewPart6 = props => {
    const { t } = useTranslation();

    const { form = {} } = props;
    const { alerts = [], addAlert = true } = form;
    const eachProd = item => {
        return (
            <View
                style={{
                    marginTop: 10,
                    backgroundColor: Colors.blue96,
                    paddingHorizontal: 10,
                    paddingBottom: 7,
                    borderRadius: 10,
                }}>
                <TextLineComponent leftText={t('Lot Number')} rightText={item.batchNo} />
                <TextLineComponent leftText={t('Serial Number of Electronic Device')} rightText={item.boxNo + ''} />
                <TextLineComponent
                    leftText={t('Alarm in Electronic Device')}
                    rightText={item.temperature}
                />
                <TextLineComponent
                    leftText={t('Vaccine Vial Management')}
                    rightText={item.vvmStatus}
                />
                <TextLineComponent
                    leftText={t('Vaccine vial monitor')}
                    rightText={
                        item.vvmStatus === '1' || item.vvmStatus === '2'
                            ? t('Usable')
                            : t('non usable')
                    }
                />
                <TextLineComponent
                    leftText={t('Exp Date')}
                    rightText={formatDateDDMMYYYY(item.inspectionDateTime)}
                />
            </View>
        );
    };
    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 5: Status of Shipping Indicators')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Total Number of Boxes Inspected')}
                    rightText={form.noOfInspected}
                />
                <TextLineComponent
                    leftText={t('Invoice Amount')}
                    rightText={form.invoiceAmount}
                />
                <TextLineComponent
                    leftText={t('Coolant Type')}
                    rightText={getLabel(options1, form.coolantType)}
                />
                <TextLineComponent
                    leftText={t('Temperature Monitors Present')}
                    // rightText={getLabel(options2, form.tempMonitors)}
                    rightText={Array.isArray(form.tempMonitors) ? form.tempMonitors.join(', ') : form.tempMonitors}
                />
                {addAlert && (
                    <View>
                        <Text
                            style={{
                                fontSize: 16,
                                fontFamily: fonts.MEDIUM,
                                marginTop: 10,
                                color: Colors.blue1EA,
                            }}>
                            {t('Alerts')}
                        </Text>

                        <FlatList
                            contentContainerStyle={{ paddingBottom: 100 }}
                            data={alerts}
                            renderItem={({ item }) => eachProd(item)}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                )}
            </View>
        </View>
    );
};

const ViewPart7 = props => {
    const { form = {} } = props;
    const { t } = useTranslation();

    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 6: General Conditions of Shipment')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('What was the condition of the boxes on arrival?')}
                    rightText={form.type}
                />
                <TextLineComponent
                    leftText={t('Were necessary labels attached to the shipping boxes?')}
                    rightText={form.labelsAttached ? t('Yes') : t('No')}
                />
                <TextLineComponent
                    leftText={t('Other comments including description of alarms in electronic devices')}
                    rightText={form.comment}
                />
            </View>
        </View>
    );
};
const ViewPart8 = props => {
    const { t } = useTranslation();

    const { form = {} } = props;
    const { inspectionSupervisor = {}, epiManager = {} } = form;
    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    fontFamily: fonts.MEDIUM,
                    margin: 10,
                    textAlign: 'centers',
                }}>
                {t('Part 7: Name and Signature')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: 5,
                }}>
                <TextLineComponent
                    leftText={t('Authorized Inspection Supervisor')}
                    rightText={
                        form.inspectionSupervisorName || inspectionSupervisor.name + ''
                    }
                />
                <TextLineComponent
                    leftText={t('Authorized Inspection Date')}
                    rightText={
                        form.inspectionSupervisorDate
                            ? formatDateDDMMYYYY(form.inspectionSupervisorDate)
                            : formatDateDDMMYYYY(inspectionSupervisor.date)
                    }
                />
                <TextLineComponent
                    leftText={t('Central store or EPI Manager')}
                    rightText={form.epiManagerName || epiManager.name + ''}
                />
                <TextLineComponent
                    leftText={t('Date')}
                    rightText={
                        form.epiManagerDate
                            ? formatDateDDMMYYYY(form.epiManagerDate)
                            : formatDateDDMMYYYY(epiManager.date)
                    }
                />
                <TextLineComponent leftText={t('Contact')} rightText={form.contact} />
            </View>
        </View>
    );
};
