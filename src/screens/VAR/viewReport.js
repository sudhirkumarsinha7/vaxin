/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';
import { useRoute } from '@react-navigation/native';
import { VarReviewComponent } from '../VAR/addVarHelper';
import {
    create_Var,
} from '../../redux/action/var';
import { VirtualizedList } from '../inventory/AddInventory/productHelper';

const ViewVarComponent = props => {
    const { t } = useTranslation();

    const route = useRoute();
    const { params } = route;
    const { form1, form2, form3, form4 = {}, form5, form6, form7, form8 } = params;

    useEffect(() => {
        async function fetchDeta() { }
        fetchDeta();
    }, []);

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Vaccine Arrival Report')}
            />

            <VirtualizedList>
                <VarReviewComponent
                    form1={form1}
                    form2={form2}
                    form3={form3}
                    form4={form4}
                    form5={form5}
                    form6={form6}
                    form7={form7}
                    form8={form8}
                />
            </VirtualizedList>
        </View>
    );
};

function mapStateToProps(state) {
    return {};
}
export default connect(mapStateToProps, {
    create_Var,
})(ViewVarComponent);
