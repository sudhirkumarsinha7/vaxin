/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect, cloneElement } from 'react';
import { ScrollView, View, Text, StyleSheet } from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';
import { log_out } from '../../redux/action/auth';
import { useRoute } from '@react-navigation/native';
import { ToastShow } from '../../components/Toast';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useIsFocused } from '@react-navigation/native';
import { CustomButton } from '../../components/Common/helper';
import ProgressStepper from './ProgressStepper';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import fonts from '../../../FontFamily';
import { VarReviewComponent } from '../VAR/addVarHelper';
import {
    getCountryList,
    getAirportList,
    getVarList,
    getVarInfo,
    create_Var,
} from '../../redux/action/var';
import { PopupMessage } from '../../components/popUp';
import { localImage } from '../../config/global';
import { VirtualizedList } from '../inventory/AddInventory/productHelper';

const ReviewVarComponent = props => {
    const { t, i18n } = useTranslation();
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [isPopupVisible, setPopupVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('');
    const route = useRoute();
    const { params } = route;
    const {
        form1 = {},
        form2 = {},
        form3 = {},
        form4 = {},
        form5 = {},
        form6 = {},
        form7 = {},
        form8 = {},
    } = params;
    const { productName = '' } = form1;
    useEffect(() => {
        async function fetchDeta() { }
        fetchDeta();
    }, []);

    const handleNext = async () => {
        const { products = [], diluent = [] } = form4;
        setLocalLoader(true);
        const newProductList = products.map(each => {
            (each.productId = form1.productId),
                (each.manufacturerId = form4.manufacturerId), // manufacturerId
                (each.noOfUnits = Number(each.noOfUnits)),
                (each.noOfBoxes = Number(each.noOfBoxes)),
                (each.quantityPerUnit = Number(form4.quantityPerUnit));

            return each;
        });
        const newdiluentList = diluent.map(each => {
            (each.productId = form1.productId),
                (each.manufacturerId = form4.manufacturerId), // manufacturerId
                (each.noOfUnits = Number(each.noOfUnits)),
                (each.noOfBoxes = Number(each.noOfBoxes)),
                (each.quantityPerUnit = Number(form4.quantityPerUnit));

            return each;
        });

        let varbody = {
            inventoryId: form4.inventoryId,
            consignee: form4.consignee,
            country: form1.country,
            reportNo: form1.reportNo,
            reportDate: form1.reportDate,
            placeOfInspection: form1.placeOfInspection,
            inspectionDateTime: form1.inspectionDateTime,
            storageName: form1.storageName,
            enteredDateTime: form1.enteredDateTime,
            shipmentDetails: {
                poNo: form4.poNo,
                consignee: form4.consignee,
                originCountry: form4.country,
                shortShipment: form4.shortShipment,
                shortShipmentNotification: form4.shortShipmentNotification,
                comments: form4.comments,
                products: newProductList,
                // "diluentDroppers": newdiluentList
            },
            advanceNotice: {
                preAdviceDate: form2.preAdviceDate,
                shippingNotificationDate: form2.shippingNotificationDate,
                copyAwb: form2.copyAwb,
                copyInvoice: form2.copyInvoice,
                copyPackingList: form2.copyPackingList,
                copyReleaseCertificate: form2.copyReleaseCertificate,
            },
            flightArrivalReport: {
                awb: form3.awb,
                flightNo: form3.flightNo,
                destination: form3.destination,
                scheduledDateTime: form3.scheduledDateTime,
                actualDateTime: form3.actualDateTime,
                clearingAgent: {
                    name: form3.name,
                    behalf: form3.behalf,
                },
            },
            documents: {
                invoice: form5.invoice,
                packingList: form5.packingList,
                vaccineArrivalReport: form5.vaccineArrivalReport,
                releaseCertificate: form5.releaseCertificate,
                comment: form5.comment,
            },
            shippingIndicators: {
                noOfInspected: Number(form6.noOfInspected),
                invoiceAmount: Number(form6.invoiceAmount),
                coolantType: form6.coolantType,
                tempMonitors: form6.tempMonitors,
                alerts: form6.addAlert ? form6.alerts : [],
            },
            conditions: {
                type: form7.type,
                labelsAttached: form7.labelsAttached,
                comment: form7.comment,
            },
            signatures: {
                inspectionSupervisor: {
                    name: 'Supervisor',
                    sign: form8.inspectionSupervisorSign,
                    date: form8.inspectionSupervisorDate,
                },
                epiManager: {
                    name: "EPI",
                    sign: form8.epiManagerSign,
                    date: form8.epiManagerDate,
                },
                receivedDate: form8.receivedDate,
                contact: form8.contact,
            },
        };
        if (
            productName === 'BCG' ||
            productName === 'MR' ||
            productName === 'Measles' ||
            productName === 'Rubella'
        ) {
            varbody.shipmentDetails.diluentDroppers = newdiluentList;
        }
        console.log('create_Var res varbody' + JSON.stringify(varbody));

        const result = await props.create_Var(varbody);
        setLocalLoader(false);

        console.log('create_Var res result' + JSON.stringify(result));
        if (result?.status === 200) {
            // const orderID = result.data && result.data.data && result.data.data.id || ''
            const msg = 'var Created ';
            setSuccessMsg(msg);

            setPopupVisible(true);
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };
    const handlePrev = () => {
        // props.navigation.navigate('var')
        props.navigation.goBack();
    };
    const closPopUp = () => {
        setPopupVisible(false);
        props.navigation.navigate('var');
    };
    console.log('val ' + JSON.stringify(form8));
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Vaccine Arrival Report Review')}
            />
            <PopupMessage
                // message={successMsg}
                title={t("Var has been created Successfully")}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t("Okay")}
                image={localImage.congrats}
            />
            <VirtualizedList>
                <VarReviewComponent
                    handlePrev={handlePrev}
                    handleNext={handleNext}
                    form1={form1}
                    form2={form2}
                    form3={form3}
                    form4={form4}
                    form5={form5}
                    form6={form6}
                    form7={form7}
                    form8={form8}
                    isReview={true}
                    localLoader={localLoader}
                />
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
function mapStateToProps(state) {
    return {};
}
export default connect(mapStateToProps, {
    create_Var,
})(ReviewVarComponent);
