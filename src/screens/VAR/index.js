/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';
import Empty_Card from '../../components/Empty_Card';
import { Colors } from '../../components/Common/Style';

import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useIsFocused } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import {
    getProdList,
    getInventoryLocationList,
} from '../../redux/action/inventory';
import { getUserLocationList } from '../../redux/action/auth';
import { TextLineComponent } from './addVarHelper';
import {
    getCountryList,
    getVarList,
} from '../../redux/action/var';
import { formatDateDDMMYYYY } from '../../Util/utils';

const VARScreen = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [userToken, setToken] = useState(true);
    const insets = useSafeAreaInsets();
    const [localLoader, setLocalLoader] = useState(false);

    useEffect(() => {
        async function fetchDeta() {
            getData();
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const getData = async () => {
        setLocalLoader(true);
        await props.getCountryList();
        await props.getVarList();
        await props.getProdList();
        await props.getUserLocationList();
        await props.getInventoryLocationList();

        setLocalLoader(false);
    };
    const eachVar = item => {
        let {
            flightArrivalReport = {},
            advanceNotice = {},
            documents = {},
            shippingIndicators = {},
            conditions = {},
            signatures = {},
            shipmentDetails = {},
        } = item;
        let { products = [] } = shipmentDetails;
        let mfgDetails = products.length ? products[0] : {};

        const { manufacturer = {} } = mfgDetails;
        let { clearingAgent = {} } = flightArrivalReport;
        return (
            <View
                style={{
                    padding: 10,
                    marginHorizontal: 15,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    marginTop: 10,
                    borderRadius: 10,
                }}>
                <TouchableOpacity
                    onPress={() =>
                        props.navigation.navigate('view_var', {
                            form1: item,
                            form2: advanceNotice,
                            form3: flightArrivalReport,
                            form4: shipmentDetails,
                            form5: documents,
                            form6: shippingIndicators,
                            form7: conditions,
                            form8: signatures,
                        })
                    }>
                    <TextLineComponent leftText={t("VAR ID No ")} rightText={item.reportNo} />
                    <TextLineComponent
                        leftText={t("Date of Report")}
                        rightText={formatDateDDMMYYYY(item.reportDate)}
                    />
                    <TextLineComponent
                        leftText={t("Date of Inspection")}
                        rightText={formatDateDDMMYYYY(item.enteredDateTime)}
                    />
                    <TextLineComponent leftText={t("From")} rightText={manufacturer?.name} />
                    <TextLineComponent leftText={t("To")} rightText={clearingAgent.behalf} />
                </TouchableOpacity>
            </View>
        );
    };
    const loadMoreVar = async () => {
        await props.getVarList(var_list_page + 1);
    };
    const { var_list = [], userPermissions = {}, var_list_page = 1 } = props;
    // console.log('var_list ' + JSON.stringify(var_list))
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: '#fff',
            }}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Vaccine Arrival Report')}
            />
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : null}
            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={var_list}
                onEndReached={loadMoreVar}
                onEndReachedThreshold={10}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => eachVar(item, index)}
                ListEmptyComponent={<Empty_Card Text="No items available" />}
            />
            {userPermissions.ADD_VAR && (
                <View
                    style={{
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        marginRight: scale(15),
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 10,
                        }}>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('createVar')}
                            style={{
                                backgroundColor: Colors.whiteFF,
                                borderRadius: scale(42),
                            }}>
                            <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
function mapStateToProps(state) {
    return {
        country_list: state.Var.country_list,
        airport_list: state.Var.airport_list,
        userLocation: state.auth.userLocation,
        var_list: state.Var.var_list,
        var_list_page: state.Var.var_list_page,
        loder: state.loder,
        userPermissions: state.auth.userPermissions,
    };
}
export default connect(mapStateToProps, {
    getCountryList,
    getVarList,
    getProdList,
    getUserLocationList,
    getInventoryLocationList,
})(VARScreen);
