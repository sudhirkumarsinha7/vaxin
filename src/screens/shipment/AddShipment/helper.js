/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { scale } from '../../../components/Scale';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Colors } from '../../../components/Common/Style';
import fonts from '../../../../FontFamily';
import { formatDateDDMMYYYY } from '../../../Util/utils';
import {
    ReviewHeader,
    ReviewProductList,
} from '../../inventory/AddInventory/productHelper';
import { InputField } from '../../../components/Common/helper';
import { ToastShow } from '../../../components/Toast';
import { useTranslation } from 'react-i18next';

export const ProductListComponent = props => {
    const { products = [] } = props;
    const { t, i18n } = useTranslation();

    return (
        <View>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 16,
                    color: Colors.black0,
                    marginBottom: 10,
                }}>
                {t('Batches Added:')}
            </Text>
            <ReviewHeader />

            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={products}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => <ReviewProductList item={item} />}
            />
        </View>
    );
};

export const ShipmentProductListComponent = props => {
    const { t, i18n } = useTranslation();

    const { products = [], error = [], isMandatory = false, isPreview = true } = props;
    return (
        <View>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 16,
                    color: Colors.black0,
                    marginBottom: 10,
                }}>
                {t('Product List')}
            </Text>
            <ShipmentProductHeader />

            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={products}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (
                    <ShipmentProductList
                        item={item}
                        index={index}
                        error={error}
                        fetchBaches={props.fetchBaches}
                        delete={props.delete}
                        isMandatory={isMandatory}
                        isPreview={isPreview}
                    />
                )}
            />
        </View>
    );
};
export const ShipmentProductListWithoutOrderComponent = props => {
    const { t, i18n } = useTranslation();

    const { products = [], error = [], isMandatory = false, isPreview = true } = props;
    return (
        <View>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 16,
                    color: Colors.black0,
                    marginBottom: 10,
                }}>
                {t('Product List')}
            </Text>
            <ShipmentProductHeader />

            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={products}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => (
                    <ShipmentProductList
                        item={item}
                        index={index}
                        error={error}
                        delete={props.delete}
                        isMandatory={isMandatory}
                        isPreview={isPreview}
                    />
                )}
            />
            <AddShipmentProduct fetchBaches={props.fetchBaches} isPreview={isPreview} />
        </View>
    );
};
export const AddShipmentProduct = props => {
    return (
        <View
            style={{
                flexDirection: 'row',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{'-'}</Text>
            </View>
            <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{'-'}</Text>
            </View>
            <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{'-'}</Text>
            </View>
            {props.isPreview && <View
                style={{
                    flex: 0.1,
                    justifyContent: 'center',
                    padding: 2,
                    alignItems: 'center',
                }}>
                <TouchableOpacity onPress={() => props.fetchBaches()}>
                    <AntDesign
                        name="plussquareo"
                        color={Colors.lightGrayBlue1}
                        size={25}
                    />
                </TouchableOpacity>
            </View>}
        </View>
    );
};

export const ShipmentProductHeader = props => {
    const { t, i18n } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Name')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Quantity')}
                </Text>
            </View>
            <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Batch No')}</Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Batch Qty')}
                </Text>
            </View>
            <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Mfg. Date')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Exp. Date')}
                </Text>
            </View>
        </View>
    );
};
export const ShipmentProductList = props => {
    const { t, i18n } = useTranslation();

    const [isExtended, setExtended] = useState(false);
    const { item = {}, index, error = [], isMandatory = false, isPreview } = props;
    const { product = {}, units = '', batches = [] } = item;
    const totalQuantity = batches.reduce(
        (total, item) => total + item.batchQty,
        0,
    );

    const eachBachview = item => {
        return (
            <View
                style={{
                    flexDirection: 'row',
                    backgroundColor: '#F0F2F7',
                    borderTopWidth: 0.5,
                    borderColor: '#7F7F7F',
                    paddingVertical: 3,
                }}>
                <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }} />
                <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                    <Text style={{ fontFamily: fonts.REGULAR, fontSize: 13 }}>
                        {item.batchNo}
                    </Text>
                    {/* {item.batchQty && (units ? <Text style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: '#155897', marginTop: 5 }}>{item.batchQty + ' ' + units}</Text> : <Text style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: '#155897', marginTop: 5 }}>{item.batchQty + ' ' + productUnit.id}</Text>)} */}
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 13,
                            color: '#155897',
                            marginTop: 5,
                        }}>
                        {item.batchQty + ' ' + units}
                    </Text>
                </View>
                <View
                    style={{
                        flex: 0.25,
                        justifyContent: 'center',
                        padding: 2,
                        alignItems: 'center',
                    }}>
                    {item.mfgDate && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 12,
                                color: Colors.grayTextColor,
                            }}>
                            {formatDateDDMMYYYY(item.mfgDate)}
                        </Text>
                    )}
                    {item.expDate && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 12,
                                color: Colors.grayTextColor,
                                marginTop: 7,
                            }}>
                            {formatDateDDMMYYYY(item.expDate)}
                        </Text>
                    )}
                </View>
            </View>
        );
    };
    return (
        <View
            style={{
                borderBottomWidth: 0.5,
                borderColor: '#7F7F7F',
                justifyContent: 'center',
                paddingHorizontal: 10,
            }}>
            <View style={{ flexDirection: 'row', padding: 2 }}>
                <View style={{ flex: 0.5 }}>
                    <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                        {item.productName || product.name}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 13,
                            color: '#155897',
                            marginTop: 5,
                        }}>
                        {item.remainingQuantity
                            ? item.remainingQuantity + ' ' + product.units
                            : totalQuantity + ' ' + units}
                    </Text>
                </View>
                <View style={{ flex: 0.4 }}>
                    {batches.length ? (
                        <TouchableOpacity onPress={() => setExtended(!isExtended)}>
                            <Text
                                style={{
                                    fontFamily: fonts.REGULAR,
                                    textAlign: 'center',
                                    fontSize: 13,
                                    color: '#155897',
                                    marginTop: 5,
                                }}>
                                {' + ' + batches.length + t(' Batches')}
                            </Text>
                        </TouchableOpacity>
                    ) : null}
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 13,
                            color: '#155897',
                            marginTop: 5,
                            textAlign: 'center',
                        }}>
                        {'(' + totalQuantity + ' ' + units + ')'}
                    </Text>
                </View>
                {isPreview && <View
                    style={{
                        flex: 0.1,
                        justifyContent: 'center',
                        padding: 2,
                        alignItems: 'center',
                    }}>
                    <TouchableOpacity
                        style={{ marginBottom: 10 }}
                        onPress={() => props.delete(item)}>
                        <AntDesign name="delete" color={Colors.red} size={20} />
                    </TouchableOpacity>
                    {props.fetchBaches && (
                        <TouchableOpacity onPress={() => props.fetchBaches(item)}>
                            <AntDesign
                                name="plussquareo"
                                color={Colors.lightGrayBlue1}
                                size={25}
                            />
                        </TouchableOpacity>
                    )}
                </View>}
            </View>
            {error && error[index] && error[index].batches && isMandatory && (
                <Text
                    style={{
                        color: 'red',
                        marginTop: -5,
                        fontSize: 12,
                        fontFamily: fonts.MEDIUM,
                        marginLeft: 5,
                        marginBottom: 5,
                    }}>
                    {error && error[index] && error[index].batches}
                </Text>
            )}
            <TouchableOpacity
                style={{ alignSelf: 'center' }}
                onPress={() => setExtended(!isExtended)}>
                <AntDesign
                    name={isExtended ? 'caretup' : 'caretdown'}
                    size={20}
                    color={Colors.blueChill}
                />
            </TouchableOpacity>
            {isExtended && (
                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={batches}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item, index }) => eachBachview(item, index)}
                />
            )}
        </View>
    );
};
export const ShipmentBatchQtyComponent = props => {
    const [isExtended, setExtended] = useState(false);
    const [batchQty, setBatchQty] = useState('');
    const [isEdit, setIsEdit] = useState(false);
    const { t, i18n } = useTranslation();

    useEffect(() => {
        async function fetchData() {
            // let minQty = Math.min(MaxQty, quantity);
            // let maxBatchQty = (Math.floor(minQty / quantityPerUnit) * quantityPerUnit) + '';

            // setBatchQty(maxBatchQty)
            setBatchQty(quantityPerUnit);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isSelected]);
    const handleInputChange = text => {
        // Validate input (allow only positive integers)
        // const value = parseInt(text, 10);
        // let minQty = Math.min(MaxQty, quantity);

        // if (!isNaN(value) && value > 0 && value <= minQty) {
        //     setBatchQty(value)
        // } else {
        //     // Clear the input if it becomes empty
        //     setBatchQty('')
        // }
        setBatchQty(text);
    };
    const {
        item = {},
        MaxQty,
        addBatchLocal,
        selectedBatch = {},
        selectedProduct = {},
    } = props;
    const { product = {} } = selectedProduct;
    const { manufacturer = {}, units = '' } = product;
    const { quantity = 1, quantityPerUnit = 1, currentCampaignDetails = {}, vvmStatus = '' } = item;
    const vvmstatus = vvmStatus === 1 || vvmStatus === 2 ? t('Usable')
        : t('non usable')
    const { batches = [] } = selectedBatch;
    const selectCard = () => {
        if (/^\d+$/.test(batchQty)) {
            // Check if the value is a multiple of 10
            if (parseInt(batchQty) % quantityPerUnit === 0) {
                if (MaxQty) {
                    if (batchQty <= MaxQty) {
                        item.batchQty = batchQty;
                        addBatchLocal(item);
                        setIsEdit(false);
                    } else {
                        ToastShow(t('Invalid Qty'), 'error', 'long', 'top');
                    }
                } else {
                    item.batchQty = batchQty;
                    addBatchLocal(item);
                    setIsEdit(false);
                }
            } else {
                ToastShow(t('Invalid Qty'), 'error', 'long', 'top');
            }
        } else {
            ToastShow(t('Invalid Qty'), 'error', 'long', 'top');
        }
    };

    // const isSelected = selectedBatch._id === item._id;

    const isSelected = item =>
        batches.some(selectedItem => selectedItem.atomId === item._id);

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
                backgroundColor: isSelected(item)
                    ? Colors.lightGrayBlue
                    : Colors.whiteFF,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 7,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                    justifyContent: 'space-between',
                }}>
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 14,
                        color: Colors.grayTextColor,
                    }}>
                    {t('Category: ') + product.type}
                </Text>
                <TouchableOpacity
                    onPress={() =>
                        isSelected(item) ? props.removeBatch(item) : selectCard()
                    }>
                    <Text
                        style={{
                            fontFamily: fonts.BOLD,
                            fontSize: 14,
                            color: isSelected(item) ? Colors.red : Colors.blue96,
                        }}>
                        {isSelected(item) ? t('Remove') : t('Add')}
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={{ padding: 7 }}>
                {currentCampaignDetails && currentCampaignDetails.name && (
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 16,
                            color: Colors.blueD2,
                            marginBottom: 5,
                        }}>
                        {'Campaign : ' +
                            currentCampaignDetails.id +
                            ' - ' +
                            currentCampaignDetails.name}
                    </Text>
                )}
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.5 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {product.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginTop: 5,
                            }}>
                            {product.id}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blackTextColor,
                                marginTop: 7,
                            }}>
                            {item.quantity + ' ' + units}
                        </Text>
                    </View>
                    <View style={{ flex: 0.5, justifyContent: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blueD2,
                                marginBottom: 12,
                            }}>
                            {t('Batch No : ') + item.batchNo}
                        </Text>

                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 0.6 }}>
                                <InputField
                                    placeholder={t('Qty')}
                                    inputValue={batchQty + ''}
                                    setInputValue={handleInputChange}
                                    inputStyle={{ marginBottom: 10 }}
                                    labelStyle={{ marginBottom: 5 }}
                                    disable={!isEdit || !isSelected(item)}
                                    KeyboardType="numeric"
                                />
                            </View>
                            {/* <View style={{ flex: 0.3, justifyContent: 'center', marginLeft: 15 }}>
                            {isSelected ? <TouchableOpacity onPress={() => selectCard()}>
                                <Text style={{ fontFamily: fonts.BOLD, fontSize: 16, color: Colors.blueAE }}>{'Save'}</Text>

                            </TouchableOpacity> : null}
                        </View> */}
                            {isSelected(item) && (
                                <View
                                    style={{ flex: 0.3, justifyContent: 'center', marginLeft: 15 }}>
                                    {isEdit ? (
                                        <TouchableOpacity onPress={() => selectCard()}>
                                            <Text
                                                style={{
                                                    fontFamily: fonts.BOLD,
                                                    fontSize: 16,
                                                    color: Colors.blueAE,
                                                }}>
                                                {t('Save')}
                                            </Text>
                                        </TouchableOpacity>
                                    ) : (
                                        <TouchableOpacity onPress={() => setIsEdit(true)}>
                                            <Text
                                                style={{
                                                    fontFamily: fonts.BOLD,
                                                    fontSize: 16,
                                                    color: Colors.grey9A,
                                                }}>
                                                {t('Edit')}
                                            </Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                            )}
                        </View>

                        {isSelected(item) && (
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 10,
                                    color: Colors.black0,
                                    marginBottom: 12,
                                }}>
                                {t('Enter Multiple of ') + quantityPerUnit}
                            </Text>
                        )}
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: scale(7),
                        backgroundColor: Colors.greenF8,
                        borderRadius: 12,
                        borderColor: Colors.green8b,
                        borderWidth: 0.5,
                    }}>
                    {item.mfgDate && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {t('Mfg Date: ') + formatDateDDMMYYYY(item.mfgDate)}
                        </Text>
                    )}
                    {item.expDate && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.red67,
                                marginTop: 7,
                            }}>
                            {t('Expiry Date: ') + formatDateDDMMYYYY(item.expDate)}
                        </Text>
                    )}
                    {vvmStatus && <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            marginTop: 7,
                            color: vvmStatus === 1 || vvmStatus === 2 ? 'green' : 'red'
                        }}>
                        {t('Vaccine vial monitor') + ' : ' + vvmstatus + ' (' + vvmStatus + ') '}
                    </Text>}
                </View>
            ) : null}
        </View>
    );
};
export const ShipmentBatchQtyComponentNew = props => {
    const [isExtended, setExtended] = useState(false);
    const [batchQty, setBatchQty] = useState('');
    const [isEdit, setIsEdit] = useState(true);
    const [qtyList, setQtyList] = useState([]);
    const { t, i18n } = useTranslation();

    useEffect(() => {
        async function fetchData() {
            // let minQty = Math.min(MaxQty, quantity) + '';
            // setBatchQty(minQty)
            // const list = [];

            // for (let i = quantityPerUnit; i <= quantity; i += quantityPerUnit) {
            //     list.push(i);
            // }
            // setQtyList(list)
            // if (list.length > 0) {
            //     setBatchQty(list[0])
            // }
            setBatchQty(quantityPerUnit);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isSelected]);
    const handleInputChange = value => {
        setBatchQty(value);
    };
    const {
        item = {},
        MaxQty,
        addBatchLocal,
        selectedProdcucts = [],
        product = {},
    } = props;
    const { units = '' } = product;
    const { quantity = 1, quantityPerUnit = 1, currentCampaignDetails = {}, vvmStatus = '' } = item;
    const vvmstatus = vvmStatus === 1 || vvmStatus === 2 ? t('Usable')
        : t('non usable')
    const selectCard = () => {
        if (/^\d+$/.test(batchQty)) {
            // Check if the value is a multiple of 10
            if (parseInt(batchQty) % quantityPerUnit === 0) {
                item.batchQty = batchQty;
                addBatchLocal(item);
                setIsEdit(false);
            } else {
                ToastShow(t('Invalid Qty'), 'error', 'long', 'top');
            }
        } else {
            ToastShow(t('Invalid Qty'), 'error', 'long', 'top');
        }
    };

    const selectedBachByProduct = selectedProdcucts.filter(each => {
        if (each.productId === product._id) {
            return each;
        }
    });
    const selectedBaches =
        (selectedBachByProduct &&
            selectedBachByProduct[0] &&
            selectedBachByProduct[0].batches) ||
        [];
    const isSelected = item =>
        selectedBaches.some(selectedItem => selectedItem.atomId === item._id);

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
                backgroundColor: isSelected(item)
                    ? Colors.lightGrayBlue
                    : Colors.whiteFF,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 7,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                    justifyContent: 'space-between',
                }}>
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 14,
                        color: Colors.grayTextColor,
                    }}>
                    {'Category: ' + product.type}
                </Text>
                <TouchableOpacity
                    onPress={() =>
                        isSelected(item) ? props.removeBatch(item) : selectCard()
                    }>
                    <Text
                        style={{
                            fontFamily: fonts.BOLD,
                            fontSize: 14,
                            color: isSelected(item) ? Colors.red : Colors.blue96,
                        }}>
                        {isSelected(item) ? t('Remove') : t('Add')}
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={{ padding: 7 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.6 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {product.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginTop: 5,
                            }}>
                            {product.id}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blackTextColor,
                                marginTop: 7,
                            }}>
                            {quantity + ' ' + units}
                        </Text>
                    </View>

                    <View style={{ flex: 0.5, justifyContent: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blueD2,
                                marginBottom: 12,
                            }}>
                            {t('Batch No : ') + item.batchNo}
                        </Text>

                        <View style={{ flexDirection: 'row' }}>
                            {isSelected(item) && (
                                <View style={{ flex: 0.6 }}>
                                    <InputField
                                        placeholder={t('Qty')}
                                        inputValue={batchQty + ''}
                                        setInputValue={handleInputChange}
                                        inputStyle={{ marginBottom: 10 }}
                                        labelStyle={{ marginBottom: 5 }}
                                        disable={!isEdit || !isSelected(item)}
                                        KeyboardType="numeric"
                                    />
                                </View>
                            )}
                            {isSelected(item) && (
                                <View
                                    style={{ flex: 0.3, justifyContent: 'center', marginLeft: 15 }}>
                                    {isEdit ? (
                                        <TouchableOpacity onPress={() => selectCard()}>
                                            <Text
                                                style={{
                                                    fontFamily: fonts.BOLD,
                                                    fontSize: 16,
                                                    color: Colors.blueAE,
                                                }}>
                                                {t('Save')}
                                            </Text>
                                        </TouchableOpacity>
                                    ) : (
                                        <TouchableOpacity onPress={() => setIsEdit(true)}>
                                            <Text
                                                style={{
                                                    fontFamily: fonts.BOLD,
                                                    fontSize: 16,
                                                    color: Colors.grey9A,
                                                }}>
                                                {t('Edit')}
                                            </Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                            )}
                        </View>
                        {isSelected(item) && (
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 10,
                                    color: Colors.black0,
                                    marginBottom: 12,
                                }}>
                                {t('Enter Multiple of ') + quantityPerUnit}
                            </Text>
                        )}
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: scale(7),
                        backgroundColor: Colors.greenF8,
                        borderRadius: 12,
                        borderColor: Colors.green8b,
                        borderWidth: 0.5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Mfg Date: ') + formatDateDDMMYYYY(item.mfgDate)}
                    </Text>
                    {item.expDate && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.red67,
                                marginTop: 7,
                            }}>
                            {t('Expiry Date: ') + formatDateDDMMYYYY(item.expDate)}
                        </Text>
                    )}
                    {vvmStatus && <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            marginTop: 7,
                            color: vvmStatus === 1 || vvmStatus === 2 ? 'green' : 'red'
                        }}>
                        {t('Vaccine vial monitor') + ' : ' + vvmstatus + ' (' + vvmStatus + ') '}
                    </Text>}
                </View>
            ) : null}
        </View>
    );
};
