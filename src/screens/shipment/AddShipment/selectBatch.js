/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    ActivityIndicator,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useIsFocused } from '@react-navigation/native';
import { CustomButton } from '../../../components/Common/helper';
import { getProdListByBatch } from '../../../redux/action/inventory';
import fonts from '../../../../FontFamily';
import { ShipmentBatchQtyComponent } from './helper';
import { ToastShow } from '../../../components/Toast';
export function filteredExpiredBatchArray(array) {
    return array.filter(item => {
        // Check if 'expDate' exists and is not null
        if (item.expDate) {
            // Get the current date
            const currentDate = new Date();

            // Get the 'expDate' as a Date object
            const expDate = new Date(item.expDate);

            // Compare 'expDate' with the current date
            return expDate > currentDate;
        } else {
            return item;
        }
        // eslint-disable-next-line no-unreachable
        return false;
    });
}
const SelectBatch = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [batchList, setBatchList] = useState([]);
    const [selectedBatch, setSelectedBatch] = useState([]);
    const [localLoader, setLocalLoader] = useState(false);
    const [totalBatchQty, setTotalBatchQty] = useState(0);

    const { selectedProduct = {} } = props;
    const { product = {} } = selectedProduct;
    const { manufacturer = {}, units = '' } = product;

    useEffect(() => {
        async function fetchData() {
            setLocalLoader(true);
            // console.log('changeAfterSelectProduct selectedProduct ', product);

            const response = await props.getProdListByBatch(product._id);
            // console.log('changeAfterSelectProduct result ', response);
            if (
                response &&
                response.data &&
                response.data.data &&
                response.data.data.data
            ) {
                let getBacheslist =
                    (response.data && response.data.data && response.data.data.data) ||
                    [];
                // const uniqueArray = filterUniqueObjectsByProperty(getBacheslist, 'batchNo');
                const uniqueArray = filteredExpiredBatchArray(getBacheslist);
                setBatchList(uniqueArray);
            } else {
                setBatchList([]);
            }
            setSelectedBatch(selectedProduct);
            setLocalLoader(false);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const addBatchLocal = item => {
        let existingProd = selectedBatch;
        setLocalLoader(true);

        if (item.batchQty > 0) {
            const newQty = totalBatchQty + Number(item.batchQty);
            setTotalBatchQty(newQty);
            const newBatch = {
                quantity: Number(item.batchQty),
                batchNo: item.batchNo,
                atomId: item._id,

                // units: selectedProduct.units,
                mfgDate: item.mfgDate,
                expDate: item.expDate,
                batchQty: Number(item.batchQty),
            };
            // console.log('existingProd ' + JSON.stringify(existingProd))

            if (existingProd.batches && existingProd.batches.length) {
                let batchlist = existingProd.batches;
                const batchindex = batchlist.findIndex(
                    each => each.atomId === item._id,
                );
                if (batchindex !== -1) {
                    batchlist[batchindex] = newBatch;
                } else {
                    let newProduct = {
                        productId: product._id,
                        productName: product.name,
                        type: product.type,
                        units: product.units,
                        batches: [...batchlist, newBatch],
                    };
                    // existingArray.push(newProduct);
                    existingProd = newProduct;
                }
                setSelectedBatch(existingProd);
            } else {
                let newProduct = {
                    productId: product._id,
                    productName: product.name,
                    type: product.type,
                    units: product.units,
                    batches: [newBatch],
                };
                // console.log('newProduct ' + JSON.stringify(newProduct))

                setSelectedBatch(newProduct);
            }
        } else {
            // removeBatch(item)
            ToastShow(t('Quantity should be greater than 0'), 'error', 'long', 'top');
        }
        setLocalLoader(false);
    };
    const removeBatch = item => {
        let existingProd = selectedBatch;
        let newlist = existingProd?.batches?.filter(
            batch => batch.atomId !== item._id,
        );
        // existingProd.batches = newlist
        // console.log('removeBatch ' + JSON.stringify(item))
        // console.log('existingProd' + JSON.stringify(existingProd))
        // console.log('newlist ' + JSON.stringify(newlist))
        let newProduct = {
            productId: product._id,
            productName: product.name,
            type: product.type,
            units: product.units,
            batches: newlist,
        };
        setSelectedBatch(newProduct);
    };

    const confirmBatch = () => {
        if (selectedProduct.remainingQuantity) {
            if (Number(totalQuantity) <= Number(selectedProduct.remainingQuantity)) {
                props.addBatchInList(selectedBatch, selectedProduct);
            } else {
                const msg =
                    t('Sending Qauntity should be less than or equal to order Qauntity');
                ToastShow(t(msg), 'error', 'long', 'top');
            }
        } else {
            if (Number(totalQuantity)) {
                props.addBatchInList(selectedBatch, selectedProduct);
            }
        }
    };
    const { batches = [] } = selectedBatch;
    const totalQuantity = batches.reduce(
        (total, item) => total + item.batchQty,
        0,
    );

    console.log('totalQuantity ' + totalQuantity);
    // console.log('selectedBatch ', selectedBatch)

    return (
        <View style={styles.container}>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 10,
                }}>
                <View style={{ alignItems: 'center', marginBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                color: Colors.black0,
                                marginRight: 20,
                            }}>
                            {t('Select Batch')}
                        </Text>
                        <TouchableOpacity onPress={() => props.cancelBatch()}>
                            <AntDesign name="close" color={'#000'} size={25} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View
                    style={{
                        backgroundColor: Colors.whiteFF,
                        borderTopLeftRadius: 10,
                        borderTopEndRadius: 10,
                        paddingBottom: 5,
                    }}>
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 12,
                        }}>
                        <View
                            style={{
                                borderRadius: 2,
                                width: scale(72),
                                height: scale(3),
                                backgroundColor: '#9B9B9B',
                            }}
                        />
                    </View>
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        margin: 15,
                    }}>
                    <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                        {product.name}
                    </Text>
                    {selectedProduct.remainingQuantity && (
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                                color: '#155897',
                                marginTop: 5,
                            }}>
                            {selectedProduct.remainingQuantity + ' ' + units}
                        </Text>
                    )}
                </View>

                {totalQuantity && !localLoader ? (
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 15,
                            color: Colors.green00,
                            justifyContent: 'center',
                        }}>
                        {t('Total Sending Quantity : ') + totalQuantity + ' ' + units}
                    </Text>
                ) : null}
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                    }}>
                    <Text style={{ fontFamily: fonts.BOLD, fontSize: 16 }}>
                        {batchList.length + t(' Batches')}
                    </Text>

                    <CustomButton
                        buttonName={t('Confirm')}
                        onPressButton={() => confirmBatch()}
                        bgColor={Colors.blueAE}
                        textColor={Colors.whiteFF}
                    />
                </View>
                {localLoader ? (
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 1,
                        }}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                ) : null}
                {/* <FlatList
contentContainerStyle={{ paddingBottom: 100 }}
                data={batchList}
                renderItem={({ item }) => <ShipmentBatchQtyComponent
                    item={item}
                    navigation={props.navigation}
                    addBatchLocal={addBatchLocal}
                    selectedBatch={selectedBatch}
                    MaxQty={selectedProduct.remainingQuantity}
                    selectedProduct={selectedProduct}
                />}
                keyExtractor={(item, index) => index.toString()}
            /> */}
                <ScrollView>
                    {batchList.map(item => {
                        return (
                            <ShipmentBatchQtyComponent
                                item={item}
                                navigation={props.navigation}
                                addBatchLocal={addBatchLocal}
                                selectedBatch={selectedBatch}
                                MaxQty={selectedProduct.remainingQuantity}
                                selectedProduct={selectedProduct}
                                removeBatch={removeBatch}
                            />
                        );
                    })}
                    <View style={{ height: 200 }} />
                </ScrollView>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

function mapStateToProps(state) {
    return {};
}
export default connect(mapStateToProps, {
    getProdListByBatch,
})(SelectBatch);
