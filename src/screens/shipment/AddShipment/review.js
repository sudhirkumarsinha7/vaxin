/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    ActivityIndicator,
    Keyboard,
    Text
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import {
    VirtualizedList,
} from '../../inventory/AddInventory/productHelper';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useRoute, useIsFocused, useNavigation } from '@react-navigation/native';
import { localImage } from '../../../config/global';
import DropDown, { CampaignDropdown } from '../../../components/Dropdown';
import {
    InputField,
    CustomButtonWithBorder,
    SubHeaderComponent,
    CustomButton,
} from '../../../components/Common/helper';
import {
    DatePickerComponent,
} from '../../../components/DatePicker';
import { getProdList, getInStockList } from '../../../redux/action/inventory';

import {
    create_shipment,
    getShipmentInboudSummary,
    getInboudShipmentAnalytcs,
    getShipmentInboundDamaged,
    getShipmentInboundDelivery,
    getShipmentInboundShipped,
    getShipmentOutboundSummary,
    getOutboudShipmentAnalytcs,
    getShipmentOutboudDamaged,
    getShipmentOutboundDelivered,
    getShipmentOutboundShiped,
} from '../../../redux/action/shipment';
import { getInboundAcceptedPartial } from '../../../redux/action/order';
import { getUserLocationList, getUserLocation } from '../../../redux/action/auth';

import { LocationComponent } from '../../order/AddOrder/helper';
import SelectBatchComponent from './selectBatch';
import SelectBatchWithoutOrderComponent from './selectBatchWithoutOrder';

import {
    ShipmentProductListComponent,
    ShipmentProductListWithoutOrderComponent,
} from './helper';
import { ToastShow } from '../../../components/Toast';
import { PopupMessage, ConfirmationPopUp } from '../../../components/popUp';
import {
    getCampaignList,
    getActiveCampaignList,
} from '../../../redux/action/campaign';

// Function to replace an object based on a condition
export const replaceObject = (existingArray, condition, newData) => {
    const updatedArray = existingArray.map(item => {
        // Check if the condition is met
        if (condition(item)) {
            // If the condition is met, replace the object with newData
            return newData;
        }
        // If the condition is not met, keep the object unchanged
        return item;
    });

    // Update the existing array with the updated array
    existingArray = updatedArray;

    return existingArray;
    // Log the updated array for verification
};

export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}

const ReviewShipment = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [isPopupVisible, setPopupVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('Shipment ID: ');
    const [shipmentData, setshipmentData] = useState({});
    const navigation = useNavigation();

    const route = useRoute();
    const { params } = route;
    const { values1 = {}, selectedOrder = {} } = params;

    useEffect(() => {
        async function fetchData() {
            if (values1.products) {
                setshipmentData(values1)
            }
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const reFreshList = async () => {
        // await props.getShipmentInboudSummary();
        // await props.getInboudShipmentAnalytcs();
        // await props.getShipmentInboundDamaged();
        // await props.getShipmentInboundDelivery();
        // await props.getShipmentInboundShipped();
        // await props.getShipmentOutboundSummary();
        // await props.getOutboudShipmentAnalytcs();
        // await props.getShipmentOutboudDamaged();
        // await props.getShipmentOutboundDelivered();
        // await props.getShipmentOutboundShiped();
    };




    const _create_shipment = async () => {
        setLocalLoader(true);


        const result = await props.create_shipment(values1);
        setLocalLoader(false);

        // console.log('create_shipment  result' + JSON.stringify(result));
        if (result?.status === 200) {
            const ID =
                (result.data && result.data.data && result.data.data.id) || '';
            const msg = t('Shipment ID: ') + ID;
            setSuccessMsg(msg);

            setPopupVisible(true);
            await reFreshList();
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }

    };

    const closPopUp = () => {
        setPopupVisible(false);
        // props.navigation.navigate('outboundshipment')
        navigation.navigate('shipment');
    };




    const {
        userLocationList = {},
        inbound_accepted_partial_list = [],
        campaign_active_list = [],
    } = props;

    const { source = {}, destination = {} } = selectedOrder;

    const { locations = [], childLocations = [] } = userLocationList;
    console.log('shipmentData' + JSON.stringify(shipmentData))
    const parentLocations = locations.map(location => location.parentLocation);

    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Reveiw Shipment')}
                back={() => navigation.goBack()}
            />

            <PopupMessage
                message={successMsg}
                title={t('Shipment has been created Successfully')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Okay')}
                image={localImage.congrats}
            />
            <VirtualizedList>
                <View style={{ padding: 15 }}>
                    {selectedOrder?.id && <DropDown
                        dropdownData={inbound_accepted_partial_list}
                        onChangeValue={() => console.log()}
                        label="id"
                        mapKey="id"
                        val={selectedOrder?.id}
                        labelText={t('Order ID')}
                        disabled={true}
                    />}

                    {shipmentData?.campaignId && <CampaignDropdown
                        dropdownData={campaign_active_list}
                        onChangeValue={() => console.log()}
                        label="name"
                        mapKey="_id"
                        val={shipmentData?.campaignId}
                        placeholderText={t('Campaign')}
                        labelText={t('Campaign')}
                        disable={true}
                    />}
                    <View style={{ marginTop: 10 }} />
                    <SubHeaderComponent name={t('From')} textColor={'#000'} />
                    {shipmentData.orderId ? (
                        <LocationComponent userLocation={destination} />
                    ) : (
                        <View>
                            <DropDown
                                dropdownData={locations}
                                onChangeValue={() => console.log()}
                                label="name"
                                mapKey="_id"
                                val={shipmentData.from_org_loc}
                                placeholderText={t('Select location')}
                                labelText={t('Organization location')}
                                disabled={true}


                            />
                        </View>
                    )}

                    <SubHeaderComponent name={t('To')} textColor={'#000'} />

                    {shipmentData.orderId ? (
                        <LocationComponent userLocation={source} />
                    ) : (
                        <View>
                            <DropDown
                                dropdownData={
                                    childLocations.length ? childLocations : parentLocations
                                }
                                onChangeValue={() => console.log()}
                                label="name"
                                mapKey="_id"
                                val={shipmentData.to_org_loc}
                                placeholderText={t('Select location')}
                                labelText={t('Organization location')}

                                disabled={true}

                            />
                        </View>
                    )}

                    <View style={{ marginTop: 10 }} />

                    <SubHeaderComponent
                        name={t('Delivery Details')}
                        textColor={'#000'}
                    />
                    <View style={{ marginTop: 10 }} />

                    <InputField
                        placeholder={t('Transit Number')}
                        label={t('Transit Number')}
                        inputValue={shipmentData.transitNo}
                        setInputValue={() => console.log()}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        disable={true}

                    />


                    <DatePickerComponent
                        label={t('Shipment Date')}
                        placeholder="DD/MM/YYYY"
                        onChange={() => console.log()}
                        val={shipmentData.shippingDate}
                        disabled={true}
                    />
                    {shipmentData.shippingDate ? (
                        <DatePickerComponent
                            label={t('Estimated Delivery Date')}
                            placeholder="DD/MM/YYYY"
                            onChange={() => console.log()}
                            val={shipmentData.est_date}
                            disabled={true}

                        />
                    ) : null}
                    {shipmentData.orderId ||
                        shipmentData.campaignId ? (
                        shipmentData.products.length ? (
                            <ShipmentProductListComponent
                                products={shipmentData.products}
                                delete={() => console.log()}
                                fetchBaches={() => console.log()}
                                isPreview={false}
                            />
                        ) : null
                    ) : (
                        <ShipmentProductListWithoutOrderComponent
                            products={shipmentData.products}
                            delete={() => console.log()}
                            fetchBaches={() => console.log()}
                            isPreview={false}

                        />
                    )}

                    {props.loder || localLoader ? (
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    ) : (
                        <View
                            style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                            <CustomButtonWithBorder
                                buttonName={t('Back')}
                                onPressButton={() => navigation.goBack()}
                                bdColor={'#BFBFBF'}
                                textColor={'#20232B'}
                            />
                            <CustomButton
                                buttonName={t('Confirm')}
                                onPressButton={_create_shipment}
                                bgColor={'#208196'}
                                textColor={'#fff'}
                            />
                        </View>
                    )}
                </View>
            </VirtualizedList>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        inv_product_list: state.inventory.inv_product_list,
        userLocation: state.auth.userLocation,
        userLocationList: state.auth.userLocationList,
        inbound_accepted_partial_list: state.order.inbound_accepted_partial_list,
        outbound_accepted_list: state.order.outbound_accepted_list,
        inv_stock_list: state.inventory.inv_stock_list,
        campaign_list: state.campaign.campaign_list,
        campaign_active_list: state.campaign.campaign_active_list,
    };
}
export default connect(mapStateToProps, {
    create_shipment,
    getInboundAcceptedPartial,
    getShipmentInboudSummary,
    getInboudShipmentAnalytcs,
    getShipmentInboundDamaged,
    getShipmentInboundDelivery,
    getShipmentInboundShipped,
    getShipmentOutboundSummary,
    getOutboudShipmentAnalytcs,
    getShipmentOutboudDamaged,
    getShipmentOutboundDelivered,
    getShipmentOutboundShiped,
    getUserLocationList,
    getUserLocation,
    getInStockList,
    getCampaignList,
    getActiveCampaignList,
})(ReviewShipment);
