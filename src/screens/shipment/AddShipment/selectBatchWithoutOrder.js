/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useIsFocused } from '@react-navigation/native';
import { CustomButton } from '../../../components/Common/helper';
import { getProdListByBatch } from '../../../redux/action/inventory';
import fonts from '../../../../FontFamily';
import { ShipmentBatchQtyComponentNew } from './helper';
import { ToastShow } from '../../../components/Toast';
import DropDown from '../../../components/Dropdown';
import { formatNumber } from '../../../Util/utils';

export function filteredExpiredBatchArray(array) {
    return array.filter(item => {
        // Check if 'expDate' exists and is not null
        if (item.expDate) {
            // Get the current date
            const currentDate = new Date();

            // Get the 'expDate' as a Date object
            const expDate = new Date(item.expDate);

            // Compare 'expDate' with the current date
            return expDate > currentDate;
        } else {
            return item;
        }
        // eslint-disable-next-line no-unreachable
        return false;
    });
}
function groupAndStoreByCategory(data) {
    return data.reduce((result, item) => {
        const categoryValue = item.product && item.product.type;
        let product = item.product;
        product.quantity = item.quantity;

        if (!(categoryValue in result)) {
            // If not, create a new object with the inner array
            result[categoryValue] = { type: categoryValue, productsByType: [] };
        }

        // Push the current item to the inner array of the corresponding category
        result[categoryValue].productsByType.push(product);

        return result;
    }, {});
}
function groupAndStoreByCategorynew(data) {
    return data.reduce((result, item) => {
        const categoryValue =
            item.product && item.product[0] && item.product[0].type;
        let product = item.product[0];
        product.quantity = item.quantity;

        if (!(categoryValue in result)) {
            // If not, create a new object with the inner array
            result[categoryValue] = { type: categoryValue, productsByType: [] };
        }

        // Push the current item to the inner array of the corresponding category
        result[categoryValue].productsByType.push(product);

        return result;
    }, {});
}

const SelectBatchWithoutOrder = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [batchList, setBatchList] = useState([]);
    const [productTypes, setProductTypes] = useState([]);
    const [selectedProdType, setSelectedProdType] = useState([]);
    const [selectedProd, setSelectedProd] = useState([]);
    const [selectedProduct, setSelectedProduct] = useState([]);
    const [selectedBatchList, setSelectedBatchList] = useState([]);

    const [productListByType, setProductListByType] = useState([]);
    const { inv_stock_list = [], existingProd = [] } = props;
    const [localLoader, setLocalLoader] = useState(false);

    useEffect(() => {
        async function fetchData() {
            const result = groupAndStoreByCategory(inv_stock_list);
            const resultArray = Object.values(result);
            // console.log('inv_stock_list  resultArray' + JSON.stringify(resultArray));

            setProductTypes(resultArray);

            if (existingProd.length) {
                setSelectedBatchList(existingProd);
            }
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const addBatchLocal = item => {
        // setSelectedBatchList((prevDataArray) => [...prevDataArray, item]);
        const existingArray = selectedBatchList;

        const newObj = item;
        if (item.batchQty > 0) {
            const index = existingArray.findIndex(
                each1 => each1.productId === newObj.productId,
            );
            const newBatch = {
                quantity: Number(item.batchQty),
                batchNo: item.batchNo,
                atomId: item._id,

                // units: selectedProduct.units,
                mfgDate: item.mfgDate,
                expDate: item.expDate,
                batchQty: Number(item.batchQty),
            };
            // console.log('index ' + JSON.stringify(index))
            // console.log('newObj ' + JSON.stringify(newObj))

            if (index !== -1) {
                // If the object with the same id exists, replace it
                let batchlist = existingArray[index].batches;
                // console.log('batchlist ' + JSON.stringify(batchlist))
                // console.log('batchlist.length ' + JSON.stringify(batchlist.length))
                // console.log('newObj ' + JSON.stringify(newObj))

                if (batchlist.length) {
                    const batchindex = batchlist.findIndex(
                        each => each.atomId === newObj._id,
                    );
                    // console.log('batchindex ' + JSON.stringify(batchindex));
                    if (batchindex !== -1) {
                        batchlist[batchindex] = newBatch;
                    } else {
                        let newProduct = {
                            productId: selectedProduct._id,
                            productName: selectedProduct.name,
                            type: selectedProduct.type,
                            units: selectedProduct.units,
                            batches: [...batchlist, newBatch],
                        };
                        // existingArray.push(newProduct);
                        existingArray[index] = newProduct;
                    }
                }

                // existingArray[index] = newObj;
            } else {
                // If the object with the same id doesn't exist, add the new object
                let newProduct = {
                    productId: selectedProduct._id,
                    productName: selectedProduct.name,
                    type: selectedProduct.type,
                    units: selectedProduct.units,
                    batches: [newBatch],
                };
                // console.log('newProduct ' + JSON.stringify(newProduct))

                existingArray.push(newProduct);
            }
            setSelectedBatchList(existingArray);
        } else {
            removeBatch(item);
            ToastShow(t('Quantity should be greater than 0'), 'error', 'long', 'top');
        }

        // console.log('item ' + JSON.stringify(item))
    };
    const removeBatch = item => {
        let existingArray = selectedBatchList;

        let newlist = existingArray.map(product => {
            if (product.productId === item.productId) {
                product.batches = product.batches.filter(
                    batch => batch.atomId !== item._id,
                );
            }
            return product;
        });
        newlist = newlist.filter(product => product.batches.length > 0);
        setSelectedBatchList(newlist);
        // console.log('newlist ' + JSON.stringify(newlist))
        // console.log('existingArray ' + JSON.stringify(existingArray))
    };

    const changeAfterSelectProdType = item => {
        const { value, eachItem } = item;
        setSelectedProdType(value);
        setProductListByType(eachItem.productsByType);
        setBatchList([]);
        setSelectedProd('');
    };
    const changeAfterSelectProdlist = async item => {
        const { value, eachItem } = item;
        setSelectedProd(value);
        setSelectedProduct(eachItem);
        setBatchList([]);
        // console.log('changeAfterSelectProdlist  item ', item)
        setLocalLoader(true);

        const response = await props.getProdListByBatch(value);
        // console.log('inv_stock_list  resultArray ', response);
        if (
            response &&
            response.data &&
            response.data.data &&
            response.data.data.data
        ) {
            let getBacheslist =
                (response.data && response.data.data && response.data.data.data) || [];
            // const uniqueArray = filterUniqueObjectsByProperty(getBacheslist, 'batchNo');
            const uniqueArray = filteredExpiredBatchArray(getBacheslist);
            setBatchList(uniqueArray);
        } else {
            setBatchList([]);
        }
        setLocalLoader(false);
    };

    const confirmBatch = () => {
        props.addBatchInList(selectedBatchList);
        // console.log('selectedBatchList ' + JSON.stringify(selectedBatchList))
    };
    // console.log('batchList ' + JSON.stringify(batchList))
    return (
        <View style={styles.container}>
            <View
                style={{
                    flex: 1,
                    backgroundColor: '#fff',
                    borderRadius: 20,
                    padding: 10,
                }}>
                <View style={{ alignItems: 'center', marginBottom: 10 }}>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-around' }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                color: Colors.black0,
                                marginRight: 20,
                            }}>
                            {t('Select Batch')}
                        </Text>
                        <TouchableOpacity onPress={() => props.cancelBatch()}>
                            <AntDesign name="close" color={'#000'} size={25} />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView>
                    <DropDown
                        dropdownData={productTypes}
                        onChangeValue={changeAfterSelectProdType}
                        label="type"
                        mapKey="type"
                        val={selectedProdType}
                        placeholderText={t('Select Category')}
                        labelText={t('Category')}
                    />
                    <DropDown
                        dropdownData={productListByType}
                        onChangeValue={changeAfterSelectProdlist}
                        label="name"
                        mapKey="_id"
                        val={selectedProd}
                        placeholderText={t('Select Product')}
                        labelText={t('Product')}
                    />
                    <View
                        style={{
                            backgroundColor: Colors.whiteFF,
                            borderTopLeftRadius: 10,
                            borderTopEndRadius: 10,
                            paddingBottom: 5,
                        }}>
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: 12,
                            }}>
                            <View
                                style={{
                                    borderRadius: 2,
                                    width: scale(72),
                                    height: scale(3),
                                    backgroundColor: '#9B9B9B',
                                }}
                            />
                        </View>
                    </View>
                    {selectedProduct.quantity && (
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                alignSelf: 'center',
                                marginTop: 8,
                            }}>
                            {t('Current Stock ') + formatNumber(selectedProduct.quantity)}
                        </Text>
                    )}

                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-evenly',
                            alignItems: 'center',
                        }}>
                        <Text style={{ fontFamily: fonts.BOLD, fontSize: 16 }}>
                            {batchList.length + t(' Batches')}
                        </Text>

                        <CustomButton
                            buttonName={t('Confirm')}
                            onPressButton={() => confirmBatch()}
                            bgColor={Colors.blueAE}
                            textColor={Colors.whiteFF}
                        />
                    </View>

                    {batchList.map(item => {
                        return (
                            <ShipmentBatchQtyComponentNew
                                item={item}
                                navigation={props.navigation}
                                addBatchLocal={addBatchLocal}
                                selectedProdcucts={selectedBatchList}
                                MaxQty={selectedProduct.quantity}
                                removeBatch={removeBatch}
                                product={selectedProduct}
                            />
                        );
                    })}
                    <View style={{ height: 200 }} />
                </ScrollView>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

function mapStateToProps(state) {
    return {
        inv_stock_list: state.inventory.inv_stock_list,
    };
}
export default connect(mapStateToProps, {
    getProdListByBatch,
})(SelectBatchWithoutOrder);
