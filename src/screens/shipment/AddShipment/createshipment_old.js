/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    ActivityIndicator,
    Keyboard,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import {
    VirtualizedList,
} from '../../inventory/AddInventory/productHelper';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useRoute } from '@react-navigation/native';
import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../../config/global';
import DropDown, { CampaignDropdown } from '../../../components/Dropdown';
import {
    InputField,
    CustomButtonWithBorder,
    SubHeaderComponent,
    CustomButton,
} from '../../../components/Common/helper';
import {
    DatePickerComponent,
} from '../../../components/DatePicker';
import { getProdList, getInStockList } from '../../../redux/action/inventory';

import {
    create_shipment,
    getShipmentInboudSummary,
    getInboudShipmentAnalytcs,
    getShipmentInboundDamaged,
    getShipmentInboundDelivery,
    getShipmentInboundShipped,
    getShipmentOutboundSummary,
    getOutboudShipmentAnalytcs,
    getShipmentOutboudDamaged,
    getShipmentOutboundDelivered,
    getShipmentOutboundShiped,
} from '../../../redux/action/shipment';
import { getInboundAcceptedPartial } from '../../../redux/action/order';
import { getUserLocationList, getUserLocation } from '../../../redux/action/auth';

import { LocationComponent } from '../../order/AddOrder/helper';
import SelectBatchComponent from './selectBatch';
import SelectBatchWithoutOrderComponent from './selectBatchWithoutOrder';

import {
    ShipmentProductListComponent,
    ShipmentProductListWithoutOrderComponent,
} from './helper';
import { ToastShow } from '../../../components/Toast';
import { PopupMessage, ConfirmationPopUp } from '../../../components/popUp';
import {
    getCampaignList,
    getActiveCampaignList,
} from '../../../redux/action/campaign';

// Function to replace an object based on a condition
export const replaceObject = (existingArray, condition, newData) => {
    const updatedArray = existingArray.map(item => {
        // Check if the condition is met
        if (condition(item)) {
            // If the condition is met, replace the object with newData
            return newData;
        }
        // If the condition is not met, keep the object unchanged
        return item;
    });

    // Update the existing array with the updated array
    existingArray = updatedArray;

    return existingArray;
    // Log the updated array for verification
};

export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}

const CreateShipment = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isMandatory, setIsMandatory] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [AddBatchScreen, setAddBatchScreen] = useState(false);
    const [AddBatchScreenWithoutOrder, setAddBatchScreenWithoutOrder] =
        useState(false);

    const [selectedOrder, setSeletedOrder] = useState({});
    const [orderProducts, setOrderProducts] = useState([]);
    const [selectProduct, setSelectPRoduct] = useState({});

    const [isOrderSelected, setIsOrderSelected] = useState(false);

    const [isPopupVisible, setPopupVisible] = useState(false);
    const [isQrScanner, setIsQrScanner] = useState(false);
    const [isDeletePrdPopup, setIsDeletePrdPopup] = useState(false);
    const [deletePrdData, setIsDeletePrdData] = useState({});

    const [successMsg, setSuccessMsg] = useState('Shipment ID: ');
    const route = useRoute();

    const { params } = route;
    const { acceptOrderDetail = {}, campaignDetail = {} } = params;

    useEffect(() => {
        async function fetchData() {
            GetData();

            if (acceptOrderDetail.products) {
                setSeletedOrder(acceptOrderDetail);
                shipmentForm.handleChange({
                    target: { name: 'orderId', value: acceptOrderDetail.id },
                });
                setOrderProducts(acceptOrderDetail.products);
                shipmentForm.handleChange({
                    target: { name: 'products', value: acceptOrderDetail.products },
                });
                shipmentForm.handleChange({
                    target: { name: 'campaignId', value: acceptOrderDetail.campaignId },
                });

                // shipmentForm.handleChange({ target: { name: `from_org_loc`, value: value, } })
                // shipmentForm.handleChange({ target: { name: `to_org_loc`, value: value, } })
                // const { source = {}, destination = {}, } = selectedOrder

                setIsOrderSelected(true);
            } else if (campaignDetail.name) {
                setSeletedOrder({});
                const data = {
                    value: campaignDetail._id,
                    eachItem: campaignDetail,
                };
                changeAfterSelectCampaign(data);
                setIsOrderSelected(false);
            } else {
                setIsOrderSelected(false);
                setSeletedOrder({});
                shipmentForm.resetForm();
                setOrderProducts({});
            }
            if (locations.length) {
                changeAfterSelectFromOrgLevel({ value: locations[0]._id })
            }
            setIsMandatory(false);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        setLocalLoader(true);

        await props.getProdList();
        await props.getInboundAcceptedPartial();
        await props.getUserLocationList();
        await props.getUserLocation();
        await props.getInStockList();
        await props.getCampaignList();
        await props.getActiveCampaignList();
        setLocalLoader(false);
    };
    const reFreshList = async () => {
        // await props.getShipmentInboudSummary();
        // await props.getInboudShipmentAnalytcs();
        // await props.getShipmentInboundDamaged();
        // await props.getShipmentInboundDelivery();
        // await props.getShipmentInboundShipped();
        // await props.getShipmentOutboundSummary();
        // await props.getOutboudShipmentAnalytcs();
        // await props.getShipmentOutboudDamaged();
        // await props.getShipmentOutboundDelivered();
        // await props.getShipmentOutboundShiped();
    };

    var validationSchema = Yup.object().shape(
        {
            shippingDate: Yup.string().required(t('Required')),
            products: Yup.array()
                .of(
                    Yup.object().shape({
                        productId: Yup.string().required(t('Required')),
                        batches: Yup.array()
                            .of(
                                Yup.object().shape({
                                    batchNo: Yup.string().required(t('Required')),
                                    quantity: Yup.number()
                                        .min(1, t('Invalid Qauntity'))
                                        .required(t('Required')),
                                }),
                            )
                            .required(t('Batches Required')),
                    }),
                )
                .required(t('Required')),

            // from_org_loc: Yup.string().required(t('Required')),
            // to_org_loc: Yup.string().required(t('Required')),
        },
        ['shippingDate'],
    ); // <-- HERE!!!!!!!!
    const shipmentForm = useFormik({
        initialValues: {
            orderId: '',

            transitNo: 'TR0' + new Date().getTime(),
            shippingDate: '',
            est_date: '',
            products: [],
            // from_org_level: '',
            from_org_loc: '',
            // to_org_level: '',
            // to_org_name: '',
            to_org_loc: '',
        },
        validationSchema,
        onSubmit: (values) => {
            handleSubmit({ ...values });
        },
    });
    const _addshipment = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        shipmentForm.handleSubmit();
    };

    const handleSubmit = async values => {
        // console.log('handleSubmit values' + JSON.stringify(values))
        if (values.products.length) {
            setLocalLoader(true);

            values.source = destination._id || values.from_org_loc;
            values.destination = source._id || values.to_org_loc;
            values.orderId = selectedOrder._id;
            // values.products = newArrayWithFilteredDetails
            const newData = values;
            const values1 = removeEmptyValues(newData);
            console.log('create_shipment  values1' + JSON.stringify(values1));

            const result = await props.create_shipment(values1);
            setLocalLoader(false);

            // console.log('create_shipment  result' + JSON.stringify(result));
            if (result?.status === 200) {
                const ID =
                    (result.data && result.data.data && result.data.data.id) || '';
                const msg = t('Shipment ID: ') + ID;
                setSuccessMsg(msg);

                setPopupVisible(true);
                await reFreshList();
                shipmentForm.resetForm();
                setOrderProducts({});
            } else if (result.status === 500) {
                const err = result?.data?.message;
                setErrorMessage(err);
                ToastShow(t(err), 'error', 'long', 'top');
            } else if (result?.status === 401) {
                const err = result?.data?.message;
                setErrorMessage(err);
                ToastShow(t(err), 'error', 'long', 'top');
            } else {
                const err = result.data;
                setErrorMessage(err?.message);
                ToastShow(t(err?.message), 'error', 'long', 'top');
            }
        } else {
            ToastShow(t('Please Add Batch'), 'error', 'long', 'top');
        }
    };

    const closPopUp = () => {
        setPopupVisible(false);
        // props.navigation.navigate('outboundshipment')
        props.navigation.navigate('shipment');
    };

    const onChangeShipmentDate = val => {
        shipmentForm.handleChange({ target: { name: 'shippingDate', value: val } });
        shipmentForm.handleChange({ target: { name: 'est_date', value: null } });
    };
    const onChangeEstShipmentDate = val => {
        shipmentForm.handleChange({ target: { name: 'est_date', value: val } });
    };
    const selectProductList = (batch, product) => {
        let existingArray = orderProducts;

        const newObj = product;
        // console.log('existingArray ' + JSON.stringify(existingArray))
        // console.log('newObj ' + JSON.stringify(newObj))

        const index = existingArray.findIndex(
            each1 => each1.productId === newObj.productId,
        );

        if (index !== -1) {
            existingArray[index].batches = batch.batches;
        }

        setOrderProducts(existingArray);
        shipmentForm.handleChange({
            target: { name: 'products', value: existingArray },
        });
        setAddBatchScreen(false);
    };
    const selectProductListWithoutOrder = val => {
        shipmentForm.handleChange({ target: { name: 'products', value: val } });
        setAddBatchScreenWithoutOrder(false);
    };
    const _cancel_shipment = () => {
        shipmentForm.resetForm();
        setSeletedOrder({});
        setOrderProducts([]);
        props.navigation.navigate('shipment');
    };
    const fetchBaches = item => {
        setSelectPRoduct(item);
        setAddBatchScreen(true);
    };
    const fetchBachesWithoutOrder = () => {
        if (
            shipmentForm?.values?.from_org_loc &&
            shipmentForm?.values?.to_org_loc
        ) {
            setAddBatchScreenWithoutOrder(true);
        } else {
            ToastShow(
                t('Please select From and To location'),
                'error',
                'long',
                'top',
            );
        }
    };
    const changeAfterSelectOrderID = item => {
        const { value, eachItem } = item;
        shipmentForm.handleChange({ target: { name: 'orderId', value: value } });
        setSeletedOrder(eachItem);
        setOrderProducts(eachItem.products);
        shipmentForm.handleChange({
            target: { name: 'products', value: eachItem.products },
        });
        shipmentForm.handleChange({
            target: { name: 'campaignId', value: eachItem.campaignId },
        });
    };
    const changeAfterSelectFromOrgLevel = item => {
        const { value } = item;
        shipmentForm.handleChange({ target: { name: 'from_org_loc', value: value } });
    };
    const changeAfterSelectToOrgLevel = item => {
        const { value } = item;
        shipmentForm.handleChange({ target: { name: 'to_org_loc', value: value } });
    };
    const deleteProduct = prd => {
        setIsDeletePrdData(prd);
        setIsDeletePrdPopup(true);
    };
    const deleteProductFromList = () => {
        let productIdToDelete = deletePrdData.productId;
        let productlist = shipmentForm?.values?.products;
        productlist = productlist.filter(
            product => product.productId !== productIdToDelete,
        );
        if (shipmentForm?.values?.orderId || shipmentForm?.values?.campaignId) {
            setOrderProducts(productlist);
        }
        shipmentForm.handleChange({ target: { name: 'products', value: productlist } });
        setIsDeletePrdPopup(false);
    };
    const changeAfterSelectCampaign = item => {
        const { value, eachItem } = item;

        shipmentForm.handleChange({ target: { name: 'campaignId', value: value } });
        const product = {
            productId: eachItem?.productDetails?._id,
            product: eachItem?.productDetails,
        };
        setOrderProducts([product]);
        shipmentForm.handleChange({ target: { name: 'products', value: [product] } });
        shipmentForm.handleChange({ target: { name: 'orderId', value: null } });
    };
    const {
        userLocationList = {},
        inbound_accepted_partial_list = [],
        campaign_active_list = [],
    } = props;

    const { source = {}, destination = {} } = selectedOrder;

    const { locations = [], childLocations = [] } = userLocationList;
    // console.log('shipmentForm?.values' + JSON.stringify(shipmentForm?.values))
    const parentLocations = locations.map(location => location.parentLocation);
    // console.log('campaignDetail' + JSON.stringify(campaignDetail))

    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Create Shipment')}
                back={() => props.navigation.navigate('shipment')}
            />

            <PopupMessage
                message={successMsg}
                title={t('Shipment has been created Successfully')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Okay')}
                image={localImage.congrats}
            />
            <ConfirmationPopUp
                title={t('Delete Product')}
                message={
                    deletePrdData.productName
                        ? t('Are you sure delete the ') + deletePrdData.productName
                        : t('Are you sure delete the ') +
                        (deletePrdData &&
                            deletePrdData.product &&
                            deletePrdData.product.name)
                }
                isVisible={isDeletePrdPopup}
                onClose={() => setIsDeletePrdPopup(false)}
                confirm={() => deleteProductFromList()}
            />
            {AddBatchScreen ? (
                <SelectBatchComponent
                    navigation={props.navigation}
                    existingProd={shipmentForm?.values?.products}
                    addBatchInList={selectProductList}
                    selectedProduct={selectProduct}
                    cancelBatch={() => setAddBatchScreen(false)}
                />
            ) : AddBatchScreenWithoutOrder ? (
                <SelectBatchWithoutOrderComponent
                    existingProd={shipmentForm?.values?.products}
                    navigation={props.navigation}
                    addBatchInList={selectProductListWithoutOrder}
                    cancelBatch={() => setAddBatchScreenWithoutOrder(false)}
                />
            ) : (
                <VirtualizedList>
                    <View
                        style={{
                            flex: 1,
                            backgroundColor: '#fff',
                            borderRadius: 20,
                            padding: 10,
                        }}>
                        <View
                            style={{
                                backgroundColor: Colors.whiteFF,
                                borderTopLeftRadius: 10,
                                borderTopEndRadius: 10,
                                paddingBottom: 5,
                            }}>
                            <View
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    marginTop: 12,
                                }}>
                                <View
                                    style={{
                                        borderRadius: 2,
                                        width: scale(72),
                                        height: scale(3),
                                        backgroundColor: '#9B9B9B',
                                    }}
                                />
                            </View>
                        </View>
                        <View>
                            <View style={{ marginTop: 10 }} />
                            {props.loder || localLoader ? (
                                <View
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flex: 1,
                                    }}>
                                    <ActivityIndicator size="large" color="#0000ff" />
                                </View>
                            ) : null}
                            {/* {!isOrderSelected ? (
                                    <View>

                                        <DropDown
                                            dropdownData={inbound_accepted_partial_list}
                                            onChangeValue={changeAfterSelectOrderID}
                                            label='id'
                                            mapKey='id'
                                            val={shipmentForm?.values?.orderId}
                                            placeholderText="Select Order"
                                            labelText='Order ID'
                                            errorMsg={shipmentForm?.errors?.orderId}
                                            isMandatoryField={false}

                                        // mandatory={isMandatory}
                                        />

                                    </View>
                                ) : (<InputField
                                    placeholder="Order ID"
                                    label="Order ID"
                                    disable={true}
                                    inputValue={shipmentForm?.values?.orderId}
                                    setInputValue={shipmentForm.handleChange('orderId')}
                                    inputStyle={{ marginBottom: 10 }}
                                    labelStyle={{ marginBottom: 5 }}
                                    errorMsg={shipmentForm?.errors?.orderId}
                                    mandatory={isMandatory}
                                    isMandatoryField={false}

                                />
                                )} */}
                            <DropDown
                                dropdownData={inbound_accepted_partial_list}
                                onChangeValue={changeAfterSelectOrderID}
                                label="id"
                                mapKey="id"
                                val={shipmentForm?.values?.orderId}
                                placeholderText={t('Select Order')}
                                labelText={t('Order ID')}
                                errorMsg={shipmentForm?.errors?.orderId}
                                isMandatoryField={false}

                            // mandatory={isMandatory}
                            />
                            <CampaignDropdown
                                dropdownData={campaign_active_list}
                                onChangeValue={changeAfterSelectCampaign}
                                label="name"
                                mapKey="_id"
                                val={shipmentForm?.values?.campaignId}
                                placeholderText={t('Campaign')}
                                labelText={t('Campaign')}
                                errorMsg={shipmentForm?.errors?.campaignId}
                                mandatory={isMandatory}
                                isMandatoryField={false}
                            />
                            <View style={{ marginTop: 10 }} />
                            <SubHeaderComponent name={t('From')} textColor={'#000'} />
                            {shipmentForm?.values?.orderId ? (
                                <LocationComponent userLocation={destination} />
                            ) : (
                                <View>
                                    <DropDown
                                        dropdownData={locations}
                                        onChangeValue={changeAfterSelectFromOrgLevel}
                                        label="name"
                                        mapKey="_id"
                                        val={shipmentForm?.values?.from_org_loc}
                                        placeholderText={t('Select location')}
                                        labelText={t('Organization location')}
                                        errorMsg={shipmentForm?.errors?.from_org_loc}
                                        mandatory={isMandatory}
                                        search={false}
                                    // disabled={true}

                                    />
                                </View>
                            )}

                            <SubHeaderComponent name={t('To')} textColor={'#000'} />

                            {shipmentForm?.values?.orderId ? (
                                <LocationComponent userLocation={source} />
                            ) : (
                                <View>
                                    <DropDown
                                        dropdownData={
                                            childLocations.length ? childLocations : parentLocations
                                        }
                                        onChangeValue={changeAfterSelectToOrgLevel}
                                        label="name"
                                        mapKey="_id"
                                        val={shipmentForm?.values?.to_org_loc}
                                        placeholderText={t('Select location')}
                                        labelText={t('Organization location')}
                                        errorMsg={shipmentForm?.errors?.to_org_loc}
                                        mandatory={isMandatory}
                                    // search={false}
                                    />
                                </View>
                            )}

                            <View style={{ marginTop: 10 }} />

                            <SubHeaderComponent
                                name={t('Delivery Details')}
                                textColor={'#000'}
                            />
                            <View style={{ marginTop: 10 }} />

                            <InputField
                                placeholder={t('Transit Number')}
                                label={t('Transit Number')}
                                inputValue={shipmentForm?.values?.transitNo}
                                setInputValue={shipmentForm.handleChange('transitNo')}
                                inputStyle={{ marginBottom: 10 }}
                                labelStyle={{ marginBottom: 5 }}
                                errorMsg={shipmentForm?.errors?.transitNo}
                                mandatory={isMandatory}
                            />

                            {/* {isQrScanner && <QrcodeScanner getQrcode={getQrcode} closeQr={() => setIsQrScanner(false)} />}
                                <InputField
                                    placeholder="Label Code"
                                    label="Label Code"
                                    inputValue={shipmentForm?.values?.label}
                                    setInputValue={shipmentForm.handleChange('label')}
                                    inputStyle={{ marginBottom: 10 }}
                                    labelStyle={{ marginBottom: 5 }}
                                    errorMsg={shipmentForm?.errors?.label}
                                    mandatory={isMandatory}
                                    RightIcon={
                                        () =>
                                            <TouchableOpacity
                                                onPress={() => setIsQrScanner(true)}>
                                                <MaterialIcons name='qr-code-scanner' color='#000' size={25} />
                                            </TouchableOpacity>
                                    }
                                /> */}
                            <DatePickerComponent
                                label={t('Shipment Date')}
                                placeholder="DD/MM/YYYY"
                                onChange={val => onChangeShipmentDate(val)}
                                val={shipmentForm?.values?.shippingDate}
                                errorMsg={shipmentForm?.errors?.shippingDate}
                                mandatory={isMandatory}
                            // minDate={new Date()}
                            />
                            {shipmentForm?.values?.shippingDate ? (
                                <DatePickerComponent
                                    label={t('Estimated Delivery Date')}
                                    placeholder="DD/MM/YYYY"
                                    onChange={val => onChangeEstShipmentDate(val)}
                                    val={shipmentForm?.values?.est_date}
                                    errorMsg={shipmentForm?.errors?.est_date}
                                    mandatory={isMandatory}
                                    minDate={
                                        shipmentForm?.values?.shippingDate
                                            ? new Date(shipmentForm?.values?.shippingDate)
                                            : new Date()
                                    }
                                    isMandatoryField={false}
                                />
                            ) : null}

                            {shipmentForm?.values?.orderId ||
                                shipmentForm?.values?.campaignId ? (
                                shipmentForm?.values?.products.length ? (
                                    <ShipmentProductListComponent
                                        products={orderProducts}
                                        error={shipmentForm?.errors?.products}
                                        delete={item => deleteProduct(item)}
                                        isMandatory={isMandatory}
                                        fetchBaches={item => fetchBaches(item)}
                                    />
                                ) : null
                            ) : (
                                <ShipmentProductListWithoutOrderComponent
                                    products={shipmentForm?.values?.products}
                                    error={shipmentForm?.errors?.products}
                                    delete={item => deleteProduct(item)}
                                    isMandatory={isMandatory}
                                    fetchBaches={() => fetchBachesWithoutOrder()}
                                />
                            )}

                            {props.loder || localLoader ? (
                                <View
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        flex: 1,
                                    }}>
                                    <ActivityIndicator size="large" color="#0000ff" />
                                </View>
                            ) : (
                                <View
                                    style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                                    <CustomButtonWithBorder
                                        buttonName={t('Back')}
                                        onPressButton={() => _cancel_shipment()}
                                        bdColor={'#BFBFBF'}
                                        textColor={'#20232B'}
                                    />
                                    <CustomButton
                                        buttonName={t('Confirm')}
                                        onPressButton={_addshipment}
                                        bgColor={'#208196'}
                                        textColor={'#fff'}
                                    />
                                </View>
                            )}
                        </View>
                    </View>
                </VirtualizedList>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        inv_product_list: state.inventory.inv_product_list,
        userLocation: state.auth.userLocation,
        userLocationList: state.auth.userLocationList,
        inbound_accepted_partial_list: state.order.inbound_accepted_partial_list,
        outbound_accepted_list: state.order.outbound_accepted_list,
        inv_stock_list: state.inventory.inv_stock_list,
        campaign_list: state.campaign.campaign_list,
        campaign_active_list: state.campaign.campaign_active_list,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    create_shipment,
    getInboundAcceptedPartial,
    getShipmentInboudSummary,
    getInboudShipmentAnalytcs,
    getShipmentInboundDamaged,
    getShipmentInboundDelivery,
    getShipmentInboundShipped,
    getShipmentOutboundSummary,
    getOutboudShipmentAnalytcs,
    getShipmentOutboudDamaged,
    getShipmentOutboundDelivered,
    getShipmentOutboundShiped,
    getUserLocationList,
    getUserLocation,
    getInStockList,
    getCampaignList,
    getActiveCampaignList,
})(CreateShipment);
