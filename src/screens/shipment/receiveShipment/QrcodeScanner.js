/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useCallback, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
    Dimensions,
    Platform,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { useIsFocused } from '@react-navigation/native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import {
    useCameraPermission,
    useCameraDevice,
    Camera,
    useCodeScanner,
} from 'react-native-vision-camera';
import fonts from '../../../../FontFamily';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import AntDesign from 'react-native-vector-icons/AntDesign';

const ButtonOption = ({ onPress, image, title }) => {
    return (
        <View>
            <TouchableOpacity
                onPress={onPress}
                style={{
                    height: 50,
                    width: 50,
                    borderRadius: 50,
                    borderWidth: 1,
                    alignSelf: 'center',
                    borderColor: '#eff1f4',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#fff',
                }}>
                <Image source={image} style={{ height: 25, width: 25 }} />
            </TouchableOpacity>
            <Text
                style={{
                    color: '#fff',
                    fontFamily: fonts.REGULAR,
                    fontSize: 14,
                    paddingTop: 5,
                    alignSelf: 'center',
                }}>
                {title}
            </Text>
        </View>
    );
};

const ScanQR = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const insets = useSafeAreaInsets();

    const { height, width } = Dimensions.get('window');
    const { hasPermission, requestPermission } = useCameraPermission();
    const device = useCameraDevice('back');

    const [scanValue, setScanvalue] = useState('');

    const codeScanner = useCodeScanner({
        codeTypes: ['qr', 'ean-13'],
        onCodeScanned: codes => {
            setScanvalue(codes[0].value);
            if (getQrcode) {
                getQrcode(codes[0].value);
            }
            console.log(`Scanned ${codes.length} codes!`);
            console.log(`Scanned value ${codes[0].value} codes!`);
        },
    });
    const cancelQR = () => {
        if (getQrcode) {
            props.closeQr();
        } else {
            props.navigation.goBack();
        }
    };
    const onError = useCallback(error => {
        console.error(error);
    }, []);

    useEffect(() => {
        requestPermission();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const { getQrcode } = props;
    if (device == null) {
        return (
            <View
                style={{
                    paddingTop: Platform.OS === 'ios' ? insets.top : insets.top + 30,
                    height: 250,
                    width: 250,
                    alignSelf: 'center',
                }}>
                <Text
                    style={{
                        color: '#000',
                        fontFamily: fonts.MEDIUM,
                        fontSize: scale(15),
                    }}>
                    Device not found.
                </Text>
            </View>
        );
    }
    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <View
                style={{
                    marginTop: Platform.OS === 'ios' ? insets.top : insets.top + 30,
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        marginBottom: 20,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: scale(16),
                            color: '#000',
                            marginRight: 20,
                        }}>
                        {'Scan Qr'}
                    </Text>
                    <TouchableOpacity onPress={() => cancelQR()}>
                        <AntDesign name="close" color={'#000'} size={25} />
                    </TouchableOpacity>
                </View>
                <View
                    style={{
                        height: 250,
                        width: 250,
                        alignSelf: 'center',
                        marginBottom: 20,
                    }}>
                    <Camera
                        style={StyleSheet.absoluteFill}
                        device={device}
                        onError={onError}
                        isActive={isFocused}
                        codeScanner={codeScanner}
                    />
                </View>
            </View>
            {scanValue && (
                <Text style={{ color: '#000', textAlign: 'center' }}>{scanValue}</Text>
            )}
        </View>
    );
};
export default ScanQR;
