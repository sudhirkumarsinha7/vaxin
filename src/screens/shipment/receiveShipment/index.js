/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    ScrollView,
    Text,
    StyleSheet,
    ActivityIndicator,
    TouchableOpacity,
    Keyboard,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useRoute } from '@react-navigation/native';

import { useIsFocused } from '@react-navigation/native';
import {
    getShipmentDetails,
    receiveShipment,
} from '../../../redux/action/shipment';
import { scale } from '../../../components/Scale';
import { Colors } from '../../../components/Common/Style';
import fonts from '../../../../FontFamily';
import { formatDateDDMMYYYY } from '../../../Util/utils';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import { ShipmentStatus } from '../helpler';
import {
    InputField,
    CustomButtonWithBorder,
    CustomButton,
} from '../../../components/Common/helper';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import DropDown from '../../../components/Dropdown';
import {
    getShipmentInboudSummary,
    getInboudShipmentAnalytcs,
    getShipmentInboundDamaged,
    getShipmentInboundDelivery,
    getShipmentInboundShipped,
    getShipmentOutboundSummary,
    getOutboudShipmentAnalytcs,
    getShipmentOutboudDamaged,
    getShipmentOutboundDelivered,
    getShipmentOutboundShiped,
} from '../../../redux/action/shipment';

import {
    getInventoryLocationList,
} from '../../../redux/action/inventory';
export const RejectionListData = [
    { name: 'Transit Damage', key: 'DAMAGED' },
    { name: 'Miscount', key: 'LOST' },
    { name: 'Cold Chain Damage', key: 'COLD_CHAIN_FAILURE' },
    { name: 'Other', key: 'OTHER' },
];
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { replaceObject } from '../AddShipment';
import { ToastShow } from '../../../components/Toast';
import { PopupMessage } from '../../../components/popUp';
import { localImage } from '../../../config/global';
import { UserUpdatedComponent } from '../viewShipment';
export const getRejectionLabel = (options1 = [], colentye) => {
    const option = options1.find(opt => opt.key === colentye);
    return option ? option.name : colentye;
};

function addObjectAndUpdateQuantity(array, newObj) {
    // Check if the new object meets the conditions
    const existingIndex = array.findIndex(
        item =>

            item.type === newObj.type
            &&
            (item.type === 'OTHER' || item.comment === newObj.comment)
    );
    if (existingIndex !== -1) {
        // If the object already exists, update the quantity
        array[existingIndex].quantity = Number(array[existingIndex].quantity) + Number(newObj.quantity);
    } else {
        // If the object does not exist, add it to the array
        array.push(newObj);
    }

    return array;
}
export const ProductHeader = props => {
    const { t, i18n } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.35, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Name')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Id')}
                </Text>
            </View>
            <View style={{ flex: 0.35, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{'Category'}</Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Manufacturer')}
                </Text>
            </View>
            <View
                style={{
                    flex: 0.3,
                    justifyContent: 'center',
                    padding: 2,
                    alignItems: 'center',
                }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Quantity')}</Text>
            </View>
        </View>
    );
};
export const ProductList = props => {
    const { t, i18n } = useTranslation();

    const { item = {} } = props;
    const { product = {}, batches = [] } = item;
    const { manufacturer = {}, units = '' } = product;
    return (
        <View
            style={{
                borderBottomWidth: 0.5,
                borderColor: '#7F7F7F',
                paddingHorizontal: 10,
                paddingVertical: 5,
            }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                <View style={{ flex: 0.35, justifyContent: 'center', padding: 2 }}>
                    <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                        {product.name}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 13,
                            color: '#7F7F7F',
                            marginTop: 5,
                        }}>
                        {product.id}
                    </Text>
                </View>
                <View style={{ flex: 0.35, justifyContent: 'center', padding: 2 }}>
                    <Text
                        style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: '#16B0D2' }}>
                        {product.type}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 13,
                            color: '#7F7F7F',
                            marginTop: 5,
                        }}>
                        {manufacturer.name}
                    </Text>
                </View>
                <View
                    style={{
                        flex: 0.3,
                        justifyContent: 'center',
                        padding: 2,
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 13,
                            color: '#7F7F7F',
                            marginTop: 5,
                        }}>
                        {item.quantity + ' ' + units}
                    </Text>
                </View>
            </View>

            {/* <FlatList
contentContainerStyle={{ paddingBottom: 100 }}
            data={batches}
            keyExtractor={(item, index) => index.toString()}

            renderItem={({ item, index }) =>
                <BatchList item={item} units={units} />
            }
        /> */}

            {batches.map((rejection, index) => (
                <View key={index}>
                    <BatchList item={rejection} units={units} />
                </View>
            ))}
        </View>
    );
};
export const BatchList = props => {
    const { t, i18n } = useTranslation();

    const [isExtended, setExtended] = useState(false);
    const [isMandatory, setIsMandatory] = useState(true);
    const [perUnit, setPerUnit] = useState(1);
    const [rejections, setRejections] = useState([]);

    useEffect(() => {
        const actQty = item.quantity - rejectQunatity;
        async function fetchData() {
            shipmentForm.handleChange({
                target: { name: 'accept_qty', value: actQty + '' },
            });
            setPerUnit(quantityPerUnit);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const setAcceptQty = text => {
        const value = Number(text);

        if (value <= item.quantity) {
            shipmentForm.handleChange({ target: { name: 'accept_qty', value: value } });
        } else {
            // Clear the input if it becomes empty
            shipmentForm.handleChange({ target: { name: 'accept_qty', value: '' } });
        }
    };

    var validationSchema = Yup.object().shape(
        {
            accept_qty: Yup.number()
                .integer(t('Quantity must be an integer'))
                .test(
                    'is-multiple-of-' + perUnit,
                    t('Quantity must be a multiple of ') + perUnit,
                    value => value % perUnit === 0,
                )
                .required(t('Required')),
            rejections: Yup.array()
                .of(
                    Yup.object().shape({
                        type: Yup.string().required(t('Required')),
                        quantity: Yup.number()
                            .positive(t('Quantity must be greater than 0'))
                            .integer(t('Quantity must be an integer'))
                            .test(
                                'is-multiple-of-' + perUnit,
                                t('Quantity must be a multiple of ') + perUnit,
                                value => value % perUnit === 0,
                            )
                            .required(t('Required')),
                    }),
                )

        },
        ['accept_qty'],
    ); // <-- HERE!!!!!!!!
    const shipmentForm = useFormik({
        initialValues: {
            accept_qty: '',
            pending_qty: '',
            rejections: [

            ],
        },
        validationSchema,
        onSubmit: (values, actions) => {
            values.rejections = rejections;
            handleSubmit({ ...values });
        },
    });
    const _confirmAcceptQty = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        shipmentForm.handleSubmit();
    };

    const handleSubmit = async values => {
        // console.log('handleSubmit values ', values);
        let rejections = values.rejections;
        let pending_qty = item.quantity - values.accept_qty;
        const rejectQunatity = rejections.reduce(
            (total, rejection) => total + Number(rejection.quantity),
            0,
        );
        item.accept_qty = values.accept_qty;
        // console.log('rejectQunatity ', rejectQunatity);
        // console.log('pending_qty ', pending_qty);

        if (rejectQunatity <= pending_qty) {
            setExtended(false);

            item.rejections = values.rejections;
        } else {
            ToastShow(
                t('Total rejction quantity cannot exceed ') + pending_qty,
                'error',
                'long',
                'top',
            );
        }

        // console.log('confirmQty item  ' + JSON.stringify(item))
    };



    const addRejection = (newRejection) => {
        const currentRejectQuantity = rejections.reduce(
            (total, rejection) => total + Number(rejection.quantity),
            0,
        );
        const pending_qty = item.quantity - Number(shipmentForm?.values?.accept_qty);
        const newTotalRejectionQty = currentRejectQuantity + Number(newRejection.quantity);

        if (newTotalRejectionQty <= pending_qty) {
            setRejections(prevRejections => {
                if (prevRejections.length) {
                    const newData = addObjectAndUpdateQuantity(prevRejections, newRejection);
                    return newData;
                } else {
                    return [newRejection];
                }
            });
        } else {
            ToastShow(
                t('Total rejction quantity cannot exceed ') + pending_qty + '  ' + item.quantity,
                'error',
                'long',
                'top',
            );
        }
    };
    const handleDeleteRejection = (index) => {
        const updatedRejections = rejections.filter((_, i) => i !== index);
        setRejections(updatedRejections);
    };
    const { item = {}, units = '' } = props;
    const { atom = {} } = item;
    const { quantityPerUnit = 1 } = atom;
    const rejectQunatity = rejections.reduce(
        (total, rejection) => total + Number(rejection?.quantity),
        0,
    );


    return (
        <View>
            <View
                style={{
                    flexDirection: 'row',
                    backgroundColor: '#F0F2F7',
                    borderTopWidth: 0.5,
                    borderColor: '#7F7F7F',
                }}>
                <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                    <Text style={{ fontFamily: fonts.REGULAR, fontSize: 13 }}>
                        {item.batchNo}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 13,
                            color: '#155897',
                            marginTop: 5,
                        }}>
                        {item.quantity + ' ' + units}
                    </Text>
                </View>
                <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 12,
                            color: '#155897',
                            marginTop: 5,
                        }}>
                        {t('Accept Qty')}
                    </Text>

                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 13,
                            color: '#155897',
                            marginTop: 5,
                        }}>
                        {item.accept_qty || item.accept_qty === 0
                            ? item.accept_qty + ' ' + units
                            : item.quantity + ' ' + units}
                    </Text>
                </View>
                {(item.accept_qty || item.accept_qty === 0) && (
                    <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 12,
                                color: '#155897',
                                marginTop: 5,
                            }}>
                            {t('Pending Qty')}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                                color: '#155897',
                                marginTop: 5,
                            }}>
                            {item.accept_qty || item.accept_qty === 0
                                ? item.quantity - item.accept_qty - rejectQunatity + ' ' + units
                                : '0' + units}
                        </Text>
                    </View>
                )}
                {(item.accept_qty || item.accept_qty === 0) && (
                    <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 12,
                                color: '#155897',
                                marginTop: 5,
                            }}>
                            {t('Reject Qty')}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                                color: '#155897',
                                marginTop: 5,
                            }}>
                            {rejectQunatity + ' ' + units}
                        </Text>
                    </View>
                )}
            </View>
            <TouchableOpacity
                style={{ alignSelf: 'center' }}
                onPress={() => setExtended(!isExtended)}>
                <AntDesign
                    name={isExtended ? 'caretup' : 'caretdown'}
                    size={scale(12)}
                    color={Colors.blueChill}
                />
            </TouchableOpacity>

            {isExtended && (
                <View style={{ margin: 5 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 12,
                            color: Colors.gray9,
                            marginBottom: 5,
                        }}>
                        {t('Enter Qty multiple of ') + quantityPerUnit}
                    </Text>

                    <InputField
                        placeholder={t('Quantity Accepting')}
                        label={t('Quantity Accepting')}
                        KeyboardType={'numeric'}
                        inputValue={shipmentForm?.values?.accept_qty + ''}
                        setInputValue={setAcceptQty}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={shipmentForm?.errors?.accept_qty}
                        mandatory={isMandatory}
                    />

                    {shipmentForm?.values?.accept_qty < item.quantity ?
                        <RejectionComponent
                            rejectionList={rejections}
                            perUnit={perUnit}
                            addRejection={addRejection}
                            totatQty={item.quantity}
                            rejectQunatity={rejectQunatity}
                            Accepting_qty={shipmentForm?.values?.accept_qty}
                            handleDeleteRejection={handleDeleteRejection}
                        />
                        : null}


                    {shipmentForm?.values?.accept_qty < item.quantity && (
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}>
                            {/* <CustomButtonWithBorder
                                bdColor={'#C5C5C5'}
                                textColor={'#20232B'}
                                buttonName={t('Cancel')}
                                navigation={props.navigation}
                                onPressButton={() => setExtended(!isExtended)}
                            /> */}
                            <View />
                            <CustomButton
                                bgColor={'#208196'}
                                buttonName={t('Confirm')}
                                navigation={props.navigation}
                                onPressButton={_confirmAcceptQty}
                            />
                        </View>
                    )}
                </View>
            )}
        </View>
    );
};
export const RejectionComponent = props => {
    const { t, i18n } = useTranslation();
    const [ischanged, setIschanged] = useState(false);

    useEffect(() => {
        async function fetchData() {

        }
        fetchData();
    }, []);
    const { perUnit = 1, rejectionList = [], totatQty = 0, Accepting_qty = 0, addRejection, handleDeleteRejection } = props



    var validationSchema = Yup.object().shape(
        {
            type: Yup.string().required(t('Required')),
            quantity: Yup.number()
                .positive(t('Quantity must be greater than 0'))
                .integer(t('Quantity must be an integer'))
                .test(
                    'is-multiple-of-' + perUnit,
                    t('Quantity must be a multiple of ') + perUnit,
                    value => value % perUnit === 0,
                )
                .required(t('Required')),
        },
        [],
    ); // <-- HERE!!!!!!!!
    const rejectionForm = useFormik({
        initialValues: {
            quantity: '',
            type: '',
        },
        validationSchema,
        onSubmit: (values, actions) => {
            handleSubmit({ ...values });
        },
    });
    const _confirm = () => {
        Keyboard.dismiss();
        rejectionForm.handleSubmit();
    };

    const handleSubmit = async values => {
        setIschanged(true)
        await props.addRejection(values)
        setIschanged(false)

    };



    const changeAfterSelectRejectType = (item) => {
        rejectionForm.handleChange({
            target: { name: `type`, value: item?.value },
        });
    };
    const currentRejectQuantity = rejectionList.reduce(
        (total, rejection) => total + Number(rejection.quantity),
        0,
    );
    const pendingQty = (totatQty - currentRejectQuantity - Accepting_qty)
    return (

        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#fff',
                borderTopWidth: 0.5,
                borderColor: '#7F7F7F',
            }}>
            <View style={{ flex: 0.5, }}>


                <DropDown
                    dropdownData={RejectionListData}
                    onChangeValue={val =>
                        changeAfterSelectRejectType(val)
                    }
                    label="name"
                    mapKey="key"
                    val={rejectionForm?.values?.type}
                    placeholder={t('Select')}
                    errorMsg={t('Required')}
                    labelText={t('Reject Type')}
                    search={false}
                    mandatory={true}
                />
                <InputField
                    placeholder={t('Quantity')}
                    label={t('Quantity')}
                    KeyboardType={'numeric'}
                    inputValue={rejectionForm?.values?.quantity + ''}
                    setInputValue={rejectionForm.handleChange('quantity')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={rejectionForm?.errors?.quantity}
                    mandatory={true}
                />
                {rejectionForm?.values?.type === 'OTHER' && (
                    <InputField
                        placeholder={t('Enter here')}
                        label={t('Comment')}

                        setInputValue={rejectionForm.handleChange('comment')}

                        inputValue={
                            rejectionForm?.values?.comment
                        }
                        errorMsg={
                            rejectionForm.errors?.comment
                        }
                        mandatory={true}
                        inputStyle={{ marginBottom: 5 }}
                        labelStyle={{ marginBottom: 5 }}
                    />
                )}
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <CustomButton
                        bgColor={'#208196'}
                        buttonName={t('Add')}
                        navigation={props.navigation}
                        onPressButton={_confirm}
                    />
                    <View />
                </View>

            </View>
            <View style={{ flex: 0.5 }} />
            <View style={{ flex: 0.45, justifyContent: 'center' }}>
                {rejectionList.map((each, index) => (
                    <View style={{ flexDirection: 'row', }}>

                        <Text style={{ fontSize: 14, fontFamily: fonts.REGULAR, marginRight: 20 }}>
                            {t(getRejectionLabel(RejectionListData, each.type)) + ' :' + each.quantity}
                        </Text>
                        <TouchableOpacity

                            onPress={() => handleDeleteRejection(index)}
                        >
                            <MaterialIcons
                                name="delete"
                                size={24}
                                color={
                                    Colors.red
                                }
                            />
                        </TouchableOpacity>
                    </View>
                ))}
                <Text style={{ fontSize: 14, fontFamily: fonts.REGULAR, color: Colors.blue8E }}>
                    {t('Pending Quantity') + ' :' + pendingQty + ''}
                </Text>

            </View>
        </View>
    );
};





const ViewProducDetail = props => {
    const { products = [], selectProductList } = props;
    return (
        <View>
            <ProductHeader />

            {/* <FlatList
contentContainerStyle={{ paddingBottom: 100 }}
            data={products}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
                <ProductList item={item} selectProductList={selectProductList} />
            )}
        /> */}
            <ScrollView>
                {products.map((item, index) => (
                    <View key={index}>
                        <ProductList item={item} selectProductList={selectProductList} />
                    </View>
                ))}
            </ScrollView>
        </View>
    );
};
const RecieveShipment = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [isExtended, setExtended] = useState(false);
    const [shipmentDetails, setShipmentDetails] = useState({});
    const [shipmentProduct, setShipmentProduct] = useState([]);
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [isPopupVisible, setPopupVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('Shipment ID: ');
    const [selectInventory, setSelectInventory] = useState('');

    const route = useRoute();
    const { params } = route;
    const SelectedShipment = params.SelectedShipment || {};
    useEffect(() => {
        async function fetchData() {
            const response = await props.getShipmentDetails(SelectedShipment._id);
            const inv_response = await props.getInventoryLocationList();
            if (response && response.data && response.data.data) {
                const res = response.data.data;
                setShipmentDetails(res);
                setShipmentProduct(res.products);
            }
            if (inv_response && inv_response.data && inv_response.data.data) {
                const res1 = inv_response.data.data;

                if (res1.length) {
                    setSelectInventory(res1[0]._id);
                }
            }
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const selectProductList = val => {
        const condition = item => item.productId === val.productId;
        const updatedList = replaceObject(shipmentProduct, condition, val);
        setShipmentProduct(updatedList);
        // console.log('orderProducts  ' + JSON.stringify(updatedList))

        // setOrderProducts(updatedList)
    };
    // const _receive = async () => {
    //     // console.log('shipmentDetails  ' + JSON.stringify(shipmentDetails))

    //     let listofArary = divideArray(shipmentProduct)
    //     listofArary.shipmentId = shipmentDetails._id

    //     const result = await props.receiveShipment(listofArary);
    //     setLocalLoader(false)

    //     // console.log('receiveShipment res result' + JSON.stringify(result));
    //     if (result?.status === 200) {
    //         const orderID = result.data && result.data.data && result.data.data.id || ''
    //         const msg = "Shipment ID: " + orderID
    //         setSuccessMsg(msg)

    //         setPopupVisible(true)
    //         refreshData()

    //     } else if (result.status === 500) {
    //         const err = result?.data?.message;
    //         setErrorMessage(err);
    //         ToastShow(
    //             t(err),
    //             'error',
    //             'long',
    //             'top',
    //         )
    //     } else if (result?.status === 401) {
    //         const err = result?.data?.message;
    //         setErrorMessage(err);
    //         ToastShow(
    //             t(err),
    //             'error',
    //             'long',
    //             'top',
    //         )
    //     } else {
    //         const err = result.data;
    //         setErrorMessage(err?.message);
    //         ToastShow(
    //             t((err?.message)),
    //             'error',
    //             'long',
    //             'top',
    //         )
    //     }

    // }
    const _receive = async () => {
        setLocalLoader(true);

        const transformedData = shipmentProduct.map(item => ({
            productId: item.productId,
            batches:
                item.batches &&
                item.batches.map(batch => ({
                    atomId:
                        batch.atomId ||
                        (batch.atoms && batch.atoms[0] && batch.atoms[0]._id),
                    inventoryId: selectInventory,
                    quantity: Number(batch.accept_qty) || batch.quantity,
                    rejections: batch.rejections,
                })),
        }));
        const data = {
            shipmentId: shipmentDetails._id,
            products: transformedData,
        };

        // console.log('receiveShipment data' + JSON.stringify(data));
        const result = await props.receiveShipment(data);
        setLocalLoader(false);

        // console.log('receiveShipment res result' + JSON.stringify(result));
        if (result?.status === 200) {
            const orderID =
                (result.data && result.data.data && result.data.data.id) || '';
            const msg = t('Shipment ID: ') + orderID;
            setSuccessMsg(msg);

            await refreshData();
            setPopupVisible(true);
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };
    const refreshData = async () => {
        await props.getShipmentInboudSummary();
        await props.getInboudShipmentAnalytcs();
        await props.getShipmentInboundDamaged();
        await props.getShipmentInboundDelivery();
        await props.getShipmentInboundShipped();
        await props.getShipmentOutboundSummary();
        await props.getOutboudShipmentAnalytcs();
        await props.getShipmentOutboudDamaged();
        await props.getShipmentOutboundDelivered();
        await props.getShipmentOutboundShiped();
    };
    const closPopUp = () => {
        setPopupVisible(false);
        props.navigation.navigate('outboundshipment');
    };
    const changeAfterSelectInventory = item => {
        const { value, eachItem, label } = item;

        setSelectInventory(value);
    };
    const { shipment_details = {}, inv_list = [] } = props;
    const {
        updatedBy = [],
        products = [],
        source = {},
        destination = {},
        createdBy = {},
        campaign = {},
    } = shipmentDetails;
    const { user = {}, locationId = {} } = createdBy;

    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Accept Shipment')}
            />
            <PopupMessage
                message={successMsg}
                title={t('Shipment has been updated Successfully')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Okay')}
                image={localImage.congrats}
            />

            <VirtualizedList>
                <View style={{ backgroundColor: '#fff', borderRadius: 10 }}>


                    <View
                        style={{
                            borderRadius: 12,
                            borderWidth: 0.5,
                            marginTop: 12,
                            marginHorizontal: scale(15),
                            borderColor: Colors.grayC5,
                        }}>
                        <View
                            style={{
                                borderBottomWidth: 0.5,
                                padding: 7,
                                paddingHorizontal: 12,
                                flexDirection: 'row',
                                borderColor: Colors.grayC5,
                            }}>
                            <View style={{ flex: 0.7, flexDirection: 'row' }}>
                                <ShipmentStatus status={shipment_details.status} />
                            </View>
                            <View
                                style={{
                                    flex: 0.3,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-end',
                                    alignContent: 'center',
                                }}>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 12,
                                        color: Colors.grayTextColor,
                                    }}>
                                    {t('Date: ') + formatDateDDMMYYYY(shipment_details.createdAt)}
                                </Text>
                            </View>
                        </View>
                        <View style={{ padding: 7, paddingHorizontal: 12 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 0.7 }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.BOLD,
                                            fontSize: 16,
                                            color: Colors.blueD2,
                                            marginTop: 5,
                                        }}>
                                        {t('Shipment ID: ') + shipment_details.id}
                                    </Text>
                                    {campaign && campaign.name && (
                                        <Text
                                            style={{
                                                fontFamily: fonts.MEDIUM,
                                                fontSize: 14,
                                                color: Colors.blueD2,
                                                marginTop: 5,
                                            }}>
                                            {t('Campaign: ') + campaign.id + ' - ' + campaign.name}
                                        </Text>
                                    )}
                                    <Text
                                        style={{
                                            fontFamily: fonts.MEDIUM,
                                            fontSize: 14,
                                            color: Colors.grayTextColor,
                                            marginTop: 5,
                                        }}>
                                        {t('Created By: ') + user?.firstName + ' ' + user?.lastName}
                                    </Text>
                                </View>
                                <View style={{ flex: 0.3 }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.MEDIUM,
                                            fontSize: 14,
                                            color: Colors.grayTextColor,
                                            marginTop: 5,
                                        }}>
                                        {t('Transit No:  ') + shipment_details.transitNo}
                                    </Text>
                                </View>
                            </View>

                            {updatedBy.length ? <TouchableOpacity
                                style={{ alignSelf: 'center' }}
                                onPress={() => setExtended(!isExtended)}>
                                <AntDesign
                                    name={isExtended ? 'caretup' : 'caretdown'}
                                    size={scale(12)}
                                    color={Colors.blueChill}
                                />
                            </TouchableOpacity> : null}
                        </View>
                        {isExtended ? (
                            <View
                                style={{
                                    padding: 7,
                                    paddingHorizontal: 12,
                                    backgroundColor: Colors.lightGrayBlue,
                                    borderRadius: 12,
                                    borderColor: Colors.lightGrayBlue1,
                                    borderWidth: 0.5,
                                }}>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 14,
                                        color: Colors.blackTextColor,
                                        marginTop: 12,
                                    }}>
                                    {t('Last Updated by: ')}

                                </Text>
                                <FlatList
                                    contentContainerStyle={{ paddingBottom: 100 }}
                                    data={updatedBy}
                                    renderItem={({ item }) => <UserUpdatedComponent item={item} />}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        ) : null}
                    </View>
                    <View style={{ marginTop: 20 }} />
                    <View style={{ paddingHorizontal: 15 }}>
                        <DropDown
                            dropdownData={inv_list}
                            onChangeValue={changeAfterSelectInventory}
                            label="name"
                            mapKey="_id"
                            val={selectInventory}
                            placeholder={t('Select Location')}
                            errorMsg={''}
                            labelText={t('Inventory Location')}
                            search={false}
                        />
                    </View>

                    <ViewProducDetail
                        products={products}
                        selectProductList={selectProductList}
                    />
                    <View style={{ marginTop: 20 }} />
                    {props.loder || localLoader ? (
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    ) : (
                        <CustomButton
                            bgColor={'#208196'}
                            buttonName={t('Receive')}
                            navigation={props.navigation}
                            onPressButton={_receive}
                        />
                    )}

                    <View style={{ height: 100 }} />
                </View>
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
function mapStateToProps(state) {
    return {
        shipment_details: state.shipment.shipment_details,
        userLocationList: state.auth.userLocationList,
        inv_list: state.inventory.inv_list,
    };
}
export default connect(mapStateToProps, {
    getShipmentDetails,
    receiveShipment,
    getShipmentInboudSummary,
    getInboudShipmentAnalytcs,
    getShipmentInboundDamaged,
    getShipmentInboundDelivery,
    getShipmentInboundShipped,
    getShipmentOutboundSummary,
    getOutboudShipmentAnalytcs,
    getShipmentOutboudDamaged,
    getShipmentOutboundDelivered,
    getShipmentOutboundShiped,
    getInventoryLocationList,
})(RecieveShipment);
