/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react';
import {
    View,
} from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useIsFocused } from '@react-navigation/native';

import { scale } from '../../components/Scale';
import { fonts, Colors } from '../../components/Common/Style';
import Header from '../../components/Header';
import { useTranslation } from 'react-i18next';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AddShipment from './AddShipment';
import ReviewShipment from './AddShipment/review';
import ViewShipment from './viewShipment';
import ReceiveShipment from './receiveShipment';
import MapviewShipment from './viewShipment/MapShipment';
import QrcodeScanner from './receiveShipment/QrcodeScanner';

import InBoundList from './InBoundList';
import OutBoundList from './OutBoundList';
import { AcceptShipmentPopup } from '../../components/popUp';
const headerOptions = {
    headerShown: false,
};
import InboundShipmentSVG from '../../assets/img/InboundShipment.svg'
import OutboundShipmentSVG from '../../assets/img/OutboundShipment.svg'
import { CustomTabBar } from '../Notificans/stack';

const Stack = createNativeStackNavigator();
const Tab = createMaterialTopTabNavigator();

const ShipmentStack = () => {

    return (
        <Stack.Navigator screenOptions={headerOptions} initialRoute={'shipment'}>
            <Stack.Screen
                name="shipment"
                component={ShipmentTab}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="addshipment"
                component={AddShipment}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ReviewShipment"
                component={ReviewShipment}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="MapviewShipment"
                component={MapviewShipment}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ViewShipment"
                component={ViewShipment}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="QrcodeScanner"
                component={QrcodeScanner}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ReceiveShipment"
                component={ReceiveShipment}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};

const ShipmentTab = props => {
    const isFocused = useIsFocused();
    const { t } = useTranslation();

    const [isAdd, setISAdd] = React.useState(true);
    const [isPopupVisible, setPopupVisible] = React.useState(false);
    const closPopUp = () => {
        setPopupVisible(false);
    };
    useEffect(() => {
        async function fetchData() {
            setISAdd(true);
        }
        fetchData();
    }, [isFocused]);
    return (
        <View style={{ flex: 1, backgroundColor: Colors.whiteFF }}>
            <Header navigation={props.navigation} name={t('Shipment')} />
            {/* <Search
                navigation={props.navigation}
                fromScreen={'shipment'}
            /> */}
            <AcceptShipmentPopup
                message=""
                title={t('Products added to Inventory')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Confirm')}
                navigation={props.navigation}
            />

            <View
                style={{
                    backgroundColor: Colors.whiteFF,
                    borderTopLeftRadius: 10,
                    borderTopEndRadius: 10,
                    paddingBottom: 5,
                }}>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 12,
                    }}>
                    <View
                        style={{
                            borderRadius: 2,
                            width: scale(72),
                            height: scale(3),
                            backgroundColor: '#9B9B9B',
                        }}
                    />
                </View>
            </View>
            <View />
            <Tab.Navigator
                initialRoute={'inboundshipment'}
                // screenOptions={{
                //     tabBarLabelStyle: {
                //         fontSize: 14,
                //         textTransform: 'capitalize',
                //         fontFamily: fonts.BOLD,
                //     },
                //     tabBarInactiveTintColor: '#7F7F7F',
                //     tabBarActiveTintColor: '#208196',

                //     tabBarIndicatorStyle: { backgroundColor: '#208196' },

                //     tabBarPressColor: '#208196',
                // }}
                tabBar={(props) => <CustomTabBar {...props} />}

            >
                <Tab.Screen
                    name="outboundshipment"
                    component={OutBoundList}
                    options={{
                        // tabBarLabel: t('Outbound Shipments'),
                        tabBarLabel: 'Outbound Shipments',
                        tabBarIcon: () => <OutboundShipmentSVG height={25} width={25} />,
                    }}
                />

                <Tab.Screen
                    name="inboundshipment"
                    component={InBoundList}
                    options={{
                        // tabBarLabel: t('Inbound Shipments'),
                        tabBarLabel: 'Inbound Shipments',
                        tabBarIcon: () => <InboundShipmentSVG height={25} width={25} />,
                    }}
                />
            </Tab.Navigator>

            {/* <View style={{
                alignItems: 'flex-end', justifyContent: 'flex-end', marginRight: scale(15)
            }}>
                <View style={{
                    position: 'absolute', bottom: 10,
                }}>
                    {isAdd ? <TouchableOpacity onPress={() => setISAdd(false)} style={{ backgroundColor: Colors.whiteFF, borderRadius: scale(42), }}>

                        <AntDesign name='pluscircle' size={40} color={'#16B0D2'} />
                    </TouchableOpacity> : <TouchableOpacity onPress={() => setISAdd(true)} style={{ backgroundColor: '#208196', borderRadius: 12, padding: 12 }}>
                        <TouchableOpacity onPress={() => { setISAdd(true); props.navigation.navigate('addshipment', { acceptOrderDetail: {} }) }}
                            style={{ flexDirection: 'row', padding: scale(8), justifyContent: 'center', backgroundColor: '#0B6579', borderRadius: scale(16), }}>
                            <Text style={{ color: '#fff', fontFamily: fonts.MEDIUM, marginRight: 10, fontSize: 14 }}>{'Create'}</Text>
                            <AntDesign name='plus' size={20} color={'#fff'} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => { setISAdd(true); setPopupVisible(true) }}
                            style={{ flexDirection: 'row', marginTop: scale(7), padding: scale(8), justifyContent: 'center', backgroundColor: '#0B6579', borderRadius: scale(16), }}>
                            <Text style={{ color: '#fff', fontFamily: fonts.MEDIUM, marginRight: 10, fontSize: 14 }}>{'Accept'}</Text>

                            <AntDesign name='checkcircleo' size={18} color={'#fff'} />
                        </TouchableOpacity>

                    </TouchableOpacity>}
                </View>
            </View> */}
        </View>
    );
};
export default ShipmentStack;
