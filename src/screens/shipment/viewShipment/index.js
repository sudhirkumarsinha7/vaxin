/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useRoute } from '@react-navigation/native';

import { useIsFocused } from '@react-navigation/native';
import { getShipmentDetails } from '../../../redux/action/shipment';
import { scale } from '../../../components/Scale';
import { Colors } from '../../../components/Common/Style';
import fonts from '../../../../FontFamily';
import { formatDateDDMMYYYY } from '../../../Util/utils';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import { ChainOfCustordy, ShipmentStatus } from '../helpler';
import { CustomButtonLine } from '../../../components/Common/helper';
import { OrderStatus } from '../../order/helpler';
import { RejectionListData, getRejectionLabel } from '../receiveShipment';
export const ProductHeader = () => {
    const { t } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.35, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Name')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Id')}
                </Text>
            </View>
            <View style={{ flex: 0.35, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t(' Batches')}</Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Order Qty')}
                </Text>
            </View>
            <View
                style={{
                    flex: 0.3,
                    justifyContent: 'center',
                    padding: 2,
                    alignItems: 'center',
                }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Shipped Qty')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Received Qty')}
                </Text>
            </View>
        </View>
    );
};
export const ProductList = props => {
    const [isExtended, setExtended] = useState(false);
    const { t } = useTranslation();

    const { item = {}, status = '', orederId = '', received = [], rejected = [], ordered = [] } = props;
    const { product = {}, batches = [] } = item;
    const { units = '' } = product;
    // Filter Shipments
    const orderedProd = ordered?.find(
        (item1) => item1?.productId === item?.productId
    );

    const receivedProd = received?.find(
        (item1) => item1?.productId === item?.productId
    );

    const rejectedProd = rejected?.find(
        (item1) => item1?.productId === item?.productId
    );

    // Filter Mapping
    let totalShippedQty = 0;
    for (let i = 0; i < item?.batches?.length; i++) {
        totalShippedQty += item.batches[i]?.quantity;
    }

    const filtBatch = (id) => {
        const batch = item?.batches?.find((b) => b.atomId === id);
        return batch?.atom?.batchNo;
    };

    const eachRejection = batch => {
        const comments = batch?.type === "OTHER" && batch?.comment;
        const Reason = t(getRejectionLabel(RejectionListData, batch?.type))
        return (
            <View
                style={{
                    padding: 7,
                    marginLeft: 10,
                    borderBottomWidth: 0.5,
                    borderColor: '#7F7F7F',
                }}>

                {comments ? <Text style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: Colors.blue8E }}>
                    {
                        "Batch No : " +
                        filtBatch(batch?.atomId) +
                        " (" +
                        batch?.quantity + " " +
                        units + ") " +
                        Reason +
                        " - " +
                        comments
                    }
                </Text> : <Text style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: Colors.blue8E }}>
                    {
                        "Batch No : " +
                        filtBatch(batch?.atomId) +
                        " (" +
                        batch?.quantity + " " +
                        units + ") " +
                        Reason

                    }
                </Text>}

            </View>
        );
    }
    return (
        <View>
            <View
                style={{ borderBottomWidth: 0.5, borderColor: '#7F7F7F', paddingTop: 5, }}>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        paddingHorizontal: 10,
                    }}>
                    <View style={{ flex: 0.35, justifyContent: 'center', padding: 2 }}>
                        <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                            {product.name}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                                color: '#7F7F7F',
                                marginTop: 5,
                            }}>
                            {product.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.35, justifyContent: 'center', padding: 2 }}>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                            }}>
                            {
                                item?.batches?.[0]?.atom?.batchNo +
                                (item?.batches?.length > 1
                                    ? " +" + (item?.batches?.length - 1) + ' Batch(s)'
                                    : "")}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                                color: '#7F7F7F',
                                marginTop: 5,
                            }}>
                            {orederId ? orderedProd?.quantity + ' ' + units : '-'}
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 0.3,
                            justifyContent: 'center',
                            padding: 2,
                            alignItems: 'center',
                        }}>
                        {totalShippedQty ? (
                            <Text
                                style={{
                                    fontFamily: fonts.REGULAR,
                                    fontSize: 13,
                                    color: '#7F7F7F',
                                    marginTop: 5,
                                }}>
                                {totalShippedQty + ' ' + units}
                            </Text>
                        ) : (
                            <Text>-</Text>
                        )}
                        {receivedProd && status != 'CREATED' ? (
                            <Text
                                style={{
                                    fontFamily: fonts.REGULAR,
                                    fontSize: 13,
                                    color: '#7F7F7F',
                                    marginTop: 5,
                                }}>
                                {receivedProd?.quantity + ' ' + units}
                            </Text>
                        ) : (
                            <Text>-</Text>
                        )}
                    </View>
                </View>
                {rejectedProd?.batches?.length ? <View>
                    <TouchableOpacity
                        style={{ alignSelf: 'center' }}
                        onPress={() => setExtended(!isExtended)}>


                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                                color: '#155897',
                                marginBottom: 7,
                                alignSelf: 'center',
                            }}>
                            {isExtended ? t('Hide Reason') : t('View Reason')}
                        </Text>
                    </TouchableOpacity>
                </View> : null}

            </View>
            {isExtended && (
                <View>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            fontSize: 15,
                            marginLeft: 15,
                            marginTop: 5
                        }}>
                        {t('Rejected Reasons')}
                    </Text>
                    <FlatList
                        data={rejectedProd?.batches}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => eachRejection(item, index)}
                    />

                </View>
            )}

        </View>
    );
};

const UserUpdatedComponent = props => {
    const { item = {} } = props;
    const { user = {} } = item;

    return (
        <View>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 14,
                    color: Colors.grayTextColor,
                    marginTop: 5,
                }}>
                {user?.firstName + ' ' + user?.lastName}
            </Text>
        </View>
    );
};

const ViewProducDetail = props => {

    const { products = [], status = '', orederId = '', received = [], rejected = [], ordered = [] } = props;
    return (
        <View>
            <ProductHeader />

            <FlatList
                data={products}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                    <ProductList
                        item={item}
                        status={status}
                        orederId={orederId}
                        received={received}
                        rejected={rejected}
                        ordered={ordered}
                    />
                )}
            />
        </View>
    );
};
const ViewShipment = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isExtended, setExtended] = useState(false);

    const route = useRoute();
    const { params } = route;
    const SelectedShipment = params.SelectedShipment || {};
    const outbound = params.outbound || true;
    useEffect(() => {
        async function fetchData() {
            await props.getShipmentDetails(SelectedShipment._id);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const { shipment_details = {} } = props;
    let {
        updatedBy = [],
        products = [],
        source = {},
        destination = {},
        createdBy = {},
        order = {},
        campaign = {},
        receivedProducts = [],
        rejectedProducts = []
    } = shipment_details;
    const { user = {} } = createdBy;
    // console.log(' shipment_details ' + JSON.stringify(shipment_details))
    return (
        <View style={styles.container}>
            <HeaderWithBack navigation={props.navigation} name={t('View Shipment')} />

            <VirtualizedList>
                <View style={{ backgroundColor: '#fff', borderRadius: 10 }}>
                    {/* <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ fontFamily: fonts.MEDIUM, fontSize: scale(16), color: Colors.black0, marginTop: 5 }}>{'View Shipment '}</Text>
                        <TouchableOpacity onPress={() => props.navigation.goBack()}>
                            <AntDesign name='close' color={'#000'} size={25} />
                        </TouchableOpacity>
                    </View> */}

                    <View
                        style={{
                            borderRadius: 12,
                            borderWidth: 0.5,
                            marginTop: 12,
                            marginHorizontal: 15,
                            borderColor: Colors.grayC5,
                        }}>
                        <View
                            style={{
                                borderBottomWidth: 0.5,
                                padding: 7,
                                paddingHorizontal: 12,
                                flexDirection: 'row',
                                borderColor: Colors.grayC5,
                            }}>
                            <View style={{ flex: 0.7, flexDirection: 'row' }}>
                                <ShipmentStatus status={shipment_details.status} outbound={outbound} />
                            </View>
                            <View
                                style={{
                                    flex: 0.3,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-end',
                                    alignContent: 'center',
                                }}>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 12,
                                        color: Colors.grayTextColor,
                                    }}>
                                    {'Date: ' + formatDateDDMMYYYY(shipment_details.createdAt)}
                                </Text>
                            </View>
                        </View>
                        <View style={{ padding: 7, paddingHorizontal: 12 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 0.7 }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.BOLD,
                                            fontSize: 16,
                                            color: Colors.blueD2,
                                            marginTop: 5,
                                        }}>
                                        {t('Shipment ID: ') + shipment_details.id}
                                    </Text>
                                    <Text
                                        style={{
                                            fontFamily: fonts.MEDIUM,
                                            fontSize: 14,
                                            color: Colors.grayTextColor,
                                            marginTop: 5,
                                        }}>
                                        {t('Created By: ') + user?.firstName + ' ' + user?.lastName}
                                    </Text>
                                </View>
                                <View style={{ flex: 0.3 }}>
                                    <CustomButtonLine
                                        buttonName={t('Show on Map')}
                                        onPressButton={() =>
                                            props.navigation.navigate('MapviewShipment')
                                        }
                                        bgColor={Colors.green00}
                                    />
                                </View>
                            </View>
                            <Text
                                style={{
                                    fontFamily: fonts.REGULAR,
                                    fontSize: 14,
                                    color: Colors.grayTextColor,
                                    marginTop: 5,
                                }}>
                                {t('From: ') + source.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.REGULAR,
                                    fontSize: 14,
                                    color: Colors.grayTextColor,
                                    marginBottom: 10,
                                    marginTop: 5,
                                }}>
                                {t('To: ') + destination.name}
                            </Text>
                            {order?.id && (
                                <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                    <View style={{ flex: 0.4 }}>
                                        <Text
                                            style={{
                                                fontFamily: fonts.BOLD,
                                                fontSize: 16,
                                                color: Colors.blueD2,
                                                marginTop: 5,
                                            }}>
                                            {t('Order ID: ') + order.id}
                                        </Text>
                                    </View>
                                    <View style={{ flex: 0.6 }}>
                                        <OrderStatus status={order.status} />
                                    </View>
                                </View>
                            )}
                            {campaign && campaign.name && (
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 14,
                                        color: Colors.blueD2,
                                        marginTop: 5,
                                    }}>
                                    {t('Campaign: ') + campaign.id + ' - ' + campaign.name}
                                </Text>
                            )}
                            {updatedBy.length ? <TouchableOpacity
                                style={{ alignSelf: 'center' }}
                                onPress={() => setExtended(!isExtended)}>
                                <AntDesign
                                    name={isExtended ? 'caretup' : 'caretdown'}
                                    size={scale(12)}
                                    color={Colors.blueChill}
                                />
                            </TouchableOpacity> : null}
                        </View>
                        {isExtended ? (
                            <View
                                style={{
                                    padding: 7,
                                    paddingHorizontal: 12,
                                    backgroundColor: Colors.lightGrayBlue,
                                    borderRadius: 12,
                                    borderColor: Colors.lightGrayBlue1,
                                    borderWidth: 0.5,
                                }}>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 14,
                                        color: Colors.blackTextColor,
                                        marginTop: 12,
                                    }}>
                                    {t('Last Updated by: ')}
                                </Text>
                                <FlatList
                                    data={updatedBy}
                                    renderItem={({ item }) => <UserUpdatedComponent item={item} />}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        ) : null}
                    </View>
                    <ChainOfCustordy item={shipment_details} />
                    <View style={{ marginTop: 20 }} />
                    {/* <SupplierLocationComponent userLocation={source} />
                    <DeliveryLocationComponent userLocation={destination} /> */}
                    <ViewProducDetail
                        products={products}
                        status={shipment_details.status}
                        orederId={order?.id}
                        ordered={order?.products}
                        received={receivedProducts}
                        rejected={rejectedProducts}
                    />
                    <View style={{ height: 100 }} />
                </View>
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
});
function mapStateToProps(state) {
    return {
        shipment_details: state.shipment.shipment_details,
    };
}
export default connect(mapStateToProps, {
    getShipmentDetails,
})(ViewShipment);
