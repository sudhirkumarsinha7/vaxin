/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    ScrollView,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { useIsFocused } from '@react-navigation/native';
import { defaultLocation, localImage } from '../../../config/global';
import MapView, { Marker, Polyline } from 'react-native-maps';

const MapShipment = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();

    const { shipment_details = {} } = props;
    const { destination = {}, source = {} } = shipment_details;
    const originCoordinate = {
        latitude: source?.location?.coordinates[0],
        longitude: source?.location?.coordinates[1],
    };

    const destinationCoordinate = {
        latitude: destination?.location?.coordinates[0],
        longitude: destination?.location?.coordinates[1],
    };
    return (
        <View style={styles.container}>
            <HeaderWithBack navigation={props.navigation} name={t('View Shipment')} />
            {/* <MapWithDirection source={source} destination={destination} /> */}
            <MapView
                style={styles.map}
                initialRegion={{
                    latitude: destinationCoordinate.latitude,
                    longitude: destinationCoordinate.longitude,
                    latitudeDelta: 0.9,
                    longitudeDelta: 0.9,
                }}>
                <Marker
                    coordinate={originCoordinate}
                    title={t('Source - ') + source.name}
                />
                <Marker
                    coordinate={destinationCoordinate}
                    title={t('Destination - ') + destination.name}
                />

                <Polyline
                    coordinates={[originCoordinate, destinationCoordinate]}
                    strokeColor="#FF0000"
                    strokeWidth={2}
                />
            </MapView>

            {/* <MapView
                style={styles.map}
                initialRegion={defaultLocation}
            >
                <Marker
                    coordinate={{ latitude: source?.location?.coordinates[0] || 17.4345506, longitude: source?.location?.coordinates[1] || 78.3766436 }}
                    title="SH10001"
                    description="Shipment in Transist"
                />
                <Marker
                    coordinate={{ latitude: destination?.location?.coordinates[0] || 17.4345506, longitude: destination?.location?.coordinates[1] || 78.3766436 }}

                    title="SH10001"
                    description="Shipment in Transist"
                    zIndex={3}
                >
                    <LocationIcon />
                </Marker>
            </MapView> */}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    map: {
        flex: 1.01,
    },
});
function mapStateToProps(state) {
    return {
        shipment_details: state.shipment.shipment_details,
    };
}
export default connect(mapStateToProps, {})(MapShipment);
