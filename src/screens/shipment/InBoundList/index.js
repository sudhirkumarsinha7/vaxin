/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect, useRef } from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    ScrollView,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import { ShipmentListComponent } from '../helpler';
import fonts from '../../../../FontFamily';
import { getProdList } from '../../../redux/action/inventory';
import Empty_Card from '../../../components/Empty_Card';
import {
    getShipmentInboudSummary,
    getInboudShipmentAnalytcs,
    getShipmentInboundDamaged,
    getShipmentInboundDelivery,
    getShipmentInboundShipped,
} from '../../../redux/action/shipment';
const listInvTab = [
    { name: 'Summary', value: 'Summary', key: 'totalInboundShipments', color: 'green' },
    { name: 'Delivered', value: 'Received', key: 'deliveredInboundShipments', color: 'green' },
    { name: 'Shipped', value: 'Shipped', key: 'shippedInboundShipments', color: 'green' },
    { name: 'Partially Delivered', value: 'Partially Received', key: 'partiallyDeliveredInboundShipments', color: 'green' },
];
import { formatNumber } from '../../../Util/utils';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import AntDesign from 'react-native-vector-icons/AntDesign';
const InBoundList = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [activeInTab, setInTab] = useState('Summary');
    const [localLoader, setLocalLoader] = useState(false);
    const [isAdd, setISAdd] = React.useState(true);

    useEffect(() => {
        async function fetchData() {
            GetData(activeInTab);
            await props.getProdList();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async (title = '') => {
        setLocalLoader(true);
        await props.getInboudShipmentAnalytcs();

        if (title === 'Summary') {
            await props.getShipmentInboudSummary();
        } else if (title === 'Delivered') {
            await props.getShipmentInboundDelivery();

        } else if (title === 'Shipped') {
            await props.getShipmentInboundShipped();

        } else if (title === 'Partially Delivered') {
            await props.getShipmentInboundDamaged();

        } else {
            await props.getShipmentInboudSummary();

        }

        setLocalLoader(false);
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    flexDirection: 'row',
                    padding: 12,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <Text style={{
                    fontFamily: fonts.MEDIUM, color:
                        activeInTab === item.name ? Colors.whiteFF : Colors.black0
                }}>{t(item.value)}</Text>
                <View
                    style={{
                        backgroundColor: '#D4E6ED',
                        paddingLeft: 5,
                        paddingRight: 5,
                        borderRadius: 10,
                        marginLeft: 5,
                        marginTop: -5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            color: '#03557C',
                            padding: 5,
                            textAlign: 'center',
                        }}>
                        {formatNumber(shipment_inbound_analytcs[item.key]) || 0}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const loadMoreSummary = async () => {
        await props.getShipmentInboudSummary(inbound_shipment_summary_page + 1);
    };
    const loadMoreDamage = async () => {
        await props.getShipmentInboundDamaged(inbound_shipment_damaged_page + 1);
    };
    const loadMoreDelivery = async () => {
        await props.getShipmentInboundDelivery(inbound_shipment_delivery_page + 1);
    };
    const loadMoreShipped = async () => {
        if (
            shipment_inbound_analytcs.shippedInboundShipments >
            inbound_shipment_shipped_list.length
        ) {
            await props.getShipmentInboundShipped(inbound_shipment_shipped_page + 1);
        }
    };
    const onChangeTab = item => {
        setInTab(item.name);
        GetData(item.name);
    };
    const {
        inbound_shipment_summary_list = [],
        inbound_shipment_delivery_list = [],
        inbound_shipment_shipped_list = [],
        inbound_shipment_damaged_list = [],
        shipment_inbound_analytcs = {},
        inbound_shipment_summary_page = 1,
        inbound_shipment_delivery_page = 1,
        inbound_shipment_shipped_page = 1,
        inbound_shipment_damaged_page = 1,
        userPermissions = {},
    } = props;

    return (
        <SafeAreaView style={styles.container}>
            {/* <FilterCardButtons onPressButton={() => props.navigation.navigate('order', {
                screen: 'Filter',

            })} /> */}

            <View
                style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    marginTop: 15,
                }}>
                {listInvTab.map(each => eachTab(each))}
            </View>
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : null}
            <VirtualizedList>
                {activeInTab === 'Summary' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 50 }}
                        data={inbound_shipment_summary_list}
                        renderItem={({ item }) => (
                            <ShipmentListComponent
                                item={item}
                                navigation={props.navigation}
                                isAccept={true}
                                isRecieve={userPermissions.RECEIVE_SHIPMENT}
                                isViewShipment={userPermissions.VIEW_SHIPMENT}
                                outbound={false}
                            />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        onEndReached={loadMoreSummary}
                        onEndReachedThreshold={10}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Delivered' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 50 }}
                        data={inbound_shipment_delivery_list}
                        renderItem={({ item }) => (
                            <ShipmentListComponent
                                item={item}
                                navigation={props.navigation}
                                isViewShipment={userPermissions.VIEW_SHIPMENT}
                                outbound={false}

                            />
                        )}
                        onEndReached={loadMoreDelivery}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Shipped' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 50 }}
                        data={inbound_shipment_shipped_list}
                        renderItem={({ item }) => (
                            <ShipmentListComponent
                                item={item}
                                navigation={props.navigation}
                                isAccept={true}
                                isRecieve={userPermissions.RECEIVE_SHIPMENT}
                                isViewShipment={userPermissions.VIEW_SHIPMENT}
                                outbound={false}

                            />
                        )}
                        onEndReached={loadMoreShipped}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Partially Delivered' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 50 }}
                        data={inbound_shipment_damaged_list}
                        renderItem={({ item }) => (
                            <ShipmentListComponent
                                item={item}
                                navigation={props.navigation}
                                isViewShipment={userPermissions.VIEW_SHIPMENT}
                                outbound={false}

                            />
                        )}
                        onEndReached={loadMoreDamage}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {/* <Text>{'inbound_shipment_damaged_list ' + inbound_shipment_damaged_list.length}</Text> */}

            </VirtualizedList>
            {userPermissions.CREATE_SHIPMENT && (
                <View
                    style={{
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        marginRight: scale(15),
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 10,
                        }}>
                        <TouchableOpacity
                            onPress={() =>
                                props.navigation.navigate('addshipment', {
                                    acceptOrderDetail: {},
                                    campaignDetail: {},
                                })
                            }
                            style={{
                                backgroundColor: Colors.whiteFF,
                                borderRadius: scale(42),
                            }}>
                            <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteFF,
        borderRadius: scale(15),
    },
});

function mapStateToProps(state) {
    return {
        inbound_shipment_summary_list: state.shipment.inbound_shipment_summary_list,
        inbound_shipment_delivery_list:
            state.shipment.inbound_shipment_delivery_list,
        inbound_shipment_shipped_list: state.shipment.inbound_shipment_shipped_list,
        inbound_shipment_damaged_list: state.shipment.inbound_shipment_damaged_list,
        shipment_inbound_analytcs: state.shipment.shipment_inbound_analytcs,
        inbound_shipment_summary_count:
            state.shipment.inbound_shipment_summary_count,
        inbound_shipment_delivery_count:
            state.shipment.inbound_shipment_delivery_count,
        inbound_shipment_shipped_count:
            state.shipment.inbound_shipment_shipped_count,
        inbound_shipment_damaged_count:
            state.shipment.inbound_shipment_damaged_count,
        inbound_shipment_summary_page: state.shipment.inbound_shipment_summary_page,
        inbound_shipment_delivery_page:
            state.shipment.inbound_shipment_delivery_page,
        inbound_shipment_shipped_page: state.shipment.inbound_shipment_shipped_page,
        inbound_shipment_damaged_page: state.shipment.inbound_shipment_damaged_page,
        loder: state.loder,
        userPermissions: state.auth.userPermissions,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    getShipmentInboudSummary,
    getInboudShipmentAnalytcs,
    getShipmentInboundDamaged,
    getShipmentInboundDelivery,
    getShipmentInboundShipped,
})(InBoundList);
