/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import { scale } from '../../components/Scale';
import {
    CustomButtonWithBorder,
} from '../../components/Common/helper';
import { Colors } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import fonts from '../../../FontFamily';
import { formatDateDDMMYYYY } from '../../Util/utils';

export function getBackgroundcolor(status) {
    if (status === 'CANCELLED' || status === 'CANCELED' || status === 'OPEN') {
        return Colors.redE6;
    } else if (status === 'RECEIVED' || status === 'CONSUMED') {
        return Colors.greenDA;
    } else {
        return Colors.lightGrayBlue;
    }
}
export function getTextcolor(status) {
    if (status === 'CANCELLED' || status === 'CANCELED' || status === 'OPEN') {
        return Colors.red00;
    } else if (status === 'RECEIVED' || status === 'CONSUMED') {
        return Colors.green00;
    } else {
        return Colors.blueEA;
    }
}
export function getTextStatus(status, outbound) {
    let newStatus = status;
    if (status === 'CANCELLED' || status === 'CANCELED') {
        newStatus = 'Canceled';
    } else if (status === 'CREATED') {
        newStatus = 'Shipped';
    } else if (status === 'PARTIALLY_RECEIVED') {
        if (outbound) {
            newStatus = 'Partially Delivered';

        } else {
            newStatus = 'Partially Received';

        }
    } else if (status === 'RECEIVED') {

        if (outbound) {
            newStatus = 'Delivered';

        } else {
            newStatus = 'Received';

        }

    } else if (status === 'CONSUMED') {
        newStatus = 'Completed';
    } else {
        newStatus = status;
    }
    return newStatus;
}
export const ShipmentStatus = ({ status, outbound }) => {
    const { t, i18n } = useTranslation();

    return (
        <View
            style={{
                padding: 7,
                paddingHorizontal: 15,
                borderRadius: 20,
                backgroundColor: getBackgroundcolor(status),
            }}>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 14,
                    color: getTextcolor(status),
                    textAlign: 'center',
                }}>
                {t(getTextStatus(status, outbound))}
            </Text>
        </View>
    );
};

export const ShipmentListComponent = props => {
    const { t, i18n } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const {
        item = {},
        isAccept = false,
        isRecieve = false,
        isViewShipment = false,
        outbound = true
    } = props;
    const { source = {}, destination = {}, status = '', } = item;
    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: scale(7),
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                    alignSelf: 'center',
                }}>
                <View style={{ flex: 0.7, flexDirection: 'row', alignSelf: 'center' }}>
                    {/* <View style={{ backgroundColor: '#C9D6EF', padding: 7, borderRadius: 20 }}>
                    <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 14, color: '#1C42C5', }}>{'Shipped'}</Text>

                </View> */}
                    <ShipmentStatus status={status} outbound={outbound} />
                </View>
                <View style={{ flex: 0.3, alignSelf: 'center' }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 12,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.createdAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: scale(7) }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: isAccept ? 0.6 : 0.95 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <View
                                style={{
                                    borderBottomWidth: 0.5,
                                    borderColor: Colors.blueD2,
                                    paddingVertical: 3,
                                }}>
                                <Text
                                    style={{
                                        fontSize: 15,
                                        color: Colors.blueD2,
                                        fontFamily: fonts.BOLD,
                                        marginTop: 5,
                                    }}>
                                    {t('Shipment ID: ') + item.id}
                                </Text>
                            </View>
                        </View>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginTop: 5,
                            }}>
                            {t('From: ') + source.name}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginBottom: 10,
                                marginTop: 5,
                            }}>
                            {t('To: ') + destination.name}
                        </Text>
                    </View>
                    {isAccept &&
                        isRecieve &&
                        status != 'RECEIVED' &&
                        status != 'PARTIALLY_RECEIVED' && (
                            <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                                <CustomButtonWithBorder
                                    buttonName={t('Accept')}
                                    onPressButton={() =>
                                        props.navigation.navigate('ReceiveShipment', {
                                            SelectedShipment: item,
                                        })
                                    }
                                    bdColor={'#208196'}
                                    textColor={'#208196'}
                                />
                            </View>
                        )}
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={scale(12)}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 7,
                        backgroundColor: Colors.lightGrayBlue,
                        borderRadius: 12,
                        borderColor: Colors.lightGrayBlue1,
                        borderWidth: 0.5,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.5, alignSelf: 'center' }}>
                            {/* <CustomButtonLine
                        buttonName={t('Track')}
                        onPressButton={() => props.navigation.navigate('MapviewShipment')}
                        bgColor={'#B26720'}
                        image={localImage.track_map}
                    /> */}
                        </View>
                        <View style={{ flex: 0.5, alignItems: 'flex-end' }}>
                            {isViewShipment && (
                                <CustomButtonWithBorder
                                    buttonName={t('View Details')}
                                    onPressButton={() =>
                                        props.navigation.navigate('ViewShipment', {
                                            SelectedShipment: item,
                                            outbound: outbound
                                        })
                                    }
                                    bdColor={'#BFBFBF'}
                                />
                            )}
                        </View>
                    </View>
                </View>
            ) : null}
        </View>
    );
};
export const ChainOfCustordy = props => {
    const { t, i18n } = useTranslation();

    const { item = {} } = props;
    const { source = {}, destination = {}, status = '' } = item;
    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                padding: 15,
                margin: 15,
                borderColor: Colors.grayC5,
            }}>
            <Text
                style={{
                    fontSize: 15,
                    color: Colors.blueD2,
                    fontFamily: fonts.BOLD,
                    marginTop: 5,
                }}>
                {t('Chain of custody')}
            </Text>

            <Text
                style={{
                    fontSize: 12,
                    color: Colors.gray9,
                    fontFamily: fonts.MEDIUM,
                    marginTop: 5,
                }}>
                {t('Departure')}
            </Text>
            <Text
                style={{
                    fontSize: 15,
                    color: Colors.black0,
                    fontFamily: fonts.MEDIUM,
                    marginTop: 5,
                }}>
                {source.name + ' ' + source.country}
            </Text>

            <Text
                style={{
                    fontSize: 12,
                    color: Colors.gray9,
                    fontFamily: fonts.MEDIUM,
                    marginTop: 5,
                }}>
                {t('Arrived')}
            </Text>
            <Text
                style={{
                    fontSize: 15,
                    color: Colors.black0,
                    fontFamily: fonts.MEDIUM,
                    marginTop: 5,
                }}>
                {destination.name + ' ' + destination.country}
            </Text>
        </View>
    );
};



