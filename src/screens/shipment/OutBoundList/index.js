/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    ScrollView,
    FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors, DeviceWidth } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import { ShipmentListComponent } from '../helpler';
import fonts from '../../../../FontFamily';
import { getProdList } from '../../../redux/action/inventory';
import { getUserInfo } from '../../../redux/action/auth';
import {
    getShipmentOutboundSummary,
    getOutboudShipmentAnalytcs,
    getShipmentOutboudDamaged,
    getShipmentOutboundDelivered,
    getShipmentOutboundShiped,
} from '../../../redux/action/shipment';
import Empty_Card from '../../../components/Empty_Card';
import { FilterCardButtons } from '../../../components/Common/Button';
import { formatNumber } from '../../../Util/utils';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';

const listInvTab = [
    {
        name: 'Summary',
        key: 'totalOutboundShipments',
        value: 'Summary',
        color: 'green',
    },
    { name: 'Delivered', value: 'Delivered', key: 'deliveredOutboundShipments', color: 'green' },
    { name: 'Shipped', value: 'Shipped', key: 'shippedOutboundShipments', color: 'green' },
    { name: 'Partially Delivered', value: 'Partially Delivered', key: 'partiallyDeliveredOutboundShipments', color: 'green' },
];
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
const OutBoundList = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [activeInTab, setInTab] = useState('Summary');
    const [localLoader, setLocalLoader] = useState(false);
    const [isAdd, setISAdd] = React.useState(true);

    useEffect(() => {
        async function fetchData() {
            GetData(activeInTab);
            await props.getUserInfo();

        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async (title = '') => {
        // await props.getProdList()
        setLocalLoader(true);
        await props.getOutboudShipmentAnalytcs();
        if (title === 'Summary') {
            await props.getShipmentOutboundSummary();
        } else if (title === 'Delivered') {
            await props.getShipmentOutboundDelivered();

        } else if (title === 'Shipped') {
            await props.getShipmentOutboundShiped();

        } else if (title === 'Partially Delivered') {
            await props.getShipmentOutboudDamaged();

        } else {
            await props.getShipmentOutboundSummary();

        }
        setLocalLoader(false);
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    flexDirection: 'row',
                    padding: 12,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <Text style={{
                    fontFamily: fonts.MEDIUM, color:
                        activeInTab === item.name ? Colors.whiteFF : Colors.black0
                }}>{t(item.value)}</Text>
                <View
                    style={{
                        backgroundColor: '#D4E6ED',
                        paddingLeft: 5,
                        paddingRight: 5,
                        borderRadius: 10,
                        marginLeft: 5,
                        marginTop: -5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            color: '#03557C',
                            padding: 5,
                            textAlign: 'center',
                        }}>
                        {formatNumber(shipment_outbound_analytcs[item.key]) || 0}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const onChangeTab = item => {
        setInTab(item.name);
        GetData(item.name);
    };

    const loadMoreSummary = async () => {
        await props.getShipmentOutboundSummary(outbound_shipment_summary_page + 1);
    };
    const loadMoreDamage = async () => {
        await props.getShipmentOutboudDamaged(outbound_shipment_damaged_page + 1);
    };
    const loadMoreDelivery = async () => {
        await props.getShipmentOutboundDelivered(
            outbound_shipment_delivery_page + 1,
        );
    };
    const loadMoreShipped = async () => {
        await props.getShipmentOutboundShiped(outbound_shipment_shipped_page + 1);

    };
    const {
        outbound_shipment_summary_list = [],
        outbound_shipment_delivery_list = [],
        outbound_shipment_shipped_list = [],
        outbound_shipment_damaged_list = [],
        shipment_outbound_analytcs = {},
        outbound_shipment_summary_page = 1,
        outbound_shipment_delivery_page = 1,
        outbound_shipment_shipped_page = 1,
        outbound_shipment_damaged_page = 1,
        userPermissions = {},
    } = props;

    return (
        <SafeAreaView style={styles.container}>
            {/* <FilterCardButtons onPressButton={() => props.navigation.navigate('order', {
                screen: 'Filter',

            })} /> */}
            <View
                style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    marginTop: 15,
                }}>
                {listInvTab.map(each => eachTab(each))}
            </View>
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : null}
            <VirtualizedList>
                {/* <Text>{JSON.stringify(shipment_outbound_analytcs)}</Text> */}
                {activeInTab === 'Summary' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_shipment_summary_list}
                        renderItem={({ item }) => (
                            <ShipmentListComponent
                                item={item}
                                navigation={props.navigation}
                                isViewShipment={userPermissions.VIEW_SHIPMENT}
                            />
                        )}
                        onEndReached={loadMoreSummary}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Delivered' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_shipment_delivery_list}
                        renderItem={({ item }) => (
                            <ShipmentListComponent
                                item={item}
                                navigation={props.navigation}
                                isViewShipment={userPermissions.VIEW_SHIPMENT}
                            />
                        )}
                        onEndReached={loadMoreDelivery}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Shipped' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_shipment_shipped_list}
                        renderItem={({ item }) => (
                            <ShipmentListComponent
                                item={item}
                                navigation={props.navigation}
                                isViewShipment={userPermissions.VIEW_SHIPMENT}
                            />
                        )}
                        onEndReached={loadMoreShipped}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Partially Delivered' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_shipment_damaged_list}
                        renderItem={({ item }) => (
                            <ShipmentListComponent
                                item={item}
                                navigation={props.navigation}
                                isViewShipment={userPermissions.VIEW_SHIPMENT}
                            />
                        )}
                        onEndReached={loadMoreDamage}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {/* <Text>{'outbound_shipment_damaged_list ' + outbound_shipment_damaged_list.length}</Text> */}
            </VirtualizedList>
            {userPermissions.CREATE_SHIPMENT && (
                <View
                    style={{
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        marginRight: scale(15),
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 10,
                        }}>
                        <TouchableOpacity
                            onPress={() =>
                                props.navigation.navigate('addshipment', {
                                    acceptOrderDetail: {},
                                    campaignDetail: {},
                                })
                            }
                            style={{
                                backgroundColor: Colors.whiteFF,
                                borderRadius: scale(42),
                            }}>
                            <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteFF,
        borderRadius: scale(15),
    },
});

function mapStateToProps(state) {
    return {
        outbound_shipment_summary_list:
            state.shipment.outbound_shipment_summary_list,
        outbound_shipment_delivery_list:
            state.shipment.outbound_shipment_delivery_list,
        outbound_shipment_shipped_list:
            state.shipment.outbound_shipment_shipped_list,
        outbound_shipment_damaged_list:
            state.shipment.outbound_shipment_damaged_list,
        shipment_outbound_analytcs: state.shipment.shipment_outbound_analytcs,
        outbound_shipment_summary_page:
            state.shipment.outbound_shipment_summary_page,
        outbound_shipment_delivery_page:
            state.shipment.outbound_shipment_delivery_page,
        outbound_shipment_shipped_page:
            state.shipment.outbound_shipment_shipped_page,
        outbound_shipment_damaged_page:
            state.shipment.outbound_shipment_damaged_page,
        userPermissions: state.auth.userPermissions,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    getUserInfo,
    getShipmentOutboundSummary,
    getOutboudShipmentAnalytcs,
    getShipmentOutboudDamaged,
    getShipmentOutboundDelivered,
    getShipmentOutboundShiped,
})(OutBoundList);
