/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useRoute } from '@react-navigation/native';

import { useIsFocused } from '@react-navigation/native';
import { getOrderDetails } from '../../../redux/action/order';
import { scale } from '../../../components/Scale';
import { Colors } from '../../../components/Common/Style';
import fonts from '../../../../FontFamily';
import { formatDateDDMMYYYY } from '../../../Util/utils';
import {
    DeliveryLocationComponent,
    SupplierLocationComponent,
} from '../AddOrder/helper';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import { CampaignImage, OrderStatus } from '../helpler';
import { removeNullKeys } from '../../inventory/helpler';

export const ProductHeader = props => {
    const { t, i18n } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Name')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Id')}
                </Text>
            </View>
            <View style={{ flex: 0.3, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Category')}</Text>
                {/* <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{'(Manufacturer)'}</Text> */}
            </View>
            <View
                style={{
                    flex: 0.3,
                    justifyContent: 'center',
                    padding: 2,
                    alignItems: 'center',
                }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Quantity')}</Text>
                <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 12 }}>
                    {t('Current Stock')}
                </Text>
            </View>
        </View>
    );
};
export const ProductList = props => {
    const { item = {} } = props;
    const cleanedObject = removeNullKeys(item);

    const { product = {} } = cleanedObject;
    const { manufacturer = {}, units = '' } = product;

    return (
        <View
            style={{
                flexDirection: 'row',
                borderBottomWidth: 0.5,
                borderColor: '#7F7F7F',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {product.name}
                </Text>
                <Text
                    style={{
                        fontFamily: fonts.REGULAR,
                        fontSize: 13,
                        color: '#155897',
                        marginTop: 5,
                    }}>
                    {product.id}
                </Text>
            </View>
            <View style={{ flex: 0.3, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.REGULAR, fontSize: 13 }}>
                    {product.type}
                </Text>
                {/* <Text style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: '#7F7F7F', marginTop: 5 }}>{manufacturer.name}</Text> */}
            </View>
            <View
                style={{
                    flex: 0.3,
                    justifyContent: 'center',
                    padding: 2,
                    alignItems: 'center',
                }}>
                <Text
                    style={{
                        fontFamily: fonts.REGULAR,
                        fontSize: 13,
                        color: '#155897',
                        marginTop: 5,
                    }}>
                    {item.quantity + ' ' + units}
                </Text>
                <Text
                    style={{
                        fontFamily: fonts.REGULAR,
                        fontSize: 13,
                        color: Colors.green00,
                        marginTop: 5,
                    }}>
                    {'(' + item.currentStock + ' ' + units + ')'}
                </Text>
            </View>
        </View>
    );
};

const ViewProducDetail = props => {
    const { products = [] } = props;
    return (
        <View>
            <ProductHeader />

            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={products}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => <ProductList item={item} />}
            />
        </View>
    );
};
const UserUpdatedComponent = props => {
    const { item = [] } = props;
    const { user = {}, locationId = {} } = item;

    return (
        <View>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 14,
                    color: Colors.grayTextColor,
                    marginTop: 5,
                }}>
                {user?.firstName + ' ' + user?.lastName}
            </Text>
        </View>
    );
};
const ViewOrder = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [isExtended, setExtended] = useState(false);

    const route = useRoute();
    const { params } = route;
    const SelectedOrder = params.SelectedOrder || {};
    useEffect(() => {
        async function fetchData() {
            await props.getOrderDetails(SelectedOrder._id);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const { order_details = {} } = props;
    const {
        updatedBy = [],
        products = [],
        source = {},
        destination = {},
        createdBy = {},
        campaignId = '',
        campaign = {},
    } = order_details;
    const { user = {}, locationId = {} } = createdBy;

    // console.log('order_details ' + JSON.stringify(order_details))
    return (
        <View style={styles.container}>
            <HeaderWithBack navigation={props.navigation} name={t('View Order')} />

            <VirtualizedList>
                <View style={{ backgroundColor: '#fff', borderRadius: 10 }}>
                    {/* <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 16, color: Colors.black0, marginTop: 5 }}>{'View Order '}</Text>
                        <TouchableOpacity onPress={() => props.navigation.goBack()}>
                            <AntDesign name='close' color={'#000'} size={25} />
                        </TouchableOpacity>
                    </View> */}

                    <View
                        style={{
                            borderRadius: 12,
                            borderWidth: 0.5,
                            marginTop: 12,
                            marginHorizontal: 15,
                            borderColor: Colors.grayC5,
                        }}>
                        <View
                            style={{
                                borderBottomWidth: 0.5,
                                padding: 7,
                                paddingHorizontal: 12,
                                flexDirection: 'row',
                                borderColor: Colors.grayC5,
                            }}>
                            <View style={{ flex: 0.7, flexDirection: 'row' }}>
                                {/* <View style={{ padding:7, borderRadius: scale(20), backgroundColor: Colors.greenDA }}>
                                    <Text style={{ fontFamily: fonts.BOLD, fontSize: 14, color: Colors.green00, textAlign: 'center', }}>{order_details.status}</Text>

                                </View> */}
                                <OrderStatus status={order_details.status} />
                            </View>
                            <View
                                style={{
                                    flex: 0.3,
                                    flexDirection: 'row',
                                    justifyContent: 'flex-end',
                                    alignorder_detailss: 'center',
                                }}>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 12,
                                        color: Colors.grayTextColor,
                                    }}>
                                    {t('Date: ') + formatDateDDMMYYYY(order_details.createdAt)}
                                </Text>
                            </View>
                        </View>
                        <View style={{ padding: 7, paddingHorizontal: 12 }}>
                            <View>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.BOLD,
                                            fontSize: 16,
                                            color: Colors.blueD2,
                                            marginTop: 5,
                                        }}>
                                        {t('Order ID: ') + order_details.id}
                                    </Text>
                                    {campaignId && <CampaignImage />}
                                </View>
                                {campaign && campaign.name && (
                                    <Text
                                        style={{
                                            fontFamily: fonts.MEDIUM,
                                            fontSize: 14,
                                            color: Colors.blueD2,
                                            marginTop: 5,
                                        }}>
                                        {t('Campaign: ') + campaign.id + ' - ' + campaign.name}
                                    </Text>
                                )}
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 14,
                                        color: Colors.grayTextColor,
                                        marginTop: 5,
                                    }}>
                                    {t('Created By: ') + user?.firstName + ' ' + user?.lastName}
                                </Text>
                            </View>
                            {updatedBy.length ? <TouchableOpacity
                                style={{ alignSelf: 'center' }}
                                onPress={() => setExtended(!isExtended)}>
                                <AntDesign
                                    name={isExtended ? 'caretup' : 'caretdown'}
                                    size={scale(12)}
                                    color={Colors.blueChill}
                                />
                            </TouchableOpacity> : null}
                        </View>
                        {isExtended ? (
                            <View
                                style={{
                                    padding: 7,
                                    paddingHorizontal: 12,
                                    backgroundColor: Colors.lightGrayBlue,
                                    borderRadius: 12,
                                    borderColor: Colors.lightGrayBlue1,
                                    borderWidth: 0.5,
                                }}>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 14,
                                        color: Colors.blackTextColor,
                                        marginTop: 12,
                                    }}>
                                    {t('Last Updated by: ')}
                                </Text>
                                <FlatList
                                    contentContainerStyle={{ paddingBottom: 100 }}
                                    data={updatedBy}
                                    renderItem={
                                        ({ item }) => <UserUpdatedComponent item={item} />
                                        // <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 14, color: Colors.blackTextColor, marginTop:7 }}>{JSON.stringify(item)}</Text>
                                    }
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        ) : null}
                    </View>
                    <View style={{ marginTop: 20 }} />
                    <SupplierLocationComponent userLocation={destination} />
                    <DeliveryLocationComponent userLocation={source} />
                    <ViewProducDetail products={products} />
                    <View style={{ height: 100 }} />
                </View>
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
function mapStateToProps(state) {
    return {
        order_details: state.order.order_details,
    };
}
export default connect(mapStateToProps, {
    getOrderDetails,
})(ViewOrder);
