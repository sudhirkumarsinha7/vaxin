/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react';
import { View } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { useIsFocused } from '@react-navigation/native';

import { scale } from '../../components/Scale';
import { fonts, Colors } from '../../components/Common/Style';
import Header from '../../components/Header';
import { useTranslation } from 'react-i18next';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AddOrder from './AddOrder';
import ReviewOrder from './AddOrder/revieworder';
import ViewOrder from './viewOrder';
import TransferOrder from './AddOrder/transfer';
import { connect } from 'react-redux';

import InBoundList from './InBoundList';
import OutBoundList from './OutBoundList';
import Filter from './Filter';
const headerOptions = {
    headerShown: false,
};
const Stack = createNativeStackNavigator();
const Tab = createMaterialTopTabNavigator();
import InboundOrdersSVG from '../../assets/img/InboundOrders.svg'
import OutboundOrdersSVG from '../../assets/img/OutboundOrders.svg'
import { CustomTabBar } from '../Notificans/stack';

const OrderStack = () => {
    return (
        <Stack.Navigator screenOptions={headerOptions} initialRoute={'order'}>
            <Stack.Screen
                name="order"
                component={OrderTab}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="addorder"
                component={AddOrder}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="transfer"
                component={TransferOrder}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="revieworder"
                component={ReviewOrder}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ViewOrder"
                component={ViewOrder}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Filter"
                component={Filter}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};

const OrderTab = props => {
    const isFocused = useIsFocused();
    const [isAdd, setISAdd] = React.useState(true);
    const { t } = useTranslation();

    useEffect(() => {
        async function fetchData() {
            setISAdd(true);
        }
        fetchData();
    }, [isFocused]);
    const { userInfo = {}, userPermissions = {} } = props;
    return (
        <View style={{ flex: 1, backgroundColor: Colors.whiteFF }}>
            <Header navigation={props.navigation} name={'order'} />
            {/* <Search
                navigation={props.navigation}
                fromScreen={'Inventory'}
            /> */}
            <View
                style={{
                    backgroundColor: Colors.whiteFF,
                    borderTopLeftRadius: 10,
                    borderTopEndRadius: 10,
                    paddingBottom: 10,
                }}>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 12,
                    }}>
                    <View
                        style={{
                            borderRadius: 2,
                            width: 75,
                            height: 5,
                            backgroundColor: '#9B9B9B',
                        }}
                    />
                </View>
            </View>
            <Tab.Navigator
                initialRoute={'inbound'}
                // screenOptions={{
                //     tabBarLabelStyle: {
                //         fontSize: 16,
                //         textTransform: 'capitalize',
                //         fontFamily: fonts.BOLD,
                //     },
                //     tabBarInactiveTintColor: '#7F7F7F',
                //     tabBarActiveTintColor: '#208196',
                //     tabBarIndicatorStyle: { backgroundColor: '#208196' },
                //     tabBarPressColor: '#208196',
                // }}
                tabBar={(props) => <CustomTabBar {...props} />}
            >
                <Tab.Screen name="outbound"
                    component={OutBoundList}
                    options={{
                        // tabBarLabel: t('Outbound'),
                        tabBarLabel: 'Outbound',
                        tabBarIcon: () => <OutboundOrdersSVG height={25} width={35} />,
                    }} />

                <Tab.Screen name="inbound"
                    component={InBoundList}
                    options={{
                        // tabBarLabel: t('Inbound'),
                        tabBarLabel: 'Inbound',
                        tabBarIcon: () => <InboundOrdersSVG height={25} width={35} />,
                    }}
                />
            </Tab.Navigator>
            {/* <View style={{
                alignItems: 'flex-end', justifyContent: 'flex-end', marginRight: scale(15)
            }}>
                <View style={{
                    position: 'absolute', bottom: 10,
                }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('addorder')} style={{ backgroundColor: Colors.whiteFF, borderRadius: scale(42), }}>

                        <AntDesign name='pluscircle' size={40} color={'#16B0D2'} />
                    </TouchableOpacity>
                </View>
            </View> */}
        </View>
    );
};
function mapStateToProps(state) {
    return {
        userInfo: state.auth.userInfo,
        userPermissions: state.auth.userPermissions,
    };
}
export default connect(mapStateToProps, {})(OrderStack);
