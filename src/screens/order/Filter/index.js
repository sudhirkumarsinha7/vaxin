

import React, { useState, useEffect } from 'react';
import {

    View,
    SafeAreaView,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors, DeviceWidth } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';


import { useIsFocused } from '@react-navigation/native';

import fonts from '../../../../FontFamily';

import { getUserInfo, getUserLocation } from '../../../redux/action/auth'

import {
    getInboudHistory,
    getInboundAccepted,
    getInboundRejected,
    getInboundReceived,
    getInboudOrderAnalytcs,
    getOrderDeliveryLocation,
} from '../../../redux/action/order';

import Empty_Card from '../../../components/Empty_Card';
import { PopupMessage } from '../../../components/popUp';
import { localImage } from '../../../config/global';
import { ToastShow } from '../../../components/Toast';
import { FilterCardButtons } from '../../../components/Common/Button';
import { HeaderWithBack } from '../../../components/Header';
import { InputField, CustomInputText, CustomButton, CustomButtonLine, SubHeaderComponent, CustomButtonWithBorder } from "../../../components/Common/helper"
import { Dropdowns } from 'react-native-element-dropdown';

let FilterData = [
    { name: 'Today', id: 'today' },
    { name: 'Last Week', id: 'week' },
    { name: 'Last Month', id: 'month' },
    { name: 'Last 3 Months', id: 'threeMonth' },
    { name: 'Last 6 Months', id: 'sixMonth' },
    { name: 'Last Year', id: 'year' },
];
const Filter = (props) => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();

    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
    }, [isFocused]);
    const GetData = async () => {



    }
    const APIcall = () => {

    }
    const eachData = (item) => {

        return (
            <View
                style={{
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderWidth: 0.5,
                    margin: 5,
                    padding: 10,
                    borderColor: '#20232B',
                }}>
                <TouchableOpacity onPress={() => APIcall(item)}>
                    <Text
                        style={{
                            color: '#20232B',
                            fontFamily: fonts.MEDIUM
                        }}>
                        {t(item.name)}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
    return (
        <View style={styles.container}>

            <HeaderWithBack navigation={props.navigation}
                name={t('Filter')}
                back={() => props.navigation.navigate('order')}

            />
            <View style={{ backgroundColor: '#fff', margin: 12 }}>
                <View style={{ flexDirection: 'row', padding: 7 }}>
                    <Text style={{ flex: 0.9, fontFamily: fonts.MEDIUM, fontSize: 14 }}>{'Select all that apply'}</Text>
                    <TouchableOpacity>
                        <Text style={{ color: '#208196', fontFamily: fonts.MEDIUM, fontSize: 14 }}>Clear</Text>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ backgroundColor: Colors.lightGrayBlue, padding: 7 }}>
                <View style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                    marginTop: 15

                }}>
                    {FilterData.map((each) => (
                        eachData(each)
                    ))}
                </View>
            </View>

            <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                <CustomButtonWithBorder
                    bdColor={'#C5C5C5'}
                    textColor={'#20232B'}
                    buttonName={'Apply Filters'}
                    navigation={props.navigation}
                    onPressButton={() => props.navigation.goBack()}
                />
            </View>

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteFF,
        marginRight: 15
    },

});

function mapStateToProps(state) {
    return {
        inbound_history_list: state.order.inbound_history_list,
        inbound_received_list: state.order.inbound_received_list,
        inbound_accepted_list: state.order.inbound_accepted_list,
        inbound_rejected_list: state.order.inbound_rejected_list,
        order_inbound_analytcs: state.order.order_inbound_analytcs,
        order_outbound_analytcs: state.order.order_outbound_analytcs


    };
}
export default connect(mapStateToProps, {


},
)(Filter);