/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { getProdList } from '../../../redux/action/inventory';

import {
    getOutboudHistory,
    getOutboundAccepted,
    getOutboundSent,
    getOutboundRejected,
    update_order_status,
    getOutboudOrderAnalytcs,
    getOutboudTransfer,
    add_order,
    getOutboudShippedOrder,
    getOutboudPartialShippedOrder,
    getOutboudFullFilled,
    getOutboudPartialFullFilled,
    getOutboudCanceled
} from '../../../redux/action/order';
import { useIsFocused } from '@react-navigation/native';
import {
    InOrderComponet,
    InOrderAcceptComponet,
    InOrderRejectComponet,
    InOrderSentComponet,
} from '../helpler';
import Empty_Card from '../../../components/Empty_Card';
import { ToastShow } from '../../../components/Toast';
const listInvTab = [
    { name: 'History', value: 'History', key: 'totalOrders', color: 'green', },
    { name: 'Sent', value: 'Sent', key: 'sentOrders', color: 'green', },
    { name: 'Accepted', value: 'Accepted', key: 'acceptedOrders', color: 'green', },
    { name: 'Rejected', value: 'Rejected', key: 'rejectedOrders', color: 'green', },
    { name: 'Transfer', value: 'Transfer', key: 'transferOrders', color: 'green', },
    { name: 'Shipped', value: 'Shipped', key: 'shippedOrders', color: 'green', },
    { name: 'Partially Shipped', value: 'Partially Shipped', key: 'partiallyShippedOrders', color: 'green', },
    { name: 'Fulfilled', value: 'Fulfilled', key: 'fullFiledOrders', color: 'green', },
    { name: 'Partially Fulfilled', value: 'Partially Fulfilled', key: 'partialFullFiledOrders', color: 'green', },
    { name: 'Cancelled', value: 'Cancelled', key: 'canceledOrders', color: 'green', },
];
import fonts from '../../../../FontFamily';
import { PopupMessage } from '../../../components/popUp';
import { localImage } from '../../../config/global';
import { formatNumber, NetworkUtils } from '../../../Util/utils';
import { getUserInfo } from '../../../redux/action/auth';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import {
    queryAllorder,
    queryDeleteAllOrder,
} from '../../../databases/allSchemas';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
const OutBoundList = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [activeInTab, setInTab] = useState('History');
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [isPopupVisible, setPopupVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('Order ID: ');
    const [isAdd, setISAdd] = React.useState(true);

    useEffect(() => {
        async function fetchData() {
            GetData(activeInTab);
            await props.getUserInfo();
            await props.getProdList();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async (title = '') => {
        const isConnected = await NetworkUtils.isNetworkAvailable();

        if (isConnected) {
            syncOrderData();
            setLocalLoader(true);
            await props.getOutboudOrderAnalytcs();

            if (title === 'History') {
                await props.getOutboudHistory();
            } else if (title === 'Sent') {
                await props.getOutboundSent();
            } else if (title === 'Accepted') {
                await props.getOutboundAccepted();
            } else if (title === 'Rejected') {
                await props.getOutboundRejected();
            } else if (title === 'Shipped') {
                await props.getOutboudShippedOrder();
            } else if (title === 'Partially Shipped') {
                await props.getOutboudPartialShippedOrder();
            } else if (title === 'Fulfilled') {
                await props.getOutboudFullFilled();
            } else if (title === 'Partially Fulfilled') {
                await props.getOutboudPartialFullFilled();
            } else if (title === 'Cancelled') {
                await props.getOutboudCanceled();
            } else if (title === 'Transfer') {
                await props.getOutboudTransfer();
            } else {
                await props.getOutboudHistory();
            }
            setISAdd(true);

            setLocalLoader(false);
        }
    };
    const syncOrderData = async () => {
        let data = await queryAllorder();
        // console.log('out bound queryAllorder list ' + JSON.stringify(data));

        if (data.length) {
            for (var i = 0; i < data.length; i++) {
                let productData = data[i].products;
                let cleanedData = productData.map(obj =>
                    Object.fromEntries(
                        Object.entries(obj).filter(([key, value]) => value !== null),
                    ),
                );

                let newbody = {
                    destination: data[i].destination,
                    source: data[i].source,
                    fromName: data[i].fromName,
                    toName: data[i].toName,
                    products: cleanedData,
                    locations: data[i].locations,
                };
                // console.log('out bound newbody ' + JSON.stringify(newbody));

                await props.add_order(newbody);
            }
            await queryDeleteAllOrder();
        } else {
        }
    };

    const _cancelOrder = async item => {
        const orderbody = {
            orderId: item._id,
            status: 'CANCELLED',
        };

        const result = await props.update_order_status(orderbody);
        setLocalLoader(false);

        console.log('update res result' + JSON.stringify(result));
        if (result?.status === 200) {
            const orderID =
                (result.data && result.data.data && result.data.data.id) || '';
            const msg = 'Order ID: ' + orderID;
            setSuccessMsg(msg);

            await GetData(activeInTab);
            setPopupVisible(true);
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    flexDirection: 'row',
                    padding: 12,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <Text style={{
                    fontFamily: fonts.MEDIUM, color:
                        activeInTab === item.name ? Colors.whiteFF : Colors.black0
                }}>{t(item.value)}</Text>
                <View
                    style={{
                        backgroundColor: '#D4E6ED',
                        paddingLeft: 5,
                        paddingRight: 5,
                        borderRadius: 10,
                        marginLeft: 5,
                        marginTop: -5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            color: '#03557C',
                            padding: 5,
                            textAlign: 'center',
                        }}>
                        {formatNumber(order_outbound_analytcs[item.key]) || 0}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const onChangeTab = item => {
        setInTab(item.name);
        GetData(item.name);
    };

    const loadMoreHistory = async () => {
        await props.getOutboudHistory(outbound_order_history_page + 1);
    };
    const loadMoreSent = async () => {
        await props.getOutboundSent(outbound_order_sent_page + 1);
    };
    const loadMoreAccepted = async () => {
        await props.getOutboundAccepted(outbound_order_accepted_page + 1);
    };
    const loadMoreRejected = async () => {
        await props.getOutboundRejected(outbound_order_rejected_page + 1);

    };
    const loadMoreTransfer = async () => {
        await props.getOutboudTransfer(outbound_order_transfer_page + 1);

    };

    const loadMoreShipped = async () => {
        await props.getOutboudShippedOrder(outbound_shipped_order_page + 1);
    };
    const loadMorepartilalShipped = async () => {
        await props.getOutboudPartialShippedOrder(outbound_partial_shipped_order_page + 1);
    };
    const loadMoreFullFil = async () => {
        await props.getOutboudFullFilled(outbound_fullfill_order_page + 1);
    };

    const loadMorePartialFullFiled = async () => {
        await props.getOutboudPartialFullFilled(outbound_partilal_fullfill_page + 1);

    };
    const loadMoreCancled = async () => {
        await props.getOutboudCanceled(outbound_canceled_page + 1);

    };
    const closPopUp = () => {
        setPopupVisible(false);
        props.navigation.navigate('order');
    };
    const {
        outbound_history_list = [],
        outbound_sent_list = [],
        outbound_accepted_list = [],
        outbound_rejected_list = [],
        outbound_transfer_list = [],
        outbound_shipped_order_list = [],
        outbound_partial_shipped_order_list = [],
        outbound_fullfill_order_list = [],
        outbound_partilal_fullfill_order_list = [],
        outbound_canceled_order_list = [],
        order_outbound_analytcs = {},
        userPermissions = {},
        outbound_order_history_page = 1,
        outbound_order_sent_page = 1,
        outbound_order_accepted_page = 1,
        outbound_order_rejected_page = 1,
        outbound_order_transfer_page = 1,
        outbound_shipped_order_page = 1,
        outbound_partial_shipped_order_page = 1,
        outbound_fullfill_order_page = 1,
        outbound_partilal_fullfill_page = 1,
        outbound_canceled_page = 1,
    } = props;

    // console.log('outbound_history_list' + JSON.stringify(outbound_history_list))
    return (
        <SafeAreaView style={styles.container}>
            {/* <FilterCardButtons onPressButton={() => props.navigation.navigate('Filter')} /> */}

            <PopupMessage
                message={successMsg}
                title={t('Order has been canceled Successfully')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Okay')}
                image={localImage.congrats}
            />
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : null}
            <VirtualizedList>

                <View
                    style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        marginTop: 15,
                    }}>
                    {listInvTab.map(each => eachTab(each))}
                </View>
                {activeInTab === 'History' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_history_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                outbound={true}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreHistory}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}

                {activeInTab === 'Accepted' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_accepted_list}
                        renderItem={({ item }) => (
                            <InOrderAcceptComponet
                                item={item}
                                navigation={props.navigation}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreAccepted}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Rejected' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_rejected_list}
                        onEndReached={loadMoreRejected}
                        onEndReachedThreshold={10}
                        renderItem={({ item }) => (
                            <InOrderRejectComponet
                                item={item}
                                navigation={props.navigation}
                                outbound={true}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Sent' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_sent_list}
                        renderItem={({ item }) => (
                            <InOrderSentComponet
                                item={item}
                                navigation={props.navigation}
                                cancelOrder={() => _cancelOrder(item)}
                                isCancel={userPermissions.CANCEL_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreSent}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Transfer' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_transfer_list}
                        renderItem={({ item }) => (
                            <InOrderSentComponet
                                item={item}
                                navigation={props.navigation}
                                cancelOrder={() => _cancelOrder(item)}
                                isCancel={userPermissions.CANCEL_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                                isReOrder={userPermissions.RE_ORDER}
                                outbound={true}
                            />
                        )}
                        onEndReached={loadMoreTransfer}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Shipped' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_shipped_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                outbound={true}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreShipped}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Partially Shipped' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_partial_shipped_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                outbound={true}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMorepartilalShipped}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Fulfilled' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_fullfill_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                outbound={true}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreFullFil}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Partially Fulfilled' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_partilal_fullfill_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                outbound={true}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMorePartialFullFiled}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Cancelled' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={outbound_canceled_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                outbound={true}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreCancled}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {/* <Text style={{ textAlign: 'center' }}>{'outbound_history_list ' + outbound_history_list.length}</Text> */}
            </VirtualizedList>
            {userPermissions.CREATE_ORDER && (
                <View
                    style={{
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        marginRight: scale(15),
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 10,
                        }}>
                        {isAdd ? (
                            <TouchableOpacity
                                onPress={() => setISAdd(false)}
                                style={{
                                    backgroundColor: Colors.whiteFF,
                                    borderRadius: scale(42),
                                }}>
                                <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                                onPress={() => setISAdd(true)}
                                style={{
                                    backgroundColor: '#208196',
                                    borderRadius: 12,
                                    padding: 12,
                                }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        setISAdd(true);
                                        props.navigation.navigate('addorder');
                                    }}
                                    style={{
                                        flexDirection: 'row',
                                        padding: scale(8),
                                        justifyContent: 'center',
                                        backgroundColor: '#0B6579',
                                        borderRadius: scale(16),
                                    }}>
                                    <Text
                                        style={{
                                            color: '#fff',
                                            fontFamily: fonts.MEDIUM,
                                            marginRight: 10,
                                            fontSize: 14,
                                        }}>
                                        {t('Create')}
                                    </Text>
                                    <AntDesign name="plus" size={20} color={'#fff'} />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => {
                                        setISAdd(true);
                                        props.navigation.navigate('transfer');
                                    }}
                                    style={{
                                        flexDirection: 'row',
                                        marginTop: scale(7),
                                        padding: scale(8),
                                        justifyContent: 'center',
                                        backgroundColor: '#0B6579',
                                        borderRadius: scale(16),
                                    }}>
                                    <Text
                                        style={{
                                            color: '#fff',
                                            fontFamily: fonts.MEDIUM,
                                            marginRight: 10,
                                            fontSize: 14,
                                        }}>
                                        {t('Transfer')}
                                    </Text>

                                    <Entypo name="swap" size={18} color={'#fff'} />
                                </TouchableOpacity>
                            </TouchableOpacity>
                        )}
                    </View>
                </View>
            )}
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteFF,
        borderRadius: scale(15),
    },
});

function mapStateToProps(state) {
    return {
        outbound_history_list: state.order.outbound_history_list,
        outbound_sent_list: state.order.outbound_sent_list,
        outbound_accepted_list: state.order.outbound_accepted_list,
        outbound_rejected_list: state.order.outbound_rejected_list,
        outbound_shipped_order_list: state.order.outbound_shipped_order_list,
        outbound_partial_shipped_order_list: state.order.outbound_partial_shipped_order_list,
        outbound_fullfill_order_list: state.order.outbound_fullfill_order_list,
        outbound_partilal_fullfill_order_list: state.order.outbound_partilal_fullfill_order_list,
        outbound_canceled_order_list: state.order.outbound_canceled_order_list,

        order_outbound_analytcs: state.order.order_outbound_analytcs,
        outbound_transfer_list: state.order.outbound_transfer_list,
        loder: state.loder,
        userPermissions: state.auth.userPermissions,
        outbound_order_history_page: state.order.outbound_order_history_page,
        outbound_order_sent_page: state.order.outbound_order_sent_page,
        outbound_order_accepted_page: state.order.outbound_order_accepted_page,
        outbound_order_rejected_page: state.order.outbound_order_rejected_page,
        outbound_order_transfer_page: state.order.outbound_order_transfer_page,
        outbound_shipped_order_page: state.order.outbound_shipped_order_page,
        outbound_partial_shipped_order_page: state.order.outbound_partial_shipped_order_page,
        outbound_fullfill_order_page: state.order.outbound_fullfill_order_page,
        outbound_partilal_fullfill_page: state.order.outbound_partilal_fullfill_page,
        outbound_canceled_page: state.order.outbound_canceled_page,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    getOutboudHistory,
    getOutboundAccepted,
    getOutboundSent,
    getOutboundRejected,
    update_order_status,
    getOutboudOrderAnalytcs,
    getUserInfo,
    add_order,
    getOutboudTransfer,
    getOutboudShippedOrder,
    getOutboudPartialShippedOrder,
    getOutboudFullFilled,
    getOutboudPartialFullFilled,
    getOutboudCanceled
})(OutBoundList);
