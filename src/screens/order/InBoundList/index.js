/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    ScrollView,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import {
    InOrderComponet,
    InOrderReciveComponet,
    InOrderAcceptComponet,
    InOrderRejectComponet,
} from '../helpler';
import fonts from '../../../../FontFamily';
import { getProdList } from '../../../redux/action/inventory';
import { getUserInfo, getUserLocation } from '../../../redux/action/auth';

import {
    getInboudHistory,
    getInboundAccepted,
    getInboundRejected,
    getInboundReceived,
    respond_order_status,
    getInboudOrderAnalytcs,
    getOrderDeliveryLocation,
    add_order,
    getInboudShippedOrder,
    getInboundPartialShippedOrder,
    getInboundFullFilled,
    getInboundPartialFullFilled,
    getInboundCanceled,
} from '../../../redux/action/order';
const listInvTab = [
    { name: 'History', value: 'History', key: 'totalOrders', color: 'green', },
    { name: 'Received', value: 'Received', key: 'receivedOrders', color: 'green', },
    { name: 'Accepted', value: 'Accepted', key: 'acceptedOrders', color: 'green', },
    { name: 'Rejected', value: 'Rejected', key: 'rejectedOrders', color: 'green', },
    { name: 'Shipped', value: 'Shipped', key: 'shippedOrders', color: 'green', },
    { name: 'Partially Shipped', value: 'Partially Shipped', key: 'partiallyShippedOrders', color: 'green', },
    { name: 'Fulfilled', value: 'Fulfilled', key: 'fullFiledOrders', color: 'green', },
    { name: 'Partially Fulfilled', value: 'Partially Fulfilled', key: 'partialFullFiledOrders', color: 'green', },
    { name: 'Cancelled', value: 'Cancelled', key: 'canceledOrders', color: 'green', },
];
import AntDesign from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Empty_Card from '../../../components/Empty_Card';
import { PopupMessage } from '../../../components/popUp';
import { localImage } from '../../../config/global';
import { ToastShow } from '../../../components/Toast';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import { formatNumber, NetworkUtils } from '../../../Util/utils';
import {
    queryAllorder,
    queryDeleteAllOrder,
} from '../../../databases/allSchemas';
const InBoundList = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [activeInTab, setInTab] = useState('History');
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [isPopupVisible, setPopupVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('Order ID: ');
    const [isAdd, setISAdd] = React.useState(true);

    useEffect(() => {
        async function fetchData() {
            setISAdd(true);

            GetData(activeInTab);
            await props.getUserInfo();
            await props.getProdList();
            await props.getUserLocation();
            await props.getOrderDeliveryLocation();

        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async (title = '') => {
        const isConnected = await NetworkUtils.isNetworkAvailable();

        if (isConnected) {
            // syncOrderData()
            setLocalLoader(true);
            await props.getInboudOrderAnalytcs();

            if (title === 'History') {
                await props.getInboudHistory();
            } else if (title === 'Received') {
                await props.getInboundReceived();
            } else if (title === 'Accepted') {
                await props.getInboundAccepted();
            } else if (title === 'Rejected') {
                await props.getInboundRejected();
            } else if (title === 'Shipped') {
                await props.getInboudShippedOrder();
            } else if (title === 'Partially Shipped') {
                await props.getInboundPartialShippedOrder();
            } else if (title === 'Fulfilled') {
                await props.getInboundFullFilled();
            } else if (title === 'Partially Fulfilled') {
                await props.getInboundPartialFullFilled();
            } else if (title === 'Cancelled') {
                await props.getInboundCanceled();
            } else {
                await props.getInboudHistory();
            }

            setLocalLoader(false);
        }
    };
    const syncOrderData = async () => {
        let data = await queryAllorder();
        // console.log('out bound queryAllorder list ' + JSON.stringify(data));

        if (data.length) {
            for (var i = 0; i < data.length; i++) {
                let productData = data[i].products;
                let cleanedData = productData.map(obj =>
                    Object.fromEntries(
                        Object.entries(obj).filter(([key, value]) => value !== null),
                    ),
                );

                let newbody = {
                    destination: data[i].destination,
                    source: data[i].source,
                    fromName: data[i].fromName,
                    toName: data[i].toName,
                    products: cleanedData,
                    locations: data[i].locations,
                };
                // console.log('out bound newbody ' + JSON.stringify(newbody));

                await props.add_order(newbody);
            }
            await queryDeleteAllOrder();
        } else {
        }
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    flexDirection: 'row',
                    padding: 12,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <Text style={{
                    fontFamily: fonts.MEDIUM, color:
                        activeInTab === item.name ? Colors.whiteFF : Colors.black0
                }}>{t(item.value)}</Text>
                <View
                    style={{
                        backgroundColor: '#D4E6ED',
                        paddingLeft: 5,
                        paddingRight: 5,
                        borderRadius: 10,
                        marginLeft: 5,
                        marginTop: -5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            color: '#03557C',
                            padding: 5,
                            textAlign: 'center',
                        }}>
                        {formatNumber(order_inbound_analytcs[item.key]) || 0}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const onChangeTab = item => {
        setInTab(item.name);
        GetData(item.name);
    };

    const _updateStatus = async (item, status) => {
        const orderbody = {
            orderId: item._id,
            status: status,
        };

        const result = await props.respond_order_status(orderbody);
        setLocalLoader(false);

        // console.log('cancelOrder res result' + JSON.stringify(result));
        if (result?.status === 200) {
            const orderID =
                (result.data && result.data.data && result.data.data.id) || '';
            const msg = 'Order ID: ' + orderID;
            await GetData(activeInTab);
            setSuccessMsg(msg);
            setPopupVisible(true);

            if (status === 'ACCEPTED') {
                setInTab('Accepted');
            } else {
                setInTab('Rejected');
            }
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };
    const closPopUp = () => {
        setPopupVisible(false);
        props.navigation.navigate('order');
    };
    const loadMoreHistory = async () => {
        await props.getInboudHistory(inbound_order_history_page + 1);
    };
    const loadMoreReceived = async () => {
        await props.getInboundReceived(inbound_order_received_page + 1);
    };
    const loadMoreRejected = async () => {
        await props.getInboundRejected(inbound_order_rejected_page + 1);

    };
    const loadMoreAccepted = async () => {
        await props.getInboundAccepted(inbound_order_accepted_page + 1);
    };



    const loadMoreShipped = async () => {
        await props.getInboudShippedOrder(inbound_shipped_order_page + 1);
    };
    const loadMorepartilalShipped = async () => {
        await props.getInboundPartialShippedOrder(inbound_partial_shipped_order_page + 1);
    };
    const loadMoreFullFil = async () => {
        await props.getInboundFullFilled(inbound_fullfill_order_page + 1);
    };

    const loadMorePartialFullFiled = async () => {
        await props.getInboundPartialFullFilled(inbound_partilal_fullfill_page + 1);

    };
    const loadMoreCancled = async () => {
        await props.getInboundCanceled(inbound_canceled_page + 1);

    };
    const {
        inbound_history_list = [],
        inbound_received_list = [],
        inbound_accepted_list = [],
        inbound_rejected_list = [],
        inbound_shipped_order_list = [],
        inbound_partial_shipped_order_list = [],
        inbound_fullfill_order_list = [],
        inbound_partilal_fullfill_order_list = [],
        inbound_canceled_order_list = [],
        order_inbound_analytcs = {},
        userPermissions = {},
        inbound_order_history_page = 1,
        inbound_order_received_page = 1,
        inbound_order_accepted_page = 1,
        inbound_order_rejected_page = 1,
        inbound_shipped_order_page = 1,
        inbound_partial_shipped_order_page = 1,
        inbound_fullfill_order_page = 1,
        inbound_partilal_fullfill_page = 1,
        inbound_canceled_page = 1,
    } = props;
    return (
        <SafeAreaView style={styles.container}>
            {/* <FilterCardButtons onPressButton={() => props.navigation.navigate('Filter')} /> */}

            <PopupMessage
                message={successMsg}
                title={t('Order has been updated Successfully')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Okay')}
                image={localImage.congrats}
            />

            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : null}
            <VirtualizedList>
                <View
                    style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                        marginTop: 15,
                    }}>
                    {listInvTab.map(each => eachTab(each))}
                </View>
                {activeInTab === 'History' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_history_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreHistory}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Received' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_received_list}
                        renderItem={({ item }) => (
                            <InOrderReciveComponet
                                item={item}
                                navigation={props.navigation}
                                acceptOrder={() => _updateStatus(item, 'ACCEPTED')}
                                rejectOrder={() => _updateStatus(item, 'REJECTED')}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreReceived}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Accepted' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_accepted_list}
                        renderItem={({ item }) => (
                            <InOrderAcceptComponet
                                item={item}
                                navigation={props.navigation}
                                inbound={true}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreAccepted}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Rejected' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_rejected_list}
                        renderItem={({ item }) => (
                            <InOrderRejectComponet
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreRejected}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Shipped' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_shipped_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreShipped}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Partially Shipped' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_partial_shipped_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMorepartilalShipped}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Fulfilled' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_fullfill_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreFullFil}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Partially Fulfilled' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_partilal_fullfill_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMorePartialFullFiled}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Cancelled' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inbound_canceled_order_list}
                        renderItem={({ item }) => (
                            <InOrderComponet
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                                isViewOrder={userPermissions.VIEW_ORDER}
                            />
                        )}
                        onEndReached={loadMoreCancled}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {/* <Text>{'inbound_history_list ' + inbound_history_list.length}</Text> */}

            </VirtualizedList>
            {userPermissions.CREATE_ORDER && (
                <View
                    style={{
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        marginRight: scale(15),
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 10,
                        }}>
                        {isAdd ? (
                            <TouchableOpacity
                                onPress={() => setISAdd(false)}
                                style={{
                                    backgroundColor: Colors.whiteFF,
                                    borderRadius: scale(42),
                                }}>
                                <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                                onPress={() => setISAdd(true)}
                                style={{
                                    backgroundColor: '#208196',
                                    borderRadius: 12,
                                    padding: 12,
                                }}>
                                <TouchableOpacity
                                    onPress={() => {
                                        setISAdd(true);
                                        props.navigation.navigate('addorder');
                                    }}
                                    style={{
                                        flexDirection: 'row',
                                        padding: scale(8),
                                        justifyContent: 'center',
                                        backgroundColor: '#0B6579',
                                        borderRadius: scale(16),
                                    }}>
                                    <Text
                                        style={{
                                            color: '#fff',
                                            fontFamily: fonts.MEDIUM,
                                            marginRight: 10,
                                            fontSize: 14,
                                        }}>
                                        {t('Create')}
                                    </Text>
                                    <AntDesign name="plus" size={20} color={'#fff'} />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => {
                                        setISAdd(true);
                                        props.navigation.navigate('transfer');
                                    }}
                                    style={{
                                        flexDirection: 'row',
                                        marginTop: scale(7),
                                        padding: scale(8),
                                        justifyContent: 'center',
                                        backgroundColor: '#0B6579',
                                        borderRadius: scale(16),
                                    }}>
                                    <Text
                                        style={{
                                            color: '#fff',
                                            fontFamily: fonts.MEDIUM,
                                            marginRight: 10,
                                            fontSize: 14,
                                        }}>
                                        {t('Transfer')}
                                    </Text>

                                    <Entypo name="swap" size={18} color={'#fff'} />
                                </TouchableOpacity>
                            </TouchableOpacity>
                        )}
                    </View>
                </View>
            )}
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteFF,
        borderRadius: scale(15),
    },
});

function mapStateToProps(state) {
    return {
        inbound_history_list: state.order.inbound_history_list,
        inbound_received_list: state.order.inbound_received_list,
        inbound_accepted_list: state.order.inbound_accepted_list,
        inbound_rejected_list: state.order.inbound_rejected_list,

        inbound_shipped_order_list: state.order.inbound_shipped_order_list,
        inbound_partial_shipped_order_list: state.order.inbound_partial_shipped_order_list,
        inbound_fullfill_order_list: state.order.inbound_fullfill_order_list,
        inbound_partilal_fullfill_order_list: state.order.inbound_partilal_fullfill_order_list,
        inbound_canceled_order_list: state.order.inbound_canceled_order_list,

        order_inbound_analytcs: state.order.order_inbound_analytcs,
        order_outbound_analytcs: state.order.order_outbound_analytcs,
        loder: state.loder,
        userPermissions: state.auth.userPermissions,
        inbound_order_history_page: state.order.inbound_order_history_page,
        inbound_order_received_page: state.order.inbound_order_received_page,
        inbound_order_accepted_page: state.order.inbound_order_accepted_page,
        inbound_order_rejected_page: state.order.inbound_order_rejected_page,
        inbound_shipped_order_page: state.order.inbound_shipped_order_page,
        inbound_partial_shipped_order_page: state.order.inbound_partial_shipped_order_page,
        inbound_fullfill_order_page: state.order.inbound_fullfill_order_page,
        inbound_partilal_fullfill_page: state.order.inbound_partilal_fullfill_page,
        inbound_canceled_page: state.order.inbound_canceled_page,

    };
}
export default connect(mapStateToProps, {
    getProdList,
    getUserLocation,
    getInboudHistory,
    getInboundAccepted,
    getInboundRejected,
    getInboundReceived,
    respond_order_status,
    getInboudOrderAnalytcs,
    getOrderDeliveryLocation,
    getUserInfo,
    add_order,
    getInboudShippedOrder,
    getInboundPartialShippedOrder,
    getInboundFullFilled,
    getInboundPartialFullFilled,
    getInboundCanceled
})(InBoundList);
