/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    Image,
} from 'react-native';
import { scale } from '../../components/Scale';
import {
    CustomButtonWithBorder,
} from '../../components/Common/helper';
import { Colors } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import fonts from '../../../FontFamily';
import { formatDateDDMMYYYY } from '../../Util/utils';
import Entypo from 'react-native-vector-icons/Entypo';
import { localImage } from '../../config/global';
import CampaignSvg from '../../assets/img/campaign.svg'

export function getBackgroundcolor(status, outbound = true) {
    let Color = Colors.greenDA;

    if (status === 'CANCELLED' || status === 'CANCELED') {
        Color = Colors.redE6;
    } else if (status === 'CREATED') {
        if (outbound) {
            Color = '#efeffd';
        } else {
            Color = '#9848b5';
        }
    } else if (status === 'PARTIALLY_FULFILLED') {
        Color = '#e7cff2';
    } else if (status === 'SHIPPED') {
        Color = '#efeffd';
    } else if (status === 'FULFILLED') {
        Color = Colors.greenDA;
    } else if (status === 'ACCEPTED') {
        Color = Colors.greenDA;
    } else if (status === 'PARTIALLY_SHIPPED') {
        Color = Colors.greenDA;
    } else if (status === 'REJECTED') {
        Color = Colors.redE6;
    }
    return Color;
}
export function getTextcolor(status, outbound = true) {
    let Color = Colors.green00;

    if (
        status === 'CANCELLED' ||
        status === 'CANCELED' ||
        status === 'REJECTED'
    ) {
        Color = Colors.red00;
    } else if (status === 'CREATED') {
        if (outbound) {
            Color = 'blue';
        } else {
            Color = '#545aeb';
        }
    } else if (status === 'PARTIALLY_FULFILLED') {
        Color = '#9848b5';
    } else if (status === 'SHIPPED') {
        Color = '#545aeb';
    } else if (status === 'FULFILLED') {
        Color = Colors.green00;
    } else if (status === 'ACCEPTED') {
        Color = Colors.green00;
    } else if (status === 'PARTIALLY_SHIPPED') {
        Color = Colors.green00;
    } else if (status === 'REJECTED') {
        Color = Colors.red00;
    }
    return Color;
}
export function getreorderStatus(status) {
    if (
        status === 'CANCELLED' ||
        status === 'CANCELED' ||
        status === 'REJECTED'
    ) {
        return true;
    } else {
        return false;
    }
}

export function getTextStatus(status, outbound = true) {
    let newStatus = status;

    if (status === 'CANCELLED' || status === 'CANCELED') {
        newStatus = 'Canceled';
    } else if (status === 'CREATED') {
        if (outbound) {
            newStatus = 'Sent';
        } else {
            newStatus = 'Received';
        }
    } else if (status === 'PARTIALLY_FULFILLED') {
        newStatus = 'Partially Fulfilled';
    } else if (status === 'SHIPPED') {
        newStatus = 'Transit&Fulfilled';
    } else if (status === 'FULFILLED') {
        newStatus = 'Fulfilled';
    } else if (status === 'ACCEPTED') {
        newStatus = 'Accepted';
    } else if (status === 'PARTIALLY_SHIPPED') {
        newStatus = 'Transit&PartiallyFulfilled';
    } else if (status === 'REJECTED') {
        newStatus = 'Rejected';
    } else {
        // return capitalizeFirstLetter(status)
        newStatus = status;
    }
    return newStatus;
}
export const OrderStatus = ({ status, outbound }) => {
    const { t } = useTranslation();

    return (
        <View
            style={{
                padding: 7,
                paddingHorizontal: 15,
                borderRadius: scale(20),
                backgroundColor: getBackgroundcolor(status),
            }}>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 14,
                    color: getTextcolor(status),
                    textAlign: 'center',
                }}>
                {t(getTextStatus(status, outbound))}
            </Text>
        </View>
    );
};
export const CampaignImage = () => {
    return (
        <View style={{ marginLeft: 10 }}>
            <CampaignSvg height={30} width={30} />
        </View>
    );
};

export const InOrderComponet = props => {
    const { t, i18n } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const {
        item = {},
        outbound = false,
        isReOrder = false,
        isViewOrder = false,
    } = props;
    const {
        products = [],
        source = {},
        destination = {},
        createdBy = {},
        isTransfer = false,
        campaignId = '',
    } = item;
    const { user = {} } = createdBy;
    let fName = user && user.firstName ? user.firstName : '';
    let lName = user && user.lastName ? user.lastName : '';
    let userName = fName + ' ' + lName;
    const firstproduct = products.length
        ? products[0].product && products[0].product.name
        : '';
    const ProductName =
        products.length > 1
            ? firstproduct + ' +' + (Number(products.length) - 1)
            : firstproduct;
    const unit = products.length
        ? products[0].product &&
        products[0].product.units &&
        products[0].product.units
        : '';

    const Qty = products.length ? products[0].quantity + ' ' + unit : '0 Unit';
    const reorderPrd = () => {
        const newList = products.map(each => ({
            productId: each.product && each.product._id,
            quantity: each.quantity || 100,
            units: each.product && each.product.units,
            productName: each.product && each.product.name,
            type: each.product && each.product.type,
            fromPage: 'ReOrder',
        }));

        props.navigation.navigate('revieworder', {
            productList: newList,
            supplierLoc: destination,
            deliveryLoc: source,
            updQty: true,
        });

        // props.navigation.navigate('revieworder', {
        //     productList: newList
        // })
    };
    return (
        <View
            style={{
                borderRadius: 15,
                borderWidth: 0.5,
                marginTop: 15,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 7,
                    paddingHorizontal: 15,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }}>
                    <Text
                        style={{
                            fontSize: 16,
                            color: Colors.blueD2,
                            fontFamily: fonts.BOLD,
                            marginTop: 5,
                            marginRight: 5,
                        }}>
                        {t('Order ID: ') + item.id}
                    </Text>
                    {isTransfer && !getreorderStatus(item.status) && (
                        <Entypo name="swap" size={18} color={'blue'} />
                    )}
                    {campaignId && <CampaignImage />}
                </View>
                <View
                    style={{
                        flex: 0.4,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 15,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.createdAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 7, paddingHorizontal: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.6 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {ProductName}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                color: '#7F7F7F',
                                marginTop: 5,
                            }}>
                            {Qty}
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 0.4,
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                        }}>
                        <OrderStatus status={item.status} outbound={outbound} />
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={scale(12)}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 7,
                        paddingHorizontal: 15,
                        backgroundColor: Colors.lightGrayBlue,
                        borderRadius: 15,
                        borderColor: Colors.lightGrayBlue1,
                        borderWidth: 0.5,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.6 }}>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Supplier: ') + destination.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Delivery: ') + source.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Order sent by: ') + userName}
                            </Text>
                        </View>
                        <View
                            style={{
                                flex: 0.4,
                                justifyContent: 'flex-end',
                                alignItems: 'flex-end',
                            }}>
                            {isViewOrder && (
                                <CustomButtonWithBorder
                                    buttonName={t('View')}
                                    onPressButton={() =>
                                        props.navigation.navigate('ViewOrder', {
                                            SelectedOrder: item,
                                        })
                                    }
                                    bdColor={Colors.grayTextColor}
                                />
                            )}
                            {outbound && getreorderStatus(item.status) && isReOrder && (
                                <CustomButtonWithBorder
                                    buttonName={t('Reorder')}
                                    onPressButton={reorderPrd}
                                    bdColor={Colors.lightGrayBlue1}
                                    bgColor={Colors.whiteFF}
                                />
                            )}
                        </View>
                    </View>
                </View>
            ) : null}
        </View>
    );
};
export const InOrderReciveComponet = props => {
    const { t, i18n } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const { item = {} } = props;
    const {
        products = [],
        source = {},
        destination = {},
        createdBy = {},
        campaignId = '',
    } = item;
    const { user = {} } = createdBy;
    let fName = user && user.firstName ? user.firstName : '';
    let lName = user && user.lastName ? user.lastName : '';
    let userName = fName + ' ' + lName;

    const firstproduct = products.length
        ? products[0].product && products[0].product.name
        : '';
    const ProductName =
        products.length > 1 ? firstproduct + ' +' + products.length : firstproduct;
    const unit = products.length
        ? products[0].product &&
        products[0].product.units &&
        products[0].product.units
        : '';

    const Qty = products.length ? products[0].quantity + ' ' + unit : '0 Unit';
    return (
        <View
            style={{
                borderRadius: 7,
                borderWidth: 0.5,
                marginTop: 15,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 7,
                    paddingHorizontal: 15,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }}>
                    <Text
                        style={{
                            fontSize: 16,
                            color: Colors.blueD2,
                            fontFamily: fonts.BOLD,
                            marginTop: 5,
                            marginRight: 5,
                        }}>
                        {t('Order ID: ') + item.id}
                    </Text>
                    {campaignId && <CampaignImage />}
                </View>
                <View
                    style={{
                        flex: 0.4,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 15,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.createdAt)}
                    </Text>
                </View>
            </View>
            <View style={{ paddingHorizontal: 15 }}>
                <View style={{ flexDirection: 'row', }}>
                    <View style={{ flex: 0.6 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                                marginTop: 10,
                            }}>
                            {ProductName}
                        </Text>
                        <View>
                            <View
                                style={{ flexDirection: 'row', }}>
                                <CustomButtonWithBorder
                                    buttonName={t('Reject')}
                                    onPressButton={() => props.rejectOrder()}
                                    bdColor={Colors.grayTextColor}
                                />
                                <CustomButtonWithBorder
                                    buttonName={t('Accept')}
                                    onPressButton={() => props.acceptOrder()}
                                    bdColor={Colors.blueChill}
                                    textColor={Colors.blueChill}
                                />
                            </View>
                        </View>
                    </View>
                    <View
                        style={{
                            flex: 0.4,
                            alignItems: 'flex-end',
                        }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                color: '#7F7F7F',
                                marginTop: 10,
                            }}>
                            {Qty}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 7,
                        paddingHorizontal: 15,
                        backgroundColor: Colors.lightGrayBlue,
                        borderRadius: 15,
                        borderColor: Colors.lightGrayBlue1,
                        borderWidth: 0.5,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.6 }}>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Supplier: ') + destination.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Delivery: ') + source.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Order sent by: ') + userName}
                            </Text>
                        </View>
                        <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                            <CustomButtonWithBorder
                                buttonName={t('View Details')}
                                onPressButton={() =>
                                    props.navigation.navigate('ViewOrder', { SelectedOrder: item })
                                }
                                bdColor={Colors.lightGrayBlue1}
                            />
                        </View>
                    </View>
                </View>
            ) : null}
        </View>
    );
};

export const InOrderAcceptComponet = props => {
    const { t, i18n } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const { item = {}, inbound = false, isViewOrder = false } = props;
    const {
        products = [],
        source = {},
        destination = {},
        createdBy = {},
        campaignId = '',
    } = item;

    const { user = {} } = createdBy;
    let fName = user && user.firstName ? user.firstName : '';
    let lName = user && user.lastName ? user.lastName : '';
    let userName = fName + ' ' + lName;
    const firstproduct = products.length
        ? products[0].product && products[0].product.name
        : '';
    const ProductName =
        products.length > 1 ? firstproduct + ' +' + products.length : firstproduct;
    const unit = products.length
        ? products[0].product &&
        products[0].product.units &&
        products[0].product.units
        : '';
    const Qty = products.length ? products[0].quantity + ' ' + unit : '0 Unit';

    return (
        <View
            style={{
                borderRadius: 7,
                borderWidth: 0.5,
                marginTop: 15,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    paddingHorizontal: 15,
                    padding: 7,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }}>
                    <Text
                        style={{
                            fontSize: 16,
                            color: Colors.blueD2,
                            fontFamily: fonts.BOLD,
                            marginTop: 5,
                            marginRight: 5,
                        }}>
                        {t('Order ID: ') + item.id}
                    </Text>
                    {campaignId && <CampaignImage />}
                </View>
                <View
                    style={{
                        flex: 0.4,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 15,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.createdAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 7, paddingHorizontal: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.5 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {ProductName}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                color: '#7F7F7F',
                                marginTop: 5,
                            }}>
                            {Qty}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blackTextColor,
                                marginTop: 15,
                            }}>
                            {t('Order sent by: ') + userName}
                        </Text>
                    </View>
                    {inbound && (
                        <View
                            style={{
                                flex: 0.5,
                                flexDirection: 'row',
                                justifyContent: 'flex-end',
                                alignItems: 'center',
                            }}>
                            <TouchableOpacity
                                onPress={() =>
                                    props.navigation.navigate('shipment', {
                                        screen: 'addshipment',
                                        params: {
                                            acceptOrderDetail: item,
                                            campaignDetail: {},
                                        },
                                    })
                                }
                                style={{
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    margin: 10,
                                    backgroundColor: Colors.blueChill,
                                    padding: 10,
                                    paddingHorizontal: 14,
                                    borderRadius: 15,
                                    flexDirection: 'row',
                                }}>
                                <Text
                                    style={{
                                        color: '#fff',
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 14,
                                    }}>
                                    {t('Create Shipment')}
                                </Text>
                            </TouchableOpacity>
                        </View>
                    )}
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={scale(12)}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 7,
                        paddingHorizontal: 15,
                        backgroundColor: Colors.lightGrayBlue,
                        borderRadius: 15,
                        borderColor: Colors.lightGrayBlue1,
                        borderWidth: 0.5,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.6 }}>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Supplier: ') + destination.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Delivery: ') + source.name}
                            </Text>
                        </View>
                        <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                            {isViewOrder && (
                                <CustomButtonWithBorder
                                    buttonName={t('View Details')}
                                    onPressButton={() =>
                                        props.navigation.navigate('ViewOrder', {
                                            SelectedOrder: item,
                                        })
                                    }
                                    bdColor={Colors.lightGrayBlue1}
                                />
                            )}
                        </View>
                    </View>
                </View>
            ) : null}
        </View>
    );
};
export const InOrderSentComponet = props => {
    const { t, i18n } = useTranslation();
    const [isExtended, setExtended] = useState(false);
    const { item = {}, isCancel = false, isViewOrder = false, outbound = false, isReOrder = false } = props;
    const {
        products = [],
        source = {},
        destination = {},
        createdBy = {},
        isTransfer = false,
        campaignId = '',
    } = item;
    const { user = {} } = createdBy;
    let fName = user && user.firstName ? user.firstName : '';
    let lName = user && user.lastName ? user.lastName : '';
    let userName = fName + ' ' + lName;

    const firstproduct = products.length
        ? products[0].product && products[0].product.name
        : '';
    const ProductName =
        products.length > 1 ? firstproduct + ' +' + products.length : firstproduct;
    const unit = products.length
        ? products[0].product &&
        products[0].product.units &&
        products[0].product.units
        : '';

    const Qty = products.length ? products[0].quantity + ' ' + unit : '0 Unit';
    const reorderPrd = () => {
        const newList = products.map(each => ({
            productId: each.product && each.product._id,
            quantity: each.quantity || 100,
            units: each.product && each.product.units,
            productName: each.product && each.product.name,
            type: each.product && each.product.type,
            fromPage: 'ReOrder',
        }));

        props.navigation.navigate('revieworder', {
            productList: newList,
            supplierLoc: destination,
            deliveryLoc: source,
            updQty: true,
        });
    };
    return (
        <View
            style={{
                borderRadius: 15,
                borderWidth: 0.5,
                marginTop: 15,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 7,
                    paddingHorizontal: 15,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }}>
                    <Text
                        style={{
                            fontSize: 16,
                            color: Colors.blueD2,
                            fontFamily: fonts.BOLD,
                            marginTop: 5,
                            marginRight: 5,
                        }}>
                        {t('Order ID: ') + item.id}
                    </Text>
                    {/* {isTransfer && !getreorderStatus(item.status) && (
                        <Entypo name="swap" size={18} color={'blue'} />
                    )} */}
                    {isTransfer && (
                        <Entypo name="swap" size={18} color={'blue'} />
                    )}
                    {campaignId && <CampaignImage />}
                </View>

                <View
                    style={{
                        flex: 0.4,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 15,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.createdAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 7, paddingHorizontal: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.6 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {ProductName}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                color: '#7F7F7F',
                                marginTop: 5,
                            }}>
                            {Qty}
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 0.4,
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                        }}>
                        <OrderStatus status={item.status} />
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={scale(12)}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 7,
                        paddingHorizontal: 15,
                        backgroundColor: Colors.lightGrayBlue,
                        borderRadius: 15,
                        borderColor: Colors.lightGrayBlue1,
                        borderWidth: 0.5,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.6 }}>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Supplier: ') + destination.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Delivery: ') + source.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Order sent by: ') + userName}
                            </Text>
                        </View>
                        <View style={{ flex: 0.4 }}>
                            <View
                                style={{ justifyContent: 'flex-end', alignItems: 'flex-end' }}>
                                {item.status === 'CREATED' && !getreorderStatus(item.status) && (
                                    <CustomButtonWithBorder
                                        buttonName={t('Cancel')}
                                        onPressButton={() => props.cancelOrder()}
                                        bdColor={Colors.grayTextColor}
                                    />
                                )}
                                {(item.status === 'CANCELLED' || item.status === 'REJECTED') && outbound && isReOrder && (
                                    <CustomButtonWithBorder
                                        buttonName={t('Reorder')}
                                        onPressButton={reorderPrd}
                                        bdColor={Colors.lightGrayBlue1}
                                        bgColor={Colors.whiteFF}
                                    />
                                )}
                                {isViewOrder && (
                                    <CustomButtonWithBorder
                                        buttonName={t('View')}
                                        onPressButton={() =>
                                            props.navigation.navigate('ViewOrder', {
                                                SelectedOrder: item,
                                            })
                                        }
                                        bdColor={Colors.grayTextColor}
                                    />
                                )}
                            </View>
                        </View>
                    </View>
                </View>
            ) : null}
        </View>
    );
};

export const InOrderRejectComponet = props => {
    const { t, i18n } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const {
        item = {},
        outbound = false,
        isReOrder = false,
        isViewOrder = false,
    } = props;
    const {
        products = [],
        source = {},
        destination = {},
        createdBy = {},
        campaignId = '',
    } = item;

    const { user = {} } = createdBy;
    let fName = user && user.firstName ? user.firstName : '';
    let lName = user && user.lastName ? user.lastName : '';
    let userName = fName + ' ' + lName;

    const firstproduct = products.length
        ? products[0].product && products[0].product.name
        : '';
    const ProductName =
        products.length > 1 ? firstproduct + ' +' + products.length : firstproduct;
    const unit = products.length
        ? products[0].product &&
        products[0].product.units &&
        products[0].product.units
        : '';
    const Qty = products.length ? products[0].quantity + ' ' + unit : '0 Unit';
    const reorderPrd = () => {
        const newList = products.map(each => ({
            productId: each.product && each.product._id,
            quantity: each.quantity || 100,
            units: each.product && each.product.units,
            productName: each.product && each.product.name,
            type: each.product && each.product.type,
            fromPage: 'ReOrder',
        }));

        props.navigation.navigate('revieworder', {
            productList: newList,
            supplierLoc: destination,
            deliveryLoc: source,
            updQty: true,
        });
    };
    return (
        <View
            style={{
                borderRadius: 15,
                borderWidth: 0.5,
                marginTop: 15,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 7,
                    paddingHorizontal: 15,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6, flexDirection: 'row', alignItems: 'center' }}>
                    <Text
                        style={{
                            fontSize: 16,
                            color: Colors.blueD2,
                            fontFamily: fonts.BOLD,
                            marginTop: 5,
                            marginRight: 5,
                        }}>
                        {t('Order ID: ') + item.id}
                    </Text>
                    {campaignId && <CampaignImage />}
                </View>
                <View
                    style={{
                        flex: 0.4,
                        flexDirection: 'row',
                        justifyContent: 'flex-end',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 15,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.createdAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 7, paddingHorizontal: 15 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.6 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {ProductName}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                color: '#7F7F7F',
                                marginTop: 5,
                            }}>
                            {Qty}
                        </Text>
                    </View>
                    <View
                        style={{
                            flex: 0.4,
                            flexDirection: 'row',
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                        }}>
                        <OrderStatus status={item.status} />
                    </View>
                </View>

                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={scale(12)}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 7,
                        paddingHorizontal: 15,
                        backgroundColor: Colors.lightGrayBlue,
                        borderRadius: 15,
                        borderColor: Colors.lightGrayBlue1,
                        borderWidth: 0.5,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.6 }}>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Supplier: ') + destination.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Delivery: ') + source.name}
                            </Text>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 15,
                                }}>
                                {t('Order sent by: ') + userName}
                            </Text>
                        </View>
                        <View
                            style={{
                                flex: 0.4,
                                justifyContent: 'flex-end',
                                alignItems: 'flex-end',
                            }}>
                            {isViewOrder && (
                                <CustomButtonWithBorder
                                    buttonName={t('View')}
                                    onPressButton={() =>
                                        props.navigation.navigate('ViewOrder', {
                                            SelectedOrder: item,
                                        })
                                    }
                                    bdColor={Colors.grayTextColor}
                                />
                            )}
                            {outbound && isReOrder && (
                                <CustomButtonWithBorder
                                    buttonName={t('Reorder')}
                                    onPressButton={reorderPrd}
                                    bdColor={Colors.lightGrayBlue1}
                                    bgColor={Colors.whiteFF}
                                />
                            )}
                        </View>
                    </View>
                </View>
            ) : null}
        </View>
    );
};
