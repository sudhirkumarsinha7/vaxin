/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    StyleSheet,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import {
    Colors,
} from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../../config/global';
import {
    InputField,
    CustomButton,
    CustomButtonWithBorder,
} from '../../../components/Common/helper';
import { PopupMessage } from '../../../components/popUp';
import { useRoute } from '@react-navigation/native';
import {
    removeEmptyValues,
} from '../../inventory/AddInventory/productUtil';

import {
    EditOrderProductComponent,
    ReviewOrderHeader,
    ReviewOrderProductList,
} from '../../inventory/AddInventory/productHelper';
import { add_order } from '../../../redux/action/order';
import { ToastShow } from '../../../components/Toast';
import { DeliveryLocationComponent, SupplierLocationComponent } from './helper';
import { VirtualizedList } from '../../inventory/AddInventory/productHelper';
import { NetworkUtils } from '../../../Util/utils';
import { addOrderlocal } from '../../../databases/realmConnection';

function updateObjectAtIndex(array, index, updatedObject) {
    // Check if the index is valid
    if (index >= 0 && index < array.length) {
        // Update the object at the specified index
        array[index] = { ...array[index], ...updatedObject };
    }

    return array;
}
const ReviewOrder = props => {
    const { t } = useTranslation();
    const [localLoader, setLocalLoader] = useState(false);
    const [errorMessage, setErrorMessage] = useState('');
    const [isPopupVisible, setPopupVisible] = useState(false);
    const [successMsg, setSuccessMsg] = useState('Order ID: ');
    const [isEdit, setIsEdit] = useState(false);
    const [selectedPrd, setSelectedPrd] = useState(null);
    const [currentIndx, setCurrentIndx] = useState(0);
    const route = useRoute();
    const { params } = route;
    const [productList, setProductList] = useState([]);
    // const productList = params.productList
    const {
        supplierLoc = {},
        deliveryLoc = {},
        campaign = '',
        campaignId = '',
    } = params;
    const updQty = params.updQty;
    useEffect(() => {
        console.log('params', params);

        async function fetchData() {
            setProductList(params.productList);
        }
        fetchData();
    }, [params]);

    const _createOrder = async () => {
        let orderbody = {
            destination: supplierLoc._id || parentLocation._id,
            source: deliveryLoc._id || userLocation._id,
            fromName: parentLocation.name,
            toName: userLocation.name,
            products: productList,
            locations: userLocation,
        };
        if (campaignId) {
            orderbody.campaignId = campaignId;
        }
        console.log('_createOrder orderbody' + JSON.stringify(orderbody));
        setLocalLoader(true);

        const isConnected = await NetworkUtils.isNetworkAvailable();
        let result = {};
        setLocalLoader(true);
        if (isConnected) {
            result = await props.add_order(orderbody);
        } else {
            result = await addOrderlocal(orderbody);
        }

        setLocalLoader(false);

        console.log('_createOrder res result' + JSON.stringify(result));
        if (result?.status === 200) {
            const orderID =
                (result.data && result.data.data && result.data.data.id) || '';
            const msg = 'Order ID: ' + orderID;
            setSuccessMsg(msg);

            setPopupVisible(true);
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };
    const closPopUp = () => {
        setPopupVisible(false);
        props.navigation.navigate('outbound');
    };
    const editProduct = async (item, indexToEdit) => {
        console.log('indexToEdit ' + indexToEdit);
        setCurrentIndx(indexToEdit);
        setSelectedPrd(item);
        setIsEdit(true);
    };
    const updateProductInList = data => {
        const productData = removeEmptyValues(data);
        // console.log('currentIndx', currentIndx)
        // console.log('productData', productData)
        // console.log('productList', productList)

        const newArray = updateObjectAtIndex(productList, currentIndx, productData);
        console.log('newArray', newArray);
        setProductList(newArray);
        setIsEdit(false);
    };
    const { userLocation = {} } = props;
    const { parentLocation = {} } = userLocation;
    return (
        <View style={styles.container}>
            <HeaderWithBack navigation={props.navigation} name={t('Review Order')} />
            <PopupMessage
                message={successMsg}
                title={t('Order has been created Successfully')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Okay')}
                image={localImage.congrats}
            />
            <VirtualizedList>
                <View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20 }}>
                    {props.loder || localLoader ? (
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    ) : null}

                    {isEdit ? (
                        <EditOrderProductComponent
                            selectedPrd={selectedPrd}
                            updateProductInList={updateProductInList}
                        />
                    ) : (
                        <View>
                            <ReviewOrderHeader />

                            <FlatList
                                data={productList}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={({ item, index }) => (
                                    <ReviewOrderProductList
                                        item={item}
                                        editProduct={editProduct}
                                        AddQty={updQty}
                                        index={index}
                                    />
                                )}
                            />
                        </View>
                    )}

                    <View style={{ marginTop: 20 }} />
                    {campaign && (
                        <View style={{ margin: 15 }}>
                            <InputField
                                label={t('Campaign')}
                                inputValue={campaign}
                                setInputValue={() => console.log('')}
                                disable="true"
                            />
                        </View>
                    )}

                    {/* <SupplierLocationComponent userLocation={parentLocation} />
                    <DeliveryLocationComponent userLocation={userLocation} /> */}
                    <SupplierLocationComponent
                        userLocation={supplierLoc._id ? supplierLoc : parentLocation}
                    />
                    <DeliveryLocationComponent
                        userLocation={deliveryLoc._id ? deliveryLoc : userLocation}
                    />
                </View>
            </VirtualizedList>
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : (
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <CustomButtonWithBorder
                        bdColor={'#C5C5C5'}
                        textColor={'#20232B'}
                        buttonName={t('Back')}
                        navigation={props.navigation}
                        onPressButton={() => props.navigation.goBack()}
                    />
                    <CustomButton
                        bgColor={'#208196'}
                        buttonName={t('Confirm')}
                        navigation={props.navigation}
                        onPressButton={() => _createOrder()}
                    />
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

function mapStateToProps(state) {
    return {
        userLocation: state.auth.userLocation,
    };
}
export default connect(mapStateToProps, {
    add_order,
})(ReviewOrder);
