/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';
import DropDown, { CampaignDropdown } from '../../../components/Dropdown';

import { HeaderWithBack } from '../../../components/Header';
import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
    ProductListComponent,
    AddNewProductOrderComponent,
    VirtualizedList,
    EditOrderProductComponent,
} from '../../inventory/AddInventory/productHelper';
import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../../config/global';
import fonts from '../../../../FontFamily';
import {
    filterAndMoveToFront,
    groupAndStoreByCategory,
} from '../../inventory/AddInventory/productUtil';
import { getProdList } from '../../../redux/action/inventory';
import { ToastShow } from '../../../components/Toast';
import {
    removeEmptyValues,
    filterUniqueObjectsByProperty,
} from '../../inventory/AddInventory/productUtil';
import { getUserLocationList, getUserLocation } from '../../../redux/action/auth';
import {
    getCampaignList,
    getActiveCampaignList,
} from '../../../redux/action/campaign';

function addObjectAndUpdateQuantity(array, newObj) {
    // Check if the new object meets the conditions
    const existingIndex = array.findIndex(
        item => item.productId === newObj.productId && item.type === newObj.type,
    );
    if (existingIndex !== -1) {
        // If the object already exists, update the quantity
        array[existingIndex].quantity += newObj.quantity;
    } else {
        // If the object does not exist, add it to the array
        array.push(newObj);
    }

    return array;
}
function deleteAtIndex(array, index) {
    // Check if the index is valid
    if (index >= 0 && index < array.length) {
        // Use splice to remove the element at the specified index
        array.splice(index, 1);
    }

    return array;
}
function updateObjectAtIndex(array, index, updatedObject) {
    // Check if the index is valid
    if (index >= 0 && index < array.length) {
        // Update the object at the specified index
        array[index] = { ...array[index], ...updatedObject };
    }

    return array;
}
const AddOrder = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isAdd, setISAdd] = useState(true);
    const [currentIndx, setCurrentIndx] = useState(0);
    const [selectedProductType, setSelectedProductType] = useState(null);
    const [productList, setProductList] = useState([]);
    const [productTypes, setProductTypes] = useState([]);
    const [productListByType, setProductListByType] = useState([]);
    const [isEdit, setIsEdit] = useState(false);
    const [selectedPrd, setSelectedPrd] = useState(null);
    const [supplierLoc, setSupplierLoc] = useState({});
    const [deliveryLoc, setDeliveryLoc] = useState({});
    const [supplierLocId, setSupplierLocId] = useState(null);
    const [deliveryLocId, setDeliveryLocId] = useState(null);
    const [campaignId, setCampaignId] = useState(null);
    const [campaign, setCampaign] = useState(null);
    // const actionSheetRef = createRef();
    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        await props.getProdList();
        const groupedAndStored = groupAndStoreByCategory(
            props.inv_product_list,
            'type',
        );
        const resultArray = Object.values(groupedAndStored);
        setProductTypes(resultArray);
        await props.getUserLocationList();
        await props.getUserLocation();
        await props.getCampaignList();
        await props.getActiveCampaignList();
    };

    const confirmProductlist = () => {
        if (productList.length) {
            if (supplierLocId && deliveryLocId) {
                props.navigation.navigate('revieworder', {
                    productList: productList,
                    supplierLoc,
                    deliveryLoc,
                    campaign,
                    campaignId,
                });
            } else {
                ToastShow(
                    'Please select supplier and delivery location',
                    'error',
                    'long',
                    'top',
                );
            }
        } else {
            ToastShow(t('Please add atleast one Product'), 'error', 'long', 'top');
        }
    };

    const eachProductType = (item, index) => {
        return (
            <TouchableOpacity
                style={{
                    margin: 7,
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10,
                    width: scale(90),
                    backgroundColor:
                        selectedProductType === item.type ? '#208196' : '#fff',
                    borderColor: '#7F7F7F',
                    borderRadius: 10,
                    borderWidth: 0.5,
                }}
                onPress={() => onSelectProdType(item, index)}>
                <View
                    style={{
                        backgroundColor: '#fff',
                        borderWidth: 0.5,
                        borderColor: '#7F7F7F',
                        borderBottomEndRadius: 10,
                        borderBottomLeftRadius: 10,
                        borderRadius: 12,
                    }}>
                    <Image
                        source={localImage.media}
                        style={{
                            height: scale(30),
                            width: scale(90),
                            alignSelf: 'center',
                            borderRadius: 10,
                        }}
                    />
                </View>
                <View style={{ alignSelf: 'center' }}>
                    <Text
                        numberOfLines={3}
                        ellipsizeMode="tail"
                        style={{
                            borderTopWidth: 0.2,
                            padding: 7,
                            color:
                                selectedProductType === item.type ? Colors.whiteFF : '#7F7F7F',
                            fontFamily: fonts.REGULAR,
                        }}>
                        {item.type}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const onSelectProdType = (item) => {
        setSelectedProductType(item.type);
        const uniqueArray = filterUniqueObjectsByProperty(
            item.productsByType,
            'name',
        );

        setProductListByType(uniqueArray);
    };
    const addProductInList = data => {
        // console.log('addProductInList ', data)

        let productData = removeEmptyValues(data);
        productData.type = selectedProductType;
        // console.log('addProductInList productList', productList)
        if (productList.length) {
            const newData = addObjectAndUpdateQuantity(productList, productData);
            // console.log('addProductInList newData', newData)

            setProductList(newData);
        } else {
            setProductList([productData]);
        }
    };
    const updateProductInList = data => {
        const productData = removeEmptyValues(data);

        const newArray = updateObjectAtIndex(productList, currentIndx, productData);

        setProductList(newArray);
        setIsEdit(false);
    };
    const deleteProduct = (item, indexToDelete) => {
        const newArray = deleteAtIndex(productList, indexToDelete);
        setProductList(newArray);
    };
    const editProduct = async (item, indexToEdit) => {
        setISAdd(false);
        setCurrentIndx(indexToEdit);
        setSelectedPrd(item);
        setIsEdit(true);
    };
    const changeAfterSelectSupplier = item => {
        const { value, eachItem } = item;
        setSupplierLoc(eachItem);
        setSupplierLocId(value);
    };
    const changeAfterSelectDelivery = item => {
        const { value, eachItem } = item;
        setDeliveryLoc(eachItem);
        setDeliveryLocId(value);
    };
    const changeAfterSelectCampaign = item => {
        const { value, eachItem } = item;
        setCampaignId(value);
        setCampaign(eachItem.name);
        setSelectedProductType(eachItem?.productDetails?.type);
        const product = {
            name: eachItem?.productDetails?.name,
            _id: eachItem?.productDetails?._id,
            type: eachItem?.productDetails?.type,
            units: eachItem?.productDetails?.units,
        };
        setProductListByType([product]);
    };

    const FilterproductTypes = filterAndMoveToFront(
        productTypes,
        'type',
        selectedProductType,
    );

    const {
        userLocation = {},
        userLocationList = {},
        userInfo = {},
        campaign_active_list = [],
    } = props;
    const { parentLocation = {} } = userLocation;
    const { childLocations = [] } = userLocationList;
    const { orgLevel = '' } = userInfo;

    let deliveryLoclist = childLocations.filter(loc => loc._id !== supplierLocId);
    let supplierLoclist = childLocations.filter(loc => loc._id !== deliveryLocId);

    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Transfer Order')}
            />

            <VirtualizedList>
                <View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20 }}>
                    <View
                        style={{
                            backgroundColor: Colors.whiteFF,
                            borderTopLeftRadius: 10,
                            borderTopEndRadius: 10,
                            paddingBottom: 5,
                        }}>
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: 12,
                            }}>
                            <View
                                style={{
                                    borderRadius: 2,
                                    width: scale(72),
                                    height: scale(3),
                                    backgroundColor: '#9B9B9B',
                                }}
                            />
                        </View>
                    </View>
                    <View>
                        {isAdd ? (
                            <View>
                                {orgLevel != 'EPI' && (
                                    <View style={{ marginHorizontal: 15 }}>
                                        <CampaignDropdown
                                            dropdownData={campaign_active_list}
                                            onChangeValue={changeAfterSelectCampaign}
                                            label="name"
                                            mapKey="_id"
                                            val={campaignId}
                                            placeholderText="Campaign"
                                            labelText="Campaign"
                                            isMandatoryField={false}
                                        />
                                    </View>
                                )}
                                <View
                                    style={{
                                        flex: 0.2,
                                        backgroundColor: '#fff',
                                        borderRadius: 20,
                                    }}>
                                    {!campaignId && (
                                        <FlatList
                                            contentContainerStyle={{ paddingBottom: 100 }}
                                            data={FilterproductTypes}
                                            horizontal
                                            keyExtractor={(item, index) => index.toString()}
                                            renderItem={({ item, index }) =>
                                                eachProductType(item, index)
                                            }
                                            showsHorizontalScrollIndicator={false}
                                        />
                                    )}
                                </View>

                                {selectedProductType ? (
                                    <AddNewProductOrderComponent
                                        productListByType={productListByType}
                                        addProductInList={addProductInList}
                                        selectedProductType={selectedProductType}
                                    />
                                ) : (
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            padding: 40,
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: fonts.MEDIUM,
                                                fontSize: 16,
                                                textAlign: 'center',
                                            }}>
                                            {t('*Select Product Category to show list of products')}
                                        </Text>
                                    </View>
                                )}
                            </View>
                        ) : isEdit ? (
                            <EditOrderProductComponent
                                selectedPrd={selectedPrd}
                                updateProductInList={updateProductInList}
                            />
                        ) : (
                            <ProductListComponent
                                navigation={props.navigation}
                                closeForm={() => setISAdd(true)}
                                confirmProductlist={confirmProductlist}
                                productData={productList}
                                deleteProduct={deleteProduct}
                                editProduct={editProduct}
                            />
                        )}

                        <View style={{ marginHorizontal: 10 }}>
                            <DropDown
                                dropdownData={supplierLoclist}
                                onChangeValue={changeAfterSelectSupplier}
                                label="name"
                                mapKey="_id"
                                val={supplierLocId}
                                placeholderText={t('Supplier')}
                                labelText={t('Supplier Location')}
                                errorMsg={supplierLocId ? '' : t('Required')}
                                mandatory={true}
                                search={false}
                            />

                            <DropDown
                                dropdownData={deliveryLoclist}
                                onChangeValue={changeAfterSelectDelivery}
                                label="name"
                                mapKey="_id"
                                val={deliveryLocId}
                                placeholderText={t('Delivery')}
                                labelText={t('Delivery Location')}
                                errorMsg={deliveryLocId ? '' : 'Required'}
                                mandatory={true}
                                search={false}
                            />
                        </View>

                        {/* <SupplierLocationComponent userLocation={parentLocation} />
                        <DeliveryLocationComponent userLocation={userLocation} /> */}
                    </View>
                </View>
            </VirtualizedList>

            {isAdd && (
                <View
                    style={{
                        // position: 'absolute',
                        // bottom: 10,
                        backgroundColor: '#fff',
                        padding: 15,
                    }}>
                    <TouchableOpacity
                        onPress={() => setISAdd(false)}
                        style={{ flexDirection: 'row' }}>
                        <Text
                            style={{ fontSize: 20, fontFamily: fonts.BOLD, marginRight: 10 }}>
                            {t('Selected Products')}
                        </Text>
                        <AntDesign
                            name="up"
                            size={15}
                            color={'#000'}
                            style={{ justifyContent: 'center', marginTop: 5 }}
                        />
                    </TouchableOpacity>
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        inv_product_list: state.inventory.inv_product_list,
        userLocation: state.auth.userLocation,
        userLocationList: state.auth.userLocationList,
        campaign_list: state.campaign.campaign_list,
        campaign_active_list: state.campaign.campaign_active_list,
        userInfo: state.auth.userInfo,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    getUserLocationList,
    getUserLocation,
    getCampaignList,
    getActiveCampaignList,
})(AddOrder);
