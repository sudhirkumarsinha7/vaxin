/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { } from 'react';
import { View, Text } from 'react-native';
import { useTranslation } from 'react-i18next';

import fonts from '../../../../FontFamily';
export const TextLineComponent = props => {
    const { leftText = '', rightText = '' } = props;
    return (
        <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ flex: 0.3 }}>
                <Text style={{ fontSize: 14, fontFamily: fonts.REGULAR }}>
                    {leftText}
                </Text>
            </View>
            <View style={{ flex: 0.7 }}>
                <Text
                    style={{ fontSize: 14, fontFamily: fonts.REGULAR, color: '#7F7F7F' }}>
                    {rightText}
                </Text>
            </View>
        </View>
    );
};
export const DeliveryLocationComponent = props => {
    const { userLocation = {} } = props;
    const { t } = useTranslation();

    return (
        <View style={{ margin: 15, marginTop: -5 }}>
            <Text style={{ fontSize: 16, fontFamily: fonts.MEDIUM, marginBottom: 5 }}>
                {t('Delivery Location')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: -5,
                }}>
                <TextLineComponent
                    leftText={t('Type : ')}
                    rightText={userLocation.orgLevel}
                />
                <TextLineComponent
                    leftText={t('Name : ')}
                    rightText={userLocation.name} />
                <TextLineComponent
                    leftText={t('Address : ')}
                    rightText={userLocation.division + ', ' + userLocation.district}
                />
                {/* <TextLineComponent
                leftText='District : '
                rightText={userLocation.district}
            />
            <TextLineComponent
                leftText='Division : '
                rightText={userLocation.division}
            />
            <TextLineComponent
                leftText='Country : '
                rightText={userLocation.country}
            /> */}
            </View>
        </View>
    );
};
export const SupplierLocationComponent = props => {
    const { userLocation = {} } = props;
    const { t } = useTranslation();

    return (
        <View style={{ margin: 15, marginTop: -5 }}>
            <Text style={{ fontSize: 16, fontFamily: fonts.MEDIUM, marginBottom: 5 }}>
                {t('Supplier Location')}
            </Text>

            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                    paddingTop: -5,
                }}>
                <TextLineComponent
                    leftText={t('Type : ')}
                    rightText={userLocation.orgLevel}
                />
                <TextLineComponent
                    leftText={t('Name : ')}
                    rightText={userLocation.name} />
                <TextLineComponent
                    leftText={t('Address : ')}
                    rightText={userLocation.division + ', ' + userLocation.district}
                />
                {/* <TextLineComponent
                leftText='District : '
                rightText={userLocation.district}
            />
            <TextLineComponent
                leftText='Division : '
                rightText={userLocation.division}
            />
            <TextLineComponent
                leftText='Country : '
                rightText={userLocation.country}
            /> */}
                {/* <TextLineComponent
                leftText='Pin : '
                rightText={userLocation.pinCode}
            /> */}
            </View>
        </View>
    );
};
export const LocationComponent = props => {
    const { userLocation = {} } = props;
    const { t } = useTranslation();

    return (
        <View style={{ marginTop: 10 }}>
            <View
                style={{
                    borderWidth: 1,
                    borderRadius: 15,
                    padding: 15,
                    borderColor: '#C5C5C5',
                }}>
                <TextLineComponent
                    leftText={t('Type : ')}
                    rightText={userLocation.orgLevel}
                />
                <TextLineComponent
                    leftText={t('Name : ')}
                    rightText={userLocation.name} />
                <TextLineComponent
                    leftText={t('Address : ')}
                    rightText={userLocation.division + ', ' + userLocation.district}
                />
                {/* <TextLineComponent
                leftText='District : '
                rightText={userLocation.district}
            />
            <TextLineComponent
                leftText='Division : '
                rightText={userLocation.division}
            />
            <TextLineComponent
                leftText='Country : '
                rightText={userLocation.country}
            /> */}
                {/* <TextLineComponent
                leftText='Pin : '
                rightText={userLocation.pinCode}
            /> */}
            </View>
        </View>
    );
};
