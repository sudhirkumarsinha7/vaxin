/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */


import React, { useState, useEffect } from 'react';
import {

    View,
    Text,
    StyleSheet,
    ActivityIndicator,
    Switch,
} from 'react-native';
import { connect } from 'react-redux';

import { scale } from '../../../components/Scale';
import { useTranslation } from 'react-i18next';
import { log_out, getUserInfo, UpdateNotification } from '../../../redux/action/auth';
import fonts from '../../../../FontFamily';
import { useIsFocused } from '@react-navigation/native';
import { CustomButton } from '../../../components/Common/helper';
import { ToastShow } from '../../../components/Toast';
export const ToggleButton = (props) => {
    const { t, i18n } = useTranslation();
    const { isEnabled, toggleSwitch } = props;

    return <View>
        <Switch
            trackColor={{ false: '#000000', true: '#208196' }}
            thumbColor={'#fff'}
            // ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
            style={{ transform: [{ scaleX: 0.9 }, { scaleY: 0.9 }], borderWidth: 1, borderRadius: 15, borderColor: isEnabled ? '#208196' : '#B3BEC1' }}

        />

    </View>;
};
export const AlertButton = props => {
    const { title, bgColor = 'transparent', isEnabled, toggleSwitch } = props;
    const { t } = useTranslation();
    // const [isEnabled, setIsEnabled] = useState(false);

    // const toggleSwitch = () => setIsEnabled(previousState => !previousState);
    return (
        <View
            style={{ height: 50, paddingVertical: 10, flexDirection: 'row', paddingHorizontal: 10, backgroundColor: bgColor }}>
            <View style={{ width: '70%', flexDirection: 'row', alignItems: 'center' }}>

                <Text style={{ fontFamily: fonts.REGULAR, color: '#000', fontSize: 15 }}>{t(title)}</Text>
            </View>
            <View style={{ width: '30%', alignItems: 'flex-end', justifyContent: 'space-between', flexDirection: 'row' }}>

                <ToggleButton isEnabled={isEnabled} toggleSwitch={toggleSwitch} />
                {/* <ToggleButton isEnabled={isEnabled} toggleSwitch={toggleSwitch} /> */}

            </View>
        </View>
    );
};
const HomeScreen = (props) => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [email, setEmailToggle] = useState(true);
    const [sms, setSMSToggle] = useState(false);
    const [notification, setNotificationToggle] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);


    useEffect(() => {
        async function fetchDeta() {
            await props.getUserInfo();
            setEmailToggle(alertPreferences.email);
            setSMSToggle(alertPreferences.sms);
            setNotificationToggle(alertPreferences.notification);

        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    const toggleSwitch = () => setEmailToggle(previousState => !previousState);
    const toggleSwitch1 = () => setSMSToggle(previousState => !previousState);
    const toggleSwitch2 = () => setNotificationToggle(previousState => !previousState);
    const updateAlert = async values => {
        setLocalLoader(true);

        // console.log('handleSubmit values ' + JSON.stringify(values))
        const body = {
            email: email,
            sms: sms,
            notification: notification,
        };
        const result = await props.UpdateNotification(body);
        setLocalLoader(false);

        // console.log('res result' + JSON.stringify(result));
        if (result?.status === 200) {
            await props.getUserInfo();

            ToastShow(
                t('Alert permission Updated'),
                'succes',
                'long',
                'top',
            );

        } else if (result.status === 500) {
            setLocalLoader(false);
            const err = result?.data?.message;
            ToastShow(
                t(err),
                'error',
                'long',
                'top',
            );
        } else if (result?.status === 401) {
            setLocalLoader(false);
            const err = result?.data?.message;
            ToastShow(
                t(err),
                'error',
                'long',
                'top',
            );
        } else {
            // const err = result?.data?.data[0];
            // setErrorMessage(err?.msg);
            const err = result.data;
            ToastShow(
                t((err?.message)),
                'error',
                'long',
                'top',
            );
        }
    };
    const { userInfo = {} } = props;
    const { alertPreferences = {} } = userInfo;

    return (
        <View style={styles.container}>


            <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', fontSize: 16, marginTop: 15 }}>{t('Alerts')}</Text>

            <View style={{ alignItems: 'flex-end', paddingTop: 10, paddingVertical: 10, marginRight: 10 }}>
                <View style={{ width: '28%', justifyContent: 'space-between', flexDirection: 'row' }}>
                    <Text style={{ fontFamily: fonts.MEDIUM, color: '#000', fontSize: 14 }}>{t('Preference')}</Text>
                </View>


            </View>

            <AlertButton title={t('Email')} isEnabled={email} toggleSwitch={toggleSwitch} />
            <AlertButton title={t('Sms')} isEnabled={sms} toggleSwitch={toggleSwitch1} />
            <AlertButton title={t('Notification')} isEnabled={notification} toggleSwitch={toggleSwitch2} />
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>

                    <ActivityIndicator size="large" color="#0000ff" />

                </View>
            ) : null}
            <View style={{ alignItems: 'flex-end', paddingTop: 10, paddingVertical: 10 }}>
                <CustomButton
                    bgColor={'#208196'}
                    buttonName={t('Save')}
                    navigation={props.navigation}
                    onPressButton={() => updateAlert()}
                />
            </View>



        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: scale(15),
    },
});
function mapStateToProps(state) {
    return {

        userInfo: state.auth.userInfo,
        loder: state.loder,


    };
}
export default connect(mapStateToProps, {
    log_out,
    getUserInfo,
    UpdateNotification,
},
)(HomeScreen);
