/* eslint-disable prettier/prettier */


import React, { useState, useEffect } from 'react';
import {

    View,
    StyleSheet,
} from 'react-native';
import { connect } from 'react-redux';

import { useTranslation } from 'react-i18next';
import { log_out } from '../../../redux/action/auth';
import { useIsFocused } from '@react-navigation/native';
import { Info } from '../../../config/global';
import { SubHeaderComponent } from '../../../components/Common/helper';

const HomeScreen = () => {
    const { t, i18n } = useTranslation();
    useEffect(() => {
        async function fetchDeta() {

        }
        fetchDeta();
    }, []);


    return (
        <View style={styles.container}>


            <SubHeaderComponent
                name={t('Support Mail : ') + Info.supportMail}
                textColor={'#000'}
            />




        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        padding: 15
    },
});
function mapStateToProps(state) {
    return {



    };
}
export default connect(mapStateToProps, {
    log_out
},
)(HomeScreen);