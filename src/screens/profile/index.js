/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import * as React from 'react';
import { View } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { scale } from '../../components/Scale';
import { useTranslation } from 'react-i18next';
import { HeaderWithBack } from '../../components/Header';
import EditProfile from './edit_Profile';
import AlertSetting from './AlertSetting';
import Help from './help';
import fonts from '../../../FontFamily';
const Tab = createMaterialTopTabNavigator();

const ProfileStack = props => {
    const { t } = useTranslation();

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <HeaderWithBack navigation={props.navigation} name={t('My Profile')} />
            <View
                style={{
                    backgroundColor: '#fff',
                    borderTopLeftRadius: 10,
                    borderTopEndRadius: 10,
                    paddingBottom: 5,
                }}>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 12,
                    }}>
                    <View
                        style={{
                            borderRadius: 2,
                            width: scale(72),
                            height: scale(3),
                            backgroundColor: '#9B9B9B',
                        }}
                    />
                </View>
            </View>
            <View />
            <Tab.Navigator
                initialRoute={'editprofile'}
                screenOptions={{
                    tabBarLabelStyle: {
                        textTransform: 'capitalize',
                        fontFamily: fonts.MEDIUM,
                    },
                    tabBarInactiveTintColor: '#7F7F7F',
                    tabBarActiveTintColor: '#208196',
                    tabBarIndicatorStyle: { backgroundColor: '#208196' },
                    tabBarPressColor: '#208196',
                }}>
                <Tab.Screen
                    name="editprofile"
                    component={EditProfile}
                    options={{
                        tabBarLabel: t('Edit Profile'),
                    }}
                />

                <Tab.Screen
                    name="alertsetting"
                    component={AlertSetting}
                    options={{
                        tabBarLabel: t('Manage Alerts'),
                    }}
                />
                <Tab.Screen
                    name="help"
                    component={Help}
                    options={{
                        tabBarLabel: t('Help & Support'),
                    }}
                />
            </Tab.Navigator>
        </View>
    );
};
export default ProfileStack;
