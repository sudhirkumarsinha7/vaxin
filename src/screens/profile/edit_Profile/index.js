/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import { useTranslation } from 'react-i18next';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import {
    log_out,
    getUserInfo,
    getUserLocationList,
} from '../../../redux/action/auth';
import fonts from '../../../../FontFamily';
import { useIsFocused } from '@react-navigation/native';
import { LocationComponent } from '../../order/AddOrder/helper';
import { Image } from 'react-native';
import { localImage } from '../../../config/global';
import UserProfile from '../../../assets/img/UserProfile.svg'

const ProfileScreen = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [isEnabled, setIsEnabled] = useState(true);

    useEffect(() => {
        async function fetchDeta() {
            await props.getUserInfo();
            await props.getUserLocationList();
        }
        fetchDeta();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const { userInfo = {}, userLocationList = {} } = props;
    let {
        firstName = '',
        lastName = '',
        orgLevel = '',
        emailId = '',
    } = userInfo;
    const { locations = [] } = userLocationList;

    return (
        <View style={styles.container}>
            <View
                style={{
                    flexDirection: 'row',
                    borderTopWidth: 2,
                    borderBottomWidth: 2,
                    borderColor: '#F1F1F1',
                    paddingVertical: 15,
                    alignItems: 'center',
                }}>
                <View style={{ flex: 0.5 }}>
                    <View
                        style={{
                            height: 80,
                            width: 80,
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: '#F1F1F1',
                            borderRadius: 100,
                        }}>
                        {/* <FontAwesome name="user" color={'#000'} size={65} /> */}
                        <UserProfile height={40} width={40} />
                    </View>
                </View>
                <View style={{ flex: 0.5 }}>
                    <Text style={{ fontFamily: fonts.BOLD, color: '#000', fontSize: 15 }}>
                        {firstName + '  ' + lastName}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            color: '#000',
                            fontSize: 15,
                            marginTop: 10,
                        }}>
                        {orgLevel}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            color: '#000',
                            fontSize: 15,
                            marginTop: 10,
                        }}>
                        {emailId}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            color: '#000',
                            fontSize: 15,
                            marginTop: 10,
                        }}>
                        {t('Role : ') + userInfo?.roleId?.name}
                    </Text>
                </View>
                {/* <View style={{ flex: 0.3, alignItems: 'center', }}>
                    <ToggleButton isEnabled={isEnabled} toggleSwitch={toggleSwitch} />
                    <Text style={{ fontFamily: fonts.REGULAR, color: '#000', fontSize: 12, marginTop: 10 }}>{'Switch to Inactive'}</Text>

                </View> */}
            </View>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    color: '#000',
                    fontSize: 15,
                    marginTop: 10,
                }}>
                {t('MY Locations')}
            </Text>

            <FlatList
                contentContainerStyle={{ paddingBottom: 100 }}
                data={locations}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => (
                    <LocationComponent userLocation={item} />
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15,
        backgroundColor: '#fff',
    },
});
function mapStateToProps(state) {
    return {
        userInfo: state.auth.userInfo,
        userLocation: state.auth.userLocation,
        userLocationList: state.auth.userLocationList,
    };
}
export default connect(mapStateToProps, {
    log_out,
    getUserInfo,
    getUserLocationList,
})(ProfileScreen);
