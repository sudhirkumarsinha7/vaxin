/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    ScrollView,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';

import { Colors, DeviceWidth } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import fonts from '../../../FontFamily';
import {
    calculateAge,
    formatDateDDMMYYYY,
    formatDateMMYYYY,
    formatNumber,
    timeAgo,
} from '../../Util/utils';
import { TEST_SERVER_URL } from '../../config/config';
import { CustomButton } from '../../components/Common/helper';
import { ShipmentStatus } from '../shipment/helpler';

export function removeNullKeys(obj) {
    const cleanedObj = {};

    for (const key in obj) {
        if (obj[key] !== null && obj[key] !== undefined) {
            cleanedObj[key] = obj[key];
        }
    }

    return cleanedObj;
}

export const UnitUtilisedComponent = props => {
    const { t, i18n } = useTranslation();

    const onPressButton = () => {
        props.navigation.navigate('AddIBeneficiary', { details: item });
    };
    const { item = {} } = props;
    const {
        productDetails = {},
        quantityUtilized = 0,
        currentCampaignDetails = {},
    } = item;

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.5 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date') + ' - ' + formatDateDDMMYYYY(item.createdAt)}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Last updated') + ' - ' + formatDateDDMMYYYY(item.updatedAt)}
                    </Text>
                </View>
                <View style={{ flex: 0.5, alignItems: 'flex-end' }}>
                    <ShipmentStatus status={item.status} />
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.6 }}>
                        {currentCampaignDetails && currentCampaignDetails.name && (
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 16,
                                    color: Colors.blueD2,
                                    marginBottom: 5,
                                }}>
                                {t('Campaign : ') +
                                    currentCampaignDetails.id +
                                    ' - ' +
                                    currentCampaignDetails.name}
                            </Text>
                        )}
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {productDetails.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginTop: 10,
                            }}>
                            {t('Batch No') + ' - ' + item.batchNo}
                        </Text>
                    </View>
                    <View style={{ flex: 0.4 }}>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {t('Doses Utilized')}
                        </Text>
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}>
                            <Text
                                style={{
                                    fontFamily: fonts.BOLD,
                                    fontSize: 16,
                                    color: Colors.blackTextColor,
                                    marginTop: 10,
                                }}>
                                {quantityUtilized + ''}
                            </Text>
                            {item.status === 'OPEN' && (
                                <TouchableOpacity
                                    onPress={() => onPressButton()}
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        margin: 10,
                                        backgroundColor: '#208196',
                                        padding: 5,
                                        paddingHorizontal: 10,
                                        borderRadius: 12,
                                    }}>
                                    <Text
                                        style={{
                                            color: '#fff',
                                            fontFamily: fonts.MEDIUM,
                                            fontSize: 16,
                                        }}>
                                        {'+ ' + t('Add')}
                                    </Text>
                                </TouchableOpacity>
                            )}
                        </View>
                    </View>
                </View>
            </View>
        </View>
    );
};
export const BeneficiariesVaccinatedComponent = props => {
    const { t, i18n } = useTranslation();

    const { item = {} } = props;
    const { productDetails = {}, gender = 'M' } = item;
    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: 15,
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.5 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date') + ' - ' + formatDateDDMMYYYY(item.createdAt)}
                    </Text>
                </View>
                <View style={{ flex: 0.5 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Last updated') + ' - ' + timeAgo(item.updatedAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.5 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {productDetails.name}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginTop: 10,
                            }}>
                            {t('Dose No.') + ' - ' + item.doseNo}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: '#B2389F',
                                marginTop: 10,
                            }}>
                            {t('DOB') + ' - ' + formatDateDDMMYYYY(item.dob)}
                        </Text>
                    </View>
                    <View style={{ flex: 0.5 }}>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                                color: Colors.grayTextColor,
                            }}>
                            {t('Beneficiary') + ' - ' + item.name}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: '#B2389F',
                                marginTop: 10,
                            }}>
                            {t('Age') + ' - ' + calculateAge(item.dob) + ' / ' + gender}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 13,
                                color: Colors.grayTextColor,
                                marginTop: 10,
                            }}>
                            {t('Parent') + ' - ' + item.parentName}
                        </Text>
                    </View>
                </View>
            </View>
        </View>
    );
};
