/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import {
    Colors,
} from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
    CustomButtonWithBorder,
} from '../../../components/Common/helper';

import { useIsFocused } from '@react-navigation/native';
import fonts from '../../../../FontFamily';
import { ToastShow } from '../../../components/Toast';
import {
    getInventoryLocationList,
    getInStockList,
    getProdListByBatch,
} from '../../../redux/action/inventory';
import { filterUniqueObjectsByProperty } from '../../inventory/AddInventory/productUtil';
import {
    AddNewBeneficiaryComponent,
    VirtualizedList,
    BeneficiaryHeader,
    BeneficiaryListComponent,
    BatchComponent,
    OpenBatchComponent,
} from './helper';
import DropDown from '../../../components/Dropdown';
import { filteredExpiredBatchArray } from '../../shipment/AddShipment/selectBatch';
import Empty_Card from '../../../components/Empty_Card';
import { BatchConfirmationPopUp } from '../../../components/popUp';
import { add_benificiary } from '../../../redux/action/beneficiary';
import { useRoute } from '@react-navigation/native';

export function FilterVaccineList(data) {
    return data
        .filter(
            item =>
                item?.product?.type === 'Vaccine' || item?.product?.type === 'VACCINE',
        )
        .map(item => ({ _id: item?.product?._id, name: item?.product?.name }));
}
const AddBeneficiary = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [isBatchList, setIsBatchList] = useState(true);
    const [errorMessage, setErrorMessage] = useState('');
    const [selectedVaccine, setSelectedBatch] = useState({});
    const [batchList, setBatchList] = useState([]);
    const [selectedVaccineId, setSelectedVaccine] = useState('');
    const [isVisibleModal, setIsVisibleModal] = useState(false);
    const [localLoader, setLocalLoader] = useState(false);
    const [showDetails, setShowDetails] = useState(false);

    const [VaccineList, setVaccineList] = useState([]);
    const [currentIndx, setCurrentIndx] = useState(null);
    const [editData, setEditData] = useState({});
    const [beneficiaryList, setBeneficiaryList] = useState([]);

    const route = useRoute();
    const { params } = route;
    const details = params.details || {};
    useEffect(() => {
        async function fetchData() {
            GetData();
            if (details?._id) {
                setSelectedBatch(details);
            }
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        setLocalLoader(true);
        const response = await props.getInStockList();
        setLocalLoader(false);
        console.log('response ', response.data);
        if (
            response &&
            response.data &&
            response.data.data &&
            response.data.data.data
        ) {
            const result = FilterVaccineList(response.data.data.data);

            setVaccineList(result);
        } else {
            setVaccineList([]);
        }
    };

    const handleSelectBatch = batch => {
        if (selectedVaccine && selectedVaccine._id === batch._id) {
            // Unselect the card if it's already selected
            setSelectedBatch({});
            setIsBatchList(true);
        } else {
            // Select the card
            setSelectedBatch(batch);
            setIsBatchList(false);
        }
    };

    const _add = () => {
        const maxVaxin = details?.quantityUtilized
            ? selectedVaccine?.quantityPerUnit - details?.quantityUtilized
            : selectedVaccine?.quantityPerUnit;

        if (beneficiaryList.length < maxVaxin) {
            const remaingBen = Number(maxVaxin) - Number(beneficiaryList.length);
            const error = remaingBen + ' Beneficiaries yet to be added to this vial';
            setErrorMessage(error);
            setIsVisibleModal(true);
        } else {
            _addbeneficiary_API();
        }
    };

    const _addbeneficiary_API = async () => {
        setLocalLoader(true);

        const modifiedData = beneficiaryList.map(item => {
            const { countryCode, mob, ...rest } = item;
            return rest;
        });
        const body = {
            atomId: selectedVaccine._id,
            beneficiaries: modifiedData,
        };
        // console.log('_addbeneficiary_API res body' + JSON.stringify(body));

        const result = await props.add_benificiary(body);
        setLocalLoader(false);

        // console.log('_addbeneficiary_API res result' + JSON.stringify(result));
        if (result?.status === 200) {
            ToastShow(t('Beneficiary added suceesfully'), 'success', 'long', 'top');
            props.navigation.goBack();
        } else if (result.status === 500) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            setErrorMessage(err);
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            setErrorMessage(err?.message);
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };
    const addBeneficiaryInList = async data => {
        setLocalLoader(true);

        const maxVaxin = details?.quantityUtilized
            ? selectedVaccine?.quantityPerUnit - details?.quantityUtilized
            : selectedVaccine?.quantityPerUnit;
        // console.log('maxVaxin  ' + JSON.stringify(maxVaxin))
        // console.log('beneficiaryList.length  ' + JSON.stringify(beneficiaryList.length))

        if (beneficiaryList.length < Number(maxVaxin) || editData.name) {
            let extistData = beneficiaryList;

            if (currentIndx || currentIndx === 0) {
                extistData[currentIndx] = data;
                await setBeneficiaryList(extistData);
                setCurrentIndx(null);
                setEditData({});
                setLocalLoader(false);
            } else {
                let addedNewdata = [...extistData, data];
                await setBeneficiaryList(addedNewdata);
                setLocalLoader(false);
            }

            ToastShow(data.name + t(' Added succesfully'), 'success', 'long', 'top');
        } else {
            setLocalLoader(false);
            ToastShow(t('Maximum Beneficiary Added'), 'error', 'long', 'top');
        }
    };

    const deleteBeneficiary = async (item, index) => {
        setLocalLoader(true);
        let extistData = beneficiaryList;

        extistData.splice(index, 1);
        // console.log('index  ' + JSON.stringify(index))
        // console.log('extistData  ' + JSON.stringify(extistData))
        await setBeneficiaryList(extistData);
        setLocalLoader(false);
    };
    const editBeneficiary = (item, index) => {
        setCurrentIndx(index);
        setEditData(item);
        setShowDetails(false);
    };
    const changeAfterSelectProduct = async item => {
        const { value } = item;
        setSelectedVaccine(value);
        const response = await props.getProdListByBatch(value);
        // console.log('changeAfterSelectProduct ', response)
        if (
            response &&
            response.data &&
            response.data.data &&
            response.data.data.data
        ) {
            let getBacheslist =
                (response.data && response.data.data && response.data.data.data) || [];
            // const uniqueArray = filterUniqueObjectsByProperty(getBacheslist, 'batchNo');
            // const uniqueArray = filteredExpiredBatchArray(getBacheslist);
            const uniqueArray = getBacheslist;

            setBatchList(uniqueArray);
            setIsBatchList(true);
            setSelectedBatch({});
        } else {
            setBatchList([]);
        }
    };
    const AddDetails = () => {
        return (
            <View>
                <View>
                    <View
                        style={{
                            backgroundColor: '#fff',
                            padding: 15,
                        }}>
                        <TouchableOpacity
                            onPress={() => setShowDetails(false)}
                            style={{ flexDirection: 'row' }}>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    margin: 5,
                                    fontSize: 20,
                                    color: '#1874C9',
                                }}>
                                {t('Beneficiary Details')}
                            </Text>
                            <AntDesign
                                name="down"
                                size={15}
                                color={'#000'}
                                style={{ justifyContent: 'center', marginTop: 5 }}
                            />
                        </TouchableOpacity>
                    </View>
                    <BeneficiaryHeader />
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={beneficiaryList}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item, index }) => (
                            <BeneficiaryListComponent
                                item={item}
                                index={index}
                                deleteBeneficiary={deleteBeneficiary}
                                editBeneficiary={editBeneficiary}
                            />
                        )}
                    />

                    {beneficiaryList.length > 0 ? (
                        <View
                            style={{
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                alignItems: 'center',
                            }}>
                            <Text />

                            <CustomButtonWithBorder
                                bdColor={'#208196'}
                                textColor={'#208196'}
                                buttonName={t('Submit')}
                                navigation={props.navigation}
                                onPressButton={_add}
                            />
                        </View>
                    ) : null}
                </View>
            </View>
        );
    };
    const showBenficiaryDetails = () => {
        return (
            <View>
                <View style={{ marginHorizontal: 10, marginTop: 15 }}>
                    <Text
                        style={{ fontFamily: fonts.MEDIUM, fontSize: 12, color: '#1874C9' }}>
                        {t('Vaccine Details')}
                    </Text>
                    {details && details?.batchNo ? (
                        <OpenBatchComponent
                            item={details}
                            navigation={props.navigation}
                            selected={true}
                        />
                    ) : (
                        <View>
                            <DropDown
                                dropdownData={VaccineList}
                                // dropdownData={VaccineListNew}
                                onChangeValue={changeAfterSelectProduct}
                                label="name"
                                mapKey="_id"
                                val={selectedVaccineId}
                                placeholderText={t('Vaccine')}
                                labelText={t('Vaccine Name')}
                            />

                            {isBatchList ? (
                                <FlatList
                                    contentContainerStyle={{ paddingBottom: 100 }}
                                    data={batchList}
                                    renderItem={({ item }) => (
                                        <BatchComponent
                                            item={item}
                                            navigation={props.navigation}
                                            selected={
                                                selectedVaccine && selectedVaccine._id === item._id
                                            }
                                            onSelect={handleSelectBatch}
                                        />
                                    )}
                                    keyExtractor={(item, index) => index.toString()}
                                    ListEmptyComponent={
                                        selectedVaccineId ? (
                                            <Empty_Card Text="No Batch available" />
                                        ) : null
                                    }
                                />
                            ) : (
                                <BatchComponent
                                    item={selectedVaccine}
                                    navigation={props.navigation}
                                    selected={true}
                                    onSelect={() => console.log('')}
                                />
                            )}
                        </View>
                    )}
                </View>

                {selectedVaccine._id && (
                    <View>
                        {beneficiaryList.length > 0 && !showDetails && (
                            <View
                                style={{
                                    backgroundColor: '#fff',
                                    padding: 15,
                                }}>
                                <TouchableOpacity
                                    onPress={() => setShowDetails(true)}
                                    style={{ flexDirection: 'row' }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.MEDIUM,
                                            margin: 5,
                                            fontSize: 20,
                                            color: '#1874C9',
                                        }}>
                                        {t('Beneficiary Details')}
                                    </Text>
                                    <AntDesign
                                        name="up"
                                        size={15}
                                        color={'#000'}
                                        style={{ justifyContent: 'center', marginTop: 5 }}
                                    />
                                </TouchableOpacity>
                            </View>
                        )}
                        {details?.quantityUtilized ? (
                            <View>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        margin: 3,
                                        fontSize: 12,
                                        color: Colors.gray9,
                                    }}>
                                    {'*' +
                                        details?.quantityUtilized +
                                        '/' +
                                        selectedVaccine?.quantityPerUnit +
                                        ' Beneficiaries added for the current vial in Last update'}
                                </Text>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        margin: 3,
                                        fontSize: 12,
                                        color: Colors.gray9,
                                    }}>
                                    {'*' +
                                        beneficiaryList.length +
                                        '/' +
                                        (selectedVaccine?.quantityPerUnit -
                                            details?.quantityUtilized) +
                                        ' Beneficiaries can be added for the open vial'}
                                </Text>
                            </View>
                        ) : (
                            <View>
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        margin: 3,
                                        fontSize: 12,
                                        color: Colors.gray9,
                                    }}>
                                    {'*' +
                                        beneficiaryList.length +
                                        '/' +
                                        selectedVaccine?.quantityPerUnit +
                                        ' Beneficiaries added for the current vial'}
                                </Text>
                            </View>
                        )}

                        <AddNewBeneficiaryComponent
                            addBeneficiaryInList={addBeneficiaryInList}
                            form={editData}
                        />
                    </View>
                )}
            </View>
        );
    };

    const { inv_product_list = [] } = props;
    const uniqueProduct = filterUniqueObjectsByProperty(inv_product_list, 'name');

    // console.log('beneficiaryList  ' + JSON.stringify(beneficiaryList))
    // console.log('details  ' + JSON.stringify(details))

    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Add Beneficiary')}
            />
            <BatchConfirmationPopUp
                title={t('Vial Wastage')}
                message={errorMessage}
                isVisible={isVisibleModal}
                onClose={() => setIsVisibleModal(false)}
                confirm={() => {
                    setIsVisibleModal(false);
                    _addbeneficiary_API();
                }}
            />

            <VirtualizedList>
                <View style={{ marginHorizontal: 10 }}>
                    {props.loder || localLoader ? (
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                flex: 1,
                            }}>
                            <ActivityIndicator size="large" color="#0000ff" />
                        </View>
                    ) : null}
                    {showDetails ? AddDetails() : showBenficiaryDetails()}
                </View>
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        inv_product_list: state.inventory.inv_product_list,
        inv_list: state.inventory.inv_list,
        loder: state.loder,
    };
}
export default connect(mapStateToProps, {
    getInStockList,
    getInventoryLocationList,
    getProdListByBatch,
    add_benificiary,
})(AddBeneficiary);
