/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect, createRef } from 'react';
import {
    Keyboard,
    View,
    Text,
    TouchableOpacity,
    FlatList,
    ScrollView,
    StyleSheet,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../../components/Scale';
import {
    Colors,
    DeviceWidth,
    DeviceHeight,
} from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Formik, useFormik } from 'formik';
import * as Yup from 'yup';
import { localImage } from '../../../config/global';
import DropDown, { ManufacturerDropdown } from '../../../components/Dropdown';
import {
    InputField,
    CustomInputText,
    CustomButton,
    CustomButtonLine,
    SubHeaderComponent,
    CustomTextLine,
    CustomButtonWithBorder,
    CustomTextView,
} from '../../../components/Common/helper';
import DateInput, { DatePickerComponent } from '../../../components/DatePicker';
import fonts from '../../../../FontFamily';
const defaultQty = [50, 100, 200, 500, 1000];
import {
    calculateAge,
    formatDateDDMMYYYY,
    formatNumber,
} from '../../../Util/utils';
import { ToastShow } from '../../../components/Toast';
import { PhoneComponentNew } from '../../../components/Common/PhoneNumber';
import {
    PhoneNumberInput,
    PhoneNumberUtil,
    isValidNumber,
} from 'react-native-phone-number-input';

const GenderList = [
    { id: 'M', name: 'Male' },
    { id: 'F', name: 'Female' },
    { id: 'O', name: 'Other' },
];
export const VirtualizedList = ({ children }) => {
    return (
        <FlatList
            data={[]}
            keyExtractor={() => 'key'}
            renderItem={null}
            ListHeaderComponent={<>{children}</>}

        />
    );
};


export const AddNewBeneficiaryComponent = props => {
    const { t, i18n } = useTranslation();
    const [isMandatory, setIsMandatory] = useState(false);
    const [isPhone, setIsPhone] = useState(true);

    const { addBeneficiaryInList, form = {} } = props;
    useEffect(() => {
        async function fetchData() {
            if (form.name) {
                setIsPhone(false);

                await beneficiaryForm.setValues(form);
                beneficiaryForm.handleChange({ target: { name: 'mob', value: form.mob } });
                beneficiaryForm.handleChange({
                    target: { name: 'phoneNumber', value: form.phoneNumber },
                });
                setIsPhone(true);
            }
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [form]);

    var validationSchema = Yup.object().shape(
        {
            doseNo: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            beneficiaryId: Yup.string()
                .required(t('Required'))
                .trim()
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            name: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            dob: Yup.string().required(t('Required')),
            parentName: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),

            phoneNumber: Yup.string()
                .test(
                    'is-valid-phone-number',
                    'Invalid phone number',
                    function (value) {
                        if (!value) {
                            return true; // Allow empty field if not required
                        }
                        return isValidNumber(value, beneficiaryForm?.values?.countryCode);
                    },
                )
                .required(t('Required')),
        },
        ['doseNo', 'name', 'gender'],
    ); // <-- HERE!!!!!!!!
    const beneficiaryForm = useFormik({
        initialValues: {
            doseNo: '',
            name: '',
            dob: '',
            gender: '',
            parentName: '',
            phoneNumber: '',
            countryCode: 'BD',
            mob: '',
            beneficiaryId: ''
        },
        validationSchema,
        onSubmit: (values, actions) => {
            handleSubmit({ ...values });
        },
    });
    const _addbeneficiary = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        beneficiaryForm.handleSubmit();
    };

    const handleSubmit = async values => {
        // console.log('handleSubmit values', values)
        setIsMandatory(false);
        setIsPhone(false);
        await addBeneficiaryInList(values);
        await beneficiaryForm.resetForm();
        setIsPhone(true);
    };

    const changeAfterSelectGender = val => {
        beneficiaryForm.handleChange({ target: { name: 'gender', value: val.value } });
    };
    const onChangeDob = val => {
        beneficiaryForm.handleChange({ target: { name: 'dob', value: val } });
    };
    // console.log('beneficiaryForm  ' + JSON.stringify(beneficiaryForm))

    return (
        <View>
            <View
                style={{ backgroundColor: '#fff', marginHorizontal: 15, marginTop: 10 }}>
                <InputField
                    placeholder={t("Enter here")}
                    label={t("Dose Number")}
                    inputValue={beneficiaryForm?.values?.doseNo}
                    setInputValue={text => {
                        beneficiaryForm.handleChange({
                            target: { name: 'doseNo', value: text },
                        });
                    }}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={beneficiaryForm?.errors?.doseNo}
                    mandatory={isMandatory}
                    KeyboardType={'numeric'}
                />
                <InputField
                    placeholder={t("Enter here")}
                    label={t("Beneficiary Id")}
                    inputValue={beneficiaryForm?.values?.beneficiaryId}
                    setInputValue={text => {
                        beneficiaryForm.handleChange({
                            target: { name: 'beneficiaryId', value: text },
                        });
                    }}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={beneficiaryForm?.errors?.beneficiaryId}
                    mandatory={isMandatory}
                />

                <InputField
                    placeholder={t("Enter here")}
                    label={t("Beneficiary Name")}
                    inputValue={beneficiaryForm?.values?.name}
                    setInputValue={text => {
                        beneficiaryForm.handleChange({ target: { name: 'name', value: text } });
                    }}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={beneficiaryForm?.errors?.name}
                    mandatory={isMandatory}
                />

                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <View style={{ width: '48%' }}>
                        <DatePickerComponent
                            label={t('D.O.B')}
                            placeholder="DD/MM/YYYY"
                            onChange={val => onChangeDob(val)}
                            val={beneficiaryForm?.values?.dob}
                            errorMsg={beneficiaryForm?.errors?.dob}
                            mandatory={isMandatory}
                            isMfgDate={true}
                            isMaxDate={true}
                        />
                    </View>
                    <View style={{ width: '48%' }}>
                        <DropDown
                            dropdownData={GenderList}
                            onChangeValue={changeAfterSelectGender}
                            label="name"
                            mapKey="id"
                            val={beneficiaryForm?.values?.gender}
                            placeholder={t("Enter here")}
                            errorMsg={beneficiaryForm?.errors?.gender}
                            mandatory={isMandatory}
                            labelText={t("Gender")}
                            search={false}
                        />
                    </View>
                </View>
                {beneficiaryForm?.values?.dob && (
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            marginVertical: 5,
                            fontSize: 12,
                            color: Colors.grey4B,
                        }}>
                        {'Age: ' + calculateAge(beneficiaryForm?.values?.dob)}
                    </Text>
                )}
                <InputField
                    placeholder={t("Enter here")}
                    label={t("Parent’s Name")}
                    // isMandatoryField={false}
                    inputValue={beneficiaryForm?.values?.parentName}
                    setInputValue={beneficiaryForm.handleChange('parentName')}
                    inputStyle={{ marginBottom: 10 }}
                    labelStyle={{ marginBottom: 5 }}
                    errorMsg={beneficiaryForm?.errors?.parentName}
                    mandatory={isMandatory}
                />

                {isPhone && (
                    <PhoneComponentNew
                        label={t('Phone number')}
                        val={beneficiaryForm?.values?.phoneNumber}
                        val1={beneficiaryForm?.values?.mob}
                        onChange={text => {
                            beneficiaryForm.handleChange({
                                target: { name: 'mob', value: text },
                            });
                        }}
                        onChangeFormattedText={text => {
                            beneficiaryForm.handleChange({
                                target: { name: 'phoneNumber', value: text },
                            });
                        }}
                        setCountryCode={text => {
                            beneficiaryForm.handleChange({
                                target: { name: 'countryCode', value: text },
                            });
                        }}
                        countryCode={beneficiaryForm?.values?.countryCode}
                        errorMsg={beneficiaryForm?.errors?.phoneNumber}
                        mandatory={isMandatory}
                        KeyboardType={'numeric'}
                        isMandatoryField={true}
                    />
                )}
            </View>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                <Text />
                {/* <CustomButton
                bgColor={'#208196'}
                buttonName={'Add Beneficiary'}
                navigation={props.navigation}
                onPressButton={_addbeneficiary}
            /> */}
                <CustomButtonWithBorder
                    bdColor={'#208196'}
                    textColor={'#208196'}
                    buttonName={form.name ? t('Update Beneficiary') : t('Add Beneficiary')}
                    navigation={props.navigation}
                    onPressButton={_addbeneficiary}
                />
            </View>
        </View>
    );
};




export const BeneficiaryHeader = props => {
    const { t, i18n } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.02, justifyContent: 'center', padding: 2 }}>
                {/* <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{'Vaccine'}</Text> */}
            </View>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Beneficiary Name')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('DOB & Gender')}
                </Text>
            </View>
            <View style={{ flex: 0.45, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Parent’s Name')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Phone no ')}
                </Text>
            </View>
            <View style={{ flex: 0.1, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Dose No.')}</Text>
            </View>
        </View>
    );
};

export const BeneficiaryListComponent = props => {
    const { t, i18n } = useTranslation();

    const { deleteBeneficiary, editBeneficiary, item = {}, index = 0 } = props;

    return (
        <View
            style={{
                flexDirection: 'row',
                padding: 7,
                borderBottomWidth: 1,
                borderColor: Colors.grayC5,
                alignItems: 'center',
            }}>
            <TouchableOpacity
                style={{ flex: 0.05, marginRight: 10 }}
                onPress={() => {
                    deleteBeneficiary(item, index);
                }}>
                <MaterialCommunityIcons
                    name={'delete-outline'}
                    color={'#41416E'}
                    size={20}
                />
            </TouchableOpacity>
            {/* <View style={{ flex: 0.3, marginRight: 10 }}>
            <Text style={{ fontFamily: fonts.MEDIUM, marginBottom: 3, fontSize: 12 }}>{item.type || 'Penta'}</Text>
        </View> */}
            <View style={{ flex: 0.4, marginRight: 10 }}>
                <Text style={{ fontFamily: fonts.MEDIUM, marginBottom: 3, fontSize: 12 }}>
                    {item.name}
                </Text>
                <Text style={{ fontFamily: fonts.REGULAR, fontSize: 11 }}>
                    {formatDateDDMMYYYY(item.dob)}
                </Text>

                <Text style={{ fontFamily: fonts.REGULAR, fontSize: 11 }}>
                    {calculateAge(item.dob) + ' / ' + item.gender}
                </Text>
            </View>
            <View style={{ flex: 0.45, marginRight: 10 }}>
                <Text style={{ fontFamily: fonts.MEDIUM, marginBottom: 3, fontSize: 12 }}>
                    {item.parentName}
                </Text>

                <Text style={{ fontFamily: fonts.REGULAR, fontSize: 11 }}>
                    {item.mob}
                </Text>
            </View>
            <View style={{ flex: 0.15 }}>
                <Text
                    style={{ fontFamily: fonts.REGULAR, fontSize: 11, marginBottom: 10 }}>
                    {item.doseNo}
                </Text>

                <TouchableOpacity
                    onPress={() => {
                        editBeneficiary(item, index);
                    }}>
                    <MaterialCommunityIcons
                        name={'square-edit-outline'}
                        color={'#7F7F7F'}
                        size={20}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
};

export const BatchComponent = props => {
    const { t, i18n } = useTranslation();

    const { item = {}, selected, onSelect } = props;
    const { currentCampaignDetails = {}, vvmStatus = '' } = item;
    const vvmstatus = vvmStatus === 1 || vvmStatus === 2 ? t('Usable')
        : t('non usable')
    return (
        <TouchableOpacity
            onPress={() => onSelect(item)}
            style={[styles.card, selected && styles.selectedCard]}>
            {currentCampaignDetails && currentCampaignDetails.name && (
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 16,
                        color: Colors.blueD2,
                        marginBottom: 5,
                    }}>
                    {t('Campaign : ') +
                        currentCampaignDetails.id +
                        ' - ' +
                        currentCampaignDetails.name}
                </Text>
            )}

            <View style={{ flexDirection: 'row' }}>
                <View style={{ flex: 0.48 }}>
                    <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 13 }}>
                        {t('Batch No: ') + item.batchNo}
                    </Text>

                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            color: Colors.blackTextColor,
                            marginTop: 12,
                        }}>
                        {t('Qty: ') + formatNumber(item.quantity)}
                    </Text>
                    {item.serialNo && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 13,
                                marginTop: 12,
                            }}>
                            {t('SerialNo.: ') + item.serialNo}
                        </Text>
                    )}
                </View>
                <View style={{ flex: 0.48 }}>
                    <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 13 }}>
                        {t('Mfg Date ') + formatDateDDMMYYYY(item.mfgDate)}
                    </Text>

                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            marginTop: 12,
                        }}>
                        {' '}
                        {t('Exp Date ') + formatDateDDMMYYYY(item.expDate)}
                    </Text>
                    {vvmStatus && <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            marginTop: 2,
                            color: vvmStatus === 1 || vvmStatus === 2 ? 'green' : 'red'
                        }}>
                        {t('Vaccine vial monitor') + ' : ' + vvmstatus + ' (' + vvmStatus + ') '}
                    </Text>}
                </View>
            </View>
        </TouchableOpacity>
    );
};
export const OpenBatchComponent = props => {
    const { t, i18n } = useTranslation();

    const { item = {}, selected } = props;
    const { productDetails = {}, currentCampaignDetails = {}, vvmStatus = '' } = item;
    const vvmstatus = vvmStatus === 1 || vvmStatus === 2 ? t('Usable')
        : t('non usable')
    return (
        <View style={[styles.card, selected && styles.selectedCard]}>
            <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 13 }}>
                {t('Vaccine: ') + productDetails.name}
            </Text>
            {currentCampaignDetails && currentCampaignDetails.name && (
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 16,
                        color: Colors.blueD2,
                        marginTop: 7,
                    }}>
                    {t('Campaign : ') +
                        currentCampaignDetails.id +
                        ' - ' +
                        currentCampaignDetails.name}
                </Text>
            )}
            <View style={{ flexDirection: 'row', marginTop: 12 }}>
                <View style={{ flex: 0.48 }}>
                    <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 13 }}>
                        {t('Batch No: ') + item.batchNo}
                    </Text>

                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            color: Colors.blackTextColor,
                            marginTop: 12,
                        }}>
                        {t('Qty: ') + formatNumber(item.quantity)}
                    </Text>
                    {item.serialNo && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 13,
                                marginTop: 12,
                            }}>
                            {t('SerialNo.: ') + item.serialNo}
                        </Text>
                    )}
                </View>
                <View style={{ flex: 0.48 }}>
                    <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 13 }}>
                        {t('Mfg Date ') + formatDateDDMMYYYY(item.mfgDate)}
                    </Text>

                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            marginTop: 12,
                        }}>
                        {' '}
                        {t('Exp Date ') + formatDateDDMMYYYY(item.expDate)}
                    </Text>
                    {vvmStatus && <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 13,
                            marginTop: 2,
                            color: vvmStatus === 1 || vvmStatus === 2 ? 'green' : 'red'
                        }}>
                        {t('Vaccine vial monitor') + ' : ' + vvmstatus + ' (' + vvmStatus + ') '}
                    </Text>}
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    card: {
        padding: 10,
        marginVertical: 5,
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: 'gray',
    },
    selectedCard: {
        backgroundColor: Colors.blueDE,
    },
    cardText: {
        fontSize: 16,
        fontWeight: 'bold',
    },
});
