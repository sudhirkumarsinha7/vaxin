/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
    ScrollView
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import {
    UnitUtilisedComponent,
    BeneficiariesVaccinatedComponent,
} from '../helpler';
import fonts from '../../../../FontFamily';

const listBeneficiaryTab = [
    {
        name: 'No. of Units Utilised',
        value: 'No. of Units Utilised',
        color: Colors.lightGrayBlue1,
        key: 'totalQuantityUtilized',
    },
    {
        name: 'Beneficiaries Vaccinated so far',
        value: 'Beneficiaries Vaccinated so far',
        color: Colors.green8b,
        key: 'totalBeneficiaries',
    },
    {
        name: 'Beneficiaries Vaccinated Today',
        value: 'Beneficiaries Vaccinated Today',
        color: Colors.yellow9A,
        key: 'todayBeneficiaries',
    },
];

import Empty_Card from '../../../components/Empty_Card';
import { formatNumber } from '../../../Util/utils';

const BeneficiaryList = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [activeInTab, setInTab] = useState('No. of Units Utilised');
    const [localLoader, setLocalLoader] = useState(false);

    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
    }, [isFocused]);
    const GetData = async () => {
        setInTab('No. of Units Utilised');
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    flexDirection: 'row',
                    padding: 12,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <Text style={{ fontFamily: fonts.MEDIUM }}>{t(item.value)}</Text>
                <View
                    style={{
                        backgroundColor: '#D4E6ED',
                        paddingLeft: 5,
                        paddingRight: 5,
                        borderRadius: 10,
                        marginLeft: 5,
                        marginTop: -5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            color: '#03557C',
                            padding: 5,
                            textAlign: 'center',
                        }}>
                        {formatNumber(vaccinated_analytics[item.key]) || 0}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const onChangeTab = async item => {
        setLocalLoader(true);
        await props.GetData();
        setInTab(item.name);
        setLocalLoader(false);
    };
    const {
        unit_utilized = [],
        vaccinated_List = [],
        vaccinated_today_List = [],
        vaccinated_analytics = {},
        lastmile_analytcs = {},
    } = props;
    // console.log('lastmile_analytcs ' + JSON.stringify(lastmile_analytcs))
    return (
        <SafeAreaView style={styles.container}>
            {/* <FilterCardButtons onPressButton={() => props.navigation.navigate('order', {
                screen: 'Filter',

            })} /> */}
            <View
                style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                }}>
                {listBeneficiaryTab.map(each => eachTab(each))}
            </View>
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : null}
            {/* <Text>{JSON.stringify(vaccinated_analytics)}</Text> */}
            {activeInTab === 'No. of Units Utilised' ? (
                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={unit_utilized}
                    renderItem={({ item }) => (
                        <UnitUtilisedComponent item={item} navigation={props.navigation} />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    ListEmptyComponent={<Empty_Card Text="No items available" />}
                />
            ) : null}
            {activeInTab === 'Beneficiaries Vaccinated so far' ? (
                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={vaccinated_List}
                    renderItem={({ item }) => (
                        <BeneficiariesVaccinatedComponent
                            item={item}
                            navigation={props.navigation}
                        />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    ListEmptyComponent={<Empty_Card Text="No items available" />}
                />
            ) : null}
            {activeInTab === 'Beneficiaries Vaccinated Today' ? (
                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={vaccinated_today_List}
                    renderItem={({ item }) => (
                        <BeneficiariesVaccinatedComponent
                            item={item}
                            navigation={props.navigation}
                        />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    ListEmptyComponent={<Empty_Card Text="No items available" />}
                />
            ) : null}
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteFF,
        // borderRadius: scale(15),
        paddingTop: 10,
    },
});

function mapStateToProps(state) {
    return {
        loder: state.loder,
        unit_utilized: state.beneficiary.unit_utilized,
        vaccinated_List: state.beneficiary.vaccinated_List,
        vaccinated_today_List: state.beneficiary.vaccinated_today_List,
        unit_utilized_count: state.beneficiary.unit_utilized_count,
        vaccinated_count: state.beneficiary.vaccinated_count,
        vaccinated_today_count: state.beneficiary.vaccinated_today_count,
        vaccinated_analytics: {
            totalQuantityUtilized: state.beneficiary.unit_utilized_count,
            totalBeneficiaries: state.beneficiary.vaccinated_count,
            todayBeneficiaries: state.beneficiary.vaccinated_today_count,
        },
        lastmile_analytcs: state.beneficiary.lastmile_analytcs,
    };
}
export default connect(mapStateToProps, {})(BeneficiaryList);
