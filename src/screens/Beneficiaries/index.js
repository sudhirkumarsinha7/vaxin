/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    ScrollView,
    FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';

import Header, { HeaderWithBack } from '../../components/Header';
import { Colors, DeviceWidth } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { ToastShow } from '../../components/Toast';
import Search from '../../components/Search';
import { useIsFocused } from '@react-navigation/native';
import {
    getNoOfUtilized,
    getVaccinatedList,
    getTodayVaccinatedList,
    getlastMileAnalytcs,
} from '../../redux/action/beneficiary';
import {
    UnitUtilisedComponent,
    BeneficiariesVaccinatedComponent,
} from './helpler';
import fonts from '../../../FontFamily';

const listBeneficiaryTab = [
    {
        name: 'No. of Units Utilised',
        value: 'No. of Units Utilised',
        color: 'green',
        key: 'totalQuantityUtilized',
    },
    {
        name: 'Beneficiaries Vaccinated so far',
        value: 'Beneficiaries Vaccinated so far',
        color: 'green',
        key: 'totalBeneficiaries',
    },
    {
        name: 'Beneficiaries Vaccinated Today',
        value: 'Beneficiaries Vaccinated Today',
        color: 'green',
        key: 'todayBeneficiaries',
    },
];
import Empty_Card from '../../components/Empty_Card';
import { formatNumber } from '../../Util/utils';
import { VirtualizedList } from '../inventory/AddInventory/productHelper';

const BeneficaryScreen = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [activeInTab, setInTab] = useState('No. of Units Utilised');
    const [localLoader, setLocalLoader] = useState(false);

    useEffect(() => {
        async function fetchData() {
            setInTab('No. of Units Utilised');
            GetData(activeInTab);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async (title = '') => {
        await props.getlastMileAnalytcs();
        if (title === 'No. of Units Utilised') {
            await props.getNoOfUtilized();
        } else if (title === 'Beneficiaries Vaccinated so far') {
            await props.getVaccinatedList();

        } else if (title === 'Beneficiaries Vaccinated Today') {
            await props.getTodayVaccinatedList();

        } else {
            await props.getNoOfUtilized();

        }
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    flexDirection: 'row',
                    padding: 12,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <Text style={{
                    fontFamily: fonts.MEDIUM, color:
                        activeInTab === item.name ? Colors.whiteFF : Colors.black0
                }}>{t(item.value)}</Text>
                <View
                    style={{
                        backgroundColor: '#D4E6ED',
                        paddingLeft: 5,
                        paddingRight: 5,
                        borderRadius: 10,
                        marginLeft: 5,
                        marginTop: -5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            color: '#03557C',
                            padding: 5,
                            textAlign: 'center',
                        }}>
                        {formatNumber(vaccinated_analytics[item.key]) || 0}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const onChangeTab = async item => {
        setLocalLoader(true);
        GetData(item.name);
        setInTab(item.name);
        setLocalLoader(false);
    };
    const loadMoreUnitUtilized = async () => {
        await props.getNoOfUtilized(unit_utilized_page + 1);
    };
    const loadMoreVaccinated = async () => {
        await props.getVaccinatedList(vaccinated_list_page + 1);
    };
    const loadMoreTodayVaccinated = async () => {
        await props.getTodayVaccinatedList(vaccinated_today_list_page + 1);
    };
    const {
        unit_utilized = [],
        vaccinated_List = [],
        vaccinated_today_List = [],
        vaccinated_analytics = {},
        lastmile_analytcs = {},
        unit_utilized_page = 1,
        vaccinated_list_page = 1,
        vaccinated_today_list_page = 1,
    } = props;
    const { userPermissions = {} } = props;
    return (
        <View style={styles.container}>
            {/* <Header navigation={props.navigation}
                name={t('Beneficiaries')}
                scan={true}
            /> */}
            <HeaderWithBack navigation={props.navigation} name={t('Beneficiaries')} />

            <View
                style={{
                    backgroundColor: Colors.whiteFF,
                    borderTopLeftRadius: 10,
                    borderTopEndRadius: 10,
                    paddingBottom: 20,
                }}>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 12,
                    }}>
                    <View
                        style={{
                            borderRadius: 2,
                            width: scale(72),
                            height: scale(3),
                            backgroundColor: '#9B9B9B',
                        }}
                    />
                </View>
            </View>
            <VirtualizedList>
                <View
                    style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                    }}>
                    {listBeneficiaryTab.map(each => eachTab(each))}
                </View>
                {props.loder || localLoader ? (
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 1,
                        }}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                ) : null}
                {/* <Text>{JSON.stringify(vaccinated_analytics)}</Text> */}
                {activeInTab === 'No. of Units Utilised' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={unit_utilized}
                        renderItem={({ item }) => (
                            <UnitUtilisedComponent item={item} navigation={props.navigation} />
                        )}
                        onEndReached={loadMoreUnitUtilized}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Beneficiaries Vaccinated so far' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={vaccinated_List}
                        renderItem={({ item }) => (
                            <BeneficiariesVaccinatedComponent
                                item={item}
                                navigation={props.navigation}
                            />
                        )}
                        onEndReached={loadMoreVaccinated}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Beneficiaries Vaccinated Today' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={vaccinated_today_List}
                        renderItem={({ item }) => (
                            <BeneficiariesVaccinatedComponent
                                item={item}
                                navigation={props.navigation}
                            />
                        )}
                        onEndReached={loadMoreTodayVaccinated}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
            </VirtualizedList>

            <View
                style={{
                    alignItems: 'flex-end',
                    justifyContent: 'flex-end',
                    marginRight: scale(15),
                }}>
                <View
                    style={{
                        position: 'absolute',
                        bottom: 10,
                    }}>
                    <TouchableOpacity
                        onPress={() =>
                            props.navigation.navigate('AddIBeneficiary', { details: {} })
                        }
                        style={{ backgroundColor: Colors.whiteFF, borderRadius: scale(42) }}>
                        <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
});
function mapStateToProps(state) {
    return {
        userPermissions: state.auth.userPermissions,
        unit_utilized: state.beneficiary.unit_utilized,
        vaccinated_List: state.beneficiary.vaccinated_List,
        vaccinated_today_List: state.beneficiary.vaccinated_today_List,
        unit_utilized_count: state.beneficiary.unit_utilized_count,
        vaccinated_count: state.beneficiary.vaccinated_count,
        vaccinated_today_count: state.beneficiary.vaccinated_today_count,
        // vaccinated_analytics: {
        //     totalQuantityUtilized: state.beneficiary.unit_utilized_count,
        //     totalBeneficiaries: state.beneficiary.vaccinated_count,
        //     todayBeneficiaries: state.beneficiary.vaccinated_today_count,
        // },
        vaccinated_analytics: state.beneficiary.lastmile_analytcs,
        unit_utilized_page: state.beneficiary.unit_utilized_page,
        vaccinated_list_page: state.beneficiary.vaccinated_list_page,
        vaccinated_today_list_page: state.beneficiary.vaccinated_today_list_page,
    };
}
export default connect(mapStateToProps, {
    getNoOfUtilized,
    getVaccinatedList,
    getTodayVaccinatedList,
    getlastMileAnalytcs,
})(BeneficaryScreen);
