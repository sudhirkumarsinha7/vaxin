/* eslint-disable prettier/prettier */
import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import Beneficiary from './index';
import AddIBeneficiary from './AddBeneficiary';

const headerOptions = {
    headerShown: false,
};
const Stack = createNativeStackNavigator();

const BeneficiaryStack = () => {
    return (
        <Stack.Navigator
            initialRouteName="Beneficiary"
            screenOptions={headerOptions}>
            <Stack.Screen
                name="Beneficiary"
                component={Beneficiary}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="AddIBeneficiary"
                component={AddIBeneficiary}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};
export default BeneficiaryStack;
