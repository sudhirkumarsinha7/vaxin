/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    ScrollView,
    FlatList,
} from 'react-native';
import { connect } from 'react-redux';
import {
    getLocationList,
    getOrgLevel
} from '../../redux/action/auth';
import { HeaderWithBack } from '../../components/Header';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { useIsFocused } from '@react-navigation/native';
import { defaultLocation, localImage } from '../../config/global';
import MapView, { Marker, Callout } from 'react-native-maps';
import DropDown from '../../components/Dropdown';
import { config } from '../../config/config';
import axios from 'axios';
import { Colors } from '../../components/Common/Style';
import fonts from '../../../FontFamily';
import { VirtualizedList } from '../inventory/AddInventory/productHelper';
import { AnalytcsTab } from './helper'
const listGeoTab = [
    { name: 'Inbound Orders', value: 'Inbound Orders', color: 'green', },
    { name: 'Outbound Orders', value: 'Outbound Orders', color: 'green', },
    { name: 'Inbound Shipments', value: 'Inbound Shipments', color: 'green', },
    { name: 'Outbound Shipments', value: 'Outbound Shipments', color: 'green', },

];

const listOrder = [
    { name: 'Total', value: 'History', key: 'totalOrders', color: Colors.lightGrayBlue1 },
    { name: 'Received', value: 'Received', key: 'receivedOrders', color: Colors.green8b },
    { name: 'Accepted', value: 'Accepted', key: 'acceptedOrders', color: Colors.yellow9A },
    { name: 'Rejected', value: 'Rejected', key: 'rejectedOrders', color: Colors.rose },
    { name: 'Shipped', value: 'Shipped', key: 'shippedOrders', color: Colors.yellow9A },
    { name: 'Partially Shipped', value: 'Partially Shipped', key: 'partiallyShippedOrders', color: Colors.rose },
    { name: 'Fulfilled', value: 'Fulfilled', key: 'fullFiledOrders', color: Colors.lightGrayBlue1 },
    { name: 'Partially Fulfilled', value: 'Partially Fulfilled', key: 'partialFullFiledOrders', color: Colors.green8b },
    { name: 'Cancelled', value: 'Cancelled', key: 'canceledOrders', color: Colors.blueDE },
];

const listoutbound = [
    { name: 'Total', value: 'History', key: 'totalOrders', color: Colors.lightGrayBlue1 },
    { name: 'Sent', value: 'Sent', key: 'sentOrders', color: Colors.green8b },
    { name: 'Accepted', value: 'Accepted', key: 'acceptedOrders', color: Colors.yellow9A },
    { name: 'Rejected', value: 'Rejected', key: 'rejectedOrders', color: Colors.rose },
    { name: 'Transfer', value: 'Transfer', key: 'transferOrders', color: Colors.blueDE },
    { name: 'Shipped', value: 'Shipped', key: 'shippedOrders', color: Colors.yellow9A },
    { name: 'Partially Shipped', value: 'Partially Shipped', key: 'partiallyShippedOrders', color: Colors.rose },
    { name: 'Fulfilled', value: 'Fulfilled', key: 'fullFiledOrders', color: Colors.lightGrayBlue1 },
    { name: 'Partially Fulfilled', value: 'Partially Fulfilled', key: 'partialFullFiledOrders', color: Colors.green8b },
    { name: 'Cancelled', value: 'Cancelled', key: 'canceledOrders', color: Colors.blueDE },
];
const listoutboundShipment = [
    {
        name: 'Total',
        key: 'totalOutboundShipments',
        value: 'Summary',
        color: Colors.lightGrayBlue1,
    },
    { name: 'Delivered', value: 'Delivered', key: 'deliveredOutboundShipments', color: Colors.green8b },
    { name: 'Shipped', value: 'Shipped', key: 'shippedOutboundShipments', color: Colors.yellow9A },
    { name: 'Partially Delivered', value: 'Partially Delivered', key: 'partiallyDeliveredOutboundShipments', color: Colors.rose },
];
const listInboundShipment = [
    { name: 'Total', value: 'Summary', key: 'totalInboundShipments', color: Colors.lightGrayBlue1 },
    { name: 'Delivered', value: 'Delivered', key: 'deliveredInboundShipments', color: Colors.green8b },
    { name: 'Shipped', value: 'Shipped', key: 'shippedInboundShipments', color: Colors.yellow9A },
    { name: 'Partially Delivered', value: 'Partially Delivered', key: 'partiallyDeliveredInboundShipments', color: Colors.rose },
];
const GeoTacking = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [selectOrg, setOrg] = useState(null)
    const [locationList, setLocationList] = useState([])
    const [localLoader, setLocalLoader] = useState(false)
    const [apiResponse, setApiResponse] = useState(null);
    const [selectedLocation, setSelectedLocation] = useState(null);
    const [InorderAnalytcs, setInOrderAnalytcs] = useState({});
    const [InshipmentAnalytcs, setInShipmentAnalytcs] = useState({});
    const [outorderAnalytcs, setoutOrderAnalytcs] = useState({});
    const [outshipmentAnalytcs, setoutShipmentAnalytcs] = useState({});
    const [activeInTab, setInTab] = useState('Inbound Orders');
    const [show, setShow] = useState(true)

    useEffect(() => {
        async function fetchlocationList() {
            getData();
        }
        fetchlocationList();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const getData = async () => {
        await props.getOrgLevel();
    };


    const handleMarkerPress = async (location) => {
        setSelectedLocation(location);

        // Replace with your actual API URL
        await inboundOrder(location);
        await outboundOrder(location);
        await inboundShipment(location);
        await outboundShipment(location);
        setShow(true)

    };
    const inboundOrder = async (location) => {

        // Replace with your actual API URL

        try {
            const response = await axios.get(config().inbound_analtics + '?locationId=' + location._id);
            // console.log('handleMarkerPress response ' + JSON.stringify(response))

            setInOrderAnalytcs(response.data && response.data.data);
        } catch (error) {
            console.log('handleMarkerPress error ' + JSON.stringify(error))

        }
    };
    const outboundOrder = async (location) => {

        // Replace with your actual API URL
        // console.log('handleMarkerPress location ' + JSON.stringify(location))

        try {
            const response = await axios.get(config().outbound_analtics + '?locationId=' + location._id);

            setoutOrderAnalytcs(response.data && response.data.data);
        } catch (error) {
            console.log('handleMarkerPress error ' + JSON.stringify(error))

        }
    };
    const inboundShipment = async (location) => {

        // Replace with your actual API URL
        // console.log('handleMarkerPress location ' + JSON.stringify(location))

        try {
            const response = await axios.get(config().shipments_inbound_analtics + '?locationId=' + location._id);
            // console.log('handleMarkerPress response ' + JSON.stringify(response))

            setInShipmentAnalytcs(response.data && response.data.data);

        } catch (error) {
            console.log('handleMarkerPress error ' + JSON.stringify(error))

        }
    };
    const outboundShipment = async (location) => {

        // Replace with your actual API URL
        // console.log('handleMarkerPress location ' + JSON.stringify(location))

        try {
            const response = await axios.get(config().shipments_outbound_analtics + '?locationId=' + location._id);
            // console.log('handleMarkerPress response ' + JSON.stringify(response))

            setoutShipmentAnalytcs(response.data && response.data.data);
        } catch (error) {
            console.log('handleMarkerPress error ' + JSON.stringify(error))

        }
    };
    const changeAfterSelectOrg = async item => {
        const { value } = item;
        setOrg(value)
        setLocalLoader(true)
        const list = await props.getLocationList(value);
        setLocationList(list)
        setLocalLoader(false)
        setSelectedLocation(null);
        setInOrderAnalytcs({})
        setoutOrderAnalytcs({})
        setInShipmentAnalytcs({})
        setoutShipmentAnalytcs({})
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    flexDirection: 'row',
                    padding: 12,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <Text style={{
                    fontFamily: fonts.MEDIUM, color:
                        activeInTab === item.name ? Colors.whiteFF : Colors.black0
                }}>{t(item.value)}</Text>

            </TouchableOpacity>
        );
    };
    const onChangeTab = async item => {
        setInTab(item.name);
    };
    const { orgLevels = [] } = props
    const initialRegion = locationList.length > 0
        ? {
            latitude: locationList[0]?.location?.coordinates[0],
            longitude: locationList[0]?.location?.coordinates[1],
            latitudeDelta: 0.9,
            longitudeDelta: 0.9,
        }
        : {
            latitude: 23.6850,
            longitude: 90.3563,
            latitudeDelta: 0.9,
            longitudeDelta: 0.9,
        };

    console.log('locationList ' + JSON.stringify(locationList))
    return (
        <View style={styles.container}>
            <HeaderWithBack navigation={props.navigation} name={t('Geo Tracking')} />
            <View style={{ padding: 15 }}>
                <DropDown
                    labelText={t('Organization Level')}
                    val={selectOrg}
                    dropdownData={orgLevels}
                    label={'name'}
                    mapKey={'value'}
                    onChangeValue={changeAfterSelectOrg}
                />
                {props.loder || localLoader ? (
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 1,
                        }}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                ) : null}
                <Text style={styles.cardContent}>{t('Location') + ' : ' + locationList.length}</Text>

            </View>

            <MapView
                style={styles.map}
                initialRegion={initialRegion}
            >
                {locationList.map((item) => (
                    <Marker
                        key={item._id}
                        coordinate={{
                            latitude: item.location.coordinates[0],
                            longitude: item.location.coordinates[1],
                        }}
                        title={item.name}

                    >
                        <MaterialCommunityIcons
                            name="map-marker"
                            size={40} // Custom size for the icon
                            color="red" // Custom color for the icon
                        />
                        <Callout onPress={() => handleMarkerPress(item)}>
                            <Text style={{ fontSize: 14 }}>{item.name}</Text>
                        </Callout>
                    </Marker>
                ))}
            </MapView>

            {selectedLocation && (
                <View style={styles.card}>
                    {/* <TouchableOpacity onPress={() => setShow(!show)}>
                        <Text style={styles.cardTitle}>{t('Location') + ' : ' + selectedLocation.name}</Text>

                    </TouchableOpacity> */}
                    <TouchableOpacity
                        onPress={() => setShow(!show)}
                        style={{
                            flexDirection: 'row',
                        }}>
                        <Text style={styles.cardTitle}>{t('Location') + ' : ' + selectedLocation.name}</Text>

                        <AntDesign
                            name={show ? "up" : "down"}
                            size={22}
                            color={Colors.blueCF}
                            style={{ justifyContent: 'center', }}
                        />
                    </TouchableOpacity>

                    {show && <View>
                        <View
                            style={{
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                            }}>
                            {listGeoTab.map(each => eachTab(each))}
                        </View>
                        {activeInTab === 'Inbound Orders' ? (


                            Array.isArray(listOrder) ? (
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                    }}>
                                    {listOrder.map((item, index) => (
                                        <AnalytcsTab
                                            key={index.toString()}
                                            item={item}
                                            navigation={props.navigation}
                                            analytcs={InorderAnalytcs}
                                        />
                                    ))}
                                </View>
                            ) : null
                        ) : null}
                        {activeInTab === 'Outbound Orders' ? (
                            Array.isArray(listOrder) ? (
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                    }}>
                                    {listoutbound.map((item, index) => (
                                        <AnalytcsTab
                                            key={index.toString()}
                                            item={item}
                                            navigation={props.navigation}
                                            analytcs={outorderAnalytcs}
                                        />
                                    ))}
                                </View>
                            ) : null


                        ) : null}
                        {activeInTab === 'Inbound Shipments' ? (

                            Array.isArray(listOrder) ? (
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                    }}>
                                    {listInboundShipment.map((item, index) => (
                                        <AnalytcsTab
                                            key={index.toString()}
                                            item={item}
                                            navigation={props.navigation}
                                            analytcs={InshipmentAnalytcs}
                                        />
                                    ))}
                                </View>
                            ) : null


                        ) : null}
                        {activeInTab === 'Outbound Shipments' ? (

                            Array.isArray(listOrder) ? (
                                <View
                                    style={{
                                        flexDirection: 'row',
                                        flexWrap: 'wrap',
                                    }}>
                                    {listoutboundShipment.map((item, index) => (
                                        <AnalytcsTab
                                            key={index.toString()}
                                            item={item}
                                            navigation={props.navigation}
                                            analytcs={outshipmentAnalytcs}
                                        />
                                    ))}
                                </View>
                            ) : null

                        ) : null}
                    </View>}
                </View>
            )}

        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    map: {
        flex: 1.01,
    },
    card: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'white',
        padding: 20,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
        elevation: 10,
    },
    cardTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        marginRight: 10,
        color: Colors.blueCF
    },
    cardContent: {
        marginTop: 10,
    },
});
function mapStateToProps(state) {
    return {
        shipment_details: state.shipment.shipment_details,
        loder: state.loder,
        userInfo: state.auth.userInfo,
        locationList: state.auth.locationList,
        orgLevels: state.auth.orgLevels,
    };
}
export default connect(mapStateToProps, {
    getLocationList,
    getOrgLevel
})(GeoTacking);
