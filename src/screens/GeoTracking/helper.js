/* eslint-disable prettier/prettier */
import React, { useState, useEffect, useTransition } from 'react';
import {
    View,
    ScrollView,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { useTranslation } from 'react-i18next';
import { formatDateDDMMYYYY, formatNumber } from '../../Util/utils';
import fonts from '../../../FontFamily';

export const AnalytcsTab = (props) => {
    const { t, i18n } = useTranslation();
    const { item = {}, analytcs = [] } = props
    return (
        <View

            style={{
                flexDirection: 'row',
                padding: 12,
                margin: 5,

            }}>
            <Text style={{ fontFamily: fonts.MEDIUM }}>{t(item?.name) + ' : '}</Text>
            <View
                style={{
                    fontFamily: fonts.MEDIUM,
                    marginLeft: 10
                }}>
                <Text
                    style={{
                        fontFamily: fonts.REGULAR,
                        color: '#03557C',
                        textAlign: 'center',
                    }}>
                    {formatNumber(analytcs[item.key]) || 0}
                </Text>
            </View>
        </View>
    );
};