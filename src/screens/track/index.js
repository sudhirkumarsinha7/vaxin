

import React, { useState, useEffect, cloneElement } from 'react';
import {

    View,
    ScrollView,
    Text,
    RefreshControl,
    StyleSheet,
    ImageBackground,
    TouchableOpacity,
    Image,
    StatusBar,
    TextInput,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale, moderateScale, verticalScale } from '../../components/Scale';
import { connect } from 'react-redux';
import setAuthToken from '../../config/setAuthToken';

import Header from '../../components/Header';
import { Colors, DeviceWidth } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { log_out } from "../../redux/action/auth";
import { useRoute } from '@react-navigation/native';
import { ToastShow } from "../../components/Toast";
import AsyncStorage from '@react-native-async-storage/async-storage';
import Search from '../../components/Search'
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { CustomButton } from '../../components/Common/helper';
const TrackScreen = (props) => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    useEffect(() => {
        async function fetchDeta() {

        }
        fetchDeta();
    }, []);


    return (
        <View style={styles.container}>

            <Header navigation={props.navigation}
                name={t('inventory')}
                scan={true}
            />
            {/* <Search
                navigation={props.navigation}
                fromScreen={'Inventory'}
            /> */}
            <View style={{ margin: 20 }}>

            </View>



        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
function mapStateToProps(state) {
    return {



    };
}
export default connect(mapStateToProps, {
    log_out
},
)(TrackScreen);