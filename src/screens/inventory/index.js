/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect, useState } from 'react';
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Text,
    FlatList,
    ScrollView,
} from 'react-native';
import { scale } from '../../components/Scale';
import { connect } from 'react-redux';

import Header from '../../components/Header';
import { Colors } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import InventoryList from './InventoryList';

import { useIsFocused } from '@react-navigation/native';
// import { ITab } from './stack';
import {
    getInvSummary,
    getInStockList,
    getOutOFStockList,
    getInvNearExpList,
    getInvExpList,
    getInvRejectedList,
    getInvNetUtilList,
    getInvMinMaxList,
    getProdList,
    getInventoryAnalytcs,
    getInventoryValue,
    getInventoryLocationList,
    add_inventory,
} from '../../redux/action/inventory';
import { getUserInfo } from '../../redux/action/auth';
import { NetworkUtils } from '../../Util/utils';
import {
    queryAllInventryData,
    queryDeleteAllInventryData,
} from '../../databases/allSchemas';


import {
    InvSummaryComponent,
    InvInStockComponent,
    InvOutOfStockComponent,
    InvNearExpComponent,
    InvExpComponent,
    InvNetUtiComponent,
    InvMinMaxComponent,
    RejectQtyComponent,
} from './helpler';
import fonts from '../../../FontFamily';

const listInvTab = [

    { name: 'In Stock', value: 'Current Stock', color: 'green', key: 'inStock', valueKey: "inStockValue" },
    { name: 'Near Expiry', value: 'Near Expiry', color: 'green', key: 'nearExpiry', valueKey: "nearExpiryValue" },
    { name: 'Expired', value: 'Expired', color: 'green', key: 'expiredStock', valueKey: "expiredValue" },
    { name: 'Stock Out', value: 'Stock Out', color: 'green', key: 'outOfStock', valueKey: "out" },
    { name: 'Summary', value: 'Summary', color: 'green', key: 'totalInventory', valueKey: "inv" },
    { name: 'Net Utilization', value: 'Net Utilization', color: 'green', key: 'netutil', valueKey: "util" },
    { name: 'Min Max', value: 'Min Max', color: 'green', key: '-', valueKey: "rej" },

    { name: 'Reject Qty', value: 'Reject Qty', color: 'green', key: '-', valueKey: "rej" },
];

import Empty_Card from '../../components/Empty_Card';
import { formatNumber } from '../../Util/utils';
import { VirtualizedList } from './AddInventory/productHelper';


const InventoryScreen = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [activeInTab, setInTab] = useState('In Stock');
    const [localLoader, setLocalLoader] = useState(false);
    useEffect(() => {
        async function fetchData() {
            GetData(activeInTab);
            await props.getUserInfo()
            await props.getInventoryLocationList();
            await props.getProdList()
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async (title = '') => {
        const isConnected = await NetworkUtils.isNetworkAvailable();

        if (isConnected) {
            await syncInventoryData();
            await props.getInventoryAnalytcs();
            await props.getInventoryValue();
            ;
            if (title === 'In Stock') {
                await props.getInStockList();
            } else if (title === 'Near Expiry') {
                await props.getInvNearExpList();
            } else if (title === 'Expired') {
                await props.getInvExpList();
            } else if (title === 'Stock Out') {
                await props.getOutOFStockList();
            } else if (title === 'Summary') {
                await props.getInvSummary();
            } else if (title === 'Net Utilization') {
                await props.getInvNetUtilList();
            } else if (title === 'Min Max') {
                await props.getInvMinMaxList();
            } else if (title === 'Reject Qty') {
                await props.getInvRejectedList();

            } else {
                await props.getInStockList();
            }

        } else {
            // await syncInventoryData();
        }
    };
    const syncInventoryData = async () => {
        let data = await queryAllInventryData();
        console.log('queryAllInventryData list ' + JSON.stringify(data));

        if (data.length) {
            for (var i = 0; i < data.length; i++) {
                let productData = data[i].products;
                const cleanedData = productData.map(obj =>
                    Object.fromEntries(
                        Object.entries(obj).filter(([, value]) => value !== null),
                    ),
                );
                await props.add_inventory(cleanedData);
            }
            await queryDeleteAllInventryData();
        } else {
        }
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    paddingHorizontal: 15,
                    paddingVertical: 5,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <View style={{
                    padding: 5
                }}>


                    <Text style={{
                        fontFamily: fonts.MEDIUM, color:
                            activeInTab === item.name ? Colors.whiteFF : Colors.black0
                    }}>{t(item.value)}</Text>

                </View>
                {/* <View
                    style={{
                        backgroundColor: '#D4E6ED',
                        paddingLeft: 5,
                        paddingRight: 5,
                        borderRadius: 10,
                        marginLeft: 5,
                        marginTop: -5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            color: '#03557C',
                            padding: 5,
                            textAlign: 'center',
                        }}>
                        {item.name === 'Summary' ||
                            item.name === 'Net Utilization' ||
                            item.name === 'Reject Qty'
                            ? '--'
                            : formatNumber(inv_analytics[item.key]) || 0}
                    </Text>
                </View> */}
                {inv_value[item.valueKey] ? <Text
                    style={{
                        fontFamily: fonts.BOLD,
                        textAlign: 'center',
                        fontSize: 14,
                        marginTop: 5,

                        color:
                            activeInTab === item.name ? Colors.whiteFF : Colors.black0
                    }}>
                    {'$ ' + formatNumber(inv_value[item.valueKey]) + ' USD'}
                </Text> : null}
                <Text
                    style={{
                        fontFamily: fonts.BOLD,
                        textAlign: 'center',
                        marginTop: 5,
                        fontSize: 14,
                        color:
                            activeInTab === item.name ? Colors.whiteFF : Colors.black0
                    }}>
                    {formatNumber(inv_value[item.valueKey]) ? null : item.name === 'Summary' ||
                        item.name === 'Net Utilization' ||
                        item.name === 'Reject Qty' || item.name === 'Min Max'
                        ? '-'
                        : formatNumber(inv_analytics[item.key])}
                </Text>

            </TouchableOpacity>
        );
    };
    const onChangeTab = async item => {
        setLocalLoader(true);
        setInTab(item.name);
        GetData(item.name);
        setLocalLoader(false);
    };

    const loadMoreSummary = async () => {
        await props.getInvSummary(inv_summary_page + 1);
    };
    const loadMoreInstock = async () => {
        await props.getInStockList(inv_instock_page + 1);
    };
    const loadMoreOutofStock = async () => {
        await props.getOutOFStockList(inv_outstock_page + 1);
    };
    const loadMoreNearExpire = async () => {
        await props.getInvNearExpList(inv_nearexpired_page + 1);

    };
    const loadMoreExpired = async () => {
        await props.getInvExpList(inv_expired_page + 1);

    };

    const loadMoreNetUtil = async () => {
        await props.getInvNetUtilList(inv_netutilized_page + 1);
    };
    const loadMoreRejection = async () => {
        await props.getInvRejectedList(inv_rejqty_page + 1);
    };
    const loadMoreminmax = async () => {
        await props.getInvMinMaxList(inv_minmax_page + 1);
    };
    const {
        inv_summary_list = [],
        inv_stock_list = [],
        inv_out_of_stock_list = [],
        inv_near_exp_list = [],
        inv_exp_list = [],
        inv_net_utilization_list = [],
        inv_rejected_list = [],
        inv_min_max_list = [],

        userPermissions = {},
        inv_analytics = {},
        inv_value = {},
        inv_summary_page = 1,
        inv_instock_page = 1,
        inv_outstock_page = 1,
        inv_nearexpired_page = 1,
        inv_expired_page = 1,
        inv_netutilized_page = 1,
        inv_rejqty_page = 1,
        inv_minmax_page = 1
    } = props;


    return (
        <View style={styles.container}>
            <Header navigation={props.navigation} name={t('inventory')} />
            {/* <Search
                navigation={props.navigation}
                fromScreen={'Inventory'}
            /> */}

            <View
                style={{
                    borderTopLeftRadius: 10,
                    borderTopEndRadius: 10,
                    paddingBottom: 20,
                }}>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 12,
                    }}>
                    <View
                        style={{
                            borderRadius: 2,
                            width: scale(72),
                            height: scale(3),
                            backgroundColor: '#9B9B9B',
                        }}
                    />
                </View>
            </View>

            <VirtualizedList>
                <View
                    style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                    }}>
                    {listInvTab.map(each => eachTab(each))}
                </View>
                {activeInTab === 'Summary' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_summary_list}
                        renderItem={({ item }) => (
                            <InvSummaryComponent item={item} navigation={props.navigation} />
                        )}
                        onEndReached={loadMoreSummary}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'In Stock' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_stock_list}
                        renderItem={({ item }) => (
                            <InvInStockComponent item={item} navigation={props.navigation} />
                        )}
                        onEndReached={loadMoreInstock}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Stock Out' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_out_of_stock_list}
                        renderItem={({ item }) => (
                            <InvOutOfStockComponent
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                            />
                        )}
                        onEndReached={loadMoreOutofStock}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Near Expiry' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_near_exp_list}
                        renderItem={({ item }) => (
                            <InvNearExpComponent item={item} navigation={props.navigation} />
                        )}
                        onEndReached={loadMoreNearExpire}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Expired' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_exp_list}
                        renderItem={({ item }) => (
                            <InvExpComponent
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                            />
                        )}
                        onEndReached={loadMoreExpired}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Net Utilization' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_net_utilization_list}
                        renderItem={({ item }) => (
                            <InvNetUtiComponent item={item} navigation={props.navigation} />
                        )}
                        onEndReached={loadMoreNetUtil}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Min Max' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_min_max_list}
                        renderItem={({ item }) => (
                            <InvMinMaxComponent item={item} navigation={props.navigation} />
                        )}
                        onEndReached={loadMoreminmax}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Reject Qty' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_rejected_list}
                        renderItem={({ item }) => (
                            <RejectQtyComponent item={item} navigation={props.navigation} />
                        )}
                        onEndReached={loadMoreRejection}
                        onEndReachedThreshold={10}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
            </VirtualizedList>

            {userPermissions.CREATE_INVENTORY && (
                <View
                    style={{
                        alignItems: 'flex-end',
                        justifyContent: 'flex-end',
                        marginRight: scale(15),
                    }}>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: 10,
                        }}>
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('AddInventory')}
                            style={{
                                backgroundColor: Colors.whiteFF,
                                borderRadius: scale(42),
                            }}>
                            <AntDesign name="pluscircle" size={40} color={'#16B0D2'} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
});
function mapStateToProps(state) {
    return {
        userInfo: state.auth.userInfo,
        userPermissions: state.auth.userPermissions,
        loder: state.loder,
        inv_summary_list: state.inventory.inv_summary_list,
        inv_stock_list: state.inventory.inv_stock_list,
        inv_out_of_stock_list: state.inventory.inv_out_of_stock_list,
        inv_near_exp_list: state.inventory.inv_near_exp_list,
        inv_exp_list: state.inventory.inv_exp_list,
        inv_net_utilization_list: state.inventory.inv_net_utilization_list,
        inv_min_max_list: state.inventory.inv_min_max_list,
        inv_product_list: state.inventory.inv_product_list,
        inv_analytics: state.inventory.inv_analytics,
        inv_rejected_list: state.inventory.inv_rejected_list,
        inv_summary_page: state.inventory.inv_summary_page,
        inv_instock_page: state.inventory.inv_instock_page,
        inv_outstock_page: state.inventory.inv_outstock_page,
        inv_nearexpired_page: state.inventory.inv_nearexpired_page,
        inv_expired_page: state.inventory.inv_expired_page,
        inv_netutilized_page: state.inventory.inv_netutilized_page,
        inv_rejqty_page: state.inventory.inv_rejqty_page,
        inv_value: state.inventory.inv_value,
        inv_minmax_page: state.inventory.inv_minmax_page,
    };
}
export default connect(mapStateToProps, {
    getInvSummary,
    getInStockList,
    getOutOFStockList,
    getInvNearExpList,
    getInvExpList,
    getInvNetUtilList,
    getInvMinMaxList,
    getProdList,
    getInventoryAnalytcs,
    getInventoryValue,
    getInventoryLocationList,
    add_inventory,
    getInvRejectedList,
    getUserInfo,
})(InventoryScreen);
