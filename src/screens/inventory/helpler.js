/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { scale } from '../../components/Scale';

import { Colors } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import fonts from '../../../FontFamily';
import { formatDateDDMMYYYY, formatNumber, timeAgo } from '../../Util/utils';

export function removeNullKeys(obj) {
    const cleanedObj = {};

    for (const key in obj) {
        if (obj[key] !== null && obj[key] !== undefined) {
            cleanedObj[key] = obj[key];
        }
    }

    return cleanedObj;
}

export function getBackgroundcolor(status, type) {
    if (status === 'CANCELLED' || status === 'CANCELED') {
        return Colors.redE6;
    } else if (status === 'CREATE') {
        if (type === 'INVENTORY') {
            return '#dcedda';
        } else {
            return '#efeffd';
        }
    }
    if (status === 'ACCEPTED' || status === 'ACCEPT' || status === 'UPDATE') {
        return '#e7cff2';
    } else {
        return Colors.greenDA;
    }
}
export function getTextcolor(status, type) {
    if (status === 'CANCELLED' || status === 'CANCELED') {
        return Colors.red00;
    } else if (status === 'CREATE') {
        if (type === 'INVENTORY') {
            return '#0c9c00';
        } else {
            return '#545aeb';
        }
    }
    if (status === 'ACCEPTED' || status === 'ACCEPT' || status === 'UPDATE') {
        return '#9848b5';
    } else {
        return Colors.green00;
    }
}

export function getTextStatus(status, type) {
    if (status === 'CANCELLED' || status === 'CANCELED') {
        return 'Canceled';
    } else if (status === 'CREATE') {
        if (type === 'INVENTORY') {
            return 'Added';
        } else {
            return 'Sent';
        }
    } else if (
        status === 'ACCEPTED' ||
        status === 'ACCEPT' ||
        status === 'UPDATE'
    ) {
        return 'Received';
    } else if (status === 'DAMAGED') {
        return 'Transit Damage';
    } else if (status === 'LOST') {
        return 'Miscount';
    } else if (status === 'COLD_CHAIN_FAILURE') {
        return 'Cold Chain Damage';
    } else {
        return status;
    }
}
export const InventoryStatus = ({ status, type = 'INVENTORY' }) => {
    const { t } = useTranslation();

    return (
        <View
            style={{
                padding: 7,
                paddingHorizontal: scale(15),
                borderRadius: scale(20),
                backgroundColor: getBackgroundcolor(status, type),
            }}>
            <Text
                style={{
                    fontFamily: fonts.MEDIUM,
                    fontSize: 14,
                    color: getTextcolor(status, type),
                    textAlign: 'center',
                }}>
                {t(getTextStatus(status, type))}
            </Text>
        </View>
    );
};
export const InvSummaryComponent = props => {
    const { t } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const { item = {} } = props;
    const cleanedObject = removeNullKeys(item);

    const { manufacturer = {}, product = {}, batchNos = [] } = cleanedObject;
    const { units = '' } = product;
    let batchNo = '';
    if (batchNos.length) {
        batchNo = batchNos.toString();
    }
    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Category: ') + product.type}
                    </Text>
                </View>
                <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.eventTime)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.7 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {product.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {product.id}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.black0,
                                marginTop: 7,
                            }}>
                            {t('Qty : ') + item?.quantity + ' ' + units}
                        </Text>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        <InventoryStatus status={item.event} type={item.eventType} />
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 12,
                        backgroundColor: Colors.lightGrayBlue,
                        borderRadius: 12,
                    }}>
                    {manufacturer.name && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {t('Manufacturer: ') + manufacturer.name}
                        </Text>
                    )}
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.blackTextColor,
                            marginTop: 12,
                        }}>
                        {t('Expiry Date: ') + formatDateDDMMYYYY(item.expDate)}
                    </Text>
                    {/* {(item.batchNo) && <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 14, color: Colors.blackTextColor, marginTop: 12 }}>{'Batch No: ' + item.batchNo}</Text>} */}
                    {batchNo && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blackTextColor,
                                marginTop: 12,
                            }}>
                            {t('Batch No: ') + batchNo}
                        </Text>
                    )}
                </View>
            ) : null}
        </View>
    );
};
export const InvInStockComponent = props => {
    const { t } = useTranslation();

    const { item = {} } = props;
    let { product = {} } = item;

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Category: ') + product.type}
                    </Text>
                </View>
                <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.updatedAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.7 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {product.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {product.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {formatNumber(item.quantity) + ' ' + product.units}
                        </Text>
                        <TouchableOpacity
                            style={{ borderBottomWidth: 0.5, borderColor: Colors.blueD2 }}
                            onPress={() =>
                                props.navigation.navigate('ProductListByBatch', {
                                    productId: product,
                                })
                            }>
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blueD2,
                                }}>
                                {item.batches.length + t(' Batches')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => setExtended(!isExtended)}>
                <AntDesign name={isExtended ? 'caretup' : 'caretdown'} size={20} color={Colors.blueChill} />

            </TouchableOpacity> */}
            </View>
            {/* {isExtended ? <View style={{ padding: 12, backgroundColor: Colors.greenF8, borderRadius: 12, borderColor: Colors.green8b, borderWidth: 0.5 }}>
            <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 14, color: Colors.grayTextColor }}>{'Manufacturer: ' + manufacturer.name}</Text>

        </View> : null} */}
        </View>
    );
};

export const InvInStockBatchComponent = props => {
    const { t } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const { item = {}, product = {} } = props;

    const { units = '' } = product;
    const { manufacturer = {}, vvmStatus = '',
    } = item;
    const vvmstatus = vvmStatus === 1 || vvmStatus === 2 ? t('Usable')
        : t('non usable')
    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 14,
                        color: Colors.grayTextColor,
                    }}>
                    {t('Category: ') + product.type}
                </Text>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.6 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {product.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginTop: 5,
                            }}>
                            {product.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blackTextColor,
                                marginTop: 12,
                            }}>
                            {formatNumber(item.quantity) + ' ' + units}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blueD2,
                                marginTop: 12,
                            }}>
                            {t('Batch No: ') + item.batchNo}
                        </Text>
                        {item.serialNo && (
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blueD2,
                                    marginTop: 12,
                                }}>
                                {t('serialNo: ') + item.serialNo}
                            </Text>
                        )}
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 12,
                        backgroundColor: Colors.greenF8,
                        borderRadius: 12,
                        borderColor: Colors.green8b,
                        borderWidth: 0.5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.black0,
                        }}>
                        {t('Manufacturer :  ') + manufacturer.name}
                    </Text>

                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                            marginTop: 7,
                        }}>
                        {t('Mfg Date: ') + formatDateDDMMYYYY(item.mfgDate)}
                    </Text>
                    {item.expDate && <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.red67,
                            marginTop: 7,
                        }}>
                        {t('Expiry Date: ') + formatDateDDMMYYYY(item.expDate)}
                    </Text>}
                    {vvmStatus && <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            marginTop: 7,
                            color: vvmStatus === 1 || vvmStatus === 2 ? 'green' : 'red'
                        }}>
                        {t('Vaccine vial monitor') + ' : ' + vvmstatus + ' (' + vvmStatus + ') '}
                    </Text>}
                </View>
            ) : null}
        </View>
    );
};
export const InvOutOfStockComponent = props => {
    const { t } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const { item = {}, isReOrder = false } = props;
    const cleanedObject = removeNullKeys(item);
    let { product = [] } = cleanedObject;
    if (product.length) {
        product = product[0];
    }
    const { productId = {} } = cleanedObject;
    const { units = {}, manufacturer = {} } = product;
    const reorderPrd = () => {
        const productList = {
            productId: product._id,
            quantity: item.quantityInTransit || 100,
            units: units,
            productName: product.name,
            type: product.type,
            fromPage: 'ReOrder',
        };
        props.navigation.navigate('order', {
            screen: 'revieworder',
            params: {
                productList: [productList],
                updQty: true,
            },
        });
    };
    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Category: ') + product.type}
                    </Text>
                </View>
                <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.updatedAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.7 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {product.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {product.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {timeAgo(item.lastOutOfStock)}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 12,
                        backgroundColor: Colors.yellowFb,
                        borderRadius: 12,
                        borderWidth: 0.5,
                        borderColor: Colors.yellow9A,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.7 }}>
                            {manufacturer.name && (
                                <Text
                                    style={{
                                        fontFamily: fonts.BOLD,
                                        fontSize: 14,
                                        color: Colors.grayTextColor,
                                    }}>
                                    {t('Manufacturer: ') + manufacturer.name}
                                </Text>
                            )}
                        </View>
                        {isReOrder && (
                            <View style={{ flex: 0.3 }}>
                                <TouchableOpacity
                                    onPress={() => reorderPrd()}
                                    style={{
                                        padding: 12,
                                        borderRadius: scale(20),
                                        borderWidth: 1,
                                        borderColor: Colors.grayC5,
                                    }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.BOLD,
                                            fontSize: 16,
                                            color: Colors.blackTextColor,
                                            textAlign: 'center',
                                        }}>
                                        {t('Reorder')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        )}
                    </View>
                </View>
            ) : null}
        </View>
    );
};
export const InvNearExpComponent = props => {
    const { t } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const { item = {} } = props;
    const cleanedObject = removeNullKeys(item);

    const { productId = {} } = cleanedObject;
    const { manufacturer = {} } = productId;

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 14,
                        color: Colors.grayTextColor,
                    }}>
                    {t('Category: ') + productId.type}
                </Text>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.7 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {productId.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {productId.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 14,
                                color: Colors.red67,
                            }}>
                            {formatDateDDMMYYYY(item.expDate)}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {timeAgo(item.expDate)}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 12,
                        backgroundColor: Colors.yellowFb,
                        borderRadius: 12,
                        borderWidth: 0.5,
                        borderColor: Colors.yellow9A,
                    }}>
                    {manufacturer.name && (
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {t('Manufacturer: ') + manufacturer.name}
                        </Text>
                    )}
                    {item.batchNo && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blackTextColor,
                                marginTop: 12,
                            }}>
                            {t('Batch No: ') + item.batchNo}
                        </Text>
                    )}
                </View>
            ) : null}
        </View>
    );
};
export const InvExpComponent = props => {
    const { t } = useTranslation();
    const [isExtended, setExtended] = useState(false);
    const { item = {}, isReOrder = false } = props;
    const cleanedObject = removeNullKeys(item);

    const { productId = {} } = cleanedObject;
    const { units = '', manufacturer = {} } = productId;
    const reorderPrd = () => {
        const productList = {
            productId: productId._id,
            quantity: item.quantity || 100,
            units: units,
            productName: productId.name,
            type: productId.type,
            fromPage: 'ReOrder',
        };
        props.navigation.navigate('order', {
            screen: 'revieworder',
            params: {
                productList: [productList],
                updQty: true,
            },
        });
    };

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 14,
                        color: Colors.grayTextColor,
                    }}>
                    {t('Category: ') + productId.type}
                </Text>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.7 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {productId.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {productId.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 14,
                                color: Colors.red67,
                            }}>
                            {formatDateDDMMYYYY(item.expDate)}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {timeAgo(item.expDate)}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 12,
                        backgroundColor: Colors.yellowFb,
                        borderRadius: 12,
                        borderWidth: 0.5,
                        borderColor: Colors.yellow9A,
                    }}>
                    <View style={{ flexDirection: 'row' }}>
                        <View style={{ flex: 0.7 }}>
                            {manufacturer.name && (
                                <Text
                                    style={{
                                        fontFamily: fonts.BOLD,
                                        fontSize: 14,
                                        color: Colors.grayTextColor,
                                    }}>
                                    {t('Manufacturer: ') + manufacturer.name}
                                </Text>
                            )}
                            <Text
                                style={{
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 14,
                                    color: Colors.blackTextColor,
                                    marginTop: 12,
                                }}>
                                {t('Quantity: ') + item.quantity + ' ' + units}
                            </Text>
                            {item.batchNo && (
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 14,
                                        color: Colors.blackTextColor,
                                        marginTop: 12,
                                    }}>
                                    {t('Batch No: ') + item.batchNo}
                                </Text>
                            )}
                        </View>

                        {isReOrder && (
                            <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                                <TouchableOpacity
                                    onPress={() => reorderPrd()}
                                    style={{
                                        padding: 12,
                                        borderRadius: scale(20),
                                        borderWidth: 1,
                                        borderColor: Colors.grayC5,
                                    }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.BOLD,
                                            fontSize: 16,
                                            color: Colors.blackTextColor,
                                            textAlign: 'center',
                                        }}>
                                        {t('Reorder')}
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        )}
                    </View>
                </View>
            ) : null}
        </View>
    );
};
export const InvNetUtiComponent = props => {
    const { t } = useTranslation();
    const [isExtended, setExtended] = useState(false);
    const { item } = props;
    let { product = [] } = item;
    if (product.length) {
        product = product[0];
    }

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 14,
                        color: Colors.grayTextColor,
                    }}>
                    {t('Category: ') + product.type}
                </Text>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.4 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {product.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                            }}>
                            {product.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.6, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 14,
                                color: Colors.blue8E,
                            }}>
                            {t('Current Balance: ') + formatNumber(item.quantity)}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 12,
                        backgroundColor: Colors.bluec2Wh,
                        borderRadius: 12,
                        borderWidth: 0.5,
                        borderColor: Colors.blueC2,
                    }}>
                    {/* <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 14, color: Colors.grayTextColor }}>{'Manufacturer: Bharat BioTech'}</Text> */}
                    <Text
                        style={{
                            fontFamily: fonts.BOLD,
                            fontSize: 14,
                            color: Colors.green00,
                            marginTop: 12,
                        }}>
                        {t('Opening Balance: ') + item.openingBalance}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.BOLD,
                            fontSize: 14,
                            color: Colors.YellowishBrown,
                            marginTop: 12,
                        }}>
                        {t('Utilized Qty : ') + item.quantityUtilized}
                    </Text>
                </View>
            ) : null}
        </View>
    );
};

export const RejectQtyComponent = props => {
    const { t } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    const { item = {} } = props;
    let { productId = {}, lastShipment = {} } = item;

    // const {destination={}} =lastShipment

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.4 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Category: ') + productId.type}
                    </Text>
                </View>
                <View style={{ flex: 0.6, alignItems: 'flex-end' }}>
                    <View
                        style={{
                            padding: 7,
                            paddingHorizontal: scale(15),
                            borderRadius: scale(20),
                            backgroundColor: getBackgroundcolor('CANCELED'),
                        }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 12,
                                color: getTextcolor('CANCELED'),
                                textAlign: 'center',
                            }}>
                            {getTextStatus(item.status)}
                        </Text>
                    </View>
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.7 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {productId.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginTop: 5,
                            }}>
                            {productId.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {formatNumber(item.quantity) + ' ' + productId.units}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 12,
                                color: Colors.blackTextColor,
                                marginTop: 5,
                            }}>
                            {formatDateDDMMYYYY(item.updatedAt)}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 12,
                        backgroundColor: Colors.greenF8,
                        borderRadius: 12,
                        borderColor: Colors.green8b,
                        borderWidth: 0.5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.black0,
                        }}>
                        {t('Batch No : ') + item.batchNo}
                    </Text>
                    {item.serialNo && (
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.black0,
                            }}>
                            {t('Serial No :  ') + item.serialNo}
                        </Text>
                    )}

                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                            marginTop: 7,
                        }}>
                        {t('Shipment ID: ') + (lastShipment.id || '')}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                            marginTop: 7,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(lastShipment.actualDeliveryDate)}
                    </Text>

                    {/* <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 14, color: Colors.grayTextColor, marginTop: 7 }}>{'Delivery location: ' + ('')}</Text> */}
                </View>
            ) : null}
        </View>
    );
};
export const InvMinMaxComponent = (props) => {
    const { t } = useTranslation();

    const { item = {} } = props;
    let { product = [], threshold = {} } = item;
    if (product && product.length) {
        product = product[0]
    }
    const min = threshold.min ? threshold.min : '-'
    const max = threshold.max ? threshold.max : '-'

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Category: ') + product.type}
                    </Text>
                </View>
                <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {t('Date: ') + formatDateDDMMYYYY(item.updatedAt)}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.7 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: 16,
                                color: Colors.blackTextColor,
                            }}>
                            {product.name}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.REGULAR,
                                fontSize: 14,
                                color: Colors.grayTextColor,
                                marginTop: 5
                            }}>
                            {product.id}
                        </Text>
                    </View>
                    <View style={{ flex: 0.3, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blackTextColor,
                            }}>
                            {t('Min : ') + min}
                        </Text>
                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 14,
                                color: Colors.blackTextColor,
                                morginTop: 5
                            }}>
                            {t('Max : ') + max}
                        </Text>
                    </View>
                </View>

            </View>

        </View>
    );
};

export const InventoryListCardComponent = () => {
    const { t, i18n } = useTranslation();
    const [isExtended, setExtended] = useState(false);

    return (
        <View
            style={{
                borderRadius: 12,
                borderWidth: 0.5,
                marginTop: 12,
                marginHorizontal: scale(15),
                borderColor: Colors.grayC5,
            }}>
            <View
                style={{
                    borderBottomWidth: 0.5,
                    padding: 12,
                    flexDirection: 'row',
                    borderColor: Colors.grayC5,
                }}>
                <View style={{ flex: 0.6 }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 16,
                            color: Colors.grayTextColor,
                        }}>
                        {'Category: Vaccine'}
                    </Text>
                </View>
                <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 16,
                            color: Colors.grayTextColor,
                        }}>
                        {'Date: 19/06/2023'}
                    </Text>
                </View>
            </View>
            <View style={{ padding: 12 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 0.6 }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: scale(16),
                                color: Colors.blackTextColor,
                            }}>
                            {'Covishield'}
                        </Text>

                        <Text
                            style={{
                                fontFamily: fonts.MEDIUM,
                                fontSize: 16,
                                color: Colors.grayTextColor,
                            }}>
                            {'435647233'}
                        </Text>
                    </View>
                    <View style={{ flex: 0.4, alignItems: 'flex-end' }}>
                        <Text
                            style={{
                                fontFamily: fonts.BOLD,
                                fontSize: scale(16),
                                color: Colors.blackTextColor,
                            }}>
                            {'300 Units'}
                        </Text>
                    </View>
                </View>
                <TouchableOpacity
                    style={{ alignSelf: 'center' }}
                    onPress={() => setExtended(!isExtended)}>
                    <AntDesign
                        name={isExtended ? 'caretup' : 'caretdown'}
                        size={20}
                        color={Colors.blueChill}
                    />
                </TouchableOpacity>
            </View>
            {isExtended ? (
                <View
                    style={{
                        padding: 12,
                        backgroundColor: Colors.bluec2Wh,
                        borderRadius: 12,
                        borderWidth: 0.5,
                        borderColor: Colors.blueC2,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 14,
                            color: Colors.grayTextColor,
                        }}>
                        {'Manufacturer: Bharat BioTech'}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.BOLD,
                            fontSize: 14,
                            color: Colors.YellowishBrown,
                            marginTop: 12,
                        }}>
                        {'Minimum: 100 Units'}
                    </Text>
                    <Text
                        style={{
                            fontFamily: fonts.BOLD,
                            fontSize: 14,
                            color: Colors.blue8E,
                            marginTop: 12,
                        }}>
                        {'Maximum: 8000 Units'}
                    </Text>
                </View>
            ) : null}
        </View>
    );
};
