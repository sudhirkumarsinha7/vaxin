/* eslint-disable prettier/prettier */
import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import Inventory from './index';
import AddInventory from './AddInventory';
import ReviewInventory from './AddInventory/reviewInventory';

import ProductListByBatch from './InventoryList/batchlist';
const headerOptions = {
    headerShown: false,
};
const Stack = createNativeStackNavigator();

const InventoryStack = () => {
    return (
        <Stack.Navigator initialRouteName="Inventory" screenOptions={headerOptions}>
            <Stack.Screen
                name="Inventory"
                component={Inventory}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="AddInventory"
                component={AddInventory}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ReviewInventory"
                component={ReviewInventory}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ProductListByBatch"
                component={ProductListByBatch}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
};
export default InventoryStack;
