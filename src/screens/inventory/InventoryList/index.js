/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    SafeAreaView,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import {
    InvSummaryComponent,
    InvInStockComponent,
    InvOutOfStockComponent,
    InvNearExpComponent,
    InvExpComponent,
    InvNetUtiComponent,
    InvMinMaxComponent,
    RejectQtyComponent,
} from '../helpler';
import fonts from '../../../../FontFamily';

const listInvTab = [
    { name: 'Summary', value: 'Summary', color: Colors.lightGrayBlue1, key: 'totalInventory' },
    { name: 'In Stock', value: 'In Stock', color: Colors.green8b, key: 'inStock' },
    { name: 'Stock Out', value: 'Stock Out', color: Colors.yellow9A, key: 'outOfStock' },
    { name: 'Near Expiry', value: 'Near Expiry', color: Colors.rose, key: 'nearExpiry' },
    { name: 'Expired', value: 'Expired', color: Colors.YellowishBrown, key: 'expiredStock' },
    { name: 'Net Utilization', value: 'Net Utilization', color: Colors.greenDA, key: 'netutil' },
    { name: 'Reject Qty', value: 'Reject Qty', color: Colors.greenDA, key: '-' },
];

import Empty_Card from '../../../components/Empty_Card';
import { formatNumber } from '../../../Util/utils';
import { VirtualizedList } from '../AddInventory/productHelper';

const InventoryList = props => {
    const { t } = useTranslation();
    const isFocused = useIsFocused();
    const [activeInTab, setInTab] = useState('Summary');
    const [localLoader, setLocalLoader] = useState(false);

    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        setInTab('Summary');
    };
    const eachTab = item => {
        return (
            <TouchableOpacity
                onPress={() => onChangeTab(item)}
                style={{
                    flexDirection: 'row',
                    padding: 12,
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor:
                        activeInTab === item.name ? item.color : Colors.whiteFF,
                }}>
                <Text style={{ fontFamily: fonts.MEDIUM }}>{t(item.value)}</Text>
                <View
                    style={{
                        backgroundColor: '#D4E6ED',
                        paddingLeft: 5,
                        paddingRight: 5,
                        borderRadius: 10,
                        marginLeft: 5,
                        marginTop: -5,
                    }}>
                    <Text
                        style={{
                            fontFamily: fonts.REGULAR,
                            color: '#03557C',
                            padding: 5,
                            textAlign: 'center',
                        }}>
                        {item.name === 'Summary' ||
                            item.name === 'Net Utilization' ||
                            item.name === 'Reject Qty'
                            ? '--'
                            : formatNumber(inv_analytics[item.key]) || 0}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const onChangeTab = async item => {
        setLocalLoader(true);
        await props.GetData();
        setInTab(item.name);
        setLocalLoader(false);
    };
    const {
        inv_summary_list = [],
        inv_stock_list = [],
        inv_out_of_stock_list = [],
        inv_near_exp_list = [],
        inv_exp_list = [],
        inv_net_utilization_list = [],
        inv_rejected_list = [],
        userPermissions = {},
        inv_analytics = {},
    } = props;
    // console.log('inv_out_of_stock_list ' + JSON.stringify(inv_rejected_list))
    return (
        <SafeAreaView style={styles.container}>
            {/* <FilterCardButtons onPressButton={() => props.navigation.navigate('order', {
                screen: 'Filter',

            })} /> */}

            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : null}
            <VirtualizedList>
                <View
                    style={{
                        flexDirection: 'row',
                        flexWrap: 'wrap',
                    }}>
                    {listInvTab.map(each => eachTab(each))}
                </View>
                {/* <Text>{JSON.stringify(inv_analytics)}</Text> */}
                {activeInTab === 'Summary' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_summary_list}
                        renderItem={({ item }) => (
                            <InvSummaryComponent item={item} navigation={props.navigation} />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'In Stock' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_stock_list}
                        renderItem={({ item }) => (
                            <InvInStockComponent item={item} navigation={props.navigation} />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Stock Out' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_out_of_stock_list}
                        renderItem={({ item }) => (
                            <InvOutOfStockComponent
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                            />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Near Expiry' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_near_exp_list}
                        renderItem={({ item }) => (
                            <InvNearExpComponent item={item} navigation={props.navigation} />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Expired' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_exp_list}
                        renderItem={({ item }) => (
                            <InvExpComponent
                                item={item}
                                navigation={props.navigation}
                                isReOrder={userPermissions.RE_ORDER}
                            />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Net Utilization' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_net_utilization_list}
                        renderItem={({ item }) => (
                            <InvNetUtiComponent item={item} navigation={props.navigation} />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Min. & Max.' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={[]}
                        renderItem={({ item }) => (
                            <InvMinMaxComponent item={item} navigation={props.navigation} />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
                {activeInTab === 'Reject Qty' ? (
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_rejected_list}
                        renderItem={({ item }) => (
                            <RejectQtyComponent item={item} navigation={props.navigation} />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        ListEmptyComponent={<Empty_Card Text="No items available" />}
                    />
                ) : null}
            </VirtualizedList>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.whiteFF,
        // borderRadius: scale(15),
        paddingTop: 10,
    },
});

function mapStateToProps(state) {
    return {
        loder: state.loder,
        inv_summary_list: state.inventory.inv_summary_list,
        inv_stock_list: state.inventory.inv_stock_list,
        inv_out_of_stock_list: state.inventory.inv_out_of_stock_list,
        inv_near_exp_list: state.inventory.inv_near_exp_list,
        inv_exp_list: state.inventory.inv_exp_list,
        inv_net_utilization_list: state.inventory.inv_net_utilization_list,
        inv_min_max_list: state.inventory.inv_min_max_list,
        inv_product_list: state.inventory.inv_product_list,
        inv_analytics: state.inventory.inv_analytics,
        inv_rejected_list: state.inventory.inv_rejected_list,
        userPermissions: state.auth.userPermissions,
    };
}
export default connect(mapStateToProps, {})(InventoryList);
