/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from 'react';
import { View, StyleSheet, Text, FlatList, TouchableOpacity } from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { Colors } from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import { InvInStockBatchComponent } from '../helpler';
import fonts from '../../../../FontFamily';
import Header from '../../../components/Header';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useRoute } from '@react-navigation/native';
import { getProdListByBatch } from '../../../redux/action/inventory';
import { VirtualizedList } from '../AddInventory/productHelper';
const ProductListByBatch = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const route = useRoute();
    const { params } = route;
    const productId = params.productId || {};
    useEffect(() => {
        async function fetchData() {
            // console.log('productId', productId);
            await props.getProdListByBatch(productId._id);
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);

    const { inv_prod_list_by_Bath = [] } = props;
    // console.log('inv_prod_list_by_Bath' + JSON.stringify(inv_prod_list_by_Bath));

    return (
        <View style={styles.container}>
            <Header navigation={props.navigation} name={t('inventory')} />

            <View
                style={{
                    backgroundColor: Colors.whiteFF,
                    borderTopLeftRadius: 10,
                    borderTopEndRadius: 10,
                    paddingBottom: 20,
                }}>
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginTop: 12,
                    }}>
                    <View
                        style={{
                            borderRadius: 2,
                            width: scale(72),
                            height: scale(3),
                            backgroundColor: '#9B9B9B',
                        }}
                    />
                </View>
            </View>
            <VirtualizedList>
                <View style={{ backgroundColor: '#fff', paddingBottom: 20 }}>
                    <TouchableOpacity
                        onPress={() => props.navigation.goBack()}
                        style={{
                            flexDirection: 'row',
                            backgroundColor: '#D4E6ED',
                            padding: 10,
                            borderRadius: 10,
                            alignSelf: 'center',
                        }}>
                        <AntDesign name="caretleft" color={'#16B0D2'} size={18} />
                        <Text style={{ fontFamily: fonts.BOLD }}>
                            {inv_prod_list_by_Bath.length + ' Batches'}
                        </Text>
                    </TouchableOpacity>
                    <FlatList
                        contentContainerStyle={{ paddingBottom: 100 }}
                        data={inv_prod_list_by_Bath}
                        renderItem={({ item }) => (
                            <InvInStockBatchComponent
                                item={item}
                                navigation={props.navigation}
                                product={productId}
                            />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // borderRadius: scale(15),
        backgroundColor: Colors.whiteFF,
    },
});

function mapStateToProps(state) {
    return {
        loder: state.loder,
        inv_prod_list_by_Bath: state.inventory.inv_prod_list_by_Bath,
    };
}
export default connect(mapStateToProps, {
    getProdListByBatch,
})(ProductListByBatch);
