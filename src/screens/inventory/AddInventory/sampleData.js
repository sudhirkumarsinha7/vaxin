export const productList = [
    { value: 'covid-19', key: '1' },
    { value: 'Chickenpox', key: '2' },
    { value: 'Dengue', key: '3' },
    { value: 'Diphtheria', key: '4' },
    { value: 'Influenza', key: '5' },
    { value: 'Hepatitis A', key: '6' },
    { value: 'Hepatitis B', key: '7' },
    { value: 'Haemophilus influenzae type b', key: '8' },
    { value: 'Human Papillomavirus', key: '9' },
    { value: 'Pneumococcal', key: '10' }
]

export const productTypes = [
    { type: 'Vaccine', key: '1' },
    { type: 'Medichine', key: '2' },
    { type: 'Homeopathic Remedies', key: '3' },
    { type: 'Stress Relieving Products', key: '4' },
    { type: 'Sensory Organs Care', key: '5' },
    { type: 'Treatments', key: '6' },
    { type: 'Gastrointestinal Remedy Products', key: '7' },
    { type: 'Mouth Treatments', key: '8' },
    { type: 'Respiratory', key: '9' },
    { type: 'Pain Relief', key: '10' }
]


