/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect, useRef } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import {
    Colors,
} from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../../config/global';
import {
    ProductListComponentNew,
    AddNewProductComponent,
    VirtualizedList,
    EditProductComponentNew,
} from './productHelper';
import fonts from '../../../../FontFamily';
import { ToastShow } from '../../../components/Toast';
import {
    getInventoryLocationList,
    getProdList,
} from '../../../redux/action/inventory';
import {
    filterAndMoveToFront,
    groupAndStoreByCategory,
    removeEmptyValues,
    filterUniqueObjectsByProperty,
} from './productUtil';
import { NetworkUtils } from '../../../Util/utils';

function addObjectAndUpdateQuantity(array, newObj) {
    // Check if the new object meets the conditions
    const existingIndex = array.findIndex(
        item =>
            item.productId === newObj.productId &&
            item.mfgDate === newObj.mfgDate &&
            item.expDate === newObj.expDate &&
            item.batchNo === newObj.batchNo &&
            item.type === newObj.type,
    );
    if (existingIndex !== -1) {
        // If the object already exists, update the quantity
        array[existingIndex].quantity += newObj.quantity;
    } else {
        // If the object does not exist, add it to the array
        array.push(newObj);
    }

    return array;
}
function deleteAtIndex(array, index) {
    // Check if the index is valid
    if (index >= 0 && index < array.length) {
        // Use splice to remove the element at the specified index
        array.splice(index, 1);
    }

    return array;
}
function updateObjectAtIndex(array, index, updatedObject) {
    // Check if the index is valid
    if (index >= 0 && index < array.length) {
        // Update the object at the specified index
        array[index] = { ...array[index], ...updatedObject };
    }

    return array;
}
function updateInnerObjectInArray(
    array,
    outerIndex,
    innerIndex,
    updatedProperty,
) {
    console.log('array.length ', array.length);
    console.log(' array[outerIndex] ', array[outerIndex]);

    if (
        outerIndex >= 0 &&
        outerIndex < array.length &&
        innerIndex >= 0 &&
        innerIndex < array[outerIndex].products.length
    ) {
        // Update the inner object at the specified index
        array[outerIndex].products[innerIndex] = {
            ...array[outerIndex].products[innerIndex],
            ...updatedProperty,
        };
    }

    return array;
}
const AddInventory = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [isAdd, setISAdd] = useState(true);
    const [currentIndx, setCurrentIndx] = useState(null);
    const [selectedProductType, setSelectedProductType] = useState(null);
    const [productList, setProductList] = useState([]);
    const [productTypes, setProductTypes] = useState([]);
    const [productListByType, setProductListByType] = useState([]);
    const flatListRef = useRef(null);
    const [isEdit, setIsEdit] = useState(false);

    const [selectedPrd, setSelectedPrd] = useState(null);

    const onViewRef = useRef(viewableItems => { });

    const viewConfigRef = useRef({ viewAreaCoveragePercentThreshold: 50 });

    useEffect(() => {
        async function fetchData() {
            GetData();
        }
        fetchData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isFocused]);
    const GetData = async () => {
        const isConnected = await NetworkUtils.isNetworkAvailable();
        if (isConnected) {
            await props.getInventoryLocationList();
            const ProdList = await props.getProdList();
            const groupedAndStored = groupAndStoreByCategory(ProdList, 'type');
            const resultArray = Object.values(groupedAndStored);
            setProductTypes(resultArray);
        } else {
            const groupedAndStored = groupAndStoreByCategory(
                inv_product_list,
                'type',
            );
            const resultArray = Object.values(groupedAndStored);
            setProductTypes(resultArray);
        }
    };

    const confirmProductlist = () => {
        if (productList.length) {
            props.navigation.navigate('ReviewInventory', { productList: productList });
        } else {
            ToastShow(t('Please add atleast one Product'), 'error', 'long', 'top');
        }
    };

    const eachProductType = (item, index) => {
        return (
            <TouchableOpacity
                style={{
                    margin: 7,
                    borderTopLeftRadius: 10,
                    borderTopRightRadius: 10,
                    width: scale(90),
                    backgroundColor:
                        selectedProductType === item.type ? '#208196' : '#fff',
                    borderColor: '#7F7F7F',
                    borderRadius: 10,
                    borderWidth: 0.5,
                }}
                onPress={() => onSelectProdType(item, index)}>
                <View
                    style={{
                        backgroundColor: '#fff',
                        borderWidth: 0.5,
                        borderColor: '#7F7F7F',
                        borderBottomEndRadius: 10,
                        borderBottomLeftRadius: 10,
                        borderRadius: 12,
                    }}>
                    <Image
                        source={localImage.media}
                        style={{
                            height: scale(30),
                            width: scale(90),
                            alignSelf: 'center',
                            borderRadius: 10,
                        }}
                    />
                </View>
                <View style={{ alignSelf: 'center' }}>
                    <Text
                        numberOfLines={3}
                        ellipsizeMode="tail"
                        style={{
                            borderTopWidth: 0.2,
                            padding: 7,
                            color:
                                selectedProductType === item.type ? Colors.whiteFF : '#7F7F7F',
                            fontFamily: fonts.REGULAR,
                        }}>
                        {item.type}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const onSelectProdType = (item, index) => {
        setSelectedProductType(item.type);
        if (flatListRef.current) {
            flatListRef.current.scrollToIndex({ index: 0 }); // Scroll to day 10
        }
        const uniqueArray = filterUniqueObjectsByProperty(
            item.productsByType,
            'name',
        );

        setProductListByType(uniqueArray);
        setIsEdit(false);
    };

    const addProductInList = data => {
        let productData = removeEmptyValues(data);
        productData.type = selectedProductType;
        console.log('addProductInList productList', productList);
        if (productList.length) {
            const newData = addObjectAndUpdateQuantity(productList, productData);
            console.log('addProductInList newData', newData);

            setProductList(newData);
        } else {
            setProductList([productData]);
        }
    };
    const updateProductInList = data => {
        const productData = removeEmptyValues(data);

        const newArray = updateObjectAtIndex(productList, currentIndx, productData);

        setProductList(newArray);
        setIsEdit(false);
    };
    const deleteProduct = (item, indexToDelete) => {
        const newArray = deleteAtIndex(productList, indexToDelete);
        setProductList(newArray);
    };
    const editProduct = async (item, indexToEdit) => {
        setISAdd(false);
        setCurrentIndx(indexToEdit);
        setSelectedPrd(item);
        setIsEdit(true);
    };
    const { inv_product_list = [] } = props;
    // console.log('selectedPrd ', selectedPrd)
    const FilterproductTypes = filterAndMoveToFront(
        productTypes,
        'type',
        selectedProductType,
    );
    console.log('addProductInList productListwr', productList);

    return (
        <View style={styles.container}>
            <HeaderWithBack navigation={props.navigation} name={t('Add Inventory')} />

            <VirtualizedList>
                <View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20 }}>
                    {/* <View
                        style={{
                            backgroundColor: Colors.whiteFF,
                            borderTopLeftRadius: 10,
                            borderTopEndRadius: 10,
                            paddingBottom: 5,
                        }}>
                        <View
                            style={{
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginTop: 12,
                            }}>
                            <View
                                style={{
                                    borderRadius: 2,
                                    width: scale(72),
                                    height: scale(3),
                                    backgroundColor: '#9B9B9B',
                                }}
                            />
                        </View>
                    </View> */}

                    {isAdd ? (
                        <View>
                            <View style={{ flex: 0.4, padding: 12, borderRadius: 20 }}>
                                <FlatList
                                    ref={flatListRef} // add ref
                                    data={FilterproductTypes}
                                    horizontal
                                    keyExtractor={(item, index) => index.toString()}
                                    renderItem={({ item, index }) => eachProductType(item, index)}
                                    showsHorizontalScrollIndicator={false}
                                    onViewableItemsChanged={onViewRef.current}
                                    viewabilityConfig={viewConfigRef.current}
                                />
                            </View>
                            <View>
                                {selectedProductType ? (
                                    <AddNewProductComponent
                                        selectedPrd={selectedPrd}
                                        productListByType={productListByType}
                                        addProductInList={addProductInList}
                                        selectedProductType={selectedProductType}
                                        inv_list={props.inv_list}
                                    />
                                ) : (
                                    <View
                                        style={{
                                            justifyContent: 'center',
                                            alignItems: 'center',
                                            padding: 40,
                                        }}>
                                        <Text
                                            style={{
                                                fontFamily: fonts.MEDIUM,
                                                fontSize: 16,
                                                textAlign: 'center',
                                            }}>
                                            {t('*Select Product Category to show list of products')}
                                        </Text>
                                    </View>
                                )}
                            </View>
                        </View>
                    ) : isEdit ? (
                        <EditProductComponentNew
                            selectedPrd={selectedPrd}
                            updateProductInList={updateProductInList}
                        />
                    ) : (
                        <ProductListComponentNew
                            navigation={props.navigation}
                            confirmProductlist={confirmProductlist}
                            productData={productList}
                            deleteProduct={deleteProduct}
                            editProduct={editProduct}
                            closeForm={() => setISAdd(true)}
                        />
                    )}
                </View>

                <TouchableOpacity
                    onPress={() => { setISAdd(!isAdd); }}
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                    }}>
                    <Text style={{ fontSize: 20, fontFamily: fonts.BOLD, marginRight: 10 }}>
                        {t('Selected Products')}
                    </Text>
                    <AntDesign
                        name="up"
                        size={15}
                        color={'#000'}
                        style={{ justifyContent: 'center', marginTop: 5 }}
                    />
                </TouchableOpacity>
            </VirtualizedList>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

function mapStateToProps(state) {
    return {
        inv_product_list: state.inventory.inv_product_list,
        inv_list: state.inventory.inv_list,
    };
}
export default connect(mapStateToProps, {
    getProdList,
    getInventoryLocationList,
})(AddInventory);
