/* eslint-disable no-unused-vars */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    ActivityIndicator,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import { connect } from 'react-redux';

import { HeaderWithBack } from '../../../components/Header';
import {
    Colors,
} from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';

import { useIsFocused } from '@react-navigation/native';
import { localImage } from '../../../config/global';
import {
    CustomButton,
    CustomButtonWithBorder,
} from '../../../components/Common/helper';
import { PopupMessage } from '../../../components/popUp';
import { useRoute } from '@react-navigation/native';

import { ReviewHeader, ReviewProductList } from './productHelper';
import { add_inventory } from '../../../redux/action/inventory';
import fonts from '../../../../FontFamily';
import { ToastShow } from '../../../components/Toast';

import { NetworkUtils } from '../../../Util/utils';
import { addInventorylocal } from '../../../databases/realmConnection';
const ReviewInventory = props => {
    const { t, i18n } = useTranslation();
    const isFocused = useIsFocused();
    const [localLoader, setLocalLoader] = useState(false);
    const [isPopupVisible, setPopupVisible] = useState(false);

    const route = useRoute();
    const { params } = route;
    const productList = params.productList || [];
    useEffect(() => {
        async function fetchData() { }
        fetchData();
    }, [isFocused]);




    const _addInv = async () => {
        // console.log('productList ' + JSON.stringify(productList))

        const isConnected = await NetworkUtils.isNetworkAvailable();
        let result = {};
        setLocalLoader(true);
        if (isConnected) {
            result = await props.add_inventory(productList);
        } else {
            result = await addInventorylocal(productList);
        }

        setLocalLoader(false);

        console.log(' _addInv res result' + JSON.stringify(result));
        if (result?.status === 200) {
            setPopupVisible(true);
        } else if (result.status === 500) {
            const err = result?.data?.message;
            ToastShow(t(err), 'error', 'long', 'top');
        } else if (result?.status === 401) {
            const err = result?.data?.message;
            ToastShow(t(err), 'error', 'long', 'top');
        } else {
            const err = result.data;
            ToastShow(t(err?.message), 'error', 'long', 'top');
        }
    };
    const closPopUp = () => {
        setPopupVisible(false);
        props.navigation.navigate('Inventory');
    };
    return (
        <View style={styles.container}>
            <HeaderWithBack
                navigation={props.navigation}
                name={t('Review Inventory')}
            />

            <PopupMessage
                message=""
                title={t('Products added to Inventory')}
                isVisible={isPopupVisible}
                onClose={() => closPopUp()}
                buttonName={t('Confirm')}
                image={localImage.addInvSuccess}
            />

            <View style={{ flex: 1, backgroundColor: '#fff', borderRadius: 20 }}>
                {props.loder || localLoader ? (
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            flex: 1,
                        }}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                ) : null}
                <View
                    style={{
                        backgroundColor: Colors.whiteFF,
                        borderTopLeftRadius: 10,
                        borderTopEndRadius: 10,
                        paddingBottom: 20,
                    }}>
                    <View
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 12,
                        }}>
                        <View
                            style={{
                                borderRadius: 2,
                                width: scale(72),
                                height: scale(3),
                                backgroundColor: '#9B9B9B',
                            }}
                        />
                    </View>
                </View>

                <ReviewHeader />

                <FlatList
                    data={productList}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => <ReviewProductList item={item} />}
                />
            </View>
            {props.loder || localLoader ? (
                <View
                    style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                        flex: 1,
                    }}>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>
            ) : (
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                    }}>
                    <CustomButtonWithBorder
                        bdColor={'#C5C5C5'}
                        textColor={'#20232B'}
                        buttonName={t('Back')}
                        navigation={props.navigation}
                        onPressButton={() => props.navigation.goBack()}
                    />
                    <CustomButton
                        bgColor={'#208196'}
                        buttonName={t('Confirm')}
                        navigation={props.navigation}
                        onPressButton={() => _addInv()}
                    />
                </View>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

function mapStateToProps(state) {
    return {};
}
export default connect(mapStateToProps, {
    add_inventory,
})(ReviewInventory);
