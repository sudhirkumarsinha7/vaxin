/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect } from 'react';
import {
    Keyboard,
    View,
    Text,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import { scale } from '../../../components/Scale';
import {
    Colors,
    DeviceWidth,
} from '../../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import DropDown, { ManufacturerDropdown } from '../../../components/Dropdown';
import {
    InputField,
    CustomButton,
} from '../../../components/Common/helper';
import { DatePickerComponent } from '../../../components/DatePicker';
import fonts from '../../../../FontFamily';
const defaultQty = [50, 100, 200, 500, 1000];
import { formatDateDDMMYYYY } from '../../../Util/utils';
import { ToastShow } from '../../../components/Toast';
export const VirtualizedList = ({ children }) => {
    return (
        <FlatList
            data={[]}
            keyExtractor={() => 'key'}
            renderItem={null}
            ListHeaderComponent={<>{children}</>}
        />
    );
};
export const SelectedProdHeader = props => {
    const { t } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Category')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Name')}
                </Text>
            </View>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{'Batch No'}</Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {'(' + t('Quantity') + ')'}
                </Text>
            </View>
            <View style={{ flex: 0.1, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Edit')}</Text>
            </View>
            <View style={{ flex: 0.1, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Del')}</Text>
            </View>
        </View>
    );
};
export const SelectedProdHeaderOrder = props => {
    const { t } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Category')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Name')}
                </Text>
            </View>
            <View style={{ flex: 0.4, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Quantity')}</Text>
            </View>
            <View style={{ flex: 0.1, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Edit')}</Text>
            </View>
            <View style={{ flex: 0.1, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Del')}</Text>
            </View>
        </View>
    );
};
export const ProductListComponent = props => {
    const { t } = useTranslation();
    const [isRefresh, setIsRefresh] = useState(false);

    useEffect(() => { }, [isRefresh]);
    const {
        closeForm,
        confirmProductlist,
        productData,
        deleteProduct,
        editProduct,
    } = props;
    const eachProduct = (item, index) => {
        const { units = '' } = item;

        return (
            <View
                style={{
                    flexDirection: 'row',
                    padding: 7,
                    borderBottomWidth: 1,
                    borderColor: Colors.grayC5,
                    alignItems: 'center',
                }}>
                <View style={{ flex: 0.45 }}>
                    <Text
                        style={{ fontFamily: fonts.MEDIUM, marginBottom: 3, fontSize: 12 }}>
                        {item.type}
                    </Text>
                    <Text style={{ fontFamily: fonts.REGULAR, fontSize: 11 }}>
                        {item.productName}
                    </Text>
                </View>
                <View style={{ flex: 0.35 }}>
                    <Text style={{ fontFamily: fonts.REGULAR, fontSize: 11 }}>
                        {item.quantity + ' ' + units}
                    </Text>
                </View>

                <TouchableOpacity
                    style={{ flex: 0.15 }}
                    onPress={() => {
                        editProduct(item, index);
                        setIsRefresh(!isRefresh);
                    }}>
                    <MaterialCommunityIcons
                        name={'square-edit-outline'}
                        color={'#7F7F7F'}
                        size={20}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    style={{ flex: 0.05 }}
                    onPress={() => {
                        deleteProduct(item, index);
                        setIsRefresh(!isRefresh);
                    }}>
                    <MaterialCommunityIcons
                        name={'delete-outline'}
                        color={'#7F7F7F'}
                        size={20}
                    />
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={{ paddingVertical: 20, paddingHorizontal: 20 }}>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        justifyContent: 'space-between',
                    }}>
                    <TouchableOpacity
                        onPress={() => closeForm()}
                        style={{ flexDirection: 'row' }}>
                        <Text
                            style={{ fontSize: 16, fontFamily: fonts.MEDIUM, marginRight: 10 }}>
                            {t('Selected Products')}
                        </Text>
                        <AntDesign
                            name="down"
                            size={15}
                            color={'#000'}
                            style={{ justifyContent: 'center', marginTop: 3 }}
                        />
                    </TouchableOpacity>
                    <Text style={{ color: '#7F7F7F', fontSize: 14, marginTop: 5 }}>
                        {t('View Summary of the Products')}
                    </Text>
                </View>
                <CustomButton
                    bgColor={'#208196'}
                    buttonName={t('Confirm')}
                    navigation={props.navigation}
                    onPressButton={confirmProductlist}
                />
            </View>
            <SelectedProdHeaderOrder />

            <FlatList
                contentContainerStyle={{ paddingBottom: 20 }}
                data={productData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => eachProduct(item, index)}
            />
        </View>
    );
};
export const ProductListComponentNew = props => {
    const { t, i18n } = useTranslation();
    const [isRefresh, setIsRefresh] = useState(false);

    useEffect(() => { }, [isRefresh]);
    const {
        closeForm,
        confirmProductlist,
        productData,
        deleteProduct,
        editProduct,
    } = props;
    const eachProduct = (item, index) => {
        const { units = '' } = item;

        return (
            <View
                style={{
                    flexDirection: 'row',
                    padding: 7,
                    borderBottomWidth: 1,
                    borderColor: Colors.grayC5,
                    alignItems: 'center',
                }}>
                <View style={{ flex: 0.4, marginRight: 10 }}>
                    <Text
                        style={{ fontFamily: fonts.MEDIUM, marginBottom: 3, fontSize: 12 }}>
                        {item.type}
                    </Text>
                    <Text style={{ fontFamily: fonts.REGULAR, fontSize: 11 }}>
                        {item.productName}
                    </Text>
                </View>
                <View style={{ flex: 0.4, marginRight: 10 }}>
                    <Text
                        style={{ fontFamily: fonts.MEDIUM, marginBottom: 3, fontSize: 12 }}>
                        {item.batchNo}
                    </Text>

                    <Text style={{ fontFamily: fonts.REGULAR, fontSize: 11 }}>
                        {item.quantity + ' ' + units}
                    </Text>
                </View>

                <TouchableOpacity
                    style={{ flex: 0.15 }}
                    onPress={() => {
                        editProduct(item, index);
                        setIsRefresh(!isRefresh);
                    }}>
                    <MaterialCommunityIcons
                        name={'square-edit-outline'}
                        color={'#7F7F7F'}
                        size={20}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    style={{ flex: 0.05 }}
                    onPress={() => {
                        deleteProduct(item, index);
                        setIsRefresh(!isRefresh);
                    }}>
                    <MaterialCommunityIcons
                        name={'delete-outline'}
                        color={'#7F7F7F'}
                        size={20}
                    />
                </TouchableOpacity>
            </View>
        );
    };

    return (
        <View style={{ paddingTop: 10, paddingHorizontal: 20 }}>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                }}>
                <View
                    style={{
                        justifyContent: 'space-between',
                        // alignItems: 'center',
                    }}>
                    <TouchableOpacity
                        onPress={() => closeForm()}
                        style={{ flexDirection: 'row' }}>
                        <Text
                            style={{ fontSize: 16, fontFamily: fonts.BOLD, marginRight: 10 }}>
                            {t('Selected Products')}
                        </Text>
                        <AntDesign
                            name="down"
                            size={15}
                            color={'#000'}
                            style={{ justifyContent: 'center', marginTop: 2 }}
                        />
                    </TouchableOpacity>
                    <Text style={{ color: '#7F7F7F', fontSize: 12, marginTop: 5 }}>
                        {t('View Summary of the Products')}
                    </Text>
                </View>
                <CustomButton
                    bgColor={'#208196'}
                    buttonName={t('Confirm')}
                    navigation={props.navigation}
                    onPressButton={confirmProductlist}
                />
            </View>
            <SelectedProdHeader />
            <FlatList
                contentContainerStyle={{ paddingBottom: 20 }}
                data={productData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => eachProduct(item, index)}
            />
            <View style={{ height: DeviceWidth / 3 }} />
        </View>
    );
};
export const AddNewProductComponent = props => {
    const { t, i18n } = useTranslation();
    const {
        addProductInList,
        selectedProductType,
        productListByType,
        inv_list = [],
    } = props;
    const [isMandatory, setIsMandatory] = useState(false);

    const [manufaturerList, setManufatureList] = useState([]);
    const [quantityPerUnit, setQuantityPerUnit] = useState(1);
    const [perUnitList, setPerUnitList] = useState([]);

    useEffect(() => {
        productForm.resetForm();
        if (inv_list.length) {
            productForm.handleChange({
                target: { name: 'inventoryId', value: inv_list[0]._id },
            });
            productForm.handleChange({
                target: { name: 'inventory', value: inv_list[0].name },
            });
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedProductType]);
    var validationSchema = Yup.object().shape(
        {
            batchNo: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            productId: Yup.string().required(t('Required')),
            manufacturerId: Yup.string().required(t('Required')),
            inventoryId: Yup.string().required(t('Required')),
            mfgDate: Yup.string().required(t('Required')),
            quantityPerUnit: Yup.string().required(t('Required')),
            quantity: Yup.number()
                .positive(t('Quantity must be greater than 0'))
                .integer(t('Quantity must be an integer'))
                .test(
                    'is-multiple-of-' + quantityPerUnit,
                    t('Quantity must be a multiple of ') + quantityPerUnit,
                    value => value % quantityPerUnit === 0,
                ),
        },
        ['batchNo', 'productId', 'quantity'],
    ); // <-- HERE!!!!!!!!
    const productForm = useFormik({
        initialValues: {
            productId: '',
            quantity: '',
            mfgDate: '',
            expDate: '',
            batchNo: '',
            serialNo: '',
            units: '',
            inventoryId: '',
            manufacturerId: '',
            quantityPerUnit: '',
        },
        validationSchema,
        onSubmit: (values, actions) => {
            handleSubmit({ ...values });
        },
    });
    const _addproduct = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        productForm.handleSubmit();
    };

    const handleSubmit = async values => {
        console.log('handleSubmit values', values);
        setIsMandatory(false);

        addProductInList(values);
        // productForm.resetForm()
        ToastShow(
            values.productName + t(' Added succesfully'),
            'success',
            'long',
            'top',
        );
        productForm.handleChange({ target: { name: 'quantity', value: '' } });
        productForm.handleChange({ target: { name: 'mfgDate', value: '' } });
        productForm.handleChange({ target: { name: 'expDate', value: '' } });
        productForm.handleChange({ target: { name: 'batchNo', value: '' } });
        productForm.handleChange({ target: { name: 'serialNo', value: '' } });
    };
    const incrementQuantity = () => {
        let Qty = Number(productForm?.values?.quantity) + 1;
        productForm.handleChange({ target: { name: 'quantity', value: Qty } });
    };

    const decrementQuantity = () => {
        if (productForm?.values?.quantity > 1) {
            let Qty = Number(productForm?.values?.quantity) - 1;
            productForm.handleChange({ target: { name: 'quantity', value: Qty } });
        }
    };

    const handleInputChange = text => {
        // Validate input (allow only positive integers)
        const value = parseInt(text, 10);
        if (!isNaN(value) && value > 0) {
            productForm.handleChange({ target: { name: 'quantity', value: value } });
        } else if (text === '') {
            // Clear the input if it becomes empty
            productForm.handleChange({ target: { name: 'quantity', value: '' } });
        }
    };
    const eachQty = item => {
        return (
            <TouchableOpacity
                onPress={() => handleInputChange(item)}
                style={{
                    padding: scale(7),
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor: Colors.whiteFF,
                }}>
                <Text>{item + ''}</Text>
            </TouchableOpacity>
        );
    };
    const onChangeMfg = val => {
        productForm.handleChange({ target: { name: 'mfgDate', value: val } });
        productForm.handleChange({ target: { name: 'expDate', value: null } });
    };
    const onChangeExp = val => {
        productForm.handleChange({ target: { name: 'expDate', value: val } });
    };
    // const { values, handleChange } = productForm
    const changeAfterSelectProduct = item => {
        const { value, eachItem } = item;
        const { manufacturers = [] } = eachItem;
        // console.log('changeAfterSelectProduct ' + JSON.stringify(eachItem))
        productForm.handleChange({ target: { name: 'productId', value: value } });
        productForm.handleChange({
            target: { name: 'productName', value: eachItem.name },
        });
        productForm.handleChange({ target: { name: 'units', value: eachItem.units } });

        if (manufacturers.length) {
            setManufatureList(manufacturers);
            productForm.handleChange({
                target: { name: 'manufacturerId', value: manufacturers[0].id },
            });
            if (manufacturers[0].quantityPerUnit.length) {
                setPerUnitList(manufacturers[0].quantityPerUnit);

                productForm.handleChange({
                    target: {
                        name: 'quantityPerUnit',
                        value: manufacturers[0].quantityPerUnit[0],
                    },
                });
                setQuantityPerUnit(manufacturers[0].quantityPerUnit[0]);
            }
        }
    };
    const changeAfterSelectManufacturer = item => {
        const { value, eachItem } = item;
        productForm.handleChange({ target: { name: 'manufacturerId', value: value } });
        productForm.handleChange({
            target: { name: 'quantityPerUnit', value: eachItem.quantityPerUnit },
        });
        if (eachItem.quantityPerUnit.length) {
            setPerUnitList(eachItem.quantityPerUnit);

            productForm.handleChange({
                target: { name: 'quantityPerUnit', value: eachItem.quantityPerUnit[0] },
            });
            setQuantityPerUnit(eachItem.quantityPerUnit[0]);
        }
    };
    const changeAfterSelectPerUnit = item => {
        const { value } = item;
        productForm.handleChange({ target: { name: 'quantityPerUnit', value: value } });
        setQuantityPerUnit(value);
    };
    const changeAfterSelectInventory = item => {
        const { value, label } = item;
        productForm.handleChange({ target: { name: 'inventoryId', value: value } });
        productForm.handleChange({ target: { name: 'inventory', value: label } });
    };
    return (
        <View>
            <View style={{ backgroundColor: '#CFE7DE' }}>
                <View style={{ margin: 10 }}>
                    <DropDown
                        dropdownData={inv_list}
                        onChangeValue={changeAfterSelectInventory}
                        label="name"
                        mapKey="_id"
                        val={productForm?.values?.inventoryId}
                        placeholder={t('Select Location')}
                        errorMsg={productForm?.errors?.inventoryId}
                        mandatory={isMandatory}
                        labelText={t('Inventory Location')}
                        search={false}
                    />
                    <DropDown
                        dropdownData={productListByType}
                        onChangeValue={changeAfterSelectProduct}
                        label="name"
                        mapKey="_id"
                        val={productForm?.values?.productId}
                        placeholder={t('Select Product')}
                        errorMsg={productForm?.errors?.productId}
                        mandatory={isMandatory}
                        labelText={t('Product')}
                    />
                    <ManufacturerDropdown
                        dropdownData={manufaturerList}
                        onChangeValue={changeAfterSelectManufacturer}
                        label="name"
                        mapKey="id"
                        val={productForm?.values?.manufacturerId}
                        placeholder={t('Select Manufacturer')}
                        errorMsg={productForm?.errors?.manufacturerId}
                        mandatory={isMandatory}
                        labelText={t('Manufacturer')}
                        search={false}
                    />
                    <DropDown
                        dropdownData={perUnitList}
                        onChangeValue={changeAfterSelectPerUnit}
                        val={productForm?.values?.quantityPerUnit}
                        placeholder={t('Select')}
                        errorMsg={productForm?.errors?.quantityPerUnit}
                        mandatory={isMandatory}
                        labelText={t('Quantity Per unit')}
                        search={false}
                    />
                </View>
                <View style={{ margin: 10 }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            marginBottom: 10,
                        }}>
                        <View style={{ flex: 25, marginLeft: 5, marginTop: 20 }}>
                            <Text
                                style={{
                                    flexDirection: 'row',
                                    fontFamily: fonts.BOLD,
                                    fontSize: 15,
                                }}>
                                {t('Quantity')}
                                <Text style={{ color: 'red' }}>*</Text>
                            </Text>
                            {productForm?.values?.units && (
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 12,
                                        marginTop: 5,
                                    }}>
                                    {'(' + productForm?.values?.units + ')'}
                                </Text>
                            )}
                        </View>
                        <View style={{ flex: 75, justifyContent: 'center', marginRight: 20 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={decrementQuantity}
                                    style={{ justifyContent: 'center' }}>
                                    <AntDesign name={'minus'} color={'#000'} size={18} />
                                </TouchableOpacity>
                                <View style={{ width: '90%' }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.MEDIUM,
                                            fontSize: 12,
                                            color: Colors.gray9,
                                            marginBottom: 5,
                                        }}>
                                        {t('Enter Qty multiple of ') + quantityPerUnit}
                                    </Text>

                                    <InputField
                                        placeholder={t('Quantity')}
                                        inputValue={productForm?.values?.quantity + ''}
                                        setInputValue={handleInputChange}
                                        KeyboardType={'numeric'}
                                    />
                                </View>
                                <TouchableOpacity
                                    onPress={incrementQuantity}
                                    style={{ justifyContent: 'center' }}>
                                    <AntDesign name={'plus'} color={'#000'} size={18} />
                                </TouchableOpacity>
                            </View>
                            {isMandatory && productForm?.errors?.quantity && (
                                <Text
                                    style={{
                                        color: 'red',
                                        marginTop: 7,
                                        fontSize: 12,
                                        fontFamily: fonts.MEDIUM,
                                        marginLeft: 5,
                                        marginBottom: 5,
                                    }}>
                                    {productForm?.errors?.quantity}
                                </Text>
                            )}

                            <View
                                style={{
                                    flexDirection: 'row',
                                    flexWrap: 'wrap',
                                }}>
                                {defaultQty.map(each => eachQty(each))}
                            </View>
                        </View>
                    </View>
                    <InputField
                        placeholder={t('Batch Number')}
                        label={t('Batch Number')}
                        inputValue={productForm?.values?.batchNo}
                        setInputValue={text => {
                            productForm.handleChange({
                                target: { name: 'batchNo', value: text },
                            });
                        }}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={productForm?.errors?.batchNo}
                        mandatory={isMandatory}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: '48%' }}>
                            <DatePickerComponent
                                label={t('Mfg. Date')}
                                placeholder={t('Mfg. Date')}
                                onChange={val => onChangeMfg(val)}
                                val={productForm?.values?.mfgDate}
                                isMfgDate={true}
                                errorMsg={productForm?.errors?.mfgDate}
                                mandatory={isMandatory}
                            />
                        </View>
                        <View style={{ width: '48%' }}>
                            <DatePickerComponent
                                label={t('Exp. Date')}
                                placeholder={t('Exp. Date')}
                                onChange={val => onChangeExp(val)}
                                val={productForm?.values?.expDate}
                                minDate={
                                    productForm?.values?.mfgDate
                                        ? new Date(productForm?.values?.mfgDate) >= new Date()
                                            ? productForm?.values?.mfgDate
                                            : new Date()
                                        : new Date()
                                }
                                isMfgDate={false}
                                errorMsg={productForm?.errors?.expDate}
                                isMandatoryField={false}
                            />
                        </View>
                    </View>
                    <InputField
                        placeholder={t('Serial Number')}
                        label={t('Serial Number')}
                        isMandatoryField={false}
                        inputValue={productForm?.values?.serialNo}
                        setInputValue={productForm.handleChange('serialNo')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={productForm?.errors?.batchNo}
                        mandatory={isMandatory}
                    />
                </View>
            </View>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                <Text />
                <CustomButton
                    bgColor={'#208196'}
                    buttonName={t('Confirm')}
                    navigation={props.navigation}
                    onPressButton={_addproduct}
                />
            </View>
        </View>
    );
};
export const EditProductComponentNew = props => {
    const { t, i18n } = useTranslation();
    const { updateProductInList, selectedPrd = {} } = props;
    const [isMandatory, setIsMandatory] = useState(false);

    const [quantityPerUnit, setQuantityPerUnit] = useState(1);

    useEffect(() => {
        productForm.handleChange({
            target: { name: 'quantity', value: selectedPrd.quantity + '' },
        });
        productForm.handleChange({
            target: { name: 'mfgDate', value: selectedPrd.mfgDate },
        });
        productForm.handleChange({
            target: { name: 'expDate', value: selectedPrd.expDate },
        });
        productForm.handleChange({
            target: { name: 'batchNo', value: selectedPrd.batchNo },
        });
        productForm.handleChange({
            target: { name: 'serialNo', value: selectedPrd.serialNo },
        });
        productForm.handleChange({
            target: { name: 'productId', value: selectedPrd.productId },
        });
        productForm.handleChange({
            target: { name: 'units', value: selectedPrd.units },
        });
        productForm.handleChange({
            target: { name: 'productName', value: selectedPrd.productName },
        });
        productForm.handleChange({ target: { name: 'type', value: selectedPrd.type } });
        productForm.handleChange({
            target: { name: 'quantityPerUnit', value: selectedPrd.quantityPerUnit },
        });
        setQuantityPerUnit(selectedPrd.quantityPerUnit);
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedPrd]);

    var validationSchema = Yup.object().shape(
        {
            batchNo: Yup.string()
                .required(t('Required'))
                .trim()
                .min(2, t('Must be at least 2 characters long'))
                .test(
                    'no-spaces',
                    t('It must not be empty spaces'),
                    value => value && value.trim() !== '',
                ),
            productId: Yup.string()
                .min(1, t('Too Short!'))
                .max(256, t('Too Lonng!'))
                .required(t('Required')),
            quantity: Yup.number()
                .positive(t('Quantity must be greater than 0'))
                .integer(t('Quantity must be an integer'))
                .test(
                    'is-multiple-of-' + quantityPerUnit,
                    t('Quantity must be a multiple of ') + quantityPerUnit,
                    value => value % quantityPerUnit === 0,
                ),

            mfgDate: Yup.string().required(t('Required')),
        },
        ['batchNo', 'productId', 'quantity'],
    ); // <-- HERE!!!!!!!!
    const productForm = useFormik({
        initialValues: {
            productId: '',
            quantity: '',
            mfgDate: '',
            expDate: '',
            batchNo: '',
            serialNo: '',
            units: '',
        },
        validationSchema,
        onSubmit: (values, actions) => {
            handleSubmit({ ...values });
        },
    });
    const _addproduct = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        productForm.handleSubmit();
    };

    const handleSubmit = async values => {
        console.log('handleSubmit values', values);
        setIsMandatory(false);

        updateProductInList(values);
        productForm.resetForm();
        ToastShow(
            values.productName + ' updated succesfully ',
            'success',
            'long',
            'top',
        );
        productForm.handleChange({ target: { name: 'quantity', value: '' } });
        productForm.handleChange({ target: { name: 'mfgDate', value: '' } });
        productForm.handleChange({ target: { name: 'expDate', value: '' } });
        productForm.handleChange({ target: { name: 'batchNo', value: '' } });
        productForm.handleChange({ target: { name: 'serialNo', value: '' } });
    };
    const incrementQuantity = () => {
        let Qty = Number(productForm?.values?.quantity) + 1;
        productForm.handleChange({ target: { name: 'quantity', value: Qty } });
    };

    const decrementQuantity = () => {
        if (productForm?.values?.quantity > 1) {
            let Qty = Number(productForm?.values?.quantity) - 1;
            productForm.handleChange({ target: { name: 'quantity', value: Qty } });
        }
    };

    const handleInputChange = text => {
        // Validate input (allow only positive integers)
        const value = parseInt(text, 10);
        if (!isNaN(value) && value > 0) {
            productForm.handleChange({ target: { name: 'quantity', value: value } });
        } else if (text === '') {
            // Clear the input if it becomes empty
            productForm.handleChange({ target: { name: 'quantity', value: '' } });
        }
    };
    const eachQty = item => {
        return (
            <TouchableOpacity
                onPress={() => handleInputChange(item)}
                style={{
                    padding: scale(7),
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor: Colors.whiteFF,
                }}>
                <Text>{item + ''}</Text>
            </TouchableOpacity>
        );
    };
    const onChangeMfg = val => {
        productForm.handleChange({ target: { name: 'mfgDate', value: val } });
        productForm.handleChange({ target: { name: 'expDate', value: null } });
    };
    const onChangeExp = val => {
        productForm.handleChange({ target: { name: 'expDate', value: val } });
    };

    return (
        <View>
            <View style={{ backgroundColor: '#CFE7DE' }}>
                <Text
                    style={{
                        fontFamily: fonts.B,
                        fontSize: 15,
                        textAlign: 'center',
                        marginTop: 10,
                    }}>
                    {t('Edit Product')}
                </Text>
                <View style={{ margin: 10 }}>
                    <InputField
                        placeholder={t('Product Name')}
                        label={t('Product Name')}
                        inputValue={productForm?.values?.productName}
                        setInputValue={text => {
                            productForm.handleChange({
                                target: { name: 'productName', value: text },
                            });
                        }}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={productForm?.errors?.productName}
                        mandatory={isMandatory}
                        disable={true}
                    />
                    <InputField
                        placeholder={t('Quantity per Unit')}
                        label={t('Quantity per Unit')}
                        inputValue={productForm?.values?.quantityPerUnit + ''}
                        setInputValue={text => {
                            productForm.handleChange({
                                target: { name: 'quantityPerUnit', value: text },
                            });
                        }}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={productForm?.errors?.quantityPerUnit}
                        mandatory={isMandatory}
                        disable={true}
                    />
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            marginBottom: 10,
                        }}>
                        <View style={{ flex: 25, marginLeft: 5, marginTop: 20 }}>
                            <Text
                                style={{
                                    flexDirection: 'row',
                                    fontFamily: fonts.BOLD,
                                    fontSize: 15,
                                }}>
                                {t('Quantity')}
                                <Text style={{ color: 'red' }}>*</Text>
                            </Text>
                            {productForm?.values?.units && (
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 12,
                                        marginTop: 5,
                                    }}>
                                    {'(' + productForm?.values?.units + ')'}
                                </Text>
                            )}
                        </View>
                        <View style={{ flex: 75, justifyContent: 'center', marginRight: 20 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={decrementQuantity}
                                    style={{ justifyContent: 'center' }}>
                                    <AntDesign name={'minus'} color={'#000'} size={18} />
                                </TouchableOpacity>
                                <View style={{ width: '90%' }}>
                                    <Text
                                        style={{
                                            fontFamily: fonts.MEDIUM,
                                            fontSize: 12,
                                            color: Colors.gray9,
                                            marginBottom: 5,
                                        }}>
                                        {t('Enter Qty multiple of ') + quantityPerUnit}
                                    </Text>

                                    <InputField
                                        placeholder={t('Quantity')}
                                        inputValue={productForm?.values?.quantity + ''}
                                        setInputValue={handleInputChange}
                                        KeyboardType={'numeric'}
                                    />
                                </View>
                                <TouchableOpacity
                                    onPress={incrementQuantity}
                                    style={{ justifyContent: 'center' }}>
                                    <AntDesign name={'plus'} color={'#000'} size={18} />
                                </TouchableOpacity>
                            </View>
                            {isMandatory && productForm?.errors?.quantity && (
                                <Text
                                    style={{
                                        color: 'red',
                                        marginTop: 7,
                                        fontSize: 12,
                                        fontFamily: fonts.MEDIUM,
                                        marginLeft: 5,
                                        marginBottom: 5,
                                    }}>
                                    {productForm?.errors?.quantity}
                                </Text>
                            )}

                            <View
                                style={{
                                    flexDirection: 'row',
                                    flexWrap: 'wrap',
                                }}>
                                {defaultQty.map(each => eachQty(each))}
                            </View>
                        </View>
                    </View>
                    <InputField
                        placeholder={t('Batch Number')}
                        label={t('Batch Number')}
                        inputValue={productForm?.values?.batchNo}
                        setInputValue={text => {
                            productForm.handleChange({
                                target: { name: 'batchNo', value: text },
                            });
                        }}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={productForm?.errors?.batchNo}
                        mandatory={isMandatory}
                    />
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: '48%' }}>
                            <DatePickerComponent
                                label={t('Mfg. Date')}
                                placeholder={t('Mfg. Date')}
                                onChange={val => onChangeMfg(val)}
                                val={productForm?.values?.mfgDate}
                                isMfgDate={true}
                                errorMsg={productForm?.errors?.mfgDate}
                                mandatory={isMandatory}
                            />
                        </View>
                        <View style={{ width: '48%' }}>
                            <DatePickerComponent
                                label={t('Exp. Date')}
                                placeholder={t('Exp. Date')}
                                onChange={val => onChangeExp(val)}
                                val={productForm?.values?.expDate}
                                minDate={
                                    productForm?.values?.mfgDate
                                        ? new Date(productForm?.values?.mfgDate) >= new Date()
                                            ? productForm?.values?.mfgDate
                                            : new Date()
                                        : new Date()
                                }
                                isMfgDate={false}
                                errorMsg={productForm?.errors?.expDate}
                                mandatory={isMandatory}
                            />
                        </View>
                    </View>
                    <InputField
                        placeholder={t('Serial Number')}
                        inputValue={productForm?.values?.serialNo}
                        setInputValue={productForm.handleChange('serialNo')}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={productForm?.errors?.batchNo}
                        mandatory={isMandatory}
                    />
                </View>
            </View>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                <Text />
                <CustomButton
                    bgColor={'#208196'}
                    buttonName={t('Confirm')}
                    navigation={props.navigation}
                    onPressButton={_addproduct}
                />
            </View>
        </View>
    );
};

export const ReviewHeader = props => {
    const { t } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.37, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Category')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Name')}
                </Text>
            </View>
            <View style={{ flex: 0.37, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{'Batch No'}</Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('(Quantity)')}
                </Text>
            </View>
            <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Mfg. Date')}
                </Text>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Exp. Date')}
                </Text>
            </View>
        </View>
    );
};
export const ReviewProductList = props => {

    const { item = {} } = props;
    const { units = '' } = item;
    return (
        <View
            style={{
                flexDirection: 'row',
                borderBottomWidth: 0.5,
                borderColor: '#7F7F7F',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.37, justifyContent: 'center', padding: 2 }}>
                <Text
                    style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: '#7F7F7F' }}>
                    {item.type}
                </Text>

                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13, marginTop: 3 }}>
                    {item.productName}
                </Text>
            </View>
            <View style={{ flex: 0.37, justifyContent: 'center', padding: 2 }}>
                <Text
                    style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: '#7F7F7F' }}>
                    {item.batchNo}
                </Text>
                <Text
                    style={{
                        fontFamily: fonts.REGULAR,
                        fontSize: 13,
                        color: '#155897',
                        marginTop: 3,
                    }}>
                    {item.quantity + ' ' + units}
                </Text>
            </View>
            <View style={{ flex: 0.25, justifyContent: 'center', padding: 2 }}>
                <Text
                    style={{ fontFamily: fonts.REGULAR, fontSize: 13, color: '#7F7F7F' }}>
                    {item.mfgDate ? formatDateDDMMYYYY(item.mfgDate) : ''}
                </Text>
                <Text
                    style={{
                        fontFamily: fonts.REGULAR,
                        fontSize: 13,
                        color: '#7F7F7F',
                        marginTop: 3,
                    }}>
                    {item.expDate ? formatDateDDMMYYYY(item.expDate) : ''}
                </Text>
            </View>
        </View>
    );
};

export const AddNewProductOrderComponent = props => {
    const { t, i18n } = useTranslation();
    const { addProductInList, selectedProductType, productListByType = [] } = props;
    const [isMandatory, setIsMandatory] = useState(false);

    useEffect(() => {
        productForm.resetForm();
        if (productListByType.length && productListByType.length === 1) {
            const data = {
                value: productListByType[0]._id,
                eachItem: productListByType[0],
            };
            changeAfterSelectProduct(data);
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedProductType]);
    var validationSchema = Yup.object().shape(
        {
            productId: Yup.string()
                .min(1, t('Too Short!'))
                .max(256, t('Too Lonng!'))
                .required(t('Required')),
            quantity: Yup.number()
                .positive(t('Quantity must be greater than 0'))
                .integer(t('Quantity must be an integer'))
                .required(t('Required')),
        },
        ['productId', 'quantity'],
    ); // <-- HERE!!!!!!!!
    const productForm = useFormik({
        initialValues: {
            productId: '',
            quantity: '',
            units: '',
        },
        validationSchema,
        onSubmit: (values, actions) => {
            handleSubmit({ ...values });
        },
    });
    const _addproduct = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        productForm.handleSubmit();
    };

    const handleSubmit = async values => {
        setIsMandatory(false);

        addProductInList(values);
        // productForm.resetForm()
        ToastShow(
            values.productName + t(' Added succesfully'),
            'success',
            'long',
            'top',
        );
        productForm.handleChange({ target: { name: 'quantity', value: '' } });
    };
    const incrementQuantity = () => {
        let Qty = Number(productForm?.values?.quantity) + 1;
        productForm.handleChange({ target: { name: 'quantity', value: Qty } });
    };

    const decrementQuantity = () => {
        if (productForm?.values?.quantity > 1) {
            let Qty = Number(productForm?.values?.quantity) - 1;
            productForm.handleChange({ target: { name: 'quantity', value: Qty } });
        }
    };

    const handleInputChange = text => {
        // Validate input (allow only positive integers)
        const value = parseInt(text, 10);
        if (!isNaN(value) && value > 0) {
            productForm.handleChange({ target: { name: 'quantity', value: value } });
        } else if (text === '') {
            // Clear the input if it becomes empty
            productForm.handleChange({ target: { name: 'quantity', value: '' } });
        }
    };
    const eachQty = item => {
        return (
            <TouchableOpacity
                onPress={() => handleInputChange(item)}
                style={{
                    padding: scale(7),
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor: Colors.whiteFF,
                }}>
                <Text>{item + ''}</Text>
            </TouchableOpacity>
        );
    };

    const changeAfterSelectProduct = item => {
        const { value, eachItem } = item;

        productForm.handleChange({ target: { name: 'productId', value: value } });
        productForm.handleChange({
            target: { name: 'productName', value: eachItem.name },
        });
        productForm.handleChange({ target: { name: 'units', value: eachItem.units } });
    };
    return (
        <View>
            <View style={{ margin: 10 }}>
                <DropDown
                    dropdownData={productListByType}
                    onChangeValue={changeAfterSelectProduct}
                    label="name"
                    mapKey="_id"
                    val={productForm?.values?.productId}
                    placeholder={t('Select Product')}
                    errorMsg={productForm?.errors?.productId}
                    mandatory={isMandatory}
                />
            </View>
            <View style={{ backgroundColor: '#CFE7DE' }}>
                <View style={{ margin: 10 }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            marginBottom: 10,
                        }}>
                        <View style={{ flex: 25, marginLeft: 5, marginTop: 20 }}>
                            <Text
                                style={{
                                    flexDirection: 'row',
                                    fontFamily: fonts.BOLD,
                                    fontSize: 15,
                                }}>
                                {t('Quantity')}
                                <Text style={{ color: 'red' }}>*</Text>
                            </Text>
                        </View>
                        <View style={{ flex: 75, justifyContent: 'center', marginRight: 20 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={decrementQuantity}
                                    style={{ justifyContent: 'center' }}>
                                    <AntDesign name={'minus'} color={'#000'} size={18} />
                                </TouchableOpacity>
                                <View style={{ width: '90%' }}>
                                    <InputField
                                        placeholder={t('Quantity')}
                                        inputValue={productForm?.values?.quantity + ''}
                                        setInputValue={handleInputChange}
                                        KeyboardType={'numeric'}
                                    />
                                </View>
                                <TouchableOpacity
                                    onPress={incrementQuantity}
                                    style={{ justifyContent: 'center' }}>
                                    <AntDesign name={'plus'} color={'#000'} size={18} />
                                </TouchableOpacity>
                            </View>
                            {isMandatory && productForm?.errors?.quantity && (
                                <Text
                                    style={{
                                        color: 'red',
                                        marginTop: 7,
                                        fontSize: 12,
                                        fontFamily: fonts.MEDIUM,
                                        marginLeft: 5,
                                        marginBottom: 5,
                                    }}>
                                    {productForm?.errors?.quantity}
                                </Text>
                            )}

                            <View
                                style={{
                                    flexDirection: 'row',
                                    flexWrap: 'wrap',
                                }}>
                                {defaultQty.map(each => eachQty(each))}
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                <Text />
                <CustomButton
                    bgColor={'#208196'}
                    buttonName={t('Confirm')}
                    navigation={props.navigation}
                    onPressButton={_addproduct}
                />
            </View>
        </View>
    );
};
export const EditOrderProductComponent = props => {
    const { t, i18n } = useTranslation();
    const { updateProductInList, selectedPrd = {} } = props;
    const [isMandatory, setIsMandatory] = useState(false);

    useEffect(() => {
        productForm.handleChange({
            target: { name: 'quantity', value: selectedPrd.quantity + '' },
        });

        productForm.handleChange({
            target: { name: 'productId', value: selectedPrd.productId },
        });
        productForm.handleChange({
            target: { name: 'units', value: selectedPrd.units },
        });
        productForm.handleChange({
            target: { name: 'productName', value: selectedPrd.productName },
        });
        productForm.handleChange({ target: { name: 'type', value: selectedPrd.type } });
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedPrd]);

    var validationSchema = Yup.object().shape(
        {
            productId: Yup.string()
                .min(1, t('Too Short!'))
                .max(256, t('Too Lonng!'))
                .required(t('Required')),
            quantity: Yup.number()
                .positive(t('Quantity must be greater than 0'))
                .integer(t('Quantity must be an integer'))
                .required(t('Required')),
        },
        ['productId', 'quantity'],
    ); // <-- HERE!!!!!!!!
    const productForm = useFormik({
        initialValues: {
            productId: '',
            quantity: '',

            units: '',
        },
        validationSchema,
        onSubmit: (values, actions) => {
            handleSubmit({ ...values });
        },
    });
    const _updateproduct = () => {
        Keyboard.dismiss();
        setIsMandatory(true);
        productForm.handleSubmit();
    };

    const handleSubmit = async values => {
        console.log('handleSubmit values', values);
        setIsMandatory(false);

        updateProductInList(values);
        productForm.resetForm();
        ToastShow(
            values.productName + t(' updated succesfully'),
            'success',
            'long',
            'top',
        );
        productForm.handleChange({ target: { name: 'quantity', value: '' } });
    };
    const incrementQuantity = () => {
        let Qty = Number(productForm?.values?.quantity) + 1;
        productForm.handleChange({ target: { name: 'quantity', value: Qty } });
    };

    const decrementQuantity = () => {
        if (productForm?.values?.quantity > 1) {
            let Qty = Number(productForm?.values?.quantity) - 1;
            productForm.handleChange({ target: { name: 'quantity', value: Qty } });
        }
    };

    const handleInputChange = text => {
        // Validate input (allow only positive integers)
        const value = parseInt(text, 10);
        if (!isNaN(value) && value > 0) {
            productForm.handleChange({ target: { name: 'quantity', value: value } });
        } else if (text === '') {
            // Clear the input if it becomes empty
            productForm.handleChange({ target: { name: 'quantity', value: '' } });
        }
    };
    const eachQty = item => {
        return (
            <TouchableOpacity
                onPress={() => handleInputChange(item)}
                style={{
                    padding: scale(7),
                    borderWidth: 0.5,
                    borderColor: Colors.grayC5,
                    margin: 5,
                    borderRadius: 12,
                    backgroundColor: Colors.whiteFF,
                }}>
                <Text>{item + ''}</Text>
            </TouchableOpacity>
        );
    };

    return (
        <View>
            <View style={{ backgroundColor: '#CFE7DE' }}>
                <Text
                    style={{
                        fontFamily: fonts.B,
                        fontSize: 15,
                        textAlign: 'center',
                        marginTop: 10,
                    }}>
                    {t('Update Qty')}
                </Text>
                <View style={{ margin: 10 }}>
                    <InputField
                        placeholder={t('Product Name')}
                        label={t('Product Name')}
                        inputValue={productForm?.values?.productName}
                        setInputValue={text => {
                            productForm.handleChange({
                                target: { name: 'productName', value: text },
                            });
                        }}
                        inputStyle={{ marginBottom: 10 }}
                        labelStyle={{ marginBottom: 5 }}
                        errorMsg={productForm?.errors?.productName}
                        mandatory={isMandatory}
                        disable={true}
                    />
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            marginBottom: 10,
                        }}>
                        <View style={{ flex: 25, marginLeft: 5, marginTop: 20 }}>
                            <Text
                                style={{
                                    flexDirection: 'row',
                                    fontFamily: fonts.BOLD,
                                    fontSize: 15,
                                }}>
                                {t('Quantity')}
                                <Text style={{ color: 'red' }}>*</Text>
                            </Text>
                            {productForm?.values?.units && (
                                <Text
                                    style={{
                                        fontFamily: fonts.MEDIUM,
                                        fontSize: 12,
                                        marginTop: 5,
                                    }}>
                                    {'(' + productForm?.values?.units + ')'}
                                </Text>
                            )}
                        </View>
                        <View style={{ flex: 75, justifyContent: 'center', marginRight: 20 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                                <TouchableOpacity
                                    onPress={decrementQuantity}
                                    style={{ justifyContent: 'center' }}>
                                    <AntDesign name={'minus'} color={'#000'} size={18} />
                                </TouchableOpacity>
                                <View style={{ width: '90%' }}>
                                    <InputField
                                        placeholder={t('Quantity')}
                                        inputValue={productForm?.values?.quantity + ''}
                                        setInputValue={handleInputChange}
                                        KeyboardType={'numeric'}
                                    />
                                </View>
                                <TouchableOpacity
                                    onPress={incrementQuantity}
                                    style={{ justifyContent: 'center' }}>
                                    <AntDesign name={'plus'} color={'#000'} size={18} />
                                </TouchableOpacity>
                            </View>
                            {isMandatory && productForm?.errors?.quantity && (
                                <Text
                                    style={{
                                        color: 'red',
                                        marginTop: 7,
                                        fontSize: 12,
                                        fontFamily: fonts.MEDIUM,
                                        marginLeft: 5,
                                        marginBottom: 5,
                                    }}>
                                    {productForm?.errors?.quantity}
                                </Text>
                            )}

                            <View
                                style={{
                                    flexDirection: 'row',
                                    flexWrap: 'wrap',
                                }}>
                                {defaultQty.map(each => eachQty(each))}
                            </View>
                        </View>
                    </View>
                </View>
            </View>
            <View
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                }}>
                <Text />
                <CustomButton
                    bgColor={'#208196'}
                    buttonName={t('Confirm')}
                    navigation={props.navigation}
                    onPressButton={_updateproduct}
                />
            </View>
        </View>
    );
};

export const ReviewOrderHeader = props => {
    const { t } = useTranslation();

    return (
        <View
            style={{
                flexDirection: 'row',
                backgroundColor: '#E9ECF2',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.7, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13, marginBottom: 2 }}>
                    {t('Product Category')}
                </Text>

                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>
                    {t('Product Name')}
                </Text>
            </View>
            <View style={{ flex: 0.3, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13 }}>{t('Quantity')}</Text>
            </View>
        </View>
    );
};
export const ReviewOrderProductList = props => {
    const { item = {}, AddQty, editProduct, index } = props;
    const { units = '' } = item;
    const [isRefresh, setIsRefresh] = useState(false);

    useEffect(() => { }, [isRefresh]);
    return (
        <View
            style={{
                flexDirection: 'row',
                borderBottomWidth: 0.5,
                borderColor: '#7F7F7F',
                justifyContent: 'center',
                paddingLeft: 10,
                paddingRight: 10,
                padding: 5,
            }}>
            <View style={{ flex: 0.7, justifyContent: 'center', padding: 2 }}>
                <Text style={{ fontFamily: fonts.BOLD, fontSize: 13, marginBottom: 3 }}>
                    {item.type}
                </Text>

                <Text style={{ fontFamily: fonts.MEDIUM, fontSize: 13 }}>
                    {item.productName}
                </Text>
            </View>
            <View style={{ flex: 0.3, justifyContent: 'center', padding: 2 }}>
                <Text
                    style={{
                        fontFamily: fonts.REGULAR,
                        fontSize: 13,
                        color: '#155897',
                        marginTop: 5,
                    }}>
                    {item.quantity + ' ' + units}
                </Text>
            </View>
            {AddQty && (
                <TouchableOpacity
                    style={{ flex: 0.15, justifyContent: 'center' }}
                    onPress={() => {
                        editProduct(item, index);
                        setIsRefresh(!isRefresh);
                    }}>
                    <MaterialCommunityIcons
                        name={'square-edit-outline'}
                        color={'#7F7F7F'}
                        size={20}
                    />
                </TouchableOpacity>
            )}
        </View>
    );
};
