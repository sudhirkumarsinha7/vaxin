/* eslint-disable prettier/prettier */
export function filterAndMoveToFront(array, propertyName, selectedValue) {
    // Find the selected object based on the specified property value
    const selectedObject = array.find(obj => obj[propertyName] === selectedValue);

    // If the selected object is not found, return the array as is
    if (!selectedObject) {
        return array;
    }

    // Remove the selected object from its current position
    const filteredArray = array.filter(
        obj => obj[propertyName] !== selectedValue,
    );

    // Place the selected object at the beginning of the array
    return [selectedObject, ...filteredArray];
}
export function pushDataIntoInnerArray(array, targetObjectKey, newData) {
    // Find the object with the specified key

    const targetObject = array.find(obj => obj.type === targetObjectKey);
    // If the target object is found, push the new data into its inner array
    if (targetObject) {
        targetObject.products.push(newData);
    }

    return array;
}
export function addObjectToExistingObject(existingObject, newKey, newValue) {
    // Add or update the property with the new object
    const newProductType = {
        type: newKey,
        products: [newValue],
    };
    existingObject.push(newProductType);

    return existingObject;
}
export function mergeArraysConcat(array1, array2) {
    return array1.concat(array2);
}

export function deleteInnerObjectData(arrayOfObjects, targetIndex, innerIndex) {
    // Make a shallow copy of the array to avoid modifying the original
    const newArray = [...arrayOfObjects];

    // Check if the target index is valid
    if (targetIndex >= 0 && targetIndex < newArray.length) {
        // Create a shallow copy of the target object
        const targetObject = { ...newArray[targetIndex] };

        // Check if the inner index is valid
        if (innerIndex >= 0 && innerIndex < targetObject.products.length) {
            // Use splice to remove the inner object data from the inner array
            targetObject.products.splice(innerIndex, 1);

            // Replace the original object with the modified object
            newArray[targetIndex] = targetObject;
        }
    }

    return newArray;
}
export function deleteObjectsWithEmptyInnerArray(array) {
    // Use filter to create a new array excluding objects with an empty inner array
    const newArray = array.filter(obj => obj.products.length > 0);

    return newArray;
}
export function groupAndStoreByCategory(array, categoryKey) {
    return array.reduce((result, item) => {
        const categoryValue = item[categoryKey];

        // Check if the category exists in the result object
        if (!(categoryValue in result)) {
            // If not, create a new object with the inner array
            result[categoryValue] = { type: categoryValue, productsByType: [] };
        }

        // Push the current item to the inner array of the corresponding category
        result[categoryValue].productsByType.push(item);

        return result;
    }, {});
}
export function mergeIntoProductList(arrayOfObjects) {
    // Use the spread operator to merge inner arrays
    return [].concat(...arrayOfObjects.map(obj => obj.products));
}
export function removeEmptyValues(obj) {
    return Object.keys(obj).reduce((acc, key) => {
        const value = obj[key];

        // Check if the value is not null, undefined, or an empty string
        if (value !== null && value !== undefined && value !== '') {
            acc[key] = value;
        }

        return acc;
    }, {});
}
export function groupByToArray(array, key) {
    const grouped = array.reduce((result, obj) => {
        const keyValue = obj[key];

        // Check if the key value exists in the result object
        if (!result[keyValue]) {
            // If not, create a new array for that key value
            result[keyValue] = [];
        }

        // Push the current object to the array of the corresponding key value
        result[keyValue].push(obj);

        return result;
    }, {});

    // Convert the result object to an array of key-value pairs
    const resultArray = Object.entries(grouped).map(([key, value]) => ({
        key,
        value,
    }));

    return resultArray;
}
export function filterUniqueObjectsByProperty(array, propertyKey) {
    const uniqueKeys = new Set();
    return array.filter(obj => {
        if (!uniqueKeys.has(obj[propertyKey])) {
            uniqueKeys.add(obj[propertyKey]);
            return true;
        }
        return false;
    });
}
