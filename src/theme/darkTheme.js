const Colors = {
    primary: 'lightblue',
    text: 'white',
    inputBackground: 'gray',
    card:'#333333',
    card1:'#454545',
    tabUnselected:'#454545',
  }
  
  const NavigationColors = {
    primary: Colors.primary,
  }
  
  export default {
    Colors,
    NavigationColors,
  }