/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect, useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    ImageBackground,
    StatusBar,
    Animated,
} from 'react-native';
import { verticalScale } from '../components/Scale';
import VersionNumber from 'react-native-version-number';
import fonts from '../../FontFamily';
import { DeviceHeight, localImage } from '../config/global';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

const ImageLoader = props => {
    const opacity = new Animated.Value(0);

    const onLoad = () => {
        Animated.timing(opacity, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }).start();
    };

    return (
        <Animated.Image
            onLoad={onLoad}
            {...props}
            style={[
                {
                    opacity: opacity,
                    transform: [
                        {
                            scale: opacity.interpolate({
                                inputRange: [0, 1],
                                outputRange: [0.85, 1],
                            }),
                        },
                    ],
                },
                props.style,
            ]}
        />
    );
};

const SplashScreen = props => {
    const [currentFlaour, setCurrentFlaour] = useState('VAXIN');
    const insets = useSafeAreaInsets();

    useEffect(() => {
        async function navigaeScreen() {
            const flavor = await AsyncStorage.getItem('productFlavour');
            setCurrentFlaour(flavor);
            setTimeout(() => {
                props.navigation.navigate('AuthLoading');
            }, 2000);
        }
        navigaeScreen();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
    return (
        <ImageBackground
            style={styles.imgBackground}
            resizeMode="cover"
            source={localImage.backgraound}>
            <StatusBar hidden />

            <StatusBar hidden />

            <View style={styles.container}>
                <View style={{ alignItems: 'center' }}>
                    <ImageLoader
                        style={{
                            width: 200,
                            height: 120,
                            paddingTop: DeviceHeight / 2.5,
                        }}
                        source={
                            currentFlaour === 'FARVIEW'
                                ? localImage.farviewLogo
                                : localImage.applogo
                        }
                        resizeMode="contain"
                    />
                </View>

                <View
                    style={{
                        alignItems: 'center',
                        justifyContent: 'flex-end',
                        paddingTop: DeviceHeight / 4,
                    }}>
                    <View style={{ alignItems: 'center' }}>
                        <Text
                            style={{
                                fontSize: 14,
                                // color: 'white',
                                fontFamily: fonts.BOLD,
                            }}>
                            Powered By
                        </Text>

                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: verticalScale(8),
                            }}>
                            <Text style={styles.greenText}>S T A T</Text>
                            <Text style={styles.whiteText}> W I G</Text>
                        </View>
                    </View>

                    <Text
                        style={{
                            // color: 'white',
                            fontSize: 16,
                            marginTop: verticalScale(8),
                            fontFamily: fonts.BOLD,
                        }}>
                        Version {VersionNumber.appVersion}
                    </Text>
                    <Text
                        style={{
                            // color: 'white',
                            fontSize: 14,
                            marginTop: verticalScale(8),
                            fontFamily: fonts.REGULAR,
                        }}>
                        © 2022 StaTwig. All rights reserved.
                    </Text>
                </View>
            </View>
        </ImageBackground>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'transparent',
    },
    imgBackground: {
        width: '100%',
        height: '100%',
        flex: 1,
    },

    greenText: {
        color: 'green',
        fontSize: 24,
        fontWeight: fonts.BOLD,
    },
    whiteText: {
        // color: 'white',
        fontSize: 24,
        fontWeight: fonts.BOLD,
    },
});
export default SplashScreen;
