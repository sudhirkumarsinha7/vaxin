/* eslint-disable prettier/prettier */
import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from './en.json';
import hi from './hi.json';
import te from './te.json';
import bn from './bn.json';

i18n.use(initReactI18next).init({
    resources: {
        en: en,
        hi: hi,
        te: te,
        bn: bn,
    },
    lng: 'en',
    fallbackLng: 'en',
    compatibilityJSON: 'v3',

    interpolation: {
        escapeValue: false,
    },
});


export default i18n;
