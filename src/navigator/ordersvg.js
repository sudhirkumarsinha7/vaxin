import Svg, { G, Path, Line } from "react-native-svg";
/* SVGR has dropped some elements not supported by react-native-svg: style */
const SVGComponent = (props) => (
    <Svg
        id="Layer_1"
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        x="0px"
        y="0px"
        viewBox="0 0 24 24"
        style={{
            enableBackground: "new 0 0 24 24",
        }}
        xmlSpace="preserve"
        {...props}
    >
        <G>
            <Path
                className="st0"
                d="M22.52,21.81H5.47c-0.31,0-0.56-0.25-0.56-0.56V4.2c0-0.31,0.25-0.56,0.56-0.56h17.05 c0.31,0,0.56,0.25,0.56,0.56v17.05C23.08,21.56,22.83,21.81,22.52,21.81z"
            />
            <Line className="st0" x1={5.19} y1={7.79} x2={23.08} y2={7.79} />
            <Line className="st0" x1={18.84} y1={5.16} x2={18.84} y2={2.19} />
            <Line className="st0" x1={14} y1={5.16} x2={14} y2={2.19} />
            <Line className="st0" x1={9.07} y1={5.16} x2={9.07} y2={2.19} />
            <Path
                className="st0"
                d="M4.94,3.67c0,0,0.85,10.25-4.14,15.46c7.21,0,18.04,0,18.04,0s4.17-4.28,4.24-9.35c0-2.27,0-5.83,0-5.83"
            />
        </G>
    </Svg>
);
export default SVGComponent