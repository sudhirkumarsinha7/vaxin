/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable prettier/prettier */
/* eslint-disable no-shadow */
import React, { useEffect } from 'react';
import { SafeAreaView, StatusBar } from 'react-native';

import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from '@react-navigation/native';
import MainNavigator from './MainNavigator';
import { useTheme } from '../theme/Hooks';
import { navigationRef } from './utils';
import { useDispatch } from 'react-redux';
import { changeTheme } from '../redux/reducer/slice.reducer';

export default function Navigation() {
  const dispatch = useDispatch();
  const { NavigationTheme, darkMode } = useTheme();
  const onChangeTheme = ({ theme, darkMode }) => {
    dispatch(changeTheme({ theme, darkMode }));
  };
  useEffect(() => {
    onChangeTheme({ darkMode: false });
  }, [darkMode]);
  return (
    <NavigationContainer theme={NavigationTheme} ref={navigationRef}>
      <StatusBar barStyle={'light-content'} />

      {/* <StatusBar barStyle={darkMode ? 'light-content' : 'dark-content'} /> */}
      <MainNavigator />
    </NavigationContainer>
  );
}
