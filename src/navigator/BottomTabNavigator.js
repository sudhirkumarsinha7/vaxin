/* eslint-disable no-shadow */
/* eslint-disable react/no-unstable-nested-components */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import React from 'react';
import { Text, View, Image } from 'react-native';
import { localImage, } from '../config/global';
import HomeStack from '../screens/home/stack';
import OrderStack from '../screens/order/stack';
import InventoryStack from '../screens/inventory/stack';
import ShipmentStack from '../screens/shipment/stack';
import { useTranslation } from 'react-i18next';

// import VarStack from '../screens/VAR/stack';
import fonts from '../../FontFamily';
import {
  StackActions,
} from '@react-navigation/native';
import { scale } from '../components/Scale';
import { Colors } from '../components/Common/Style';
import HomeSvg from '../assets/img/home.svg'
import InventorySvg from '../assets/img/inventory.svg'
import OrderSvg from '../assets/img/order.svg'
import ShipmentSvg from '../assets/img/shipment.svg'
import GrayDashboard from '../assets/GraySvg/Dashboard.svg'
import GrayOrders from '../assets/GraySvg/Orders.svg'
import GrayShipment from '../assets/GraySvg/Shipment.svg'
import GrayInventory from '../assets/GraySvg/Inventory.svg'

const BottomTabNavigator = ({ navigation, route }) => {
  const Tab = createBottomTabNavigator();
  const { t } = useTranslation();


  const tabPressListener = props => {
    const { navigation } = props;
    return {
      blur: e => {
        const target = e.target;
        const state = navigation.getState();
        const route = state.routes.find(r => r.key === target);
        // If we are leaving a tab that has its own stack navigation, then clear it
        console.log('route ' + JSON.stringify(route));
        if (route.state?.type === 'stack' && route.state.routes?.length > 1) {
          navigation.dispatch(StackActions.popToTop());
        }
      },
      // Log the state for debug only
      state: e => {
        const state = navigation.getState();
        console.log('state', state);
      },
    };
  };
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: '#fff',
          height: scale(60),
        },
      }}
      initialRouteName="home">
      <Tab.Screen
        name="home"
        component={HomeStack}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ focused }) => (
            <View style={{ height: '100%', marginTop: 12 }}>
              <View
                style={{
                  backgroundColor: focused ? '#ede6f7' : 'transparent',
                  width: scale(30),
                  height: scale(30),
                  borderRadius: scale(50),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {focused ? <HomeSvg height={scale(15)} width={scale(15)} />
                  : <GrayDashboard height={scale(15)} width={scale(15)} />}

              </View>
              <Text
                style={{
                  color: focused ? Colors.blueD2 : '#D6D6D6',
                  fontSize: scale(8),
                  fontFamily: fonts.BOLD,
                  textAlign: 'center',
                }}>
                {t('HOME')}
              </Text>
            </View>
          ),
          tabBarIconStyle: {
            width: '100%',
          },
        }}
        listeners={props => tabPressListener({ ...props })}
      />
      <Tab.Screen
        name="order"
        component={OrderStack}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ focused }) => (
            <View style={{ height: '100%', marginTop: 12 }}>
              <View
                style={{
                  backgroundColor: focused ? '#ede6f7' : 'transparent',
                  width: scale(30),
                  height: scale(30),
                  borderRadius: scale(50),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {focused ? <OrderSvg height={scale(15)} width={scale(15)} /> : <GrayOrders height={scale(15)} width={scale(15)} />}


              </View>
              <Text
                style={{
                  color: focused ? Colors.blueD2 : '#D6D6D6',
                  fontSize: scale(8),
                  fontFamily: fonts.BOLD,
                  textAlign: 'center',
                }}>
                {t('ORDER')}
              </Text>
            </View>
          ),
          tabBarIconStyle: {
            width: '100%',
          },
        }}
        listeners={props => tabPressListener({ ...props })}
      />

      <Tab.Screen
        name="inventrory"
        component={InventoryStack}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ focused }) => (
            <View style={{ height: '100%', marginTop: 12 }}>
              <View
                style={{
                  backgroundColor: focused ? '#ede6f7' : 'transparent',
                  width: scale(30),
                  height: scale(30),
                  borderRadius: scale(50),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                {focused ? <InventorySvg height={scale(15)} width={scale(15)} />
                  : <GrayInventory height={scale(15)} width={scale(15)} />}


              </View>
              <Text
                style={{
                  color: focused ? Colors.blueD2 : '#D6D6D6',
                  fontSize: scale(8),
                  fontFamily: fonts.BOLD,
                  textAlign: 'center',
                }}>
                {t('INVENTORY')}
              </Text>
            </View>
          ),
          tabBarIconStyle: {
            width: '100%',
          },
        }}
        listeners={props => tabPressListener({ ...props })}
      />
      <Tab.Screen
        name="shipment"
        component={ShipmentStack}
        options={{
          tabBarLabel: '',
          tabBarIcon: ({ focused }) => (
            <View style={{ height: '100%', marginTop: 12 }}>
              <View
                style={{
                  backgroundColor: focused ? '#ede6f7' : 'transparent',
                  width: scale(30),
                  height: scale(30),
                  borderRadius: scale(50),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>


                {focused ? <ShipmentSvg height={scale(20)} width={scale(15)} />
                  : <GrayShipment height={scale(20)} width={scale(15)} />}

              </View>
              <Text
                style={{
                  color: focused ? Colors.blueD2 : '#D6D6D6',
                  fontSize: scale(8),
                  fontFamily: fonts.BOLD,
                  textAlign: 'center',
                }}>
                {t('SHIPMENT')}
              </Text>
            </View>
          ),
          tabBarIconStyle: {
            width: '100%',
          },
        }}
        listeners={props => tabPressListener({ ...props })}
      />
    </Tab.Navigator>
  );
};
export default BottomTabNavigator;
