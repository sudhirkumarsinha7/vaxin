/* eslint-disable jsx-quotes */
/* eslint-disable prettier/prettier */
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {
  createDrawerNavigator,
} from '@react-navigation/drawer';
import * as React from 'react';

import Login from '../screens/login';
import Register from '../screens/register';
import loginWithM from '../screens/login/loginWithM';

import LoginDashboard from '../screens/login/loginDashboard';
import ResetPin from '../screens/login/forgetpassword';
import SetOtpScreen from '../screens/login/sendotp';
import ResetPassword from '../screens/login/resetpassword';

import RegisterDashboard from '../screens/register/registerDashboard';
import VerifyOtp from '../screens/register/verifyOtp';
import SetPassword from '../screens/register/setpassword';
import SpashScreen from '../SpashScreen';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';
import DrawerContent from '../screens/Drawer'
import BottomTabNavigator from './BottomTabNavigator';
import { DeviceWidth } from '../config/global';
const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();

const headerOptions = {
  headerShown: false,
};
export const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={headerOptions}
      initialRouteName="Login"

    >
      {/* <Stack.Screen name="SpashScreen" component={SpashScreen} options={{
        headerShown: false
      }} />
      <Stack.Screen name="AuthLoading" component={AuthLoadingScreen} options={{
        headerShown: false
      }} /> */}
      <Stack.Screen name="Login" component={Login} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="register" component={Register} options={{
        headerShown: false,
      }} />

      <Stack.Screen name="logindashboard" component={LoginDashboard} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="resetpin" component={ResetPin} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="setotp" component={SetOtpScreen} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="registerdashboard" component={RegisterDashboard} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="veryotp" component={VerifyOtp} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="setpass" component={SetPassword} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="resetpass" component={ResetPassword} options={{
        headerShown: false,
      }} />
      <Stack.Screen name="loginWithM" component={loginWithM} options={{
        headerShown: false
      }} />
    </Stack.Navigator>
  );
}

export function MainNavigator() {


  return (


    <Drawer.Navigator
      screenOptions={{
        headerShown: false,
        drawerStyle: {
          width: DeviceWidth / 1.5,
        },
        // gestureEnabled: false,
      }}
      // swipeEnabled={false}
      // gestureEnabled={false} // Enable swipe gesture for opening the drawer

      initialRouteName="SpashScreen"
      drawerContent={(props) => <DrawerContent {...props} />}

    >
      <Drawer.Screen name='SpashScreen' component={SpashScreen} options={{ headerShown: false }} />
      <Drawer.Screen name="Auth" component={AuthStack} options={{
        headerShown: false,
      }} />
      <Drawer.Screen name='AuthLoading' component={AuthLoadingScreen} options={{ headerShown: false }} />
      <Drawer.Screen
        options={{ headerShown: false }}
        name="main"
        component={BottomTabNavigator}
      />

    </Drawer.Navigator>
  );

}

export default MainNavigator
