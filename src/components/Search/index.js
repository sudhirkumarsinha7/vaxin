import React, { useCallback, useEffect, useRef, useState } from 'react'
import { View, Text, TextInput, FlatList, StyleSheet, TouchableOpacity, Image } from 'react-native';

import { Colors, DeviceHeight, DeviceWidth } from '../../components/Common/Style';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import Feather from 'react-native-vector-icons/Feather'
import { scale, verticalScale } from '../Scale';
import { config } from '../../config'
import { connect } from 'react-redux';
import { ToastShow } from '../Toast'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import fonts from '../../../FontFamily';
const AutocompleteSearch = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [suggestions, setSuggestions] = useState([]);

  const allSuggestions = ['Test1', 'Test2', 'Test3', 'Test4', 'Test6', 'Test7', 'Test9'];

  const handleSearch = (text) => {
    const filteredSuggestions = allSuggestions.filter((item) =>
      item.toLowerCase().includes(text.toLowerCase())
    );
    setSuggestions(filteredSuggestions);
    setSearchTerm(text);
  };

  const handleSelectSuggestion = (selectedItem) => {
    setSearchTerm(selectedItem);
    setSuggestions([]);
  };
  const handleCancel = () => {
    setSearchTerm('');
    setSuggestions([]);
  };
  return (
    <View
      style={{
        justifyContent: 'center',
        margin: 12,
        marginTop: -DeviceWidth / 6
      }}

    >
      <TextInput
        style={styles.input}
        placeholder="Search"
        value={searchTerm}
        onChangeText={handleSearch}
      />

      {searchTerm.length > 0 && (
        <TouchableOpacity style={styles.cancelButton} onPress={handleCancel}>
          <MaterialIcons name="cancel" size={24} color="black" />
        </TouchableOpacity>
      )}

      <FlatList
        contentContainerStyle={{ paddingBottom: 100 }}
        style={styles.dropdown}
        data={suggestions}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => handleSelectSuggestion(item)}>
            <Text style={styles.suggestion}>{item}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
  input: {
    // height: 40,
    // borderColor: 'gray',
    // borderWidth: 1,
    // paddingHorizontal: 10,
    // marginBottom: 10,
    // width: '100%',
    height: 40,
    borderColor: '#BCBCBD',
    borderWidth: 0.5,
    borderRadius: 12,
    paddingHorizontal: 8,
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  dropdown: {
    // position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    elevation: 4,
    zIndex: 1,
    maxHeight: 150,
    borderRadius: 12,

  },
  suggestion: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
  },
  cancelButton: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
});

export default AutocompleteSearch;