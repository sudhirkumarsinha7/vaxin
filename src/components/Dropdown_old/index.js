import React, { useCallback, useEffect, useRef, useState } from 'react'
import { View, Text, TextInput, FlatList, StyleSheet, TouchableOpacity, Image } from 'react-native';

import { Colors, DeviceHeight, DeviceWidth } from '../Common/Style';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import Feather from 'react-native-vector-icons/Feather'
import { scale, verticalScale } from '../Scale';
import { config } from '../../config'
import { connect } from 'react-redux';
import { ToastShow } from '../Toast'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
const dropdownData = ['Test1', 'Test2', 'Test3', 'Test4', 'Test6', 'Test7', 'Test9']
const AutoSearchDropdown = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [suggestions, setSuggestions] = useState(dropdownData);
  const [isDropdownVisible, setIsDropdownVisible] = useState(false);

  const allSuggestions = ['Test1', 'Test2', 'Test3', 'Test4', 'Test6', 'Test7', 'Test9'];

  const handleSearch = (text) => {
    setIsDropdownVisible(true);
    const filteredSuggestions = suggestions.filter((item) =>
      item.toLowerCase().includes(text.toLowerCase())
    );
    setSuggestions(filteredSuggestions);
    setSearchTerm(text);
  };

  const handleSelectSuggestion = (selectedItem) => {
    setSearchTerm(selectedItem);
    setIsDropdownVisible(false);

  };
  const handleCancel = () => {
    setSearchTerm('');
    setIsDropdownVisible(false);

  };
  return (
    <View
      style={{
        justifyContent: 'center',
        margin: 12,
      }}

    >
      <TextInput
        style={styles.input}
        placeholder="Search"
        value={searchTerm}
        onChangeText={handleSearch}
      />

      {/* {isDropdownVisible && (
        <TouchableOpacity style={styles.cancelButton} onPress={handleCancel}>
          <MaterialIcons name="cancel" size={24} color="black" />
        </TouchableOpacity>
      )} */}
      <TouchableOpacity style={styles.cancelButton} onPress={() => setIsDropdownVisible(!isDropdownVisible)}>
        <MaterialIcons name={isDropdownVisible ? "keyboard-arrow-down" : "keyboard-arrow-up"} size={24} color="black" />
      </TouchableOpacity>

      {
        isDropdownVisible && (<FlatList
          style={styles.dropdown}
          data={suggestions}
          keyExtractor={(item) => item}
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => handleSelectSuggestion(item)}>
              <Text style={styles.suggestion}>{item}</Text>
            </TouchableOpacity>
          )}
        />)
      }
    </View >
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
  },
  input: {
    height: 40,
    borderColor: '#BCBCBD',
    borderWidth: 0.5,
    borderRadius: 12,
    paddingHorizontal: 8,
    justifyContent: 'center',
    backgroundColor: 'white'
  },
  dropdown: {
    // position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    elevation: 4,
    zIndex: 1,
    maxHeight: 150,
    borderRadius: 12,

  },
  suggestion: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
  },
  cancelButton: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
});

// export default DropDown;


// import React, { useState } from 'react';
// import { View, Text, TouchableOpacity, TextInput, FlatList, StyleSheet } from 'react-native';

// const AutoSearchDropdown = (props) => {
//   const [isDropdownVisible, setIsDropdownVisible] = useState(false);
//   const [selectedOption, setSelectedOption] = useState(null);
//   const [searchQuery, setSearchQuery] = useState('');
//   const options = ['Test1', 'Test2', 'Test3', 'Test4', 'Test6', 'Test7', 'Test9'];

//   const toggleDropdown = () => {
//     setIsDropdownVisible(!isDropdownVisible);
//   };

//   const handleOptionPress = (option) => {
//     setSelectedOption(option);
//     setIsDropdownVisible(false);
//     setSearchQuery('');
//   };

//   const filteredOptions = options.filter(
//     (option) => option.toLowerCase().includes(searchQuery.toLowerCase())
//   );

//   const handleSearchChange = (text) => {
//     setSearchQuery(text);
//     setIsDropdownVisible(true);
//   };

//   return (
//     <View style={styles.container}>
//       <View style={styles.header}>
//         <TextInput
//           style={styles.searchInput}
//           placeholder="Search..."
//           value={searchQuery}
//           onChangeText={handleSearchChange}
//         />
//         <Text>{isDropdownVisible ? '👆' : '👇'}</Text>
//       </View>

//       {isDropdownVisible && (
//         <FlatList
contentContainerStyle = {{ paddingBottom: 100 }}
//           data={filteredOptions}
//           renderItem={({ item }) => (
//             <TouchableOpacity onPress={() => handleOptionPress(item)} style={styles.option}>
//               <Text>{item}</Text>
//             </TouchableOpacity>
//           )}
//           keyExtractor={(item) => item.toString()}
//         />
//       )}
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     position: 'relative',
//   },
//   header: {
//     flexDirection: 'row',
//     justifyContent: 'space-between',
//     alignItems: 'center',
//     padding: 10,
//     borderRadius: 5,
//     borderWidth: 1,
//     borderColor: 'gray',
//   },
//   searchInput: {
//     flex: 1,
//     marginRight: 10,
//   },
//   option: {
//     padding: 10,
//     borderBottomWidth: 1,
//     borderBottomColor: 'gray',
//   },
// });

export default AutoSearchDropdown;

// import React, { useState } from 'react';
// import { View, Text, TouchableOpacity, FlatList, Modal, StyleSheet } from 'react-native';

// const MultiSelectDropdown = () => {
//   const options = ['Option 1', 'Option 2', 'Option 3', 'Option 4', 'Option 5'];

//   const [selectedValues, setSelectedValues] = useState([]);
//   const [isDropdownVisible, setIsDropdownVisible] = useState(false);

//   const toggleDropdown = () => {
//     setIsDropdownVisible(!isDropdownVisible);
//   };

//   const handleOptionPress = (option) => {
//     if (selectedValues.includes(option)) {
//       // Remove option if already selected
//       setSelectedValues(selectedValues.filter((value) => value !== option));
//     } else {
//       // Add option if not selected
//       setSelectedValues([...selectedValues, option]);
//     }
//   };

//   const renderOptionItem = ({ item }) => (
//     <TouchableOpacity
//       style={[styles.optionItem, selectedValues.includes(item) && styles.selectedOption]}
//       onPress={() => handleOptionPress(item)}
//     >
//       <Text>{item}</Text>
//     </TouchableOpacity>
//   );

//   return (
//     <View style={styles.container}>
//       <TouchableOpacity onPress={toggleDropdown} style={styles.dropdownButton}>
//         <Text>Open Dropdown</Text>
//       </TouchableOpacity>

//       <Modal
//         transparent={true}
//         animationType="slide"
//         visible={isDropdownVisible}
//         onRequestClose={toggleDropdown}
//       >
//         <View style={styles.modalContainer}>
//           <FlatList
contentContainerStyle = {{ paddingBottom: 100 }}
//             data={options}
//             renderItem={renderOptionItem}
//             keyExtractor={(item) => item}
//             extraData={selectedValues}
//           />

//           <TouchableOpacity onPress={toggleDropdown} style={styles.closeButton}>
//             <Text>Close</Text>
//           </TouchableOpacity>
//         </View>
//       </Modal>

//       {selectedValues.length > 0 && (
//         <View style={styles.selectedValuesContainer}>
//           <Text>Selected: {selectedValues.join(', ')}</Text>
//         </View>
//       )}
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   dropdownButton: {
//     backgroundColor: 'white',
//     padding: 10,
//     borderRadius: 5,
//   },
//   modalContainer: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     backgroundColor: 'rgba(0, 0, 0, 0.5)',
//   },
//   optionItem: {
//     padding: 10,
//     borderBottomWidth: 1,
//     borderBottomColor: 'gray',
//   },
//   selectedOption: {
//     backgroundColor: 'red',
//     color: 'white',
//   },
//   closeButton: {
//     marginTop: 20,
//     backgroundColor: '#007BFF',
//     padding: 10,
//     borderRadius: 5,
//   },
//   selectedValuesContainer: {
//     marginTop: 20,
//   },
// });

// export default MultiSelectDropdown;