/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Image,
  Text,
} from 'react-native';
import { scale, verticalScale } from '../Scale';
import { useTranslation } from 'react-i18next';
import { localImage } from '../../config/global';
const Empty_Card = props => {
  const { t, i18n } = useTranslation();

  return (
    <View
      style={{
        backgroundColor: '#FFFFFF',
        borderRadius: 8,
        margin: 15,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 20,
      }}>
      <Text style={{ fontSize: 22, textAlign: 'center' }} numberOfLines={2}>
        {t('Looks like')}
      </Text>
      <Text
        style={{ fontSize: 22, width: scale(190), textAlign: 'center' }}
        numberOfLines={2}>
        {t(props.Text)}
      </Text>
      <Image
        style={{
          width: scale(100),
          height: scale(211),
          marginTop: verticalScale(11),
        }}
        resizeMode={'contain'}
        source={localImage.empty}
      />
    </View>
  );
};
export default Empty_Card;
