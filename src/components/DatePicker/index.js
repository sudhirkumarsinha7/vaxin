import React, { useState, useEffect } from 'react';
import { View, TextInput, TouchableOpacity, Text, StyleSheet } from 'react-native';
import DatePicker from 'react-native-date-picker';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { InputField, InputFielCalender } from '../Common/helper';
import { ToastShow } from '../Toast'
import { scale, moderateScale, verticalScale } from '../Scale';
import { useTranslation } from 'react-i18next';
import { formatDateDDMMYYYY, formatDateDDMMYYYYTime } from '../../Util/utils';
import fonts from '../../../FontFamily';

const DateInput = (props) => {
    const [date, setDate] = useState(new Date());
    const [openDatePicker, setOpenDatePicker] = useState(false);
    const { placeholder, inputValue, setInputValue } = props

    return (
        <View >
            <InputField
                placeholder={placeholder}
                inputStyle={{ marginBottom: 10, color: '#000' }}
                labelStyle={{ marginBottom: 5 }}
                // inputValue={date.toLocaleDateString()}
                inputValue={inputValue}

                rightIcon={
                    <FontAwesome
                        name="calendar"
                        size={24}
                        color={"#000"}
                        style={styles.inputIconStyle}
                        onPress={() => setOpenDatePicker(true)}
                    />
                }
            />
            <DatePicker
                modal
                theme={"light"}
                open={openDatePicker}
                date={date}
                mode="date"
                onConfirm={date => {
                    setOpenDatePicker(false);
                    setDate(date);
                    setInputValue(date)
                }}
                onCancel={() => {
                    setOpenDatePicker(false);
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 10,
    },
    dateButton: {
        height: 40,
        borderWidth: 1,
        borderColor: 'gray',
        justifyContent: 'center',
        paddingHorizontal: 10,

    },
    input: {
        backgroundColor: '#FFFFFF',
        borderColor: '#BCBCBC',
        borderWidth: 1,
        borderRadius: 12,
        height: 50,
        paddingHorizontal: 15,
        paddingTop: 15,
        flex: 1,
        justifyContent: 'center'
    },
    labelInput: { fontSize: 16, fontWeight: '500', color: '#090A0A' },
});

export default DateInput;

export const DatePickerComponent = (props) => {
    const { t, i18n } = useTranslation();
    const {
        label = '',
        onChange,
        val = '',
        minDate = '',
        isMfgDate = false,
        placeholder = '',
        errorMsg = '',
        mandatory = false,
        isMandatoryField = true,
        isMaxDate = false,
        disabled = false
    } = props;
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);
    const [newMinDate, setNewMinDate] = useState(new Date());
    const [maximumDate, setMaximumDate] = useState(new Date());

    useEffect(() => {
        if (minDate) {
            const CurrentYear = new Date(minDate).getFullYear();
            const CurrentMonth = new Date(minDate).getMonth()
            const CurrentDate = new Date(minDate).getDate()
            let newFormatDate = new Date(CurrentYear, CurrentMonth, (CurrentDate + 1), 23, 59, 59)
            setNewMinDate(newFormatDate)
        }
        if (isMaxDate) {
            const maxDate = new Date(); // Today's date
            maxDate.setMonth(maxDate.getMonth() - 6); // Set to 6 months ago
            setMaximumDate(maxDate)
        }

    }, []);


    const onValueChange = (newDate) => {

        const CurrentYear = new Date(newDate).getFullYear();
        const CurrentMonth = new Date(newDate).getMonth()
        const CurrentDate = new Date(newDate).getDate()
        let newFormatDate = new Date(CurrentYear, CurrentMonth, CurrentDate)
        setShow(false)
        if (minDate) {
            if ((new Date(minDate) <= new Date(newDate))) {
                let newFormatDate1 = new Date(CurrentYear, CurrentMonth, CurrentDate, 23, 59, 59)

                // console.log('newFormatDate1  ',newFormatDate1)

                onChange(newFormatDate1)
                // onChange(newDate)
            } else {
                ToastShow(
                    t('Exp/Delivery Date should be greater than Mfg/shipping/Current Date'),
                    'error',
                    'long',
                    'top',
                )
            }
        } else {
            // onChange(newDate)
            onChange(newFormatDate)
        }
    }

    // console.log('DatePickerComponent show ' + show)
    return (
        <View>
            {/* <Text style={styles.labelStyle}>{t(label)}</Text> */}
            <InputFielCalender
                placeholder={placeholder}
                inputStyle={{ marginBottom: 10, color: '#000' }}
                labelStyle={{ marginBottom: 5 }}
                setInputValue={() => disabled ? console.log('') : setShow(true)}
                inputValue={val ? formatDateDDMMYYYY(val) : null}
                errorMsg={errorMsg}
                mandatory={mandatory}
                label={label}
                isMandatoryField={isMandatoryField}
                disabled={disabled}
            />
            {/* <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <TouchableOpacity style={styles.input} onPress={() => setShow(true)}>
                    <Text>{val ? formatDateDDMMYYYY(val) : placeholder}</Text>
                    <Text>{val}</Text>

                </TouchableOpacity>
            </View> */}

            {show && <DatePicker
                modal
                theme={"light"}
                open={show}
                mode="date"
                minimumDate={isMfgDate ? new Date(1900, 1) : minDate ? new Date(newMinDate) : date}
                maximumDate={isMfgDate ? isMaxDate && maximumDate ? maximumDate : new Date() : new Date(2100, 1)}
                date={val ? new Date(val) : isMfgDate ? isMaxDate && maximumDate ? maximumDate : date : newMinDate}
                onConfirm={(date) => {
                    onValueChange(date)
                }}
                onCancel={() => {
                    setShow(false)
                }}
            // androidVariant='nativeAndroid'

            />}

        </View>
    );
};
export const FreeDatePicker = (props) => {
    const { t, i18n } = useTranslation();
    const {
        label = '',
        onChange,
        val = '',
        minDate = '',
        placeholder = '',
        errorMsg = '',
        mandatory = false,
        isMandatoryField = true,
    } = props;
    const [date, setDate] = useState(new Date());
    const [show, setShow] = useState(false);




    const onValueChange = (newDate) => {

        const CurrentYear = new Date(newDate).getFullYear();
        const CurrentMonth = new Date(newDate).getMonth()
        const CurrentDate = new Date(newDate).getDate()
        let newFormatDate = new Date(CurrentYear, CurrentMonth, CurrentDate)
        setShow(false)
        if (minDate) {
            if ((new Date(minDate) <= new Date(newDate))) {
                let newFormatDate1 = new Date(CurrentYear, CurrentMonth, CurrentDate, 23, 59, 59)

                // console.log('newFormatDate1  ',newFormatDate1)

                onChange(newFormatDate1)
                // onChange(newDate)
            } else {
                ToastShow(
                    t('Exp/Delivery Date should be greater than Mfg/shipping/Current Date'),
                    'error',
                    'long',
                    'top',
                )
            }
        } else {
            // onChange(newDate)
            onChange(newFormatDate)
        }
    }

    return (
        <View>
            {/* <Text style={styles.labelStyle}>{t(label)}</Text> */}
            <InputFielCalender
                placeholder={placeholder}
                inputStyle={{ marginBottom: 10, color: '#000' }}
                labelStyle={{ marginBottom: 5 }}
                setInputValue={() => setShow(true)}
                inputValue={val ? formatDateDDMMYYYY(val) : null}
                errorMsg={errorMsg}
                mandatory={mandatory}
                label={label}
                isMandatoryField={isMandatoryField}
            />


            {show && <DatePicker
                modal
                theme={"light"}
                open={show}
                mode="date"
                date={date}
                onConfirm={(date) => {
                    onValueChange(date)
                }}
                onCancel={() => {
                    setShow(false)
                }}
            // androidVariant='nativeAndroid'

            />}

        </View>
    );
};
export const TimePickerComponent = (props) => {
    const { t, i18n } = useTranslation();
    const {
        label = '',
        onChange,
        val = '',
        minDate = '',
        maxDate = '',
        isMfgDate = false,
        placeholder = '',
        errorMsg = '',
        mandatory = false
    } = props;
    const [selectedTime, setSelectedTime] = useState(null);
    const [show, setShow] = useState(false);



    const onValueChange = (time) => {
        console.log('onValueChange newDate ' + time)
        setSelectedTime(time);
        setShow(false)
        // const CurrentYear = new Date(newDate).getFullYear();
        // const CurrentMonth = new Date(newDate).getMonth()
        // const CurrentDate = new Date(newDate).getDate()
        // let newFormatDate = new Date(CurrentYear, CurrentMonth, CurrentDate)
        // setShow(false)
        // if (minDate) {
        //     if ((new Date(minDate) <= new Date(newDate))) {
        //         let newFormatDate1 = new Date(CurrentYear, CurrentMonth, CurrentDate, 23, 59, 59)

        //         // console.log('newFormatDate1  ',newFormatDate1)

        //         onChange(newFormatDate1)
        //         // onChange(newDate)
        //     } else {
        //         ToastShow(
        //             t('Exp Date should be greater than Mfg Date/Current Date'),
        //             'error',
        //             'long',
        //             'top',
        //         )
        //     }
        // } else {
        //     // onChange(newDate)
        //     onChange(newFormatDate)
        // }
    }

    // console.log('show ' + label + ' ' + show)
    return (
        <View>
            {/* <Text style={styles.labelStyle}>{t(label)}</Text> */}
            <InputFielCalender
                placeholder={placeholder}
                inputStyle={{ marginBottom: 10, color: '#000' }}
                labelStyle={{ marginBottom: 5 }}
                setInputValue={() => setShow(true)}
                // inputValue={val ? formatDateDDMMYYYY(val) : null}
                // inputValue={selectedTime}
                // inputValue={selectedTime.toLocaleTimeString()}
                inputValue={selectedTime ? selectedTime.toLocaleTimeString() : null}

                errorMsg={errorMsg}
                mandatory={mandatory}
                label={label}

            />


            {show && <DatePicker
                modal
                theme={"light"}
                open={show}
                mode="time"
                date={new Date()}
                onConfirm={(date) => {
                    onValueChange(date)
                }}
                onCancel={() => {
                    setShow(false)
                }}
            />}



        </View>
    );
};
export const DateTimePickerComponent = (props) => {
    const { t, i18n } = useTranslation();
    const {
        label = '',
        onChange,
        val = '',
        minDate = '',
        maxDate = '',
        isMfgDate = false,
        placeholder = '',
        errorMsg = '',
        mandatory = false
    } = props;
    const [selectedTime, setSelectedTime] = useState(null);
    const [show, setShow] = useState(false);



    const onValueChange = (time) => {
        console.log('onValueChange newDate ' + time)
        // setSelectedTime(time);
        onChange(time)
        setShow(false)

    }

    // console.log('show ' + label + ' ' + show)
    return (
        <View>
            {/* <Text style={styles.labelStyle}>{t(label)}</Text> */}
            <InputFielCalender
                placeholder={placeholder}
                inputStyle={{ marginBottom: 10, color: '#000' }}
                labelStyle={{ marginBottom: 5 }}
                setInputValue={() => setShow(true)}
                // inputValue={val ? formatDateDDMMYYYY(val) : null}
                // inputValue={selectedTime}
                // inputValue={selectedTime.toLocaleTimeString()}
                inputValue={val ? formatDateDDMMYYYYTime(val) : null}

                errorMsg={errorMsg}
                mandatory={mandatory}
                label={label}

            />


            {show && <DatePicker
                modal
                theme={"light"}
                open={show}
                mode="datetime"
                date={new Date()}
                onConfirm={(date) => {
                    onValueChange(date)
                }}
                onCancel={() => {
                    setShow(false)
                }}
            />}



        </View>
    );
};



