/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */

import React, { useState, useEffect } from 'react';
import {
  Image,
  Text,
  TouchableOpacity,
  View,
  ImageBackground
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { localImage } from '../../config/global';
import { scale, verticalScale } from '../Scale';
import { useTheme } from '../../theme/Hooks';
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Colors } from '../Common/Style';
import HamburgerIcon from '../HamburgerIcon';
import fonts from '../../../FontFamily';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Badge } from '@rneui/base'
import Applogo from '../../assets/VaxinWithlogo.svg'
import NotificationSvg from '../../assets/img/Notifications.svg'

const Header = props => {
  useTranslation();
  const insets = useSafeAreaInsets();

  const getImageDetails = async () => {
    const { userInfo = {} } = props;
    // await props.fetchProfileImage(userInfo.photoId)
  };

  const {
    userInfo = {},
    newAlert = 0,
    newTransaction = 0,
    activeWareHouse = 0,
    userProfPic = '',
  } = props;
  let {
    warehouses = [],
    firstName = '',
    lastName = '',
    orgLevel = '',
  } = userInfo;
  warehouses = warehouses.filter(each => {
    if (each.status == 'ACTIVE') {
      return each;
    }
  });
  let activeWh = warehouses.length ? warehouses[activeWareHouse] : {};
  let { warehouseAddress = {} } = activeWh;
  const notificationCount = newAlert + newTransaction;
  return (
    // <View
    //   style={{
    //     backgroundColor: Colors.headerBackground,
    //     paddingVertical: scale(15),
    //     // height: DeviceHeight / 7
    //   }}>
    <ImageBackground
      style={{
        // backgroundColor: Colors.headerBackground,
        paddingVertical: scale(15),
        // height: DeviceHeight / 7
      }}
      resizeMode="stretch"
      source={localImage.backgraoundHeader}>


      <View
        style={{
          paddingTop: insets.top,
          marginLeft: scale(16),
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <HamburgerIcon
          getDetails={() => getImageDetails()}
          navigation={props.navigation}
          userProfPic={userProfPic}
        />
        <View style={{ marginLeft: scale(23), flex: props.scan ? 0.65 : 0.8 }}>
          <Text
            style={{
              fontSize: 18,
              fontFamily: fonts.BOLD,
              color: Colors.headerText,
            }}
            numberOfLines={1}
            ellipsizeMode="tail">
            {firstName + ' ' + lastName}
          </Text>
          <Text
            style={{
              fontSize: 14,
              color: Colors.headerText,
              fontFamily: fonts.REGULAR,
              marginTop: 5,
            }}
            numberOfLines={2}
            ellipsizeMode="tail">
            {orgLevel}
          </Text>
        </View>
        <View
          style={{
            justifyContent: 'flex-end',
            flex: props.scan ? 0.35 : 0.2,
            flexDirection: 'row',
            marginRight: scale(15),
          }}>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('notification')}
            style={{
              right: scale(18),
              // position: 'absolute',
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            {/* <MaterialCommunityIcons name="bell" color="#000" size={25} /> */}
            <NotificationSvg height={30} width={20} />

            {notificationCount ? (
              <Badge
                value={notificationCount + ''}
                status="error"
                containerStyle={{
                  marginTop: verticalScale(-8),
                  marginLeft: verticalScale(-10),
                }}
              />
            ) : null}
          </TouchableOpacity>
          {/* {props.scan ? (
            <TouchableOpacity onPress={props.onPress}>
              <MaterialCommunityIcons
                name="qrcode-scan"
                color="#000"
                size={20}
              />
            </TouchableOpacity>
          ) : null} */}
        </View>
      </View>
    </ImageBackground>
  );
};
function mapStateToProps(state) {
  return {
    // newAlert: state.alert.newAlert,
    // newTransaction: state.alert.newTransaction,
    userInfo: state.auth.userInfo,

    // activeWareHouse: state.userinfo.activeWareHouse,
    // userProfPic: state.userinfo.userProfPic,
  };
}

export default connect(mapStateToProps, {})(Header);
export const HeaderWithBack = props => {
  const { NavigationTheme } = useTheme();
  const { colors } = NavigationTheme;
  const insets = useSafeAreaInsets();
  const { back } = props;
  return (
    <View>
      {/* <View
        style={{
          paddingTop: insets.top,
          paddingBottom: scale(15),
          backgroundColor: Colors.headerBackground,
          marginbottom: scale(15),
        }}> */}
      <ImageBackground
        style={{
          paddingTop: insets.top,
          paddingVertical: 15,
          // backgroundColor: Colors.headerBackground,
        }}
        resizeMode="stretch"
        source={localImage.backgraoundHeader}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ flex: 0.1, alignSelf: 'center' }}>
            <TouchableOpacity
              onPress={() => (back ? back() : props.navigation.goBack())}
              style={{
                width: scale(40),
                height: scale(40),
                justifyContent: 'center',
                alignItems: 'center',
                borderColor: '#E5E7EB',
              }}>
              <FontAwesome name="chevron-left" size={30} color={'#000'} />
            </TouchableOpacity>
          </View>
          <View style={{ flex: 0.8, justifyContent: 'center' }}>
            <Text
              style={{
                color: '#000',
                fontSize: 18,
                fontFamily: fonts.BOLD,
              }}>
              {props.name}{' '}
            </Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export const HeaderIcon = () => {
  const [currentFlaour, setCurrentFlaour] = useState('VAXIN');
  useEffect(() => {
    async function fetchData() {
      const flavor = await AsyncStorage.getItem('productFlavour');
      setCurrentFlaour(flavor);
    }
    fetchData();
  }, []);
  return (
    <View>
      <View style={{ alignSelf: 'center', marginVertical: 10 }}>
        {currentFlaour === 'FARVIEW' ? (
          <Image
            style={{ height: 40, width: 280 }}
            source={localImage.farviewLogo}
          />
        ) : (


          <Applogo height={140} width={250} />

        )}
      </View>
    </View>
  );
};
