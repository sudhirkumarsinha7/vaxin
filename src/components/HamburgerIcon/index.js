import React, { Component } from 'react';
import { NavigationContainer, useIsFocused } from '@react-navigation/native';
import { scale, moderateScale, verticalScale } from '../Scale';
import {
  TouchableOpacity,
  View,
  Text,
  Image,
  Dimensions,
  TouchableHighlight,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { CommonStyle, Colors, DeviceHeight } from '../Common/Style';
import { localImage } from '../../config/global';

import UserProfile from '../../assets/img/UserProfile.svg'

const HamburgerIcon = (props) => {
  const { userProfPic } = props
  return (
    <TouchableOpacity
      style={{
        width: 16,
        height: 16,

        justifyContent: 'center',
        alignItems: 'center',
      }}
      onPress={() => {
        // props.getDetails();
        props.navigation.toggleDrawer();
      }}>
      <View style={{ height: 40, width: 40, borderRadius: 25, justifyContent: 'center', alignItems: 'center' }}>
        {userProfPic ? <Image
          style={{ width: 40, height: 40, borderRadius: 20 }}
          resizeMode="cover"
          source={{
            uri: userProfPic
          }}
        /> : <UserProfile height={40} width={40} />

        }
      </View>
      <View style={{ marginLeft: 25, marginTop: -20, backgroundColor: Colors.whiteFF, height: 30, width: 30, borderRadius: 15, justifyContent: 'center', alignItems: 'center' }}>
        <MaterialCommunityIcons name="menu" color={'#000'} size={18} />
      </View>
    </TouchableOpacity>
  );
}
export default HamburgerIcon;
