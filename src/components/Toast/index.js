// Common toast component 
import React, { Component, useState } from 'react';
import {
  Image,
} from 'react-native';
import Toast from 'react-native-root-toast';
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Entypo from 'react-native-vector-icons/Entypo'
export const WarningIcon = () => {
  return (
    <MaterialIcons name={'error'} color={'#FFAB1D'} size={20} />

  );
};
export const SuccessIcon = () => {
  return (
    <AntDesign name={'checkcircle'} color={'green'} size={20} />
  );
};
export const FailIcon = () => {
  return (
    <Entypo name={'circle-with-cross'} color={'red'} size={20} />
  );
};
//For showing the toast
const ToastShow = (message, type, duration, position) => {
  Toast.show(message, {
    duration: duration == 'long' ? Toast.durations.LONG : Toast.durations.SHORT,
    position: position == 'bottom' ? Toast.positions.BOTTOM : Toast.positions.TOP + 50,
    shadow: true,
    animation: true,
    hideOnPress: true,
    delay: 0,
    textColor:type == 'error' ? 'red' : type == 'warning' ? '#FFAB1D' : 'green',
    opacity: 1,
    backgroundColor: 'white',
    CustomIcon : type == 'error' ? <FailIcon/> : type == 'warning' ? <WarningIcon/>  : <SuccessIcon/> ,
  });
}

//For hiding the toast
const ToastHide = (props) => {
  setTimeout(function () {
    Toast.hide(ToastShow);
  }, 500);
}

//Export constant
export { ToastShow, ToastHide };