/* eslint-disable prettier/prettier */
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios';
import { config, getIosDeviceTokeApi, auth } from '../../config/config';
import { Platform } from 'react-native';
const selectPhotoFromGallary = async (setImage, hide, setbase64) => {
    ImagePicker.openPicker({
        width: 100,
        height: 50,
        cropping: true,
        // includeBase64: true
    }).then(async capturedImg => {
        setImage(capturedImg.path);

        const image = {
            uri:
                Platform.OS == 'ios'
                    ? capturedImg.path.replace('file://', '')
                    : capturedImg.path,
            type: 'image/jpeg',
            name: 'file2.jpg',
        };
        let formData = new FormData();
        formData.append('image', image);

        const configs = {
            headers: {
                'content-type': 'multipart/form-data',
            },
        };
        // console.log('selectPhotoFromGallary url', config().uploadImage);
        // console.log('selectPhotoFromGallary dta', formData);

        let response = await axios.post(config().uploadImage, formData, configs);
        let key = response && response.data && response.data.data;
        // console.log('selectPhotoFromGallary res', response);
        // console.log('selectPhotoFromGallary key', key);

        setbase64(key || 'key');
        hide();

        console.log(capturedImg);
    });
};

const takePhotoFromCamera = async (setImage, hide, setbase64) => {
    ImagePicker.openCamera({
        width: 100,
        height: 50,
        cropping: true,
        // includeBase64: true
    }).then(async capturedImg => {
        setImage(capturedImg.path);

        const image = {
            uri:
                Platform.OS == 'ios'
                    ? capturedImg.path.replace('file://', '')
                    : capturedImg.path,
            type: 'image/jpeg',
            name: 'file2.jpg',
        };
        let formData = new FormData();
        formData.append('image', image);

        const configs = {
            headers: {
                'content-type': 'multipart/form-data',
            },
        };
        // console.log('selectPhotoFromGallary url', config().uploadImage);
        // console.log('selectPhotoFromGallary dta', formData);

        let response = await axios.post(config().uploadImage, formData, configs);
        let key = response && response.data && response.data.data;
        // console.log('selectPhotoFromGallary res', response);
        // console.log('selectPhotoFromGallary key', key);

        setbase64(key || 'key');
        hide();
        console.log(capturedImg);
    });
};

export { selectPhotoFromGallary, takePhotoFromCamera };
