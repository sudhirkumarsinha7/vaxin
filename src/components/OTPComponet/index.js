import React, { useState } from 'react';
import { View, Text, ScrollView, ImageBackground, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { useTranslation } from 'react-i18next';
import { scale } from '../Scale'
import { CustomButton, CustomButtonLine, SubHeaderComponent, CustomTextLine } from '../Common/helper'
import { localImage } from '../../config/global';
import fonts from '../../../FontFamily';
const OTPComponent = (props) => {
    const { t, i18n } = useTranslation();

    const { handleBackspacePress, handleVerifyPress, handleNumberPress, handlePressbotomLine, otp, noOfCode = 4, headerText = '', headerLine = '', bottomline1 = '', bottomline2 = '', buttonName } = props
    const renderNumberButton = (number) => (
        <TouchableOpacity
            key={number}
            style={styles.keypadButton}
            onPress={() => number === 'X' ? handleBackspacePress() : handleNumberPress(number)}
        >
            <Text style={styles.keypadButtonText}>{number}</Text>
        </TouchableOpacity>
    );
    return (
        <View style={styles.container}>

            <View style={{ margin: scale(15), justifyContent: 'center', marginLeft: scale(25) }}>
                {headerText ? <SubHeaderComponent
                    name={headerText}
                // textColor={'white'}
                /> : null}
                {headerLine ? <CustomTextLine
                    name={headerLine}
                // textColor={'white'}
                /> : null}
            </View>
            <View style={styles.otpContainer}>

                {Array.from({ length: noOfCode }).map((_, index) => (
                    <View key={index} style={styles.otpDigitContainer}>
                        <Text style={styles.otpDigit}>{otp[index] || ''}</Text>
                    </View>
                ))}

            </View>
            <View style={{
                justifyContent: 'center',
                marginLeft: scale(15)
            }}>

                {bottomline2 ? <View style={{ flexDirection: 'row', marginLeft: scale(15), alignItems: 'center', marginTop: 12 }}>
                    <CustomTextLine
                        name={bottomline1}
                    // textColor={'white'}
                    />
                    <View style={{ marginTop: 10 }} />
                    <CustomButtonLine buttonName={bottomline2} onPressButton={handlePressbotomLine} bgColor={'#4DAED8'} />

                </View> : null}
                <CustomButton
                    bgColor={'#208196'}
                    buttonName={buttonName}
                    navigation={props.navigation}
                    onPressButton={handleVerifyPress}
                />
            </View>
            <View style={{ alignItems: 'center', marginTop: 15 }}>
                <FlatList
                    contentContainerStyle={{ paddingBottom: 100 }}
                    data={[1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 'X']}
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={3}

                    renderItem={({ item, index }) =>
                        renderNumberButton(item)
                    }
                />
            </View>
            {/* <View style={styles.keypadContainer}>
                {[1, 2, 3, 4, 5, 6, 7, 8, 9, 0].map((number) => (
                    <TouchableOpacity
                        key={number}
                        style={styles.keypadButton}
                        onPress={() => handleNumberPress(number)}
                    >
                        <Text style={styles.keypadButtonText}>{number}</Text>
                    </TouchableOpacity>
                ))}

                <TouchableOpacity
                    style={styles.keypadButton}
                    onPress={handleBackspacePress}
                >
                    <Image source={localImage.backSpace} />

                </TouchableOpacity>
            </View> */}


        </View>
    );
};

const styles = StyleSheet.create({
    imgBackground: {
        width: '100%',
        height: '100%',
    },
    container: {
        flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center',
    },
    title: {
        fontFamily: fonts.BOLD,

        // color: 'white',
        fontSize: scale(16)
    },
    otpContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
    },
    otpDigitContainer: {
        borderWidth: 1,
        // borderColor: 'white',
        width: scale(40),
        height: scale(40),
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        borderRadius: 12,
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.8,
        // shadowRadius: 2,
        // elevation: 5, // For Android
        // shadowColor: '#fff',
        // shadowColor: '#fff',


    },
    otpDigit: {
        fontSize: 18,
        // color: '#fff',
    },
    keypadContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center',
        margin: 20,
        borderRadius: scale(20),
        shadowOpacity: 0.7

    },
    keypadButton: {
        width: 80,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        // borderColor: 'white',
        margin: 5,
        borderRadius: scale(20),
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5, // For Android
    },
    keypadButtonText: {
        fontSize: 24,
        // color: 'white',
    },
    verifyButton: {
        backgroundColor: 'blue',
        paddingVertical: 10,
        paddingHorizontal: 20,
        borderRadius: 5,
    },
    verifyButtonText: {
        // color: 'white',
        fontSize: 18,
    },
});

export default OTPComponent;