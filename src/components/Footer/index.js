/* eslint-disable react-native/no-color-literals */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable sort-keys */
import React, { Component } from 'react';
import {
    BackHandler,
    Dimensions,
    Image,
    Platform,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { localImage } from '../../config/global';
import { DeviceHeight, DeviceWidth } from '../Common/Style';
import { moderateScale, scale, verticalScale } from '../Scale';
import { useTheme } from '../../theme/Hooks'
import { connect } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { Colors } from '../Common/Style';





export const Footer = (props) => {
    const handleBackPress = () => {
        // Handle back press as needed
        props.navigation.goBack();
    };
    return (
        <View style={styles.footer}>
            <TouchableOpacity style={styles.footerButton} onPress={handleBackPress}>
                <Image source={localImage.Fback} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.footerButton} disabled={true} onPress={handleBackPress}>
                <Image source={localImage.FCircle} />
            </TouchableOpacity>

            <TouchableOpacity style={styles.footerButton} disabled={true} onPress={handleBackPress}>
                <Image source={localImage.FSquare} />
            </TouchableOpacity>
        </View>

    );
}
const styles = StyleSheet.create({


    footer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'transparent', // Customize the background color
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: 56, // Customize the height as needed
    },
    footerButton: {
        flex: 1,
        alignItems: 'center',
        paddingVertical: 10,
    },
});