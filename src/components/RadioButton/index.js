/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { RenderLabel } from '../Common/helper';
import { useTranslation } from 'react-i18next';

const RadioButtonGroup = props => {
    const { t } = useTranslation();

    const [selectedOption, setSelectedOption] = useState(null);

    const handleOptionPress = val => {
        setSelectedOption(val);
        console.log('handleOptionPress setSelectedOption', val);
        onChangeValue(val);
    };

    const { options, label = '', val, onChangeValue } = props;
    return (
        <View style={{ marginTop: 10 }}>
            <RenderLabel title={label} color={'#000'} />

            <View
                style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                }}>
                {options.map(option => (
                    <View key={option.id} style={styles.radioButtonContainer}>
                        <TouchableOpacity
                            style={[
                                styles.radioButton,
                                val === option.id && styles.radioButtonSelected,
                            ]}
                            onPress={() => handleOptionPress(option.id)}>
                            {val === option.id && <View style={styles.innerCircle} />}
                        </TouchableOpacity>
                        <Text style={styles.label}>{t(option.label)}</Text>
                    </View>
                ))}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    radioButtonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
    },
    radioButton: {
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#208196',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
    },
    radioButtonSelected: {
        borderColor: '#208196',
        backgroundColor: '#fff', // Background color when selected
    },
    innerCircle: {
        width: 10,
        height: 10,
        borderRadius: 6,
        backgroundColor: '#208196',
    },
    label: {
        fontSize: 14,
        color: '#7F7F7F',
    },
});

// Example usage:
// const DynamicRadioButtons = () => {
//   const options = [
//     { id: 'option1', label: 'Option 1' },
//     { id: 'option2', label: 'Option 2' },
//     { id: 'option3', label: 'Option 3' },
//   ];

//   return <RadioButtonGroup options={options} />;
// };

export default RadioButtonGroup;
