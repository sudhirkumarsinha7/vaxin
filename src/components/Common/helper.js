/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useRef } from 'react';
import {
    Image,
    View,
    Text,
    TouchableOpacity,
    TextInput,
    StyleSheet,
    Platform,
} from 'react-native';
import { scale } from '../Scale';
import { Colors } from './Style';
// eslint-disable-next-line no-unused-vars
import _ from 'lodash';
import {
    Dropdown as ElDropdown,
} from 'react-native-element-dropdown';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Feather from 'react-native-vector-icons/Feather';

import { useTranslation } from 'react-i18next';


import fonts from '../../../FontFamily';
export const RenderLabel = props => {
    const { title = '', isMandatoryField = true } = props;
    return (
        <Text style={{ ...styles.label, color: props.color, marginBottom: 5 }}>
            {title} {isMandatoryField && <Text style={{ color: 'red' }}>*</Text>}
        </Text>
    );
};
export const CustomButton = props => {
    const {
        buttonName = '',
        onPressButton,
        image = '',
        bgColor = Colors.grayTextColor,
        textColor = Colors.whiteFF,
    } = props;
    return (
        <TouchableOpacity
            onPress={() => onPressButton()}
            style={{
                justifyContent: 'center',
                alignItems: 'center',
                margin: 10,
                backgroundColor: bgColor,
                padding: 8,
                paddingHorizontal: 18,
                borderRadius: 15,
                flexDirection: 'row',
            }}>
            {image ? (
                <Image
                    style={{ width: 16, height: 16, marginRight: 7 }}
                    source={image}
                    resizeMode="contain"
                />
            ) : null}
            <Text style={{ color: textColor, fontFamily: fonts.MEDIUM, fontSize: 16 }}>
                {buttonName}
            </Text>
        </TouchableOpacity>
    );
};
export const CustomButtonWithBorder = props => {
    const {
        bgColor = 'transparent',
        buttonName = '',
        onPressButton,
        bdColor = Colors.black0,
        image = '',
        textColor = Colors.black0,
        disabled = false,
    } = props;
    return (
        <TouchableOpacity
            disabled={disabled}
            onPress={() => onPressButton()}
            style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                margin: 12,
                borderColor: bdColor,
                borderWidth: 1,
                padding: 8,
                paddingHorizontal: 18,
                borderRadius: 15,
                backgroundColor: bgColor,
            }}>
            {image ? (
                <Image
                    style={{ width: 16, height: 16, marginRight: 7 }}
                    source={image}
                    resizeMode="contain"
                />
            ) : null}
            <Text style={{ color: textColor, fontFamily: fonts.MEDIUM, fontSize: 15 }}>
                {buttonName}
            </Text>
        </TouchableOpacity>
    );
};

export const CustomButtonLine = props => {
    const {
        buttonName = '',
        onPressButton,
        image = '',
        bgColor = Colors.grayTextColor,
    } = props;
    return (
        <TouchableOpacity
            onPress={() => onPressButton()}
            style={{ flexDirection: 'row', marginTop: 8, alignItems: 'center' }}>
            {image && (
                <Image
                    style={{ width: 16, height: 16, marginRight: 7 }}
                    source={image}
                    resizeMode="contain"
                />
            )}
            <Text style={{ color: bgColor, fontFamily: fonts.MEDIUM, fontSize: 16 }}>
                {buttonName}
            </Text>
        </TouchableOpacity>
    );
};

export const CustomTextLine = props => {
    const { name = '', textColor = '' } = props;
    return (
        <View style={{ marginTop: 8 }}>
            <Text
                style={{
                    color: textColor ? textColor : Colors.black0,
                    fontFamily: fonts.MEDIUM,
                    fontSize: 15,
                }}>
                {name}
            </Text>
        </View>
    );
};
export const DataSafeView = props => {
    return (
        <View style={{ marginTop: 12, flexDirection: 'row' }}>
            {/* <Image
            style={{ width: 16, height: 16, marginRight: 7 }}
            source={localImage.lock}
            resizeMode="contain"
        /> */}
            <Text
                style={{ color: Colors.garyA6, fontFamily: fonts.REGULAR, fontSize: 14 }}>
                {'Your Info is safely secured'}
            </Text>
        </View>
    );
};
export const StatusComponent = props => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <View
                style={{
                    backgroundColor: '#D9F0D6',
                    flexDirection: 'row',
                    padding: 7,
                    borderRadius: 7,
                    paddingLeft: 15,
                    paddingRight: 15,
                }}>
                <MaterialIcons
                    name={'check-circle-outline'}
                    size={20}
                    color={'#56BD4D'}
                />

                <Text
                    style={{
                        fontFamily: fonts.MEDIUM,
                        fontSize: 14,
                        marginLeft: 7,
                    }}>
                    {props.name}
                </Text>
            </View>
        </View>
    );
};
export const ErrorComponent = props => {
    return (
        <View>
            <Text
                style={{
                    color: 'red',
                    marginTop: 7,
                    fontSize: 12,
                    fontFamily: fonts.REGULAR,
                }}>
                {props.errorMsg}
            </Text>
        </View>
    );
};
export const SubHeaderComponent = props => {
    return (
        <View>
            <Text
                style={{
                    fontSize: 16,
                    color: props.textColor || Colors.black0,
                    fontFamily: fonts.MEDIUM,
                }}>
                {props.name}
            </Text>
        </View>
    );
};

export const SearchableComponent = props => {
    const [isFocus, setIsFocus] = useState(false);
    const {
        dropDowndata,
        label = '',
        displayName = '',
        mapKey = '',
        search = false,
        mandatory = false,
        val,
        disabled = false,
    } = props;

    return (
        <View style={styles.container}>
            <RenderLabel
                title={displayName}
                color={disabled ? '#646867' : Colors.black0}
            />
            <View>
                <ElDropdown
                    data={dropDowndata.map(eachElement => {
                        return {
                            value: eachElement[mapKey] || eachElement,
                            label: eachElement[label] || eachElement[mapKey] || eachElement,
                            eachItem: eachElement,
                        };
                    })}
                    search={search}
                    maxHeight={300}
                    disable={disabled}
                    labelField="label"
                    valueField="value"
                    searchPlaceholder="Search..."
                    style={[styles.dropdown, isFocus && { borderColor: Colors.black0 }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    value={val || null}
                    placeholder={displayName}
                    onFocus={() => setIsFocus(true)}
                    onChange={val => props.onChangeValue(val)}
                    searchable={search}
                />
            </View>
            {mandatory && !val ? <ErrorComponent errorMsg={'Required'} /> : null}
        </View>
    );
};

export const CustomTextViewVertical = props => {
    return (
        <View style={{ backgroundColor: Colors.whiteFF }}>
            <View style={{ padding: 10, borderRadius: 10 }}>
                <Text
                    style={{
                        fontSize: 16,
                        color: Colors.rightBlack,
                        fontFamily: fonts.BOLD,
                        marginBottom: 3,
                    }}>
                    {props.topText}
                </Text>
                <Text
                    style={{
                        fontSize: 14,
                        color: Colors.rightBlack,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {props.bottomText}
                </Text>
            </View>
        </View>
    );
};
export const CustomInputText = props => {
    const [isFocus, setIsFocus] = useState(false);
    const inputRef = useRef();

    const {
        label = '',
        mandatory = false,
        onChange,
        val,
        errorMsg = '',
        disabled = false,
        keyboardType = '',
        maxlenth = 100,
        placeholderText = '',
        islabel = true,
    } = props;

    return (
        <View style={styles.container}>
            {islabel ? (
                <RenderLabel
                    title={label}
                    color={disabled ? '#646867' : Colors.black0}
                />
            ) : null}
            <TextInput
                placeholder={placeholderText}
                placeholderTextColor="#a8a8a8"
                value={val}
                style={[styles.dropdown1, isFocus && { borderColor: Colors.gray9 }]}
                onChangeText={onChange}
                editable={!disabled}
                onFocus={() => setIsFocus(true)}
                maxLength={maxlenth}
                keyboardType={keyboardType}
                ref={inputRef}
            />
            {mandatory && errorMsg && (
                <Text
                    style={{
                        color: 'red',
                        marginTop: 7,
                        fontSize: 12,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};
export const InputField = props => {
    const { t, i18n } = useTranslation();
    const {
        label,
        placeholder,
        inputValue,
        setInputValue,
        inputStyle,
        labelStyle,
        secureTextEntry,
        errorMsg = '',
        mandatory = false,
        disable = false,
        KeyboardType = '',
        isMandatoryField = true,
        RightIcon,
    } = props;
    return (
        <View>
            {label && (
                <Text style={[styles.labelInput, labelStyle]}>
                    {t(label)}
                    {isMandatoryField && <Text style={{ color: 'red' }}>*</Text>}
                </Text>
            )}
            <View
                style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <TextInput
                    placeholder={t(placeholder)}
                    placeholderTextColor="#a8a8a8"
                    secureTextEntry={secureTextEntry || false}
                    onChangeText={setInputValue}
                    value={inputValue}
                    editable={!disable}
                    style={[styles.input, inputStyle, { color: disable ? "#a8a8a8" : '#000' }]}
                    keyboardType={KeyboardType}
                />
                {RightIcon && <RightIcon />}
            </View>
            {mandatory && errorMsg && (
                <Text
                    style={{
                        color: 'red',
                        marginTop: -5,
                        fontSize: 12,
                        fontFamily: fonts.MEDIUM,
                        marginLeft: 5,
                        marginBottom: 5,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};
export const CustomInputPassword = props => {
    const [showpass, setShowpass] = useState(false);
    const [isFocus, setIsFocus] = useState(false);

    const {
        label = '',
        mandatory = false,
        onChange,
        val,
        errorMsg = '',
        disabled = false,
        keyboardType = '',
        maxlenth = 100,
        placeholderText = '',
        islabel = true,
        inputStyle,
    } = props;

    return (
        <View style={{ ...styles.container }}>
            {islabel ? (
                <RenderLabel
                    title={label}
                    color={disabled ? '#646867' : Colors.black0}
                />
            ) : null}

            <View
                style={{
                    ...styles.dropdown,
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                <TextInput
                    placeholder={placeholderText}
                    placeholderTextColor="#a8a8a8"
                    onChangeText={onChange}
                    value={val}
                    style={{ flex: 0.95, color: Colors.black0 }}
                    secureTextEntry={!showpass}
                    onFocus={() => setIsFocus(true)}
                />
                <TouchableOpacity
                    style={{ alignItems: 'flex-end' }}
                    onPress={() => setShowpass(!showpass)}>
                    <MaterialCommunityIcons
                        name={showpass ? 'eye' : 'eye-off'}
                        size={25}
                        color={Colors.black0}
                    />
                </TouchableOpacity>
            </View>
            {mandatory && (
                <Text
                    style={{
                        color: 'red',
                        fontSize: 12,
                        marginTop: 7,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};
export const InputFielCalender = props => {
    const [showpass, setShowpass] = useState(false);
    const [isFocus, setIsFocus] = useState(false);
    const { t, i18n } = useTranslation();

    const {
        label,
        placeholder,
        inputValue,
        setInputValue,
        inputStyle,
        labelStyle,
        secureTextEntry,
        errorMsg = '',
        mandatory = false,
        disable = true,
        KeyboardType = '',
        isMandatoryField = true,
        disabled = false,

    } = props;

    return (
        <View style={{ ...styles.container }}>
            {label && (
                <Text style={[styles.labelInput, labelStyle]}>
                    {t(label)}
                    {isMandatoryField && <Text style={{ color: 'red' }}>*</Text>}
                </Text>
            )}

            <View
                style={{
                    ...styles.dropdown,
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: '#fff',
                    paddingVertical: Platform.OS === 'ios' ? 11 : 0,
                    marginBottom: 5,
                }}>
                <TextInput
                    placeholder={t(placeholder)}
                    placeholderTextColor="#a8a8a8"
                    secureTextEntry={secureTextEntry || false}
                    onChangeText={setInputValue}
                    value={inputValue}
                    editable={!disable}
                    style={{ flex: 0.95, color: disabled ? "#a8a8a8" : '#000' }}
                    keyboardType={KeyboardType}
                />
                {<TouchableOpacity
                    disabled={disabled}
                    style={{ alignItems: 'flex-end' }}
                    onPress={setInputValue}>
                    <Feather name={'calendar'} size={25} color={'#9C999F'} />
                </TouchableOpacity>}
            </View>
            {mandatory && (
                <Text
                    style={{
                        color: 'red',
                        fontSize: 12,
                        marginTop: 7,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};

export const CustomTextView = props => {
    return (
        <View style={{ flexDirection: 'row', margin: 5, marginLeft: 10 }}>
            <View style={{ flex: 0.4 }}>
                <Text
                    style={{
                        fontSize: 16,
                        color: Colors.leftgray,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {props.leftText}
                </Text>
            </View>
            <View style={{ flex: 0.6 }}>
                <Text
                    style={{
                        fontSize: 16,
                        color: Colors.rightBlack,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {props.rightText}
                </Text>
            </View>
        </View>
    );
};

export const CustomTextView1 = props => {
    return (
        <View style={{ flexDirection: 'row' }}>
            <View style={{ flex: 0.4 }}>
                <Text
                    style={{
                        fontSize: 10,
                        color: Colors.leftgray,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {props.leftText}
                </Text>
            </View>
            <View style={{ flex: 0.6, marginLeft: 5 }}>
                <Text
                    style={{
                        fontSize: 10,
                        color: Colors.rightBlack,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {props.rightText}
                </Text>
            </View>
        </View>
    );
};

export const CustomTextView2 = props => {
    return (
        <View style={{ flexDirection: 'row', margin: 5, marginLeft: 10 }}>
            <View style={{ flex: 0.5 }}>
                <Text
                    style={{
                        fontSize: 15,
                        color: Colors.black70,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {props.leftText}
                </Text>
            </View>
            <View style={{ flex: 0.5 }}>
                <Text
                    style={{
                        fontSize: 15,
                        color: Colors.rightBlack,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {props.rightText}
                </Text>
            </View>
        </View>
    );
};

export const CustomTextViewVerticalNew = props => {
    const { isDate = false } = props;
    return (
        <View style={{ margin: 5, marginLeft: 10 }}>
            <Text style={{ fontSize: 12, color: '#716D74', fontFamily: fonts.MEDIUM }}>
                {props.topText}
            </Text>
            <Text
                style={{
                    fontSize: isDate ? 10 : 14,
                    fontFamily: fonts.MEDIUM,
                    marginTop: 7,
                }}>
                {props.bottomText}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    content: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        flexShrink: 1,
    },
    container: {
        // backgroundColor: 'white',
        marginTop: 12,
    },
    dropdown: {
        // height: 40,
        borderColor: '#BCBCBD',
        borderWidth: 1,
        borderRadius: 15,
        paddingHorizontal: 8,
        paddingVertical: 11,
        color: Colors.whiteFF,
    },
    icon: {
        marginRight: 5,
    },
    label: {
        // position: 'absolute',
        // backgroundColor: 'white',
        // left: 22,
        // top: 8,
        // zIndex: 999,
        // paddingHorizontal: 8,
        fontSize: 14,
        color: 'white',
        marginBottom: 5,
        fontFamily: fonts.MEDIUM,
    },
    placeholderStyle: {
        color: 'white',
        fontSize: 14,
        fontFamily: fonts.MEDIUM,
    },
    selectedTextStyle: {
        fontSize: 14,
        fontFamily: fonts.MEDIUM,
        // color: Colors.blueFF
    },
    selectedTextMultiStyle: {
        fontSize: 14,
        fontFamily: fonts.MEDIUM,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 14,
        fontFamily: fonts.MEDIUM,
    },
    item: {
        padding: 17,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    selectedStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 14,
        backgroundColor: '#F3F5F7',
        shadowColor: '#000',
        marginTop: 8,
        marginRight: 12,
        paddingHorizontal: 12,
        paddingVertical: 8,
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,

        elevation: 2,
        borderColor: Colors.blue96,
        borderWidth: 1,
    },
    textSelectedStyle: {
        marginRight: 5,
        fontSize: 14,
        color: Colors.blue8E,
    },
    input: {
        backgroundColor: '#FFFFFF',
        borderColor: '#BCBCBC',
        borderWidth: 1,
        borderRadius: 12,
        paddingVertical: 11,
        paddingHorizontal: 15,
        flex: 1,
    },
    dropdown1: {
        borderColor: '#BCBCBD',
        borderWidth: 1,
        borderRadius: 15,
        paddingHorizontal: 8,
        paddingVertical: 14,
        // color: Colors.whiteFF,
    },
    labelInput: {
        fontSize: 14,
        fontFamily: fonts.MEDIUM,
        color: '#090A0A',
        flexDirection: 'row',
    },
});
