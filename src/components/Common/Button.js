import React, { useEffect, useState, useRef } from 'react';
import { Button, FlatList, Image, SafeAreaView, ScrollView, StyleSheet, Text, TextInput, TouchableOpacity, View, Platform, } from 'react-native';

import { Colors, DeviceWidth, CommonStyle } from './Style';
import { moderateScale, scale, verticalScale } from "../Scale";
import fonts from '../../../FontFamily';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { useTranslation } from 'react-i18next';
import { localImage } from '../../config/global';
import { formatNumber } from '../../Util/utils';


export const CardNavigationBtn = props => {
  const { title, image, type, onPressButton, bgColor = 'transparent' } = props;
  const { t, i18n } = useTranslation();

  return (
    <TouchableOpacity onPress={() => onPressButton()}
      style={{ height: 50, paddingVertical: 10, flexDirection: 'row', paddingHorizontal: 10, backgroundColor: bgColor }}>
      <View style={{ width: '90%', flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center', backgroundColor: '#ede6f7', borderRadius: 100 }}>
          {
            type == "icon" ? image :
              <Image source={image} style={{ height: 18, width: 18 }}
                resizeMode={'contain'} />
          }
        </View>
        <Text style={{ fontFamily: fonts.REGULAR, color: '#000', fontSize: 14, paddingLeft: 10 }}>{t(title)}</Text>
      </View>
      <View style={{ width: '10%', alignItems: 'center', justifyContent: 'center', }}>
        <Ionicons name={"chevron-forward"} size={20} color={'#888889'} />
      </View>
    </TouchableOpacity>
  )
}

export const CardNavigationBtnHome_old = props => {
  const { title, image, type, onPressButton, bgColor = 'transparent', textColor = '#000', descpription = '' } = props;
  const { t, i18n } = useTranslation();

  return (
    <TouchableOpacity onPress={() => onPressButton()}
      style={{ paddingVertical: 10, flexDirection: 'row', padding: 15, backgroundColor: bgColor, margin: 15, borderWidth: 1, borderColor: '#208196', borderRadius: 18 }}>
      <View style={{ flexDirection: 'row', alignItems: 'center', }}>
        <View style={{ height: 80, width: 80, alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff', borderRadius: 100, marginLeft: scale(-40), borderWidth: 1, borderColor: textColor }}>
          {
            type == "icon" ? image :
              <Image source={image} style={{ height: 40, width: 40 }} resizeMode={'contain'} />
          }
        </View>
        <View style={{ marginLeft: 22, marginRight: 20 }}>
          <Text style={{ fontFamily: fonts.BOLD, color: '#000', fontSize: 18, }}>{t(title)}</Text>
          <Text style={{ fontFamily: fonts.REGULAR, color: textColor, fontSize: 18, marginTop: 15 }}>{t(descpription)}</Text>

        </View>
      </View>

    </TouchableOpacity>
  )
}

export const CardNavigationBtnHome = props => {
  const { title, image, type, onPressButton, bgColor = 'transparent', textColor = '#000', val1 = '', val2 = '', subtitle1 = '', subtitle2 = '' } = props;
  const { t, i18n } = useTranslation();

  return (
    <TouchableOpacity onPress={() => onPressButton()}
      style={{ paddingVertical: 10, backgroundColor: bgColor, margin: 18, borderWidth: 1, borderColor: '#208196', borderRadius: 18, }}>
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ height: 60, width: 60, alignItems: 'center', justifyContent: 'center', backgroundColor: '#fff', borderRadius: 70, marginLeft: -20, borderWidth: 1, borderColor: textColor }}>
          {
            type == "icon" ? image :
              <Image source={image} style={{ height: 20, width: 20 }} resizeMode={'contain'} />
          }
        </View>
        <View style={{ alignItems: 'center' }}>
          <Text style={{ fontFamily: fonts.BOLD, color: '#000', fontSize: 18, textAlign: 'center' }}>{t(title)}</Text>
          <View style={{ flexDirection: 'row', marginTop: 10 }}>
            <View style={{ width: '50%' }}>
              <Text style={{ fontFamily: fonts.MEDIUM, color: textColor, fontSize: 16, textAlign: 'center' }}>{t(subtitle1)}</Text>
              <Text style={{ fontFamily: fonts.REGULAR, color: textColor, fontSize: 14, marginTop: 15, textAlign: 'center' }}>{formatNumber(val1)}</Text>
            </View>
            <View style={{}}>
              <Text style={{ fontFamily: fonts.MEDIUM, color: textColor, fontSize: 16, textAlign: 'center' }}>{t(subtitle2)}</Text>
              <Text style={{ fontFamily: fonts.REGULAR, color: textColor, fontSize: 14, marginTop: 15, textAlign: 'center' }}>{formatNumber(val2)}</Text>
            </View>
          </View>
        </View>

      </View>

    </TouchableOpacity>
  )
}
export const FilterCardButtons = props => {
  const { onPressButton, bgColor = 'transparent' } = props;
  const { t, i18n } = useTranslation();

  return (
    <TouchableOpacity onPress={() => onPressButton()}
      style={{ paddingVertical: 10, flexDirection: 'row', paddingHorizontal: 20, backgroundColor: bgColor }}>
      <View style={{ width: '90%', flexDirection: 'row', alignItems: 'center' }}>
        <Image source={localImage.filter} style={{ height: 15, width: 15 }} resizeMode={'contain'} />
        <Text style={{ fontFamily: fonts.REGULAR, color: '#000', fontSize: 16, paddingLeft: 10 }}>{t('Filter')}</Text>
      </View>

    </TouchableOpacity>
  )
}