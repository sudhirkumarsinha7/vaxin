/* eslint-disable quotes */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useRef } from 'react';
import { View, Text } from 'react-native';
import { useTranslation } from 'react-i18next';
import PhoneInput from 'react-native-phone-number-input';
import fonts from '../../../FontFamily';
import { RenderLabel } from './helper';
const PhoneComponent = props => {
    const { t } = useTranslation();

    const phoneInput = useRef(null);

    const {
        label,
        val,
        setCountryCode,
        isMandatoryField = false,
        onChangeFormattedText,
        onChange,
        errorMsg = '',
        mandatory = false,
        countryCode = 'IN',
    } = props;
    return (
        <View style={{ marginTop: 12 }}>
            <RenderLabel
                title={label}
                // color={'#fff'}
                isMandatoryField={isMandatoryField}
            />

            <View style={{ marginTop: 5 }}>
                <PhoneInput
                    ref={phoneInput}
                    defaultValue={val}
                    defaultCode={countryCode}
                    layout="first"
                    onChangeText={onChange}
                    onChangeFormattedText={onChangeFormattedText}
                    onChangeCountry={country => setCountryCode(country.cca2)}
                    containerStyle={{
                        borderRadius: 15,
                        // height: 50,
                        overflow: 'hidden',
                        backgroundcolor: '#a8a8a8',
                        borderWidth: 1,
                        borderColor: '#a8a8a8',
                        width: '100%',
                    }}
                    textInputStyle={{ color: '#000' }}
                    codeTextStyle={{ color: '#000' }}
                    phoneInputContainer={true}
                    placeholder={t("Enter mobile number")}
                    // containerStyle={styles.phoneInputContainer}
                    // textContainerStyle={styles.phoneInputTextContainer}
                    textInputProps={{ placeholderTextColor: '#a8a8a8', padding: 0 }}
                    textinputprops={{ color: '#000' }}
                    flagButtonStyle={{
                        color: '#a8a8a8',
                        borderRightColor: '#a8a8a8',
                        borderRightWidth: 3 / 2.5,
                    }}
                    autoFocus={false}
                />
            </View>
            {mandatory && (
                <Text
                    style={{
                        color: 'red',
                        fontSize: 12,
                        marginTop: 7,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};
export const PhoneComponentNew = props => {
    const { t } = useTranslation();

    const phoneInput = useRef(null);

    const {
        label,
        val,
        val1,
        setCountryCode,
        isMandatoryField = false,
        onChangeFormattedText,
        onChange,
        errorMsg = '',
        mandatory = false,
        countryCode = 'IN',
    } = props;
    return (
        <View style={{ marginTop: 12 }}>
            <RenderLabel
                title={label}
                color={'#000'}
                isMandatoryField={isMandatoryField}
            />

            <View style={{ marginTop: 5 }}>
                <PhoneInput
                    ref={phoneInput}
                    defaultValue={val}
                    defaultCode={countryCode}
                    layout="first"
                    onChangeText={onChange}
                    onChangeFormattedText={onChangeFormattedText}
                    onChangeCountry={country => setCountryCode(country.cca2)}
                    containerStyle={{
                        borderRadius: 15,
                        // height: 50,
                        overflow: 'hidden',
                        backgroundcolor: '#a8a8a8',
                        borderWidth: 1,
                        borderColor: '#a8a8a8',
                        width: '100%',
                    }}
                    textInputStyle={{ color: '#000' }}
                    codeTextStyle={{ color: '#000' }}
                    phoneInputContainer={true}
                    placeholder={t("Enter mobile number")}
                    textInputProps={{ placeholderTextColor: '#a8a8a8', padding: 0 }}
                    textinputprops={{ color: '#000' }}
                    flagButtonStyle={{
                        color: '#a8a8a8',
                        borderRightColor: '#a8a8a8',
                        borderRightWidth: 3 / 2.5,
                    }}
                    autoFocus={false}
                    value={val1}
                />
            </View>
            {mandatory && (
                <Text
                    style={{
                        color: 'red',
                        fontSize: 11,
                        marginTop: 7,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};
export default PhoneComponent;
