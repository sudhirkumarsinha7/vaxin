/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList } from 'react-native';
import { RenderLabel } from '../Common/helper';
import { useTranslation } from 'react-i18next';
import Checkbox from './checkbox'; // Adjust the import path as necessary
import fonts from '../../../FontFamily';

const CheckBox = props => {
    const { t } = useTranslation();


    const renderItem = ({ item }) => (
        <Checkbox
            label={item.label}
            checked={val.includes(item.label)}
            onPress={() => onChangeValue(item.label)}
        />
    );
    const { options, label = '', val, onChangeValue, mandatory = false, errorMsg = '' } = props;
    return (
        <View style={{ marginTop: 7 }}>
            <RenderLabel title={label} color={'#000'} />

            {/* <View
                style={{
                    marginTop: 10
                }}>
                <FlatList
                    data={options}
                    keyExtractor={item => item.id}
                    renderItem={renderItem}
                />
            </View> */}
            <View
                style={{
                    flexDirection: 'row',
                    flexWrap: 'wrap',
                }}>
                {options.map(item => (
                    <Checkbox
                        label={item.label}
                        checked={val.includes(item.label)}
                        onPress={() => onChangeValue(item.label)}
                    />
                ))}
            </View>
            {mandatory && errorMsg && (
                <Text
                    style={{
                        color: 'red',
                        marginTop: 5,
                        fontSize: 12,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};

const styles = StyleSheet.create({
    radioButtonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
    },
    radioButton: {
        width: 20,
        height: 20,
        borderRadius: 10,
        borderWidth: 2,
        borderColor: '#208196',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
    },
    radioButtonSelected: {
        borderColor: '#208196',
        backgroundColor: '#fff', // Background color when selected
    },
    innerCircle: {
        width: 10,
        height: 10,
        borderRadius: 6,
        backgroundColor: '#208196',
    },
    label: {
        fontSize: 14,
        color: '#7F7F7F',
    },
});

// Example usage:
// const DynamicRadioButtons = () => {
//   const options = [
//     { id: 'option1', label: 'Option 1' },
//     { id: 'option2', label: 'Option 2' },
//     { id: 'option3', label: 'Option 3' },
//   ];

//   return <CheckBox options={options} />;
// };

export default CheckBox;


// import React, { useState } from 'react';
// import { View, Text, FlatList, StyleSheet } from 'react-native';
// import Checkbox from './checkbox'; // Adjust the import path as necessary

// const options2 = [
//     { id: 'VVM', label: 'VVM' },
//     { id: 'Cold Chain Card', label: 'Cold Chain Card' },
//     { id: 'Electronic Device', label: 'Electronic Device' },
//     { id: 'ICE', label: 'Ice' },
// ];

// const CheckBox = (props) => {
//     const { val = [], onChangeValue } = props
//     // const [selectedItems, setSelectedItems] = useState([]);

//     // const handleCheckboxPress = (label) => {
//     //     if (selectedItems.includes(label)) {
//     //         setSelectedItems(selectedItems.filter(item => item !== label));
//     //     } else {
//     //         setSelectedItems([...selectedItems, label]);
//     //     }
//     // };

//     const renderItem = ({ item }) => (
//         <Checkbox
//             label={item.label}
//             checked={val.includes(item.label)}
//             onPress={() => onChangeValue(item.label)}
//         />
//     );

//     return (
//         <View style={styles.container}>
//             <FlatList
//                 data={options2}
//                 keyExtractor={item => item.id}
//                 renderItem={renderItem}
//             />
//             {/* <View style={styles.selectedItemsContainer}>
//                 <Text>Selected Items:</Text>
//                 {selectedItems.length === 0 ? (
//                     <Text>No items selected</Text>
//                 ) : (
//                     selectedItems.map(item => (
//                         <Text key={item}>{item}</Text>
//                     ))
//                 )}
//             </View> */}
//         </View>
//     );
// };

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         padding: 20,
//     },
//     selectedItemsContainer: {
//         marginTop: 20,
//     },
// });

// export default CheckBox;