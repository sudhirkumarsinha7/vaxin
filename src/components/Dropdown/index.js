/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
    View,
    Text,
    StyleSheet,
    Image
} from 'react-native';
import { scale } from '../Scale';
import { Colors } from '../Common/Style';
import fonts from '../../../FontFamily';
import {
    SelectList,
} from 'react-native-dropdown-select-list';
import { RenderLabel } from '../Common/helper';
import {
    Dropdown as ElDropdown,
} from 'react-native-element-dropdown';
import { ErrorComponent } from '../Common/helper';
import { useTranslation } from 'react-i18next';
import DropDownPicker from 'react-native-dropdown-picker';
import { localImage } from '../../config/global';
export const DropdownWithICon = props => {
    const [isFocus, setIsFocus] = useState(false);
    const { t } = useTranslation();
    const [open, setOpen] = useState(false);

    const {
        label = '',
        mapKey = '',
        mandatory = false,
        val,
        dropdownData,
        onChangeValue,
        labelText = '',
        search = true,
        disabled = false,
        isMandatoryField = true,
    } = props;


    return (
        <View >
            {/* {labelText && (
                <RenderLabel
                    title={labelText}
                    color={'#000'}
                    isMandatoryField={isMandatoryField}
                />
            )} */}


            <ElDropdown
                data={dropdownData}
                maxHeight={300}
                disable={disabled}
                labelField="label"
                valueField="value"
                searchPlaceholder="Search..."
                style={[styles.dropdown, isFocus && { borderColor: Colors.grayC5 }]}
                placeholderStyle={styles.placeholderStyle}
                selectedTextStyle={styles.selectedTextStyle}
                inputSearchStyle={styles.inputSearchStyle}
                value={val || null}
                placeholder={t('Select')}
                onFocus={() => setIsFocus(true)}
                onChange={val => onChangeValue(val)}
                renderItem={(item) => (
                    <View style={styles.item}>
                        {item.icon()}
                        <Text style={styles.textItem}>{item.label}</Text>
                    </View>
                )}
                renderLeftIcon={() => (
                    val ? dropdownData.find(item => item.value === val).icon() : <Image source={localImage.en} style={styles.icon} />
                )}


            />

        </View>
    );
};
// const CustomDropdown = (props) => {
//     const {
//         label = '',
//         mapKey = '',
//         mandatory = false,
//         val,
//         errorMsg = '',
//         placeholderText = '',
//         dropdownData,
//         onChangeValue,
//         changeAfterSelect,
//         labelText = '',
//         search = true

//     } = props;

//     return (
//         <View style={{ marginBottom: 10 }}>

//             {labelText && (<RenderLabel title={labelText} color={'#000'} />)}
//             <SelectList
//                 setSelected={(value) => onChangeValue(value)}
//                 data={dropdownData.map(eachElement => {
//                     return {
//                         key: eachElement[mapKey] || eachElement,
//                         value: eachElement[label] || eachElement[mapKey] || eachElement,
//                     };
//                 })}
//                 save="key"
//                 onSelect={() => changeAfterSelect(val)}
//                 placeholder={placeholderText}
//                 boxStyles={{
//                     height: scale(40),
//                     borderColor: '#BCBCBD',
//                     borderWidth: 1,
//                     borderRadius: 12,
//                     color: Colors.whiteFF
//                 }}
//                 search={search}

//             // defaultOption={{ key: val, }}   //default selected option

//             />
//             {mandatory && errorMsg && (<Text
//                 style={{
//                     color: 'red',
//                     marginTop: 7,
//                     fontSize: 12,
//                     fontFamily: fonts.MEDIUM
//                 }}>
//                 {errorMsg}
//             </Text>)}
//         </View>

//     )

// };
export const ManufacturerDropdown = props => {
    const [isFocus, setIsFocus] = useState(false);
    const { t } = useTranslation();

    const {
        mapKey = '',
        mandatory = false,
        val,
        dropdownData,
        onChangeValue,
        labelText = '',
        search = true,
        disabled = false,
        isMandatoryField = true,
    } = props;

    return (
        <View style={styles.container}>
            {labelText && (
                <RenderLabel
                    title={labelText}
                    color={'#000'}
                    isMandatoryField={isMandatoryField}
                />
            )}
            <View style={{ marginBottom: 10 }}>
                <ElDropdown
                    data={dropdownData.map(eachElement => {
                        return {
                            value: eachElement[mapKey] || eachElement,
                            label: eachElement.details && eachElement.details.name,
                            eachItem: eachElement,
                        };
                    })}
                    maxHeight={300}
                    disable={disabled}
                    labelField="label"
                    valueField="value"
                    searchPlaceholder="Search..."
                    style={[styles.dropdown, isFocus && { borderColor: Colors.black0 }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    value={val || null}
                    placeholder={t('Select')}
                    onFocus={() => setIsFocus(true)}
                    onChange={val => onChangeValue(val)}
                    search={search}
                />
            </View>
            {mandatory && !val ? <ErrorComponent errorMsg={t('Required')} /> : null}
        </View>
    );
};
export const CustomDropdown = props => {
    const [isFocus, setIsFocus] = useState(false);
    const { t } = useTranslation();

    const {
        label = '',
        mapKey = '',
        mandatory = false,
        val,
        dropdownData,
        onChangeValue,
        labelText = '',
        search = true,
        disabled = false,
        isMandatoryField = true,
    } = props;

    return (
        <View style={styles.container}>
            {labelText && (
                <RenderLabel
                    title={labelText}
                    color={'#000'}
                    isMandatoryField={isMandatoryField}
                />
            )}
            <View style={{ marginBottom: 10 }}>
                <ElDropdown
                    data={dropdownData.map(eachElement => {
                        return {
                            value: eachElement[mapKey] || eachElement,
                            label:
                                eachElement[label] || eachElement[mapKey] || eachElement + '',
                            eachItem: eachElement,
                        };
                    })}
                    maxHeight={300}
                    disable={disabled}
                    labelField="label"
                    valueField="value"
                    searchPlaceholder="Search..."
                    style={[styles.dropdown, isFocus && { borderColor: Colors.black0 }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    value={val || null}
                    placeholder={t('Select')}
                    onFocus={() => setIsFocus(true)}
                    onChange={val => onChangeValue(val)}
                    search={search}
                />
            </View>
            {mandatory && !val ? <ErrorComponent errorMsg={t('Required')} /> : null}
        </View>
    );
};
export const CampaignDropdown = props => {
    const [isFocus, setIsFocus] = useState(false);
    const { t } = useTranslation();

    const {
        mapKey = '',
        mandatory = false,
        val,
        dropdownData,
        onChangeValue,
        labelText = '',
        search = true,
        disabled = false,
        isMandatoryField = true,
    } = props;

    return (
        <View style={styles.container}>
            {labelText && (
                <RenderLabel
                    title={labelText}
                    color={'#000'}
                    isMandatoryField={isMandatoryField}
                />
            )}
            <View style={{ marginBottom: 10 }}>
                <ElDropdown
                    data={dropdownData.map(eachElement => {
                        return {
                            value: eachElement[mapKey] || eachElement,
                            label: eachElement.name + ' - ' + eachElement.id,
                            eachItem: eachElement,
                        };
                    })}
                    maxHeight={300}
                    disable={disabled}
                    labelField="label"
                    valueField="value"
                    searchPlaceholder="Search..."
                    style={[styles.dropdown, isFocus && { borderColor: Colors.black0 }]}
                    placeholderStyle={styles.placeholderStyle}
                    selectedTextStyle={styles.selectedTextStyle}
                    inputSearchStyle={styles.inputSearchStyle}
                    value={val || null}
                    placeholder={t('Select')}
                    onFocus={() => setIsFocus(true)}
                    onChange={val => onChangeValue(val)}
                    search={search}
                />
            </View>
            {mandatory && !val ? <ErrorComponent errorMsg={t('Required')} /> : null}
        </View>
    );
};
export const BatchProductDropdown = props => {
    const { t } = useTranslation();

    const {
        mandatory = false,
        val,
        errorMsg = '',
        placeholderText = '',
        dropdownData,
        onChangeValue,
        changeAfterSelect,
        labelText = '',
    } = props;

    return (
        <View style={{ marginBottom: 10 }}>
            {labelText && <RenderLabel title={labelText} color={'#000'} />}
            <SelectList
                setSelected={value => onChangeValue(value)}
                data={dropdownData.map(eachElement => {
                    return {
                        key: eachElement.productId._id,
                        value:
                            eachElement.productId.name + ' (' + eachElement.quantity + ')',
                    };
                })}
                save="key"
                onSelect={() => changeAfterSelect(val)}
                placeholder={placeholderText}
                boxStyles={{
                    height: scale(40),
                    borderColor: '#BCBCBD',
                    borderWidth: 1,
                    borderRadius: 12,
                    color: Colors.whiteFF,
                }}
            />
            {mandatory && errorMsg && (
                <Text
                    style={{
                        color: 'red',
                        marginTop: 7,
                        fontSize: 12,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};
export const DropDownComponent = props => {
    const [isFocus, setIsFocus] = useState(false);
    const { t } = useTranslation();

    const {
        label = '',
        mapKey = '',
        mandatory = false,
        val,
        errorMsg = '',
        disabled = false,
        labelText = '',
        dropdownData,
        onChangeValue,
    } = props;

    return (
        <View style={{ paddingTop: scale(15) }}>
            {labelText && <RenderLabel title={labelText} color={'#fff'} />}

            <ElDropdown
                data={dropdownData.map(eachElement => {
                    return {
                        value: eachElement[mapKey] || eachElement,
                        label: eachElement[label] || eachElement[mapKey] || eachElement,
                        eachItem: eachElement,
                    };
                })}
                maxHeight={300}
                disable={disabled}
                labelField="label"
                valueField="value"
                searchPlaceholder="Search..."
                style={[styles.dropdown, isFocus && { borderColor: Colors.whiteFF }]}
                placeholderStyle={{ color: '#fff' }}
                selectedTextStyle={{
                    // fontSize: 16,
                    color: Colors.whiteFF,
                }}
                value={val || null}
                placeholder={t('Select')}
                onFocus={() => setIsFocus(true)}
                onChange={val => onChangeValue(val)}
            />
            {mandatory && errorMsg && (
                <Text
                    style={{
                        color: 'red',
                        marginTop: 7,
                        fontSize: 12,
                        fontFamily: fonts.MEDIUM,
                    }}>
                    {errorMsg}
                </Text>
            )}
        </View>
    );
};
// export const CustomInputText = (props) => {
//     const [isFocus, setIsFocus] = useState(false);
//     const inputRef = useRef();

//     const {
//         label = '',
//         mandatory = false,
//         onChange,
//         val,
//         errorMsg = '',
//         disabled = false,
//         keyboardType = '',
//         maxlenth = 100,
//         placeholderText = '',
//         islabel = true
//     } = props;

//     return (

//         <View style={styles.container}>
//             {islabel ? <RenderLabel title={label} color={disabled ? '#646867' : Colors.whiteFF} /> : null}
//             <TextInput
//                 placeholder={placeholderText}
//                 placeholderTextColor={Colors.whiteFF}
//                 value={val}
//                 style={[styles.dropdown, isFocus && { borderColor: Colors.whiteFF }]}
//                 onChangeText={onChange}
//                 editable={!disabled}
//                 onFocus={() => setIsFocus(true)}
//                 maxLength={maxlenth}
//                 keyboardType={keyboardType}
//                 ref={inputRef}
//             />
//             {mandatory && errorMsg ? <Text
//                 style={{
//                     color: 'red',
//                     marginTop: 7,
//                     fontSize: 12,
//                     fontFamily: fonts.MEDIUM
//                 }}>
//                 {errorMsg}
//             </Text> : null}

//         </View>
//     );
// }

export default CustomDropdown;
const styles = StyleSheet.create({


    container: {
        // backgroundColor: 'white',
        marginTop: 14,
    },
    dropdown: {
        // height: scale(40),
        borderColor: '#BCBCBD',
        borderWidth: 1,
        borderRadius: 15,
        paddingHorizontal: 8,
        paddingVertical: 7,
        color: Colors.whiteFF,
    },
    icon: {
        marginRight: 5,
    },
    label: {
        fontSize: 14,
        fontFamily: fonts.MEDIUM,
        color: '#090A0A',
    },
    placeholderStyle: {
        color: '#a8a8a8',
        // fontSize: 16,
        // fontFamily: fonts.MEDIUM,
    },
    selectedTextStyle: {
        // fontSize: 16,
        // fontFamily: fonts.MEDIUM,
        // color: Colors.whiteFF
    },
    selectedTextMultiStyle: {
        fontSize: 14,
        fontFamily: fonts.MEDIUM,
    },
    iconStyle: {
        width: 20,
        height: 20,
    },
    inputSearchStyle: {
        height: 40,
        fontSize: 14,
        fontFamily: fonts.MEDIUM,
    },
    item: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    icon: {
        width: 24,
        height: 24,
        marginRight: 8,
    },
    textItem: {
        flex: 1,
        fontSize: 16,
    },
});
