/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
import React, { useState } from 'react';
import {
    View,
    Text,
    Modal,
    TouchableOpacity,
    StyleSheet,
    Image,
    Platform,
} from 'react-native';
import fonts from '../../../FontFamily';
import { scale, verticalScale } from '../Scale';
import { InputField } from '../Common/helper';
import { CustomButton, CustomButtonWithBorder } from '../Common/helper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { Colors, DeviceHeight, DeviceWidth } from '../Common/Style';
import { useTranslation } from 'react-i18next';
import { localImage } from '../../config/global';
import ErrorSvg from '../../assets/img/error.svg'

export const PopupMessage = props => {
    const {
        title = '',
        message = '',
        isVisible,
        onClose,
        buttonName,
        image = '',
    } = props;
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
            onRequestClose={() => onClose()}>
            <View style={styles.modalContainer}>
                <View style={styles.modalContent}>
                    {image && (
                        <Image
                            style={{ width: 50, height: 50, marginBottom: 20 }}
                            source={image}
                            resizeMode="contain"
                        />
                    )}
                    <Text
                        style={{
                            fontSize: 16,
                            fontFamily: fonts.MEDIUM,
                            textAlign: 'center',
                        }}>
                        {title}
                    </Text>

                    {message && (
                        <Text style={{ marginTop: 15, fontFamily: fonts.REGULAR }}>
                            {message}
                        </Text>
                    )}
                    <TouchableOpacity
                        onPress={() => onClose()}
                        style={{
                            backgroundColor: '#208196',
                            paddingLeft: 15,
                            paddingRight: 15,
                            padding: 8,
                            borderRadius: 15,
                            marginTop: 20,
                            marginBottom: 20,
                        }}>
                        <Text style={styles.closeButton}>{buttonName}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    );
};

export const AcceptShipmentPopup = props => {
    const { t, i18n } = useTranslation();

    const [shipmentID, setShipmentID] = useState('');
    const { title, message, isVisible, onClose, buttonName, image = '' } = props;
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
            onRequestClose={() => onClose()}>
            <View style={styles.modalContainer}>
                <View style={styles.modalContent1}>
                    <Text
                        style={{ fontSize: 16, fontFamily: fonts.REGULAR, marginBottom: 10 }}>
                        {'Enter Shipment ID or Scan Bar Code to accept Shipment'}
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'center',
                            alignContent: 'center',
                        }}>
                        <View style={{ flex: 0.8 }}>
                            <InputField
                                placeholder="Shipment ID"
                                inputValue={shipmentID}
                                setInputValue={setShipmentID}
                                inputStyle={{ marginBottom: 10 }}
                                labelStyle={{ marginBottom: 5 }}
                            />
                        </View>
                        <View style={{ flex: 0.2, marginLeft: 30, marginTop: 10 }}>
                            <TouchableOpacity
                                onPress={() => {
                                    onClose();
                                    props.navigation.navigate('QrcodeScanner');
                                }}>
                                <MaterialIcons
                                    name="qr-code-scanner"
                                    color={'#000'}
                                    size={25}
                                />
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}>
                        <CustomButtonWithBorder
                            bdColor={'#C5C5C5'}
                            textColor={'#20232B'}
                            buttonName={t('Close')}
                            onPressButton={() => onClose()}
                        />
                        <CustomButton
                            buttonName={t("Submit")}
                            onPressButton={() => onClose()}
                            bgColor={'#208196'}
                        />
                    </View>
                </View>
            </View>
        </Modal>
    );
};

export const ImageUploadPopUp = props => {
    const { t, i18n } = useTranslation();
    const { isVisible, onClose, handleChoosePhoto, requestCameraPermission } =
        props;

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
            onRequestClose={onClose}>
            <View style={styles.modalContainer}>
                <View style={styles.modalContent}>
                    <Text
                        style={{
                            fontFamily: fonts.MEDIUM,
                            fontSize: 20,
                            color: '#000',
                        }}>
                        {t('Choose from')}
                    </Text>
                    <View
                        style={{
                            width: '100%',
                            height: 120,
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                        }}>
                        <TouchableOpacity
                            onPress={() => requestCameraPermission()}
                            style={{
                                width: '50%',
                                height: '100%',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                            <MaterialIcons
                                name="camera-alt"
                                size={45}
                                color={Colors.blue87}
                            />

                            <Text
                                style={{
                                    color: '#000',
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 16,
                                    paddingTop: 10,
                                }}>
                                {t('Camera')}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={handleChoosePhoto}
                            style={{
                                width: '50%',
                                height: '100%',
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}>
                            <MaterialIcons
                                name="camera-alt"
                                size={45}
                                color={Colors.blue87}
                            />

                            <Text
                                style={{
                                    color: '#000',
                                    paddingTop: 10,
                                    fontFamily: fonts.MEDIUM,
                                    fontSize: 16,
                                }}>
                                {t('Gallery')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    );
};
export const ConfirmationPopUp = props => {
    const { t, i18n } = useTranslation();
    const { title, message, isVisible, onClose, confirm } = props;

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
            onRequestClose={() => onClose()}

        // animationInTiming={1000}
        // animationOutTiming={1000}
        // backdropTransitionInTiming={1000}
        // backdropTransitionOutTiming={1000}
        >
            <View style={styles.modalContainer}>
                <View style={styles.modalContent1}>
                    <Text style={{ fontSize: 22, textAlign: 'center', color: '#333333' }}>
                        {t(title)}
                    </Text>
                    <Text
                        style={{
                            fontSize: 16,
                            textAlign: 'center',
                            marginTop: 12,
                            color: '#707070',
                            marginRight: 20,
                            marginLeft: 20,
                        }}>
                        {t(message)}
                    </Text>
                    <View
                        style={{
                            width: scale(230),
                            flexDirection: 'row',
                            marginTop: verticalScale(20),
                            alignItems: 'center',
                            justifyContent: 'space-between',
                        }}>

                        <CustomButtonWithBorder
                            bdColor={'#C5C5C5'}
                            textColor={'#20232B'}
                            buttonName={t('No')}
                            onPressButton={() => onClose()}
                        />
                        <CustomButton
                            buttonName={t('Yes')}
                            onPressButton={() => confirm()}
                            bgColor={'#208196'}
                        />

                    </View>
                </View>
            </View>
        </Modal>
    );
};
export const BatchConfirmationPopUp = props => {
    const { t, i18n } = useTranslation();
    const { title, message, isVisible, onClose, confirm } = props;

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isVisible}
            onRequestClose={() => onClose()}

        // animationInTiming={1000}
        // animationOutTiming={1000}
        // backdropTransitionInTiming={1000}
        // backdropTransitionOutTiming={1000}
        >
            <View style={styles.modalContainer}>
                <View style={styles.modalContent2}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                        <ErrorSvg height={40} width={40} />

                        <Text style={{ fontSize: 20, color: '#333333', marginLeft: 10 }}>
                            {t(title)}
                        </Text>
                    </View>
                    <Text style={{ fontSize: 14, color: '#707070' }}>{t(message)}</Text>
                    <View
                        style={{
                            marginTop: 20,
                            alignItems: 'center',
                            justifyContent: 'space-between',
                        }}>
                        {/* <View style={{ flex: 0.35 }}> */}
                        <CustomButtonWithBorder
                            bdColor={'#C5C5C5'}
                            textColor={'#20232B'}
                            buttonName={t('Cancel')}
                            onPressButton={() => onClose()}
                        />
                        {/* </View>
                        <View style={{ flex: 0.65 }}> */}
                        <CustomButton
                            buttonName={t('Save & Open Vial')}
                            onPressButton={() => confirm()}
                            bgColor={'#208196'}
                        />
                        {/* </View> */}
                    </View>
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    modalContent: {
        backgroundColor: '#fff',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: DeviceHeight / 3,
        width: DeviceWidth / 1.2,
        borderWidth: 1,
    },
    modalContent2: {
        backgroundColor: '#F8E6E6',
        borderRadius: 10,
        padding: 30,
        height: DeviceHeight / 3,
        width: DeviceWidth / 1.2,
    },
    modalContent1: {
        backgroundColor: 'white',
        borderRadius: 10,
        padding: 30,
        height: DeviceHeight / 3,
        width: DeviceWidth / 1.2,
    },
    closeButton: {
        fontSize: 16,
        color: '#fff',
        fontWeight: '700',
    },
});
