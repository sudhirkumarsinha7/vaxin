import React, { useRef, useState } from 'react';
import { View, StyleSheet, PanResponder } from 'react-native';

const SignatureScreen = () => {
    const [path, setPath] = useState([]);
    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onMoveShouldSetPanResponder: () => true,
            onPanResponderMove: (event, gesture) => {
                setPath((prevPath) => [...prevPath, { x: gesture.moveX, y: gesture.moveY }]);
            },
            onPanResponderRelease: () => {
                // Signature capture completed
                // You can do something with the captured path (e.g., save or process it)
            },
        })
    ).current;

    return (
        <View style={styles.container}>
            <View
                {...panResponder.panHandlers}
                style={styles.signatureContainer}
            >
                <View style={styles.signature}>
                    {path.map((point, index) => (
                        <View key={index} style={[styles.point, { left: point.x, top: point.y }]} />
                    ))}
                </View>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    signatureContainer: {
        width: '90%',
        height: 100,
        borderWidth: 1,
        borderColor: '#000',
        backgroundColor: 'white',
    },
    signature: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: -300,
        marginLeft: -38
    },
    point: {
        position: 'absolute',
        width: 4,
        height: 4,
        backgroundColor: 'black',
        borderRadius: 2,
    },
});

export default SignatureScreen;