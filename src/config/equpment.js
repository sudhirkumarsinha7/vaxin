/* eslint-disable prettier/prettier */
const a = {
    id: '745683465467',
    operationalStatus: 'WORKING',
    type: 'Ventilator',
    model: 'A123456677',
    serialNumber: 'S0098764533',
    powerSource: 'DC',
    manufacturerId: '6609cb4df53ce27c7092cf1d',
    locationId: '657a6fa580557eb8f3db0445',
    supplyLevel: '10%',
    pqsStatus: 'Stable',
    pqsCode: 'PQS123',
    pqsType: 'Normal',
    registrationDate: '2024-03-31T20:45:02.893Z',
    age: 30,
    yearOfInstallation: '2024',
    yearOfDisposal: '2054',
    yearOfAnticipatedDisposal: '2055',
    capacity: {
        temperature: '25 C',
        grossStorage: 25,
        units: 'test',
    },
    reasonOfNonFunctionality: 'AWAITING_REPAIR',
    recommendation: {
        solution: 'Solution 1',
        replacement: 'Multipara Beneficiary monitor',
        reason: 'Not working Properly',
    },
    sourceOfFunding: 'Statwing',
    costOfReplacement: [
        {
            year: '2055',
            amount: 10000,
        },
    ],
    costOfMaintenance: [
        {
            year: '2054',
            amount: 234455,
        },
    ],
    costOfRunning: [
        {
            year: '2050',
            amount: 783485,
        },
    ],
};
