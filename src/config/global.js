/* eslint-disable prettier/prettier */

import { Dimensions, Image, StyleSheet } from 'react-native';
export const DeviceHeight = Dimensions.get('window').height;
export const DeviceWidth = Dimensions.get('window').width;
export const localImage = {
  applogo: require('../assets/VaxinWithlogo.png'),
  farviewLogo: require('../assets/FARVIEW.png'),
  backgraound: require('../assets/backgraound.png'),
  backgraoundHeader: require('../assets/backgraoundHeader.png'),
  account: require('../assets/account.png'),
  companyLogo: require('../assets/STATWIGLogo.png'),
  loginDashboard: require('../assets/loginDashboard.png'),
  Fback: require('../assets/Fback.png'),
  FCircle: require('../assets/FCircle.png'),
  FSquare: require('../assets/FSquare.png'),
  backSpace: require('../assets/backSpace.png'),
  home: require('../assets/img/home.png'),
  inventory: require('../assets/img/inventory.png'),
  order: require('../assets/img/order.png'),
  shipment: require('../assets/img/shipment.png'),
  track: require('../assets/img/track.png'),
  addInvSuccess: require('../assets/img/addInvSuccess.png'),
  congrats: require('../assets/img/congrats.png'),
  media: require('../assets/img/media.png'),
  track_map: require('../assets/img/track_map.png'),
  empty: require('../assets/img/empty.png'),
  Welcome: require('../assets/img/Welcome.png'),
  filter: require('../assets/img/filter.png'),
  campaign: require('../assets/img/campaign.png'),
  Calendar: require('../assets/img/Calendar.png'),
  ColdChain: require('../assets/img/ColdChain.png'),
  error: require('../assets/img/error.png'),
  LastMile: require('../assets/img/LastMile.png'),
  Notifications: require('../assets/img/Notifications.png'),
  bn: require('../assets/lang/bn.png'),
  en: require('../assets/lang/en.png'),
  UserProfile: require('../assets/img/UserProfile.png'),
  VaccineReport: require('../assets/img/VaccineReport.png'),

  // Confirm_Yes_Accept: require('../assets/img/Confirm_Yes_Accept.png'),
  // CurrentStock: require('../assets/img/CurrentStock.png'),
  // Damaged_Cold_ChainFailure: require('../assets/img/Damaged_Cold_ChainFailure.png'),
  // DataVisualisation: require('../assets/img/DataVisualisation.png'),
  // Delete: require('../assets/img/Delete.png'),
  // delivered: require('../assets/img/delivered.png'),
  // Download: require('../assets/img/Download.png'),
  // Expired: require('../assets/img/Expired.png'),
  // Export: require('../assets/img/Export.png'),
  // Import: require('../assets/img/Import.png'),
  // InboundOrders: require('../assets/img/InboundOrders.png'),
  // InboundShipment: require('../assets/img/InboundShipment.png'),
  // Min_Max: require('../assets/img/Min_Max.png'),
  // NearExpiry: require('../assets/img/NearExpiry.png'),
  // NetUtilization: require('../assets/img/NetUtilization.png'),
  // OrdersAccepted: require('../assets/img/OrdersAccepted.png'),
  // OrdersRejected: require('../assets/img/OrdersRejected.png'),
  // OrdersSent: require('../assets/img/OrdersSent.png'),
  // OrderTransfers: require('../assets/img/OrderTransfers.png'),
  // OutboundOrders: require('../assets/img/OutboundOrders.png'),
  // OutboundShipmen: require('../assets/img/OutboundShipmen.png'),
  // ProductReport: require('../assets/img/ProductReport.png'),
  // OrdersRejected: require('../assets/img/OrdersRejected.png'),
  // RejectedQuantity: require('../assets/img/RejectedQuantity.png'),
  // Report: require('../assets/img/Report.png'),
  // Search: require('../assets/img/Search.png'),
  // Settings: require('../assets/img/Settings.png'),
  // Shipped: require('../assets/img/Shipped.png'),
  // StockOut: require('../assets/img/StockOut.png'),
  // TotalNo_ofProducts: require('../assets/img/TotalNo_ofProducts.png'),
  // TotalOrders: require('../assets/img/TotalOrders.png'),
  // Upload: require('../assets/img/Upload.png'),
  // UserProfile: require('../assets/img/UserProfile.png'),
  // VaccineReport: require('../assets/img/VaccineReport.png'),
  // ViewDetails: require('../assets/img/ViewDetails.png'),

};
export const langList = [
  { value: 'en', label: 'English', icon: () => <Image source={localImage.en} style={styles.icon} />, },
  { value: 'bn', label: 'বাংলা', icon: () => <Image source={localImage.bn} style={styles.icon} />, },
  // { id: 'te', name: 'తెలుగు' },
  // { id: 'hi', name: 'हिंदी' },
];

export const Info = {
  supportMail: 'dev@statwig.com',
  supportPh: '+919059687874',
  GoogleMapsApiKey: 'AIzaSyBLwFrIrQx_0UUAIaUwt6wfItNMIIvXJ78',
};
export const defaultLocation = {
  latitude: 17.4295865,
  longitude: 78.368776,
  latitudeDelta: 0.0922,
  longitudeDelta: 0.0421,
}
const Colors = {
  red00: 'red ',
  blue: 'blue',
  black: 'black',
};
const styles = StyleSheet.create({

  icon: {
    width: 24,
    height: 24,
    marginRight: 8,
  },

  iconStyle: {
    width: 20,
    height: 20,
  },

});