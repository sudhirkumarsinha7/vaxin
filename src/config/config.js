// export const TEST_SERVER_URL = 'https://15.207.109.218';//test


// export const TEST_SERVER_URL = 'https://test.unicef.vaccineledger.com'; //test
// export const TEST_SERVER_URL = 'https://demo.farview.ai';//Demo farview

// export const TEST_SERVER_URL = 'https://unicef.vaccineledger.com';//unicef
export const TEST_SERVER_URL = 'https://epi.vaccineledger.com';//epi

// export const buildIdentifier = 'com.statwig.vaxin';
export const buildIdentifier = 'com.thevaccineledgerlite';

// export const buildIdentifier = 'com.statwig.farviewip';
export const serverToken =
  'key=AAAAOVgqbi4:APA91bHdq_TCPigrri6TdxrwL21v-A0aHWjKK9HG4xTJ_rd0MTa4ozzk4L4jk2uHI8tOU1VjTQML1EAl39N5bQ3JmXVY6DUBTypd2kvL_u9IspJHD5TxBpX_wHV5r4JK2iEQ84hNWWRG';

export const getIosDeviceTokeApi =
  'https://iid.googleapis.com/iid/v1:batchImport';

export function config() {
  const confs = {
    dev: {
      // user management
      login: `${TEST_SERVER_URL}/api/user-service/v1/login`,
      login_hris: `${TEST_SERVER_URL}/api/user-service/v1/sign-in-hris`,
      logout: `${TEST_SERVER_URL}/api/user-service/v1/logout`,
      register: `${TEST_SERVER_URL}/api/user-service/v1/register`,
      send_otp: `${TEST_SERVER_URL}/api/user-service/v1/send-otp`,
      verify_otp: `${TEST_SERVER_URL}/api/user-service/v1/verify-otp`,
      password: `${TEST_SERVER_URL}/api/user-service/v1/password`,
      info: `${TEST_SERVER_URL}/api/user-service/v1/users/info`,
      userlocation: `${TEST_SERVER_URL}/api/user-service/v1/users/locations`,
      userlocationInfo: `${TEST_SERVER_URL}/api/user-service/v1/users/locations/info`,

      orgLevels: `${TEST_SERVER_URL}/api/user-service/v1/orgLevels`,
      product_list: `${TEST_SERVER_URL}/api/user-service/v1/products`,
      orgLevelLocation: `${TEST_SERVER_URL}/api/user-service/v1/locations?orgLevel=`,

      // inventory management
      inventory_summary: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/events`,
      add_inventory: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory`,
      inventory_inStock: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/`,
      outOfStock: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/outOfStock`,
      nearExpiry: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/nearExpiry`,
      expired: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/expired`,
      inventory_analytics: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/analytics`,
      productlistByBatch: `${TEST_SERVER_URL}/api/inventory-service/v1/batches/`,
      inventoryLocation: `${TEST_SERVER_URL}/api/inventory-service/v1/inventories?isColdStorgae=false`,
      coldstorage: `${TEST_SERVER_URL}/api/inventory-service/v1/inventories?isColdStorgae=true`,
      rejected: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/rejected`,
      inventory_value: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/value`,
      min_max: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/products?`,

      // Order management
      create_order: `${TEST_SERVER_URL}/api/shipment-service/v1/orders`,
      inbound_order: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/inbound`,
      inbound_order_received: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/inbound?status=CREATED`,
      inbound_order_accepted: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/inbound?status=ACCEPTED`,
      inbound_order_rejected: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/inbound?status=REJECTED`,
      inbound_order_accepted_partial: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/inbound?status=UNFULFILLED`,

      outbound_order: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/outbound`,
      outbound_order_sent: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/outbound?status=CREATED&isTransfer=false`,
      outbound_order_transfer: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/outbound?isTransfer=true`,
      outbound_order_accepted: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/outbound?status=ACCEPTED&isTransfer=false`,
      outbound_order_rejected: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/outbound?status=REJECTED&isTransfer=false`,

      orderinfo: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/info/`,
      inbound_analtics: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/analytics/inbound`,
      outbound_analtics: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/analytics/outbound`,
      order_status: `${TEST_SERVER_URL}/api/shipment-service/v1/orders`,
      order_respond: `${TEST_SERVER_URL}/api/shipment-service/v1/orders/respond`,

      // Shipment management
      create_shipment: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments`,
      receive_shipment: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/receive`,
      // : [“CREATED”, “RECEIVED”, “PARTIALLY_RECEIVED”, “RETURNED”]
      inbound_shipment: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/inbound`,
      inbound_shipment_deliver: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/inbound?status=RECEIVED`,
      inbound_shipment_shiped: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/inbound?status=CREATED`,
      inbound_shipment_damage: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/inbound?status=PARTIALLY_RECEIVED`,

      outbound_shipment: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/outbound`,
      outbound_shipment_deliver: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/outbound?status=RECEIVED`,
      outbound_shipment_shipped: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/outbound?status=CREATED`,
      outbound_shipment_damage: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/outbound?status=PARTIALLY_RECEIVED`,

      shipment_info: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/info/`,
      shipments_inbound_analtics: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/analytics/inbound`,
      shipments_outbound_analtics: `${TEST_SERVER_URL}/api/shipment-service/v1/shipments/analytics/outbound`,

      //Var
      countryList: `${TEST_SERVER_URL}/api/user-service/v1/countries`,
      airportList: `${TEST_SERVER_URL}/api/user-service/v1/airports/`,
      varList: `${TEST_SERVER_URL}/api/inventory-service/v1/vars`,
      createVar: `${TEST_SERVER_URL}/api/inventory-service/v1/vars`,
      var_info: `${TEST_SERVER_URL}/api/inventory-service/v1/vars/`,
      //Beneficiary
      add_beneficiaries: `${TEST_SERVER_URL}/api/inventory-service/v1/beneficiaries`,
      getBeneficiaries_today: `${TEST_SERVER_URL}/api/inventory-service/v1/beneficiaries?today=true`,
      getBeneficiaries: `${TEST_SERVER_URL}/api/inventory-service/v1/beneficiaries?today=false`,
      get_utilized: `${TEST_SERVER_URL}/api/inventory-service/v1/inventory/utilized`,
      last_mile_analytics: `${TEST_SERVER_URL}/api/inventory-service/v1/beneficiaries/analytics`,

      //Par
      parList: `${TEST_SERVER_URL}/api/inventory-service/v1/pars`,
      createPar: `${TEST_SERVER_URL}/api/inventory-service/v1/pars`,
      par_info: `${TEST_SERVER_URL}/api/inventory-service/v1/pars/`,
      //Cold Chain
      get_equipment_list: `${TEST_SERVER_URL}/api/inventory-service/v1/assets`,
      get_maintance_list: `${TEST_SERVER_URL}/api/inventory-service/v1/assets/jobs`,
      create_maintance: `${TEST_SERVER_URL}/api/inventory-service/v1/assets/jobs`,
      get_maintance_info: `${TEST_SERVER_URL}/api/inventory-service/v1/assets/jobs/`,
      temperature: `${TEST_SERVER_URL}/api/inventory-service/v1/assets/temperature`,


      //images
      uploadImage: `${TEST_SERVER_URL}/api/user-service/v1/images`,
      get_Image: `${TEST_SERVER_URL}/api/user-service/v1/images/`,
      //Compaign Management
      campaigns_api: `${TEST_SERVER_URL}/api/shipment-service/v1/campaigns`,
      //Compaign Management
      registerFirebase: `${TEST_SERVER_URL}/api/notification-service/v1/registerFirebase`,
      notifications_api: `${TEST_SERVER_URL}/api/notification-service/v1/notifications`,
      inv_notifications_api: `${TEST_SERVER_URL}/api/notification-service/v1/notifications?eventType=INVENTORY`,

      order_notifications_api: `${TEST_SERVER_URL}/api/notification-service/v1/notifications?eventType=ORDER`,
      shipment_notifications_api: `${TEST_SERVER_URL}/api/notification-service/v1/notifications?eventType=SHIPMENT`,
      user_notifications_api: `${TEST_SERVER_URL}/api/notification-service/v1/notifications?eventType=USER`,
      //Compaign Management
      alerts: `${TEST_SERVER_URL}/api/user-service/v1/alerts`,
    },
  };

  const conf = confs.dev;

  return conf;
}
