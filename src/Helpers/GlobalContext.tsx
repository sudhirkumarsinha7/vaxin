/* eslint-disable prettier/prettier */
import React, { createContext, useState, useContext, useEffect } from 'react';
import i18n from '../languages/i18n';

type GlobalContextData = {
       currentLanguage: any;
       storeLanguage(data: string): Promise<void>;

};

interface Props {
       children: React.ReactNode;
}

const GlobalContext = createContext<GlobalContextData>({} as GlobalContextData);

const GlobalProvider = ({ children }: Props) => {
       const [currentLanguage, setCurrentLanguage] = useState(i18n.language);

       useEffect(() => {
              //Every time the App is opened, this provider is rendered
              //and call de loadStorage function.
       }, []);

       const storeLanguage = async (language: string) => {
              console.log('language---', language);
              i18n.changeLanguage(language);
              setCurrentLanguage(language);
       };

       return (
              <GlobalContext.Provider
                     value={{
                            currentLanguage,
                            storeLanguage,
                     }}>
                     {children}
              </GlobalContext.Provider>
       );
};
function useGlobal(): GlobalContextData {
       const context = useContext(GlobalContext);

       if (!context) {
              throw new Error('useAuth must be used within an AuthProvider');
       }

       return context;
}

export { GlobalContext, GlobalProvider, useGlobal };
