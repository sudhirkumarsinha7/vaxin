import AsyncStorage from '@react-native-async-storage/async-storage';

export const getAsyncSavedData = async (itemName: any) => {
  let userData = await AsyncStorage.getItem(itemName);
  if (userData) {
    return userData && JSON.parse(userData);
  }
  return null;
};

export const saveAsyncData = async (itemName: any, data: any) => {
  try {
    return await AsyncStorage.setItem(itemName, JSON.stringify(data));
  } catch (err) {
    return err;
  }
};

export const removeAsyncData = async (itemName: any) => {
  try {
    return await AsyncStorage.removeItem(itemName);
  } catch (err) {
    return err;
  }
};
