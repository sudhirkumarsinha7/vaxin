// @flow
import moment from 'moment';

import NetInfo from '@react-native-community/netinfo';

export function numberWithCommas(x) {
  // converts 1000 -> 1,000
  if (x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  } else {
    return 0;
  }
}

export function renderStringZeroIfUndefined(value) {
  if (value) {
    return value + '';
  } else {
    if (value === undefined || value === 0) {
      return '0';
    } else {
      return '';
    }
  }
}

export function renderStringNAIfUndefined(value) {
  if (value) {
    return value;
  } else {
    if (value === undefined) {
      return 'NA';
    } else {
      return '';
    }
  }
}
export function renderEmptyStringIfUndefined(value) {
  if (value) {
    return value;
  } else {
    return '';
  }
}
export function formatNumber(num) {
  const absNum = Math.abs(num);

  if (absNum >= 1e12) {
    return (num / 1e12).toFixed(1) + 'T';
  } else if (absNum >= 1e9) {
    return (num / 1e9).toFixed(1) + 'B';
  } else if (absNum >= 1e6) {
    return (num / 1e6).toFixed(1) + 'M';
  } else if (absNum >= 1e3) {
    return (num / 1e3).toFixed(1) + 'K';
  } else {
    return num;
  }
}

export function convertToStringIfInteger(integer) {
  if (integer && typeof integer === 'number') {
    return integer + '';
  } else {
    return '';
  }
}

export function compareValues(key, order = 'asc') {
  return function innerSort(a, b) {
    if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
      // property doesn't exist on either object
      return 0;
    }

    const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
    const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

    let comparison = 0;
    if (varA > varB) {
      comparison = 1;
    } else if (varA < varB) {
      comparison = -1;
    }
    return order === 'desc' ? comparison * -1 : comparison;
  };
}

export function normalizeLat(lat) {
  if (lat > 90) {
    const newLat = -90 + (lat - 90);
    return normalizeLat(newLat);
  } else if (lat < -90) {
    const newLat = 90 - (-90 - lat);
    return normalizeLat(newLat);
  } else {
    return lat;
  }
}

export function normalizeLon(lon) {
  if (lon > 180) {
    const newLon = -180 + (lon - 180);
    return normalizeLon(newLon);
  } else if (lon < -180) {
    const newLon = 180 - (-180 - lon);
    return normalizeLon(newLon);
  } else {
    return lon;
  }
}
export function formatDateDDMMYYYYTime(date) {
  const newDate =
    moment(date).format('DD/MM/YYYY') === 'Invalid date'
      ? date
      : moment(date).format('DD/MM/YYYY hh:mm A');
  return newDate;
  // return moment(date).format('DD/MM/YYYY hh:mm A');
}
export function formatDateYYYYMMDD(date) {
  const newDate =
    moment(date).format('DD/MM/YYYY') === 'Invalid date'
      ? date
      : moment(date).format('YYYY-MM-DD');
  return newDate;
  // return moment(date).format('DD/MM/YYYY hh:mm A');
}
export function formatDateDDMMYYYY(date) {
  const newDate =
    moment(date).format('DD/MM/YYYY') === 'Invalid date'
      ? date
      : moment(date).format('DD/MM/YYYY');
  return newDate;
  // return moment(date).format('DD/MM/YYYY hh:mm A');
}
export function formatDateMMYYYY(date) {
  const newDate =
    moment(date).format('DD/MM/YYYY') === 'Invalid date'
      ? date
      : moment(date).format('MM/YYYY');
  return newDate;
  // return moment(date).format('DD/MM/YYYY hh:mm A');
}
export function formatDateMMYY(date) {
  const newDate =
    moment(date).format('DD/MM/YYYY') === 'Invalid date'
      ? date
      : moment(date).format('MMM YYYY');
  return newDate;
  // return moment(date).format('DD/MM/YYYY hh:mm A');
}

export function timeAgo(date) {
  const newDate =
    moment(date).format('DD/MM/YYYY') === 'Invalid date'
      ? 'NA'
      : moment.utc(date).local().startOf('seconds').fromNow();
  return newDate;
}
export class NetworkUtils {
  static async isNetworkAvailable() {
    const response = await NetInfo.fetch();
    // console.log('NetworkUtils response ' + JSON.stringify(response));

    return response.isConnected;
  }
}
export const calculateAge = dob => {
  const dobDate = new Date(dob); // Convert DOB string to Date object
  const currentDate = new Date(); // Get current date

  const diffMonths =
    (currentDate.getFullYear() - dobDate.getFullYear()) * 12 +
    (currentDate.getMonth() - dobDate.getMonth());
  const ageInMonths =
    currentDate.getDate() < dobDate.getDate() ? diffMonths - 1 : diffMonths;

  if (ageInMonths <= 11) {
    return ageInMonths + ' months'; // Display age in months
  } else {
    const ageInYears = Math.floor(ageInMonths / 12);
    const remainingMonths = ageInMonths % 12;
    return ageInYears + ' years '; // Display age in years and months

    // return ageInYears + ' years and ' + remainingMonths + ' months'; // Display age in years and months
  }
};
