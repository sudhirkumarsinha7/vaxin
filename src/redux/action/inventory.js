/* eslint-disable prettier/prettier */
import axios from 'axios';
import { config } from '../../config/config';
import { loadingOff, loadingOn } from './loader';
import {
    GET_INV_SUMMARY,
    GET_INV_STOCK,
    GET_INV_OUT_STOCK,
    GET_INV_NEAR_EXPIRE,
    GET_INV_EXPIRED,
    GET_INV_REJECTED,
    GET_INV_NET_UTILIZATION,
    GET_INV_MINMAX,

    GET_INV_SUMMARY_MORE,
    GET_INV_STOCK_MORE,
    GET_INV_OUT_STOCK_MORE,
    GET_INV_NEAR_EXPIRE_MORE,
    GET_INV_EXPIRED_MORE,
    GET_INV_REJECTED_MORE,
    GET_INV_NET_UTILIZATION_MORE,
    GET_INV_MINMAX_MORE,

    GET_INV_SUMMARY_PAGE_COUNT,
    GET_INV_STOCK_PAGE_COUNT,
    GET_INV_OUT_STOCK_PAGE_COUNT,
    GET_INV_NEAR_EXPIRE_PAGE_COUNT,
    GET_INV_EXPIRED_PAGE_COUNT,
    GET_INV_REJECTED_PAGE_COUNT,
    GET_INV_NET_UTILIZATION_PAGE_COUNT,
    GET_INV_MINMAX_PAGE_COUNT,


    GET_INV_PRODUCT_LIST,
    GET_PRODUCT_LIST_BY_BATCH,
    INVENTORY_ANALYTICS,
    GET_COLDSTORAGE_LIST,
    GET_INV_LIST,
    GET_INV_VALUE
} from '../constant'
export const getInventoryValue = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inventory_value);


        // console.log('getInventoryValue response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_INV_VALUE,
                payload: response.data.data,
            });

        }

        return response;
    } catch (e) {
        // // console.log('getInventoryValue error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInventoryAnalytcs = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inventory_analytics);


        // console.log('getInventoryAnalytcs response', response);
        if (response.status === 200) {
            dispatch({
                type: INVENTORY_ANALYTICS,
                payload: response.data.data,
            });

        }

        return response;
    } catch (e) {
        // // console.log('getInventoryAnalytcs error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getProdList = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().product_list);


        // console.log('getProdList response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_INV_PRODUCT_LIST,
                payload: response.data.data,
            });

        }
        const returnVal = response.data && response.data.data
        return returnVal;
    } catch (e) {
        // console.log('getProdList error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInvSummary = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inventory_summary + '?page=' + page + '&limit=10');

        console.log(' page', page);

        console.log('getInvSummary response', response);
        // if (response.status === 200) {

        //     dispatch({
        //         type: GET_INV_SUMMARY,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });
        //     dispatch({
        //         type: GET_INV_SUMMARY_TOTAL,
        //         payload: response.data && response.data.data && response.data.data.totalRecords,
        //     });
        // }

        dispatch({
            type: GET_INV_SUMMARY_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INV_SUMMARY_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INV_SUMMARY,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInvSummary error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInStockList = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inventory_inStock + '?page=' + page + '&limit=10');


        // console.log('getInStockList response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INV_STOCK,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }
        dispatch({
            type: GET_INV_STOCK_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INV_STOCK_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INV_STOCK,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInStockList error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOutOFStockList = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outOfStock + '?page=' + page + '&limit=10');


        // console.log('getOutOFStockList response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INV_OUT_STOCK,
        //         payload: response.data.data && response.data.data.data,
        //     });

        // }
        dispatch({
            type: GET_INV_OUT_STOCK_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INV_OUT_STOCK_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INV_OUT_STOCK,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // console.log('getOutOFStockList error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInvNearExpList = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().nearExpiry + '?page=' + page + '&limit=10');


        // console.log('getInvNearExpiList response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INV_NEAR_EXPIRE,
        //         payload: response.data.data && response.data.data.data,
        //     });

        // }
        dispatch({
            type: GET_INV_NEAR_EXPIRE_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INV_NEAR_EXPIRE_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INV_NEAR_EXPIRE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInvNearExpiList error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInvExpList = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().expired + '?page=' + page + '&limit=10');


        // console.log('getInvExpList response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INV_EXPIRED,
        //         payload: response.data.data && response.data.data.data,
        //     });

        // }
        dispatch({
            type: GET_INV_EXPIRED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INV_EXPIRED_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INV_EXPIRED,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInvExpList error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInvRejectedList = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().rejected + '?page=' + page + '&limit=10');


        // console.log('getInvRejectedList response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INV_REJECTED,
        //         payload: response.data.data && response.data.data.data,
        //     });

        // }
        dispatch({
            type: GET_INV_REJECTED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INV_REJECTED_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INV_REJECTED,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInvExpList error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInvNetUtilList = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().add_inventory + '?page=' + page + '&limit=10');


        // console.log('getInvNetUtilList response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INV_NET_UTILIZATION,
        //         payload: response.data.data && response.data.data.data,
        //     });

        // }

        dispatch({
            type: GET_INV_NET_UTILIZATION_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INV_NET_UTILIZATION_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INV_NET_UTILIZATION,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInvNetUtilList error', e);
    } finally {
        loadingOff(dispatch);
    }
};


export const getInvMinMaxList = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().min_max + '&page=' + page + '&limit=10');


        // console.log('getInvMinMaxList response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INV_MINMAX,
        //         payload: response.data.data,
        //     });

        // }
        dispatch({
            type: GET_INV_MINMAX_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INV_MINMAX_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INV_MINMAX,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInvMinMaxList error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const add_inventory = (data, callback = () => { }) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        // console.log('Action_add_inventory_data' + JSON.stringify(data));
        const response = await axios.post(config().add_inventory, {
            products: data,
        });
        // console.log('Action_sadd_inventory_response' + JSON.stringify(response));
        return response;
    } catch (e) {
        // console.log('Action_add_inventory_error' + JSON.stringify(e));
        return e.response;
    } finally {
        loadingOff(dispatch);
    }
};

export const getProdListByBatch = (id) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().productlistByBatch + id + '?page=1&limit=200');


        // console.log('getProdListByBatch response' + JSON.stringify(response));
        if (response.status === 200) {
            dispatch({
                type: GET_PRODUCT_LIST_BY_BATCH,
                payload: response.data && response.data.data && response.data.data.data,
            });

        }

        return response;
    } catch (e) {
        // console.log('getProdListByBatch error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getInventoryLocationList = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        // console.log('getInventoryLocationList config().inventoryLocation', config().inventoryLocation);

        let response = await axios.get(config().inventoryLocation);


        // console.log('getInventoryLocationList response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_INV_LIST,
                payload: response.data && response.data.data || [],
            });

        }

        return response;
    } catch (e) {
        // console.log('getInventoryLocationList error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getColdStorageList = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().coldstorage);


        // console.log('getColdStorageList response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_COLDSTORAGE_LIST,
                payload: response.data.data,
            });

        }

        return response;
    } catch (e) {
        // console.log('getColdStorageList error', e);
    } finally {
        loadingOff(dispatch);
    }
};