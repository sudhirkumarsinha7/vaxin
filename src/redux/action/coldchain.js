/* eslint-disable prettier/prettier */
import axios from 'axios';

import { config } from '../../config/config';
import {
  GET_COLDCHAIN_LIST,
  GET_COLDCHAIN_INFO,
  GET_ASSETS_LIST
} from '../constant';

import { loadingOff, loadingOn } from './loader';
export const getEquipmentList = () => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('getEquipmentList url', config().get_equipment_list + '?page=1&limit=100');

    let response = await axios.get(config().get_equipment_list + '?page=1&limit=100');


    // console.log('getEquipmentList response', response);
    if (response.status === 200) {
      dispatch({
        type: GET_ASSETS_LIST,
        payload: response.data.data && response.data.data.data,
      });

    }

    return response;
  } catch (e) {
    // // console.log('getColdchainList error', e);
  } finally {
    loadingOff(dispatch);
  }
};

export const getColdchainList = () => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().get_maintance_list + '?page=1limit=100');


    // console.log('getColdchainList response', response);
    if (response.status === 200) {
      dispatch({
        type: GET_COLDCHAIN_LIST,
        payload: response.data.data && response.data.data.data,
      });

    }

    return response;
  } catch (e) {
    // // console.log('getColdchainList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getColdchainInfo = (id) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().get_maintance_info + id);


    console.log('getColdchainInfo response', response);
    if (response.status === 200) {
      dispatch({
        type: GET_COLDCHAIN_INFO,
        payload: response.data.data,
      });

    }

    return response;
  } catch (e) {
    // // console.log('getColdchainInfo error', e);
  } finally {
    loadingOff(dispatch);
  }
};

export const create_Coldchain = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_create_Coldchain_data' + JSON.stringify(data));
    // console.log('create_Coldchain' + JSON.stringify(config().createColdchain));

    const response = await axios.post(config().create_maintance, data);
    // console.log('Action_create_Coldchain_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    // console.log('Action_create_Coldchain_error' + JSON.stringify(e));
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const update_Coldchain = (id, data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_update_Coldchain_data' + JSON.stringify(data));
    // console.log('update_Coldchain' + JSON.stringify(config().createColdchain));

    const response = await axios.put(config().get_maintance_info + id, data);
    // console.log('Action_update_Coldchain_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    // console.log('Action_update_Coldchain_error' + JSON.stringify(e));
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};

export const add_temperature = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_add_temperature_data' + JSON.stringify(data));
    // console.log('add_temperature' + JSON.stringify(config().temperature));

    const response = await axios.post(config().temperature, data);
    // console.log('Action_add_temperature_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    // console.log('Action_add_temperature_error' + JSON.stringify(e));
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const getTempList = (id, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_getTempList_data' + JSON.stringify(data));
    console.log('getTempList url' + JSON.stringify(config().temperature + '/' + id));

    const response = await axios.get(config().temperature + '/' + id);
    console.log('Action_getTempList_response' + JSON.stringify(response));
    return response.data && response.data.data;
  } catch (e) {
    // console.log('Action_getTempList_error' + JSON.stringify(e));
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};


