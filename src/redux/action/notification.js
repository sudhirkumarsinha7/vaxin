/* eslint-disable prettier/prettier */
import axios from 'axios';

import { config, getIosDeviceTokeApi, buildIdentifier, serverToken, } from '../../config/config';
import {
  GET_ALL_NOTIFICATION_LIST,
  GET_ORDER_NOTIFICATION_LIST,
  GET_SHIPMENT_NOTIFICATION_LIST,
  GET_INVENTORY_NOTIFICATION_LIST,
  GET_LASTMILE_NOTIFICATION_LIST,

  GET_ALL_NOTIFICATION_LIST_MORE,
  GET_ORDER_NOTIFICATION_LIST_MORE,
  GET_SHIPMENT_NOTIFICATION_LIST_MORE,
  GET_INVENTORY_NOTIFICATION_LIST_MORE,

  GET_ALL_NOTIFICATION_LIST_PAGE_COUNT,
  GET_ORDER_NOTIFICATION_LIST_PAGE_COUNT,
  GET_SHIPMENT_NOTIFICATION_LIST_PAGE_COUNT,
  GET_INVENTORY_NOTIFICATION_LIST_PAGE_COUNT,
} from '../constant';

import { loadingOff, loadingOn } from './loader';
export const registerNotification = (userdata, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_user_registerNotification123_' + config().registerFirebase);
    console.log(
      'Action_user_userdata' + JSON.stringify(userdata),
    );
    if (userdata.device_type === 'ios') {
      let body = {
        "application": buildIdentifier,
        "sandbox": true,
        "apns_tokens": [
          userdata.notificationToken
        ]
      }
      const configs = {
        headers: {
          'Authorization': serverToken,
          'Content-Type': 'application/json'
        },
      };
      const response = await axios.post(getIosDeviceTokeApi, body, configs);
      const newToken = response.data.results && response.data.results[0] && response.data.results[0].registration_token
      // console.log(
      //   'Action_user_registerNotification_response' + JSON.stringify(response),
      // );
      console.log(
        ' newToken' + JSON.stringify(newToken),
      );
      userdata.notificationToken = newToken
      console.log(
        ' userdata newToken' + JSON.stringify(userdata),
      );
      const response1 = await axios.post(config().notifications_api, userdata);
      console.log(
        'Action_user_registerNotification_response registerFirebase' + JSON.stringify(response1),
      );
      return response1;
    } else {
      const response2 = await axios.post(config().notifications_api, userdata);
      console.log(
        'Action_user_registerNotification_response' + JSON.stringify(response2),
      );
      return response2;
    }
  } catch (e) {
    console.log('Action_registerNotification error  ' + JSON.stringify(e));
    console.log(
      'Action_registerNotification e.response  ' + JSON.stringify(e.response),
    );

    return e.response;

  } finally {
    loadingOff(dispatch);
  }
};

export const getNotificationList = (page = 1) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().notifications_api + '?page=' + page + '&limit=20');


    // console.log('getNotificationList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_ALL_NOTIFICATION_LIST,
    //     payload: response.data && response.data.data && response.data.data.data || [],
    //   });

    // }

    dispatch({
      type: GET_ALL_NOTIFICATION_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_ALL_NOTIFICATION_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.notifications,
      });
    } else {
      dispatch({
        type: GET_ALL_NOTIFICATION_LIST,
        payload: response.data && response.data.data && response.data.data.notifications,
      });
    }

    return response;
  } catch (e) {
    // // console.log('getNotificationList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getOrderNotificationList = (page = 1) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().order_notifications_api + '&page=' + page + '&limit=20');


    // console.log('getOrderNotificationList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_ORDER_NOTIFICATION_LIST,
    //     payload: response.data && response.data.data && response.data.data.data || [],
    //   });

    // }
    dispatch({
      type: GET_ORDER_NOTIFICATION_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_ORDER_NOTIFICATION_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.notifications,
      });
    } else {
      dispatch({
        type: GET_ORDER_NOTIFICATION_LIST,
        payload: response.data && response.data.data && response.data.data.notifications,
      });
    }

    return response;
  } catch (e) {
    // // console.log('getOrderNotificationList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getShipmentNotificationList = (page = 1) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().shipment_notifications_api + '&page=' + page + '&limit=20');


    // console.log('getShipmentNotificationList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_SHIPMENT_NOTIFICATION_LIST,
    //     payload: response.data && response.data.data && response.data.data.data || [],
    //   });

    // }
    dispatch({
      type: GET_SHIPMENT_NOTIFICATION_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_SHIPMENT_NOTIFICATION_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.notifications,
      });
    } else {
      dispatch({
        type: GET_SHIPMENT_NOTIFICATION_LIST,
        payload: response.data && response.data.data && response.data.data.notifications,
      });
    }

    return response;
  } catch (e) {
    // // console.log('getShipmentNotificationList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getinventoryNotificationList = (page = 1) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().inv_notifications_api + '&page=' + page + '&limit=20');


    // console.log('getinventoryNotificationList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_INVENTORY_NOTIFICATION_LIST,
    //     payload: response.data && response.data.data && response.data.data.data || [],
    //   });

    // }

    dispatch({
      type: GET_INVENTORY_NOTIFICATION_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_INVENTORY_NOTIFICATION_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.notifications,
      });
    } else {
      dispatch({
        type: GET_INVENTORY_NOTIFICATION_LIST,
        payload: response.data && response.data.data && response.data.data.notifications,
      });
    }


    return response;
  } catch (e) {
    // // console.log('getinventoryNotificationList error', e);
  } finally {
    loadingOff(dispatch);
  }
};


