/* eslint-disable prettier/prettier */
import axios from 'axios';

import { config } from '../../config/config';
import {

} from '../constant';

import { loadingOff, loadingOn } from './loader';

// import AsyncStorage from '@react-native-async-storage/async-storage';
export const uploadImage =
  (data) =>
    async (dispatch, getState) => {
      loadingOn(dispatch);
      try {
        let response;
        const configs = {
          headers: {
            'content-type': 'multipart/form-data',
          },
        };
        console.log(
          'Action_uuploadImagesUrl ' + JSON.stringify(`${config().uploadImage}`),
        );
        console.log('Action_upload_Image ', data);
        response = await axios.post(config().uploadImage, data, configs);
        console.log('Action_upload_Image_response' + JSON.stringify(response));
        return response;
      } catch (e) {
        console.log('Action_upload_Image_error' + JSON.stringify(e));
      } finally {
        loadingOff(dispatch);
      }
    };