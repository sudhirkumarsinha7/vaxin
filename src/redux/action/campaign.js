/* eslint-disable prettier/prettier */

import axios from 'axios';

import { config } from '../../config/config';
import {
  GET_COMPAIGN_LIST,
  GET_COMPAIGN_ACTIVE_LIST,
  GET_COMPAIGN_LIST_MORE,
  GET_COMPAIGN_ACTIVE_LIST_MORE,
  GET_COMPAIGN_PAGE_COUNT,
  GET_COMPAIGN_ACTIVE_PAGE_COUNT
} from '../constant';

import { loadingOff, loadingOn } from './loader';

export const getCampaignList = (page = 1) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(
      config().campaigns_api + '?page=' + page + '&limit=10',
    );
    // console.log(
    //   'getCampaignList response' + JSON.stringify(response.data.data.page),
    // );

    // dispatch({
    //   type: GET_COMPAIGN_LIST,
    //   payload: response.data.data.data || [],
    // });
    // console.log('getCampaignList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_COMPAIGN_LIST,
    //     payload:
    //       (response.data && response.data.data && response.data.data.data) ||
    //       [],
    //   });
    // }
    dispatch({
      type: GET_COMPAIGN_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_COMPAIGN_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.data,
      });
    } else {
      dispatch({
        type: GET_COMPAIGN_LIST,
        payload: response.data && response.data.data && response.data.data.data,
      });
    }
    return response;
  } catch (e) {
    // // console.log('getCampaignList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getActiveCampaignList = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(
      config().campaigns_api + '?page=1&limit=200&isActive=true',
    );

    // console.log('getCampaignList response', response);
    if (response.status === 200) {
      dispatch({
        type: GET_COMPAIGN_ACTIVE_LIST,
        payload:
          (response.data && response.data.data && response.data.data.data) ||
          [],
      });
    }

    return response;
  } catch (e) {
    // // console.log('getCampaignList error', e);
  } finally {
    loadingOff(dispatch);
  }
};

export const create_Compaign =
  (data, callback = () => { }) =>
    async (dispatch, getState) => {
      loadingOn(dispatch);
      try {
        // console.log('Action_create_Par_data' + JSON.stringify(data));
        // console.log('create_Compaign' + JSON.stringify(config().createPar));

        const response = await axios.post(config().campaigns_api, data);
        // console.log('Action_create_Par_response' + JSON.stringify(response));
        return response;
      } catch (e) {
        // console.log('Action_create_Par_error' + JSON.stringify(e));
        return e.response;
      } finally {
        loadingOff(dispatch);
      }
    };
export const update_Compaign =
  (data, callback = () => { }) =>
    async (dispatch, getState) => {
      loadingOn(dispatch);
      try {
        // console.log('Action_update_Compaign_data' + JSON.stringify(data));
        // console.log('update_Compaign' + JSON.stringify(config().createPar));

        const response = await axios.patch(config().campaigns_api, data);
        // console.log('Action_cupdate_Compaign_response' + JSON.stringify(response));
        return response;
      } catch (e) {
        // console.log('Action_update_Compaign_error' + JSON.stringify(e));
        return e.response;
      } finally {
        loadingOff(dispatch);
      }
    };
