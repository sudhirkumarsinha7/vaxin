/* eslint-disable prettier/prettier */
import {
  GET_ORG_LEVEL,
  USERINFO,
  GET_ORG_LOCATION,
  LOG_OUT,
  GET_ORG_LOCATION_LIST,
  PERMISSIONS,
  GET_LOCATION_LIST
} from '../constant';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { config } from '../../config/config';

import setAuthToken from '../../config/setAuthToken';
import { loadingOff, loadingOn } from './loader';

export const getOrgLevel = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().orgLevels);
    dispatch({
      type: GET_ORG_LEVEL,
      payload: response.data.data,
    });
    console.log('Action_getOrgLevel_response' + JSON.stringify(response));

    return response;
  } catch (e) {
    console.log('Action_getOrgLevelerror' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};

export const register = (data, callback = () => { }) =>
  async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log('Action_register_data' + JSON.stringify(data));
      let response = await axios.post(config().register, data);

      // console.log('Action_sendOtp_response' + JSON.stringify(response));
      return response;
    } catch (e) {
      console.log('Action_register_error' + JSON.stringify(e));
      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
export const verifyOtp = (data, callback = () => { }) =>
  async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log('Action_verifyOtp_data' + JSON.stringify(data));
      let response = await axios.post(config().verify_otp, data);

      // console.log('Action_verifyOtp_response' + JSON.stringify(response.data.data));
      dispatch({
        type: USERINFO,
        payload: response.data.data,
      });
      setAuthToken(
        response.data && response.data.data && response?.data?.data?.token,
      );
      return response;
    } catch (e) {
      console.log('Action_verifyOtp_error' + JSON.stringify(e));

      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
export const resetPassword = (data, callback = () => { }) =>
  async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log('Action_resetPassword_data' + JSON.stringify(data));
      let response = await axios.put(config().password, data);

      // console.log('Action_resetPassword_response' + JSON.stringify(response));

      return response;
    } catch (e) {
      console.log('Action_resetPassword_error' + JSON.stringify(e));

      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };

export const sendOtp = (data, callback = () => { }) =>
  async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log('Action_send_otp_data' + JSON.stringify(data));
      let response = await axios.post(config().send_otp, data);

      // console.log('Action_send_otp_response' + JSON.stringify(response));

      return response;
    } catch (e) {
      console.log('Action_send_otp_error' + JSON.stringify(e));

      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
export const LogIn = (data, callback = () => { }) =>
  async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log('Action_sLogIn_data' + JSON.stringify(data));
      // console.log('config().login ' + config().login);

      let response = await axios.post(config().login, data);

      // console.log('Action_LogIn_response' + JSON.stringify(response.data.data));
      if (response?.data?.data?.token) {
        await AsyncStorage.setItem('token', response?.data?.data?.token);
        setAuthToken(
          response.data && response.data.data && response?.data?.data?.token,
        );
      }

      dispatch({
        type: USERINFO,
        payload: response.data && response.data.data,
      });

      return response;
    } catch (e) {
      console.log('Action_LogIn_error' + JSON.stringify(e));

      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
export const LogIn_hris = (data, callback = () => { }) =>
  async (dispatch, getState) => {
    loadingOn(dispatch);
    try {
      // console.log('Action_sLogIn_data' + JSON.stringify(data));
      // console.log('config().login ' + config().login);

      let response = await axios.post(config().login_hris, data);

      // console.log('Action_LogIn_response' + JSON.stringify(response.data.data));
      if (response?.data?.data?.token) {
        await AsyncStorage.setItem('token', response?.data?.data?.token);
        setAuthToken(
          response.data && response.data.data && response?.data?.data?.token,
        );
      }

      dispatch({
        type: USERINFO,
        payload: response.data && response.data.data,
      });

      return response;
    } catch (e) {
      console.log('Action_LogIn_error' + JSON.stringify(e));

      return e.response;
    } finally {
      loadingOff(dispatch);
    }
  };
export const log_out = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().logout);

    // console.log('Action_log_out_response' + JSON.stringify(response));
    dispatch({
      type: LOG_OUT,
    });

    return response;
  } catch (e) {
    console.log('Action_log_outLevelerror' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};

export const getUserInfo =
  (data, callback = () => { }) =>
    async (dispatch, getState) => {
      loadingOn(dispatch);
      try {
        console.log('config().info' + JSON.stringify(config().info));

        let response = await axios.get(config().info);
        console.log('Action_getUserInfo_response' + JSON.stringify(response));

        dispatch({
          type: USERINFO,
          payload: response.data && response.data.data,
        });
        // console.log('Action_getUserInfo_response' + JSON.stringify(response));
        if (response.data && response.data.data && response.data.data.roleId) {
          let permissions = response?.data?.data?.roleId?.permissions;
          // console.log('permissions' + JSON.stringify(permissions));

          const objectData = permissions.reduce((acc, item) => {
            // acc[item.id] = { name: item.name, value: item.value };
            acc[item.id] = item.value;
            return acc;
          }, {});
          dispatch({
            type: PERMISSIONS,
            payload: objectData,
          });
        }
        return response;
      } catch (e) {
        console.log('Action_getUserInfoerror' + JSON.stringify(e));

        return e.response;
      } finally {
        loadingOff(dispatch);
      }
    };

export const getUserLocation = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().userlocationInfo);
    // console.log('Action_getUserLocationl_response' + JSON.stringify(response));

    dispatch({
      type: GET_ORG_LOCATION,
      payload: response.data.data,
    });

    return response;
  } catch (e) {
    console.log('Action_ggetUserLocationerror' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const getUserLocationList = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_getUserLocationList_response' + config().userlocation);

    let response = await axios.get(config().userlocation);

    dispatch({
      type: GET_ORG_LOCATION_LIST,
      payload: response.data.data,
    });
    // console.log('Action_getUserLocationList_response' + JSON.stringify(response));

    return response;
  } catch (e) {
    console.log('Action_getUserLocationList_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const getLocationList = (orgLevel = '') => async (dispatch) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_getLocationList_response' + config().userlocation);

    let response = await axios.get(config().orgLevelLocation + orgLevel);

    dispatch({
      type: GET_LOCATION_LIST,
      payload: response.data && response.data.data,
    });
    // console.log('Action_getLocationList_response' + JSON.stringify(response));

    return response.data && response.data.data;
  } catch (e) {
    console.log('Action_getUserLocationList_error' + JSON.stringify(e));

    // return e.response;
    return [];

  } finally {
    loadingOff(dispatch);
  }
};
export const ResetUserInfo = () => async dispatch => {
  dispatch({
    type: USERINFO,
    payload: {},
  });
};
export const DeleteAccount = () => async dispatch => {
  loadingOn(dispatch);
  try {
    let response = await axios.post(config().deleteAccount);

    // console.log('Action_sDeleteAccount_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    console.log('Action_DeleteAccount_error' + JSON.stringify(e));

    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};
export const UpdateNotification =
  (data, callback = () => { }) =>
    async (dispatch, getState) => {
      loadingOn(dispatch);
      try {
        // console.log('UpdateNotification' + JSON.stringify(data));
        // console.log('config().alerts ' + config().alerts);

        let response = await axios.put(config().alerts, data);

        // console.log('UpdateNotification res' + JSON.stringify(response));

        return response;
      } catch (e) {
        console.log('Action_UpdateNotification_error' + JSON.stringify(e));

        return e.response;
      } finally {
        loadingOff(dispatch);
      }
    };
