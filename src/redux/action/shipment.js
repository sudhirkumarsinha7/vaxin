/* eslint-disable prettier/prettier */
import axios from 'axios';

import { config } from '../../config/config';

import { loadingOff, loadingOn } from './loader';
import {
    GET_INBOUND_SUMMARY_LIST,
    GET_INBOUND_DELIVERED_LIST,
    GET_INBOUND_SHIPPED_LIST,
    GET_INBOUND_DAMAGED_LIST,
    GET_OUTBOUND_DAMAGED_LIST,
    GET_OUTBOUND_SHIPPED_LIST,
    GET_OUTBOUND_SUMMARY_LIST,
    GET_OUTBOUND_DELIVERED_LIST,
    GET_SHIPMENT_DETAILS,
    SHIPMENT_INBOUND_ANALYTICS,
    SHIPMENT_OUTBOUND_ANALYTICS,
    GET_INBOUND_SUMMARY_LIST_MORE,
    GET_INBOUND_DELIVERED_LIST_MORE,
    GET_INBOUND_SHIPPED_LIST_MORE,
    GET_INBOUND_DAMAGED_LIST_MORE,
    GET_OUTBOUND_DAMAGED_LIST_MORE,
    GET_OUTBOUND_SHIPPED_LIST_MORE,
    GET_OUTBOUND_SUMMARY_LIST_MORE,
    GET_OUTBOUND_DELIVERED_LIST_MORE,
    GET_INBOUND_SUMMARY_COUNT,
    GET_INBOUND_DELIVERED_COUNT,
    GET_INBOUND_SHIPPED_COUNT,
    GET_INBOUND_DAMAGED_COUNT,
    GET_OUTBOUND_DAMAGED_COUNT,
    GET_OUTBOUND_SHIPPED_COUNT,
    GET_OUTBOUND_SUMMARY_COUNT,
    GET_OUTBOUND_DELIVERED_COUNT,
    GET_INBOUND_SUMMARY_PAGE,
    GET_INBOUND_DELIVERED_PAGE,
    GET_INBOUND_SHIPPED_PAGE,
    GET_INBOUND_DAMAGED_PAGE,
    GET_OUTBOUND_DAMAGED_PAGE,
    GET_OUTBOUND_SHIPPED_PAGE,
    GET_OUTBOUND_SUMMARY_PAGE,
    GET_OUTBOUND_DELIVERED_PAGE,
} from '../constant'
export const getInboudShipmentAnalytcs = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().shipments_inbound_analtics);


        console.log('getInboudAnalytcs response', response);
        if (response.status === 200) {
            dispatch({
                type: SHIPMENT_INBOUND_ANALYTICS,
                payload: response.data.data,
            });

        }

        return response;
    } catch (e) {
        // // console.log('getInboudAnalytcs error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOutboudShipmentAnalytcs = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().shipments_outbound_analtics);


        console.log('getOutboudshipmentAnalytcs response', response);
        if (response.status === 200) {
            dispatch({
                type: SHIPMENT_OUTBOUND_ANALYTICS,
                payload: response.data.data,
            });

        }

        return response;
    } catch (e) {
        // // console.log('getOutboudshipmentAnalytcs error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const create_shipment = (data, callback = () => { }) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        console.log('Action_create_shipment_data' + JSON.stringify(data));
        const response = await axios.post(config().create_shipment, data);
        // console.log('Action_create_shipment_response' + JSON.stringify(response));
        return response;
    } catch (e) {
        // console.log('Action_create_shipment_error' + JSON.stringify(e));
        return e.response;
    } finally {
        loadingOff(dispatch);
    }
};
export const receiveShipment = (data, callback = () => { }) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        console.log('config().receive_shipment ' + config().receive_shipment);

        console.log('Action_creceive_shipmentt_data' + JSON.stringify(data));
        const response = await axios.post(config().receive_shipment, data);
        console.log('Action_receive_shipment_response' + JSON.stringify(response));
        return response;
    } catch (e) {
        // console.log('Action_receive_shipment_error' + JSON.stringify(e));
        return e.response;
    } finally {
        loadingOff(dispatch);
    }
};
export const getShipmentDetails = (id) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().shipment_info + id);


        // console.log('getShipmentDetails response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_SHIPMENT_DETAILS,
                payload: response.data && response.data.data,
            });

        }

        return response;
    } catch (e) {
        // console.log('getShipmentDetails error', e);
    } finally {
        loadingOff(dispatch);
    }
};


export const getShipmentInboudSummary = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_shipment + '?page=' + page + '&limit=10');


        // // console.log('getShipmentInboudSummary response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_INBOUND_SUMMARY_COUNT,
                payload: response.data && response.data.data && response.data.data.totalRecords,
            });
            dispatch({
                type: GET_INBOUND_SUMMARY_PAGE,
                payload: response.data && response.data.data && response.data.data.page,
            });
            if (page > 1) {
                dispatch({
                    type: GET_INBOUND_SUMMARY_LIST_MORE,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            } else {
                dispatch({
                    type: GET_INBOUND_SUMMARY_LIST,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            }


        }

        return response;
    } catch (e) {
        // // console.log('getShipmentInboudSummary error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getShipmentInboundDelivery = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_shipment_deliver + '&page=' + page + '&limit=10');


        // // console.log('getShipmentInboundDelivery response', response);
        if (response.status === 200) {



            dispatch({
                type: GET_INBOUND_DELIVERED_COUNT,
                payload: response.data && response.data.data && response.data.data.totalRecords,
            });
            dispatch({
                type: GET_INBOUND_DELIVERED_PAGE,
                payload: response.data && response.data.data && response.data.data.page,
            });
            if (page > 1) {
                dispatch({
                    type: GET_INBOUND_DELIVERED_LIST_MORE,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            } else {
                dispatch({
                    type: GET_INBOUND_DELIVERED_LIST,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            }
        }

        return response;
    } catch (e) {
        // console.log('getShipmentInboundDelivery error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getShipmentInboundShipped = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_shipment_shiped + '&page=' + page + '&limit=10');


        // console.log('getShipmentInboundShipped response', response);
        if (response.status === 200) {



            dispatch({
                type: GET_INBOUND_SHIPPED_COUNT,
                payload: response.data && response.data.data && response.data.data.totalRecords,
            });
            dispatch({
                type: GET_INBOUND_SHIPPED_PAGE,
                payload: response.data && response.data.data && response.data.data.page,
            });
            if (page > 1) {
                dispatch({
                    type: GET_INBOUND_SHIPPED_LIST_MORE,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            } else {
                dispatch({
                    type: GET_INBOUND_SHIPPED_LIST,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            }
        }

        return response;
    } catch (e) {
        // console.log('getShipmentInboundShipped error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getShipmentInboundDamaged = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_shipment_damage + '&page=' + page + '&limit=10');


        // console.log('getShipmentInboundDamaged response', response);
        if (response.status === 200) {

            dispatch({
                type: GET_INBOUND_DAMAGED_COUNT,
                payload: response.data && response.data.data && response.data.data.totalRecords,
            });
            dispatch({
                type: GET_INBOUND_DAMAGED_PAGE,
                payload: response.data && response.data.data && response.data.data.page,
            });
            if (page > 1) {
                dispatch({
                    type: GET_INBOUND_DAMAGED_LIST_MORE,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            } else {
                dispatch({
                    type: GET_INBOUND_DAMAGED_LIST,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            }
        }

        return response;
    } catch (e) {
        // console.log('getShipmentInboundDamaged error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getShipmentOutboudDamaged = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_shipment_damage + '&page=' + page + '&limit=10');


        // console.log('getShipmentOutboudDamaged response', response);
        if (response.status === 200) {


            dispatch({
                type: GET_OUTBOUND_DAMAGED_COUNT,
                payload: response.data && response.data.data && response.data.data.totalRecords,
            });
            dispatch({
                type: GET_OUTBOUND_DAMAGED_PAGE,
                payload: response.data && response.data.data && response.data.data.page,
            });
            if (page > 1) {
                dispatch({
                    type: GET_OUTBOUND_DAMAGED_LIST_MORE,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            } else {
                dispatch({
                    type: GET_OUTBOUND_DAMAGED_LIST,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            }

        }

        return response;
    } catch (e) {
        // console.log('getShipmentOutboudDamaged error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getShipmentOutboundShiped = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        // console.log('config().outbound_shipment_shipped', config().outbound_shipment_shipped);

        let response = await axios.get(config().outbound_shipment_shipped + '&page=' + page + '&limit=10');


        // console.log('getShipmentOutboundShiped response', response);
        if (response.status === 200) {


            dispatch({
                type: GET_OUTBOUND_SHIPPED_COUNT,
                payload: response.data && response.data.data && response.data.data.totalRecords,
            });
            dispatch({
                type: GET_OUTBOUND_SHIPPED_PAGE,
                payload: response.data && response.data.data && response.data.data.page,
            });
            if (page > 1) {
                dispatch({
                    type: GET_OUTBOUND_SHIPPED_LIST_MORE,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            } else {
                dispatch({
                    type: GET_OUTBOUND_SHIPPED_LIST,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            }


        }

        return response;
    } catch (e) {
        // console.log('getShipmentOutboundShiped error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getShipmentOutboundSummary = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_shipment + '?page=' + page + '&limit=10');


        console.log('getShipmentOutboundSummary response', response.data.data);
        if (response.status === 200) {


            dispatch({
                type: GET_OUTBOUND_SUMMARY_COUNT,
                payload: response.data && response.data.data && response.data.data.totalRecords,
            });
            dispatch({
                type: GET_OUTBOUND_SUMMARY_PAGE,
                payload: response.data && response.data.data && response.data.data.page,
            });
            if (page > 1) {
                dispatch({
                    type: GET_OUTBOUND_SUMMARY_LIST_MORE,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            } else {
                dispatch({
                    type: GET_OUTBOUND_SUMMARY_LIST,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            }

        }

        return response;
    } catch (e) {
        console.log('getoutboundAccepted error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getShipmentOutboundDelivered = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_shipment_deliver + '&page=' + page + '&limit=10');


        // console.log('getoutboundRejected response', response);
        if (response.status === 200) {


            dispatch({
                type: GET_OUTBOUND_DELIVERED_COUNT,
                payload: response.data && response.data.data && response.data.data.totalRecords,
            });
            dispatch({
                type: GET_OUTBOUND_DELIVERED_PAGE,
                payload: response.data && response.data.data && response.data.data.page,
            });
            if (page > 1) {
                dispatch({
                    type: GET_OUTBOUND_DELIVERED_LIST_MORE,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            } else {
                dispatch({
                    type: GET_OUTBOUND_DELIVERED_LIST,
                    payload: response.data && response.data.data && response.data.data.data,
                });
            }

        }

        return response;
    } catch (e) {
        // console.log('getoutboundRejected error', e);
    } finally {
        loadingOff(dispatch);
    }
};