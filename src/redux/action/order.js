/* eslint-disable prettier/prettier */
import axios from 'axios';

import { config } from '../../config/config';

import { loadingOff, loadingOn } from './loader';


import {
    GET_INBOUND_HISTORY_PAGE_COUNT,
    GET_INBOUND_RECEIVED_PAGE_COUNT,
    GET_INBOUND_ACCEPTED_PAGE_COUNT,
    GET_INBOUND_REJECTED_PAGE_COUNT,
    GET_INBOUND_ORDER_SHIPPED_PAGE_COUNT,
    GET_INBOUND_PARTIAL_ORDER_SHIPPED_PAGE_COUNT,
    GET_INBOUND_FULLFILL_PAGE_COUNT,
    GET_INBOUND_PARTIAL_FULLFILL_PAGE_COUNT,
    GET_INBOUND_CANCELLED_PAGE_COUNT,

    GET_INBOUND_HISTORY_LIST,
    GET_INBOUND_RECEIVED_LIST,
    GET_INBOUND_ACCEPTED_LIST,
    GET_INBOUND_REJECTED_LIST,
    GET_INBOUND_ORDER_SHIPPED_LIST,
    GET_INBOUND_PARTIAL_ORDER_SHIPPED_LIST,
    GET_INBOUND_FULLFILL_LIST,
    GET_INBOUND_PARTIAL_FULLFILL_LIST,
    GET_INBOUND_CANCELLED_LIST,

    GET_INBOUND_HISTORY_LIST_MORE,
    GET_INBOUND_RECEIVED_LIST_MORE,
    GET_INBOUND_ACCEPTED_LIST_MORE,
    GET_INBOUND_REJECTED_LIST_MORE,
    GET_INBOUND_ORDER_SHIPPED_LIST_MORE,
    GET_INBOUND_PARTIAL_ORDER_SHIPPED_LIST_MORE,
    GET_INBOUND_FULLFILL_LIST_MORE,
    GET_INBOUND_PARTIAL_FULLFILL_LIST_MORE,
    GET_INBOUND_CANCELLED_LIST_MORE,

    GET_OUTBOUND_REJECTED_PAGE_COUNT,
    GET_OUTBOUND_ACCEPTED_PAGE_COUNT,
    GET_OUTBOUND_HISTORY_PAGE_COUNT,
    GET_OUTBOUND_SENT_PAGE_COUNT,
    GET_OUTBOUND_TRANSFER_PAGE_COUNT,
    GET_OUTBOUND_ORDER_SHIPPED_PAGE_COUNT,
    GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_PAGE_COUNT,
    GET_OUTBOUND_FULLFILL_PAGE_COUNT,
    GET_OUTBOUND_PARTIAL_FULLFILL_PAGE_COUNT,
    GET_OUTBOUND_CANCELLED_PAGE_COUNT,

    GET_OUTBOUND_REJECTED_LIST,
    GET_OUTBOUND_ACCEPTED_LIST,
    GET_OUTBOUND_HISTORY_LIST,
    GET_OUTBOUND_SENT_LIST,
    GET_OUTBOUND_TRANSFER_LIST,
    GET_OUTBOUND_ORDER_SHIPPED_LIST,
    GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_LIST,
    GET_OUTBOUND_FULLFILL_LIST,
    GET_OUTBOUND_PARTIAL_FULLFILL_LIST,
    GET_OUTBOUND_CANCELLED_LIST,

    GET_OUTBOUND_ACCEPTED_LIST_MORE,
    GET_OUTBOUND_HISTORY_LIST_MORE,
    GET_OUTBOUND_SENT_LIST_MORE,
    GET_OUTBOUND_TRANSFER_LIST_MORE,
    GET_OUTBOUND_REJECTED_LIST_MORE,
    GET_OUTBOUND_ORDER_SHIPPED_LIST_MORE,
    GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_LIST_MORE,
    GET_OUTBOUND_FULLFILL_LIST_MORE,
    GET_OUTBOUND_PARTIAL_FULLFILL_LIST_MORE,
    GET_OUTBOUND_CANCELLED_LIST_MORE,

    GET_DELIVERY_LOCATION,
    GET_SUPPLIER_LOCATION,
    GET_ORDER_DETAILS,
    ORDER_INBOUND_ANALYTCS,
    ORDER_OUTBOUND_ANALYTCS,
    GET_INBOUND_ACCEPTED_PARTIAL_LIST,
} from '../constant'
export const getInboudOrderAnalytcs = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_analtics);


        console.log('getInboudAnalytcs response', response);
        if (response.status === 200) {
            dispatch({
                type: ORDER_INBOUND_ANALYTCS,
                payload: response.data.data,
            });

        }

        return response;
    } catch (e) {
        // // console.log('getInboudAnalytcs error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOutboudOrderAnalytcs = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_analtics);


        console.log('getOutboudOrderAnalytcs response', response);
        if (response.status === 200) {
            dispatch({
                type: ORDER_OUTBOUND_ANALYTCS,
                payload: response.data.data,
            });

        }

        return response;
    } catch (e) {
        // // console.log('getOutboudOrderAnalytcs error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInboudHistory = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order + '?page=' + page + '&limit=10');


        console.log('getInboudHistory response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INBOUND_HISTORY_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }
        dispatch({
            type: GET_INBOUND_HISTORY_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_HISTORY_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_HISTORY_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // // console.log('getInboudHistory error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getInboundReceived = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order_received + '&page=' + page + '&limit=10');


        console.log('getInboundReceived response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INBOUND_RECEIVED_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }
        dispatch({
            type: GET_INBOUND_RECEIVED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_RECEIVED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_RECEIVED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // console.log('getInboundReceived error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getInboundAccepted = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order_accepted + '&page=' + page + '&limit=10');


        // // console.log('getInboundAccepted response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INBOUND_ACCEPTED_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }
        dispatch({
            type: GET_INBOUND_ACCEPTED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_ACCEPTED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_ACCEPTED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // console.log('getInboundAccepted error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInboundAcceptedPartial = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order_accepted_partial + '&page=1&limit=200');
        // console.log('config().inbound_order_accepted_partial ' + config().inbound_order_accepted_partial);


        // console.log('getInboundAccepted response' + JSON.stringify(response));
        if (response.status === 200) {
            dispatch({
                type: GET_INBOUND_ACCEPTED_PARTIAL_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });

        }

        return response;
    } catch (e) {
        // console.log('getInboundAccepted error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInboundRejected = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order_rejected + '&page=' + page + '&limit=10');


        // console.log('getInboundRejected response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_INBOUND_REJECTED_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }

        dispatch({
            type: GET_INBOUND_REJECTED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_REJECTED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_REJECTED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInboundRejected error', e);
    } finally {
        loadingOff(dispatch);
    }
};


export const getInboudShippedOrder = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order + '?status=SHIPPED&page=' + page + '&limit=10');


        console.log('getInboudShippedOrder response', response);

        dispatch({
            type: GET_INBOUND_ORDER_SHIPPED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_ORDER_SHIPPED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_ORDER_SHIPPED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // // console.log('getInboudShippedOrder error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getInboundPartialShippedOrder = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order + '?status=PARTIALLY_SHIPPED&page=' + page + '&limit=10');


        console.log('getInboundPartialShippedOrder response', response);

        dispatch({
            type: GET_INBOUND_PARTIAL_ORDER_SHIPPED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_PARTIAL_ORDER_SHIPPED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_PARTIAL_ORDER_SHIPPED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // console.log('getInboundPartialShippedOrder error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getInboundFullFilled = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order + '?status=FULFILLED&page=' + page + '&limit=10');



        dispatch({
            type: GET_INBOUND_FULLFILL_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_FULLFILL_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_FULLFILL_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // console.log('getInboundFullFilled error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInboundPartialFullFilled = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order + '?status=PARTIALLY_FULFILLED&page=' + page + '&limit=10');
        // console.log('config().getInboundPartialFullFilled ' + config().inbound_order_accepted_partial);

        dispatch({
            type: GET_INBOUND_PARTIAL_FULLFILL_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_PARTIAL_FULLFILL_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_PARTIAL_FULLFILL_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInboundFullFilled error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getInboundCanceled = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().inbound_order + '?status=CANCELLED&page=' + page + '&limit=10');




        dispatch({
            type: GET_INBOUND_CANCELLED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_INBOUND_CANCELLED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_INBOUND_CANCELLED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInboundCanceled error', e);
    } finally {
        loadingOff(dispatch);
    }
};



export const getOutboudHistory = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order + '?page=' + page + '&limit=10');


        // console.log('getInboudHistory response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_OUTBOUND_HISTORY_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }

        dispatch({
            type: GET_OUTBOUND_HISTORY_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_HISTORY_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_HISTORY_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getInboudHistory error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOutboudTransfer = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        // console.log('config().outbound_order_transfer', config().outbound_order_transfer);

        let response = await axios.get(config().outbound_order_transfer + '&page=' + page + '&limit=10');


        console.log('getOutboudTransfer response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_OUTBOUND_TRANSFER_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }

        dispatch({
            type: GET_OUTBOUND_TRANSFER_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_TRANSFER_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_TRANSFER_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        console.log('getOutboudTransfer error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOutboundSent = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order_sent + '&page=' + page + '&limit=10');


        // console.log('getOutboundSent response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_OUTBOUND_SENT_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }

        dispatch({
            type: GET_OUTBOUND_SENT_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_SENT_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_SENT_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getOutboundSent error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getOutboundAccepted = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order_accepted + '&page=' + page + '&limit=10');


        // console.log('getoutboundAccepted response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_OUTBOUND_ACCEPTED_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }

        dispatch({
            type: GET_OUTBOUND_ACCEPTED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_ACCEPTED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_ACCEPTED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getoutboundAccepted error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOutboundRejected = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order_rejected + '&page=' + page + '&limit=10');


        // console.log('getoutboundRejected response', response);
        // if (response.status === 200) {
        //     dispatch({
        //         type: GET_OUTBOUND_REJECTED_LIST,
        //         payload: response.data && response.data.data && response.data.data.data,
        //     });

        // }

        dispatch({
            type: GET_OUTBOUND_REJECTED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_REJECTED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_REJECTED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getoutboundRejected error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getOutboudShippedOrder = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order + '?status=SHIPPED&isTransfer=false&page=' + page + '&limit=10');


        console.log('getOutboudShippedOrder response', response);

        dispatch({
            type: GET_OUTBOUND_ORDER_SHIPPED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_ORDER_SHIPPED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_ORDER_SHIPPED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // // console.log('getOutboudShippedOrder error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getOutboudPartialShippedOrder = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order + '?status=PARTIALLY_SHIPPED&isTransfer=false&page=' + page + '&limit=10');


        console.log('getOutboudPartialShippedOrder response', response);

        dispatch({
            type: GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // console.log('getOutboudPartialShippedOrder error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const getOutboudFullFilled = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order + '?status=FULFILLED&isTransfer=false&page=' + page + '&limit=10');



        dispatch({
            type: GET_OUTBOUND_FULLFILL_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_FULLFILL_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_FULLFILL_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }
        return response;
    } catch (e) {
        // console.log('getOutboudFullFilled error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOutboudPartialFullFilled = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order + '?status=PARTIALLY_FULFILLED&isTransfer=false&page=' + page + '&limit=10');
        // console.log('config().getOutboudPartialFullFilled ' + config().outbound_order_accepted_partial);

        dispatch({
            type: GET_OUTBOUND_PARTIAL_FULLFILL_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_PARTIAL_FULLFILL_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_PARTIAL_FULLFILL_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getOutboudFullFilled error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOutboudCanceled = (page = 1) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order + '?status=CANCELLED&page=' + page + '&limit=10');




        dispatch({
            type: GET_OUTBOUND_CANCELLED_PAGE_COUNT,
            payload: response.data && response.data.data && response.data.data.page,
        });
        if (page > 1) {
            dispatch({
                type: GET_OUTBOUND_CANCELLED_LIST_MORE,
                payload: response.data && response.data.data && response.data.data.data,
            });
        } else {
            dispatch({
                type: GET_OUTBOUND_CANCELLED_LIST,
                payload: response.data && response.data.data && response.data.data.data,
            });
        }

        return response;
    } catch (e) {
        // console.log('getOutboudCanceled error', e);
    } finally {
        loadingOff(dispatch);
    }
};


export const getOrderSupplireLocation = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order_accepted);


        // console.log('getOrderSupplireLocation response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_SUPPLIER_LOCATION,
                payload: response.data && response.data.data && response.data.data.data,
            });

        }

        return response;
    } catch (e) {
        // console.log('getOrderSupplireLocation error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const getOrderDeliveryLocation = () => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().outbound_order_accepted);


        // console.log('getOrderDeliveryLocation response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_DELIVERY_LOCATION,
                payload: response.data.data,
            });

        }

        return response;
    } catch (e) {
        // console.log('getOrderDeliveryLocation error', e);
    } finally {
        loadingOff(dispatch);
    }
};
export const add_order = (data, callback = () => { }) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        // console.log('Action_add_order_data' + JSON.stringify(data));
        // console.log('create_order' + JSON.stringify(config().create_order));

        const response = await axios.post(config().create_order, data);
        // console.log('Action_sadd_order_response' + JSON.stringify(response));
        return response;
    } catch (e) {
        // console.log('Action_add_order_error' + JSON.stringify(e));
        return e.response;
    } finally {
        loadingOff(dispatch);
    }
};

export const getOrderDetails = (id) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        let response = await axios.get(config().orderinfo + id);


        // console.log('getOrderDetail response', response);
        if (response.status === 200) {
            dispatch({
                type: GET_ORDER_DETAILS,
                payload: response.data && response.data.data,
            });

        }

        return response;
    } catch (e) {
        // console.log('getOrderDetails error', e);
    } finally {
        loadingOff(dispatch);
    }
};

export const update_order_status = (data, callback = () => { }) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        // console.log('Action_add_order_data' + JSON.stringify(data));
        const response = await axios.patch(config().order_status, data);
        // console.log('Action_update_order_status_response' + JSON.stringify(response));
        return response;
    } catch (e) {
        // console.log('Action_update_order_status_error' + JSON.stringify(e));
        return e.response;
    } finally {
        loadingOff(dispatch);
    }
};
export const respond_order_status = (data, callback = () => { }) => async (
    dispatch,
    getState,
) => {
    loadingOn(dispatch);
    try {
        // console.log('Action_respond_order_status_data' + JSON.stringify(data));
        const response = await axios.post(config().order_respond, data);
        // console.log('Action_respond_order_status_response' + JSON.stringify(response));
        return response;
    } catch (e) {
        // console.log('Action_respond_order_status_error' + JSON.stringify(e));
        return e.response;
    } finally {
        loadingOff(dispatch);
    }
};