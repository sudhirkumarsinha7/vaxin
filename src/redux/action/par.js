/* eslint-disable prettier/prettier */
import axios from 'axios';

import { config } from '../../config/config';
import {
  GET_PAR_LIST,
  GET_PAR_INFO,
  GET_PAR_LIST_MORE,
  GET_PAR_LIST_PAGE_COUNT
} from '../constant';

import { loadingOff, loadingOn } from './loader';


export const getParList = (page = 1) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().parList + '?page=' + page + '&limit=10');


    console.log('getParList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_PAR_LIST,
    //     payload: response.data.data && response.data.data.data,
    //   });

    // }
    dispatch({
      type: GET_PAR_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_PAR_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.data,
      });
    } else {
      dispatch({
        type: GET_PAR_LIST,
        payload: response.data && response.data.data && response.data.data.data,
      });
    }

    return response;
  } catch (e) {
    // // console.log('getParList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getParInfo = () => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().par_info);


    console.log('getParInfo response', response);
    if (response.status === 200) {
      dispatch({
        type: GET_PAR_INFO,
        payload: response.data.data,
      });

    }

    return response;
  } catch (e) {
    // // console.log('getParInfo error', e);
  } finally {
    loadingOff(dispatch);
  }
};

export const create_Par = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_create_Par_data' + JSON.stringify(data));
    // console.log('create_Par' + JSON.stringify(config().createPar));

    const response = await axios.post(config().createPar, data);
    // console.log('Action_create_Par_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    // console.log('Action_create_Par_error' + JSON.stringify(e));
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};