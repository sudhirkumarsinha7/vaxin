/* eslint-disable prettier/prettier */
import {
  GET_NO_OF_UNIT_UTILIZESD,
  GET_TODAY_VACCINATED_LIST,
  GET_VACCINATED_LIST,
  GET_NO_OF_UNIT_UTILIZESD_ANALYTCS,
  GET_TODAY_VACCINATED_ANALYTCS,
  GET_VACCINATED_ANALYTCS,
  LASTMILE_ANALYTICS,

  GET_NO_OF_UNIT_UTILIZESD_LIST_MORE,
  GET_TODAY_VACCINATED_LIST_MORE,
  GET_VACCINATED_LIST_MORE,
  GET_NO_OF_UNIT_UTILIZESD_LIST_PAGE_COUNT,
  GET_TODAY_VACCINATED_LIST_PAGE_COUNT,
  GET_VACCINATED_LIST_PAGE_COUNT
} from '../constant';


import axios from 'axios';

import { config } from '../../config/config';

import { loadingOff, loadingOn } from './loader';

export const add_benificiary =
  (data, callback = () => { }) =>
    async (dispatch, getState) => {
      loadingOn(dispatch);
      try {
        console.log('Action_add_benificiary_data' + JSON.stringify(data));
        console.log(
          'config().add_beneficiaries url ' +
          JSON.stringify(config().add_beneficiaries),
        );

        const response = await axios.post(config().add_beneficiaries, data);
        console.log('Action_add_benificiary_response' + JSON.stringify(response));
        return response;
      } catch (e) {
        console.log('Action_add_benificiary_error' + JSON.stringify(e));
        return e.response;
      } finally {
        loadingOff(dispatch);
      }
    };

export const getlastMileAnalytcs = () => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().last_mile_analytics);

    // console.log('getlastMileAnalytcs response', response);
    if (response.status === 200) {
      dispatch({
        type: LASTMILE_ANALYTICS,
        payload: response.data.data,
      });
    }

    return response;
  } catch (e) {
    // // console.log('getlastMileAnalytcs error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getNoOfUtilized = (page = 1) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().get_utilized + '?page=' + page + '&limit=10');

    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_NO_OF_UNIT_UTILIZESD,
    //     payload: response.data.data && response.data.data.data,
    //   });
    //   dispatch({
    //     type: GET_NO_OF_UNIT_UTILIZESD_ANALYTCS,
    //     payload: response.data.data && response.data.data.totalRecords,
    //   });
    // }
    dispatch({
      type: GET_NO_OF_UNIT_UTILIZESD_ANALYTCS,
      payload: response.data && response.data.data && response.data.data.totalRecords,
    });
    dispatch({
      type: GET_NO_OF_UNIT_UTILIZESD_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_NO_OF_UNIT_UTILIZESD_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.data,
      });
    } else {
      dispatch({
        type: GET_NO_OF_UNIT_UTILIZESD,
        payload: response.data && response.data.data && response.data.data.data,
      });
    }

    return response;
  } catch (e) {
    console.log('getNoOfUtilized error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getTodayVaccinatedList = (page = 1) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {


    let response = await axios.get(
      config().getBeneficiaries_today + '&page=' + page + '&limit=10',
    );

    console.log('getTodayVaccinatedList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_TODAY_VACCINATED_LIST,
    //     payload: response.data.data && response.data.data.data,
    //   });
    //   dispatch({
    //     type: GET_TODAY_VACCINATED_ANALYTCS,
    //     payload: response.data.data && response.data.data.totalRecords,
    //   });
    // }

    dispatch({
      type: GET_TODAY_VACCINATED_ANALYTCS,
      payload: response.data && response.data.data && response.data.data.totalRecords,
    });
    dispatch({
      type: GET_TODAY_VACCINATED_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_TODAY_VACCINATED_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.data,
      });
    } else {
      dispatch({
        type: GET_TODAY_VACCINATED_LIST,
        payload: response.data && response.data.data && response.data.data.data,
      });
    }

    return response;
  } catch (e) {
    console.log('getTodayVaccinatedList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getVaccinatedList = (page = 1) => async (dispatch, getState) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(
      config().getBeneficiaries + '&page=' + page + '&limit=10',
    );

    // console.log('getVaccinatedList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_VACCINATED_LIST,
    //     payload: response.data.data && response.data.data.data,
    //   });
    //   dispatch({
    //     type: GET_VACCINATED_ANALYTCS,
    //     payload: response.data.data && response.data.data.totalRecords,
    //   });
    // }
    dispatch({
      type: GET_VACCINATED_ANALYTCS,
      payload: response.data && response.data.data && response.data.data.totalRecords,
    });
    dispatch({
      type: GET_VACCINATED_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_VACCINATED_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.data,
      });
    } else {
      dispatch({
        type: GET_VACCINATED_LIST,
        payload: response.data && response.data.data && response.data.data.data,
      });
    }
    return response;
  } catch (e) {
    console.log('getVaccinatedList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
