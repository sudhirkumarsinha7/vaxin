/* eslint-disable prettier/prettier */
import axios from 'axios';

import { config } from '../../config/config';
import {
  GET_COUNTRIES_LIST,
  GET_AIRPORT_LIST,
  GET_VAR_LIST,
  GET_VAR_INFO,
  GET_VAR_LIST_MORE,
  GET_VAR_LIST_PAGE_COUNT
} from '../constant';

import { loadingOff, loadingOn } from './loader';

export const getCountryList = () => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('config().countryList', config().countryList);

    let response = await axios.get(config().countryList);


    console.log('getCountryList response', response);
    if (response.status === 200) {
      dispatch({
        type: GET_COUNTRIES_LIST,
        payload: response.data.data,
      });

    }

    return response;
  } catch (e) {
    console.log('getCountryList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getAirportList = (id) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    console.log('config().airportList', config().airportList + id + '?international=true');

    let response = await axios.get(config().airportList + id + '?international=true');


    console.log('getAirportList response', response);
    if (response.status === 200) {
      dispatch({
        type: GET_AIRPORT_LIST,
        payload: response.data.data,
      });

    }

    return response;
  } catch (e) {
    console.log('getAirportList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getVarList = (page = 1) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().varList + '?page=' + page + '&limit=10');


    console.log('getVarList response', response);
    // if (response.status === 200) {
    //   dispatch({
    //     type: GET_VAR_LIST,
    //     payload: response.data.data && response.data.data.data,
    //   });

    // }
    dispatch({
      type: GET_VAR_LIST_PAGE_COUNT,
      payload: response.data && response.data.data && response.data.data.page,
    });
    if (page > 1) {
      dispatch({
        type: GET_VAR_LIST_MORE,
        payload: response.data && response.data.data && response.data.data.data,
      });
    } else {
      dispatch({
        type: GET_VAR_LIST,
        payload: response.data && response.data.data && response.data.data.data,
      });
    }
    return response;
  } catch (e) {
    // // console.log('getVarList error', e);
  } finally {
    loadingOff(dispatch);
  }
};
export const getVarInfo = () => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    let response = await axios.get(config().var_info);


    console.log('getVarInfo response', response);
    if (response.status === 200) {
      dispatch({
        type: GET_VAR_INFO,
        payload: response.data.data,
      });

    }

    return response;
  } catch (e) {
    // // console.log('getVarInfo error', e);
  } finally {
    loadingOff(dispatch);
  }
};

export const create_Var = (data, callback = () => { }) => async (
  dispatch,
  getState,
) => {
  loadingOn(dispatch);
  try {
    // console.log('Action_create_Var_data' + JSON.stringify(data));
    // console.log('create_Var' + JSON.stringify(config().createVar));

    const response = await axios.post(config().createVar, data);
    // console.log('Action_create_Var_response' + JSON.stringify(response));
    return response;
  } catch (e) {
    // console.log('Action_create_Var_error' + JSON.stringify(e));
    return e.response;
  } finally {
    loadingOff(dispatch);
  }
};