import {
  GET_ALL_NOTIFICATION_LIST,
  GET_ORDER_NOTIFICATION_LIST,
  GET_SHIPMENT_NOTIFICATION_LIST,
  GET_INVENTORY_NOTIFICATION_LIST,
  GET_LASTMILE_NOTIFICATION_LIST,


  GET_ALL_NOTIFICATION_LIST_MORE,
  GET_ORDER_NOTIFICATION_LIST_MORE,
  GET_SHIPMENT_NOTIFICATION_LIST_MORE,
  GET_INVENTORY_NOTIFICATION_LIST_MORE,

  GET_ALL_NOTIFICATION_LIST_PAGE_COUNT,
  GET_ORDER_NOTIFICATION_LIST_PAGE_COUNT,
  GET_SHIPMENT_NOTIFICATION_LIST_PAGE_COUNT,
  GET_INVENTORY_NOTIFICATION_LIST_PAGE_COUNT,
} from '../constant';

const initialState = {
  notifications_list: [],
  order_notifications_list: [],
  shipment_notifications_list: [],
  inventory_notifications_list: [],
  lastmile_notifications_list: [],
  notifications_list_page: 1,
  order_notifications_list_page: 1,
  shipment_notifications_list_page: 1,
  inventory_notifications_list_page: 1,

};
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ALL_NOTIFICATION_LIST:
      return {
        ...state,
        notifications_list: action.payload,
      };
    case GET_ORDER_NOTIFICATION_LIST:
      return {
        ...state,
        order_notifications_list: action.payload,
      };
    case GET_SHIPMENT_NOTIFICATION_LIST:
      return {
        ...state,
        shipment_notifications_list: action.payload,
      };
    case GET_INVENTORY_NOTIFICATION_LIST:
      return {
        ...state,
        inventory_notifications_list: action.payload,
      };
    case GET_ALL_NOTIFICATION_LIST_PAGE_COUNT:
      return {
        ...state,
        notifications_list_page: action.payload,
      };
    case GET_ORDER_NOTIFICATION_LIST_PAGE_COUNT:
      return {
        ...state,
        order_notifications_list_page: action.payload,
      };
    case GET_SHIPMENT_NOTIFICATION_LIST_PAGE_COUNT:
      return {
        ...state,
        shipment_notifications_list_page: action.payload,
      };
    case GET_INVENTORY_NOTIFICATION_LIST_PAGE_COUNT:
      return {
        ...state,
        inventory_notifications_list_page: action.payload,
      };


    case GET_ALL_NOTIFICATION_LIST_MORE:

      return {
        ...state,
        notifications_list: [
          ...state.notifications_list,
          ...action.payload,
        ],
      };
    case GET_ORDER_NOTIFICATION_LIST_MORE:
      return {
        ...state,
        order_notifications_list: [
          ...state.order_notifications_list,
          ...action.payload,
        ],
      };

    case GET_SHIPMENT_NOTIFICATION_LIST_MORE:
      return {
        ...state,
        shipment_notifications_list: [
          ...state.shipment_notifications_list,
          ...action.payload,
        ],
      };

    case GET_INVENTORY_NOTIFICATION_LIST_MORE:

      return {
        ...state,
        inventory_notifications_list: [
          ...state.inventory_notifications_list,
          ...action.payload,
        ],
      };
    case GET_LASTMILE_NOTIFICATION_LIST:
      return {
        ...state,
        lastmile_notifications_list: action.payload,
      };
    default:
      return state;
  }
}
