/* eslint-disable prettier/prettier */
import {
    GET_INBOUND_SUMMARY_LIST,
    GET_INBOUND_DELIVERED_LIST,
    GET_INBOUND_SHIPPED_LIST,
    GET_INBOUND_DAMAGED_LIST,
    GET_OUTBOUND_DAMAGED_LIST,
    GET_OUTBOUND_SHIPPED_LIST,
    GET_OUTBOUND_SUMMARY_LIST,
    GET_OUTBOUND_DELIVERED_LIST,
    GET_SHIPMENT_DETAILS,
    GET_SHIPMENT_TRACK_DETAILS,
    SHIPMENT_INBOUND_ANALYTICS,
    SHIPMENT_OUTBOUND_ANALYTICS,
    GET_INBOUND_SUMMARY_LIST_MORE,
    GET_INBOUND_DELIVERED_LIST_MORE,
    GET_INBOUND_SHIPPED_LIST_MORE,
    GET_INBOUND_DAMAGED_LIST_MORE,
    GET_OUTBOUND_DAMAGED_LIST_MORE,
    GET_OUTBOUND_SHIPPED_LIST_MORE,
    GET_OUTBOUND_SUMMARY_LIST_MORE,
    GET_OUTBOUND_DELIVERED_LIST_MORE,
    GET_INBOUND_SUMMARY_COUNT,
    GET_INBOUND_DELIVERED_COUNT,
    GET_INBOUND_SHIPPED_COUNT,
    GET_INBOUND_DAMAGED_COUNT,
    GET_OUTBOUND_DAMAGED_COUNT,
    GET_OUTBOUND_SHIPPED_COUNT,
    GET_OUTBOUND_SUMMARY_COUNT,
    GET_OUTBOUND_DELIVERED_COUNT,
    GET_INBOUND_SUMMARY_PAGE,
    GET_INBOUND_DELIVERED_PAGE,
    GET_INBOUND_SHIPPED_PAGE,
    GET_INBOUND_DAMAGED_PAGE,
    GET_OUTBOUND_DAMAGED_PAGE,
    GET_OUTBOUND_SHIPPED_PAGE,
    GET_OUTBOUND_SUMMARY_PAGE,
    GET_OUTBOUND_DELIVERED_PAGE,
} from '../constant';
const initialState = {
    inbound_shipment_summary_list: [],
    inbound_shipment_delivery_list: [],
    inbound_shipment_shipped_list: [],
    inbound_shipment_damaged_list: [],
    outbound_shipment_summary_list: [],
    outbound_shipment_delivery_list: [],
    outbound_shipment_shipped_list: [],
    outbound_shipment_damaged_list: [],
    shipment_details: {},
    shipment_track_details: {},
    shipment_inbound_analytcs: {},
    shipment_outbound_analytcs: {},
    inbound_shipment_summary_count: 1,
    inbound_shipment_delivery_count: 1,
    inbound_shipment_shipped_count: 1,
    inbound_shipment_damaged_count: 1,
    outbound_shipment_summary_count: 1,
    outbound_shipment_delivery_count: 1,
    outbound_shipment_shipped_count: 1,
    outbound_shipment_damaged_count: 1,
    inbound_shipment_summary_page: 1,
    inbound_shipment_delivery_page: 1,
    inbound_shipment_shipped_page: 1,
    inbound_shipment_damaged_page: 1,
    outbound_shipment_summary_page: 1,
    outbound_shipment_delivery_page: 1,
    outbound_shipment_shipped_page: 1,
    outbound_shipment_damaged_page: 1,
};
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_INBOUND_SUMMARY_LIST:
            return {
                ...state,
                inbound_shipment_summary_list: action.payload,
            };
        case GET_INBOUND_DELIVERED_LIST:
            return {
                ...state,
                inbound_shipment_delivery_list: action.payload,
            };
        case GET_INBOUND_SHIPPED_LIST:
            return {
                ...state,
                inbound_shipment_shipped_list: action.payload,
            };
        case GET_INBOUND_DAMAGED_LIST:
            return {
                ...state,
                inbound_shipment_damaged_list: action.payload,
            };
        case GET_OUTBOUND_DAMAGED_LIST:
            return {
                ...state,
                outbound_shipment_damaged_list: action.payload,
            };
        case GET_OUTBOUND_DELIVERED_LIST:
            return {
                ...state,
                outbound_shipment_delivery_list: action.payload,
            };
        case GET_OUTBOUND_SHIPPED_LIST:
            return {
                ...state,
                outbound_shipment_shipped_list: action.payload,
            };
        case GET_OUTBOUND_SUMMARY_LIST:
            return {
                ...state,
                outbound_shipment_summary_list: action.payload,
            };
        case GET_SHIPMENT_TRACK_DETAILS:
            return {
                ...state,
                shipment_track_details: action.payload,
            };
        case GET_SHIPMENT_DETAILS:
            return {
                ...state,
                shipment_details: action.payload,
            };
        case SHIPMENT_INBOUND_ANALYTICS:
            return {
                ...state,
                shipment_inbound_analytcs: action.payload,
            };
        case SHIPMENT_OUTBOUND_ANALYTICS:
            return {
                ...state,
                shipment_outbound_analytcs: action.payload,
            };
        case GET_INBOUND_SUMMARY_LIST_MORE:
            return {
                ...state,
                inbound_shipment_summary_list: [
                    ...state.inbound_shipment_summary_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_DELIVERED_LIST_MORE:
            return {
                ...state,
                inbound_shipment_delivery_list: [
                    ...state.inbound_shipment_delivery_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_SHIPPED_LIST_MORE:
            return {
                ...state,
                inbound_shipment_shipped_list: [
                    ...state.inbound_shipment_shipped_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_DAMAGED_LIST_MORE:
            return {
                ...state,
                inbound_shipment_damaged_list: [
                    ...state.inbound_shipment_damaged_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_DAMAGED_LIST_MORE:
            return {
                ...state,
                outbound_shipment_damaged_list: [
                    ...state.outbound_shipment_damaged_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_DELIVERED_LIST_MORE:
            return {
                ...state,
                outbound_shipment_delivery_list: [
                    ...state.outbound_shipment_delivery_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_SHIPPED_LIST_MORE:
            return {
                ...state,
                outbound_shipment_shipped_list: [
                    ...state.outbound_shipment_shipped_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_SUMMARY_LIST_MORE:
            return {
                ...state,
                outbound_shipment_summary_list: [
                    ...state.outbound_shipment_summary_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_SUMMARY_COUNT:
            return {
                ...state,
                inbound_shipment_summary_count: action.payload,
            };
        case GET_INBOUND_DELIVERED_COUNT:
            return {
                ...state,
                inbound_shipment_delivery_count: action.payload,
            };
        case GET_INBOUND_SHIPPED_COUNT:
            return {
                ...state,
                inbound_shipment_shipped_count: action.payload,
            };
        case GET_INBOUND_DAMAGED_COUNT:
            return {
                ...state,
                inbound_shipment_damaged_count: action.payload,
            };
        case GET_OUTBOUND_DAMAGED_COUNT:
            return {
                ...state,
                outbound_shipment_damaged_count: action.payload,
            };
        case GET_OUTBOUND_DELIVERED_COUNT:
            return {
                ...state,
                outbound_shipment_delivery_count: action.payload,
            };
        case GET_OUTBOUND_SHIPPED_COUNT:
            return {
                ...state,
                outbound_shipment_shipped_count: action.payload,
            };
        case GET_OUTBOUND_SUMMARY_COUNT:
            return {
                ...state,
                outbound_shipment_summary_count: action.payload,
            };
        case GET_INBOUND_SUMMARY_PAGE:
            return {
                ...state,
                inbound_shipment_summary_page: action.payload,
            };
        case GET_INBOUND_DELIVERED_PAGE:
            return {
                ...state,
                inbound_shipment_delivery_page: action.payload,
            };
        case GET_INBOUND_SHIPPED_PAGE:
            return {
                ...state,
                inbound_shipment_shipped_page: action.payload,
            };
        case GET_INBOUND_DAMAGED_PAGE:
            return {
                ...state,
                inbound_shipment_damaged_page: action.payload,
            };
        case GET_OUTBOUND_DAMAGED_PAGE:
            return {
                ...state,
                outbound_shipment_damaged_page: action.payload,
            };
        case GET_OUTBOUND_DELIVERED_PAGE:
            return {
                ...state,
                outbound_shipment_delivery_page: action.payload,
            };
        case GET_OUTBOUND_SHIPPED_PAGE:
            return {
                ...state,
                outbound_shipment_shipped_page: action.payload,
            };
        case GET_OUTBOUND_SUMMARY_PAGE:
            return {
                ...state,
                outbound_shipment_summary_page: action.payload,
            };

        default:
            return state;
    }
}
