import {
  GET_NO_OF_UNIT_UTILIZESD,
  GET_TODAY_VACCINATED_LIST,
  GET_VACCINATED_LIST,
  GET_NO_OF_UNIT_UTILIZESD_ANALYTCS,
  GET_TODAY_VACCINATED_ANALYTCS,
  GET_VACCINATED_ANALYTCS,
  LASTMILE_ANALYTICS,

  GET_NO_OF_UNIT_UTILIZESD_LIST_MORE,
  GET_TODAY_VACCINATED_LIST_MORE,
  GET_VACCINATED_LIST_MORE,
  GET_NO_OF_UNIT_UTILIZESD_LIST_PAGE_COUNT,
  GET_TODAY_VACCINATED_LIST_PAGE_COUNT,
  GET_VACCINATED_LIST_PAGE_COUNT
} from '../constant';

const initialState = {
  unit_utilized: [],
  vaccinated_List: [],
  vaccinated_today_List: [],
  unit_utilized_count: 0,
  vaccinated_count: 0,
  vaccinated_today_count: 0,
  lastmile_analytcs: {},
  unit_utilized_page: 1,
  vaccinated_list_page: 1,
  vaccinated_today_list_page: 1,

};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_NO_OF_UNIT_UTILIZESD:
      return {
        ...state,
        unit_utilized: action.payload,
      };
    case GET_TODAY_VACCINATED_LIST:
      return {
        ...state,
        vaccinated_today_List: action.payload,
      };
    case GET_VACCINATED_LIST:
      return {
        ...state,
        vaccinated_List: action.payload,
      };
    case GET_NO_OF_UNIT_UTILIZESD_LIST_PAGE_COUNT:
      return {
        ...state,
        unit_utilized_page: action.payload,
      };
    case GET_TODAY_VACCINATED_LIST_PAGE_COUNT:
      return {
        ...state,
        vaccinated_today_list_page: action.payload,
      };
    case GET_VACCINATED_LIST_PAGE_COUNT:
      return {
        ...state,
        vaccinated_list_page: action.payload,
      };

    case GET_NO_OF_UNIT_UTILIZESD_LIST_MORE:
      return {
        ...state,
        unit_utilized: [
          ...state.unit_utilized,
          ...action.payload,
        ],
      };
    case GET_TODAY_VACCINATED_LIST_MORE:
      return {
        ...state,
        vaccinated_today_List: [
          ...state.vaccinated_today_List,
          ...action.payload,
        ],
      };
    case GET_VACCINATED_LIST_MORE:
      return {
        ...state,
        vaccinated_List: [
          ...state.vaccinated_List,
          ...action.payload,
        ],
      };

    case GET_NO_OF_UNIT_UTILIZESD_ANALYTCS:
      return {
        ...state,
        unit_utilized_count: action.payload,
      };
    case GET_TODAY_VACCINATED_ANALYTCS:
      return {
        ...state,
        vaccinated_today_count: action.payload,
      };
    case GET_VACCINATED_ANALYTCS:
      return {
        ...state,
        vaccinated_count: action.payload,
      };
    case LASTMILE_ANALYTICS:
      return {
        ...state,
        lastmile_analytcs: action.payload,
      };

    default:
      return state;
  }
}
