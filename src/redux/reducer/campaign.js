/* eslint-disable prettier/prettier */
import {
  GET_COMPAIGN_LIST,
  GET_COMPAIGN_ACTIVE_LIST,
  GET_COMPAIGN_LIST_MORE,
  GET_COMPAIGN_ACTIVE_LIST_MORE,
  GET_COMPAIGN_PAGE_COUNT,
  GET_COMPAIGN_ACTIVE_PAGE_COUNT
} from '../constant';

const initialState = {
  campaign_list: [],
  campaign_active_list: [],
  campaign_page: 1,
  campaign_active_page: 1,

};
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_COMPAIGN_LIST:
      return {
        ...state,
        campaign_list: action.payload,
      };
    case GET_COMPAIGN_ACTIVE_LIST:
      return {
        ...state,
        campaign_active_list: action.payload,
      };
    case GET_COMPAIGN_PAGE_COUNT:
      return {
        ...state,
        campaign_page: action.payload,
      };
    case GET_COMPAIGN_ACTIVE_PAGE_COUNT:
      return {
        ...state,
        campaign_active_page: action.payload,
      };
    case GET_COMPAIGN_LIST_MORE:
      return {
        ...state,
        campaign_list: [
          ...state.campaign_list,
          ...action.payload,
        ],
      };
    case GET_COMPAIGN_ACTIVE_LIST_MORE:
      return {
        ...state,
        campaign_active_list: [
          ...state.campaign_active_list,
          ...action.payload,
        ],
      };
    default:
      return state;
  }
}
