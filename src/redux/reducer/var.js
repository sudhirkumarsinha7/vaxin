/* eslint-disable sort-keys */
import {
  GET_COUNTRIES_LIST,
  GET_AIRPORT_LIST,
  GET_VAR_LIST,
  GET_VAR_INFO,
  GET_VAR_LIST_MORE,
  GET_VAR_LIST_PAGE_COUNT
} from '../constant';

const initialState = {
  country_list: [],
  airport_list: [],
  var_list: [],
  var_info: {},
  var_list_page: 1,

};
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_COUNTRIES_LIST:
      return {
        ...state,
        country_list: action.payload,
      };
    case GET_AIRPORT_LIST:
      return {
        ...state,
        airport_list: action.payload,
      };
    case GET_VAR_LIST:
      return {
        ...state,
        var_list: action.payload,
      };
    case GET_VAR_LIST_MORE:
      return {
        ...state,
        var_list: [
          ...state.var_list,
          ...action.payload,
        ],
      };
    case GET_VAR_INFO:
      return {
        ...state,
        var_info: action.payload,
      };
    case GET_VAR_LIST_PAGE_COUNT:
      return {
        ...state,
        var_list_page: action.payload,
      };
    default:
      return state;
  }
}
