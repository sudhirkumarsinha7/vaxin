/* eslint-disable prettier/prettier */
import AsyncStorage from '@react-native-async-storage/async-storage';
import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query';
import {
  persistReducer,
  persistStore,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from 'redux-persist';
import auth from './auth';
import inventory from './inventory';
import order from './order';
import shipment from './shipment';
import Var from './var';
import Par from './par';
import campaign from './campaign';
import coldchain from './coldchain';
import notification from './notification';
import beneficiary from './beneficiary';

import { api } from '../Services/api';
import theme from './slice.reducer';

const persistConfigUser = {
  key: 'auth',
  storage: AsyncStorage,
  whitelist: [
    'userInfo',
    'userLocation',
    'userLocationList',
    'userPermissions',
  ],
};

const persistConfigOrder = {
  key: 'order',
  storage: AsyncStorage,
  whitelist: [
    'inbound_history_list',
    'inbound_received_list',
    'inbound_accepted_list',
    'inbound_rejected_list',
    'outbound_history_list',
    'outbound_sent_list',
    'outbound_accepted_list',
    'outbound_rejected_list',
    'order_delivery_loc',
    'order_supplier_loc',
    'accepted_order_list',
    'order_details',
    'order_inbound_analytcs',
    'order_outbound_analytcs',
    'inbound_accepted_partial_list',
    'outbound_transfer_list',
  ],
};

const persistConfigInventory = {
  key: 'inventory',
  storage: AsyncStorage,

  whitelist: [
    'inv_summary_list',
    'inv_stock_list',
    'inv_out_of_stock_list',
    'inv_near_exp_list',
    'inv_exp_list',
    'inv_net_utilization_list',
    'inv_min_max_list',
    'inv_product_list',
    'inv_prod_list_by_Bath',
    'inv_analytics',
    'inv_list',
    'cold_storage_list',
  ],
};

const persistConfigShipment = {
  key: 'shipment',
  storage: AsyncStorage,
  whitelist: [
    'inbound_shipment_summary_list',
    'inbound_shipment_delivery_list',
    'inbound_shipment_shipped_list',
    'inbound_shipment_damaged_list',
    'outbound_shipment_summary_list',
    'outbound_shipment_delivery_list',
    'outbound_shipment_shipped_list',
    'outbound_shipment_damaged_list',
    'shipment_details',
    'shipment_inbound_analytcs',
    'shipment_outbound_analytcs',
  ],
};

const persistConfigBwnifiary = {
  key: 'beneficiary',
  storage: AsyncStorage,
  whitelist: [
    'unit_utilized',
    'vaccinated_List',
    'vaccinated_today_List',
    'unit_utilized_count',
    'vaccinated_count',
    'vaccinated_today_count',
  ],
};
const persistConfigVar = {
  key: 'Var',
  storage: AsyncStorage,
  whitelist: ['country_list', 'var_list'],
};
const persistConfigPar = {
  key: 'Par',
  storage: AsyncStorage,
  whitelist: ['par_list'],
};
const persistConfigColdChain = {
  key: 'coldchain',
  storage: AsyncStorage,
  whitelist: ['coldcahin_list', 'assets_list'],
};
const persistConfigCompaign = {
  key: 'campaign',
  storage: AsyncStorage,
  whitelist: ['campaign_list'],
};
const persistConfigNotification = {
  key: 'notifications',
  storage: AsyncStorage,
  whitelist: ['notifications_list'],
};
const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['theme'],
};

// const persistedReducer = persistReducer(persistConfig, reducers)
const reducers = combineReducers({
  theme,
  api: api.reducer,
  auth: persistReducer(persistConfigUser, auth),
  inventory: persistReducer(persistConfigInventory, inventory),
  order: persistReducer(persistConfigOrder, order),
  shipment: persistReducer(persistConfigShipment, shipment),
  Var: persistReducer(persistConfigVar, Var),
  Par: persistReducer(persistConfigPar, Par),
  coldchain: persistReducer(persistConfigColdChain, coldchain),
  beneficiary: persistReducer(persistConfigBwnifiary, beneficiary),
  campaign: persistReducer(persistConfigCompaign, campaign),
  notifications: persistReducer(persistConfigNotification, notification),
});
const Store = configureStore({
  reducer: reducers,
  middleware: getDefaultMiddleware => {
    const middlewares = getDefaultMiddleware({
      // serializableCheck: {
      //   ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      // },
      immutableCheck: false,
      serializableCheck: false,
    }).concat(api.middleware);

    // if (__DEV__ && !process.env.JEST_WORKER_ID) {
    //   const createDebugger = require('redux-flipper').default
    //   middlewares.push(createDebugger())
    // }

    return middlewares;
  },
});

const persistor = persistStore(Store);

setupListeners(Store.dispatch);

export { Store, persistor };
