/* eslint-disable prettier/prettier */
import {
    GET_INBOUND_HISTORY_PAGE_COUNT,
    GET_INBOUND_RECEIVED_PAGE_COUNT,
    GET_INBOUND_ACCEPTED_PAGE_COUNT,
    GET_INBOUND_REJECTED_PAGE_COUNT,
    GET_INBOUND_ORDER_SHIPPED_PAGE_COUNT,
    GET_INBOUND_PARTIAL_ORDER_SHIPPED_PAGE_COUNT,
    GET_INBOUND_FULLFILL_PAGE_COUNT,
    GET_INBOUND_PARTIAL_FULLFILL_PAGE_COUNT,
    GET_INBOUND_CANCELLED_PAGE_COUNT,

    GET_INBOUND_HISTORY_LIST,
    GET_INBOUND_RECEIVED_LIST,
    GET_INBOUND_ACCEPTED_LIST,
    GET_INBOUND_REJECTED_LIST,
    GET_INBOUND_ORDER_SHIPPED_LIST,
    GET_INBOUND_PARTIAL_ORDER_SHIPPED_LIST,
    GET_INBOUND_FULLFILL_LIST,
    GET_INBOUND_PARTIAL_FULLFILL_LIST,
    GET_INBOUND_CANCELLED_LIST,

    GET_INBOUND_HISTORY_LIST_MORE,
    GET_INBOUND_RECEIVED_LIST_MORE,
    GET_INBOUND_ACCEPTED_LIST_MORE,
    GET_INBOUND_REJECTED_LIST_MORE,
    GET_INBOUND_ORDER_SHIPPED_LIST_MORE,
    GET_INBOUND_PARTIAL_ORDER_SHIPPED_LIST_MORE,
    GET_INBOUND_FULLFILL_LIST_MORE,
    GET_INBOUND_PARTIAL_FULLFILL_LIST_MORE,
    GET_INBOUND_CANCELLED_LIST_MORE,

    GET_OUTBOUND_REJECTED_PAGE_COUNT,
    GET_OUTBOUND_ACCEPTED_PAGE_COUNT,
    GET_OUTBOUND_HISTORY_PAGE_COUNT,
    GET_OUTBOUND_SENT_PAGE_COUNT,
    GET_OUTBOUND_TRANSFER_PAGE_COUNT,
    GET_OUTBOUND_ORDER_SHIPPED_PAGE_COUNT,
    GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_PAGE_COUNT,
    GET_OUTBOUND_FULLFILL_PAGE_COUNT,
    GET_OUTBOUND_PARTIAL_FULLFILL_PAGE_COUNT,
    GET_OUTBOUND_CANCELLED_PAGE_COUNT,

    GET_OUTBOUND_REJECTED_LIST,
    GET_OUTBOUND_ACCEPTED_LIST,
    GET_OUTBOUND_HISTORY_LIST,
    GET_OUTBOUND_SENT_LIST,
    GET_OUTBOUND_TRANSFER_LIST,
    GET_OUTBOUND_ORDER_SHIPPED_LIST,
    GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_LIST,
    GET_OUTBOUND_FULLFILL_LIST,
    GET_OUTBOUND_PARTIAL_FULLFILL_LIST,
    GET_OUTBOUND_CANCELLED_LIST,

    GET_OUTBOUND_ACCEPTED_LIST_MORE,
    GET_OUTBOUND_HISTORY_LIST_MORE,
    GET_OUTBOUND_SENT_LIST_MORE,
    GET_OUTBOUND_TRANSFER_LIST_MORE,
    GET_OUTBOUND_REJECTED_LIST_MORE,
    GET_OUTBOUND_ORDER_SHIPPED_LIST_MORE,
    GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_LIST_MORE,
    GET_OUTBOUND_FULLFILL_LIST_MORE,
    GET_OUTBOUND_PARTIAL_FULLFILL_LIST_MORE,
    GET_OUTBOUND_CANCELLED_LIST_MORE,

    GET_DELIVERY_LOCATION,
    GET_SUPPLIER_LOCATION,
    GET_ORDER_DETAILS,
    GET_ACCEPTED_ORDER_LIST,
    ORDER_INBOUND_ANALYTCS,
    ORDER_OUTBOUND_ANALYTCS,
    GET_INBOUND_ACCEPTED_PARTIAL_LIST,
    GET_INBOUND_SHIPPED_LIST,
} from '../constant';
const initialState = {
    inbound_history_list: [],
    inbound_received_list: [],
    inbound_accepted_list: [],
    inbound_rejected_list: [],
    inbound_shipped_order_list: [],
    inbound_partial_shipped_order_list: [],
    inbound_fullfill_order_list: [],
    inbound_partilal_fullfill_order_list: [],
    inbound_canceled_order_list: [],

    outbound_history_list: [],
    outbound_sent_list: [],
    outbound_accepted_list: [],
    outbound_rejected_list: [],
    outbound_transfer_list: [],
    outbound_shipped_order_list: [],
    outbound_partial_shipped_order_list: [],
    outbound_fullfill_order_list: [],
    outbound_canceled_order_list: [],
    outbound_partilal_fullfill_order_list: [],
    order_delivery_loc: [],
    order_supplier_loc: [],
    accepted_order_list: [],
    order_details: {},
    order_inbound_analytcs: {},
    order_outbound_analytcs: {},
    inbound_accepted_partial_list: [],
    inbound_order_history_page: 1,
    inbound_order_received_page: 1,
    inbound_order_accepted_page: 1,
    inbound_order_rejected_page: 1,
    inbound_shipped_order_page: 1,
    inbound_partial_shipped_order_page: 1,
    inbound_fullfill_order_page: 1,
    inbound_partilal_fullfill_page: 1,
    inbound_canceled_page: 1,

    outbound_order_history_page: 1,
    outbound_order_sent_page: 1,
    outbound_order_accepted_page: 1,
    outbound_order_rejected_page: 1,
    outbound_order_transfer_page: 1,
    outbound_shipped_order_page: 1,
    outbound_partial_shipped_order_page: 1,
    outbound_fullfill_order_page: 1,
    outbound_partilal_fullfill_page: 1,
    outbound_canceled_page: 1,
};
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_INBOUND_HISTORY_LIST:
            return {
                ...state,
                inbound_history_list: action.payload,
            };
        case GET_INBOUND_RECEIVED_LIST:
            return {
                ...state,
                inbound_received_list: action.payload,
            };
        case GET_INBOUND_ACCEPTED_LIST:
            return {
                ...state,
                inbound_accepted_list: action.payload,
            };
        case GET_INBOUND_ACCEPTED_PARTIAL_LIST:
            return {
                ...state,
                inbound_accepted_partial_list: action.payload,
            };
        case GET_INBOUND_REJECTED_LIST:
            return {
                ...state,
                inbound_rejected_list: action.payload,
            };
        case GET_INBOUND_ORDER_SHIPPED_LIST:
            return {
                ...state,
                inbound_shipped_order_list: action.payload,
            };
        case GET_INBOUND_PARTIAL_ORDER_SHIPPED_LIST:
            return {
                ...state,
                inbound_partial_shipped_order_list: action.payload,
            };
        case GET_INBOUND_FULLFILL_LIST:
            return {
                ...state,
                inbound_fullfill_order_list: action.payload,
            };
        case GET_INBOUND_PARTIAL_FULLFILL_LIST:
            return {
                ...state,
                inbound_partilal_fullfill_order_list: action.payload,
            };
        case GET_INBOUND_CANCELLED_LIST:
            return {
                ...state,
                inbound_canceled_order_list: action.payload,
            };

        case GET_INBOUND_HISTORY_LIST_MORE:
            return {
                ...state,
                inbound_history_list: [
                    ...state.inbound_history_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_RECEIVED_LIST_MORE:
            return {
                ...state,
                inbound_received_list: [
                    ...state.inbound_received_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_ACCEPTED_LIST_MORE:
            return {
                ...state,
                inbound_accepted_list: [
                    ...state.inbound_accepted_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_REJECTED_LIST_MORE:
            return {
                ...state,
                inbound_rejected_list: [
                    ...state.inbound_rejected_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_ORDER_SHIPPED_LIST_MORE:
            return {
                ...state,
                inbound_shipped_order_list: [
                    ...state.inbound_shipped_order_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_PARTIAL_ORDER_SHIPPED_LIST_MORE:
            return {
                ...state,
                inbound_partial_shipped_order_list: [
                    ...state.inbound_partial_shipped_order_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_FULLFILL_LIST_MORE:
            return {
                ...state,
                inbound_fullfill_order_list: [
                    ...state.inbound_fullfill_order_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_PARTIAL_FULLFILL_LIST_MORE:
            return {
                ...state,
                inbound_partilal_fullfill_order_list: [
                    ...state.inbound_partilal_fullfill_order_list,
                    ...action.payload,
                ],
            };
        case GET_INBOUND_CANCELLED_LIST_MORE:
            return {
                ...state,
                inbound_canceled_order_list: [
                    ...state.inbound_canceled_order_list,
                    ...action.payload,
                ],
            };


        case GET_INBOUND_HISTORY_PAGE_COUNT:
            return {
                ...state,
                inbound_order_history_page: action.payload,
            };
        case GET_INBOUND_RECEIVED_PAGE_COUNT:
            return {
                ...state,
                inbound_order_received_page: action.payload,
            };
        case GET_INBOUND_ACCEPTED_PAGE_COUNT:
            return {
                ...state,
                inbound_order_accepted_page: action.payload,
            };
        case GET_INBOUND_REJECTED_PAGE_COUNT:
            return {
                ...state,
                inbound_order_rejected_page: action.payload,
            };
        case GET_INBOUND_ORDER_SHIPPED_PAGE_COUNT:
            return {
                ...state,
                inbound_shipped_order_page: action.payload,
            };
        case GET_INBOUND_PARTIAL_ORDER_SHIPPED_PAGE_COUNT:
            return {
                ...state,
                inbound_partial_shipped_order_page: action.payload,
            };
        case GET_INBOUND_FULLFILL_PAGE_COUNT:
            return {
                ...state,
                inbound_fullfill_order_page: action.payload,
            };
        case GET_INBOUND_PARTIAL_FULLFILL_PAGE_COUNT:
            return {
                ...state,
                inbound_partilal_fullfill_page: action.payload,
            };
        case GET_INBOUND_CANCELLED_PAGE_COUNT:
            return {
                ...state,
                inbound_canceled_page: action.payload,
            };

        case GET_OUTBOUND_HISTORY_LIST:
            return {
                ...state,
                outbound_history_list: action.payload,
            };
        case GET_OUTBOUND_SENT_LIST:
            return {
                ...state,
                outbound_sent_list: action.payload,
            };
        case GET_OUTBOUND_ACCEPTED_LIST:
            return {
                ...state,
                outbound_accepted_list: action.payload,
            };
        case GET_OUTBOUND_REJECTED_LIST:
            return {
                ...state,
                outbound_rejected_list: action.payload,
            };
        case GET_OUTBOUND_TRANSFER_LIST:
            return {
                ...state,
                outbound_transfer_list: action.payload,
            };
        case GET_OUTBOUND_ORDER_SHIPPED_LIST:
            return {
                ...state,
                outbound_shipped_order_list: action.payload,
            };
        case GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_LIST:
            return {
                ...state,
                outbound_partial_shipped_order_list: action.payload,
            };
        case GET_OUTBOUND_FULLFILL_LIST:
            return {
                ...state,
                outbound_fullfill_order_list: action.payload,
            };
        case GET_OUTBOUND_PARTIAL_FULLFILL_LIST:
            return {
                ...state,
                outbound_partilal_fullfill_order_list: action.payload,
            };
        case GET_OUTBOUND_CANCELLED_LIST:
            return {
                ...state,
                outbound_canceled_order_list: action.payload,
            };

        case GET_OUTBOUND_HISTORY_PAGE_COUNT:
            return {
                ...state,
                outbound_order_history_page: action.payload,
            };
        case GET_OUTBOUND_SENT_PAGE_COUNT:
            return {
                ...state,
                outbound_order_sent_page: action.payload,
            };
        case GET_OUTBOUND_REJECTED_PAGE_COUNT:
            return {
                ...state,
                outbound_order_rejected_page: action.payload,
            };
        case GET_OUTBOUND_ACCEPTED_PAGE_COUNT:
            return {
                ...state,
                outbound_order_accepted_page: action.payload,
            };
        case GET_OUTBOUND_TRANSFER_PAGE_COUNT:
            return {
                ...state,
                outbound_order_transfer_page: action.payload,
            };
        case GET_OUTBOUND_ORDER_SHIPPED_PAGE_COUNT:
            return {
                ...state,
                outbound_shipped_order_page: action.payload,
            };
        case GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_PAGE_COUNT:
            return {
                ...state,
                outbound_partial_shipped_order_page: action.payload,
            };
        case GET_OUTBOUND_FULLFILL_PAGE_COUNT:
            return {
                ...state,
                outbound_fullfill_order_page: action.payload,
            };
        case GET_OUTBOUND_PARTIAL_FULLFILL_PAGE_COUNT:
            return {
                ...state,
                outbound_partilal_fullfill_page: action.payload,
            };
        case GET_OUTBOUND_CANCELLED_PAGE_COUNT:
            return {
                ...state,
                outbound_canceled_page: action.payload,
            };



        case GET_OUTBOUND_HISTORY_LIST_MORE:

            return {
                ...state,
                outbound_history_list: [
                    ...state.outbound_history_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_SENT_LIST_MORE:

            return {
                ...state,
                outbound_sent_list: [
                    ...state.outbound_sent_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_ACCEPTED_LIST_MORE:

            return {
                ...state,
                outbound_accepted_list: [
                    ...state.outbound_accepted_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_REJECTED_LIST_MORE:

            return {
                ...state,
                outbound_rejected_list: [
                    ...state.outbound_rejected_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_TRANSFER_LIST_MORE:
            return {
                ...state,
                outbound_transfer_list: [
                    ...state.outbound_transfer_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_ORDER_SHIPPED_LIST_MORE:
            return {
                ...state,
                outbound_shipped_order_list: [
                    ...state.outbound_shipped_order_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_PARTIAL_ORDER_SHIPPED_LIST_MORE:
            return {
                ...state,
                outbound_partial_shipped_order_list: [
                    ...state.outbound_partial_shipped_order_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_FULLFILL_LIST_MORE:
            return {
                ...state,
                outbound_fullfill_order_list: [
                    ...state.outbound_fullfill_order_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_PARTIAL_FULLFILL_LIST_MORE:
            return {
                ...state,
                outbound_partilal_fullfill_order_list: [
                    ...state.outbound_partilal_fullfill_order_list,
                    ...action.payload,
                ],
            };
        case GET_OUTBOUND_CANCELLED_LIST_MORE:
            return {
                ...state,
                outbound_canceled_order_list: [
                    ...state.outbound_canceled_order_list,
                    ...action.payload,
                ],
            };

        case GET_DELIVERY_LOCATION:
            return {
                ...state,
                order_delivery_loc: action.payload,
            };
        case GET_SUPPLIER_LOCATION:
            return {
                ...state,
                order_supplier_loc: action.payload,
            };
        case GET_ORDER_DETAILS:
            return {
                ...state,
                order_details: action.payload,
            };
        case GET_ACCEPTED_ORDER_LIST:
            return {
                ...state,
                accepted_order_list: action.payload,
            };
        case ORDER_INBOUND_ANALYTCS:
            return {
                ...state,
                order_inbound_analytcs: action.payload,
            };
        case ORDER_OUTBOUND_ANALYTCS:
            return {
                ...state,
                order_outbound_analytcs: action.payload,
            };
        default:
            return state;
    }
}
