/* eslint-disable prettier/prettier */
import {
    GET_INV_SUMMARY,
    GET_INV_STOCK,
    GET_INV_OUT_STOCK,
    GET_INV_NEAR_EXPIRE,
    GET_INV_EXPIRED,
    GET_INV_REJECTED,
    GET_INV_NET_UTILIZATION,
    GET_INV_MINMAX,

    GET_INV_SUMMARY_MORE,
    GET_INV_STOCK_MORE,
    GET_INV_OUT_STOCK_MORE,
    GET_INV_NEAR_EXPIRE_MORE,
    GET_INV_EXPIRED_MORE,
    GET_INV_REJECTED_MORE,
    GET_INV_NET_UTILIZATION_MORE,
    GET_INV_MINMAX_MORE,

    GET_INV_SUMMARY_PAGE_COUNT,
    GET_INV_STOCK_PAGE_COUNT,
    GET_INV_OUT_STOCK_PAGE_COUNT,
    GET_INV_NEAR_EXPIRE_PAGE_COUNT,
    GET_INV_EXPIRED_PAGE_COUNT,
    GET_INV_REJECTED_PAGE_COUNT,
    GET_INV_NET_UTILIZATION_PAGE_COUNT,
    GET_INV_MINMAX_PAGE_COUNT,

    GET_INV_PRODUCT_LIST,
    GET_PRODUCT_LIST_BY_BATCH,
    INVENTORY_ANALYTICS,
    GET_COLDSTORAGE_LIST,
    GET_INV_LIST,
    GET_INV_VALUE
} from '../constant';
const initialState = {
    inv_summary_list: [],
    inv_stock_list: [],
    inv_out_of_stock_list: [],
    inv_near_exp_list: [],
    inv_exp_list: [],
    inv_net_utilization_list: [],
    inv_rejected_list: [],
    inv_min_max_list: [],

    inv_product_list: [],
    inv_prod_list_by_Bath: [],
    inv_analytics: {},
    inv_value: {},
    inv_list: [],
    cold_storage_list: [],

    inv_summary_page: 1,
    inv_instock_page: 1,
    inv_outstock_page: 1,
    inv_nearexpired_page: 1,
    inv_expired_page: 1,
    inv_netutilized_page: 1,
    inv_rejqty_page: 1,
    inv_minmax_page: 1,
};
export default function (state = initialState, action) {
    switch (action.type) {
        case GET_INV_SUMMARY:
            return {
                ...state,
                inv_summary_list: action.payload,
            };
        case GET_INV_REJECTED:
            return {
                ...state,
                inv_rejected_list: action.payload,
            };
        case GET_INV_STOCK:
            return {
                ...state,
                inv_stock_list: action.payload,
            };
        case GET_INV_OUT_STOCK:
            return {
                ...state,
                inv_out_of_stock_list: action.payload,
            };
        case GET_INV_NEAR_EXPIRE:
            return {
                ...state,
                inv_near_exp_list: action.payload,
            };
        case GET_INV_EXPIRED:
            return {
                ...state,
                inv_exp_list: action.payload,
            };
        case GET_INV_MINMAX:
            return {
                ...state,
                inv_min_max_list: action.payload,
            };
        case GET_INV_NET_UTILIZATION:
            return {
                ...state,
                inv_net_utilization_list: action.payload,
            };
        case GET_INV_SUMMARY_PAGE_COUNT:
            return {
                ...state,
                inv_summary_page: action.payload,
            };
        case GET_INV_STOCK_PAGE_COUNT:
            return {
                ...state,
                inv_instock_page: action.payload,
            };
        case GET_INV_OUT_STOCK_PAGE_COUNT:
            return {
                ...state,
                inv_outstock_page: action.payload,
            };
        case GET_INV_NEAR_EXPIRE_PAGE_COUNT:
            return {
                ...state,
                inv_nearexpired_page: action.payload,
            };
        case GET_INV_EXPIRED_PAGE_COUNT:
            return {
                ...state,
                inv_expired_page: action.payload,
            };
        case GET_INV_NET_UTILIZATION_PAGE_COUNT:
            return {
                ...state,
                inv_netutilized_page: action.payload,
            };
        case GET_INV_REJECTED_PAGE_COUNT:
            return {
                ...state,
                inv_rejqty_page: action.payload,
            };
        case GET_INV_MINMAX_PAGE_COUNT:
            return {
                ...state,
                inv_minmax_page: action.payload,
            };

        case GET_INV_MINMAX_MORE:
            return {
                ...state,
                inv_min_max_list: [
                    ...state.inv_min_max_list,
                    ...action.payload,
                ],
            };


        case GET_INV_SUMMARY_MORE:
            return {
                ...state,
                inv_summary_list: [
                    ...state.inv_summary_list,
                    ...action.payload,
                ],
            };

        case GET_INV_STOCK_MORE:
            return {
                ...state,
                inv_stock_list: [
                    ...state.inv_stock_list,
                    ...action.payload,
                ],
            };
        case GET_INV_OUT_STOCK_MORE:
            return {
                ...state,
                inv_out_of_stock_list: [
                    ...state.inv_out_of_stock_list,
                    ...action.payload,
                ],
            };
        case GET_INV_NEAR_EXPIRE_MORE:
            return {
                ...state,
                inv_near_exp_list: [
                    ...state.inv_near_exp_list,
                    ...action.payload,
                ],
            };
        case GET_INV_EXPIRED_MORE:
            return {
                ...state,
                inv_exp_list: [
                    ...state.inv_exp_list,
                    ...action.payload,
                ],
            };
        case GET_INV_NET_UTILIZATION_MORE:
            return {
                ...state,
                inv_net_utilization_list: [
                    ...state.inv_net_utilization_list,
                    ...action.payload,
                ],
            };
        case GET_INV_REJECTED_MORE:
            return {
                ...state,
                inv_rejected_list: [
                    ...state.inv_rejected_list,
                    ...action.payload,
                ],
            };


        case GET_INV_PRODUCT_LIST:
            return {
                ...state,
                inv_product_list: action.payload,
            };

        case GET_PRODUCT_LIST_BY_BATCH:
            return {
                ...state,
                inv_prod_list_by_Bath: action.payload,
            };
        case INVENTORY_ANALYTICS:
            return {
                ...state,
                inv_analytics: action.payload,
            };
        case GET_INV_VALUE:
            return {
                ...state,
                inv_value: action.payload,
            };
        case GET_COLDSTORAGE_LIST:
            return {
                ...state,
                cold_storage_list: action.payload,
            };
        case GET_INV_LIST:
            return {
                ...state,
                inv_list: action.payload,
            };
        default:
            return state;
    }
}
