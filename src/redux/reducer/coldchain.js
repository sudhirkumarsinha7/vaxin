import {
  GET_COLDCHAIN_LIST,
  GET_COLDCHAIN_INFO,
  GET_ASSETS_LIST,
} from '../constant';

const initialState = {
  coldcahin_list: [],
  assets_list: [],
  coldcahin_info: {},
};
export default function (state = initialState, action) {
  switch (action.type) {
    case GET_COLDCHAIN_LIST:
      return {
        ...state,
        coldcahin_list: action.payload,
      };
    case GET_ASSETS_LIST:
      return {
        ...state,
        assets_list: action.payload,
      };
    case GET_COLDCHAIN_INFO:
      return {
        ...state,
        coldcahin_info: action.payload,
      };

    default:
      return state;
  }
}
