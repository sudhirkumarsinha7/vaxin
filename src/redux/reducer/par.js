/* eslint-disable prettier/prettier */
/* eslint-disable sort-keys */
import {

  GET_PAR_LIST,
  GET_PAR_INFO,
  GET_PAR_LIST_MORE,
  GET_PAR_LIST_PAGE_COUNT
} from '../constant';

const initialState = {

  par_list: [],
  par_info: {},
  par_list_page: 1,
};
export default function (state = initialState, action) {
  switch (action.type) {

    case GET_PAR_LIST:
      return {
        ...state,
        par_list: action.payload,
      };
    case GET_PAR_LIST_MORE:
      return {
        ...state,
        par_list: [
          ...state.par_list,
          ...action.payload,
        ],
      };
    case GET_PAR_INFO:
      return {
        ...state,
        par_info: action.payload,
      };
    case GET_PAR_LIST_PAGE_COUNT:
      return {
        ...state,
        par_list_page: action.payload,
      };
    default:
      return state;
  }
}
