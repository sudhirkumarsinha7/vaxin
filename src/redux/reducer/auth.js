import {
  USERINFO,
  GET_ORG_LEVEL,
  GET_ORG_LOCATION,
  GET_ORG_LOCATION_LIST,
  LOG_OUT,
  PERMISSIONS,
  GET_LOCATION_LIST,
} from '../constant';
const initialState = {
  userInfo: {},
  orgLevels: [],
  userLocation: {},
  userLocationList: {},
  locationList: [],
  userPermissions: {},
  initialState: {
    theme: {},
    api: {},
    home: {},
    auth: {},
    inventory: {},
    order: {},
    shipment: {},
  },
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_ORG_LEVEL:
      return {
        ...state,
        orgLevels: action.payload,
      };
    case USERINFO:
      return {
        ...state,
        userInfo: action.payload,
      };
    case PERMISSIONS:
      return {
        ...state,
        userPermissions: action.payload,
      };
    case GET_ORG_LOCATION:
      return {
        ...state,
        userLocation: action.payload,
      };
    case GET_ORG_LOCATION_LIST:
      return {
        ...state,
        userLocationList: action.payload,
      };
    case GET_LOCATION_LIST:
      return {
        ...state,
        locationList: action.payload,
      };
    case LOG_OUT:
      return {
        state: initialState,
      };

    default:
      return state;
  }
}
