// Flavor.js
// Variable that holds the information for current product flavor
import {
    Platform,
    NativeModules
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

const BuildConfig = require('react-native-build-config');

const isAndroid = (Platform.OS == "android") ? true : false
const isiOS = (Platform.OS == "ios") ? true : false
// A function to get the current product flavor using bridge methods that we created.
// Since bridge methods are asynchronous this method return a promise which is resolved
// when the flavor identifier has been fetched from native
async function buildFlavor() {
    return new Promise(async function (resolve, reject) {
        const { default: productFlavour = {} } = BuildConfig
        if (isiOS) {
            if (productFlavour.CFBundleName == 'Farview') {
                await AsyncStorage.setItem('productFlavour', 'FARVIEW');

            } else {
                await AsyncStorage.setItem('productFlavour', 'VAXIN');

            }
            // // adding Flavor manually
            var buildTarget = {
                FLAVOR: "Vaxinlite" // options : "Vaxin", "Vaxinlite", "Farview",
            }
            resolve(buildTarget)
        } else {
            if (productFlavour.FLAVOR == 'FARVIEW') {
                await AsyncStorage.setItem('productFlavour', 'FARVIEW');

            } else {
                await AsyncStorage.setItem('productFlavour', 'VAXIN');

            }
            resolve(BuildConfig)
        }
    });
}
// Export the flavor variable and buildFlavor method
exports.buildFlavor = buildFlavor;
