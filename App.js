/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable prettier/prettier */
import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import { RootSiblingParent } from 'react-native-root-siblings';
import Root from './src/navigator/Navigation';
import { Store, persistor } from './src/redux/reducer';
import { AutocompleteDropdownContextProvider } from 'react-native-autocomplete-dropdown';
import { GlobalProvider } from './src/Helpers/GlobalContext';
import atob from 'core-js-pure/stable/atob';
import btoa from 'core-js-pure/stable/btoa';
import { buildFlavor } from './flavors';
import PushNotification from 'react-native-push-notification';
global.atob = atob;
global.btoa = btoa;

const App = props => {
  // useEffect(() => {
  //   buildFlavor().then((nativeCB) => {
  //     console.log('App nativeCB ' + JSON.stringify(nativeCB))
  //   });
  // }, []);

  useEffect(() => {
    buildFlavor().then(nativeCB => {
      console.log('App nativeCB ' + JSON.stringify(nativeCB));
    });
    PushNotification.configure({
      onNotification: function (notification) {
        // Handle received notifications
        console.log('Notification received:', notification);
      },
      onNotificationOpened: function (notification) {
        // Handle notification clicks
        console.log('Notification clicked:', notification);
        navigateToScreen(notification);
      },
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      // Android only: GCM or FCM Sender ID
      senderID: '246292311598',
      popInitialNotification: true,
      requestPermissions: true,
      // Other configuration options
    });
  }, []);
  const navigateToScreen = notification => {
    console.log('notification ', notification);
    // const screenName = notification.data.screen; // Assuming the screen name is passed in notification data
    // if (screenName) {
    //   navigationRef.current?.navigate(screenName);
    // }
    props.navigation.navigate('notification');
  };
  return (
    <AutocompleteDropdownContextProvider>
      <RootSiblingParent>
        <Provider store={Store}>
          <PersistGate loading={null} persistor={persistor}>
            <GlobalProvider>
              <Root />
            </GlobalProvider>
          </PersistGate>
        </Provider>
      </RootSiblingParent>
    </AutocompleteDropdownContextProvider>
  );
};
export default App;
