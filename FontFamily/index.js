/* eslint-disable prettier/prettier */
const fonts = {
    BLACK: 'Inter-Black',
    BOLD: 'Inter-Bold',
    LIGHT: 'Inter-Light',
    MEDIUM: 'Inter-Medium',
    REGULAR: 'Inter-Regular',
    SEMI_BOLD: 'Inter-SemiBold',
    EXTRA_BOLD: 'Inter-ExtraBold',
    EXTRA_LIGHT: 'Inter-ExtraLight',
    EXTRA_SEMI_BOLD: 'Inter-ExtraSemiBold',
};
export default fonts;
